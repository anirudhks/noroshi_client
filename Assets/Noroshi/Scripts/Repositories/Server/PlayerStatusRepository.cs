using System;
using Noroshi.Core.WebApi.Response;
using Noroshi.WebApi;
using UniRx;
using Noroshi.Datas.Request;


namespace Noroshi.Repositories.Server
{
    public class PlayerStatusRepository : PlayerDataRepository<PlayerStatus>
    {
        protected override string _url()
        {
            return "PlayerStatus/";
        }

        public IObservable<PlayerStatus> Get()
        {
            _webApiRequester = _webApiRequester ?? new WebApiRequester();
            return _webApiRequester.Get<PlayerStatus>(_url() + "Get");

        }

        public override IObservable<PlayerStatus> Get(uint id)
        {
            _webApiRequester = _webApiRequester ?? new WebApiRequester();
            var sendData = new PlayerStatusIDRequest() {ID = id};
            return _webApiRequester.Post<PlayerStatusIDRequest, PlayerStatus>(_url() + "GetOther", sendData);
        }

        public override IObservable<PlayerStatus[]> GetAll()
        {
            throw new Exception();
        }

        protected virtual IObservable<PlayerStatus[]> GetData()
        {
            throw new Exception();
        }
    }
}