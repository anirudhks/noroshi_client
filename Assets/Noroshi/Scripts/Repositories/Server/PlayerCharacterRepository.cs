using System;
using Noroshi.Core.WebApi.Response;
using Noroshi.Core.WebApi.Response.Character;
using Noroshi.WebApi;
using UniRx;
using UniLinq;
using Noroshi.Core.Game.Character;
using Noroshi.Datas.Request;

namespace Noroshi.Repositories.Server
{
    public class PlayerCharacterRepository : PlayerDataRepository<PlayerCharacter>
    {
        private WebApiRequester _webApiRequester;

        public override IObservable<PlayerCharacter[]> GetAll()
        {
            return Noroshi.Character.WebApiRequester.List().Select(res => res.PlayerCharacters);
        }


        public IObservable<PlayerCharacter[]> GetAllCharacters()
        {
            _webApiRequester = _webApiRequester ?? new WebApiRequester();
            return _webApiRequester.Get<PlayerCharacter[]>(_url() + "GetAllCharacters");
        }


    protected override string _url()
        {
            return "PlayerCharacter/";
        }
    }
}