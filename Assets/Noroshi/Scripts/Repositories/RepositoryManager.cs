using UniRx;
using Noroshi.Repositories.Server;

namespace Noroshi.Repositories
{
    public class RepositoryManager
    {
        public PlayerCharacterRepository PlayerCharacterRepository;
        public PlayerStatusRepository PlayerStatusRepository;
        public PlayerArenaRepository PlayerArenaRepository;
        public InventoryRepository InventoryRepository;


        public RepositoryManager()
        {
            PlayerCharacterRepository = new PlayerCharacterRepository();
            PlayerStatusRepository = new PlayerStatusRepository();
            PlayerArenaRepository = new PlayerArenaRepository();
            InventoryRepository = new InventoryRepository();
        }

        /// プレイヤーキャラクターID指定でデータを引きつつ、キャラクターステータスを返してくれるメソッド。
        /// バトル依存ではないのでキャラクターステータスクラスの名前空間は後で変える。
        /// バルク処理も後で考える。
        public IObservable<BattleScene.CharacterStatus> LoadCharacterStatusByPlayerCharacterID(uint id)
        {
            return PlayerCharacterRepository.Get(id).Select(playerData =>
            {
                var masterData = GlobalContainer.MasterManager.CharacterMaster.Get(playerData.CharacterID);
                return new BattleScene.CharacterStatus(playerData, masterData);
            });
        }
    }
}
