using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.Game.Character;

namespace Noroshi.UI {
    public class TrialController : MonoBehaviour {
        [SerializeField] TrialTrainingPanel[] panelList;
        [SerializeField] TrialTrainingStageInfo stageInfo;
        [SerializeField] NotOpenModal notOpenModal;
        [SerializeField] AlertModal noChanceAlert;

        private uint stageID;
        private List<Noroshi.Core.WebApi.Response.Trial.Trial> trialDataList = new List<Noroshi.Core.WebApi.Response.Trial.Trial>();
        private List<List<TrialTrainingStageInfo.StageData>> stageDataList = new List<List<TrialTrainingStageInfo.StageData>>();
        private List<NotOpenModal.StageData> notOpenDataList = new List<NotOpenModal.StageData>();
        private List<CharacterTagSet> tagFlagsList = new List<CharacterTagSet>();
        private int selectedIndex;
        private bool isLoad = false;

        private void Start() {
            if(SoundController.Instance != null) {
                SoundController.Instance.PlayBGM(SoundController.BGMKeys.TOMB);
            }

            Noroshi.Trial.WebApiRequester.List().Do(trialData => {
                var gclm = GlobalContainer.LocalizationManager;
                for(int i = 0, iz = panelList.Length; i < iz; i++) {
                    var tempList = new List<TrialTrainingStageInfo.StageData>();
                    
                    trialDataList.Add(trialData.Trials[i]);
                    SetNotOpenData(trialData.Trials[i]);
                    if(string.IsNullOrEmpty(trialData.Trials[i].TagFlags)) {
                        tagFlagsList.Add(null);
                    } else {
                        tagFlagsList.Add(new CharacterTagSet(trialData.Trials[i].TagFlags));
                    }
                    panelList[i].SetPanel(SetPanelData(trialData.Trials[i]));
                    for(int j = 0, jz = trialData.Trials[i].Stages.Length; j < jz; j++) {
                        var stage = trialData.Trials[i].Stages[j];
                        var stageData = new TrialTrainingStageInfo.StageData();
                        stageData.StageID = stage.ID;
                        stageData.Name = trialData.Trials[i].TextKey;
                        stageData.Level = (int)stage.Level;
                        stageData.IsOpen = stage.IsOpen;
                        stageData.ItemList = stage.DroppableRewards;
                        if(trialData.Trials[i].ID == 1) {
                            stageData.Description = gclm.GetText("UI.Alert.DisablePhysicalStage");
                        } else if(trialData.Trials[i].ID == 2) {
                            stageData.Description = gclm.GetText("UI.Alert.DisableMagicStage");
                        } else if(trialData.Trials[i].ID == 3) {
                            stageData.Description = gclm.GetText("UI.Alert.OnlyDemonAndHamStage");
                        } else {
                            stageData.Description = gclm.GetText("UI.Alert.OnlyHumanAndHamStage");
                        }
                        tempList.Add(stageData);
                    }
                    stageDataList.Add(tempList);
                }
                isLoad = true;
            }).Subscribe();

            foreach(var panel in panelList) {
                panel.OnPanelClicked.Subscribe(index => {
                    selectedIndex = index;
                    if(trialDataList[index].IsOpen) {
                        stageInfo.SetStageInfo(stageDataList[index]);
                    } else {
                        notOpenModal.SetStageInfo(notOpenDataList[index]);
                    }
                });
                panel.OnNoChance.Subscribe(_ => {
                    noChanceAlert.OnOpen();
                });
            }
            
            stageInfo.OnEditCharacter.Subscribe(id => {
                var defaultCharacterIdList = BattleCharacterSelect.Instance.GetDefaultCharacter(SaveKeys.DefaultTrialBattleCharacter + selectedIndex);
                stageID = (uint)id;
                BattleCharacterSelect.Instance.OpenPanel(false, defaultCharacterIdList, null, null, tagFlagsList[selectedIndex]);
            });

            BattleCharacterSelect.Instance.OnStartBattle.Subscribe(data => {
                BattleCharacterSelect.Instance.SaveDefaultCharacter(SaveKeys.DefaultTrialBattleCharacter + selectedIndex, data.playerCharacterIDs);
                BattleScene.Bridge.Transition.TransitToTrialBattle(stageID, data.playerCharacterIDs, data.mercenaryID);
            }).AddTo(this);

            if(UILoading.Instance.GetPreviousHistory() != Constant.SCENE_BATTLE) {
                BattleCharacterSelect.Instance.ReloadCharacterList();
            }

            BackButtonController.Instance.OnBack.Subscribe(_ => {
                UILoading.Instance.LoadScene(Constant.SCENE_MAIN);
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            StartCoroutine(OnLoading());
        }

        private IEnumerator OnLoading() {
            while(!isLoad || !BattleCharacterSelect.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            UILoading.Instance.HideLoading();
        }

        private TrialTrainingPanel.PanelData SetPanelData(Noroshi.Core.WebApi.Response.Trial.Trial trialData) {
            var panelData = new TrialTrainingPanel.PanelData();
            
            panelData.Name = trialData.TextKey;
            panelData.OpenDayOfWeeks = trialData.OpenDayOfWeeks;
            panelData.IsOpen = trialData.IsOpen;
            panelData.BattleNum = trialData.BattleNum;
            panelData.MaxBattleNum = Noroshi.Core.Game.Trial.Constant.MAX_BATTLE_NUM;
            panelData.ReopenedAt = trialData.ReopenedAt == null ? 0 : (uint)trialData.ReopenedAt;
            return panelData;
        }

        private void SetNotOpenData(Noroshi.Core.WebApi.Response.Trial.Trial trialData) {
            var gclm = GlobalContainer.LocalizationManager;
            var notOpenData = new NotOpenModal.StageData();
            
            notOpenData.Title = trialData.TextKey;
            if(trialData.ID == 1) {
                notOpenData.Description = gclm.GetText("UI.Alert.DisablePhysicalStage");
            } else if(trialData.ID == 2) {
                notOpenData.Description = gclm.GetText("UI.Alert.DisableMagicStage");
            } else if(trialData.ID == 3) {
                notOpenData.Description = gclm.GetText("UI.Alert.OnlyDemonAndHamStage");
            } else {
                notOpenData.Description = gclm.GetText("UI.Alert.OnlyHumanAndHamStage");
            }
            notOpenData.OpenDayOfWeeks = trialData.OpenDayOfWeeks;
            notOpenDataList.Add(notOpenData);
        }
    }
}
