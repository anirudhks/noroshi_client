using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.WebApi.Response.Battle;
using Noroshi.Core.WebApi.Response.Possession;
using Noroshi.Core.Game.Possession;

namespace Noroshi.UI {
    public class TrainingController : MonoBehaviour {
        [SerializeField] TrialTrainingPanel[] panelList;
        [SerializeField] TrialTrainingStageInfo stageInfo;
        [SerializeField] NotOpenModal notOpenModal;
        [SerializeField] AlertModal noChanceAlert;
        [SerializeField] Text txtDescription;

        private uint stageID;
        private List<Noroshi.Core.WebApi.Response.Training.Training> trainingDataList = new List<Noroshi.Core.WebApi.Response.Training.Training>();
        private List<List<TrialTrainingStageInfo.StageData>> stageDataList = new List<List<TrialTrainingStageInfo.StageData>>();
        private List<NotOpenModal.StageData> notOpenDataList = new List<NotOpenModal.StageData>();
        private int selectedIndex;
        private bool isLoad = false;

        private void Start() {
            if(SoundController.Instance != null) {
                SoundController.Instance.PlayBGM(SoundController.BGMKeys.COCOPIPI);
            }

            Noroshi.Training.WebApiRequester.List().Do(trainingData => {
                for(int i = 0, iz = panelList.Length; i < iz; i++) {
                    var tempList = new List<TrialTrainingStageInfo.StageData>();

                    trainingDataList.Add(trainingData.Trainings[i]);
                    SetNotOpenData(trainingData.Trainings[i]);
                    panelList[i].SetPanel(SetPanelData(trainingData.Trainings[i]));
                    for(int j = 0, jz = trainingData.Trainings[i].Stages.Length; j < jz; j++) {
                        var stage = trainingData.Trainings[i].Stages[j];
                        var stageData = new TrialTrainingStageInfo.StageData();
                        var enemyList = new List<BattleCharacter>();
                        var itemList = new List<PossessionObject>();
                        stageData.StageID = stage.ID;
                        stageData.Name = trainingData.Trainings[i].TextKey;
                        stageData.Level = (int)stage.Level;
                        stageData.NecessaryPlayerLevel = stage.NecessaryPlayerLevel;
                        stageData.IsOpen = stage.IsOpen;
                        stageData.NecessaryPlayerLevel = (int)stage.NecessaryPlayerLevel;
                        stageData.Score = (int)stage.Score;
                        for(int k = 0, kz = Mathf.Min(5, stage.CharacterIDs.Length); k < kz; k++) {
                            var battleCharacter = new BattleCharacter();
                            battleCharacter.CharacterID = stage.CharacterIDs[k];
                            battleCharacter.Level = stage.NecessaryPlayerLevel;
                            battleCharacter.PromotionLevel = 1;
                            battleCharacter.EvolutionLevel = 1;
                            enemyList.Add(battleCharacter);
                        }
                        if(trainingData.Trainings[i].Type == Noroshi.Core.Game.Training.TrainingType.DefeatingNum) {
                            var drugs = GlobalContainer.MasterManager.ItemMaster.GetDrugs();
                            foreach(var drug in drugs) {
                                var possession = new PossessionObject();
                                possession.TextKey = drug.TextKey;
                                possession.ID = drug.ID;
                                possession.Category = (byte)PossessionCategory.Drug;
                                possession.PossessingNum = ItemListManager.Instance.GetItemCount(drug.ID);
                                itemList.Add(possession);
                            }
                            stageData.Description = GlobalContainer.LocalizationManager.GetText("UI.Heading.MaxDefeatNum");
                        } else {
                            var gifts = GlobalContainer.MasterManager.ItemMaster.GetExchangeCashGifts();
                            foreach(var gift in gifts) {
                                var possession = new PossessionObject();
                                possession.TextKey = gift.TextKey;
                                possession.ID = gift.ID;
                                possession.Category = (byte)PossessionCategory.Drug;
                                possession.PossessingNum = ItemListManager.Instance.GetItemCount(gift.ID);
                                itemList.Add(possession);
                            }
                            stageData.Description = GlobalContainer.LocalizationManager.GetText("UI.Heading.MaxDamage");
                        }
                        stageData.EnemyList = enemyList.ToArray();
                        stageData.ItemList = itemList.ToArray();
                        tempList.Add(stageData);
                    }
                    stageDataList.Add(tempList);
                }
                for(int i = trainingDataList.Count - 1; i > -1; i--) {
                    if(trainingDataList[i].IsOpen) {
                        txtDescription.text = GlobalContainer.LocalizationManager.GetText(trainingDataList[i].TextKey + ".Description");
                    }
                }
                isLoad = true;
            }).Subscribe();

            foreach(var panel in panelList) {
                panel.OnPanelClicked.Subscribe(index => {
                    selectedIndex = index;
                    if(trainingDataList[index].IsOpen) {
                        stageInfo.SetStageInfo(stageDataList[index]);
                    } else {
                        notOpenModal.SetStageInfo(notOpenDataList[index]);
                    }
                });
                panel.OnNoChance.Subscribe(_ => {
                    noChanceAlert.OnOpen();
                });
            }

            stageInfo.OnEditCharacter.Subscribe(id => {
                var defaultCharacterIdList = BattleCharacterSelect.Instance.GetDefaultCharacter(SaveKeys.DefaultTrainingBattleCharacter + selectedIndex);
                stageID = (uint)id;
                BattleCharacterSelect.Instance.OpenPanel(false, defaultCharacterIdList);
            });

            BattleCharacterSelect.Instance.OnStartBattle.Subscribe(data => {
                BattleCharacterSelect.Instance.SaveDefaultCharacter(SaveKeys.DefaultTrainingBattleCharacter + selectedIndex, data.playerCharacterIDs);
                BattleScene.Bridge.Transition.TransitToTrainingBattle(stageID, data.playerCharacterIDs, data.mercenaryID);
            }).AddTo(this);

            if(UILoading.Instance.GetPreviousHistory() != Constant.SCENE_BATTLE) {
                BattleCharacterSelect.Instance.ReloadCharacterList();
            }

            BackButtonController.Instance.OnBack.Subscribe(_ => {
                UILoading.Instance.LoadScene(Constant.SCENE_MAIN);
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            StartCoroutine(OnLoading());
        }

        private IEnumerator OnLoading() {
            while(!isLoad || !BattleCharacterSelect.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            UILoading.Instance.HideLoading();
        }

        private TrialTrainingPanel.PanelData SetPanelData(Noroshi.Core.WebApi.Response.Training.Training trainingData) {
            var panelData = new TrialTrainingPanel.PanelData();

            panelData.Name = trainingData.TextKey;
            panelData.OpenDayOfWeeks = trainingData.OpenDayOfWeeks;
            panelData.IsOpen = trainingData.IsOpen;
            panelData.BattleNum = trainingData.BattleNum;
            panelData.MaxBattleNum = Noroshi.Core.Game.Training.Constant.MAX_BATTLE_NUM;
            panelData.ReopenedAt = trainingData.ReopenedAt == null ? 0 : (uint)trainingData.ReopenedAt;
            return panelData;
        }

        private void SetNotOpenData(Noroshi.Core.WebApi.Response.Training.Training trainingData) {
            var notOpenData = new NotOpenModal.StageData();
            
            notOpenData.Title = trainingData.TextKey;
            if(trainingData.Type == Noroshi.Core.Game.Training.TrainingType.DefeatingNum) {
                notOpenData.Description = GlobalContainer.LocalizationManager.GetText("UI.Alert.DefeatNumStage");
            } else {
                notOpenData.Description = GlobalContainer.LocalizationManager.GetText("UI.Alert.MaxDamageStage");
            }
            notOpenData.OpenDayOfWeeks = trainingData.OpenDayOfWeeks;
            notOpenDataList.Add(notOpenData);
        }
    }
}
