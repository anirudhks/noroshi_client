using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.Game.Story;
using Noroshi.Core.WebApi.Response.Story;
using Noroshi.Core.WebApi.Response.Battle;
using Noroshi.Core.WebApi.Response.Possession;

namespace Noroshi.UI {
    public class StoryEpisodePanel : MonoBehaviour {
        [SerializeField] int characterID;
        [SerializeField] int bossCharacterID;
        [SerializeField] GameObject guideArrow;
        [SerializeField] GameObject bossLabel;
        [SerializeField] GameObject objectWrapper;
        [SerializeField] BtnStage[] btnStageList;

        public Subject<StoryStageInfo.StageData> OnStageClicked = new Subject<StoryStageInfo.StageData>();
        public Subject<bool> OnEpisodeClear = new Subject<bool>();

        public GameObject PageWrapper;
        public GameObject PageLeft;
        public GameObject PageRight;

        private StoryStageInfo.StageData[] stageDataList;
        private GameObject character;
        private GameObject boss;
        private int crntPosition = -1;
        private int targetPosition = -1;
        private List<Vector2> stagePositionList = new List<Vector2>();
        private SkeletonAnimation skeleton;

        private void Init() {
            for(int i = 0, l = btnStageList.Length; i < l; i++) {
                var n = i;
                var stagePosition = objectWrapper.transform.InverseTransformPoint(btnStageList[i].transform.position);
                btnStageList[n].OnClickedBtn.Subscribe(index => {
                    OnStageClicked.OnNext(stageDataList[index]);
                    if(characterID > 0 && n != targetPosition) {
                        StartMoveCharacter(n);
                    }
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
                stagePositionList.Add(stagePosition);
            }
            if(characterID > 0) {
                character = Instantiate<GameObject>(GlobalContainer.AssetBundleManager.LoadFromCharacterPrefab((uint)characterID, "UICharacter"));
                character.name = "EpisodeCharacter";
                character.SetActive(false);
                skeleton = character.GetComponent<SkeletonAnimation>();
                character.transform.SetParent(objectWrapper.transform);
                character.transform.localScale = new Vector3(-20, 20, 20);
                character.transform.localPosition = stagePositionList[0];
                character.GetComponent<MeshRenderer>().sortingOrder = 2;
                TweenNull.Add(gameObject, 0.1f).Then(() => {character.SetActive(true);});
            }
        }

        private void StartMoveCharacter(int target) {
            if(target == crntPosition && skeleton.AnimationName != Constant.ANIM_RUN) {return;}
            var dir = target - crntPosition > 0 ? 1 : -1;
            if(skeleton.AnimationName != Constant.ANIM_RUN) {
                skeleton.state.SetAnimation(0, Constant.ANIM_RUN, true);
            }
            if(dir > 0) {
                if(skeleton.Skeleton.flipX) {
                    if(skeleton.AnimationName == Constant.ANIM_RUN) {
                        crntPosition -= dir;
                    }
                }
            } else {
                if((!skeleton.Skeleton.flipX && target < targetPosition) ||
                   (target > targetPosition && crntPosition <= target)) {
                    if(skeleton.AnimationName == Constant.ANIM_RUN) {
                        crntPosition -= dir;
                    }
                }
            }
            targetPosition = target;
            MoveCharacter(dir);
        }

        private void MoveCharacter(int direction) {
            var len = Vector2.Distance(character.transform.localPosition, stagePositionList[crntPosition + direction]);
            if(character.transform.localPosition.x - stagePositionList[crntPosition + direction].x > 0) {
                skeleton.Skeleton.flipX = true;
            } else {
                skeleton.Skeleton.flipX = false;
            }
            TweenXY.Add(character, len / 200f, stagePositionList[crntPosition + direction]).Then(() => {
                crntPosition += direction;
                if(crntPosition != targetPosition) {
                    MoveCharacter(direction);
                } else {
                    skeleton.state.SetAnimation(0, Constant.ANIM_IDLE, true);
                    skeleton.Skeleton.flipX = false;
                }
            });
        }

        public void SetEpisodeMap(uint stageID, StoryStageInfo.StageData[] playerStageDataList) {
            int stageLength = playerStageDataList.Length;
            Init();
            stageDataList = playerStageDataList;
            for(int i = 0, l = btnStageList.Length; i < l; i++) {
                var bossID = stageDataList[i].EnemyList[stageDataList[i].EnemyList.Length - 1].CharacterID;
                if(stageDataList[i].IsOpen) {
                    btnStageList[i].id = i;
                    if(stageID == stageDataList[i].StageID
                        || stageDataList[i].Rank < 1
                        || (i == stageLength - 1 && stageDataList[i].Rank > 0)
                    ) {
                        if(crntPosition < 0) {
                            crntPosition = i;
                            targetPosition = i;
                            if(characterID > 0) {
                                character.transform.localPosition = stagePositionList[i];
                            }
                        }
                        if(stageID == stageDataList[i].StageID
                           && UILoading.Instance.GetQuery(QueryKeys.FromCharacterList) > 0) {
                            OnStageClicked.OnNext(stageDataList[i]);
                        }
                    }
                    if(!guideArrow.activeSelf && stageDataList[i].Rank < 1) {
                        var guideArrowPosition = stagePositionList[i];
                        guideArrowPosition.y -= 80;
                        guideArrow.transform.localPosition = guideArrowPosition;
                        guideArrow.SetActive(true);
                    }
                    if(stageDataList[i].Rank < 1) {
                        if(stageDataList[i].Type != StageType.NormalStage) {
                            btnStageList[i].SetButtonState(false, true, true, bossID);
                        } else {
                            btnStageList[i].SetButtonState(false, false, true, bossID);
                        }
                    } else {
                        if(stageDataList[i].Type != StageType.NormalStage) {
                            btnStageList[i].SetButtonState(true, true, false, bossID, (byte)stageDataList[i].Rank);
                        } else {
                            btnStageList[i].SetButtonState(true, false, false, bossID);
                        }
                    }
                    if(i == stageLength - 1 && stageDataList[i].Rank > 0) {
                        if(bossLabel != null) {bossLabel.SetActive(false);}
                    }
                    if(UILoading.Instance.GetQuery(QueryKeys.IsChallengeNewStage) == (int)stageDataList[i].StageID
                       && stageDataList[i].Rank > 0) {
                        PlayerInfo.Instance.SetLastStory();
                    }
                } else {
                    btnStageList[i].SetButtonState(false, false, false, bossID);
                }
                if((i == stageLength - 1 && stageDataList[i].Rank == 0) && bossCharacterID > 0) {
                    boss = Instantiate<GameObject>(GlobalContainer.AssetBundleManager.LoadFromCharacterPrefab((uint)bossCharacterID, "UICharacter"));
                    boss.SetActive(false);
                    boss.name = "EpisodeBoss";
                    boss.transform.SetParent(objectWrapper.transform);
                    boss.transform.localScale = new Vector3(20, 20, 20);
                    boss.transform.localPosition = stagePositionList[stagePositionList.Count - 1];
                    boss.GetComponent<MeshRenderer>().sortingOrder = 1;
                    Observable.Timer(TimeSpan.FromSeconds(0.1f)).Subscribe(_ => {
                        boss.SetActive(true);
                    });
                }
            }

            if(crntPosition == stageLength - 1
               && UILoading.Instance.GetQuery(QueryKeys.IsChallengeFinalStage) == (int)stageDataList[stageLength - 1].StageID
               && stageDataList[stageLength - 1].Rank > 0) {
                OnEpisodeClear.OnNext(true);
                UILoading.Instance.RemoveQuery(QueryKeys.IsChallengeFinalStage);
            }
        }

        public void ShowMapObject() {
            objectWrapper.SetActive(true);
            Observable.TimerFrame(1).Subscribe(_ => {
            if(characterID > 0) {
            foreach(var atlas in skeleton.skeletonDataAsset.atlasAssets) {
                foreach(var m in atlas.materials) {
                    var shader = m.shader;
                        m.shader = null;
                        m.shader = shader;
                    }
                }
                skeleton.skeletonDataAsset.Reset();
            }
            if(boss != null) {
                var bossSkeleton = boss.GetComponent<SkeletonAnimation>();
                foreach(var atlas in bossSkeleton.skeletonDataAsset.atlasAssets) {
                    foreach(var m in atlas.materials) {
                        var shader = m.shader;
                        m.shader = null;
                        m.shader = shader;
                    }
                }
                bossSkeleton.skeletonDataAsset.Reset();
                }
            });
        }

        public void HideMapObject() {
            objectWrapper.SetActive(false);
        }
    }
}
