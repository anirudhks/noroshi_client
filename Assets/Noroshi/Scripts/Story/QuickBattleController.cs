﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;

namespace Noroshi.UI {
    public class QuickBattleController : MonoBehaviour {
        [SerializeField] Text txtStamina;
        [SerializeField] Text txtHaveTicket;
        [SerializeField] Dropdown battleNumSelector;
        [SerializeField] BtnCommon btnQuickBattle;
        [SerializeField] GameObject alertLotQuickBattle;
        [SerializeField] AlertModal noTicketAlert;
        [SerializeField] GameObject quickBattleResult;
        [SerializeField] Text txtEXP;
        [SerializeField] Text txtGold;
        [SerializeField] ItemIcon[] getItemList;
        [SerializeField] Text[] txtGetItemNumList;
        [SerializeField] ItemIcon[] getDrugList;
        [SerializeField] Text[] txtGetDrugNumList;
        [SerializeField] BtnCommon btnClose;
        [SerializeField] GameObject processing;
        [SerializeField] CaptionItem captionItem;

        public Subject<uint> OnPlayQuickBattle = new Subject<uint>();
        public Subject<bool> OnNotEnoughStamina = new Subject<bool>();
        public Subject<bool> OnNoChance = new Subject<bool>();

        private StoryStageInfo.StageData stageData;
        private int lotQuickBattleNum;
        private uint ticketNum = 0;
        private uint raidTicketID;
        private bool isCaptionOpen = false;
        private List<CaptionItem.ItemInfo> itemInfoList;
        private Noroshi.Core.WebApi.Response.Players.AddPlayerExpResult addPlayerExpResult;

        private void Start() {
            foreach(var btnItem in getItemList) {
                btnItem.OnTouchBtn.Subscribe(index => {
                    var pos = getItemList[index].transform.position;
                    if(isCaptionOpen) {return;}
                    isCaptionOpen = true;
                    captionItem.ShowCaption(itemInfoList[index], pos);
                }).AddTo(btnItem);
                btnItem.OnReleaseBtn.Subscribe(index => {
                    isCaptionOpen = false;
                    captionItem.HideCaption();
                }).AddTo(btnItem);
            }

            PlayerInfo.Instance.OnChangePlayerStatus.Subscribe(_ => {
                SetQuickBattle(stageData);
            }).AddTo(this);

            btnQuickBattle.OnClickedBtn.Subscribe(_ => {
                StartQuickBattle(lotQuickBattleNum);
            });
            btnQuickBattle.OnPlaySE.Subscribe(_ => {
                if(PlayerInfo.Instance.CurrentStamina < stageData.Stamina
                    || ticketNum < lotQuickBattleNum) {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.ERROR);
                } else {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
                }
            });

            btnClose.OnClickedBtn.Subscribe(_ => {
                PlayerInfo.Instance.UpdatePlayerExp(addPlayerExpResult);
                TweenA.Add(quickBattleResult, 0.15f, 0).Then(() => {
                    quickBattleResult.SetActive(false);
                });
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });
        }

        private void GetTicketID() {
            var tickets = GlobalContainer.MasterManager.ItemMaster.GetRaidTickets();
            raidTicketID = tickets[0].ID;
            SetQuickBattle(stageData);
        }

        private void StartQuickBattle(int num) {
            if(Noroshi.Core.Game.Story.Constant.MAX_BACK_STORY_STAGE_BATTLE_NUM - stageData.PlayCount < 1) {
                OnNoChance.OnNext(true);
                return;
            }
            if(PlayerInfo.Instance.CurrentStamina < stageData.Stamina) {
                OnNotEnoughStamina.OnNext(true);
                return;
            }
            if(ticketNum < num) {
                noTicketAlert.OnOpen();
                return;
            }
            processing.SetActive(true);
            ItemListManager.Instance.ChangeItemCount(raidTicketID, -num);
            OnPlayQuickBattle.OnNext(stageData.PlayCount + (uint)num);
            Noroshi.Story.WebApiRequester.StartQuickBattle(stageData.StageID, (byte)num).Do(res => {
                SetResult(res);
                processing.SetActive(false);
                quickBattleResult.SetActive(true);
                TweenA.Add(quickBattleResult, 0.15f, 1).From(0);
            }).Subscribe();
        }

        private void SetResult(Noroshi.Core.WebApi.Response.ExecuteQuickBattleResponse res) {
            var gclm = GlobalContainer.LocalizationManager;
            var possessionList = new List<Noroshi.Core.WebApi.Response.Possession.PossessionObject>();
            itemInfoList = new List<CaptionItem.ItemInfo>();
            txtEXP.text = res.GetExp.Num.ToString();
            txtGold.text = res.GetGold.Num.ToString();
            for(int i = 0, iz = res.DropRewards.Length; i < iz; i++) {
                for(int j = 0, jz = res.DropRewards[i].Length; j < jz; j++) {
                    var itemIndex = GetContainsItemIndex(res.DropRewards[i][j].ID);
                    if(itemIndex > -1) {
                        itemInfoList[itemIndex].GetNum += res.DropRewards[i][j].Num;
                        itemInfoList[itemIndex].HaveNum = res.DropRewards[i][j].PossessingNum;
                    } else {
                        var itemInfo = new CaptionItem.ItemInfo();
                        var rarity = ItemListManager.Instance.GetItemRarity(res.DropRewards[i][j]);
                        itemInfo.Name = gclm.GetText(res.DropRewards[i][j].TextKey + ".Name");
                        itemInfo.Description = gclm.GetText(res.DropRewards[i][j].TextKey + ".Description").TrimStart();
                        itemInfo.GetNum = res.DropRewards[i][j].Num;
                        itemInfo.HaveNum = res.DropRewards[i][j].PossessingNum;
                        itemInfo.ID = res.DropRewards[i][j].ID;
                        itemInfo.Rarity = rarity;
                        itemInfo.Category = res.DropRewards[i][j].Category;
                        itemInfoList.Add(itemInfo);
                        possessionList.Add(res.DropRewards[i][j]);
                    }
                }
            }
            for(int i = 0, l = getItemList.Length; i < l; i++) {
                if(i < itemInfoList.Count) {
                    var isSoul = itemInfoList[i].Category == (byte)Noroshi.Core.Game.Possession.PossessionCategory.Soul;
                    txtGetItemNumList[i].text = itemInfoList[i].GetNum > 1 ? "x" + itemInfoList[i].GetNum : "";
                    getItemList[i].SetItemIcon(possessionList[i]);
                    ItemListManager.Instance.ChangeItemCount(itemInfoList[i].ID, (int)itemInfoList[i].GetNum, isSoul);
                    getItemList[i].gameObject.SetActive(true);
                    txtGetItemNumList[i].gameObject.SetActive(true);
                } else {
                    getItemList[i].gameObject.SetActive(false);
                    txtGetItemNumList[i].gameObject.SetActive(false);
                }
            }
            for(int i = 0, l = getDrugList.Length; i < l; i++) {
                if(i < res.DrugRewards.Length) {
                    getDrugList[i].SetItemIcon(res.DrugRewards[i]);
                    txtGetDrugNumList[i].text = res.DrugRewards[i].Num > 1 ? "x" + res.DrugRewards[i].Num : "";
                    getDrugList[i].gameObject.SetActive(true);
                    txtGetDrugNumList[i].gameObject.SetActive(true);
                    ItemListManager.Instance.ChangeItemCount(res.DrugRewards[i].ID, (int)res.DrugRewards[i].Num);
                } else {
                    getDrugList[i].gameObject.SetActive(false);
                    txtGetDrugNumList[i].gameObject.SetActive(false);
                }
            }
            if(res.RaidBossAtDiscoveries.Length > 0) {
                foreach(var raidboss in res.RaidBossAtDiscoveries) {
                    var appearModal = Instantiate(Resources.Load<AppearRaidBossModal>("UI/AppearRaidBossModal"));
                    TweenNull.Add(gameObject, 1.0f).Then(() => {
                        appearModal.OpenModal(raidboss);
                    });
                }
            }
            addPlayerExpResult = res.AddPlayerExpResult;
        }

        private int GetContainsItemIndex(uint id) {
            int index = -1;
            for(int i = 0, l = itemInfoList.Count; i < l; i++) {
                if(itemInfoList[i].ID == id) {
                    index = i;
                    break;
                }
            }
            return index;
        }

        public void SetQuickBattle(StoryStageInfo.StageData data) {
            var maxBattleNum = 0;
            stageData = data;
            if(raidTicketID <= 0) {
                GetTicketID();
            } else {
                battleNumSelector.ClearOptions();
                ticketNum = ItemListManager.Instance.GetItemCount(raidTicketID);
                txtHaveTicket.text = ticketNum.ToString();
                if(ticketNum < 1) {
                    txtHaveTicket.color = Constant.TEXT_COLOR_NEGATIVE;
                } else {
                    txtHaveTicket.color = Constant.TEXT_COLOR_NORMAL_WHITE;
                }
                if(PlayerInfo.Instance.GetPlayerStatus().VipLevel < 3) {
                    maxBattleNum = 1;
                    alertLotQuickBattle.SetActive(true);
                } else if(data.Type == Noroshi.Core.Game.Story.StageType.BackStoryStage) {
                    var remainBattleCount = (int)Noroshi.Core.Game.Story.Constant.MAX_BACK_STORY_STAGE_BATTLE_NUM - (int)data.PlayCount;
                    maxBattleNum = (int)Mathf.Floor(PlayerInfo.Instance.CurrentStamina / data.Stamina);
                    if(ticketNum < maxBattleNum) {maxBattleNum = (int)ticketNum;}
                    if(remainBattleCount < maxBattleNum) {maxBattleNum = remainBattleCount;}
                    alertLotQuickBattle.SetActive(false);
                } else {
                    maxBattleNum = (int)Mathf.Min(ticketNum, Mathf.Floor(PlayerInfo.Instance.CurrentStamina / data.Stamina));
                    if(maxBattleNum > 30) {maxBattleNum = 30;}
                    alertLotQuickBattle.SetActive(false);
                }
                for(int i = 0, l = maxBattleNum; i < l; i++) {
                    var item = new Dropdown.OptionData((i + 1).ToString());
                    battleNumSelector.options.Add(item);
                }
                battleNumSelector.value = 0;
                if(battleNumSelector.options.Count > 0) {
                    lotQuickBattleNum = 1;
                    battleNumSelector.captionText.text = battleNumSelector.options[0].text;
                } else {
                    lotQuickBattleNum = 0;
                    battleNumSelector.captionText.text = "0";
                }
                txtStamina.text = stageData.Stamina.ToString();
            }
        }

        public void OpenQuickBattle() {
            gameObject.SetActive(true);
            TweenX.Add(gameObject, 0.2f, 315).EaseOutCubic();
            TweenX.Add(btnQuickBattle.gameObject, 0.2f, 209).EaseOutCubic();
        }

        public void CloseQuickBattle() {
            TweenX.Add(btnQuickBattle.gameObject, 0.2f, 209).EaseOutCubic();
            TweenX.Add(gameObject, 0.2f, 195).EaseOutCubic().Then(() => {
                gameObject.SetActive(false);
            });
        }

        public void OnSelectorValueChanged(int value) {
            lotQuickBattleNum = value + 1;
            txtStamina.text = (lotQuickBattleNum * stageData.Stamina).ToString();
        }
    }
}
