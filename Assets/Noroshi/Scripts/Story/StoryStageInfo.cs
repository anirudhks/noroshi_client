using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniLinq;
using LitJson;
using Noroshi.Game;
using Noroshi.Core.Game.Story;
using Noroshi.Core.WebApi.Response.Battle;
using Noroshi.Core.WebApi.Response.Possession;

namespace Noroshi.UI {
    public class StoryStageInfo : MonoBehaviour {
        public class StageData {
            public string Name;
            public bool IsOpen;
            public int Stamina;
            public int Rank = 0;
            public uint PlayCount = 0;
            public StageType Type;
            public BattleCharacter[] EnemyList;
            public PossessionObject[] ItemList;
            public uint StageID;
            public int ChapterIndex;
            public int EpisodeIndex;
            public int TotalEpisodeNo;
            public int StageIndex;
            public uint[] RequireIDList;
            public uint[] CPUIDList;
            public Noroshi.Core.Game.Character.CharacterTagSet TagFlags;
        }

        [SerializeField] Text txtTitle;
        [SerializeField] Text txtChapterIndex;
        [SerializeField] Text txtEpisodeIndex;
        [SerializeField] Text txtStageIndex;
        [SerializeField] Text txtStamina;
        [SerializeField] Text txtRemainChance;
        [SerializeField] Text txtMaxChance;
        [SerializeField] GameObject stageInfoPanel;
        [SerializeField] GameObject remainContainer;
        [SerializeField] Image[] scoreStarList;
        [SerializeField] Sprite starSpriteOn;
        [SerializeField] Sprite starSpriteOff;
        [SerializeField] CharacterStatusIcon[] enemyIconList;
        [SerializeField] BtnCaption[] btnEnemyCaptionList;
        [SerializeField] CaptionCharacter captionCharacter;
        [SerializeField] ItemIcon[] itemIconList;
        [SerializeField] CaptionItem captionItem;
        [SerializeField] BtnCommon overlay;
        [SerializeField] BtnCommon btnBattle;
        [SerializeField] BtnCommon btnOpenQuickBattle;
        [SerializeField] BtnCommon btnAlertNotOpenQuickBattle;
        [SerializeField] GameObject notOpenQuickBattlePanel;
        [SerializeField] QuickBattleController quickBattleController;
        [SerializeField] AlertModal noStaminaAlert;
        [SerializeField] AlertModal noChanceAlert;

        public Subject<uint> OnEditCharacter = new Subject<uint>();

        private StageData stageData;
        private bool isQuickBattleOpen = false;
        private bool isCaptionOpen = false;
        private List<CaptionCharacter.CharacterInfo> enemyInfoList;
        private List<CaptionItem.ItemInfo> itemInfoList;
        private bool isOpen = false;

        private void Start() {
            foreach(var btnEnemy in btnEnemyCaptionList) {
                btnEnemy.OnTouchBtn.Subscribe(index => {
                    var pos = btnEnemyCaptionList[index].transform.position;
                    if(isCaptionOpen) {return;}
                    isCaptionOpen = true;
                    captionCharacter.ShowCaption(enemyInfoList[index], pos);
                }).AddTo(btnEnemy);
                btnEnemy.OnReleaseBtn.Subscribe(index => {
                    isCaptionOpen = false;
                    captionCharacter.HideCaption();
                }).AddTo(btnEnemy);
            }
            foreach(var btnItem in itemIconList) {
                btnItem.OnTouchBtn.Subscribe(index => {
                    var pos = itemIconList[index].transform.position;
                    if(isCaptionOpen) {return;}
                    isCaptionOpen = true;
                    captionItem.ShowCaption(itemInfoList[index], pos);
                }).AddTo(btnItem);
                btnItem.OnReleaseBtn.Subscribe(index => {
                    isCaptionOpen = false;
                    captionItem.HideCaption();
                }).AddTo(btnItem);
            }

            overlay.OnClickedBtn.Subscribe(_ => {
                CloseQuickBattlePanel();
                CloseStageInfo();
            });
            overlay.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            quickBattleController.OnPlayQuickBattle.Subscribe(playCount => {
                UpdateRemainBattle(playCount);
            });
            quickBattleController.OnNoChance.Subscribe(_ => {
                noChanceAlert.OnOpen();
            });
            quickBattleController.OnNotEnoughStamina.Subscribe(_ => {
                noStaminaAlert.OnOpen();
            });

            btnOpenQuickBattle.OnClickedBtn.Subscribe(_ => {
                if(!isQuickBattleOpen) {
                    OpenQuickBattlePanel();
                } else {
                    CloseQuickBattlePanel();
                }
            });
            btnOpenQuickBattle.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnAlertNotOpenQuickBattle.OnTouchBtn.Subscribe(_ => {
                if(isCaptionOpen) {return;}
                isCaptionOpen = true;
                notOpenQuickBattlePanel.gameObject.SetActive(true);
            });
            btnAlertNotOpenQuickBattle.OnReleaseBtn.Subscribe(_ => {
                isCaptionOpen = false;
                notOpenQuickBattlePanel.gameObject.SetActive(false);
            });

            btnBattle.OnClickedBtn.Subscribe(_ => {
                if(stageData.Type == StageType.BackStoryStage
                    && Noroshi.Core.Game.Story.Constant.MAX_BACK_STORY_STAGE_BATTLE_NUM - stageData.PlayCount <= 0) {
                    noChanceAlert.OnOpen();
                    return;
                }
                if(PlayerInfo.Instance.CurrentStamina < stageData.Stamina) {
                    noStaminaAlert.OnOpen();
                    return;
                }
                OnEditCharacter.OnNext(stageData.StageID);
            });
            btnBattle.OnPlaySE.Subscribe(_ => {
                if(stageData.Type == StageType.BackStoryStage
                    && Noroshi.Core.Game.Story.Constant.MAX_BACK_STORY_STAGE_BATTLE_NUM - stageData.PlayCount <= 0) {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
                    return;
                }
                if(PlayerInfo.Instance.CurrentStamina < stageData.Stamina) {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
                    return;
                }
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });

            BackButtonController.Instance.OnCloseModalChild.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseStageInfo();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void OpenQuickBattlePanel() {
            isQuickBattleOpen = true;
            btnBattle.gameObject.SetActive(false);
            quickBattleController.OpenQuickBattle();
            TweenX.Add(stageInfoPanel, 0.2f, -150).EaseOutCubic();
        }

        private void CloseQuickBattlePanel() {
            isQuickBattleOpen = false;
            quickBattleController.CloseQuickBattle();
            TweenX.Add(stageInfoPanel, 0.2f, 0).EaseOutCubic().Then(() => {
                btnBattle.gameObject.SetActive(true);
            });
        }

        private void SetEnemyImg(BattleCharacter[] enemyList) {
            enemyInfoList = new List<CaptionCharacter.CharacterInfo>();
            foreach (var btn in btnEnemyCaptionList) {
                btn.gameObject.SetActive(false);
            }

            for(int i = 0, l = Mathf.Min(enemyList.Length, btnEnemyCaptionList.Length); i < l; i++) {
                var enemyIconData = new CharacterStatusIcon.CharacterData();
                var enemyInfo = new CaptionCharacter.CharacterInfo();
                var masterData = GlobalContainer.MasterManager.CharacterMaster.Get(enemyList[i].CharacterID);
                enemyInfo.Name = GlobalContainer.LocalizationManager.GetText(masterData.TextKey + ".Name");
                enemyInfo.Lv = enemyIconData.Level = enemyList[i].Level;
                enemyInfo.PromotionLevel = enemyIconData.PromotionLevel = enemyList[i].PromotionLevel;
                enemyInfo.ID = enemyIconData.CharacterID = enemyList[i].CharacterID;
                enemyInfo.IsBoss = enemyList[i].IsBoss;
//                enemyInfo.Description = enemyList[i].Description;
                enemyInfo.Index = i;
                enemyInfoList.Add(enemyInfo);
                enemyInfoList = enemyInfoList.OrderBy(c => c.Index).ToList();

                enemyIconData.EvolutionLevel = enemyList[i].EvolutionLevel;
                enemyIconData.IsDeca = new Noroshi.Core.Game.Character.CharacterTagSet(masterData.TagFlags).IsDeca;

                enemyIconList[i].SetInfo(enemyIconData);
                btnEnemyCaptionList[i].gameObject.SetActive(true);
            }
        }

        private void SetItemImg(PossessionObject[] itemList) {
            var gclm = GlobalContainer.LocalizationManager;
            itemInfoList = new List<CaptionItem.ItemInfo>();
            foreach (var btn in itemIconList) {
                btn.gameObject.SetActive(false);
            }
            if(itemList == null || itemList.Length < 1) {return;}
            for(int i = 0, l = Mathf.Min(itemList.Length, itemIconList.Length); i < l; i++) {
                var itemInfo = new CaptionItem.ItemInfo();
                var rarity = ItemListManager.Instance.GetItemRarity(itemList[i]);
                itemIconList[i].SetItemIcon(itemList[i]);
                itemIconList[i].gameObject.SetActive(true);

                itemInfo.Name = gclm.GetText(itemList[i].TextKey + ".Name");
                itemInfo.Description = gclm.GetText(itemList[i].TextKey + ".Description").TrimStart();
                itemInfo.HaveNum = itemList[i].PossessingNum;
//                itemInfo.NeedLevel = gearData != null ? (uint)gearData.Level : 1;
//                itemInfo.Price = itemData.Price;
                itemInfo.ID = itemList[i].ID;
                itemInfo.Rarity = rarity;
                itemInfo.Index = i;
                itemInfo.Category = itemList[i].Category;
                itemInfoList.Add(itemInfo);
                itemInfoList = itemInfoList.OrderBy(c => c.Index).ToList();
            }
        }

        private CharacterStatusIcon.CharacterData SetCharacterStatus(CharacterStatus characterStatus) {
            var characterData = new CharacterStatusIcon.CharacterData();
            
            characterData.CharacterID = characterStatus.CharacterID;
            characterData.Level = characterStatus.Level;
            characterData.EvolutionLevel = characterStatus.EvolutionLevel;
            characterData.PromotionLevel = characterStatus.PromotionLevel;
            characterData.IsDeca = characterStatus.TagSet.IsDeca;
            return characterData;
        }

        private void UpdateRemainBattle(uint playCount) {
            var remainNum = Noroshi.Core.Game.Story.Constant.MAX_BACK_STORY_STAGE_BATTLE_NUM - playCount;
            stageData.PlayCount = playCount;
            txtRemainChance.text = remainNum.ToString();
            quickBattleController.SetQuickBattle(stageData);
        }

        public void SetStageInfo(StageData data) {
            var remainNum = Noroshi.Core.Game.Story.Constant.MAX_BACK_STORY_STAGE_BATTLE_NUM - data.PlayCount;
            txtTitle.text = data.Name.ToString();
            txtChapterIndex.text = (data.ChapterIndex + 1).ToString();
            txtEpisodeIndex.text = data.TotalEpisodeNo.ToString();
            txtStageIndex.text = (data.StageIndex + 1).ToString();
            txtStamina.text = data.Stamina.ToString();
            stageData = data;
            for(int i = 0; i < scoreStarList.Length; i++) {
                if(i < data.Rank) {
                    scoreStarList[i].sprite = starSpriteOn;
                } else {
                    scoreStarList[i].sprite = starSpriteOff;
                }
            }
            if(data.Rank == Noroshi.Core.Game.Story.Constant.STORY_STAGE_RANK_OF_MASTER
                && PlayerInfo.Instance.GetPlayerStatus().VipLevel > 0) {
                quickBattleController.SetQuickBattle(data);
                btnOpenQuickBattle.SetEnable(true);
                btnOpenQuickBattle.gameObject.SetActive(true);
                btnAlertNotOpenQuickBattle.gameObject.SetActive(false);
            } else {
                btnOpenQuickBattle.gameObject.SetActive(false);
                btnAlertNotOpenQuickBattle.gameObject.SetActive(true);
            }
            if(data.Type == StageType.BackStoryStage) {
                txtRemainChance.text = remainNum.ToString();
                remainContainer.SetActive(true);
                if(remainNum <= 0) {
                    quickBattleController.CloseQuickBattle();
                }
            } else {
                remainContainer.SetActive(false);
            }
            SetEnemyImg(data.EnemyList);
            SetItemImg(data.ItemList);
            OpenStageInfo();
        }

        public void OpenStageInfo() {
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0).EaseOutCubic();
            TweenA.Add(stageInfoPanel, 0.2f, 1).Delay(0.5f).From(0).EaseOutCubic();
            TweenY.Add(stageInfoPanel, 0.2f, 0).Delay(0.5f).From(-30).EaseOutCubic();
            BackButtonController.Instance.IsModalChildOpen(true);
        }

        public void CloseStageInfo() {
            TweenA.Add(stageInfoPanel, 0.2f, 0).EaseOutCubic();
            TweenY.Add(stageInfoPanel, 0.2f, -40).EaseOutCubic();
            TweenA.Add(gameObject, 0.25f, 0).EaseOutCubic().Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalChildOpen(false);
            });
        }
    }
}
