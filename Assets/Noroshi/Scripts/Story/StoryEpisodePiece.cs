﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class StoryEpisodePiece : MonoBehaviour {
        [SerializeField] Text txtEpisodeTitle;
        [SerializeField] Text txtEpisodeNumber;
        [SerializeField] BtnCommon btnEpisode;
        [SerializeField] GameObject normalAchievementWrapper;
        [SerializeField] GameObject eliteAchievementWrapper;
        [SerializeField] Text txtCurrentNormalAchievement;
        [SerializeField] Text txtMaxNormalAchievement;
        [SerializeField] Text txtCurrentEliteAchievement;
        [SerializeField] Text txtMaxEliteAchievement;

        public Subject<int> OnSelectEpisode = new Subject<int>();

        private void Start() {
            btnEpisode.OnClickedBtn.Subscribe(index => {
                OnSelectEpisode.OnNext(index);
            });
            btnEpisode.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        public void SetEpisodePiece(StoryController.EpisodeData data) {
            txtEpisodeTitle.text = data.Name;
            txtEpisodeNumber.text = data.TotalNo.ToString();
            btnEpisode.id = data.Index;
            normalAchievementWrapper.SetActive(false);
            eliteAchievementWrapper.SetActive(false);
        }
    }
}
