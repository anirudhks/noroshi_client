﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.WebApi.Response.Story;

namespace Noroshi.UI {
    public class StoryChapterPanel : MonoBehaviour {
        [SerializeField] Text txtTitle;
        [SerializeField] Text txtChapterNumber;
        [SerializeField] Text txtDescription;
        [SerializeField] GameObject episodeWrapper;
        [SerializeField] StoryEpisodePiece episodePiecePref;
        [SerializeField] GameObject noOpenAlert;
        [SerializeField] Text txtNecessaryPlayerLevel;

        public Subject<int> OnSelectEpisode = new Subject<int>();

        private StoryController.ChapterData chapterData;

        public void SetChapterPanelInfo(StoryController.ChapterData data) {
            chapterData = data;
            txtTitle.text = data.Name;
            txtChapterNumber.text = (data.Index + 1).ToString();
            txtDescription.text = data.Description;
            for(int i = 0, l = data.EpisodeDataList.Length; i < l; i++) {
                if(i < data.OpenNormalEpisodeLength) {
                    var piece = Instantiate(episodePiecePref);
                    piece.transform.SetParent(episodeWrapper.transform);
                    piece.transform.localScale = Vector3.one;
                    piece.SetEpisodePiece(data.EpisodeDataList[i]);
                    piece.OnSelectEpisode.Subscribe(index => {
                        OnSelectEpisode.OnNext(index);
                    });
                }
            }
            if(!data.IsOpen) {
                txtNecessaryPlayerLevel.text = data.NecessaryPlayerLevel.ToString();
                noOpenAlert.SetActive(true);
            }
        }
    }
}
