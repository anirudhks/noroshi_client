﻿using UniRx;
using Noroshi.Core.Game.Story;
using Noroshi.Core.WebApi.Response;
using Noroshi.Core.WebApi.Request.Story;
using Noroshi.Core.WebApi.Response.Story;

namespace Noroshi.Story
{
    public class WebApiRequester
    {
        public static IObservable<ChapterListResponse> ChapterList()
        {
            return _getWebApiRequester().Get<ChapterListResponse>("Story/ChapterList");
        }
		public static IObservable<EpisodeResponse> Episode(uint episodeId, StoryCategory storyCategory)
        {
            var request = new EpisodeRequest
            {
                EpisodeID = episodeId,
				Category = (byte)storyCategory,
            };
            return _getWebApiRequester().Get<EpisodeRequest, EpisodeResponse>("Story/Episode", request);
        }
        class EpisodeRequest
        {
            public uint EpisodeID { get; set; }
            public byte Category { get; set; }
        }

        public static IObservable<ResetBattleNumResponse> ResetBattleNum(uint stageId)
        {
            var requestParams = new ResetBattleNumRequest()
            {
                StageID = stageId,
            };
            return _getWebApiRequester().Post<ResetBattleNumRequest, ResetBattleNumResponse>("Story/ResetBattleNum", requestParams);
        }
        class ResetBattleNumRequest
        {
            public uint StageID { get; set; }
        }

        public static IObservable<ExecuteQuickBattleResponse> StartQuickBattle(uint stageId, byte count)
        {
            var requestParams = new QuickBattleRequest()
            {
                StageID = stageId,
                Count = count,
            };
            return _getWebApiRequester().Post<QuickBattleRequest, ExecuteQuickBattleResponse>("Story/StartQuickBattle", requestParams);
        }


        static WebApi.WebApiRequester _getWebApiRequester()
        {
            return new WebApi.WebApiRequester();
        }
    }
}
