﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UniRx;
using Noroshi.Core.WebApi.Response.Story;

namespace Noroshi.UI {
    public class StoryMap : MonoBehaviour {
        [SerializeField] GameObject normalWorld;
        [SerializeField] GameObject eliteWorld;
        [SerializeField] GameObject switchBtnWrapper;
        [SerializeField] BtnCommon btnBackToPartSelect;
        [SerializeField] BtnCommon btnNormal;
        [SerializeField] BtnCommon btnElite;
        [SerializeField] BtnCommon btnLeft;
        [SerializeField] BtnCommon btnRight;
        [SerializeField] GameObject transitionOverlay;
        [SerializeField] Text txtEpisodeIndex;
        [SerializeField] Text txtEpisodeTitle;
        [SerializeField] Text txtCurrentEpisodeIndex;
        [SerializeField] Text txtTotalEpisodeIndex;
        [SerializeField] GameObject normalArchivementWrapper;
        [SerializeField] GameObject eliteArchivementWrapper;
        [SerializeField] ClearEpisodePanel clearEpisodePanel;
        [SerializeField] GameObject cornerPoint;
        [SerializeField] GameObject processing;

        public Subject<int> OnLoadNormalEpisode = new Subject<int>();
        public Subject<int> OnLoadEliteEpisode = new Subject<int>();
        public Subject<bool> OnOpenMap = new Subject<bool>();
        public Subject<bool> OnCloseMap = new Subject<bool>();
        public Subject<StoryStageInfo.StageData> OnSelectStage = new Subject<StoryStageInfo.StageData>();

        private Dictionary<int, StoryEpisodePanel> normalEpisodeList = new Dictionary<int, StoryEpisodePanel>();
        private Dictionary<int, StoryEpisodePanel> eliteEpisodeList = new Dictionary<int, StoryEpisodePanel>();
        private StoryController.ChapterData chapterData;
        private bool isNormalWorld = true;
        private int normalWorldIndex = 0;
        private int eliteWorldIndex = 0;
        private bool isOpen = false;

        private Vector2 leftCornerPoint = new Vector2(-568, -284);
        private Vector2 rightCornerPoint = new Vector2(568, -284);
        private float pageWidth = 568;
        private float pageHeight = 568;
        private float wrapperWidth;
        private GameObject wrapper;
        private GameObject leftPage;
        private GameObject rightPage;
        private bool isFlipping = false;


        private void Start() {
            btnBackToPartSelect.OnClickedBtn.Subscribe(_ => {
                if(UILoading.Instance.GetQuery(QueryKeys.FromCharacterList) > 0) {
                    UILoading.Instance.LoadScene(Constant.SCENE_CHARACTER_LIST);
                } else {
                    TweenX.Add(transitionOverlay, 0.5f, 0).EaseOutCubic()
                        .From(-Constant.SCREEN_BASE_WIDTH * 1.5f).Then(() => {
                        OnCloseMap.OnNext(true);
                        CloseMap();
                    });
                }
            });
            btnBackToPartSelect.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.FLICK);
            });

            btnLeft.OnClickedBtn.Subscribe(_ => {
                if(isFlipping) {return;}
                MoveToPrev();
            });
            btnLeft.OnPlaySE.Subscribe(_ => {
                if(isFlipping) {return;}
                SoundController.Instance.PlaySE(SoundController.SEKeys.FLICK);
            });

            btnRight.OnClickedBtn.Subscribe(_ => {
                if(isFlipping) {return;}
                MoveToNext();
            });
            btnRight.OnPlaySE.Subscribe(_ => {
                if(isFlipping) {return;}
                SoundController.Instance.PlaySE(SoundController.SEKeys.FLICK);
            });

            btnNormal.OnClickedBtn.Subscribe(index => {
                if(isNormalWorld) {return;}
                SwitchWorld(index, false);
            });
            btnNormal.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.FLICK);
            });
            btnElite.OnClickedBtn.Subscribe(index => {
                if(!isNormalWorld) {return;}
                SwitchWorld(index, false);
            });
            btnElite.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.FLICK);
            });

            clearEpisodePanel.OnCloseClearEpisodePanel.Subscribe(_ => {
                var index = isNormalWorld ? normalWorldIndex : eliteWorldIndex;
                var openLength = isNormalWorld ? chapterData.OpenNormalEpisodeLength : chapterData.OpenEliteEpisodeLength;
                if(index < openLength - 1) {
                    MoveToNext();
                    SoundController.Instance.PlaySE(SoundController.SEKeys.FLICK);
                }
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                if(UILoading.Instance.GetQuery(QueryKeys.FromCharacterList) > 0) {
                    UILoading.Instance.LoadScene(Constant.SCENE_CHARACTER_LIST);
                } else {
                    TweenX.Add(transitionOverlay, 0.5f, 0).EaseOutCubic()
                        .From(-Constant.SCREEN_BASE_WIDTH * 1.5f).Then(() => {
                            OnCloseMap.OnNext(true);
                            CloseMap();
                        });
                }
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            wrapperWidth = pageWidth * Mathf.Sqrt(2);
        }

        private void InitMap() {
            if(isNormalWorld) {
                SwitchWorld(0, true);
            } else {
                SwitchWorld(1, true);
            }
            switchBtnWrapper.SetActive(chapterData.OpenEliteEpisodeLength > 0);
        }

        private void MoveToNext() {
            var n = isNormalWorld ? normalWorldIndex : eliteWorldIndex;
            isFlipping = true;
            if(isNormalWorld) {
                normalWorldIndex++;
                if(normalEpisodeList.ContainsKey(normalWorldIndex)) {
                    SetFlipMap(normalEpisodeList[normalWorldIndex]);
                } else {
                    CreateMap();
                    SetFlipMap(normalEpisodeList[normalWorldIndex]);
                }
                normalEpisodeList[n].HideMapObject();
            } else {
                eliteWorldIndex++;
                if(eliteEpisodeList.ContainsKey(eliteWorldIndex)) {
                    SetFlipMap(eliteEpisodeList[eliteWorldIndex]);
                } else {
                    CreateMap();
                    SetFlipMap(eliteEpisodeList[eliteWorldIndex]);
                }
                eliteEpisodeList[n].HideMapObject();
            }
            leftPage.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
            rightPage.GetComponent<RectTransform>().pivot = new Vector2(0, 0.5f);
            cornerPoint.transform.localPosition = rightCornerPoint;
            cornerPoint.transform.DOLocalMove(new Vector2(0, -75), 0.13f).OnUpdate(() => {
                MoveCornerToLeft();
            }).SetEase(Ease.Linear).OnComplete(() => {
                cornerPoint.transform.DOLocalMove(leftCornerPoint, 0.62f).OnUpdate(() => {
                    MoveCornerToLeft();
                }).SetEase(Ease.OutCubic).OnComplete(() => {
                    isFlipping = false;
                    if(isNormalWorld) {
                        normalEpisodeList[n].gameObject.SetActive(false);
                        MovedToNormalWorld();
                    } else {
                        eliteEpisodeList[n].gameObject.SetActive(false);
                        MovedToEliteWorld();
                    }
                });
            });
        }

        private void MoveToPrev() {
            var n = isNormalWorld ? normalWorldIndex : eliteWorldIndex;
            isFlipping = true;
            if(isNormalWorld) {
                normalWorldIndex--;
                if(normalEpisodeList.ContainsKey(normalWorldIndex)) {
                    SetFlipMap(normalEpisodeList[normalWorldIndex]);
                } else {
                    CreateMap();
                    SetFlipMap(normalEpisodeList[normalWorldIndex]);
                }
                normalEpisodeList[n].HideMapObject();
            } else {
                eliteWorldIndex--;
                if(eliteEpisodeList.ContainsKey(eliteWorldIndex)) {
                    SetFlipMap(eliteEpisodeList[eliteWorldIndex]);
                } else {
                    CreateMap();
                    SetFlipMap(eliteEpisodeList[eliteWorldIndex]);
                }
                eliteEpisodeList[n].HideMapObject();
            }
            leftPage.GetComponent<RectTransform>().pivot = new Vector2(1, 0.5f);
            rightPage.GetComponent<RectTransform>().pivot = new Vector2(1, 0);
            cornerPoint.transform.localPosition = leftCornerPoint;
            cornerPoint.transform.DOLocalMove(new Vector2(0, -75), 0.13f).OnUpdate(() => {
                MoveCornerToRight();
            }).SetEase(Ease.Linear).OnComplete(() => {
                cornerPoint.transform.DOLocalMove(rightCornerPoint, 0.62f).OnUpdate(() => {
                    MoveCornerToRight();
                }).SetEase(Ease.OutCubic).OnComplete(() => {
                    isFlipping = false;
                    if(isNormalWorld) {
                        normalEpisodeList[n].gameObject.SetActive(false);
                        MovedToNormalWorld();
                    } else {
                        eliteEpisodeList[n].gameObject.SetActive(false);
                        MovedToEliteWorld();
                    }
                });
            });
        }

        private void SetFlipMap(StoryEpisodePanel storyEpisode) {
            btnLeft.gameObject.SetActive(false);
            btnRight.gameObject.SetActive(false);
            wrapper = storyEpisode.PageWrapper;
            leftPage = storyEpisode.PageLeft;
            rightPage = storyEpisode.PageRight;
            storyEpisode.gameObject.SetActive(false);
            storyEpisode.transform.SetAsLastSibling();
            TweenNull.Add(gameObject, 0.05f).Then(() => {
                storyEpisode.gameObject.SetActive(true);
            });
        }

        private void MoveCornerToLeft() {
            var midX = (rightCornerPoint.x - cornerPoint.transform.localPosition.x) / 2;
            var midY = (cornerPoint.transform.localPosition.y - rightCornerPoint.y) / 2;
            var rad = Mathf.Atan2(midY, midX);
            var deg = Mathf.Rad2Deg * rad;
            var curlLength = midY * Mathf.Cos(rad) / Mathf.Sin(rad)
                + midY * Mathf.Sin(rad) / Mathf.Cos(rad);

            if(curlLength > pageWidth) curlLength = pageWidth;
            if(curlLength < 0) curlLength = 0;

            FlipToNext(cornerPoint.transform.localPosition.x, cornerPoint.transform.localPosition.y, rad, deg, curlLength);
        }

        private void MoveCornerToRight() {
            var midX = (cornerPoint.transform.localPosition.x - leftCornerPoint.x) / 2;
            var midY = (cornerPoint.transform.localPosition.y - leftCornerPoint.y) / 2;
            var rad = Mathf.Atan2(midY, midX);
            var deg = Mathf.Rad2Deg * rad;
            var curlLength = midY * Mathf.Cos(rad) / Mathf.Sin(rad)
                + midY * Mathf.Sin(rad) / Mathf.Cos(rad);

            if(curlLength > pageWidth) curlLength = pageWidth;
            if(curlLength < 0) curlLength = 0;

            FlipToPrev(cornerPoint.transform.localPosition.x, cornerPoint.transform.localPosition.y, rad, deg, curlLength);
        }

        private void FlipToNext(float pointX, float pointY, float rad, float deg, float curlLength) {
            if(deg == 0) {
                return;
            } else if(deg > 0 && deg <= 90) {
                wrapper.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -deg + 90));
                wrapper.transform.localPosition = new Vector2(
                    (pageWidth - curlLength) * (rightCornerPoint.x - pointX) / 2 / curlLength,
                    (pageWidth - curlLength) * (rightCornerPoint.y - pointY) / 2 / curlLength - pageHeight / 2
                );

                rightPage.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, deg - 90));
                rightPage.transform.position = new Vector2(0.0f, 0.0f);

                leftPage.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -deg - 90));
                leftPage.transform.localPosition = new Vector2(
                    pageWidth * Mathf.Sin(rad),
                    (wrapperWidth - curlLength * -Mathf.Cos(rad)) - wrapperWidth
                );
            }
        }

        private void FlipToPrev(float pointX, float pointY, float rad, float deg, float curlLength) {
            if(deg == 0) {
                return;
            } else if(deg > 0 && deg <= 90) {
                wrapper.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, deg + 90));
                wrapper.transform.localPosition = new Vector2(
                    (pageWidth - curlLength) * (leftCornerPoint.x - pointX) / 2 / curlLength,
                    (pageWidth - curlLength) * (leftCornerPoint.y - pointY) / 2 / curlLength - pageHeight / 2
                );

                leftPage.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -deg - 90));
                leftPage.transform.position = new Vector2(0.0f, 0.0f);

                rightPage.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, deg - 90));
                rightPage.transform.localPosition = new Vector2(
                    pageWidth * Mathf.Sin(rad),
                    (wrapperWidth - curlLength * Mathf.Cos(rad))
                );
            }
        }


        private void SwitchWorld(int index, bool isInit) {
            var isNormal = index == 0 ? true : false;
            var duration = 0.0f;

            isNormalWorld = isNormal;
            btnNormal.SetSelect(isNormal);
            btnElite.SetSelect(!isNormal);
            btnLeft.gameObject.SetActive(false);
            btnRight.gameObject.SetActive(false);
            if(isInit) {
                duration = 0.01f;
                transitionOverlay.transform.localPosition = Vector2.zero;
            } else {
                duration = 0.5f;
                transitionOverlay.transform.localPosition = new Vector2(Constant.SCREEN_BASE_WIDTH * 1.5f, 0);
            }
            TweenX.Add(transitionOverlay, duration, 0).EaseOutCubic().Then(() => {
                normalWorld.SetActive(isNormal);
                eliteWorld.SetActive(!isNormal);
//                    normalArchivementWrapper.SetActive(isNormal);
//                    eliteArchivementWrapper.SetActive(!isNormal);
                if(isNormal) {
                    MovedToNormalWorld();
                } else {
                    MovedToEliteWorld();
                }
                TweenX.Add(transitionOverlay, 0.5f, -Constant.SCREEN_BASE_WIDTH * 1.5f).EaseOutQuart();
            });
            if((isNormal && !normalEpisodeList.ContainsKey(normalWorldIndex))
               || (!isNormal && !eliteEpisodeList.ContainsKey(eliteWorldIndex))) {
                CreateMap();
            }
        }

        private void MovedToNormalWorld() {
            if(normalWorldIndex <= 0) {
                normalWorldIndex = 0;
            } else {
                btnLeft.gameObject.SetActive(true);
            }
            if(normalWorldIndex >= chapterData.OpenNormalEpisodeLength - 1) {
                normalWorldIndex = chapterData.OpenNormalEpisodeLength - 1;
            } else {
                btnRight.gameObject.SetActive(true);
            }

            txtEpisodeIndex.text = chapterData.EpisodeDataList[normalWorldIndex].TotalNo.ToString();
            txtEpisodeTitle.text = chapterData.EpisodeDataList[normalWorldIndex].Name;
            txtCurrentEpisodeIndex.text = (normalWorldIndex + 1).ToString();
            txtTotalEpisodeIndex.text = chapterData.EpisodeDataList.Length.ToString();

            if(chapterData.EpisodeDataList[normalWorldIndex].NormalStageDataList == null) {
                processing.SetActive(true);
                OnLoadNormalEpisode.OnNext(normalWorldIndex);
            } else {
                normalEpisodeList[normalWorldIndex].ShowMapObject();
            }
        }

        private void MovedToEliteWorld() {
            if(eliteWorldIndex <= 0) {
                eliteWorldIndex = 0;
            } else {
                btnLeft.gameObject.SetActive(true);
            }
            if(eliteWorldIndex >= chapterData.OpenEliteEpisodeLength - 1) {
                eliteWorldIndex = chapterData.OpenEliteEpisodeLength - 1;
            } else {
                btnRight.gameObject.SetActive(true);
            }

            txtEpisodeIndex.text = chapterData.EpisodeDataList[eliteWorldIndex].TotalNo.ToString();
            txtEpisodeTitle.text = chapterData.EpisodeDataList[eliteWorldIndex].Name;
            txtCurrentEpisodeIndex.text = (eliteWorldIndex + 1).ToString();
            txtTotalEpisodeIndex.text = chapterData.EpisodeDataList.Length.ToString();

            if(chapterData.EpisodeDataList[eliteWorldIndex].EliteStageDataList == null) {
                processing.SetActive(true);
                OnLoadEliteEpisode.OnNext(eliteWorldIndex);
            } else {
                eliteEpisodeList[eliteWorldIndex].ShowMapObject();
            }
        }

        private void ShowClearEpisodeEffect() {
            var index = isNormalWorld ? normalWorldIndex : eliteWorldIndex;
            clearEpisodePanel.OpenClearEpisodePanel(chapterData.EpisodeDataList, index);
        }

        public void InitEpisodeMap(int index, bool isNormal) {
            var storyEpisode = isNormal ?
                normalEpisodeList[index] : eliteEpisodeList[index];
            var stageDataList = isNormal ?
                chapterData.EpisodeDataList[index].NormalStageDataList :
                chapterData.EpisodeDataList[index].EliteStageDataList;
            storyEpisode.OnStageClicked.Subscribe(stageData => {
                OnSelectStage.OnNext(stageData);
            }).AddTo(storyEpisode);
            storyEpisode.OnEpisodeClear.Subscribe(_ => {
                ShowClearEpisodeEffect();
            }).AddTo(storyEpisode);
            storyEpisode.SetEpisodeMap(chapterData.SelectedStageID, stageDataList);
            processing.SetActive(false);
            if(isNormal) {
                normalEpisodeList[normalWorldIndex].ShowMapObject();
            } else {
                eliteEpisodeList[eliteWorldIndex].ShowMapObject();
            }
        }

        public void CreateMap() {
            if(isNormalWorld) {
                var currentNormalEpisodeID = chapterData.EpisodeDataList[normalWorldIndex].ID;
                var normalMap = Instantiate(Resources.Load(
                    string.Format("Story/Normal/Normal{0}", currentNormalEpisodeID)
                )) as GameObject;
                normalMap.transform.SetParent(normalWorld.transform);
                normalMap.transform.localScale = Vector3.one;
                normalMap.transform.localPosition = Vector3.zero;
                normalEpisodeList[normalWorldIndex] = normalMap.GetComponent<StoryEpisodePanel>();
                if(chapterData.EpisodeDataList[normalWorldIndex].NormalStageDataList != null) {
                    InitEpisodeMap(normalWorldIndex, true);
                    normalEpisodeList[normalWorldIndex].HideMapObject();
                }
            } else {
                var currentEliteEpisodeID = chapterData.EpisodeDataList[eliteWorldIndex].ID;
                var eliteMap = Instantiate(Resources.Load(
                    string.Format("Story/Elite/Elite{0}", currentEliteEpisodeID)
                )) as GameObject;
                eliteMap.transform.SetParent(eliteWorld.transform);
                eliteMap.transform.localScale = Vector3.one;
                eliteMap.transform.localPosition = Vector3.zero;
                eliteEpisodeList[eliteWorldIndex] = eliteMap.GetComponent<StoryEpisodePanel>();
                if(chapterData.EpisodeDataList[eliteWorldIndex].EliteStageDataList != null) {
                    InitEpisodeMap(eliteWorldIndex, false);
                    eliteEpisodeList[eliteWorldIndex].HideMapObject();
                }
            }
        }

        public void HideMapObject() {
            if(isNormalWorld) {
                normalEpisodeList[normalWorldIndex].HideMapObject();
            } else {
                eliteEpisodeList[eliteWorldIndex].HideMapObject();
            }
        }

        public void ShowMapObject() {
            if(isNormalWorld) {
                normalEpisodeList[normalWorldIndex].ShowMapObject();
            } else {
                eliteEpisodeList[eliteWorldIndex].ShowMapObject();
            }
        }

        public void OpenMap(StoryController.ChapterData data, bool isNormal) {
            chapterData = data;
            isNormalWorld = isNormal;
            if(isNormal) {
                normalWorldIndex = data.SelectedEpisodeIndex;
                eliteWorldIndex = data.OpenEliteEpisodeLength - 1;
            } else {
                normalWorldIndex = data.OpenNormalEpisodeLength - 1;
                eliteWorldIndex = data.SelectedEpisodeIndex;
            }
            CreateMap();
            InitMap();
            OnOpenMap.OnNext(true);
            isOpen = true;
            gameObject.SetActive(true);
            BackButtonController.Instance.IsModalOpen(true);
        }

        public void CloseMap() {
            foreach(var episode in normalEpisodeList) {
                Destroy(episode.Value.gameObject);
            }
            foreach(var episode in eliteEpisodeList) {
                Destroy(episode.Value.gameObject);
            }
            normalEpisodeList = new Dictionary<int, StoryEpisodePanel>();
            eliteEpisodeList = new Dictionary<int, StoryEpisodePanel>();
            isOpen = false;
            gameObject.SetActive(false);
            BackButtonController.Instance.IsModalOpen(false);
        }
    }
}
