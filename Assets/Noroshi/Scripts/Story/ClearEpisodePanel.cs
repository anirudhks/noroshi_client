﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class ClearEpisodePanel : MonoBehaviour {
        [SerializeField] BtnCommon btnClosePanel;
        [SerializeField] Text txtClearEpisodeIndex;
        [SerializeField] Text txtClearEpisodeTitle;
        [SerializeField] Text txtNextEpisodeIndex;
        [SerializeField] Text txtNextEpisodeTitle;
        [SerializeField] GameObject openedWrapper;
        [SerializeField] GameObject continuedText;
        [SerializeField] GameObject clearEffect;

        public Subject<bool> OnCloseClearEpisodePanel = new Subject<bool>();

        private void Start() {
            btnClosePanel.OnClickedBtn.Subscribe(_ => {
                CloseClearEpisodePanel();
            });
            btnClosePanel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        private void CloseClearEpisodePanel() {
            clearEffect.SetActive(false);
            TweenA.Add(gameObject, 1.5f, 0).EaseOutCubic().Then(() => {
                gameObject.SetActive(false);
                OnCloseClearEpisodePanel.OnNext(true);
            });
        }

        public void OpenClearEpisodePanel(StoryController.EpisodeData[] episodeDataList, int index) {
            txtClearEpisodeIndex.text = (index + 1).ToString();
            txtClearEpisodeTitle.text = episodeDataList[index].Name;
            if(index < episodeDataList.Length - 1) {
                txtNextEpisodeIndex.text = (index + 2).ToString();
                txtNextEpisodeTitle.text = episodeDataList[index + 1].Name;
            } else {
                openedWrapper.SetActive(false);
                continuedText.SetActive(true);
            }
            SoundController.Instance.PlaySE(SoundController.SEKeys.EVOLUTION);
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
        }
    }
}
