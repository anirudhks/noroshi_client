﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Noroshi.Core.WebApi.Response.Story;
using UniRx;

namespace Noroshi.UI {
    public class StorySelect : MonoBehaviour {
        [SerializeField] CarouselController carouselController;
        [SerializeField] GameObject chapterWrapper;
        [SerializeField] StoryChapterPanel chapterPanelPref;
        [SerializeField] GameObject chapterBgWrapper;
        [SerializeField] GameObject transitionOverlay;

        public Subject<Dictionary<string, int>> OnSelectStory = new Subject<Dictionary<string, int>>();
        public Subject<bool> OnCreatePanel = new Subject<bool>();
        public Subject<bool> OnCloseStorySelect = new Subject<bool>();

        private List<StoryChapterPanel> chapterPanelList = new List<StoryChapterPanel>();
        private List<StoryController.ChapterData> chapterDataList = new List<StoryController.ChapterData>();
        private List<GameObject> bgList = new List<GameObject>();
        private int selectedChapterIndex = 0;

        private void Start() {
            carouselController.OnMoveEnd.Subscribe(index => {
                var n = selectedChapterIndex;
                selectedChapterIndex = index;
                if(n != selectedChapterIndex) {
                    TweenA.Add(bgList[n], 0.4f, 0).Then(() => {bgList[n].SetActive(false);});
                    bgList[selectedChapterIndex].SetActive(true);
                    TweenA.Add(bgList[selectedChapterIndex], 0.4f, 1);
                }
                for(int i = 0, l = chapterPanelList.Count; i < l; i++) {
                    if(selectedChapterIndex == 0) {
                        if(i == selectedChapterIndex || i == selectedChapterIndex + 1) {
                            chapterPanelList[i].gameObject.SetActive(true);
                        } else {
                            chapterPanelList[i].gameObject.SetActive(false);
                        }
                    } else if(selectedChapterIndex == l - 1) {
                        if(i == selectedChapterIndex || i == selectedChapterIndex - 1) {
                            chapterPanelList[i].gameObject.SetActive(true);
                        } else {
                            chapterPanelList[i].gameObject.SetActive(false);
                        }
                    } else {
                        if(i == selectedChapterIndex || i == selectedChapterIndex + 1 || i == selectedChapterIndex - 1) {
                            chapterPanelList[i].gameObject.SetActive(true);
                        } else {
                            chapterPanelList[i].gameObject.SetActive(false);
                        }
                    }
                }
            });
        }

        public void CreateChapterPanel(StoryController.ChapterData data) {
            var chapterPanel = Instantiate(chapterPanelPref);
            var bg = Instantiate(
                Resources.Load("Story/ChapterBG/BgChapter" + (data.Index + 1))
            ) as GameObject;

            chapterDataList.Add(data);
            chapterPanel.transform.SetParent(chapterWrapper.transform);
            chapterPanel.transform.localScale = Vector3.one;
            chapterPanel.SetChapterPanelInfo(data);
            chapterPanel.OnSelectEpisode.Subscribe(episodeIndex => {
                var dic = new Dictionary<string, int>() {
                    {"Chapter", data.Index},
                    {"Episode", episodeIndex}
                };
                TweenX.Add(transitionOverlay, 0.5f, 0).EaseOutCubic()
                    .From(Constant.SCREEN_BASE_WIDTH * 1.5f).Then(() => {
                    OnSelectStory.OnNext(dic);
                });
            });
            chapterPanelList.Add(chapterPanel);
            bg.transform.SetParent(chapterBgWrapper.transform);
            bg.transform.localScale = Vector3.one;
            bgList.Add(bg);
            OnCreatePanel.OnNext(true);
        }

        public void SetDefaultChapterPanel(uint chapterID) {
            for(int i = 0, l = chapterDataList.Count; i < l; i++) {
                if(chapterDataList[i].ID == chapterID) {
                    selectedChapterIndex = i;
                    bgList[i].SetActive(true);
                } else {
                    bgList[i].SetActive(false);
                }
            }
            carouselController.listNum = chapterDataList.Count;
            carouselController.Init(selectedChapterIndex);
        }

        public void OpenStorySelect() {
            gameObject.SetActive(true);
            transitionOverlay.transform.localPosition = Vector2.zero;
            TweenX.Add(transitionOverlay, 0.5f, Constant.SCREEN_BASE_WIDTH * 1.5f).EaseOutQuart();
        }

        public void CloseStorySelect() {
            gameObject.SetActive(false);
            OnCloseStorySelect.OnNext(true);
        }
    }
}
