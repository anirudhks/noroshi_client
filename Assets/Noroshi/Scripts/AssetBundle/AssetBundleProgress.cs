﻿using UnityEngine;
using System.Collections;
using Noroshi.AssetBundle;
using Noroshi.Core.Game.AssetBundle;
using Noroshi.UI;

namespace Noroshi.AssetBundle
{
    // 読み込むタイミングで演出の違いが想定されるのでFactoryデザインで実装する.
    public class AssetBundleProgress : MonoBehaviour
    {
        AbstractAssetBundleProgress assetBundleProgress;

        public AssetBundleProgress Build (AssetBundleManager.UIType uiType)
        {
            var canvas = GameObject.Find ("Canvas").gameObject;
            GameObject view;
            switch (uiType)
            {
            case AssetBundleManager.UIType.Normal:
                view = Instantiate<GameObject> (Resources.Load<GameObject>("UI/LoadingAssetNormalOverlay"));
                view.transform.SetParent (canvas.transform, false);
                assetBundleProgress = view.gameObject.GetComponent<AssetBundleProgressNormal>();
                break;
            case AssetBundleManager.UIType.Tips: 
                view = Instantiate<GameObject> (Resources.Load<GameObject>("UI/LoadingAssetTipsOverlay"));
                view.transform.SetParent (canvas.transform, false);
                assetBundleProgress = view.gameObject.GetComponent<AssetBundleProgressTip>();
                break;
            }

            if (assetBundleProgress != null) {
                assetBundleProgress.LoadStart(); // 初期化処理あれば実行.
            } else {
                Debug.LogError("Make Failure AssetBundle Progress Bar");
            }
            return this;
        }

        public void LoadProgress(float progressIN, uint assetBundleCompleteCntIn, uint assetBundleMaxValueCntIN)
        {
            assetBundleProgress.LoadProgress(progressIN, assetBundleCompleteCntIn, assetBundleMaxValueCntIN);
        }

        public void LoadEnd()
        {
            assetBundleProgress.LoadEnd();
            Destroy(this.gameObject); // AssetBundleManagerの中で作られた自分自身の処分.
        }

    }
}
