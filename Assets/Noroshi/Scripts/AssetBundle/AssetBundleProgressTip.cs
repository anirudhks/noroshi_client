﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Noroshi.AssetBundle;
using UniRx;

namespace Noroshi.AssetBundle
{
    public class AssetBundleProgressTip : AbstractAssetBundleProgress
    {
        [SerializeField] Text txtTitle;
        [SerializeField] Text txtDescription;
        [SerializeField] BtnCommon btnBG;
        // Tips雛形  AbstractAssetBundleProgressを継承してゲージを結び付ければ動作する.
        /*
        void OnGUI()
        {
            GUI.Label(new Rect(10, 10, 100, 100), string.Format("AssetundleTip Load {0}% {1}/{2}", System.Math.Floor(assetLoadProgress * 100).ToString().PadLeft(3,'0'),assetLoadCompleteCnt,assetLoadMaxValueCnt));
        }
        */
        float _timer;
        float _interval = 10.0f;

        void Start() {
            btnBG.OnClickedBtn.Subscribe(_ => {
                _timer = _interval;
                ShowTips();
            });
            _timer = _interval;
            ShowTips();
        }

        void Update() {
            _timer -= Time.deltaTime;
            if(_timer < 0) {
                _timer = _interval;
                ShowTips();
            }
        }

        void ShowTips() {
            var tips = GlobalContainer.MasterManager.HelpMaster.LotTip();
            txtTitle.text = GlobalContainer.LocalizationManager.GetText(tips.TextKey + ".Title");
            txtDescription.text = GlobalContainer.LocalizationManager.GetText(tips.TextKey + ".Body");
        }
    }
}
