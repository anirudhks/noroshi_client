﻿using UnityEngine;
using UniLinq;
using UniRx;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Collections;
using Noroshi.Core.Game.AssetBundle;
using Noroshi.Core.Game.Sound;
using Noroshi.UI;

using NoroshiAssetBundle =  Noroshi.Core.WebApi.Response.Master.AssetBundle;


namespace Noroshi.AssetBundle
{

    public class AssetBundleManager
    {
        string _platformManifest;
        AssetBundleManifest _assetBundleManifest = null;
        string[] _bundleVariants;

        private class AssetBundleRef
        {
            public UnityEngine.AssetBundle assetBundle = null;
            public int version;
            public string url;
            public AssetBundleRef(string strUrlIn, int intVersionIn, UnityEngine.AssetBundle assetBundleIn)
            {
                url = strUrlIn;
                version = intVersionIn;
                assetBundle = assetBundleIn;
            }
        }
        Dictionary<string, AssetBundleRef> _dictAssetBundleRefs = new Dictionary<string, AssetBundleRef>();

        AssetBundleProgress progressbar;

        uint assetBundleCompleteCnt = 1;
        uint assetBundleMaxValueCnt = 1;

        Group _trackedGroup = Group.Public;

        const string BUNDLE_LABEL_CHARACTER_BASE = "character/{0}";
        const string BUNDLE_LABEL_ITEM = "item";
        const string BUNDLE_LABEL_BGM = "bgm";
        const string BUNDLE_LABEL_SE = "se";
        const string BUNDLE_LABEL_VOICE = "voice";

        Dictionary<SoundType, string> _dictSoundTypeToBundleName = new Dictionary<SoundType, string>()
        {
            { SoundType.Bgm,  BUNDLE_LABEL_BGM},
            { SoundType.SE,  BUNDLE_LABEL_SE},
            { SoundType.Voice,  BUNDLE_LABEL_VOICE},
        };

        public AssetBundleManager()
        {
            AssetBundleSettings();
        }

        public void AssetBundleSettings()
        {
            #if !UNITY_EDITOR && UNITY_IOS
            // Unity5.3.2p2~4 で AssetBundle が iCloud のバックアップ対象に成っている.
            string dirPath = Application.temporaryCachePath + "/../UnityCache";
            if (System.IO.Directory.Exists(dirPath)) {
               UnityEngine.iOS.Device.SetNoBackupFlag(dirPath);
            }
            #endif
        }

        public enum UIType
        {
            Normal,
            Tips
        }

        UnityAction<float>_progressAction()
        {
            return delegate (float prog)
            {
                // 記載した処理を毎フレームコールする.
                progressbar.LoadProgress(prog,assetBundleCompleteCnt,assetBundleMaxValueCnt);
                //Debug.Log(string.Format("progress {0}% {1}/{2}", (prog * 100), assetBundleCompleteCnt, assetBundleMaxValueCnt)); // ダウンロード中のパラメータ取得 サンプル.
            };
        }

        /// <summary>
        /// アッセトバンドルをダウンロードしてトラッキング.
        /// </summary>
        public IObservable<AssetBundleManager>Load(ushort? tutorialStep, UIType uiType = UIType.Tips)
        {
            if (tutorialStep == null)
            {
                // [初回1度]チュートリアルの途中でダウンロードするシーンがあるのでいつもの処理ではチュートリアル素材だけ渡す.
                return _loadAssetBundle(Group.Tutorial, Noroshi.GlobalContainer.MasterManager.AssetBundleMaster.GetAssetBundleTutorial(), uiType);
            }
            else
            {
                // [通常フロー]全件をダウンロードOrキャッシュから取得してAssetBundleを作成.
                return _loadAssetBundle(Group.Public, Noroshi.GlobalContainer.MasterManager.AssetBundleMaster.GetAssetBundlesPublic(), uiType);
            }
        }

        /// <summary>
        /// Item素材の取得 <T>.
        /// </summary>
        public T LoadFromItemAssetBundle<T>(uint id)
        where T : UnityEngine.Object
        {
            return _getAssetBundleFromValue<T>(BUNDLE_LABEL_ITEM, id.ToString());
        }

        /// <summary>
        /// Item素材の取得 Observable<T>.
        /// </summary>
        public IObservable<T> LoadFromItemAssetBundleObservable<T>(uint id)
        where T : UnityEngine.Object
        {
            return Observable.Return<T>(LoadFromItemAssetBundle<T>(id));
        }


        /// <summary>
        /// Character / UICharacterから取得 <T>.
        /// </summary>
        public T LoadFromCharacterAssetBundle<T>(uint characterId, string assetName)
        where T : UnityEngine.Object
        {
            if (GlobalContainer.Config.EditView) {
                var resourcesPath = _editViewingResourcesPath(characterId, assetName);
                if (System.IO.Directory.Exists("Assets/Noroshi/Resources/" + resourcesPath[0])) {
                    var prefab = Resources.Load<T> (string.Join("/", resourcesPath));
                    Debug.LogWarning ("EditViewing = " + string.Join("/", resourcesPath));
                    return prefab;
                }
            }
            var assetBundlelabel = string.Format (BUNDLE_LABEL_CHARACTER_BASE, characterId);
            // 将来的にはアセットバンドル単位でのロード・アンロードにすべきだが、取り急ぎ、サイズの大部分を占める Texture 対応。
            // シェーダーを複製することで紐付いた Texture が再ロードされる裏技。
            if (assetName == "Character" || assetName == "UICharacter")
            {
                foreach (var material in _getAssetBundle(assetBundlelabel).LoadAllAssets<Material>())
                {
                    material.shader = material.shader;
                }
            }
            return _getAssetBundleFromValue<T> (assetBundlelabel, assetName);
        }

        /// <summary>
        /// Character / UICharacterから取得 Observable<T>.
        /// </summary>
        public IObservable<T> LoadFromCharacterAssetBundleObservable<T>(uint characterId, string assetName)
        where T : UnityEngine.Object
        {
            return Observable.Return<T>(LoadFromCharacterAssetBundle<T>(characterId, assetName));
        }


        /// <summary>
        /// Character / UICharacter Prefab取得 <T>.
        /// </summary>
        public GameObject LoadFromCharacterPrefab(uint characterId, string assetName)
        {
            if (GlobalContainer.Config.EditView) {
                var resourcesPath = _editViewingResourcesPath (characterId, assetName);
                if ( System.IO.Directory.Exists("Assets/Noroshi/Resources/" + resourcesPath[0])) {
                    var prefab = Resources.Load<GameObject> (string.Join("/", resourcesPath));
                    Debug.LogWarning ("EditViewing = " + string.Join("/", resourcesPath));
                    return prefab;
                }
            }
            var assetBundlelabel = string.Format (BUNDLE_LABEL_CHARACTER_BASE, characterId);
            // 将来的にはアセットバンドル単位でのロード・アンロードにすべきだが、取り急ぎ、サイズの大部分を占める Texture 対応。
            // シェーダーを複製することで紐付いた Texture が再ロードされる裏技。
            if (assetName == "Character" || assetName == "UICharacter")
            {
                foreach (var material in _getAssetBundle(assetBundlelabel).LoadAllAssets<Material>())
                {
                    material.shader = material.shader;
                }
            }
            return _getAssetBundleFromValue<GameObject> (assetBundlelabel, assetName);
        }

        /// <summary>
        /// 非同期 に Character / UICharacterから取得.
        /// </summary>
        public IObservable<T> LoadAsyncFromCharacterAssetBundle<T>(uint characterId, string assetName)
        where T : UnityEngine.Object
        {
            if (GlobalContainer.Config.EditView) {
                var resourcesPath = _editViewingResourcesPath (characterId, assetName);
                if (System.IO.Directory.Exists("Assets/Noroshi/Resources/"+resourcesPath[0])) {
                    var resources = Noroshi.MonoBehaviours.Loader.LoadObjectFromResourceAsync<T>( string.Join("/", resourcesPath));
                    Debug.LogWarning ("EditViewing = " + string.Join("/", resourcesPath));
                    return resources;
                }
            }

            var assetBundle = _getAssetBundle(string.Format(BUNDLE_LABEL_CHARACTER_BASE, characterId));
            // 将来的にはアセットバンドル単位でのロード・アンロードにすべきだが、取り急ぎ、サイズの大部分を占める Texture 対応。
            // シェーダーを複製することで紐付いた Texture が再ロードされる裏技。
            if (assetName == "Character" || assetName == "UICharacter")
            {
                foreach (var material in assetBundle.LoadAllAssets<Material>())
                {
                    material.shader = material.shader;
                }
            }
            return _loadAssetAsync<T>(assetName, assetBundle);
        }

        /// <summary>
        /// 開発中の視聴の為 Resources.Loadで素材を取得する際のパス作成.
        /// </summary>
        string[] _editViewingResourcesPath(uint id, string assetName)
        {
            string[] resourcesPath = new string[2];
            if (assetName == "Character") {
                resourcesPath[0] = "Character/" + id;
                resourcesPath[1] = assetName;
            } else if (assetName == "UICharacter") {
                resourcesPath[0] = "UICharacter/" + id;
                resourcesPath[1] = assetName;
            } else if (assetName.Contains ("thumb_")) {
                resourcesPath[0] = "Character/" + id;
                resourcesPath[1] = assetName;
            } else if (assetName.Contains ("skillbutton_")) {
                resourcesPath[0] = "Character/" + id + "/Battle";
                resourcesPath[1] = assetName;
            } else if (assetName.Contains("Action")) {
                resourcesPath[0] = "Character/" + id + "/Battle";
                resourcesPath[1] = assetName;
            } else if (assetName.Contains("Bullet")) {
                resourcesPath[0] = "Character/" + id + "/Battle";
                resourcesPath[1] = assetName;
            } else if (assetName.Contains("skill")) {
                resourcesPath[0] = "UICharacter/" + id + "/SkillIcon";
                resourcesPath[1] = assetName;
            } else {
                Debug.LogError("_editViewingResourcesPath Make Failure " + assetName + " id "+id);
            }

            return resourcesPath;
        }

        /// <summary>
        /// 非同期に AudioClipを取得する.
        /// </summary>
        public IObservable<AudioClip[]> LoadAsyncAudioClips(SoundType soundType, IEnumerable<string> audioClipPathes)
        {
            var assetBundle = _getAssetBundle(_dictSoundTypeToBundleName[soundType]);
            return audioClipPathes.Select(path => {

                if(GlobalContainer.Config.EditView &&
                    System.IO.File.Exists(Application.dataPath + "/Noroshi/Resources/Sound/" + path + ".ogg"))
                {
                    Debug.LogWarning ("EditViewing = " + path);
                    return Noroshi.MonoBehaviours.Loader.LoadObjectFromResourceAsync<AudioClip>("Sound/" + path);
                }
                var pathSplit = path.Split('/');
                var bundleName = pathSplit[pathSplit.Length - 1];
                return _loadAssetAsync<AudioClip>(bundleName, assetBundle);
            }).WhenAll();
        }


        IObservable<T> _loadAssetAsync<T>(string fileName, UnityEngine.AssetBundle assetBundle)
        where T : UnityEngine.Object
        {
            return Observable.FromCoroutine<T>((observer, cancellationToken) => _loadObjectFromAssetBundleCoroutine<T>(fileName, assetBundle, observer, cancellationToken));
        }
        IEnumerator _loadObjectFromAssetBundleCoroutine<T>(string filePath, UnityEngine.AssetBundle assetBundle, IObserver<T> observer, CancellationToken cancellationToken)
        where T : UnityEngine.Object
        {
            var bundleRequest = assetBundle.LoadAssetAsync(filePath);
            while (!bundleRequest.isDone && !cancellationToken.IsCancellationRequested)
            {
                yield return null;
            }
            if (cancellationToken.IsCancellationRequested) yield break;
            observer.OnNext(bundleRequest.asset as T);
            observer.OnCompleted();
            //Debug.LogWarning("LoadAssetAsyncFromAudioClips OnCompleted = " + assetBundle.name + " " + filePath);
        }

        T _getAssetBundleFromValue<T>(string bundleName, string assetName)
        where T : UnityEngine.Object
        {
            var assetBundle = _getAssetBundle(bundleName);
            if (assetBundle != null)
            {
                //Debug.LogWarning("GetAssetBundleFromValue OnCompleted = " + bundleName + " " + assetName);
                return assetBundle.LoadAsset<T>(assetName);
            }
            // 運用ミスの可能性大: asset_bundleマスターの更新忘れか、S3サーバにAssetBundleをアップデートしていないか.
            Debug.LogError(string.Format("Not Exist Bundle : {0} on Asset : {1}", bundleName, assetName));
            return null;
        }

        UnityEngine.AssetBundle _getAssetBundle (string bundleName)
        {
            if (IsExistentBundleDict(bundleName))
            {
                return _dictAssetBundleRefs[bundleName].assetBundle;
            }
            return null;
        }


        /// <summary>
        /// トラッキングしているAssetBundleを開放する.
        /// </summary>
        void _unload(string bundleName, int version, bool allObjects)
        {
            AssetBundleRef abRef;
            if (_dictAssetBundleRefs.TryGetValue(bundleName, out abRef))
            {
                abRef.assetBundle.Unload(allObjects);
                abRef.assetBundle = null;
                _dictAssetBundleRefs.Remove(bundleName);
            }
        }


        /// <summary>
        /// トラッキングしているAssetBundleを全て解放する.
        /// </summary>
        public void UnloadAll()
        {
            foreach (var abRef in _dictAssetBundleRefs.Values)
            {
                abRef.assetBundle.Unload(false);
            }
            _dictAssetBundleRefs.Clear();
        }

        /// <summary>
        /// AssetBundleが存在するか確認する.
        /// 非同期で行うダウンロードを外部で完了する用途.
        /// </summary>
        public bool IsExistentBundleDict(string bundleName)
        {
            return _dictAssetBundleRefs.Count < 0 ? false : _dictAssetBundleRefs.ContainsKey(bundleName);
        }

        string _makePathToBundlesRoot()
        {

            /*
            S3サーバ構成.
            └1.0.0(version)
              └0(初回起動時のみ　タイトルからチュートリアルのダウンロードするシーンまでで使う素材だけ)
                └iOS
                └Android
                └Win
                └OSX
              └1(フルダウンロード)
                └iOS
                └Android
                └Win
                └OSX
            */

            var platform = "";
            _platformManifest = "";
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            platform = _platformManifest = "Win";
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            platform = _platformManifest = "OSX";
#elif UNITY_IOS
            platform = _platformManifest = "iOS";
#elif UNITY_ANDROID
            platform = _platformManifest = "Android";
#else
            platform = "error";
            Debug.Log("unsupported platform");
#endif

            // パス作成 {S3BaseURL/}{version}/{group}/{platform}/
            return string.Format("{0}{1}/{2}/{3}/",
                Noroshi.GlobalContainer.MasterManager.ConfigMaster.GetAssetBundleBaseUrl(),
                GlobalContainer.Config.AssetBundle == null ? Noroshi.AppConstant.APP_VERSION : GlobalContainer.Config.AssetBundle,
                (uint)_trackedGroup,
                platform);
        }

        IObservable<AssetBundleManager> _loadAssetBundle(Group group, NoroshiAssetBundle[] bundleList, UIType uiType)
        {

            if (_trackedGroup == Group.Tutorial && group == Group.Public
            || _trackedGroup == Group.Public && group == Group.Tutorial)
            {
                UnloadAll();
                Caching.CleanCache();
            }

            if (bundleList.Count() == 0) {
                return Observable.Return<AssetBundleManager>(this);
            }

            _trackedGroup = group;

            assetBundleCompleteCnt = 0u; // 取得し終わった件数.
            assetBundleMaxValueCnt = (uint)bundleList.Count(); // 最大要件件数.

            var pg = new GameObject ("progressbar");
            progressbar =  new GameObject ("progressbar").gameObject.AddComponent<AssetBundleProgress>().Build(uiType);

            var progress = _progressAction(); // ゲージ処理として呼ばれるメソッド.

            // ダウンロードグループ順に垂直実行.
            var assetBundleList = bundleList.ToLookup(asset => asset.ResourceType);
            var requests = bundleList.OrderBy( assetbunde => assetbunde.ResourceType )
            .Select(bundle => Observable.Defer(() => _loadBundleCoroutine(bundle.BundleName, (int)bundle.Version, progress).Do(e => assetBundleCompleteCnt++))).ToArray();

            return _loadManifest().Do(assetBundleManifest => _assetBundleManifest = assetBundleManifest)
            .SelectMany(_ => requests.Concat().Last())
            .Select(_ =>
            {
                progressbar.LoadEnd();
                return this;
            });

            /*
            // ダウンロードグループ事に水平実行.
            return _loadManifest().Do(assetBundleManifest => _assetBundleManifest = assetBundleManifest)
            .SelectMany(_ => assetBundleList[ResourceType.Character].Select(bundle =>  Observable.Defer(() => _loadBundleCoroutine(bundle.BundleName, (int)bundle.Version, progress).Do( e => assetBundleCompleteCnt++))).WhenAll())
            .SelectMany(_ => assetBundleList[ResourceType.Item].Select(bundle => _loadBundleCoroutine(bundle.BundleName, (int)bundle.Version, progress).Do( e => assetBundleCompleteCnt++)).WhenAll())
            .SelectMany(_ => assetBundleList[ResourceType.BGM].Select(bundle => _loadBundleCoroutine(bundle.BundleName, (int)bundle.Version, progress).Do( e => assetBundleCompleteCnt++)).WhenAll())
            .SelectMany(_ => assetBundleList[ResourceType.SE].Select(bundle => _loadBundleCoroutine(bundle.BundleName, (int)bundle.Version, progress).Do( e => assetBundleCompleteCnt++)).WhenAll())
            .SelectMany(_ => assetBundleList[ResourceType.Voice].Select(bundle => _loadBundleCoroutine(bundle.BundleName, (int)bundle.Version, progress).Do( e => assetBundleCompleteCnt++)).WhenAll())
            .Select(_ => { assetBundleCompleteCnt++; return this; });
            */
        }


        IObservable<AssetBundleManifest>_loadManifest(UnityAction<float> progressAction = null)
        {
            var url = _makePathToBundlesRoot() + _platformManifest;
            Debug.Log(url);

            ScheduledNotifier<float> progress = new ScheduledNotifier<float>();
            var watcher = progress.Subscribe(prog =>
            {
                if (progressAction != null)
                {
                    progressAction(prog);
                }
            });

            return ObservableWWW.GetWWW( url, null, progress)
            .Select(www =>
            {
                var manifest = (AssetBundleManifest)www.assetBundle.LoadAsset("AssetBundleManifest", typeof(AssetBundleManifest));
                www.assetBundle.Unload(false);
                watcher.Dispose();
                return manifest;
            });
        }


        IObservable<bool>_loadBundleCoroutine(string bundleName, int version, UnityAction<float> progressAction = null)
        {

            // 依存関係のあるBundleをダウンロード.
            //var dependencies = _assetBundleManifest.GetAllDependencies(bundleName);
            //Observable.WhenAll(dependencies.Where(b => !IsExistentBundleDict(b)).Select(dependencie => _loadBundleCoroutine(dependencie, version, null)).ToArray());

            bundleName = _remapVariantName(bundleName);
            var url = _makePathToBundlesRoot() + bundleName + string.Format("?version={0}", version);

            //if (Caching.IsVersionCached(bundleName, version)) return Observable.Return<bool>(false);

            GlobalContainer.Logger.Debug(url);

            ScheduledNotifier<float> progress = new ScheduledNotifier<float>();
            var watcher = progress.Subscribe(prog =>
            {
                if (progressAction != null)
                {
                    progressAction(prog);
                }
            });

            //キャッシュに残っていればキャッシュから落とす version値が変化した場合は強制的に更新.
            return ObservableWWW.LoadFromCacheOrDownload(url, version, progress)
            .Select( (assetBundle) => 
            {
                _addAssetBundle(url, version, bundleName, assetBundle);
                watcher.Dispose();
                return true;
            } );
        }

        AssetBundleRef _addAssetBundle(string url, int version, string bundleName, UnityEngine.AssetBundle assetBundle)
        {
            AssetBundleRef abRef = new AssetBundleRef(url, version, assetBundle);
            _dictAssetBundleRefs.Add(bundleName, abRef);
            return abRef;
        }

        string _remapVariantName(string assetBundleName)
        {
            var bundlesWithVariant = _assetBundleManifest.GetAllAssetBundlesWithVariant();

            if (System.Array.IndexOf(bundlesWithVariant, assetBundleName) < 0)
            {
                return assetBundleName;
            }

            var splitBundleName = assetBundleName.Split('.');
            var candidateBundles = System.Array.FindAll(bundlesWithVariant, element => element.StartsWith(splitBundleName[0]));

            var index = -1;
            for (int i = 0; i < _bundleVariants.Length; i++)
            {
                index = System.Array.IndexOf(candidateBundles, splitBundleName[0] + "." + _bundleVariants[i]);
                if (index != -1)
                {
                    return candidateBundles[index];
                }
            }

            return assetBundleName;
        }

    }

}


