﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Noroshi.AssetBundle;

namespace Noroshi.AssetBundle
{

    public abstract class AbstractAssetBundleProgress : MonoBehaviour
    {
        protected float assetLoadProgress = 0f;
        protected uint assetLoadCompleteCnt = 0;
        protected uint assetLoadMaxValueCnt = 0;

        [SerializeField] GameObject loadingBar;
        [SerializeField] Text nowText;
        [SerializeField] Text maxText;

        float _loadingBarWidth;

        public virtual void LoadStart()
        {
            //MEMO:初期化処理必要だったら使ってください. 不要ならばAssetBundleProgress()で呼んでる箇所を消せば呼ばれなくなるのでそこを消した上, 削除してください.
            _loadingBarWidth = loadingBar.GetComponent<RectTransform>().sizeDelta.x;
        }

        public virtual void LoadProgress(float progressIN, uint assetBundleCompleteCntIn, uint assetBundleMaxValueCntIN)
        {
            var xx = _loadingBarWidth - _loadingBarWidth * progressIN;
            assetLoadProgress = progressIN;
            assetLoadCompleteCnt = (uint)(System.Math.Floor((int)assetBundleCompleteCntIn + progressIN));  // progressINが1になったら切り捨てられずカウントアップ.
            assetLoadMaxValueCnt = assetBundleMaxValueCntIN;

            nowText.text = assetLoadCompleteCnt.ToString();
            maxText.text = assetLoadMaxValueCnt.ToString();

            loadingBar.transform.localPosition = new Vector3(-xx, 0, 0);
        }

        public virtual void LoadEnd()
        {
            //MEMO:ダウンロード完了と同時に AssetBundleManagerに殺されるので ゲージが完了した状態をユーザが視認できるよう少し時間を与えているだけ　デザインを入れる過程でよしなに終了処理を書いて下さい.
            Destroy(this.gameObject, 0.2f); 
        }
    }
}
