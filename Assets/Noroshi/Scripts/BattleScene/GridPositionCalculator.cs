﻿using System;
using System.Collections.Generic;
using UniLinq;
using Noroshi.Game;

namespace Noroshi.BattleScene
{
    /// <summary>
    /// グリッドポジション計算を扱うクラス。
    /// </summary>
    public class GridPositionCalculator
    {
        readonly static Dictionary<byte, byte> NO_TO_LINE_NO_MAP = new Dictionary<byte, byte>
        {
            {5, 5},
            {3, 4},
            {1, 3},
            {2, 2},
            {4, 1},
        };
        readonly static int BOTTOM_SHADOW_LINE_SIZE = Constant.MAX_SHADOW_CHARACTER_NUM_IN_FIELD_PER_FORCE - Constant.MAX_NORMAL_CHARACTER_NUM_IN_FIELD_PER_FORCE - 1;


        public GridPositionCalculator()
        {
        }

        /// <summary>
        /// 垂直方向サイズを取得。
        /// </summary>
        /// <returns>The vertical size.</returns>
        public ushort GetVerticalSize()
        {
            return (Constant.MAX_NORMAL_CHARACTER_NUM_IN_FIELD_PER_FORCE + Constant.MAX_SHADOW_CHARACTER_NUM_IN_FIELD_PER_FORCE )* 2;
        }

        /// <summary>
        /// 自キャラクターの垂直方向インデックスを取得。
        /// </summary>
        /// <returns>垂直方向インデックス</returns>
        /// <param name="noInSameCharacterPosition">同ポジション内順番</param>
        /// <param name="usedLineNum">既に埋まっているライン数</param>
        /// <param name="totalNumInSameCharacterPosition">同ポジション内総数</param>
        public ushort GetOwnCharacterVerticalIndex(byte noInSameCharacterPosition, byte usedLineNum, byte totalNumInSameCharacterPosition)
        {
            var lineNo = _getLineNo(noInSameCharacterPosition, usedLineNum, totalNumInSameCharacterPosition);
            return (ushort)((lineNo - 1) * 4 +  BOTTOM_SHADOW_LINE_SIZE + 2 + 1);
        }
        /// <summary>
        /// 敵キャラクターの垂直方向インデックスを取得。
        /// </summary>
        /// <returns>垂直方向インデックス</returns>
        /// <param name="noInSameCharacterPosition">同ポジション内順番</param>
        /// <param name="usedLineNum">既に埋まっているライン数</param>
        /// <param name="totalNumInSameCharacterPosition">同ポジション内総数</param>
        public ushort GetEnemyCharacterVerticalIndex(byte noInSameCharacterPosition, byte characterPositionOffset, byte sameCharacterPositionNum)
        {
            var lineNo = _getLineNo(noInSameCharacterPosition, characterPositionOffset, sameCharacterPositionNum);
            return (ushort)((lineNo - 1) * 4 +  BOTTOM_SHADOW_LINE_SIZE + 2);
        }
        /// <summary>
        /// ライン番号を取得。
        /// </summary>
        /// <returns>ライン番号</returns>
        /// <param name="noInSameCharacterPosition">同ポジション内順番</param>
        /// <param name="usedLineNum">既に埋まっているライン数</param>
        /// <param name="totalNumInSameCharacterPosition">同ポジション内総数</param>
        public byte _getLineNo(byte noInSameCharacterPosition, byte usedLineNum, byte totalNumInSameCharacterPosition)
        {
            // 該当ポジションに割り当てられているライン番号を選定。
            var lineNos = new byte[totalNumInSameCharacterPosition];
            for (var i = 0; i < totalNumInSameCharacterPosition; i++)
            {
                lineNos[i] = NO_TO_LINE_NO_MAP[(byte)(usedLineNum + 1 + i)];
            }
            // ライン番号が小さい順に埋めていく。
            return lineNos.OrderBy(i => i).ToArray()[noInSameCharacterPosition - 1];
        }

        /// <summary>
        /// 分身キャラクターの垂直方向インデックスを取得。
        /// </summary>
        /// <returns>垂直方向インデックス</returns>
        /// <param name="force">勢力</param>
        /// <param name="baseVerticalIndex">基準とする垂直方向のインデックス（これに近いものが優先される）</param>
        public IEnumerable<ushort> GetShadowCharacterVerticalIndexes(Force force, ushort? baseVerticalIndex)
        {
            var diff = force == Force.Own ? 1 : 0;
            var groupNum = Constant.MAX_NORMAL_CHARACTER_NUM_IN_FIELD_PER_FORCE;
            var indexes = (new List<int>{0}).Select(i => i + diff).ToList();
            indexes.AddRange(Enumerable.Range(1, groupNum).Select(n => (n - 1) * (1 + 1) * 2 + BOTTOM_SHADOW_LINE_SIZE + diff).ToList());
            indexes.AddRange((new List<int>{22,24}).Select(i => i + diff));
            var target = baseVerticalIndex.HasValue ? baseVerticalIndex.Value : GetVerticalSize() / 2;
            return indexes.Select(i => (ushort)i).OrderBy(i => Math.Abs(i - target)).ThenBy(i => i);
        }
    }
}
