﻿namespace Noroshi.BattleScene.Sound
{
    public enum SoundCommand
    {
        Play,
        Stop,
        Cancel,
        FadeBgmVolumeZero,
        FadeBgmVolumeHalf,
        FadeBgmVolumeMax,
        ResetBgmVolume,
    }
    public class SoundEvent
    {
        public SoundCommand Command;
        public uint SoundID;
    }
}
