﻿using System;
using UniLinq;
using UniRx;

namespace Noroshi.BattleScene.Sound
{
    public class SoundManager : IManager
    {
        CompositeDisposable _disposables = new CompositeDisposable();

        const float _bgmVolumeInterval = 0.1f;
        float _currentBgmVolume = 1.0f;
        IDisposable _setBgmVolumeDisposable = null;

        public void Initialize()
        {
            SceneContainer.GetSceneManager().GetOnCommandSoundObservable()
            .Subscribe(e => _onCommandSound(e)).AddTo(_disposables);
        }

        public IObservable<IManager> LoadDatas()
        {
            return Observable.Return((IManager)this);
        }

        public IObservable<IManager> LoadAssets(IFactory factory)
        {
            var preloadBgmSoundIds = SceneContainer.GetBattleManager().GetBattleBgmSoundIDs();
            return GlobalContainer.SoundManager.PreloadSounds(preloadBgmSoundIds).Select(_ => (IManager)this);
        }
        public void Prepare()
        {
        }

        void _onCommandSound(SoundEvent soundEvent)
        {
            switch (soundEvent.Command)
            {
                case SoundCommand.Play:
                    GlobalContainer.SoundManager.Play(soundEvent.SoundID).Subscribe(_ => {});
                    break;
                case SoundCommand.Stop:
                    GlobalContainer.SoundManager.Stop(soundEvent.SoundID);
                    break;
                case SoundCommand.Cancel:
                    GlobalContainer.SoundManager.Cancel(soundEvent.SoundID);
                    break;
                case SoundCommand.FadeBgmVolumeZero:
                    _setBgmVolume(0.0f, 3.0f);
                    break;
                case SoundCommand.FadeBgmVolumeHalf:
                    _setBgmVolume(0.8f, 1.0f);
                    break;
                case SoundCommand.FadeBgmVolumeMax:
                    _setBgmVolume(1.0f, 1.0f);
                    break;
                case SoundCommand.ResetBgmVolume:
                    _setBgmVolume(1.0f, 0.0f);
                    break;
                default:
                    throw new InvalidOperationException();
            }
        }

        /// BGM音量の設定
        void _setBgmVolume(float Volume)
        {
            _currentBgmVolume = Volume;
            GlobalContainer.SoundManager.SetVolume(Noroshi.Core.Game.Sound.SoundType.Bgm, _currentBgmVolume);
        }
        /// フェードするBGM音量の設定
        void _setBgmVolume(float Volume, float DueTime)
        {
            // 既存音量フェードを停止する
            if (_setBgmVolumeDisposable != null)
            {
                _setBgmVolumeDisposable.Dispose();
                _setBgmVolumeDisposable = null;
            }

            // 音量設定の必要性確認
            if (_currentBgmVolume == Volume) return;
                
            if (DueTime > _bgmVolumeInterval)
            {
                var count = 0;
                var countMax = (int)(DueTime / _bgmVolumeInterval);
                var diff = (Volume - _currentBgmVolume) / countMax;

                // 音量フェードを開始する
                _setBgmVolumeDisposable = SceneContainer.GetTimeHandler().Interval(_bgmVolumeInterval)
                .Do(_ =>
                {
                    if (count++ < countMax)
                    {
                        // フェードは継続
                        _setBgmVolume(_currentBgmVolume + diff);
                    }
                    else
                    {
                        // 音量を設定してフェードの停止
                        _setBgmVolume(Volume, 0.0f);
                    }
                })
                .Subscribe(_ => {});
            }
            else
            {
                // 音量を直ちに設定する
                _setBgmVolume(Volume);
            }
        }

        public void Dispose()
        {
            GlobalContainer.SoundManager.Clear();
            _disposables.Dispose();
        }
    }
}
