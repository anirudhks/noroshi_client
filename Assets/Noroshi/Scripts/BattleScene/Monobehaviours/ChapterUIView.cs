﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Noroshi.BattleScene.UI;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class ChapterUIView : UIView, IChapterUIView
    {
        const string CHAPTER_NUM_TEXT = "Battle.Title.Chapter";
        const string EPISODE_NUM_TEXT = "Battle.Title.Episode";

        [SerializeField] Text _episodeText;
        [SerializeField] Text _episodeTitleText;
        [SerializeField] Text _chapterText;
        [SerializeField] Image _headerPartsImage;
        [SerializeField] Image _footerPartsImage;
        [SerializeField] float _activeTime = 2.5f;
        [SerializeField] float _deactivateDuration = 0.5f;
        [SerializeField] float _textActivateDuration = 1f;

        Image _image;
        Text[] _texts;

        new void Awake()
        {
            base.Awake();
            _image = GetComponent<Image>();
            _texts = GetComponentsInChildren<Text>();
            _headerPartsImage.color = new Color(1, 1, 1, 0);
            _footerPartsImage.color = new Color(1, 1, 1, 0);
            SetActive(false);
        }

        public IObservable<IChapterUIView> Activate(string titleTextKey, int[] titleParams, string subTitleTextKey, int[] subTitleParams)
        {
            var onDeactivate = new Subject<IChapterUIView>();
            SetActive(true);
            var delay = _activeTime - _deactivateDuration;
            _image.DOColor(new Color(0, 0, 0, 0.8f), _deactivateDuration)
            .SetDelay(delay)
            .OnComplete(() =>
            {
                SetActive(false);
                onDeactivate.OnNext(this);
                onDeactivate.OnCompleted();
            });

            _visibleAnimation();
            _invisibleAnimation(delay);

            // episode
            var episodeNumTextKey = GlobalContainer.LocalizationManager.GetText(EPISODE_NUM_TEXT);
            _episodeText.text = titleParams != null ? string.Format(episodeNumTextKey, _changeTextSizeWithRichText(titleParams[0].ToString(), 30)) : string.Empty;
            _episodeTitleText.text = GlobalContainer.LocalizationManager.GetText(titleTextKey);

            // chapter
            var chapterNumTextKey = GlobalContainer.LocalizationManager.GetText(CHAPTER_NUM_TEXT);
            _chapterText.text = subTitleParams != null ? string.Format(chapterNumTextKey, (byte)subTitleParams[0]) : string.Empty + 
                                "　" + _changeTextSizeWithRichText(GlobalContainer.LocalizationManager.GetText(subTitleTextKey), 24);
            
            return onDeactivate.AsObservable();
        }

        string _changeTextSizeWithRichText(string text, int size)
        {
            return "<size=" + size.ToString() + ">" + text + "</size>";
        }

        void _visibleAnimation()
        {
            var endColor = Color.white;
            var previewColor = Color.white;
            for (var i = 0; i < _texts.Length; i++)
            {
                var text = _texts[i];
                endColor = text.color;
                previewColor = new Color(endColor.r, endColor.g, endColor.b, 0);
                text.color = previewColor;
                text.DOColor(endColor, _textActivateDuration);
            }
            _headerPartsImage.DOColor(Color.white, _textActivateDuration);
            _footerPartsImage.DOColor(Color.white, _textActivateDuration);
        }

        void _invisibleAnimation(float delay)
        {
            for (var i = 0; i < _texts.Length; i++)
            {
                _texts[i].DOColor(new Color(0, 0, 0, 0), _deactivateDuration).SetDelay(delay);
            }
            _headerPartsImage.DOColor(new Color(0, 0, 0, 0), _deactivateDuration).SetDelay(delay);
            _footerPartsImage.DOColor(new Color(0, 0, 0, 0), _deactivateDuration).SetDelay(delay);
        }
    }
}
