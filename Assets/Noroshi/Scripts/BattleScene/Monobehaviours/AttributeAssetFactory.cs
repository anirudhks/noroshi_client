﻿using System.Collections.Generic;
using UniRx;
using Noroshi.BattleScene.Actions.Attributes;
using Noroshi.MonoBehaviours;

namespace Noroshi.BattleScene.MonoBehaviours
{
    /// <summary>
    /// Attribute 関連素材のファクトリー。
    /// </summary>
    public class AttributeAssetFactory : IAttributeAssetFactory
    {
        Queue<ICharacterView> _frogViews = new Queue<ICharacterView>();

        /// <summary>
        /// カエル View を事前ロード。
        /// </summary>
        /// <param name="num">事前ロード数</param>
        public IObservable<ICharacterView[]> PreloadFrogView(byte num)
        {
            var observables = new IObservable<ICharacterView>[num];
            for (var i = 0; i < num; i++)
            {
                observables[i] = _buildFrogView();
            }
            return observables.WhenAll().Do(views =>
            {
                foreach (var view in views)
                {
                    _frogViews.Enqueue(view);
                }
            });
        }

        /// <summary>
        /// カエル View をビルド。
        /// </summary>
        /// <returns>The frog view.</returns>
        public IObservable<ICharacterView> BuildFrogView()
        {
            if (_frogViews.Count > 0) return Observable.Return(_frogViews.Dequeue());
            return _buildFrogView();
        }

        /// <summary>
        /// カエル View を直接ロードしてビルド。
        /// </summary>
        /// <returns>The frog view.</returns>
        IObservable<ICharacterView> _buildFrogView()
        {
            return Loader.LoadComponentFromResourceAsync<CharacterView>("Attribute/Frog/Character")
                .DelayFrame(1) // Start() 待ち。
                .Cast<CharacterView, ICharacterView>();
        }

        /// <summary>
        /// カエル View を再利用のためにストックする。
        /// </summary>
        /// <param name="frogView">カエル View</param>
        public void StockFrogView(ICharacterView frogView)
        {
            _frogViews.Enqueue(frogView);
        }
    }
}
