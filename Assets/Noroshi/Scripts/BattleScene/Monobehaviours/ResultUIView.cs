using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using UnityEngine;
using DG.Tweening;
using Noroshi.Core.Game.Sound;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class ResultUIView : ModalUIView, UI.IResultUIView
    {
        // 敗北時に表示される各種ボタンのデフォルト位置
        const int DEFAULT_TRANSITION_BUTTON_POSITION_Y = -60;

        [SerializeField] AnimatorUIView _headlineContainer;
        [SerializeField] ParticleSystem _headLineParticle;
        [SerializeField] UIView _evaluationStarContainer;
        [SerializeField] AnimatorUIView[] _evaluationStars;
        [SerializeField] ParticleSystem[] _evaluationStarParticles;
        [SerializeField] ResultMiscContainer _miscContainer;
        [SerializeField] UnityEngine.UI.Text _rankDescriptionText;
        [SerializeField] ResultItemContainer _itemContainer;
        [SerializeField] ResultCharacterContainer _ownCharacterContainer;
        [SerializeField] UIView _transitionContainer;
        [SerializeField] UnityEngine.UI.Text _tipsText;
        [SerializeField] UIView _trainingContainer;
        [SerializeField] UnityEngine.UI.Text _trainingResultValueText;
        [SerializeField] UIView _transitionPowerUpButtonView;
        [SerializeField] UIView _transitionGachaButtonView;
        [SerializeField] UIView _transitionTrailButtonView;
        [SerializeField] RectTransform[] _transitionRectTransforms;
        [SerializeField] UnityEngine.UI.Text _pickupCharacterWinMessage;
        [SerializeField] UnityEngine.UI.Text _trainingResultRewardText;
        [SerializeField] UnityEngine.UI.Text _trainingResultValueTypeText;

        /// サウンド操作時に操作内容が OnNext される Subject。
        Subject<SoundEvent> _onCommandSoundSubject = new Subject<SoundEvent>();
        // 敗北時各種ボタン(キャラ詳細、ガチャ、試練)を押した時に該当シーン名が OnNext される Subject 
        Subject<string> _onTransitToSceneSubject = new Subject<string>();
        byte _rank = 0;
        bool _isButtonDown = false;

        void Start()
        {
            if (_trainingContainer != null)
            {
                _transitionContainer.SetActive(false);
            }
            _transitionContainer.SetActive(false);
        }

        void OnDestroy()
        {
            if (_onTransitToSceneSubject != null)
            {
                _onTransitToSceneSubject.Dispose();
                _onTransitToSceneSubject = null;
            }
        }

        /// 必要なアセットをロード。
        public IObservable<UI.IResultUIView> LoadAssets()
        {
            
            return _loadCharacterAssets()
                .SelectMany(_ => _loadItemAssets())
                .Select(_ => (UI.IResultUIView)this);
        }

        IObservable<bool> _loadCharacterAssets()
        {
            return _ownCharacterContainer == null ? Observable.Return<bool>(true) : _ownCharacterContainer.LoadAssets();
        }
        IObservable<bool> _loadItemAssets()
        {
            return _itemContainer == null ? Observable.Return<bool>(true) : _itemContainer.LoadAssets();
        }

        /// サウンド操作時に操作内容が OnNext される Observable を取得。
        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _onCommandSoundSubject.AsObservable();
        }
        // 敗北時各種ボタン(キャラ詳細、ガチャ、試練)を押した時に該当シーン名が OnNext される Observableを取得 
        public IObservable<string> GetOnTransitToSceneObservable()
        {
            return _onTransitToSceneSubject.AsObservable();
        }

        // ユーザーとチュートリアルの進行度を受け取り、敗北時の各シーン遷移用ボタンが表示可能かをみる
        public void SetActiveLossTransitionSceneButton(uint playerLevel, Core.Game.Player.TutorialStep tutorialStep)
        {
            if (_transitionPowerUpButtonView != null)
            {
                // チュートリアルの装備を通過していたらフラグが立つ
                var isEpuipGearOpen = tutorialStep >= Core.Game.Player.TutorialStep.EquipGear;
                _transitionPowerUpButtonView.SetActive(isEpuipGearOpen);
            }

            if (_transitionGachaButtonView != null)
            {
                // 一度でもガチャを実行しているとフラグが立つ
                var isGachaOpen = tutorialStep >= Core.Game.Player.TutorialStep.LotGacha;
                _transitionGachaButtonView.SetActive(isGachaOpen);
            }

            if (_transitionTrailButtonView != null)
            {
                // 規定のレベルに到達しているとフラグが立つ
                var isTrailOpen = Core.Game.GameContent.GameContent.IsOpen(Core.Game.GameContent.GameContentID.Trial, (ushort)playerLevel, tutorialStep); 
                _transitionTrailButtonView.SetActive(isTrailOpen);
            }
        }
        /// ランクをセット。
        public void SetRank(byte rank)
        {
            _rank = rank;
        }
        // プレイヤー経験値をセット
        public void SetPlayerExp(uint playerExp)
        {
            if (_miscContainer != null) _miscContainer.SetPlayerExp(playerExp);
        }
        // ゴールドをセット
        public void SetGold(uint gold)
        {
            if (_miscContainer != null) _miscContainer.SetGold(gold);
        }
        // プレイヤーの経験値に関する情報をセット
        public void SetAddPlayerExpResult(Core.WebApi.Response.Players.AddPlayerExpResult addPlayerExpResult)
        {
            if (_miscContainer != null) _miscContainer.SetAddPlayerExpResult(addPlayerExpResult);
        }
        /// 報酬をセット。
        public void SetRewards(IEnumerable<Noroshi.Core.WebApi.Response.Possession.PossessionObject> rewards)
        {
            if (_trainingResultRewardText != null)
            {
                var textKey = string.Empty;
                if (rewards.Count() > 0)
                {
                    textKey = "UI.Heading.Reward";
                    _trainingResultRewardText.color = new Color(1, 0.76862745098039f, 0);
                }
                else
                {
                    textKey = "Battle.Result.Training.NotRewardItem";
                    _trainingResultRewardText.color = Color.white;
                }
                _trainingResultRewardText.text = GlobalContainer.LocalizationManager.GetText(textKey);
            }
            if (_itemContainer != null) _itemContainer.SetRewards(rewards);
        }
        /// 自キャラクターサムネイル情報をセット。
        public void SetOwnCharacterThumbnails(IEnumerable<CharacterThumbnail> characterThumbnail)
        {
            if (_ownCharacterContainer != null) _ownCharacterContainer.SetCharacterThumbnails(characterThumbnail);
        }
        /// 自キャラクター経験値獲得結果をセット。
        public void SetOwnCharacterProgress(byte no, float previousExpRatio, float currentExpRatio, ushort levelUpNum)
        {
            if (_ownCharacterContainer != null) _ownCharacterContainer.SetCharacterProgress(no, previousExpRatio, currentExpRatio, levelUpNum);
        }
        /// Tips をセット。
        public void SetTips(uint tipsId)
        {
            if (_tipsText != null) _tipsText.text = GlobalContainer.LocalizationManager.GetText("Battle.Result.Tips." + tipsId);
        }
        public void SetPickupCharacterMessage(uint pickupCharacterId)
        {
            // TODO 勝利時のメッセージはいつ出来上がる？
            if (_pickupCharacterWinMessage != null) _pickupCharacterWinMessage.text = string.Empty;
        }
        /// 修行結果 をセット。
        public void SetTrainingResult(ushort? defeatingNum, uint? totalDamage)
        {
            if (defeatingNum.HasValue && totalDamage.HasValue || !defeatingNum.HasValue && !totalDamage.HasValue)
            {
                throw new InvalidOperationException();
            }
            else if (defeatingNum.HasValue)
            {
                _trainingResultValueTypeText.text = GlobalContainer.LocalizationManager.GetText("Battle.Result.Training.DefeatingNum");
                _trainingResultValueText.text = string.Format("{0:#,##0}", defeatingNum.Value);
            }
            else if (totalDamage.HasValue)
            {
                _trainingResultValueTypeText.text = GlobalContainer.LocalizationManager.GetText("Battle.Result.Training.TotalDamage");
                _trainingResultValueText.text = string.Format("{0:#,##0}", totalDamage.Value);
            }
        }

        /// 結果モーダルを開く。
        public override IObservable<UI.IModalUIView> Open()
        {
            return base.Open()
                // Start() を確実に実行させたいのでちょっと待つ。
                .Delay(TimeSpan.FromSeconds(0.01f))
                /// 見出し表示を試行。
                .SelectMany(_ => _tryToDisplayHeadline())
                /// 評価星表示を試行。
                .SelectMany(_ => _tryToDisplayEvaluationStar())
                // 修行コンテナ表示を試行。
                .SelectMany(_ => _tryToDisplayTrainingContainer())
                // 自キャラクターコンテナ表示を施行。
                .SelectMany(_ => _tryToDisplayOwnCharacterContainer())
                // その他コンテナ表示を試行。
                .SelectMany(_ => _tryToDisplayMiscContainer())
                // アイテムコンテナ表示を試行。
                .SelectMany(_ => _tryToDisplayItemContainer())
                // シーン遷移コンテナを表示。
                .SelectMany(_ => _displayTransitionContainer())
                .Select(_ => (UI.IModalUIView)this);
        }

        /// 見出し表示を試行。
        IObservable<bool> _tryToDisplayHeadline()
        {
            if (_headlineContainer != null)
            {
                // 子にあるパーティクルも同時に再生する(勝利時限定)
                if (_headLineParticle != null) _headLineParticle.Play(true);
                return _headlineContainer.PlayAnimation();
            }
            else
            {
                return Observable.Return(false);
            }
        }

        /// 評価星表示を試行。
        IObservable<bool> _tryToDisplayEvaluationStar()
        {
            if (_evaluationStarContainer == null) return Observable.Return(false);
            // 星説明。
            _rankDescriptionText.text = GlobalContainer.LocalizationManager.GetText(string.Format("Battle.Result.Rank.{0}.Description", _rank));
            // 再生順に配列に詰めて、直列再生。
            var playEvaluationStarAnimationObservables = new IObservable<bool>[_evaluationStars.Length];
            for (byte starNo = 1; starNo <= _evaluationStars.Length; starNo++)
            {
                playEvaluationStarAnimationObservables[starNo - 1] = _playEvaluationStarAnimation(starNo);
            }
            return playEvaluationStarAnimationObservables.Concat().Last().Select(_ => true);
        }
        IObservable<bool> _playEvaluationStarAnimation(byte starNo)
        {
            if (starNo > _rank) return Observable.Return<bool>(false);
            return Observable.Defer(() =>
            {
                // クリアスターの効果音
                switch (starNo)
                {
                case 1:
                    _onCommandSoundSubject.OnNext(new SoundEvent()
                    {
                        SoundID = SoundConstant.BATTLE_CLEAR_STAR_1_SOUND_ID,
                        Command = SoundCommand.Play,
                    });
                    break;
                case 2:
                    _onCommandSoundSubject.OnNext(new SoundEvent()
                    {
                        SoundID = SoundConstant.BATTLE_CLEAR_STAR_2_SOUND_ID,
                        Command = SoundCommand.Play,
                    });
                    break;
                case 3:
                    _onCommandSoundSubject.OnNext(new SoundEvent()
                    {
                        SoundID = SoundConstant.BATTLE_CLEAR_STAR_3_SOUND_ID,
                        Command = SoundCommand.Play,
                    });
                    break;
                }
                _evaluationStars[starNo - 1].SetActive(true);
                _evaluationStarParticles[starNo - 1].Play(true);
                return _evaluationStars[starNo - 1].PlayAnimation().Select(_ => true);
            });
        }
        /// その他コンテナ表示を試行。
        IObservable<bool> _tryToDisplayMiscContainer()
        {
            if (_miscContainer == null) return Observable.Return(false);
            // カウントアップ演出とプレイヤー経験値ゲージアニメーション。
            return _miscContainer.PlayAllCountUpAndSliderAnimation();
        }

        /// アイテムコンテナ表示を試行。
        IObservable<bool> _tryToDisplayItemContainer()
        {
            if (_itemContainer == null) return Observable.Return(false);
            return _itemContainer.TryToDisplayItemContainer();
        }

        /// 自キャラクターコンテナ表示を試行。
        IObservable<bool> _tryToDisplayOwnCharacterContainer()
        {
            if (_ownCharacterContainer == null) return Observable.Return(false);
            return _ownCharacterContainer.TryToDisplayCharacterContainer();
        }
        /// シーン遷移コンテナを表示。
        IObservable<bool> _displayTransitionContainer()
        {
            _transitionContainer.SetActive(true);
            return Observable.Return(true);
        }

        /// 修行コンテナ表示を試行。
        IObservable<bool> _tryToDisplayTrainingContainer()
        {
            if (_trainingContainer == null) return Observable.Return(false);
            _trainingContainer.SetActive(true);
            return Observable.Return(true);
        }

        // UGUIのButtonを押した時に呼ばれる
        // 押している間位置を少しだけ下にずらす
        public void OnPointerDownTransitToScene(int id)
        {
            if (_isButtonDown) return;
            _isButtonDown = true;
            _transitionRectTransforms[id - 1].DOAnchorPosY(DEFAULT_TRANSITION_BUTTON_POSITION_Y - 5.0f, 0.01f);
        }

        // UGUIのButtonを放した時に呼ばれる
        // 位置を元の位置にもどしシーン遷移をする
        public void OnPointerUpTransitToScene(int id)
        {
            _transitionRectTransforms[id - 1].DOAnchorPosY(DEFAULT_TRANSITION_BUTTON_POSITION_Y, 0.01f)
            .OnComplete(() => 
            {
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    Command = SoundCommand.Play,
                    SoundID = Noroshi.UI.SoundController.SEKeys.DECIDE,
                });
                _onCommandSoundSubject.OnCompleted();
                switch(id)
                {
                case 1:
                    _trasitToCharacterList();
                    break;
                case 2:
                    _transitToGacha();
                    break;
                case 3:
                    _transitToTrial();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("Loss UI");
                }
                _isButtonDown = false;
            });
        }


        // キャラクター詳細画面へ遷移する
        void _trasitToCharacterList()
        {
            _onTransitToSceneSubject.OnNext(Noroshi.UI.Constant.SCENE_CHARACTER_LIST);
        }
        // ガチャ画面へ遷移する
        void _transitToGacha()
        {
            _onTransitToSceneSubject.OnNext(Noroshi.UI.Constant.SCENE_GACHA);
        }
        // 試練画面へ遷移する
        void _transitToTrial()
        {
            _onTransitToSceneSubject.OnNext(Noroshi.UI.Constant.SCENE_TRIAL);
        }
    }
}
