﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UniRx;
using Noroshi.BattleScene.UI;
using Noroshi.Core.Game.Battle;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class TrainingScoreUIView : UIView, ITrainingScoreUIView
    {
        [SerializeField] Text _scoreTitle = null;
        [SerializeField] Text _scoreValue = null;
        [SerializeField] RectTransform _backgroundRectTransform = null;
        [SerializeField] Vector2 _loopBattleAnchoredPosition;
        [SerializeField] Vector2 _attackBattleAnchoredPosition;

        public new void Awake()
        {
            base.Awake();
            this.SetActive(false);
        }

        public void SetLoopBattleData()
        {
            _backgroundRectTransform.anchoredPosition = _loopBattleAnchoredPosition;
            _scoreTitle.text = GlobalContainer.LocalizationManager.GetText("Battle.Heading.Kills");
            UpdateScore(0);
        }

        public void SetUnlimitedHPEnemyBattleData()
        {
            _backgroundRectTransform.anchoredPosition = _attackBattleAnchoredPosition;
            _scoreTitle.text = GlobalContainer.LocalizationManager.GetText("Battle.Heading.Damage");
            UpdateScore(0);
        }

        public void UpdateScore(uint score)
        {
            _scoreValue.text = score.ToString();
        }
    }
}
