﻿using System;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using DG.Tweening;
using Noroshi.Grid;
using Noroshi.BattleScene.Actions;
using System.Collections.Generic;
using Noroshi.MonoBehaviours;

namespace Noroshi.BattleScene.MonoBehaviours
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Collider2D))]
    public class BulletView : SpineView, Actions.IBulletView
    {
        [SerializeField] Transform[] _unreversibles;
        [Header("---attack")]
        [SerializeField] BulletMoveParam _bullet1MoveParam;
        [Header("---a_skill1")]
        [SerializeField] BulletMoveParam _bullet2MoveParam;
        [Header("---p_skill1")]
        [SerializeField] BulletMoveParam _bullet3MoveParam;
        [Header("---p_skill2")]
        [SerializeField] BulletMoveParam _bullet4MoveParam;
        [Header("---p_skill3")]
        [SerializeField] BulletMoveParam _bullet5MoveParam;

        // 放物線のイージング
        const Ease horizontalEasingWithParabola = Ease.Linear;
        const Ease verticalInEasingWithParabola = Ease.InQuad;
        const Ease verticalOutEasingWithParabola = Ease.OutQuad;
        // 放物線の高さ係数
        const float upperPositionConstantWithParabola = 0.1f;

        float _estimateDuration;
        Subject<IActionTargetView> _onHitSubject;
        Subject<IBulletView> _onStockSubject;
        Dictionary<string, BulletMoveParam> _bulletMoveParamDictionary = new Dictionary<string, BulletMoveParam>();
        bool _isLauncing = false;
        bool _isFirstStep = false;

        new void Awake()
        {
            base.Awake();
            // 設定漏れを防ぐために必要な設定はここでやってしまう。
            var rigidbody2D = GetComponent<Rigidbody2D>();
            var collider2D  = GetComponent<Collider2D>();
            rigidbody2D.gravityScale = 0;
            collider2D.isTrigger = true;
        }

        new void Start()
        {
            base.Start();
            
            _bulletMoveParamDictionary.Add(Constant.ACTION_RANK_0_ANIMATION_NAME, _bullet1MoveParam);
            _bulletMoveParamDictionary.Add(Constant.ACTION_RANK_1_ANIMATION_NAME, _bullet2MoveParam);
            _bulletMoveParamDictionary.Add(Constant.ACTION_RANK_2_ANIMATION_NAME, _bullet3MoveParam);
            _bulletMoveParamDictionary.Add(Constant.ACTION_RANK_3_ANIMATION_NAME, _bullet4MoveParam);
            _bulletMoveParamDictionary.Add(Constant.ACTION_RANK_4_ANIMATION_NAME, _bullet5MoveParam);

            // 移動の差分による角度の調整
            this.UpdateAsObservable().Scan(_transform.position, (prev, _) =>
            {
                var cur = _transform.position;
                if (_isFirstStep)
                {
                    _isFirstStep = false;
                }
                else if (_isLauncing)
                {
                    _setEulerAngle(cur - prev);
                }
                return cur;
            })
            .Skip(1)
            .Subscribe(_ => {})
            .AddTo(this);
        }

        public IObservable<IBulletView> GetOnStock()
        {
            _onStockSubject = new Subject<IBulletView>();
            return _onStockSubject.AsObservable();
        }
        public void Stock()
        {
            SetActive(false);
            _onStockSubject.OnNext(this);
            _onStockSubject.OnCompleted();
            _onHitSubject.OnCompleted();
        }
        public IObservable<IActionTargetView> Launch(IActionExecutorView iExecutorView, IActionTargetView iTargetView, string animationName)
        {
            var bulletMoveParam = _bulletMoveParamDictionary[animationName];
            var targetView = (ActionTargetView)iTargetView;
            _setPosition(iExecutorView);
            _play(animationName);
            SetActive(true);

            switch(bulletMoveParam.MoveType)
            {
                case MoveType.Linear:
                    _moveLinear(bulletMoveParam, targetView);
                    break;
                case MoveType.Parabola:
                    _moveParabola(bulletMoveParam, targetView);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
                    break;
            }

            _estimateDuration = (targetView.GetPosition() - GetPosition()).magnitude / bulletMoveParam.Velocity;
            _onHitSubject = new Subject<IActionTargetView>();
            return _onHitSubject.AsObservable();
        }
        public float GetEstimateDuration()
        {
            return _estimateDuration;
        }

        // 線形軌道
        void _moveLinear(BulletMoveParam bulletMoveParam, ActionTargetView targetView)
        {
            // 一応、画面サイズに対応したワールド長さよりも2倍飛ばしておく。
            var distance = WaveField.HORIZONTAL_LENGTH * 2;
            var duration = distance / bulletMoveParam.Velocity;
            var vector = (targetView.GetPosition() - GetPosition()).normalized;
            var endValue = GetPosition() + vector * distance;
            _transform.DOMove(endValue, duration).SetEase(bulletMoveParam.Easing).OnComplete(() =>
            {
                Stock();
            });
            if (bulletMoveParam.UseShotAngle)
            {
                _setEulerAngle(endValue - (Vector2)_transform.position);
            }
        }

        // 放物軌道
        void _moveParabola(BulletMoveParam bulletMoveParam, ActionTargetView targetView)
        {
            var startValue = GetPosition();
            var endValue = targetView.GetPosition();
            var centerValue = startValue + (endValue - startValue) / 2;
            var distance = Mathf.Abs((endValue - startValue).x);
            var duration = distance / bulletMoveParam.Velocity;
            var upperPosition = bulletMoveParam.UpperPositionWithParabola * upperPositionConstantWithParabola * distance;

            // 水平軸方向 衝突判定の確実化のために放物運動後もしばらく動き続ける
            _transform.DOMoveX(startValue.x + (endValue.x - startValue.x) * 2, duration * 2).SetEase(horizontalEasingWithParabola).OnComplete(() =>
            {
                _isLauncing = false;
                Stock();
            });

            // 垂直軸方向 放物運動が完了したら非表示にして衝突判定確保動作を見えなくする
            _transform.DOMoveY(centerValue.y + upperPosition, duration / 2).SetEase(verticalOutEasingWithParabola).OnComplete(() =>
            {
                _transform.DOMoveY(endValue.y, duration - duration / 2).SetEase(verticalInEasingWithParabola).OnComplete(() =>
                {
                    SetVisible(false); 
                });
            });
                    
            // 角度変化
            if (bulletMoveParam.UseShotAngle)
            {
                var topValue = centerValue + new Vector2(0.0f, upperPosition * 2);
                _setEulerAngle(topValue - startValue);
                _isLauncing = true;
                _isFirstStep = true;
            }
        }

        void _setPosition(IActionExecutorView iExecutorView)
        {
            var view = (SpineView)iExecutorView;
            _transform.position = ((CharacterView)iExecutorView).GetArmPosition();
            SetOrderInLayer(view.GetOrderInLayer());
            _transform.rotation = view.GetTransform().rotation;
            foreach (var unreversible in _unreversibles)
            {
                unreversible.rotation = Quaternion.identity;
            }
        }

        void _setEulerAngle(Vector2 direction)
        {
            var angle = Mathf.Atan2(direction.y,direction.x) * Mathf.Rad2Deg;
            var eulerAngle = Quaternion.AngleAxis(360.0f - angle, Vector3.forward).eulerAngles;
            eulerAngle.y = 180;
            _transform.eulerAngles = eulerAngle;
        }

        public void SetSkin(int skinLevel)
        {
            var skinName = "step" + skinLevel;
            if (_rootSkeletonAnimation.Skeleton.Data.FindSkin(skinName) == null) skinName = "default";
            _setRootSkin(skinName);
        }
        
        public void PauseOn()
        {
            _transform.DOPause();
            _pauseOn();
        }
        public void PauseOff()
        {
            _transform.DOPlay();
            _pauseOff();
        }

        void OnTriggerStay2D(Collider2D collider)
        {
            var targetView = collider.GetComponent<ActionTargetView>();
            if (targetView != null)
            {
                _onHitSubject.OnNext(targetView);
            }
        }
        void OnDestroy()
        {
            if (_onHitSubject != null)
            {
                _onHitSubject.OnCompleted();
                _onStockSubject.OnCompleted();
            }
        }

        [System.Serializable]
        class BulletMoveParam
        {
            [SerializeField] bool _useShotAngle = true;
            [SerializeField] float _velocity = 5.0f;
            [SerializeField] Ease _easing = Ease.Linear;
            [SerializeField] MoveType _moveType = MoveType.Linear;
            [SerializeField] float _upperPositionWithParabola = 0.0f;

            public bool UseShotAngle { get { return _useShotAngle; } }
            public float Velocity { get { return _velocity; } }
            public Ease Easing { get { return _easing; } }
            public MoveType MoveType { get { return _moveType; } }
            public float UpperPositionWithParabola { get { return _upperPositionWithParabola; } }
        }

        public enum MoveType
        {
            Parabola = 1,
            Linear = 2,
        }
    }
}
