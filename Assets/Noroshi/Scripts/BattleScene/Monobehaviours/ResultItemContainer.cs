﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Flaggs.Extensions.Heam;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class ResultItemContainer : UIView
    {
        [SerializeField] UIView _itemUpperContainer;
        [SerializeField] UIView _itemBottomContainer;
        [SerializeField] ResultItemUIView[] _items;

        Noroshi.Core.WebApi.Response.Possession.PossessionObject[] _rewards;

        /// 必要なアセットをロード。
        public IObservable<bool> LoadAssets()
        {
            return _items.Select(ui => ui.LoadSprite()).WhenAll().Select(_ => true);
        }

        /// 報酬をセット。
        public void SetRewards(IEnumerable<Noroshi.Core.WebApi.Response.Possession.PossessionObject> rewards)
        {
            _rewards = rewards.ToArrayWithOptimization();
            for (byte no = 1; no <= _items.Length; no++)
            {
                _items[no - 1].SetActive(no <= _rewards.Length);
                if (no <= _rewards.Length)
                {
                    _items[no - 1].SetReward(_rewards[no - 1]);
                }
                _items[no - 1].SetVisible(false);
            }
        }

        /// アイテムコンテナ表示を試行。
        public IObservable<bool> TryToDisplayItemContainer()
        {
            if ((_itemUpperContainer == null && _itemBottomContainer == null) || _rewards.Length == 0) return Observable.Return(false);
            var itemNum = Mathf.Min(_rewards.Length, _items.Length);
            var itemDisplayObservables = new IObservable<ResultItemUIView>[itemNum];
            for (byte no = 1; no <= itemNum; no++)
            {
                itemDisplayObservables[no - 1] = _displayItem(no);
            }
            return itemDisplayObservables.Concat().Last().Select(_ => true);
        }
        IObservable<ResultItemUIView> _displayItem(byte no)
        {
            return _items[no - 1].PlayAppearanceAnimation()
            .SelectMany(_ => Observable.Timer(System.TimeSpan.FromSeconds(0.1f)))
            .Select(_ => _items[no - 1]);
        }
    }
}