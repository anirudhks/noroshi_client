﻿using UniRx;
using UnityEngine;
using DG.Tweening;
using PossessionCategory = Noroshi.Core.Game.Possession.PossessionCategory;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class ResultItemUIView : UIView 
    {
        [SerializeField] UnityEngine.UI.Image _itemImage;
        [SerializeField] UnityEngine.UI.Image _itemFrameImage;
        [SerializeField] float _maxAppearanceScale = 1.1f;
        [SerializeField] float _appearanceDuration = 0.1f;

        // 通常アイテムサムネイル表示用の全フレーム
        [SerializeField] Sprite[] _itemThumnailFrames;
        // 召喚石サムネイル表示用の全フレーム
        [SerializeField] Sprite[] _soulTnumnailFrames;

        Noroshi.Core.WebApi.Response.Possession.PossessionObject _reward;

        public void SetReward(Noroshi.Core.WebApi.Response.Possession.PossessionObject reward)
        {
            _reward = reward;

            var category = (PossessionCategory)_reward.Category;
            var rarity = _getItemRarrity(category, _reward.ID);
            // アイテムのフレームをカテゴリによって分ける
            var frameSprite = _isSoulOrGearPieceCategory(category) ? _soulTnumnailFrames[rarity - 1] : _itemThumnailFrames[rarity - 1];

            _itemFrameImage.sprite = frameSprite;
        }
        public void SetVisible(bool visible)
        {
            _itemImage.enabled = visible;
            _itemFrameImage.enabled = visible;
        }
        public IObservable<bool> PlayAppearanceAnimation()
        {
            return Observable.Defer(() =>
            {
                var originalScale = _transform.localScale;
                _transform.localScale = originalScale * _maxAppearanceScale;
                SetVisible(true);
                var onComplete = new Subject<bool>();
                _transform.DOScale(originalScale, _appearanceDuration).OnComplete(() =>
                {
                    onComplete.OnNext(true);
                    onComplete.OnCompleted();
                });
                return onComplete.AsObservable();
            });
        }
        public IObservable<Sprite> LoadSprite()
        {
            if (_reward == null) return Observable.Return<Sprite>(null);
            var factory = (Factory)SceneContainer.GetFactory();
            return factory.BuildItemThumbSprite(_reward.ID).Do(s => _itemImage.sprite = s);
        }


        // カテゴリがソウルかアイテムのかけらかを判定する
        bool _isSoulOrGearPieceCategory(PossessionCategory category)
        {
            return category == PossessionCategory.Soul || category == PossessionCategory.GearPiece;
        }

        /// <summary>
        /// アイテムのレアリティを取得する
        /// </summary>
        /// <returns>対象アイテムのレアリティ</returns>
        /// <param name="category">対象アイテムのカテゴリ</param>
        /// <param name="id">対象のアイテムID</param>
        uint _getItemRarrity(PossessionCategory category, uint id)
        {
            var itemMaster = GlobalContainer.MasterManager.ItemMaster;
            switch (category)
            {
            case PossessionCategory.Drug:
                return itemMaster.GetDrug(id).Rarity;
            case PossessionCategory.ExchangeCashGift:
                return itemMaster.GetExchangeCashGift(id).Rarity;
            case PossessionCategory.Gear:
                return itemMaster.GetGear(id).Rarity;
            case PossessionCategory.GearEnchantMaterial:
                return itemMaster.GetGearEnchantMaterial(id).Rarity;
            case PossessionCategory.GearPiece:
                return itemMaster.GetGearPiece(id).Rarity;
            case PossessionCategory.MiscItem:
                GlobalContainer.Logger.Debug("<color=yellow>Get MiscItem</color>");
                return 1;
            case PossessionCategory.RaidTicket:
                return itemMaster.GetRaidTicket(id).Rarity;
            case PossessionCategory.Soul:
                return itemMaster.GetSoul(id).Rarity;
            default:
                GlobalContainer.Logger.Debug("<color=red>category Not Found : ResultItem</color>");
                return 1;
            }
        }

        void OnDestory()
        {
            _itemImage.sprite = null;
        }
    }
}
