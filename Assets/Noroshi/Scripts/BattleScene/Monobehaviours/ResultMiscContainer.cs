﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Flaggs.Extensions.Heam;
using System;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class ResultMiscContainer : UIView
    {
        /// 数値カウントアップ演出の更新間隔。この値でうまく割れない方がそれっぽい演出になるので変な値を。
        static readonly TimeSpan COUNT_UP_INTERVAL = TimeSpan.FromSeconds(0.07123456789);

        // Miscのアクティブ制御用オブジェクト
        [SerializeField] UIView _miscActiveObject;
        [SerializeField] GaugeView _playerExpGaugeView;
        [SerializeField] Text _playerLevelText;
        [SerializeField] Text _playerAddExpText;
        [SerializeField] Text _goldText;
        [SerializeField] float _countUpDuration = 1.0f;

        readonly float _progressionSpeed = 2.0f;
        float _currentExpRatio = 0.0f;
        uint _playerExp;
        uint _gold;
        ushort _levelUpNum = 0;
        ushort _previousPlayerLevel = 0;
        bool _isPossessAddPlayerExp = false;

        new void Awake()
        {
            base.Awake();
            _playerLevelText.text = string.Empty;
            _playerAddExpText.text = "0";
            _goldText.text = "0";
        }

        void Start()
        {
            _miscActiveObject.SetActive(false);
        }

        string _getLevelText(ushort level)
        {
            return GlobalContainer.LocalizationManager.GetText("UI.Heading.Level") + "." + level.ToString();
        }

        // 表示に必要な情報を一括セット
        public void SetAddPlayerExpResult(Core.WebApi.Response.Players.AddPlayerExpResult addPlayerExpResult)
        {
            if (addPlayerExpResult == null)
            {
                SetActive(false);
                _isPossessAddPlayerExp = false;
                return;
            }
            SetActive(true);
            _isPossessAddPlayerExp = true;
            _previousPlayerLevel = addPlayerExpResult.PreviousPlayerLevel;
            _currentExpRatio = addPlayerExpResult.CurrentExpRatio;
            _playerExpGaugeView.SetValue(addPlayerExpResult.PreviousExpRatio);
            _levelUpNum = (ushort)(addPlayerExpResult.CurrentPlayerLevel - addPlayerExpResult.PreviousPlayerLevel);

            _playerLevelText.text = _getLevelText((ushort)(_previousPlayerLevel));
        }
        public void SetPlayerExp(uint playerExp)
        {
            _playerExp = playerExp;
        }
        public void SetGold(uint gold)
        {
            _gold = gold;
        }

        // カウントアップアニメーションとスライダーのアニメーションを同時に再生開始し、終了まで待つ
        public IObservable<bool> PlayAllCountUpAndSliderAnimation()
        {
            if (!_isPossessAddPlayerExp) return Observable.Return<bool>(false);
            _miscActiveObject.SetActive(true);
            return Observable.WhenAll(
                _playCountUpPlayerExpAndGold(),
                _playSliderAnimation()
            )
            .Select(_ => true);
        }

        /// プレイヤー経験値、ゴールドカウントアップ演出を再生する。
        IObservable<bool> _playCountUpPlayerExpAndGold()
        {
            var intervalNum = (int)(_countUpDuration / COUNT_UP_INTERVAL.TotalSeconds);
            var currentPlayerExp = 0f;
            var deltaPlayerExp = (float)_playerExp / intervalNum;
            var currentGold = 0f;
            var deltaGold = (float)_gold / intervalNum;
            return Observable.Interval(COUNT_UP_INTERVAL)
            .TakeWhile(_ => currentPlayerExp < _playerExp || currentGold < _gold)
            .Do(_ =>
            {
                currentPlayerExp += deltaPlayerExp;
                currentGold += deltaGold;
                if (currentPlayerExp > _playerExp) currentPlayerExp = _playerExp;
                if (currentGold > _gold) currentGold = _gold;
                _playerAddExpText.text = string.Format("+{0:#,##0}", (int)currentPlayerExp);
                _goldText.text = string.Format("+{0:#,##0}", (int)currentGold);
            })
            .LastOrDefault()
            .Do(_ =>
            {
                _playerAddExpText.text = string.Format("+{0:#,##0}", _playerExp);
                _goldText.text = string.Format("+{0:#,##0}", _gold);
            })
            .Select(_ => true);
        }

        IObservable<bool> _playSliderAnimation()
        {
            if (_playerExpGaugeView == null) return Observable.Return(false);
            return Observable.Defer(() =>
            {
                _playerExpGaugeView.SetActive(true);
                var sumExpRate = _currentExpRatio + (1.0f * _levelUpNum);
                return _playerExpGaugeView.SetAnimationValue(sumExpRate, _progressionSpeed, true)
                .Do(isNext => 
                {
                    if (isNext)
                    {
                        _playerLevelText.text = _getLevelText((ushort)(_previousPlayerLevel + 1));
                        _previousPlayerLevel++;
                    }
                });
            });
        }
    }
}