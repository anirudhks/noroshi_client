﻿using UnityEngine;
using UniLinq;
using UniRx;
using UniRx.Triggers;
using System;
using DG.Tweening;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class FieldLayer : Noroshi.MonoBehaviours.MonoBehaviour
    {
        enum BackgroundPositionType
        {
            Foreground = 1,
            Ground,
            Background,
        }

        [SerializeField] float _moveRatio = 0;
        [SerializeField] Transform[] _loopTargetsTranform = null;
        [Header("---1Waveのサイズでループを行うか")]
        [SerializeField] bool _isWaveFieldSize = true;
        [Header("---画像のサイズ(ピクセルで)")]
        [SerializeField] float _loopSizeWithPixel = 1280.0f;
        [SerializeField] BackgroundPositionType _backgroundPositionType = BackgroundPositionType.Ground;
        Transform _mainCameraTransform;
        Vector3 _initialPositionDiff;
        float _loopSize = 0.0f;

        public void Initialize()
        {
            _loopSize = _isWaveFieldSize ? WaveField.HORIZONTAL_LENGTH : _loopSizeWithPixel / 100.0f;
            _mainCameraTransform = UnityEngine.Camera.main.transform;
            _initialPositionDiff = _transform.position - _mainCameraTransform.position;
        }

        // Wave切り替え開始時に処理する
        public void EnterSwitchWave(byte waveNo, float duration)
        {
            // WaveNoを受け取りカメラの移動と同様の速度でアニメーション移動するようにする
            // 移動終了地点の取得
            var moveEndX = WaveField.GetPositionX(waveNo) * (1 - _moveRatio) + _initialPositionDiff.x;
            _transform.DOMoveX(moveEndX, duration).SetEase(Ease.InOutQuad);
        }

        // Foregroundのオブジェクトのみアクティブを操作する
        public void SetForegroundVisible(bool isVisible)
        {
            if (_backgroundPositionType == BackgroundPositionType.Foreground)
            {
                SetActive(isVisible);
            }
        }

        public void AdjustAfterSwitchWave()
        {
            var loopTransform = _loopTargetsTranform.OrderBy(x => x.transform.localPosition.x).FirstOrDefault();
            if (loopTransform == null) return;
            var cameraDistance = 0.0f;
            switch(_backgroundPositionType)
            {
            case BackgroundPositionType.Foreground:
                var width = _loopSize + (_loopSize / 2.0f);
                foreach (var ts in _loopTargetsTranform)
                {
                    var posX = ts.localPosition.x + width + (_loopSize / 2);
                    ts.localPosition = new Vector3(posX, ts.localPosition.y, ts.localPosition.z);
                }
                break;
            case BackgroundPositionType.Ground:
                loopTransform.localPosition = new Vector3(_mainCameraTransform.position.x + _loopSize,
                                                          loopTransform.localPosition.y, loopTransform.localPosition.z);
                break;
            case BackgroundPositionType.Background:
                cameraDistance = _mainCameraTransform.position.x - (_transform.position.x + loopTransform.localPosition.x);
                if (cameraDistance >= _loopSize)
                {
                    loopTransform.localPosition = new Vector3(loopTransform.localPosition.x + (_loopSize * 2.0f),
                                                              loopTransform.localPosition.y, loopTransform.localPosition.z);
                }
                break;
            default :
                throw new ArgumentOutOfRangeException();
            }
        }
    }
}
