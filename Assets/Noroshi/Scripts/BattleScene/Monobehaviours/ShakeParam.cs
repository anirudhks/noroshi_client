﻿using UnityEngine;
using System.Collections;

namespace Noroshi.BattleScene.MonoBehaviours
{
    [System.Serializable]
    public class ShakeParam
    {
        [Header("---揺れ幅の最大値")]
        [SerializeField] float _shakeMoveMax;
        [Header("---繰り返し回数(1で1回再生となる)")]
        [SerializeField] int _loopNum;
        [Header("---1回分の再生時間")]
        [SerializeField] float _duration;

        public float ShakeMoveMax { get { return _shakeMoveMax; } }
        public int LoopNum { get { return _loopNum > 0 ? _loopNum : 1; } }
        public float Duration { get { return _duration; } }
    }
}
