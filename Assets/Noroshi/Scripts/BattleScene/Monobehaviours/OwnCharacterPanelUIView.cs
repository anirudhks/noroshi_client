﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;
using Noroshi.Core.Game.Sound;
using Noroshi.BattleScene.UI;
using Noroshi.BattleScene.Sound;

using DG.Tweening;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class OwnCharacterPanelUIView : UIView, IOwnCharacterPanelUIView
    {
        const string LOCALIZATIOIN_ACTION_NAME_KEY = "Master.Action.Action.{0}.Name";
        const float REDUCTION_ICON_ANIMATION_INTERVAL_TIME = 0.066f;

        enum ActiveActionStateType
        {
            None = 0,    // 発動不可状態
            Reservation, // 予約待ち状態
            Invocation,  // 発動中状態(暗転中)
        }
        
        enum FrameSpriteTypes
        {
            NormalFrame = 0,
            AlertFrame,
            DeadFrame,
        }

        enum SliderEnergySpriteTypes
        {
            SkillNormal = 0,
            Dead
        }

        enum SliderHpSpriteTypes
        {
            HpAlert1 = 0,
            HpAlert2,
            HpNormal
        }

        enum EnergyBackgroundTypes
        {
            Max = 0,
            Charge
        }

        enum HpFontTypes
        {
            Normal = 0,
            Alert
        }
        enum AccumulationType
        {
            None = 0,
            Charge = 1,
        }

        [SerializeField] Image _characterImage;
        [SerializeField] Image _skillBackgroundEffectImage;
        [Header("---通常時と死亡時を表示する")]
        [SerializeField] Image _normalFrameImage;
        [Header("---瀕死時を表示する")]
        [SerializeField] Image _alertEffectFrameImage;
        [Header("---スキルMax時の明滅用")]
        [SerializeField] Image _skillMaxEffectFrameImage;
        [SerializeField] Image _skillEffectrFrameImage;

        [SerializeField] GaugeView _hpGaude;
        [SerializeField] GaugeView _energyGauge;

        [SerializeField] RectTransform _moveTransform;

        [SerializeField] UIView _skillEffectGroupView;

        [SerializeField] ImageSquareMoveUI _frameLight1Animation;
        [SerializeField] ImageSquareMoveUI _frameLight2Animation;

        [SerializeField] Text _skillLvName;
        [SerializeField] Text _hpText = null;

        [SerializeField] SkillStartingEffectAnimation _skillStartingEffectAnimation = null;

        [SerializeField] StatusBoostIconView _statusBoostIconView;

        [SerializeField] Font[] _hpFonts;

        [SerializeField] Image _reductionIconImage;
        [SerializeField] Image _reductionEffectGaugeImage;
        [SerializeField] Image _enterActiveActionEffectFrameImage;
        [SerializeField] Outline _skillNameOutline;

        [SerializeField] UIView _accumulationGroupView;

        [Header("---交換するスプライト")]
        [SerializeField] Sprite[] _changeFrameSprites;
        [SerializeField] Sprite[] _changeHpSliderSprites;
        [SerializeField] Sprite[] _changeEnergySliderSprites;
        [SerializeField] Sprite[] _animationReductionIcons;
        [SerializeField] Sprite[] _backgroundEffectImages;

        readonly float _buttonMoveValue = 35.0f;
        readonly float _movueDuration = 0.25f;
        readonly float _alphaAnimDuration = 0.5f;
        readonly float _hpAlertValue = 0.3f;
        Subject<bool> _onClickSubject = new Subject<bool>();
        protected Subject<SoundEvent> _onCommandSoundSubject = new Subject<SoundEvent>();
        // チャージアクションにてチャージ中にボタンを押した時に OnNext される Subject
        Subject<bool> _onAccumulationClickSubject = new Subject<bool>();

        Image _hpSliderImage = null;
        Image _hpSliderFillEffectImage = null;
        Image _energySliderImage = null;
        Image _energySliderFillEffectImage = null;

        Vector2 _defaultAnchorPos;
        Color _endColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        bool _isAvailable = false;
        bool _isDrawSkillEffectAnimation = true;
        bool _isButtonPositionUp = false;
        ActiveActionStateType _activeActionStateType = ActiveActionStateType.None;
        RectTransform _reductionIconRectTransform;
        RectTransform _energyFillImageRectTransform;
        CompositeDisposable _reductionIconDisposable = new CompositeDisposable();
        ActiveActionType _activeActionType;
        Vector3 _defaultReductionIconLocalPosition;

        AccumulationType _accumulationType = AccumulationType.None;

        new void Awake()
        {
            base.Awake();
            _hpText.text = "0";

            ToggleActiveActionAvailable(false);

            _defaultAnchorPos = _moveTransform.anchoredPosition;
            _hpSliderImage = _hpGaude.FillImage;
            _hpSliderFillEffectImage = _hpGaude.FillAlertImage;
            _energySliderImage = _energyGauge.FillImage;
            _energySliderFillEffectImage = _energyGauge.FillAlertImage;
            _energyFillImageRectTransform = _energySliderImage.rectTransform;
            _reductionIconRectTransform = _reductionIconImage.rectTransform;
            _defaultReductionIconLocalPosition = _reductionIconRectTransform.localPosition;
            _reductionIconImage.enabled = false;
            _reductionEffectGaugeImage.enabled = false;
            _skillLvName.enabled = false;
            _frameLight1Animation.SetVisible(false);
            _frameLight2Animation.SetVisible(false);
        }

        void Start()
        {
            _setSkillEffectGroupViewActive(false);
            _setSkillChargeEffectImageActive(false);

            _hpGaude.GetOnValueChangeObservalbe()
                .Select(n => n <= _hpAlertValue && n > 0.0f)
                .DistinctUntilChanged()
                .Subscribe(flg => { _setupHpSpriteAnimation(_hpSliderImage, flg); })
                .AddTo(_hpGaude);
        }

        void OnDestroy()
        {
            if (_reductionIconDisposable != null) _reductionIconDisposable.Dispose();
        }

        void _setupHpSpriteAnimation(Image image, bool flg)
        {
            _hpSliderFillEffectImage.enabled = flg;
            _alertEffectFrameImage.enabled = flg;
            if (flg)
            {
                image.sprite = _changeHpSliderSprites[(int)SliderHpSpriteTypes.HpAlert1];
                _imageAlphaAnimationPlay(_hpSliderFillEffectImage);
                _imageAlphaAnimationPlay(_alertEffectFrameImage);

                _hpText.font = _hpFonts[(int)HpFontTypes.Alert];
            }
            else
            {
                image.sprite = _changeHpSliderSprites[(int)SliderHpSpriteTypes.HpNormal];
                _imageAlphaAnimationKill(_hpSliderFillEffectImage);
                _imageAlphaAnimationKill(_alertEffectFrameImage);
                
                _hpText.font = _hpFonts[(int)HpFontTypes.Normal];
            }
        }
        
        void _setupEnergySpriteAnimation(bool available)
        {
            if (_energySliderFillEffectImage == null) return;
            _setSkillEffectGroupViewActive(available);
            if (available)
            {
                if (!_isButtonPositionUp)
                {
                    _isButtonPositionUp = true;
                    _moveTransform.DOAnchorPos(new Vector2(_defaultAnchorPos.x, _defaultAnchorPos.y + _buttonMoveValue), _movueDuration)
                                  .SetEase(Ease.OutBack)
                                  .OnComplete(() => 
                                  {
                                      _skillLvName.enabled = true;
                                      _skillNameOutline.DOColor(Color.black, 0.1f);
                                  });
                }
                _imageAlphaAnimationPlay(_skillMaxEffectFrameImage);
            }
            else
            {
                // アクティブアクション発動後の暗転中とアクティブアクション予約中はボタンを下げない
                if (_activeActionStateType == ActiveActionStateType.None) _moveDownButton();

                _imageAlphaAnimationKill(_skillMaxEffectFrameImage);
            }
        }

        void _imageAlphaAnimationPlay(Image image)
        {
            _imageAlphaAnimationKill(image);
            image.DOColor(_endColor, _alphaAnimDuration).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
        }

        void _imageAlphaAnimationKill(Image image)
        {
            image.DOKill();
            image.color = Color.white;
        }

        void _setSkillChargeEffectImageActive(bool isActive)
        {
            isActive = _activeActionStateType == ActiveActionStateType.Invocation ? isActive : false;
            _skillBackgroundEffectImage.gameObject.SetActive(isActive);
            _skillBackgroundEffectImage.sprite = _backgroundEffectImages[(int)EnergyBackgroundTypes.Charge];
            _accumulationGroupView.SetActive(isActive);
            _frameLight1Animation.SetVisible(isActive);
            _frameLight2Animation.SetVisible(isActive);
            if (isActive)
            {
                _frameLight1Animation.AnimationPlay();
                _frameLight2Animation.AnimationPlay();
            }
            else
            {
                _frameLight1Animation.AnimationPause();
                _frameLight2Animation.AnimationPause();
            }
        }

        void _setSkillEffectGroupViewActive(bool isActive)
        {
            isActive = _activeActionStateType == ActiveActionStateType.None ? isActive : false;
            _skillEffectGroupView.SetActive(isActive);
            _skillBackgroundEffectImage.gameObject.SetActive(isActive);
            _skillBackgroundEffectImage.sprite = _backgroundEffectImages[(int)EnergyBackgroundTypes.Max];
        }

        void _setDeadSprite(bool isDead)
        {
            if (isDead)
            {
                _setupEnergySpriteAnimation(false);
                _characterImage.color = Color.gray;
            }
            else
            {
                _characterImage.color = Color.white;
            }

            _energySliderImage.sprite = isDead ? _changeEnergySliderSprites[(int)SliderEnergySpriteTypes.Dead] : _changeEnergySliderSprites[(int)SliderEnergySpriteTypes.SkillNormal];
            _normalFrameImage.sprite = isDead ? _changeFrameSprites[(int)FrameSpriteTypes.DeadFrame] : _changeFrameSprites[(int)FrameSpriteTypes.NormalFrame];
        }

        void _moveDownButton()
        {
            _isButtonPositionUp = false;
            _moveTransform.DOAnchorPos(_defaultAnchorPos, _movueDuration).SetEase(Ease.InBack);
            _skillLvName.enabled = false;
            _skillNameOutline.effectColor = Color.white;
        }

        public IObservable<bool> GetOnClickObservable()
        {
            return _onClickSubject.AsObservable();
        }

        public IObservable<bool> GetOnAccumulationClickObservable()
        {
            return _onAccumulationClickSubject.AsObservable();
        }

        /// サウンド操作時に操作内容が OnNext される Observable を取得。
        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _onCommandSoundSubject.AsObservable();
        }

        public void ChangeHP(ChangeableValueEvent hpEvent)
        {
            _hpText.text = hpEvent.Current.ToString();
            _hpGaude.SetValue((float)hpEvent.Current / hpEvent.Max);
            var isDead = _hpGaude.Value <= 0.0f;
            _hpText.color = isDead ? Color.gray : Color.white;
            _setDeadSprite(isDead);
            if (isDead)
            {
                _imageAlphaAnimationKill(_hpSliderFillEffectImage);
                _moveDownButton();
            }
        }
        public void ChangeEnergy(ChangeableValueEvent energyEvent)
        {
            var nowValue = (float)energyEvent.Current / energyEvent.Max;
            _energyGauge.SetValue(nowValue);
            // チャージ中にエネルギーが0になった場合は発動に失敗したので、表示周りをリセットする
            if (nowValue <= 0 && _accumulationType == AccumulationType.Charge) ResetEffectAnimation();

            bool isEnergyMax = nowValue >= 1.0f;
            _energySliderFillEffectImage.enabled = isEnergyMax;
            if (isEnergyMax)
            {
                _imageAlphaAnimationPlay(_energySliderFillEffectImage);
            }
            else
            {
                _imageAlphaAnimationKill(_energySliderFillEffectImage);
            }
        }

        public void ToggleActiveActionAvailable(bool available)
        {
            if (available && _isDrawSkillEffectAnimation)
            {
                _isDrawSkillEffectAnimation = false;
                // アクティブアクションチャージ完了の効果音
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    SoundID = SoundConstant.BATTLE_AVAILABLE_ACTIVE_ACTION_SOUND_ID,
                    Command = SoundCommand.Play,
                });
                _skillStartingEffectAnimation.PlayAnimation();
            }
            _isAvailable = available;
            _setupEnergySpriteAnimation(_isAvailable);
        }
        
        public void Initialize(uint? activeActionId, uint initializeHp, uint maxHp, int initializeEnergy, int maxEnergy, ActiveActionType? activeActionType)
        {
            _hpText.text = initializeHp.ToString();
            _hpGaude.SetValue((float)initializeHp / maxHp);
            _energyGauge.SetValue((float)initializeEnergy / maxEnergy);
            _skillLvName.text = activeActionId.HasValue ? 
                                GlobalContainer.LocalizationManager.GetText(string.Format(LOCALIZATIOIN_ACTION_NAME_KEY, activeActionId.Value)) :
                                string.Empty;
            _activeActionType = activeActionType.HasValue ? activeActionType.Value : ActiveActionType.Normal;
        }

        public void ChangeStatusBoost(CharacterStatusBoostEvent boostEvent)
        {
            _statusBoostIconView.SetStatusBoostEvent(boostEvent.Type, boostEvent.StatusBoosterFactor);
        }

        public void FinishActiveAction()
        {
            // チャージ系以外はここでボタンを下げる
            if (_activeActionType != ActiveActionType.Accumulation)
            {
                _activeActionStateType = ActiveActionStateType.None;
                _isDrawSkillEffectAnimation = true;
                _moveDownButton();
            }
        }

        public void SetSprite(Sprite sprite)
        {
            _characterImage.sprite = sprite;
        }

        // アクティブアクション発動時に呼ばれる
        public void EnterActiveAction()
        {
            // 予約状態から実行状態へ
            _activeActionStateType = ActiveActionStateType.Invocation;
            // エネルギー減少系の場合はここで専用アニメーションの再生を開始する
            if (_activeActionType == ActiveActionType.ReductionGauge)
            {
                _playReductionIconAnimation();
            }
            // チャージ系スキル時は一回目ボタンを押した時にフレームを切り替える
            else if (_activeActionType == ActiveActionType.Accumulation)
            {
                _setSkillChargeEffectImageActive(true);
                _frameLight1Animation.AccumulationStart();
                _frameLight2Animation.AccumulationStart();
            }
        }
        // アクティブアクションの発動を予約する
        public void ReserveActiveAction()
        {
            if(_activeActionType == ActiveActionType.Accumulation) _accumulationType = AccumulationType.Charge;
            if (_activeActionStateType != ActiveActionStateType.None) return;
            _activeActionStateType = ActiveActionStateType.Reservation;
            // 予約時にMAX時の表示を消す
            _setSkillEffectGroupViewActive(false);
            _setSkillChargeEffectImageActive(false);
            _playEnterActiveActionFrameEffect();
        }

        // 再生中だった減少系、チャージ時に表示していたエフェクト周りを止める
        public void ResetEffectAnimation()
        {
            _resetSkillName();
            _accumulationType = AccumulationType.None;
            _activeActionStateType = ActiveActionStateType.None;
            _reductionIconDisposable.Clear();
            _reductionIconImage.enabled = false;
            _reductionEffectGaugeImage.DOKill();
            _reductionEffectGaugeImage.enabled = false;
            _setSkillChargeEffectImageActive(false);
            _moveDownButton();
        }

        void _resetSkillName()
        {
            _skillLvName.DOKill();
            _skillLvName.color = Color.white;
        }

        public void MaxAccumulation()
        {
            _resetSkillName();
            // チャージがMaxまで溜まった時に呼ばれる処理
            _skillLvName.color = Color.red;
            _skillLvName.DOColor(Color.white, 0.25f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.OutQuad);
        }

        // ゲージ減少系スキル発動時の演出再生開始
        void _playReductionIconAnimation()
        {
            var reductionIconNum = 0;
            _reductionIconImage.enabled = true;
            _reductionEffectGaugeImage.enabled = true;
            _reductionEffectGaugeImage.color = Color.white;
            _reductionIconDisposable.Clear();
            _reductionIconRectTransform.localPosition = _defaultReductionIconLocalPosition;
            // エフェクトアイコンのアニメーション部分
            SceneContainer.GetTimeHandler().Interval(REDUCTION_ICON_ANIMATION_INTERVAL_TIME)
            .Do(_ => 
            {
                ++reductionIconNum;
                if (reductionIconNum >= _animationReductionIcons.Length) reductionIconNum = 0;
                _reductionIconImage.sprite = _animationReductionIcons[reductionIconNum];
            })
            .TakeWhile(_ => 
            {
                // エネルギーが0になったらエフェクトアイコンも同時に消す
                var checkFinish = _energyGauge.Value <= 0.0f;
                if (checkFinish) 
                {
                    _reductionIconImage.enabled = false;
                    _reductionEffectGaugeImage.DOKill();
                    _reductionEffectGaugeImage.enabled = false;
                }
                return !checkFinish;
            })
            .Subscribe(_ => {}).AddTo(_reductionIconDisposable);

            _reductionEffectGaugeImage.DOColor(new Color(1, 1, 1, 0), 0.2f).SetLoops(-1, LoopType.Yoyo);

            // 減少ゲージの端に表示するエフェクトアイコンの移動処理
            // 基本はゲージの移動量を取得して、更新する
            _energyGauge.GetOnValueChangeObservalbe().Select(_ => _energyFillImageRectTransform.localPosition)
            .Buffer(2, 1).Where(localPositions => localPositions.Count > 1)
            .Do(localPositions => 
            {
                var diffX = localPositions[1].x - localPositions[0].x;
                _reductionIconRectTransform.localPosition = new Vector3(_reductionIconRectTransform.localPosition.x + diffX,
                                                                        _defaultReductionIconLocalPosition.y,
                                                                        _defaultReductionIconLocalPosition.z);
            })
            .Subscribe(_ => {}).AddTo(_reductionIconDisposable);
        }

        // チャージ系アクティブアクションの攻撃開始
        public void ExecuteAccumulationAction()
        {
            _activeActionStateType = ActiveActionStateType.None;
            _accumulationType = AccumulationType.None;
            _isDrawSkillEffectAnimation = true;
            _setSkillChargeEffectImageActive(false);
        }

        // ボタンを押した時にエフェクトを表示させる
        void _playEnterActiveActionFrameEffect()
        {
            _enterActiveActionEffectFrameImage.enabled = true;
            _enterActiveActionEffectFrameImage.DOColor(new Color(1, 1, 1, 0), 0.3f).SetEase(Ease.OutCubic);
            _enterActiveActionEffectFrameImage.transform.DOScale(Vector2.one * 1.5f, 0.3f).SetEase(Ease.OutCubic).OnComplete(() => 
            {
                _enterActiveActionEffectFrameImage.color = Color.white;
                _enterActiveActionEffectFrameImage.transform.localScale = Vector3.one;
                _enterActiveActionEffectFrameImage.enabled = false;
            });
        }

        // ボタンを押した際、全てのボタンより前面に表示させるため、ここで親オブジェクトを変更させる
        public void SetParentEnterActiveActionEffectFrame(Transform parent)
        {
            _enterActiveActionEffectFrameImage.transform.SetParent(parent, false);
            // このタイミングで座標を更新しても反映されないので、座標に変化が起きるまで監視
            this.UpdateAsObservable().Select(_ => _rectTransform.anchoredPosition)
                .DistinctUntilChanged()
                .TakeWhile(anchorPos => anchorPos.x != 0.0f)
                .Subscribe(_ => _enterActiveActionEffectFrameImage.rectTransform.anchoredPosition = new Vector2(_rectTransform.anchoredPosition.x, -20)).AddTo(this);
            _enterActiveActionEffectFrameImage.enabled = false;
        }

        /// クリックメソッド。UI から結びつけられる。
        public void Click()
        {
            if (_accumulationType == AccumulationType.None)
            {
                _onClickSubject.OnNext(true);
            }
            else if (_accumulationType == AccumulationType.Charge)
            {
                _onAccumulationClickSubject.OnNext(true);
            }
        }
    }
}
