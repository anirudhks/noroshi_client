﻿using UnityEngine;
using UniRx;
using Noroshi.MonoBehaviours;
using System;
using DG.Tweening;

namespace Noroshi.BattleScene.MonoBehaviours
{
    // 味方キャラクターアクティブアクション発動時用UI
    public class ActiveActionDescriptionUIView : UIView
    {
        const string LOCALIZATIOIN_ACTION_DESCRIPTION_KEY = "Master.Action.Action.{0}.Description";
        const float ACTIVE_SKILL_VIEW_ENABLE_TIME = 2.0f;

        [SerializeField] UnityEngine.UI.Text _descriptionText = null;
        [SerializeField] UnityEngine.UI.Image _activeSkillTitleImage = null;
        [SerializeField] RectTransform _activeSkillBgRectTransform = null;
        IDisposable _disposable = null;
        Tweener _scaleTweener = null;

        new void Awake()
        {
            base.Awake();
            SetActive(false);
        }

        void OnDestroy()
        {
            if (_scaleTweener != null) _scaleTweener.Kill();
            if (_disposable != null) _disposable.Dispose();
        }

        void _disableItem()
        {
            _descriptionText.enabled = false;
            _activeSkillTitleImage.enabled = false;
            _scaleTweener = _activeSkillBgRectTransform.DOScaleY(0.0f, 0.2f).SetEase(Ease.InCubic).OnComplete(() => SetActive(false));
        }

        void _enableItem()
        {
            SetActive(true);
            _descriptionText.enabled = true;
            _activeSkillTitleImage.enabled = true;
            _activeSkillBgRectTransform.localScale = Vector3.one;
        }

        public void SetActiveSkillDescription(uint? activeSkillId)
        {
            if (!activeSkillId.HasValue) return;
            _enableItem();
            var descriptionKey = string.Format(LOCALIZATIOIN_ACTION_DESCRIPTION_KEY, activeSkillId.Value);
            _descriptionText.text = GlobalContainer.LocalizationManager.GetText(descriptionKey);
            if (_disposable != null) _disposable.Dispose();
            if (_scaleTweener != null) _scaleTweener.Kill();
            _disposable = SceneContainer.GetTimeHandler().Timer(ACTIVE_SKILL_VIEW_ENABLE_TIME)
            .Subscribe(_ => _disableItem());
        }
    }
}
