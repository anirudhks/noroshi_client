﻿using UniRx;
using UnityEngine;
using DG.Tweening;
using Noroshi.Grid;
using System;
using Noroshi.Core.Game.Battle;
using Noroshi.BattleScene.Sound;
using Noroshi.Core.Game.Sound;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class StoryUIView : UIView, UI.IStoryUIView
    {
        const float PAGE_ICON_VISIBLE_WAIT_TIME = 0.6f;

        public enum CharacterShakeType
        {
            Vertical = 0,
            Horizontal,
            Camera,
        }

        [SerializeField] UnityEngine.UI.Text _text;
        [SerializeField] RectTransform _topLine;
        [SerializeField] RectTransform _bottomLine;
        [SerializeField] float _activeCharacterPositionX = -2.5f;
        [SerializeField] float _inactiveCharacterPositionX = -7.5f;
        [SerializeField] float _lineMoveDuration = 1f;
        [SerializeField] float _characterViewSlideDuration = 0.3f;
        [SerializeField] UnityEngine.UI.Text _characterName;
        [SerializeField] ShakeParam _shakeScreen;
        [SerializeField] ShakeParam _shakeScreenLinkedCharacter;
        [SerializeField] ShakeParam _shakeVerticalParam;
        [SerializeField] ShakeParam _shakeHorizontalParam;
        [SerializeField] UnityEngine.UI.Image _whiteOutImage;

        [SerializeField] UnityEngine.RectTransform _pageIconRectTransform;
        [SerializeField] UnityEngine.GameObject _pageIconGroup;

        bool _clickable = false;
        Subject<bool> _onClickSubject;
        Vector2 _topLinePosition;
        Vector2 _bottomLinePosition;
        Vector2 _topLineHidePosition;
        Vector2 _bottomLineHidePosition;

        ICharacterView _currentOwnCharacterView;
        ICharacterView _currentEnemyCharacterView;

        Subject<bool> _onClickSkipButtonSubject = new Subject<bool>();
        Subject<SoundEvent> _onCommandSoundSubjet = new Subject<SoundEvent>();

        bool _isWipeOutOwnCharacter = false;
        bool _isWipeOutEnemyCharacter = false;
        bool _isFirstSkipMessage = false;
        // キャラクターの移動関連のアクションが終了した時trueになる(ワイプアウトは省く)
        bool _isFixCharacterActing = false;

        Sequence _doTweenOwnSequence = null;
        Sequence _doTweenEnemySequence = null;
        Sequence _doTweenScreenSequence = null;

        IDisposable _screenShakeDisposable = null;
        IDisposable _iconTimerDisposable = null;

        StoryLayoutType _storyLayoutType;

        new void Awake()
        {
            base.Awake();
            _topLinePosition = _topLine.anchoredPosition;
            _bottomLinePosition = _bottomLine.anchoredPosition;
            _topLineHidePosition = _topLine.anchoredPosition + Vector2.up * _topLine.rect.height;
            _bottomLineHidePosition = _bottomLine.anchoredPosition + Vector2.down * _bottomLine.rect.height;
            SetActive(false);
        }

        public IObservable<bool> GetOnClickSkipButtonObservable()
        {
            return _onClickSkipButtonSubject.AsObservable();
        }
        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _onCommandSoundSubjet.AsObservable();
        }

        void _setCharacterViewUIPosition(ICharacterView characterView)
        {
            characterView.SetUpForUI(CenterPositionForUIType.Story);
            ((CharacterView)characterView).SetLocalPosition(_getStoryLayoutPosition(characterView));
        }

        void _playPageIconAnimation()
        {
            _pageIconGroup.SetActive(true);
            _pageIconRectTransform.DOLocalMoveY(5, 0.25f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.OutCubic);
        }
        void _stopPageIconAnimation()
        {
            _pageIconRectTransform.DOKill();
            _pageIconRectTransform.localPosition = Vector2.zero;
            _pageIconGroup.SetActive(false);
        }

        bool _setStoryLayoutType(StoryLayoutType? storyLayoutType)
        {
            var isLayoutChange = false;
            if (storyLayoutType.HasValue)
            {
                // 初回以降レイアウトに変化が発生した場合フラグが立つ
                if (!_isFirstSkipMessage) isLayoutChange = _storyLayoutType != storyLayoutType.Value;
                _storyLayoutType = storyLayoutType.Value;
            }
            return isLayoutChange;
        }

        public IObservable<bool> Begin()
        {
            SetActive(true);
            _topLine.anchoredPosition = _topLineHidePosition;
            _topLine.DOAnchorPos(_topLinePosition, _lineMoveDuration).OnComplete(() =>
            {
                _clickable = true;
                _isFirstSkipMessage = true;
                _nextMessage();
            });

            _bottomLine.anchoredPosition = _bottomLineHidePosition;
            _bottomLine.DOAnchorPos(_bottomLinePosition, _lineMoveDuration);

            if (_onClickSkipButtonSubject == null) _onClickSkipButtonSubject = new Subject<bool>();
            var onClickSubject = new Subject<bool>();
            _onClickSubject = onClickSubject;
            return onClickSubject.AsObservable();
        }

        public IObservable<UI.IStoryUIView> GoNext(StoryLayoutType? storyLayoutType,
                                                   ICharacterView ownCharacterView, StoryCharacterActingType? ownCharacterActingType, StoryCharacterAnimationType? ownCharacterAnimationType,
                                                   ICharacterView enemyCharacterView, StoryCharacterActingType? enemyCharacterActingType, StoryCharacterAnimationType? enemyCharacterAnimationType,
                                                   byte ownSkinLevel, byte enemySkinLevel, string textKey, string characterNameTextKey)
        {
            _isFixCharacterActing = false;
            var isLayoutChange = _setStoryLayoutType(storyLayoutType);
            var message = GlobalContainer.LocalizationManager.GetText(textKey);

            // 次のメッセージ表示の段階でキャラ揺れ強制終了させる
            StopShakeCharacter();
            StopShakeScreen();

            var updateCharacterObservable = Observable.Defer(() => 
            {
                return Observable.WhenAll(
                _updateOwnCharacter(_currentOwnCharacterView, ownCharacterView, ownCharacterActingType, ownSkinLevel, isLayoutChange),
                _updateEnemyCharacter(_currentEnemyCharacterView, enemyCharacterView, enemyCharacterActingType, enemySkinLevel, isLayoutChange));
            });

            // 表示するメッセージが空っぽの場合次のメッセージへ
            if (string.IsNullOrEmpty(message) && _isFirstSkipMessage)
            {
                _isFirstSkipMessage = false;
                // バトル前以外はここで移動アニメーションを加味しつつ次のメッセージへ
                return updateCharacterObservable
                .Select(_ => 
                {
                    _nextMessage();
                    return (UI.IStoryUIView)this;
                });
            }
            else
            {
                // 通常時
                return updateCharacterObservable.Do(_ => 
                {
                    _characterName.text = GlobalContainer.LocalizationManager.GetText(characterNameTextKey + ".Name");
                    _text.text = message;
                    if (_iconTimerDisposable != null) _iconTimerDisposable.Dispose();
                    _iconTimerDisposable = SceneContainer.GetTimeHandler().Timer(PAGE_ICON_VISIBLE_WAIT_TIME).Subscribe(tmp => _playPageIconAnimation()).AddTo(this);
                })
                // 少しだけWaitを持たせる
                .SelectMany(_ => SceneContainer.GetTimeHandler().Timer(0.1f).Do(tmp => _isFixCharacterActing = true))
                .Select(_ => (UI.IStoryUIView)this);
            }
        }

        // 左側キャラクターの更新
        IObservable<ICharacterView> _updateOwnCharacter(ICharacterView previous, ICharacterView current, StoryCharacterActingType? storyCharacterActingType, byte skinLevel, bool isLayoutChange)
        {
            if (!_checkCharacterMessageOnly(storyCharacterActingType))
            {
                _appearCharacter(_currentOwnCharacterView, current, _currentEnemyCharacterView, true, storyCharacterActingType, skinLevel);
                // 左側のキャラクターがしゃべるので、右側のキャラクターが表示されていたら暗くする
                if (_currentEnemyCharacterView != null && current != null) _pauseOnCharacter(_currentEnemyCharacterView);
                if (current != null)
                {
                    _currentOwnCharacterView = current;
                }
                if (isLayoutChange && !storyCharacterActingType.HasValue)
                {
                    _moveCharacterViewChangeLayout(_currentOwnCharacterView);
                }
            }
            return _tryCharacterActing(current, storyCharacterActingType, true, _wipeInOwnCharacter, () => { _isWipeOutOwnCharacter = true; });
        }
        // 右側キャラクターの更新
        IObservable<ICharacterView> _updateEnemyCharacter(ICharacterView previous, ICharacterView current, StoryCharacterActingType? storyCharacterActingType, byte skinLevel, bool isLayoutChange)
        {
            if (!_checkCharacterMessageOnly(storyCharacterActingType))
            {
                _appearCharacter(_currentEnemyCharacterView, current, _currentOwnCharacterView, false, storyCharacterActingType, skinLevel);
                // 右側のキャラクターがしゃべるので、左側のキャラクターが表示されていたら暗くする
                if (_currentOwnCharacterView != null && current != null) _pauseOnCharacter(_currentOwnCharacterView);
                if (current != null)
                {
                    _currentEnemyCharacterView = current;
                }
                if (isLayoutChange && !storyCharacterActingType.HasValue)
                {
                    _moveCharacterViewChangeLayout(_currentEnemyCharacterView);
                }
            }
            return _tryCharacterActing(current, storyCharacterActingType, false, _wipeInEnemyCharacter, () => { _isWipeOutEnemyCharacter = true; });
        }

        public IObservable<bool> Finish()
        {
            _clickable = false;
            // ストーリー再生終了時は強制的にフレームアウトさせる
            _isWipeOutOwnCharacter = _isWipeOutEnemyCharacter = true;
            return _wipeOutCurrentCharacters().SelectMany(_finish());
        }
        IObservable<bool> _finish()
        {
            var onFinish = new Subject<bool>();
            _topLine.DOAnchorPos(_topLineHidePosition, _lineMoveDuration).OnComplete(() =>
            {
                onFinish.OnNext(true);
                onFinish.OnCompleted();
                SetActive(false);
            });
            _bottomLine.DOAnchorPos(_bottomLineHidePosition, _lineMoveDuration);
            return onFinish.AsObservable().Do(_ => _onFinish());
        }
        void _onFinish()
        {
            _currentOwnCharacterView = null;
            _currentEnemyCharacterView = null;
            // Finish時にDisposeして次のストーリー開始時に再作成させる
            _onClickSkipButtonSubject.Dispose();
            _onClickSkipButtonSubject = null;
            _onClickSubject.Dispose();
            _onClickSubject = null;
            if (_iconTimerDisposable != null) _iconTimerDisposable.Dispose();
            _iconTimerDisposable = null;
            _characterName.text = string.Empty;
            _text.text = "";
            _clear();
        }

        IObservable<ICharacterView> _wipeInOwnCharacter(ICharacterView characterView)
        {
            if (characterView == null) return Observable.Return<ICharacterView>(characterView);
            characterView.SetActive(true);
            return _moveLocalOwnX(characterView, _inactiveCharacterPositionX, _activeCharacterPositionX, true);
        }
        IObservable<ICharacterView> _wipeOutOwnCharacter(ICharacterView characterView)
        {
            if (characterView == null || !_isWipeOutOwnCharacter) return Observable.Return<ICharacterView>(characterView);
            return _moveLocalOwnX(characterView, _activeCharacterPositionX, _inactiveCharacterPositionX, false).Do(v => v.SetActive(false));
        }
        IObservable<ICharacterView> _wipeInEnemyCharacter(ICharacterView characterView)
        {
            if (characterView == null) return Observable.Return<ICharacterView>(characterView);
            characterView.SetActive(true);
            return _moveLocalEnemyX(characterView, -_inactiveCharacterPositionX, -_activeCharacterPositionX, true);
        }
        IObservable<ICharacterView> _wipeOutEnemyCharacter(ICharacterView characterView)
        {
            if (characterView == null || !_isWipeOutEnemyCharacter) return Observable.Return<ICharacterView>(characterView);
            return _moveLocalEnemyX(characterView, -_activeCharacterPositionX, -_inactiveCharacterPositionX, false).Do(v => v.SetActive(false));
        }

        IObservable<ICharacterView> _moveLocalOwnX(ICharacterView characterView, float startValue, float endValue, bool isInMove)
        {
            characterView.SetUpForUI(CenterPositionForUIType.Story);
            var prevLocalPosition = ((CharacterView)characterView).GetTransform().localPosition;

            var startPositionX = startValue - (startValue * (isInMove ? -1 : 1) + prevLocalPosition.x);
            var endPositionX = endValue - (endValue * (isInMove ? 1 : -1) + prevLocalPosition.x);

            startPositionX = _getStartPositionX(startPositionX, isInMove);
            endPositionX = _getEndPositionX(endPositionX, isInMove);

            return _moveLocalX(characterView, prevLocalPosition, startPositionX, endPositionX);
        }

        IObservable<ICharacterView> _moveLocalEnemyX(ICharacterView characterView, float startValue, float endValue, bool isInMove)
        {
            characterView.SetUpForUI(CenterPositionForUIType.Story);
            var prevLocalPosition = ((CharacterView)characterView).GetTransform().localPosition;

            var startPositionX = startValue + (startValue * (isInMove ? 1 : -1) + prevLocalPosition.x);
            var endPositionX = endValue + (endValue * (isInMove ? -1 : 1) + prevLocalPosition.x);

            startPositionX = _getStartPositionX(startPositionX, isInMove);
            endPositionX = _getEndPositionX(endPositionX, isInMove);

            return _moveLocalX(characterView, prevLocalPosition, startPositionX, endPositionX);
        }

        IObservable<ICharacterView> _moveLocalX(ICharacterView characterView, Vector3 previewPosition, float startPositionX, float endPositionX)
        {
            var onComplete = new Subject<ICharacterView>();
            var characterViewTransform = ((CharacterView)characterView).GetTransform();
            characterViewTransform.localPosition = new Vector3(startPositionX, previewPosition.y, previewPosition.z);
            characterViewTransform.DOLocalMoveX(endPositionX, _characterViewSlideDuration).OnComplete(() => 
            {
                onComplete.OnNext(characterView);
                onComplete.OnCompleted();
            });
            return onComplete.AsObservable();
        }

        // レイアウト変更に伴うキャラクターの移動処理
        void _moveCharacterViewChangeLayout(ICharacterView characterView)
        {
            if (characterView == null) return;
            var transform = ((CharacterView)characterView).GetTransform();
            var endValue = _getStoryLayoutPosition(characterView);
            endValue = transform.localEulerAngles.y > 180 ? -endValue : endValue;
            transform.DOLocalMoveX(endValue.x, _characterViewSlideDuration);
        }

        // レイアウト別で配置座標を取得する
        Vector3 _getStoryLayoutPosition(ICharacterView characterView)
        {
            var storyPositions = ((CharacterView)characterView).GetStoryPositions();
            var characterViewTransform = ((CharacterView)characterView).GetTransform();
            var newPosition = Vector3.zero;
            switch (_storyLayoutType)
            {
            case Noroshi.Core.Game.Battle.StoryLayoutType.Centering:
                var isSpecial = storyPositions.Length > 1;
                newPosition = isSpecial ? new Vector3(-storyPositions[1].x, -storyPositions[1].y, characterViewTransform.localPosition.z) :
                    new Vector3(-storyPositions[0].x / 2.0f, -storyPositions[0].y, characterViewTransform.localPosition.z);
                break;
            case Noroshi.Core.Game.Battle.StoryLayoutType.LeftAndRightDivision:
                newPosition = new Vector3(-storyPositions[0].x, -storyPositions[0].y, characterViewTransform.localPosition.z);
                break;
            case Noroshi.Core.Game.Battle.StoryLayoutType.CenterAlone:
                newPosition = new Vector3(0, -storyPositions[0].y, characterViewTransform.localPosition.z);
                break;
            default:
                throw new ArgumentOutOfRangeException();
            }
            return newPosition;
        }

        float _getStartPositionX(float startPositionX, bool isInMove)
        {
            return isInMove ? startPositionX : _getStoryLayoutTypePosition(startPositionX);
        }
        float _getEndPositionX(float endPositionX, bool isInMove)
        {
            return isInMove ? _getStoryLayoutTypePosition(endPositionX) : endPositionX;
        }

        float _getStoryLayoutTypePosition(float position)
        {
            switch (_storyLayoutType)
            {
            case StoryLayoutType.Centering:
                return position / 2.0f;
            case StoryLayoutType.LeftAndRightDivision:
                return position;
            case StoryLayoutType.CenterAlone:
                return 0.0f;
            default:
                throw new ArgumentOutOfRangeException();
            }
        }

        IObservable<ICharacterView> _tryCharacterActing(ICharacterView characterView, StoryCharacterActingType? storyCharacterActingType, bool isOwnCharacter,
                                                        Func<ICharacterView, IObservable<ICharacterView>> actionWipeInCharacter, Action actionWipeOutCharacter)
        {
            if (!storyCharacterActingType.HasValue)
            {
                // アクション未設定でCharacterViewが設定されていたら、通常の表示位置に表示させる
                if (characterView != null)
                {
                    _setCharacterViewUIPosition(characterView);
                    if (isOwnCharacter)
                    {
                        var transform = ((CharacterView)characterView).GetTransform();
                        var reversePosition = new Vector3(-transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
                        transform.localPosition = reversePosition;
                    }
                }
                return Observable.Return<ICharacterView>(characterView);
            }
            
            switch (storyCharacterActingType.Value)
            {
            case StoryCharacterActingType.WipeIn:
                return actionWipeInCharacter(characterView);
            case StoryCharacterActingType.WipeOut:
                // フレームアウトは画面タップ時に行うのでここではフラグを立てるだけ
                actionWipeOutCharacter();
                break;
            case StoryCharacterActingType.Pitching:
                _shakeByCharacter(characterView, isOwnCharacter, CharacterShakeType.Vertical, _shakeVerticalParam).Subscribe(_ => {}).AddTo(this);
                break;
            case StoryCharacterActingType.Backward:
                _shakeByCharacter(characterView, isOwnCharacter, CharacterShakeType.Horizontal, _shakeHorizontalParam).Subscribe(_ => {}).AddTo(this);
                break;
            }
            return Observable.Return<ICharacterView>(characterView);
        }

        void _appearCharacter(ICharacterView previous, ICharacterView current, ICharacterView otherPrevious, bool isOwnCharacter,
                              StoryCharacterActingType? storyCharacterActingType, byte skinLevel)
        {
            if (previous == current)
            {
                if (current != null) _pauseOffCharacter(current);
                return;
            }
            if (current != null)
            {
                current.SetHorizontalDirection(isOwnCharacter ? Direction.Right : Direction.Left);
                _setCharacterViewUIPosition(current);

                // 左側を表示する際は座標を再設定する
                if (isOwnCharacter)
                {
                    var transform = ((CharacterView)current).GetTransform();
                    var reversePosition = new Vector3(-transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
                    transform.localPosition = reversePosition;
                }
                // 違うキャラクターが表示されていた場合
                if (previous != null)
                {
                    // 表示キャラクターにアクションが設定されていたら、非表示キャラはワイプアウトさせる
                    if (storyCharacterActingType.HasValue && storyCharacterActingType.Value == StoryCharacterActingType.WipeIn)
                    {
                        _wipeOutCharacterForcibly(isOwnCharacter, previous).Subscribe(_ => {}).AddTo(this);
                    }
                    else
                    {
                        // アクション未設定なら非表示にするだけ
                        previous.SetActive(false);
                    }
                }
                current.SetSkin(skinLevel);
                if (!storyCharacterActingType.HasValue || storyCharacterActingType.Value != StoryCharacterActingType.WipeIn)
                {
                    current.SetActive(true);
                }
                _pauseOffCharacter(current);
            }
        }

        IObservable<bool> _wipeOutCurrentCharacters()
        {
            return Observable.WhenAll(
                _wipeOutOwnCharacter(_currentOwnCharacterView),
                _wipeOutEnemyCharacter(_currentEnemyCharacterView)
            )
            .Select(_ => true);
        }

        // 対象キャラクターを強制的にワイプアウトさせる
        IObservable<ICharacterView> _wipeOutCharacterForcibly(bool isOwn, ICharacterView characterView)
        {
            if (isOwn)
            {
                _isWipeOutOwnCharacter = true;
                return _wipeOutOwnCharacter(characterView).Do(_ => _isWipeOutOwnCharacter = false);
            }
            else
            {
                _isWipeOutEnemyCharacter = true;
                return _wipeOutEnemyCharacter(characterView).Do(_ => _isWipeOutEnemyCharacter = false);
            }
        }

        bool _checkCharacterMessageOnly(StoryCharacterActingType? storyCharacterActingType)
        {
            if (!storyCharacterActingType.HasValue) return false;
            if (storyCharacterActingType.Value != StoryCharacterActingType.MessageOnly) return false;
            // メッセージしかない場合は、表示しているキャラクターを全て暗くさせる
            if (_currentOwnCharacterView != null) _pauseOnCharacter(_currentOwnCharacterView);
            if (_currentEnemyCharacterView != null) _pauseOnCharacter(_currentEnemyCharacterView);
            return true;
        }

        void _pauseOnCharacter(ICharacterView characterView)
        {
            characterView.SetColor(Color.gray);
            characterView.PauseOn();
            characterView.SetOrderInLayer(Constant.SPINE_UI_ORDER_IN_LAYER);
        }

        void _pauseOffCharacter(ICharacterView characterView)
        {
            characterView.SetColor(Color.white);
            characterView.PauseOff();
            characterView.SetActive(true);
            characterView.SetOrderInLayer(Constant.SPINE_UI_ORDER_IN_LAYER + 1);
        }
        
        public void ShakeByScreenToDrawingCharacter()
        {
            _doTweenScreenSequence = DOTween.Sequence();
            var position = _transform.localPosition;
            var duration = _shakeScreen.Duration / 5.0f;
            var addShake = _shakeScreen.ShakeMoveMax;
            var subShake = -_shakeScreen.ShakeMoveMax;
            // 揺れ１回目
            _doTweenScreenSequence.Append(_transform.DOLocalMoveY(position.y + addShake, duration));
            _doTweenScreenSequence.Append(_transform.DOLocalMoveY(position.y + subShake, duration));
            // 揺れ２回目
            _doTweenScreenSequence.Append(_transform.DOLocalMoveY(position.y + (addShake / 2.0f), duration));
            _doTweenScreenSequence.Append(_transform.DOLocalMoveY(position.y + (subShake / 2.0f), duration));
            // 元の位置に戻す
            _doTweenScreenSequence.Append(_transform.DOLocalMoveY(position.y, duration));
            // ループ数設定と再生終了時の設定
            _doTweenScreenSequence.SetLoops(_shakeScreen.LoopNum, LoopType.Restart).OnComplete(() => 
            {
                _doTweenScreenSequence = null;
            });
            // キャラクターの再生も同時に設定して再生開始
            _shakeByCharacter(_currentOwnCharacterView, true, CharacterShakeType.Camera, _shakeScreenLinkedCharacter).Subscribe(_ => {});
            _shakeByCharacter(_currentEnemyCharacterView, false, CharacterShakeType.Camera, _shakeScreenLinkedCharacter).Subscribe(_ => {});
            _doTweenScreenSequence.Play();
        }

        IObservable<bool> _shakeByCharacter(ICharacterView characterView, bool isOwnCharacter, CharacterShakeType characterShakeType, ShakeParam shakeParam)
        {
            if (characterView == null) return Observable.Return<bool>(true);
            var doTweenSequence = DOTween.Sequence();
            var onShakeCompleteSubject = new Subject<bool>();

            var transform = ((CharacterView)characterView).GetTransform();
            var position = transform.localPosition;
            var isTwoRoundTrip = characterShakeType == CharacterShakeType.Horizontal || characterShakeType == CharacterShakeType.Camera;
            var isVerticalShake = characterShakeType == CharacterShakeType.Vertical || characterShakeType == CharacterShakeType.Camera;
            // 画面揺れの場合と、横揺れは２往復を１セット
            // キャラクターのみの縦揺れは１往復を１セットとする
            var duration = isTwoRoundTrip ? shakeParam.Duration / 5.0f : shakeParam.Duration / 3.0f;
            var addShakeMove = isVerticalShake ? Vector3.up * shakeParam.ShakeMoveMax : Vector3.right * shakeParam.ShakeMoveMax;
            var subShakeMove = isVerticalShake ? Vector3.down * shakeParam.ShakeMoveMax : Vector3.left * shakeParam.ShakeMoveMax;

            doTweenSequence.Append(transform.DOLocalMove(position + addShakeMove, duration));
            doTweenSequence.Append(transform.DOLocalMove(position + subShakeMove, duration));
            if (characterShakeType != CharacterShakeType.Vertical)
            {
                doTweenSequence.Append(transform.DOLocalMove(position + (addShakeMove / 2.0f), duration));
                doTweenSequence.Append(transform.DOLocalMove(position + (subShakeMove / 2.0f), duration));
            }
            doTweenSequence.Append(transform.DOLocalMove(position, duration));

            
            doTweenSequence.SetLoops(shakeParam.LoopNum, LoopType.Restart).OnComplete(() => 
            {
                onShakeCompleteSubject.OnNext(true);
                onShakeCompleteSubject.OnCompleted();
                doTweenSequence = null;
            });
            
            doTweenSequence.Play();
            if (isOwnCharacter)
            {
                _doTweenOwnSequence = doTweenSequence;
            }
            else
            {
                _doTweenEnemySequence = doTweenSequence;
            }
            return onShakeCompleteSubject.AsObservable();
        }

        public void StopShakeScreen()
        {
            if (_doTweenScreenSequence != null)
            {
                _doTweenScreenSequence.Complete();
                _doTweenScreenSequence = null;
            }
        }

        public void StopShakeCharacter()
        {
            if (_screenShakeDisposable != null)
            {
                _screenShakeDisposable.Dispose();
                _screenShakeDisposable = null;
            }
            if (_doTweenOwnSequence != null)
            {
                _doTweenOwnSequence.Complete();
                _doTweenOwnSequence = null;
            }
            if (_doTweenEnemySequence != null)
            {
                _doTweenEnemySequence.Complete();
                _doTweenEnemySequence = null;
            }
        }

        public void WhiteOut()
        {
            _whiteOutImage.DOKill(true);
            _whiteOutImage.DOColor(Color.white, 0.02f).OnComplete(() => 
            {
                _whiteOutImage.DOColor(new Color(1, 1, 1, 0), 1.0f);
            });
        }

        void _clear()
        {
            StopShakeCharacter();
            StopShakeScreen();
        }

        // 画面タップ時に呼ばれる
        public void OnScreenClick()
        {
            // キャラクターが何かしらのアクション中ならタップ禁止
            if (!_isFixCharacterActing) return;
            if (!_clickable) return;
            // ページアイコン非表示
            _stopPageIconAnimation();
            _nextMessage();
            // ここにサウンド再生を入れる
            _onCommandSoundSubjet.OnNext(new SoundEvent()
            {
                Command = SoundCommand.Play,
                SoundID = SoundConstant.SE_CLICK_SOUND_ID
            });
        }
        void _nextMessage()
        {
            _wipeOutCurrentCharacters().Subscribe(_ => 
            {
                _isWipeOutOwnCharacter = _isWipeOutEnemyCharacter = false;
                _onClickSubject.OnNext(true);
            });
        }

        // スキップボタンを押した時に呼ばれる
        public void OnSkipClick()
        {
            _onCommandSoundSubjet.OnNext(new SoundEvent()
            {
                Command = SoundCommand.Play,
                SoundID = SoundConstant.SE_SKIP_SOUND_ID
            });
            _onClickSkipButtonSubject.OnNext(true);
        }
    }
}
