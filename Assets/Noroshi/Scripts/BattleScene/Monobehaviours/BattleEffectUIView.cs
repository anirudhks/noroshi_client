﻿using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Noroshi.MonoBehaviours;
using Noroshi.Core.Game.Sound;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class BattleEffectUIView : SpineView
    {
        const string ANIMATION_OTHERS_VICTORY_NAME = "others_victory";
        const string ANIMATION_PVP_START_NAME = "pvp_start";
        const string ANIMATION_PVP_VICTORY_NAME = "pvp_victory";
        const string ANIMATION_TIME_UP_NAME = "timeup";
        const string ANIMATION_TRAINING_START = "training_start";

        const string ANIMATION_EVENT_NAME_FOR_MOVE = "move";

        // vs-ready エフェクトの効果音
        Dictionary<float, uint> _animationPlayerBattleStartSound = new Dictionary<float, uint>()
        {
            {0.0f, SoundConstant.BATTLE_START_VS_SOUND_ID},
            {2.0f, SoundConstant.BATTLE_START_READY_SOUND_ID},
        };

        // 3.2.1-fight エフェクトの効果音
        Dictionary<float, uint> _animationTrainingStartSound = new Dictionary<float, uint>()
        {
            {0.0f, SoundConstant.BATTLE_START_321_SOUND_ID},
            {0.7f, SoundConstant.BATTLE_START_321_SOUND_ID},
            {1.4f, SoundConstant.BATTLE_START_321_SOUND_ID},
            {2.1f, SoundConstant.BATTLE_START_FIGHT_SOUND_ID},
        };

        [SerializeField] UnityEngine.Camera _uiCamera = null;

        // 上下枠が表示されていてなおかつ枠が消え始めるタイミングでOnNextされるSubject
        Subject<bool> _onEnterFrameOutSubject = new Subject<bool>();
        /// サウンド操作時に操作内容が OnNext される Subject。
        protected Subject<SoundEvent> _onCommandSoundSubject = new Subject<SoundEvent>();

        float _defaultCameraOrthographicSize = 0.0f;

        new void Awake()
        {
            base.Awake();
            // 初期化
            _transform.localScale = Vector3.one;
        }
        
        new void Start()
        {
            base.Start();
            _rootSkeletonAnimation.state.Event += _onAnimationEvent;
            _defaultCameraOrthographicSize = _uiCamera.orthographicSize;
        }

        public IObservable<bool> GetOnEnterFrameOutObservable()
        {
            return _onEnterFrameOutSubject.AsObservable();
        }

        /// サウンド操作時に操作内容が OnNext される Observable を取得。
        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _onCommandSoundSubject.AsObservable();
        }

        void _onAnimationEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
        {
            if (e.ToString() == ANIMATION_EVENT_NAME_FOR_MOVE)
            {
                _onEnterFrameOutSubject.OnNext(true);
            }
        }

        public IObservable<bool> PlayAnimationDefaultVictory()
        {
            return _playAnimation(ANIMATION_OTHERS_VICTORY_NAME);
        }

        public IObservable<bool> PlayAnimationPlayerBattleStart()
        {
            _playSound(_animationPlayerBattleStartSound);
            return _playAnimation(ANIMATION_PVP_START_NAME);
        }

        public IObservable<bool> PlayAnimationPlayerBattleVictory()
        {
            return _playAnimation(ANIMATION_PVP_VICTORY_NAME);
        }

        public IObservable<bool> PlayAnimationTimeUp()
        {
            return _playAnimation(ANIMATION_TIME_UP_NAME);
        }

        public IObservable<bool> PlayAnimationTrainingStart()
        {
            _playSound(_animationTrainingStartSound);
            return _playAnimation(ANIMATION_TRAINING_START);
        }

        IObservable<bool> _playAnimation(string animationName)
        {
            // 現在のUI用カメラのサイズに合わせて終了演出を拡大縮小させる
            var effectScale = _uiCamera.orthographicSize / _defaultCameraOrthographicSize;
            _transform.localScale = new Vector3(effectScale, effectScale, 1);
            SetActive(true);
            return _play(animationName, false);
        }

        void _playSound(Dictionary<float, uint> animationSound)
        {
            animationSound.ToObservable().Select(pair => GlobalContainer.TimeHandler.Timer(pair.Key).Select(_ => pair.Value)).Merge().Subscribe(soundId =>
            {
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    SoundID = soundId,
                    Command = SoundCommand.Play,
                });
            });
        }
    }
}
