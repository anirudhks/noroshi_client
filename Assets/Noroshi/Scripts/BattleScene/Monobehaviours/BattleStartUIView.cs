﻿using UniRx;
using UnityEngine;
using DG.Tweening;
using Noroshi.Core.Game.Battle;

namespace Noroshi.BattleScene.MonoBehaviours
{
    // 修行とアリーナで使用する上下枠制御用クラス
    public class BattleStartUIView : UIView
    {
        enum FramePositionType
        {
            InScreen = 1,
            OutScreen,
        }
        [SerializeField] RectTransform _topLine = null;
        [SerializeField] RectTransform _bottomLine = null;
        [SerializeField] UnityEngine.UI.Text _ownPlayerName = null;
        [SerializeField] UnityEngine.UI.Text _enemyPlayerName = null;
        [SerializeField] RectTransform _ownPlayerFrame = null;
        [SerializeField] RectTransform _enemyPlayerFrame = null;
        [SerializeField] RectTransform _ownBackground = null;
        [SerializeField] RectTransform _enemyBackground = null;
        [SerializeField] float _lineMoveDuration = 0.5f;
        [SerializeField] float _playerBattleUIMoveDuration = 0.4f;

        Vector2 _topLineAnchorPosition;
        Vector2 _bottomLineAnchorPosition;
        Vector2 _topLineHideAnchorPosition;
        Vector2 _bottomLineHideAnchorPosition;

        // 対人専用
        Vector2 _ownPlayerNameFrameAnchorPosition;
        Vector2 _ownPlayerNameFrameHideAnchorPosition;
        Vector2 _enemyPlayerNameFrameAnchorPosition;
        Vector2 _enemyPlayerNameFrameHideAnchorPosition;
        Vector2 _ownBackgroundAnchorPosition;
        Vector2 _ownBackgroundHideAnchorPosition;
        Vector2 _enemyBackgroundAnchorPosition;
        Vector2 _enemyBackgroundHideAnchorPosition;

        // 枠について最初に表示するタイミングはフレームインさせずに直接配置する
        FramePositionType _frameActiveType = FramePositionType.InScreen;

        new void Awake()
        {
            base.Awake();
            _topLineAnchorPosition = _topLine.anchoredPosition;
            _bottomLineAnchorPosition = _bottomLine.anchoredPosition;
            _topLineHideAnchorPosition = _topLineAnchorPosition + Vector2.up * _topLine.rect.height;
            _bottomLineHideAnchorPosition = _bottomLineAnchorPosition + Vector2.down * _bottomLine.rect.height;

            _ownPlayerNameFrameAnchorPosition = _ownPlayerFrame.anchoredPosition;
            _ownPlayerNameFrameHideAnchorPosition = _ownPlayerNameFrameAnchorPosition + Vector2.left * _ownBackground.rect.width;
            _enemyPlayerNameFrameAnchorPosition = _enemyPlayerFrame.anchoredPosition;
            _enemyPlayerNameFrameHideAnchorPosition = _enemyPlayerNameFrameAnchorPosition + Vector2.right * _enemyBackground.rect.width;
            _ownBackgroundAnchorPosition = _ownBackground.anchoredPosition;
            _ownBackgroundHideAnchorPosition = _ownBackgroundAnchorPosition + Vector2.left * _ownBackground.rect.width;
            _enemyBackgroundAnchorPosition = _enemyBackground.anchoredPosition;
            _enemyBackgroundHideAnchorPosition = _enemyBackgroundAnchorPosition + Vector2.right * _enemyBackground.rect.width;

            SetActive(false);
            _setPlayerBattleViewActive(false);
        }

        void _setPlayerBattleViewActive(bool active)
        {
            _ownPlayerFrame.gameObject.SetActive(active);
            _enemyPlayerFrame.gameObject.SetActive(active);
            _ownBackground.gameObject.SetActive(active);
            _enemyBackground.gameObject.SetActive(active);
        }

        public void SetPlayerNames(string ownPlayerName, string enemyPlayerName)
        {
            _ownPlayerName.text = ownPlayerName;
            _enemyPlayerName.text = enemyPlayerName;
        }

        void _activeBattleStartUIView(bool isPlayerBattle)
        {
            _setPlayerBattleViewActive(isPlayerBattle);
        }

        void _activatePlayerBattleView()
        {
            _ownPlayerFrame.DOAnchorPos(_ownPlayerNameFrameAnchorPosition, _playerBattleUIMoveDuration);
            _ownBackground.DOAnchorPos(_ownBackgroundAnchorPosition, _playerBattleUIMoveDuration);
            _enemyPlayerFrame.DOAnchorPos(_enemyPlayerNameFrameAnchorPosition, _playerBattleUIMoveDuration);
            _enemyBackground.DOAnchorPos(_enemyBackgroundAnchorPosition, _playerBattleUIMoveDuration);
        }

        void _deactivePlayerBattleView()
        {
            _ownPlayerFrame.DOAnchorPos(_ownPlayerNameFrameHideAnchorPosition, _playerBattleUIMoveDuration);
            _ownBackground.DOAnchorPos(_ownBackgroundHideAnchorPosition, _playerBattleUIMoveDuration);
            _enemyPlayerFrame.DOAnchorPos(_enemyPlayerNameFrameHideAnchorPosition, _playerBattleUIMoveDuration);
            _enemyBackground.DOAnchorPos(_enemyBackgroundHideAnchorPosition, _playerBattleUIMoveDuration);
        }

        public IObservable<bool> Activate(bool isPlayerBattle)
        {
            var onCompleteSubject = new Subject<bool>();
            SetActive(true);
            _activeBattleStartUIView(isPlayerBattle);
            // 一度フレームアウトしていれば、フレームインのアニメーションを再生させる
            if (_frameActiveType == FramePositionType.OutScreen)
            {
                _topLine.anchoredPosition = _topLineHideAnchorPosition;
                _bottomLine.anchoredPosition = _bottomLineHideAnchorPosition;
                _topLine.DOAnchorPos(_topLineAnchorPosition, _lineMoveDuration).OnComplete(() => 
                {
                    _frameActiveType = FramePositionType.InScreen;
                    onCompleteSubject.OnNext(true);
                    onCompleteSubject.OnCompleted();
                });
                _bottomLine.DOAnchorPos(_bottomLineAnchorPosition, _lineMoveDuration);
                if (isPlayerBattle) _activatePlayerBattleView();
                return onCompleteSubject.AsObservable();
            }
            else
            {
                //状態に変化がない場合は、最初から移動終了位置に置く
                _topLine.anchoredPosition = _topLineAnchorPosition;
                _bottomLine.anchoredPosition = _bottomLineAnchorPosition;
                _ownPlayerFrame.anchoredPosition = _ownPlayerNameFrameAnchorPosition;
                _enemyPlayerFrame.anchoredPosition = _enemyPlayerNameFrameAnchorPosition;
                _ownBackground.anchoredPosition = _ownBackgroundAnchorPosition;
                _enemyBackground.anchoredPosition = _enemyBackgroundAnchorPosition;
                return Observable.Empty<bool>();
            }
        }

        public IObservable<bool> Deactivate(bool isPlayerBattle)
        {
            // フレームインしている状態であれば、フレームアウトアニメーションを再生させる
            if (_frameActiveType == FramePositionType.InScreen)
            {
                var onCompleteSubject = new Subject<bool>();
                _topLine.anchoredPosition = _topLineAnchorPosition;
                _bottomLine.anchoredPosition = _bottomLineAnchorPosition;
                _topLine.DOAnchorPos(_topLineHideAnchorPosition, _lineMoveDuration).OnComplete(() => 
                {
                    _frameActiveType = FramePositionType.OutScreen;
                    _setPlayerBattleViewActive(false);
                    SetActive(false);
                    onCompleteSubject.OnNext(true);
                    onCompleteSubject.OnCompleted();
                });
                _bottomLine.DOAnchorPos(_bottomLineHideAnchorPosition, _lineMoveDuration);
                if (isPlayerBattle) _deactivePlayerBattleView();
                return onCompleteSubject.AsObservable();
            }
            else
            {
                //状態に変化がない場合は、最初から移動終了位置に置く
                _topLine.anchoredPosition = _topLineHideAnchorPosition;
                _bottomLine.anchoredPosition = _bottomLineHideAnchorPosition;
                _ownPlayerFrame.anchoredPosition = _ownPlayerNameFrameHideAnchorPosition;
                _enemyPlayerFrame.anchoredPosition = _enemyPlayerNameFrameHideAnchorPosition;
                _ownBackground.anchoredPosition = _ownBackgroundHideAnchorPosition;
                _enemyBackground.anchoredPosition = _enemyBackgroundHideAnchorPosition;
                return Observable.Empty<bool>();
            }
        }
    }
}
