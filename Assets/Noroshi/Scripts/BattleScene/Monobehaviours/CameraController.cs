﻿using UnityEngine;
using UniRx;
using Noroshi.BattleScene.Camera;
using DG.Tweening;
using UniLinq;
using Noroshi.Grid;
using System.Collections.Generic;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class CameraController : MonoBehaviour, ICameraController
    {
        const float ACTIVE_ACTION_CAMERA_OFFSET_Y = 4.0f;

        [SerializeField] UnityEngine.Camera _mainCamera;
        [SerializeField] UnityStandardAssets.ImageEffects.BlurOptimized _blurOptimized;
        [SerializeField] float _shakeDuration = 0.5f;
        [SerializeField] Vector3 _shakeStrength = new Vector3(0.5f, 1.0f, 0);
        [SerializeField] int _shakeVibrato = 10;
        [SerializeField] ShakeParam _shakeByActionParam = new ShakeParam();
        [SerializeField] ShakeParam _shakeBySwitchWaveParam = new ShakeParam();
        [SerializeField] float _battleSituationCameraOffset = 5.0f;
        [SerializeField] float _activeActionCameraOffset = 4.0f;

        float _mainCameraOriginalSize;
        Transform _mainCameraTransform;
        Sequence _doTweenSequence = null;
        Sequence _doTweenSwitchWaveSequence = null;
        float _screentAspect = 0.0f;
        Vector2[] _defaultCameraVertices;
        // 右と上の最大値及び、左と下の最小値座標を持つキャラクター(最大４体)を格納するDictionary
        Dictionary<Direction, Character> _screenEndCharacters = new Dictionary<Direction, Character>();
        bool _isPauseBattleSituationCamera = false;

        void _fixCameraWidth()
        {
            // カメラの横幅を DEFAULT_BATTLE_SCREEN_WIDTH に合わせて固定する
            // ピクセルで設定した横幅をメートルに変換する
            var widthMeter = (Constant.DEFAULT_BATTLE_SCREEN_WIDTH / Constant.PIXEL_PER_UNIT);
            _mainCamera.orthographicSize = ((widthMeter * Screen.height) / Screen.width) / 2.0f;
        }

        void Awake()
        {
            // バトルに入った初回だけカメラの横幅を調整する
            _fixCameraWidth();
        }

        void Start()
        {
            _mainCameraTransform = _mainCamera.transform;
            _mainCameraOriginalSize = _mainCamera.orthographicSize;
            _blurOptimized.enabled = false;
            _screentAspect = (float)Screen.width / Screen.height;
            _defaultCameraVertices = _getCameraVertices(_mainCameraOriginalSize, _mainCamera.aspect, _mainCameraTransform.position);
        }

        /// Blur のオンオフをセットする。
        public IObservable<bool> SetBlurActive(bool active)
        {
            _blurOptimized.enabled = active;
            return Observable.Return(active);
        }

        public void PauseOnWithBattleSituationCamera()
        {
            _isPauseBattleSituationCamera = true;
        }

        public void PauseOffWithBattleSituationCamera()
        {
            _isPauseBattleSituationCamera = false;
        }

        public void SetScreenEndCharacters(Dictionary<Direction, Character> characters)
        {
            _screenEndCharacters = characters;
        }

        public void Clear()
        {
            _screenEndCharacters.Clear();
        }

        void Update()
        {
            // 現在のWaveに登場しているキャラクターの座標監視
            if(_screenEndCharacters.Count > 0 && !_isPauseBattleSituationCamera)
            {
                var characters = SceneContainer.GetBattleManager().CurrentWave.Field.GetAllCharacters().Where(c => !c.IsDead);
                // すでに抽出されている上下左右の画面端座標を超えている場合Characterの更新を行う
                foreach (var character in characters)
                {
                    var characterView = ((CharacterView)character.GetView());
                    var leftUpPos = characterView.GetDamageAreaLeftUpPosition();
                    var rightBottomPos = characterView.GetDamageAreaRightDownPosition();
                    if (leftUpPos.HasValue)
                    {
                        var leftCharacterView = ((CharacterView)_screenEndCharacters[Direction.Left].GetView());
                        var upCharacterView = ((CharacterView)_screenEndCharacters[Direction.Up].GetView());
                        _screenEndCharacters[Direction.Left] = 
                            leftCharacterView.GetDamageAreaLeftUpPosition().Value.x > characterView.GetDamageAreaLeftUpPosition().Value.x ?
                            character : _screenEndCharacters[Direction.Left];
                        _screenEndCharacters[Direction.Up] = 
                            upCharacterView.GetDamageAreaLeftUpPosition().Value.y < characterView.GetDamageAreaLeftUpPosition().Value.y ?
                            character : _screenEndCharacters[Direction.Up];
                    }
                    if (rightBottomPos.HasValue)
                    {
                        var rightCharacterView = ((CharacterView)_screenEndCharacters[Direction.Right].GetView());
                        var bottomCharacterView = ((CharacterView)_screenEndCharacters[Direction.Down].GetView());
                        _screenEndCharacters[Direction.Right] = 
                            rightCharacterView.GetDamageAreaRightDownPosition().Value.x < characterView.GetDamageAreaRightDownPosition().Value.x ?
                            character : _screenEndCharacters[Direction.Right];
                        _screenEndCharacters[Direction.Down] = 
                            bottomCharacterView.GetDamageAreaRightDownPosition().Value.y > characterView.GetDamageAreaRightDownPosition().Value.y ?
                            character : _screenEndCharacters[Direction.Down];
                    }
                }
                _updateCameraPosition();
            }
        }

        void _updateCameraPosition()
        {
            // 上下左右の座標を左上、右下のVector2に変換してズームインアウトをさせる
            var newPosition = new Vector2[]
            {
                new Vector2(((CharacterView)_screenEndCharacters[Direction.Left].GetView()).GetDamageAreaLeftUpPosition().Value.x,
                    ((CharacterView)_screenEndCharacters[Direction.Up].GetView()).GetDamageAreaLeftUpPosition().Value.y),
                new Vector2(((CharacterView)_screenEndCharacters[Direction.Right].GetView()).GetDamageAreaRightDownPosition().Value.x,
                    ((CharacterView)_screenEndCharacters[Direction.Down].GetView()).GetDamageAreaRightDownPosition().Value.y)
            };
            _moveAndZoom(newPosition, 0.25f, new Vector2(_battleSituationCameraOffset, _battleSituationCameraOffset)).Subscribe(_ => {});
        }

        public void SetNextWavedefaultCameraVertices()
        {
            // 次のWaveに移動したあとに各頂点座標を更新しておく
            _defaultCameraVertices = _getCameraVertices(_mainCameraOriginalSize, _mainCamera.aspect, _mainCameraTransform.position);
        }

        Vector2[] _getCameraVertices(float orthographicSize, float screenAspect, Vector3 newPosition)
        {
            float cameraWidthHalf = orthographicSize * screenAspect;
            var cameraVertices = new Vector2[4];
            // 0 = 右上 1 = 左上 2 = 左下 3 = 右下
            cameraVertices[0] = new Vector2(newPosition.x + cameraWidthHalf, newPosition.y + orthographicSize);
            cameraVertices[1] = new Vector2(newPosition.x - cameraWidthHalf, newPosition.y + orthographicSize);
            cameraVertices[2] = new Vector2(newPosition.x - cameraWidthHalf, newPosition.y - orthographicSize);
            cameraVertices[3] = new Vector2(newPosition.x + cameraWidthHalf, newPosition.y - orthographicSize);
            return cameraVertices;
        }

        public IObservable<bool> ZoomIn(float originalSize, float duration)
        {
            var onCompleteSubject = new Subject<bool>();
            _mainCamera.DOOrthoSize(originalSize, duration).OnComplete(() =>
            {
                onCompleteSubject.OnNext(true);
                onCompleteSubject.OnCompleted();
            });
            return onCompleteSubject.AsObservable();
        }

        public IObservable<bool> ZoomOut(float duration)
        {
            var onCompleteSubject = new Subject<bool>();
            _mainCamera.DOOrthoSize(_mainCameraOriginalSize, duration).OnComplete(() =>
            {
                onCompleteSubject.OnNext(true);
                onCompleteSubject.OnCompleted();
            });
            return onCompleteSubject.AsObservable();
        }

        public IObservable<bool> MoveX(float x, float duration)
        {
            var onCompleteSubject = new Subject<bool>();
            _mainCameraTransform.DOMoveX(x, duration).SetEase(Ease.InOutQuad).OnComplete(() =>
            {
                onCompleteSubject.OnNext(true);
                onCompleteSubject.OnCompleted();
            });
            return onCompleteSubject.AsObservable();
        }

        public IObservable<bool> Move(Vector3 pos, float duration, Ease ease)
        {
            var endPosition = new Vector3(pos.x, pos.y, _mainCameraTransform.position.z);
            var onCompleteSubject = new Subject<bool>();
            _mainCameraTransform.DOMove(endPosition, duration).SetEase(ease).OnComplete(() =>
            {
                onCompleteSubject.OnNext(true);
                onCompleteSubject.OnCompleted();
            });
            return onCompleteSubject.AsObservable();
        }

        // 縦揺れ
        IObservable<bool> _shakeByVertical(ShakeParam shakeParam, System.Action<Sequence> onSetSequence, System.Action onComplete)
        {
            var sequence = DOTween.Sequence();
            var onShakeCompleteSubject = new Subject<bool>();
            var moveY = shakeParam.ShakeMoveMax;
            var duration = shakeParam.Duration / 5.0f;
            var positionY = _mainCameraTransform.position.y;
            sequence.Append(_mainCameraTransform.DOMoveY(moveY, duration));
            sequence.Append(_mainCameraTransform.DOMoveY(-moveY, duration));
            sequence.Append(_mainCameraTransform.DOMoveY((moveY / 2.0f), duration));
            sequence.Append(_mainCameraTransform.DOMoveY(-(moveY / 2.0f), duration));
            sequence.Append(_mainCameraTransform.DOMoveY(positionY, duration));

            if (shakeParam.LoopNum > 1)
            {
                sequence.SetLoops(shakeParam.LoopNum, LoopType.Restart);
            }

            sequence.OnComplete(() => 
            {
                onShakeCompleteSubject.OnNext(true);
                onShakeCompleteSubject.OnCompleted();
                if (onComplete != null) onComplete();
            });

            sequence.Play();

            if (onSetSequence != null) onSetSequence(sequence);
            return onShakeCompleteSubject.AsObservable();
        }

        // Wave切り替え時の縦揺れ
        public IObservable<bool> ShakeBySwitchWave()
        {
            EnterTimeStopWithShakeAction();
            return _shakeByVertical(_shakeBySwitchWaveParam,
                                    (sequence) => { _doTweenSwitchWaveSequence = sequence; },
                                    () => 
                                    {
                                        ExitTimeStopWithShakeAction();
                                        _doTweenSwitchWaveSequence = null; 
                                    });
        }
        // アクションによる縦揺れ
        public IObservable<bool> ShakeByAction()
        {
            return _shakeByVertical(_shakeByActionParam, (sequence) => { _doTweenSequence = sequence; }, () => { _doTweenSequence = null; });
        }

        public IObservable<bool> Shake()
        {
            var onShakeCompleteSubject = new Subject<bool>();
            _mainCamera.DOShakePosition(_shakeDuration, _shakeStrength, _shakeVibrato, 0).OnComplete(() =>
            {
                onShakeCompleteSubject.OnNext(true);
                onShakeCompleteSubject.OnCompleted();
            });
            return onShakeCompleteSubject.AsObservable();
        }

        public void EnterTimeStop()
        {
            if (_doTweenSequence != null) _doTweenSequence.Pause();
            if (_doTweenSwitchWaveSequence != null) _doTweenSwitchWaveSequence.Pause();
        }

        public void EnterTimeStopWithShakeAction()
        {
            if (_doTweenSequence != null) _doTweenSequence.Pause();
        }

        public void ExitTimeStop()
        {
            if (_doTweenSequence != null) _doTweenSequence.Play();
            if (_doTweenSwitchWaveSequence != null) _doTweenSwitchWaveSequence.Play();
        }
        public void ExitTimeStopWithShakeAction()
        {
            if (_doTweenSequence != null) _doTweenSequence.Play();
        }

        public void ClearShake()
        {
            if (_doTweenSequence != null)
            {
                _doTweenSequence.Complete();
                _doTweenSequence = null;
            }
            if (_doTweenSwitchWaveSequence != null)
            {
                _doTweenSwitchWaveSequence.Complete();
                _doTweenSwitchWaveSequence = null;
            }
        }

        public void AddChildToMainCamera(Transform transform)
        {
            transform.SetParent(_mainCameraTransform);
        }

        float _getCameraOrthographicSize(Vector2[] targetsPosition, Vector2 offset)
        {
            var vecDiff = targetsPosition[0] - targetsPosition[1];
            vecDiff = new Vector2(System.Math.Abs(vecDiff.x), System.Math.Abs(vecDiff.y)) + offset;
            var targetAspect = vecDiff.y / ((vecDiff.x == 0) ? 1 : vecDiff.x);

            var orthographicSize = 0.0f;
            if (_screentAspect < targetAspect)
            {
                orthographicSize = vecDiff.y * 0.5f;
            }
            else
            {
                orthographicSize = vecDiff.x * (1 / _mainCamera.aspect) * 0.5f;
            }

            return orthographicSize;
        }

        public IObservable<bool> MoveAndZoomWithActiveAction(Character targetCharacter, float duration)
        {
            // 描画エリアの端を指定したキャラクターのみに固定する
            // ここで更新しておくことでズームアウトをUpdate側の処理に任せられる
            _screenEndCharacters[Direction.Left] = _screenEndCharacters[Direction.Right] = 
                _screenEndCharacters[Direction.Up] = _screenEndCharacters[Direction.Down] = targetCharacter;

            var targetsPosition = new Vector2[]
            {
                ((CharacterView)targetCharacter.GetView()).GetDamageAreaLeftUpPosition().Value,
                ((CharacterView)targetCharacter.GetView()).GetDamageAreaRightDownPosition().Value
            };

            // 目測でダメージエリアの高さ * 4.0fが大中小それぞれちょうどいい値になったのでこれで固定とする
            var offset = System.Math.Abs(((CharacterView)targetCharacter.GetView()).GetDamageAreaHeight() * _activeActionCameraOffset);
            var activeActionCameraOffset = new Vector2(offset, offset);
            // オフセットの上限値を超えたら上限値に上書きする
            if (activeActionCameraOffset.x > ACTIVE_ACTION_CAMERA_OFFSET_Y) activeActionCameraOffset.x = ACTIVE_ACTION_CAMERA_OFFSET_Y;
            if (activeActionCameraOffset.y > ACTIVE_ACTION_CAMERA_OFFSET_Y) activeActionCameraOffset.y = ACTIVE_ACTION_CAMERA_OFFSET_Y;
            return _moveAndZoom(targetsPosition, duration, activeActionCameraOffset);
        }

        IObservable<bool> _moveAndZoom(Vector2[] targetsPosition, float duration, Vector2 offset)
        {
            _mainCamera.DOKill();
            _mainCameraTransform.DOKill();
            var newCameraPosition = Vector2.Lerp(targetsPosition[0], targetsPosition[1], 0.5f);
            var newOriginalSize = _getCameraOrthographicSize(targetsPosition, offset);
            newOriginalSize = newOriginalSize > _mainCameraOriginalSize ? _mainCameraOriginalSize : newOriginalSize;
            var newCameraVertices = _getCameraVertices(newOriginalSize, _mainCamera.aspect, newCameraPosition);
            newCameraPosition = _checkDefaultRectArea(newCameraVertices, newCameraPosition);
            return Observable.WhenAll(
                Move(newCameraPosition, duration, Ease.Linear),
                ZoomIn(newOriginalSize, duration)
            )
            .Select(_ => true);
        }

        // カメラのはみ出しをチェックする
        // はみ出していなければnewCameraPositionをそのまま返し
        // はみ出していたら領域内に収まるように補間してから返す
        Vector2 _checkDefaultRectArea(Vector2[] checkVertices, Vector2 newCameraPosition)
        {
            if (_defaultCameraVertices[0].x >= checkVertices[0].x && _defaultCameraVertices[0].y >= checkVertices[0].y &&
                _defaultCameraVertices[1].x <= checkVertices[1].x && _defaultCameraVertices[1].y >= checkVertices[1].y &&
                _defaultCameraVertices[2].x <= checkVertices[2].x && _defaultCameraVertices[2].y <= checkVertices[2].y &&
                _defaultCameraVertices[3].x >= checkVertices[3].x && _defaultCameraVertices[3].y <= checkVertices[3].y)
            {
                // デフォルトカメラ領域内に収まっている場合は座標をそのまま返す
                return newCameraPosition;
            }
            else
            {
                // 0 = 右上 1 = 左上 2 = 左下 3 = 右下
                // はみ出しているならどこがどれだけはみ出したかを探し出す
                // 上下左右を確認するが調査対象として右上及び左下の座標を利用する
                var distanceX = 0.0f;
                var distanceY = 0.0f;
                if (_defaultCameraVertices[0].y < checkVertices[0].y) distanceY = checkVertices[0].y - _defaultCameraVertices[0].y;
                if (_defaultCameraVertices[2].y > checkVertices[2].y) distanceY = checkVertices[2].y - _defaultCameraVertices[2].y;
                if (_defaultCameraVertices[0].x < checkVertices[0].x) distanceX = checkVertices[0].x - _defaultCameraVertices[0].x;
                if (_defaultCameraVertices[2].x > checkVertices[2].x) distanceX = checkVertices[2].x - _defaultCameraVertices[2].x;

                for (var i = 0; i < checkVertices.Length; i++)
                {
                    checkVertices[i] = checkVertices[i] - new Vector2(distanceX, distanceY);
                }

                // 補間された頂点情報から中心点を返す
                return Vector2.Lerp(checkVertices[0], checkVertices[2], 0.5f);
            }
        }
    }
}
