﻿using System;
using UnityEngine;
using UniRx;
using UniLinq;
using DG.Tweening;
using Noroshi.Core.Game.Character;
using CharacterConstant = Noroshi.Core.Game.Character.Constant;
using Noroshi.Grid;
using Noroshi.Character;
using Noroshi.BattleScene.Actions;
using System.Collections.Generic;

namespace Noroshi.BattleScene.MonoBehaviours
{
    /// バトル内のキャラクターアニメーションを扱うクラス。
    public class CharacterView : Noroshi.Character.CharacterView, ICharacterView
    {
        // キャラクターの前につけるエフェクトの位置
        enum EffectFrontLocalXType
        {
            Smal,       // チビキャラ
            Middle,     // 人型など
            Lrage,      // デカキャラ
            Original,   // オリジナル
        }

        const string DAMAGE_AREA_NAME = "damage_area";
        const float EFFECT_FROMT_LOCAL_SMAL_X = 2.0f;
        const float EFFECT_FRONT_LOCAL_MIDDLE_X = 4.0f;
        const float EFFECT_FRONT_LOCAL_LRAGE_X = 5.0f;

        [SerializeField] Transform _arm;
        [SerializeField] float _effectTopLocalY = 0;
        [SerializeField] float _effectFrontLocalX = 0;
        [SerializeField] float _effectScale = 1f;
        [SerializeField] Vector2[] _centerPositionsForUIWithStory;
        [SerializeField] Vector2 _centerPositionForUIWithResult;
        [SerializeField] float _scaleForUIWithStory = 1.0f;
        [SerializeField] float _scaleForUIWithResult = 1.0f;
        [SerializeField] EffectFrontLocalXType _effectFrontLocalXType = EffectFrontLocalXType.Middle;

        /// 当たり判定。
        BoundingBoxFollower _body;
        /// 当たり判定 Transform。頻繁に利用するのでキャッシュしておく。
        Transform _bodyTransform;
        ActionTargetView  _actionTargetView;
        /// アクション関連アニメーションが完了した際に、該当アニメーション正常終了是非が OnNext される Subject。
        Subject<bool> _onExitActionAnimation = new Subject<bool>();
        /// 死亡アニメーションが完了した際に、該当アニメーション正常終了是非が OnNext される Subject。
        Subject<bool> _onExitDeadAnimation;
        /// 死亡前逃亡アニメーションフラグ。
        bool _escapeBeforeDead;

        /// ストック時に、自身が OnNext される Subject。
        Subject<ICharacterView> _onStock = null;

        /// キャラクターの表示非表示を変更した時に OnNext される Subject。
        Subject<bool> _onSetVisible = new Subject<bool>();

        Vector2[] _damageAreaLocalPositions = null;
        float _damageAreaOffsetY = 0.0f;

        Dictionary<EffectFrontLocalXType, float> _effectFrontLocalXMap = new Dictionary<EffectFrontLocalXType, float>();

        new void Start()
        {
            // 各コンポーネントをキャッシュ
            _body = GetComponentInChildren<BoundingBoxFollower>();
            _bodyTransform = _body.transform;
            _actionTargetView = GetComponentInChildren<ActionTargetView>();

            _effectFrontLocalXMap.Add(EffectFrontLocalXType.Smal, EFFECT_FROMT_LOCAL_SMAL_X);
            _effectFrontLocalXMap.Add(EffectFrontLocalXType.Middle, EFFECT_FRONT_LOCAL_MIDDLE_X);
            _effectFrontLocalXMap.Add(EffectFrontLocalXType.Lrage, EFFECT_FRONT_LOCAL_LRAGE_X);
            _effectFrontLocalXMap.Add(EffectFrontLocalXType.Original, _effectFrontLocalX);

            // 共通 Start()。
            base.Start();
            // デフォルトアニメーション再生。
            PlayIdle();

            _getDamageArealocalPositions();
        }

        public override bool SetActive(bool active)
        {
            /// 当たり判定おかしい問題対応。
            if (_body != null)
            {
                GameObject.DestroyImmediate(_body);
                _body = _bodyTransform.gameObject.AddComponent<BoundingBoxFollower>();
                _body.slotName = DAMAGE_AREA_NAME;
            }
            return base.SetActive(active);
        }

        /// アクション関連アニメーションが完了した際に、該当アニメーション正常終了是非が OnNext される Observable を取得。
        public IObservable<bool> GetOnExitActionAnimationObservable()
        {
            return _onExitActionAnimation.AsObservable();
        }
        /// 弾丸ヒット時に弾丸ヒットイベントが OnNext される Observable を取得。
        public IObservable<BulletHitEvent> GetOnHitBulletObservable()
        {
            return _actionTargetView.GetOnHitObservable();
        }

        /// ストック時に、自身が OnNext される Observable を取得。
        public IObservable<ICharacterView> GetOnStockObservable()
        {
            if (_onStock == null) _onStock = new Subject<ICharacterView>();
            return _onStock.AsObservable();
        }

        /// キャラクターの表示非表示を変更した時に OnNext される Observable を取得。
        public IObservable<bool> GetOnSetVisibleObservable()
        {
            return _onSetVisible.AsObservable();
        }

        /// 表示・非表示する。
        public override void SetVisible(bool isVisible)
        {
            base.SetVisible(isVisible);
            _onSetVisible.OnNext(isVisible);
        }

        /// ポーズする。
        public void PauseOn()
        {
            _pauseOn();
            _soundPauseOn();
            SuspendMove();
        }
        /// ポーズを解除する。
        public void PauseOff()
        {
            ResumeMove();
            _soundPauseOff();
            _pauseOff();
        }

        public void PlayTransparency(bool isOn, float duration)
        {
            if (isOn)
            {
                _fadeOut(duration).Subscribe(_ => {});
            }
            else
            {
                _fadeIn(duration).Subscribe(_ => {});
            }
        }

        /// アニメーションスピードをセットする。
        public void SetSpeed(float speed)
        {
            _setTimeScale(speed);
        }

        /// 死亡前逃走アニメーションフラグを立てる。
        public void SetEscapeBeforeDeadOn()
        {
            _escapeBeforeDead = true;
        }

        /// 指定ローカル座標を取得する。
        public Vector2 GetLocalPosition(CharacterEffectAnimationPosition position)
        {
            var localPosition = default(Vector2);
            switch (position)
            {
                case CharacterEffectAnimationPosition.Top:
                    localPosition = GetBodyLocalPosition() + Vector2.up * (_effectTopLocalY > 0 ? _effectTopLocalY : GetBodyLocalPosition().y);
                    break;
                case CharacterEffectAnimationPosition.Center:
                    localPosition = GetBodyLocalPosition();
                    break;
                case CharacterEffectAnimationPosition.Bottom:
                    localPosition = Vector2.zero;
                    break;
                case CharacterEffectAnimationPosition.Front:
                    localPosition = GetBodyLocalPosition() + Vector2.left * _effectFrontLocalXMap[_effectFrontLocalXType];
                    break;
                default:
                    break;
            }
            return localPosition;
        }
        /// 本体のローカル座標を取得する。
        public Vector2 GetBodyLocalPosition()
        {
            return _bodyTransform.localPosition;
        }

        // ダメージエリアを取得する
        // isReacquisition : すでに_damageAreaLocalPositionsに値が入っていても再取得を行うか
        Vector2[] _getDamageArealocalPositions(bool isReacquisition = false)
        {
            if (_damageAreaLocalPositions != null && !isReacquisition) return _damageAreaLocalPositions;
            if (_body == null || _body.CurrentCollider == null) return null;
            var localPositions = _body.CurrentCollider.GetPath(0).Select(position => 
            {
                // 親のTransformが回転しているケースがあるので、ここでY軸分の回転角度をかけて座標を再計算
                var newPos = Quaternion.Euler(0, _transform.eulerAngles.y, 0) * position;
                return new Vector2(newPos.x, newPos.y);
            })
            .ToArray();
            // 0 = 右上 1 = 左上 2 = 左下 3 = 右下
            // 取得したポリゴンの頂点座標はローカル座標なので、一度ワールド座標時の四隅の座標に変換
            var worldPositions = localPositions
                .Select(localposition => _getWorldPositionToTransformPoint(localposition))
                .Select(worldPosition => new Vector2(worldPosition.x, worldPosition.y))
                .ToArray();
            // その状態で上下左右の最大、最小値を割り出し四隅の座標を割り出す
            var rightPos  = worldPositions.Max(v => v.x);
            var upPos     = worldPositions.Max(v => v.y);
            var leftPos   = worldPositions.Min(v => v.x);
            var bottomPos = worldPositions.Min(v => v.y);
            var rightUpPosition     = new Vector2(rightPos, upPos);
            var leftUpPosition      = new Vector2(leftPos , upPos);
            var leftBottomPosition  = new Vector2(leftPos , bottomPos);
            var rightBottomPosition = new Vector2(rightPos, bottomPos);
            // Viewの原点から、ダメージエリアのY軸最小値の差分をとる
            _damageAreaOffsetY = System.Math.Abs(_transform.position.y - rightBottomPosition.y);
            // 最終的にはローカルへ変換し直して保持ておく
            _damageAreaLocalPositions = new Vector2[]
            {
                _getInverseTransformPoint2D(rightUpPosition),
                _getInverseTransformPoint2D(leftUpPosition),
                _getInverseTransformPoint2D(leftBottomPosition),
                _getInverseTransformPoint2D(rightBottomPosition)
            };
            return _damageAreaLocalPositions;
        }

        // ワールド座標をVector2型のローカル座標に変換して返す
        Vector2 _getInverseTransformPoint2D(Vector2 worldPos)
        {
            var localPosition = _transform.InverseTransformPoint(worldPos);
            var bodylocalPosition = _bodyTransform.InverseTransformPoint(worldPos);
            return new Vector2(localPosition.x, bodylocalPosition.y);
        }

        Vector3 _getWorldPositionToTransformPoint(Vector3 localPosition)
        {
            // X軸は親のTransformに追従し、Y軸はBodyのTransformに追従する
            var worldPositioin = _transform.TransformPoint(localPosition);
            var bodyWorldPosition = _bodyTransform.TransformPoint(localPosition);
            return new Vector3(worldPositioin.x, bodyWorldPosition.y, localPosition.z);
        }

        // ダメージエリア左上の座標をワールド座標に直した上で渡す
        public Vector2? GetDamageAreaLeftUpPosition()
        {
            var damageAreaLocalPositions = _getDamageArealocalPositions();
            if (_transform != null && damageAreaLocalPositions != null)
            {
                var damageAreaLeftUpWorldPosition = _getWorldPositionToTransformPoint(damageAreaLocalPositions[1]);
                return new Vector2(damageAreaLeftUpWorldPosition.x, damageAreaLeftUpWorldPosition.y + _damageAreaOffsetY);
            }
            return null;
        }

        // ダメージエリア右下の座標をワールド座標に直した上で渡す
        public Vector2? GetDamageAreaRightDownPosition()
        {
            var damageAreaLocalPositions = _getDamageArealocalPositions();
            if (_transform != null && damageAreaLocalPositions != null)
            {
                var damageAreaRightDownWorldPosition = _getWorldPositionToTransformPoint(damageAreaLocalPositions[3]);
                return new Vector2(damageAreaRightDownWorldPosition.x, damageAreaRightDownWorldPosition.y + _damageAreaOffsetY);
            }
            return null;
        }

        // ダメージエリアの各頂点をワールド座標へ変換して返す
        public Vector2[] GetDamageAreaWorldPositions()
        {
            // ローカル位置からbodyの現在位置に伴ったワールド座標へ変換(Vector2形式で)
            var damageAreaWorldPositions = _getDamageArealocalPositions()
                .Select(localposition => _getWorldPositionToTransformPoint(localposition))
                .Select(worldPosition => new Vector2(worldPosition.x, worldPosition.y + _damageAreaOffsetY))
                .ToArray();
            return damageAreaWorldPositions;
        }

        public float GetDamageAreaHeight()
        {
            var leftUpPosition = GetDamageAreaLeftUpPosition();
            var rightBottomPosition = GetDamageAreaRightDownPosition();
            if (!leftUpPosition.HasValue || !rightBottomPosition.HasValue) return 0.0f;
            return System.Math.Abs(leftUpPosition.Value.y - rightBottomPosition.Value.y);
        }

        /// 頭上座標を取得する。
        public Vector2 GetTopPosition(Vector2 offsetPosition)
        {
            var dir = 0.0f;
            switch((int)_transform.eulerAngles.y)
            {
            case 0:
                dir = 1.0f;
                break;
            case 180:
                dir = -1.0f;
                break;
            default:
                throw new NotImplementedException();
            }
            var localPosition = (GetLocalPosition(CharacterEffectAnimationPosition.Top) + offsetPosition) * SCALE;
            localPosition.Set(localPosition.x * dir, localPosition.y);
            return GetPosition() + localPosition;
        }

        /// 飛び道具発射座標を取得する。
        public Vector2 GetArmPosition()
        {
            return _arm.position;
        }

        /// エフェクト倍率を取得する。
        public float GetEffectScale()
        {
            return _effectScale;
        }

        /// アクションターゲット用 View を取得する。
        public IActionTargetView GetActionTargetView()
        {
            return _actionTargetView;
        }

        /// 指定ポジションまで歩いて移動する。
        public IObservable<bool> WalkTo(Vector2 position, float duration)
        {
            PlayWalk();
            return Move(position, duration).Do(_ => PlayIdle());
        }
        /// 指定ポジションまで走って移動する。
        public IObservable<bool> RunTo(Vector2 position, float duration)
        {
            PlayRun();
            return Move(position, duration).Do(_ => PlayIdle());
        }

        /// 待機アニメーションを再生する。
        public void PlayIdle() { PlayAnimation(CharacterAnimation.Idle, true).Subscribe(_ => {}); }
        /// 歩きアニメーションを再生する。
        public void PlayWalk() { PlayAnimation(CharacterAnimation.Walk, true).Subscribe(_ => {}); }
        /// 走りアニメーションを再生する。
        public void PlayRun() { PlayAnimation(CharacterAnimation.Run, true).Subscribe(_ => {}); }
        /// アクションアニメーションを再生する。
        public void PlayAction(CharacterAnimationEvent actionAnimationEvent)
        {
            PlayAnimation(actionAnimationEvent.CharacterAnimation, actionAnimationEvent.Loop, actionAnimationEvent.AnimationNameSuffix).Subscribe(_onExitActionAnimation.OnNext);
        }
        /// 被ダメージアニメーションを再生する。
        public void PlayDamage() { PlayAnimation(CharacterAnimation.Damage, false).Subscribe(_ => {}); }
        /// ストップアニメーションを再生する。
        public void PlayStop(bool stopAnimationIsLoop)
        {
            if (stopAnimationIsLoop)
            {
                PlayAnimation(CharacterAnimation.Idle, true).Subscribe(_ => {});
            }
            else
            {
                _stopAtAnimationHead(CharacterConstant.ANIMATION_NAME_MAP[CharacterAnimation.Idle]);
            }
        }
        /// ノックバックアニメーションを再生する。
        public void PlayKnockback(Vector2 position, float duration, float diffrenceY)
        {
            PlayDamage();
            StopMove();
            var firstDuration = duration / 2;
            _addMoveTweener(_transform.DOLocalMoveX(position.x, duration));
            _addMoveTweener(_transform.DOLocalMoveY(position.y + diffrenceY, firstDuration).SetEase(Ease.OutExpo));
            _addMoveTweener(_transform.DOLocalMoveY(position.y, duration - firstDuration).SetDelay(firstDuration).SetEase(Ease.OutBounce));
        }
        /// 竜巻時のアニメーションを再生する。
        public void PlayTornado()
        {
            var position = GetPosition();
            PlayDamage();
            StopMove();
            var firstDuration = Constant.TORNADO_FIRST_MOVE_TIME / 2;
            _addMoveTweener(_transform.DOLocalMoveY(position.y + Constant.TORNADO_FIRST_MOVE_DISTANCE, firstDuration).SetEase(Ease.OutCubic));
            _addMoveTweener(_transform.DOLocalMoveY(position.y, Constant.TORNADO_FIRST_MOVE_TIME - firstDuration).SetDelay(firstDuration).SetEase(Ease.InCubic));
            var secondDuration = Constant.TORNADO_SECOND_MOVE_TIME / 2;
            _addMoveTweener(_transform.DOLocalMoveY(position.y + Constant.TORNADO_SECOND_MOVE_DISTANCE, secondDuration).SetDelay(Constant.TORNADO_FIRST_MOVE_TIME).SetEase(Ease.OutCubic));
            _addMoveTweener(_transform.DOLocalMoveY(position.y, Constant.TORNADO_SECOND_MOVE_TIME - secondDuration).SetDelay(Constant.TORNADO_FIRST_MOVE_TIME + secondDuration).SetEase(Ease.InCubic));


        }
        /// 死亡状態に遷移した際のアニメーションを再生する。
        public IObservable<bool> PlayAnimationAtEnterDead()
        {
            _onExitDeadAnimation = new Subject<bool>();
            /// 死亡前逃走アニメーションフラグが立っている場合、
            /// ダメージアニメーションを一回再生後、待機アニメーションをループ再生。
            /// アニメーション完了時の OnNext はここでせず、死亡前逃走アニメーション再生に任せる。
            if (_escapeBeforeDead)
            {
                PlayAnimation(CharacterAnimation.Damage, false)
                .SelectMany(PlayAnimation(CharacterAnimation.Idle, true))
                .Subscribe(_ => {});
            }
            else
            {
                /// 死亡アニメーション再生後にフェードアウト。完了後にアニメーション完了時の OnNext をする。
                PlayAnimation(CharacterAnimation.Dead, false).SelectMany(_ => _fadeOut())
                .Do(_ => PlayIdle()).DelayFrame(2)
                .Subscribe(_ =>
                {
                   _onExitDeadAnimation.OnNext(true);
                   _onExitDeadAnimation.OnCompleted(); 
                });
            }
            return _onExitDeadAnimation.AsObservable();
        }
        /// 死亡前逃走アニメーションを再生する。
        public void PlayEscapeBeforeDead()
        {
            SetHorizontalDirection(Direction.Right);
            PlayRun();
            Move(GetPosition() + Vector2.right * WaveField.HORIZONTAL_LENGTH * 2, Constant.BOSS_ESCAPE_TIME)
            .Subscribe(_ =>
            {
               _onExitDeadAnimation.OnNext(true);
               _onExitDeadAnimation.OnCompleted(); 
            });
        }
        /// 逃走アニメーションを再生する。
        public IObservable<bool> PlayEscape() { return PlayAnimation(CharacterAnimation.Escape, false); }
        /// 勝利アニメーションを再生する。
        public IObservable<bool> PlayWin() { return PlayAnimation(CharacterAnimation.Win, false); }
        /// 死亡アニメーション再生後に復活させる。
        public void Resurrect()
        {
            _recoverFromFadeOut();
        }

        // キャラクターの向きを変更する
        public override void SetHorizontalDirection(Direction direction)
        {
            base.SetHorizontalDirection(direction);
            // 向きが変更されたことによりコライダーの頂点座標も変更されるので、ここで再取得
            _getDamageArealocalPositions(true);
        }

        public Vector2[] GetStoryPositions()
        {
            return _centerPositionsForUIWithStory;
        }

        /// UI レイヤーに表示するために変換する。
        public void SetUpForUI(CenterPositionForUIType centerPositionForUIType)
        {
            var scaleForUI = 0.0f;
            var centerPositionForUI = Vector2.zero;
            switch (centerPositionForUIType)
            {
            case CenterPositionForUIType.Story:
                centerPositionForUI = _centerPositionsForUIWithStory[0];
                scaleForUI = _scaleForUIWithStory;
                break;
            case CenterPositionForUIType.Result:
                centerPositionForUI = _centerPositionForUIWithResult;
                scaleForUI = _scaleForUIWithResult;
                // ストーリー中は任意にSortOrderを切り替えるのでここで設定する場合はResultで使用する場合のみ
                SetOrderInLayer(Constant.SPINE_UI_ORDER_IN_LAYER);
                break;
            default:
                throw new ArgumentOutOfRangeException();
            }
            _transform.localScale = Vector3.one * scaleForUI;
            _transform.localPosition = new Vector3(-centerPositionForUI.x , -centerPositionForUI.y, _transform.localPosition.z);
            SetSortingLayerName(Constant.UI_SORTING_LAYER_NAME);
            SetLayer(_gameObject, "UI", true);
        }

        /// ストックする。
        public void Stock()
        {
            if (_onStock != null)
            {
                _onStock.OnNext(this);
                _onStock.OnCompleted();
                _onStock = null;
            }

            SetActive(false);
        }
    }
}
