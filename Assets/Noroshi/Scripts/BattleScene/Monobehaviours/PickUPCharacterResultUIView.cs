using System;
using UniRx;
using UnityEngine;
using DG.Tweening;
using Noroshi.Grid;

namespace Noroshi.BattleScene.MonoBehaviours
{
    /// <summary>
    /// バトル結果時に表示するピックアップキャラクター UI の View を扱うクラス。
    /// </summary>
    public class PickUPCharacterResultUIView : UIView
    {
        [SerializeField] RectTransform _topLine;
        [SerializeField] RectTransform _bottomLine;
        [SerializeField] float _activeCharacterPositionX = 0f;
        [SerializeField] float _stopCharacterPositionX = -4f;
        [SerializeField] float _lineMoveDuration = 0.5f;
        [SerializeField] float _characterViewSlideDuration = 0.3f;

        Vector2 _topLineAnchoredPosition;
        Vector2 _bottomLineAnchoredPosition;
        Vector2 _topLineHiddenAnchoredPosition;
        Vector2 _bottomLineHiddenAnchoredPosition;

        new void Awake()
        {
            base.Awake();
            // 各座標情報をキャッシュ。
            _topLineAnchoredPosition = _topLine.anchoredPosition;
            _bottomLineAnchoredPosition = _bottomLine.anchoredPosition;
            _topLineHiddenAnchoredPosition = _topLine.anchoredPosition + Vector2.up * _topLine.rect.height;
            _bottomLineHiddenAnchoredPosition = _bottomLine.anchoredPosition + Vector2.down * _bottomLine.rect.height;
            // 上下ラインは隠しておく。
            _hideLines();
            // 最初は非アクティブ。
            SetActive(false);
        }
        /// 上下ラインを隠す。
        void _hideLines()
        {
            _topLine.anchoredPosition = _topLineHiddenAnchoredPosition;
            _bottomLine.anchoredPosition = _bottomLineHiddenAnchoredPosition;
        }

        /// <summary>
        /// アクティブ化。
        /// </summary>
        /// <param name="characterView">ピックアップキャラクターのビュー</param>
        /// <param name="isOwnCharacter">ピックアップキャラクターが自キャラクターかどうか</param>
        public IObservable<bool> Activate(ICharacterView characterView, bool isOwnCharacter)
        {
            SetActive(true);
            // 上下ラインを表示。
            // 移動終了後ウェイトを待たずにアニメーションを再生しつつ次へ
            return _displayLines()
                .SelectMany(_ => _displayCharacter(characterView, isOwnCharacter))
                .Do(_ => _playCharacterWinAnimation(characterView))
                .Select(_ => true);
        }

        /// 上下ラインを表示する。
        IObservable<bool> _displayLines()
        {
            var onComplete = new Subject<bool>();
            _topLine.DOAnchorPos(_topLineAnchoredPosition, _lineMoveDuration).OnComplete(() =>
            {
                onComplete.OnNext(true);
                onComplete.OnCompleted();
            });
            _bottomLine.DOAnchorPos(_bottomLineAnchoredPosition, _lineMoveDuration);
            return onComplete.AsObservable();
        }

        /// キャラクターを表示する。
        IObservable<ICharacterView> _displayCharacter(ICharacterView characterView, bool isOwnCharacter)
        {
            if (characterView == null) return Observable.Return<ICharacterView>(null);
            characterView.SetUpForUI(CenterPositionForUIType.Result);
            characterView.SetHorizontalDirection(isOwnCharacter ? Direction.Right : Direction.Left);
            characterView.SetActive(true);
            var sign = isOwnCharacter ? 1 : -1;
            return _moveLocalX(characterView, _activeCharacterPositionX * sign, _stopCharacterPositionX * sign, isOwnCharacter);
        }
        IObservable<ICharacterView> _moveLocalX(ICharacterView characterView, float startValue, float endValue, bool isOwnCharacter)
        {
            // TODO
            ((CharacterView)characterView).SetParent(UnityEngine.Camera.main.transform);
            characterView.SetUpForUI(CenterPositionForUIType.Result);

            var onComplete = new Subject<ICharacterView>();
            var prevLocalPosition = ((CharacterView)characterView).GetTransform().localPosition;
            ((CharacterView)characterView).GetTransform().localPosition = new Vector3(startValue, prevLocalPosition.y, -UnityEngine.Camera.main.transform.position.z);
            ((CharacterView)characterView).GetTransform().DOLocalMoveX(endValue - (endValue + prevLocalPosition.x * (isOwnCharacter ? 1 : -1)), _characterViewSlideDuration).OnComplete(() =>
            {
                onComplete.OnNext(characterView);
                onComplete.OnCompleted();
            });
            return onComplete.AsObservable();
        }

        /// キャラクターの勝利アニメーションを再生する。
        void _playCharacterWinAnimation(ICharacterView characterView)
        {
            if (characterView == null) return;
            characterView.PlayWin().Subscribe(_ => {}).AddTo(_gameObject);
        }
    }
}
