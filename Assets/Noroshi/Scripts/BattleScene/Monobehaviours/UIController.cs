﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;
using DG.Tweening;
using Noroshi.BattleScene.UI;
using Noroshi.Core.Game.Battle;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class UIController : Noroshi.MonoBehaviours.MonoBehaviour, IUIController
    {
        const float HEADER_AND_FOOTER_MOVE_DURATION = 0.5f;
        const float DARKEN_ALPHA = 0.7f;
        const float DARKEN_LIGHTEN_DURATION = 0.5f;
        [SerializeField] UnityEngine.Camera _uiCamera;
        [SerializeField] RectTransform _headerContainer;
        [SerializeField] RectTransform _footerContainer;
        [SerializeField] UIView _modalContainer;
        [SerializeField] UIView _bottomPanelContainer;
        [SerializeField] UnityEngine.UI.Text _gainMoneyNum;
        [SerializeField] UnityEngine.UI.Text _gainItemNum;
        [SerializeField] UnityEngine.UI.Text _currentWaveNum;
        [SerializeField] UnityEngine.UI.Text _maxWaveNum;
        [SerializeField] UIView _nextWaveButton;
        [SerializeField] TimeUIView _timeUIView;
        [SerializeField] SwapImageToggleView _autoModeToggleView;
        [SerializeField] UIView _worldUICanvas;
        [SerializeField] UIView _readyMessage;
        [SerializeField] BattleEffectUIView _battleEffectUIView;
        [SerializeField] BattleStartUIView _battleStartUIView;
        [SerializeField] ChapterUIView _chapterUIView;
        [SerializeField] DarkScreenUIView _darkScreenUIView;
        [SerializeField] PickUPCharacterResultUIView _pickUpCharacterResultUiView;

        [SerializeField] UIView _headerWaveImageUIView;
        [SerializeField] WaveGaugeUIView _waveGaugeUIView;
        [SerializeField] UIView _textUICanvas;

        [SerializeField] TrainingScoreUIView _trainingScoreUIView;
        [SerializeField] UnityEngine.UI.Text _trainingWaveNum;
        // アクティブ制御用
        [SerializeField] UIView _otherWaveNumGroup;

        [SerializeField] ActiveActionDescriptionUIView _activeActionDescriptionUIView = null;
        [SerializeField] Noroshi.UI.PlayerLevelUpPanel _playerLevelUpPanel;
        [SerializeField] AnimatorUIView _sceneTransitionUI;

        [SerializeField] Transform _enterEffectFrameGroupTransform;

        Noroshi.UI.UILoading _uiLoading = null;

        Vector2 _headerAnchoredPosition;
        Vector2 _footerAnchoredPosition;
        sbyte _darkScreenCallCount = 0;
        sbyte _deactiveHeaderAndFooterCallCount = 0;

        Subject<bool> _onNextWaveSubject = new Subject<bool>();
        Subject<bool> _onPauseSubject = new Subject<bool>();
        Subject<bool> _onToggleAutoModeSubject = new Subject<bool>();
        CompositeDisposable _disposables = new CompositeDisposable();

        // UI用のローディング画面をバトルでも使用できるようにカメラを変更する
        void _setupUILoading()
        {
            // インスタンスを取得
            _uiLoading = Noroshi.UI.UILoading.Instance;
            // 描画対象のカメラを設定
            if (_uiLoading == null)
            {
                // もし空っぽだった場合取ってくる(チュートリアルなど)
                var uiLoadingObj = GameObject.FindGameObjectWithTag("UILoading");
                if (uiLoadingObj != null)
                {
                    _uiLoading = uiLoadingObj.GetComponent<Noroshi.UI.UILoading>();
                }
            }
            _uiLoading.Canvas.worldCamera = _uiCamera;
        }

        new void Awake()
        {
            base.Awake();
            SetCurrentMoneyNum(0);
            SetCurrentItemNum(0);
            _autoModeToggleView.SetActive(false);
            _darkScreenCallCount = 0;
            _deactiveHeaderAndFooterCallCount = 0;
            _trainingWaveNum.gameObject.SetActive(false);
        }
        void Start()
        {
            // 処理順として Awake -> OnLevelWasLoaded -> Start となっているので
            // ここでUILoadingのCanvasに対しUICameraを設定する
            _setupUILoading();
            _headerAnchoredPosition = _headerContainer.anchoredPosition;
            _footerAnchoredPosition = _footerContainer.anchoredPosition;

            _readyMessage.SetActive(false);
        }

        public void DeactiveLoadingUIView()
        {
            if (_uiLoading != null) _uiLoading.HideLoading();
        }

        public IObservable<bool> ActivateHeaderAndFooter()
        {
            if (--_deactiveHeaderAndFooterCallCount > 0) return Observable.Return<bool>(true);
            var onComplete = new Subject<bool>();
            _headerContainer.DOAnchorPos(_headerAnchoredPosition, HEADER_AND_FOOTER_MOVE_DURATION).OnComplete(() =>
            {
                onComplete.OnNext(true);
                onComplete.OnCompleted();
            });
            _footerContainer.DOAnchorPos(_footerAnchoredPosition, HEADER_AND_FOOTER_MOVE_DURATION);
            return onComplete.AsObservable();
        }
        public IObservable<bool> DeactivateHeaderAndFooter()
        {
            ++_deactiveHeaderAndFooterCallCount;
            var onComplete = new Subject<bool>();
            _headerContainer.DOAnchorPos(_headerAnchoredPosition + Vector2.up * _headerContainer.rect.height, HEADER_AND_FOOTER_MOVE_DURATION).OnComplete(() =>
            {
                onComplete.OnNext(true);
                onComplete.OnCompleted();
            });
            _footerContainer.DOAnchorPos(_footerAnchoredPosition + Vector2.down * _footerContainer.rect.height, HEADER_AND_FOOTER_MOVE_DURATION);
            return onComplete.AsObservable();
        }

        public void AddModalUIView(IModalUIView uiView)
        {
            uiView.SetParent(_modalContainer, false);
        }
        public void AddResultUIView(IResultUIView uiView)
        {
            uiView.SetParent(_modalContainer, false);
        }
        public void AddPlayerCharacterPanelUI(IOwnCharacterPanelUIView uiView)
        {
            uiView.SetParent(_bottomPanelContainer, false);
            uiView.SetParentEnterActiveActionEffectFrame(_enterEffectFrameGroupTransform);
        }
        public void SetCurrentMoneyNum(uint num)
        {
            _gainMoneyNum.text = num.ToString();
        }
        public void SetCurrentItemNum(byte num)
        {
            _gainItemNum.text = num.ToString();
        }

        public void SetCurrentWaveNum(int num)
        {
            _currentWaveNum.text = num.ToString();
            _trainingWaveNum.text = num <= Constant.LOOP_BATTLE_MAX_WAVE_NUM_FOR_DISPLAY ? num.ToString() : Constant.LOOP_BATTLE_MAX_WAVE_NUM_FOR_DISPLAY.ToString();
        }
        public void SetMaxWaveNum(int num)
        {
            _maxWaveNum.text = num.ToString();
        }

        public void SetNextWaveButtonVisible(bool visible)
        {
            _nextWaveButton.SetActive(visible);
        }

        public void InitializeWaveGaugeView(byte? level, string textKey, uint nowHP, uint maxHP, Noroshi.Core.Game.Battle.WaveGaugeType waveGaugeType)
        {
            _waveGaugeUIView.SetActive(true);
            _headerWaveImageUIView.SetActive(false);
            _waveGaugeUIView.Initialize(level.Value, textKey, (float)nowHP / maxHP, waveGaugeType);
        }

        public void ChangeWaveGauge(float ratio)
        {
            _waveGaugeUIView.ChangeHpRatio(ratio);
        }

        public void InitializeTrainingScoreUIView(bool isLoopBattle)
        {
            _trainingScoreUIView.SetActive(true);
            _trainingWaveNum.gameObject.SetActive(true);
            _otherWaveNumGroup.SetActive(false);
            if (isLoopBattle)
            {
                _trainingScoreUIView.SetLoopBattleData();
            }
            else
            {
                _trainingScoreUIView.SetUnlimitedHPEnemyBattleData();
            }
        }

        public void UpdateTrainingScore(uint score)
        {
            _trainingScoreUIView.UpdateScore(score);
        }

        public IObservable<bool> GetOnClickNextWaveButtonObservable()
        {
            return _onNextWaveSubject.AsObservable();
        }
        public void ClickNextWaveButton()
        {
            _onNextWaveSubject.OnNext(true);
        }

        public IObservable<bool> GetOnClickPauseButtonObservable()
        {
            return _onPauseSubject.AsObservable();
        }
        public void ClickPauseButton()
        {
            _onPauseSubject.OnNext(true);
        }

        public IObservable<bool> GetOnToggleAutoModeObservable()
        {
            _autoModeToggleView.SetActive(true);
            return _autoModeToggleView.GetOnToggleObservable()
            .Do(isOn =>
            {
                if (isOn) _onNextWaveSubject.OnNext(true);
            });
        }

        public void UpdateTime(int time, bool isTrainingBattle)
        {
            _timeUIView.UpdateTime(time, isTrainingBattle);
        }

        public void SetToWorldUICanvas(MonoBehaviours.IUIView uiView)
        {
            uiView.SetParent(_worldUICanvas, false);
        }

        public IUIView GetTextUICanvas()
        {
            return _textUICanvas;
        }

        public IObservable<bool> GetOnEnterBattleEffectFrameOutObservable()
        {
            return _battleEffectUIView.GetOnEnterFrameOutObservable();
        }
        /// サウンド操作時に操作内容が OnNext される Observable を取得。
        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _battleEffectUIView.GetOnCommandSoundObservable();
        }
        public IObservable<bool> PlayAnimationDefaultVictory()
        {
            return _battleEffectUIView.PlayAnimationDefaultVictory();
        }
        public IObservable<bool> PlayAnimationPlayerBattleStart()
        {
            return _battleEffectUIView.PlayAnimationPlayerBattleStart();
        }
        public IObservable<bool> PlayAnimationPlayerBattleVictory()
        {
            return _battleEffectUIView.PlayAnimationPlayerBattleVictory();
        }
        public IObservable<bool> PlayAnimationTimeUp()
        {
            return _battleEffectUIView.PlayAnimationTimeUp();
        }
        public IObservable<bool> PlayAnimationTrainingStart()
        {
            return _battleEffectUIView.PlayAnimationTrainingStart();
        }

        public IObservable<IUIController> ActivateChapterUIView(string titleTextKey, int[] titleParams, string subTitleTextKey, int[] subTitleParams)
        {
            return _chapterUIView.Activate(titleTextKey, titleParams, subTitleTextKey, subTitleParams).Select(_ => (IUIController)this);
        }

        public IObservable<bool> ActivateBattleStartUIView(bool isPlayerBattle)
        {
            return _battleStartUIView.Activate(isPlayerBattle);
        }
        public IObservable<bool> DeactivateBattleStartUIView(bool isPlayerBattle)
        {
            return _battleStartUIView.Deactivate(isPlayerBattle);
        }

        public void SetPlayerNames(string ownPlayerName, string enemyPlayerName)
        {
            _battleStartUIView.SetPlayerNames(ownPlayerName, enemyPlayerName);
        }

        public void SetActiveActionDescription(uint? activeSkillId)
        {
            _activeActionDescriptionUIView.SetActiveSkillDescription(activeSkillId);
        }

        public IObservable<bool> DarkenWorld()
        {
            ++_darkScreenCallCount;
            return _darkScreenUIView.Activate(DARKEN_ALPHA, DARKEN_LIGHTEN_DURATION).Select(_ => true);
        }

        public IObservable<bool> LightenWorld()
        {
            if (--_darkScreenCallCount > 0) return Observable.Return<bool>(true);
            return _darkScreenUIView.Deactivate(DARKEN_LIGHTEN_DURATION).Select(_ => true);
        }

        /// <summary>
        /// ピックアップキャラクター UI をアクティブ化。
        /// </summary>
        /// <param name="characterView">ピックアップキャラクターのビュー</param>
        /// <param name="isOwnCharacter">ピックアップキャラクターが自キャラクターかどうか</param>
        public IObservable<bool> ActivatePickUPCharacterResultUI(ICharacterView characterView, bool isOwnCharacter)
        {
            return _pickUpCharacterResultUiView.Activate(characterView, isOwnCharacter);
        }

        /// プレイヤーレベルアップモーダルを開く。
        public IObservable<bool> OpenPlayerLevelUPModal(Core.WebApi.Response.Players.AddPlayerExpResult addPlayerExpResult)
        {
            // バトル外の Prefab を使うので外から無理やりいじって表示上問題ないようにする。
            _playerLevelUpPanel.gameObject.GetComponent<Canvas>().sortingLayerName = "UI";
            _playerLevelUpPanel.ShowPanel(addPlayerExpResult);
            return _playerLevelUpPanel.OnDisableAsObservable().Select(_ => true);
        }
        /// レイドボス出現モーダルを開く。
        public IObservable<bool> OpenRaidBossModal(Core.WebApi.Response.RaidBoss.RaidBossAtDiscovery raidBoss)
        {
            // バトル外の Prefab を使うので外から無理やりいじって表示上問題ないようにする。
            var appearModal = Instantiate(Resources.Load<Noroshi.UI.AppearRaidBossModal>("UI/AppearRaidBossModal"));
            appearModal.OpenModal(raidBoss, _uiCamera);
            appearModal.gameObject.GetComponent<Canvas>().sortingLayerName = "UI";
            foreach (var renderer in appearModal.gameObject.GetComponentsInChildren<MeshRenderer>())
            {
                renderer.sortingLayerName = "UI";
                renderer.gameObject.layer = LayerMask.NameToLayer("UI");
            }
            return appearModal.OnDisableAsObservable().Select(_ => true);
        }
        /// <summary>
        /// シーン遷移時の UI をアクティブ化する。
        /// </summary>
        public IObservable<bool> ActivateSceneTransitionUI()
        {
            return _sceneTransitionUI.PlayAnimation();
        }

        void OnDestroy()
        {
            _uiLoading = null;
            _disposables.Dispose();
        }
    }
}
