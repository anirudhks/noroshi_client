﻿using UniRx;
using UnityEngine;

namespace Noroshi.BattleScene.MonoBehaviours
{
    [RequireComponent(typeof(Animator))]
    public class AnimatorUIView : UIView 
    {
        Animator _animator;
        Subject<bool> _onExitAnimation;

        new void Awake()
        {
            base.Awake();
            _animator = GetComponent<Animator>();
            _animator.enabled = false;
        }

        void OnDestroy()
        {
            if (_onExitAnimation != null) _onExitAnimation.OnCompleted();
        }

        /// アニメーション再生。
        public IObservable<bool> PlayAnimation()
        {
            _onExitAnimation = new Subject<bool>();
            _animator.enabled = true;
            return _onExitAnimation.AsObservable();
        }

        /// アニメーション終了時に呼ばれるコールバック。
        void OnExitAnimation()
        {
            _onExitAnimation.OnNext(true);
            _onExitAnimation.OnCompleted();
        }
    }
}
