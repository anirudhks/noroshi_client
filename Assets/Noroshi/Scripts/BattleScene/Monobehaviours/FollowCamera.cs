﻿using UnityEngine;

namespace Noroshi.BattleScene.MonoBehaviours
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    public class FollowCamera : UnityEngine.MonoBehaviour
    {
        [SerializeField] UnityEngine.Camera _originCamera;

        UnityEngine.Camera _selfCamera = null;

        void Start()
        {
            _selfCamera = GetComponent<UnityEngine.Camera>();
        }

        void Update()
        {
            if (_selfCamera == null) return;
            // このカメラのサイズを対象のカメラサイズに合わせる
            _selfCamera.orthographicSize = _originCamera.orthographicSize;
        }
    }
}
