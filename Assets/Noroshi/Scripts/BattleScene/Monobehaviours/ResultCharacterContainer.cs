﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Flaggs.Extensions.Heam;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class ResultCharacterContainer : UIView
    {
        [SerializeField] ResultCharacterUIView[] _ownCharacters;

        void Start()
        {
            SetActive(false);
        }

        /// 必要なアセットをロード。
        public IObservable<bool> LoadAssets()
        {
            return _ownCharacters.Select(ui => ui.LoadSprite()).WhenAll().Select(_ => true);
        }

        /// 自キャラクターサムネイル情報をセット。
        public void SetCharacterThumbnails(IEnumerable<CharacterThumbnail> characterThumbnail)
        {
            var characterNum = characterThumbnail.Count();
            var characterThumbnailArray = characterThumbnail.ToArrayWithOptimization();
            for (byte no = 1; no <= _ownCharacters.Length; no++)
            {
                if (no <= characterNum)
                {
                    var characterThumnail = characterThumbnailArray[no - 1];
                    _ownCharacters[no - 1].SetActive(true);
                    _ownCharacters[no - 1].SetCharacterThumbnail(characterThumnail);
                }
                else
                {
                    _ownCharacters[no - 1].SetActive(false);
                }
            }
        }

        /// 自キャラクターコンテナ表示を試行。
        public IObservable<bool> TryToDisplayCharacterContainer()
        {
            SetActive(true);
            // アクティブ化後の初期化を待つ意味も含め、タイマー処理。
            return Observable.Timer(System.TimeSpan.FromSeconds(0.1f))
            // 全自キャラクター UI 演出。
            .SelectMany(_ =>
            {
                return _ownCharacters
                .Where(ownCharacter => ownCharacter.isActiveAndEnabled)
                .Select(ownCharacter => ownCharacter.PlaySliderAnimation())
                .WhenAll();
            })
            .Select(_ => true);
        }

        /// 自キャラクター経験値獲得結果をセット。
        public void SetCharacterProgress(byte no, float previousExpRatio, float currentExpRatio, ushort levelUpNum)
        {
            _ownCharacters[no - 1].SetProgress(previousExpRatio, currentExpRatio, levelUpNum);
        }
    }
}