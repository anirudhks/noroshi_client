﻿using UnityEngine;
using UniRx;
using Noroshi.BattleScene.UI;
using DG.Tweening;
using Noroshi.Core.Game.Sound;
using Noroshi.MonoBehaviours;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class DropItemUIView : SpineView, IDropItemUIView
    {
        const string DROP_EVENT_NAME_FOR_MOVE = "move";
        const string DROP_ANIMATION_NAME = "battleeffect_treasurebox";
        [SerializeField] float _moveDuration = 0.4f;
        byte _executeNum = 0;

        /// サウンド操作時に操作内容が OnNext される Subject。
        protected Subject<SoundEvent> _onCommandSoundSubject = new Subject<SoundEvent>();

        new void Awake()
        {
            base.Awake();
        }

        new void Start()
        {
            base.Start();
            _rootSkeletonAnimation.state.Event += _onAnimationEvent;
        }

        public void Drop(ICharacterView characterView, byte dropItemNo, bool isMultiDrop)
        {
            _executeNum = 0;
            _transform.position = characterView.GetPosition();

            var startTime = isMultiDrop ? GlobalContainer.RandomGenerator.GenerateFloat(0.75f) : 0.0f;
            SceneContainer.GetTimeHandler().Timer(startTime).Subscribe(_ => _playDropItemAnimation()).AddTo(this);
        }

        /// サウンド操作時に操作内容が OnNext される Observable を取得。
        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _onCommandSoundSubject.AsObservable();
        }

        void _playDropItemAnimation()
        {
            _play(DROP_ANIMATION_NAME, false);
        }
        
        // アニメーション内に仕込まれたイベントをハンドリング
        void _onAnimationEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
        {
            if (e.ToString() == DROP_EVENT_NAME_FOR_MOVE)
            {
                _execute();
            }
        }

        void _execute()
        {
            _executeNum++;
            if (_executeNum == 1)
            {
                // 宝箱出現の効果音
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    SoundID = SoundConstant.BATTLE_DROP_ITEM_SOUND_ID,
                    Command = SoundCommand.Play,
                });

                var moveX = UnityEngine.Random.Range(-1.0f, 1.0f);
                var moveY = UnityEngine.Random.Range(-0.1f, 0.1f);
                var endPosition = new Vector3(_transform.position.x + moveX, _transform.position.y + moveY, _transform.position.z);
                _transform.DOMove(endPosition, _moveDuration).SetEase(Ease.Linear);
            }
            else if (_executeNum == 2)
            {
                // 宝箱獲得の効果音
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    SoundID = SoundConstant.BATTLE_PICK_UP_DROP_ITEM_SOUND_ID,
                    Command = SoundCommand.Play,
                });
            }
        }
        
        public Vector2 GetLocalPosition()
        {
            return _transform.localPosition;
        }
    }
}
