﻿using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class FieldView : MonoBehaviour, IFieldView
    {
        [SerializeField] float _brightenTime = 0.5f;
        [SerializeField] float _darkenTime = 0.5f;
        [SerializeField] Color _darkColor = new Color(0.0f, 0.0f, 0.0f);
        // ループバトル用
        [SerializeField] FieldLayer[] _loopFieldLayers = null;
        SpriteRenderer[] _renderers;
        // ループバトル用、通常用関係なくアクセスする必要があるため別途保持しておく
        FieldLayer[] _fieldLayers = null;
        Dictionary<SpriteRenderer, Color> _defaultColorMap = new Dictionary<SpriteRenderer, Color>();
        ParticleSystemRenderer[] _particleSystemRenderers = null;

        void Start()
        {
            _renderers = GetComponentsInChildren<SpriteRenderer>();
            _fieldLayers = GetComponentsInChildren<FieldLayer>();
            _particleSystemRenderers = GetComponentsInChildren<ParticleSystemRenderer>();
            foreach (var renderer in _renderers)
            {
                _defaultColorMap.Add(renderer, renderer.color);
            }
            foreach (var fieldLayer in _fieldLayers)
            {
                fieldLayer.Initialize();
            }
        }
        public void Brighten()
        {
            foreach (var renderer in _renderers)
            {
                renderer.DOColor(_defaultColorMap[renderer], _brightenTime);
            }
            // パーティクルのRendererを表示する
            foreach (var particleSystemRenderer in _particleSystemRenderers)
            {
                particleSystemRenderer.enabled = true;
            }
        }
        public void Darken()
        {
            var darkColor = _darkColor;
            foreach (var renderer in _renderers)
            {
                darkColor.a = _defaultColorMap[renderer].a;
                renderer.DOColor(darkColor, _darkenTime);
            }
            // パーティクルのRendererを非表示にする
            foreach (var particleSystemRenderer in _particleSystemRenderers)
            {
                particleSystemRenderer.enabled = false;
            }
        }

        public void AdjustAfterSwitchWave()
        {
            foreach (var fieldLayer in _loopFieldLayers)
            {
                fieldLayer.AdjustAfterSwitchWave();
            }
        }

        // FieldLayerの移動を開始させる(SwitchWave連動)
        public void EnterSwitchWave(byte waveNo, float duration)
        {
            foreach (var fieldLayer in _fieldLayers)
            {
                fieldLayer.EnterSwitchWave(waveNo, duration);
            }
        }

        public void SetForegroundVisible(bool isVisible)
        {
            foreach (var fieldLayer in _fieldLayers)
            {
                fieldLayer.SetForegroundVisible(isVisible);
            }
        }
    }
}
