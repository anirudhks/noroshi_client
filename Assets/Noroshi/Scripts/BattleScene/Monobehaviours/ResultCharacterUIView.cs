﻿using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;
using DG.Tweening;

namespace Noroshi.BattleScene.MonoBehaviours
{
    public class ResultCharacterUIView : UIView 
    {
        [SerializeField] Image _characterImage;
        [SerializeField] Image _characterFrameImage;
        [SerializeField] GaugeView _expGauge;
        [SerializeField] Image _expGaugeFullEffectImage;
        [SerializeField] Image _expLevelupEffectImage;
        [SerializeField] Text _lvText;
        // キャラクターサムネイル表示用の全フレーム
        [SerializeField] Sprite[] _characterThumnailFrames;
        // 死亡表示用グレースケールマテリアル
        [SerializeField] Material _grayScaleMaterial;

        readonly float _progressionSpeed = 1.0f;

        RectTransform _levelupImageRectTransform = null;
        Vector2 _defaultLevelupImagePosition = Vector2.zero;
        float _currentExpRatio = 0.0f;
        ushort _levelUpNum = 0;

        CharacterThumbnail _characterThumbnail;

        new void Awake()
        {
            base.Awake();
            if (_expLevelupEffectImage != null)
            {
                _levelupImageRectTransform = _expLevelupEffectImage.rectTransform;
                _defaultLevelupImagePosition = _levelupImageRectTransform.anchoredPosition;
            }
        }

        void Start()
        {
            if (_expGauge != null) _expGauge.SetActive(false);
        }

        string _getLevelText(ushort level)
        {
            return GlobalContainer.LocalizationManager.GetText("UI.Heading.Level") + "." + level.ToString();
        }

        public void SetCharacterThumbnail(CharacterThumbnail characterThumbnail)
        {
            _characterThumbnail = characterThumbnail;
            _characterFrameImage.sprite = _characterThumnailFrames[characterThumbnail.PromotionLevel - 1];
            _lvText.text = _getLevelText(_characterThumbnail.Level);

            if (characterThumbnail.IsDead)
            {
                // 死んでいる場合はサムネとフレームはグレーアウト
                _characterImage.material = _grayScaleMaterial;
                _characterFrameImage.material = _grayScaleMaterial;
                // 死んでいれば経験値スライダーは隠してしまう。
                if (_expGauge != null) _expGauge.SetActive(false);
            }
            else
            {
                // 生存の場合はマテリアルは空に
                _characterImage.material = null;
                _characterFrameImage.material = null;
            }
        }
        public IObservable<Sprite> LoadSprite()
        {
            if (_characterThumbnail == null) return Observable.Return<Sprite>(null);
            var factory = (Factory)SceneContainer.GetFactory();
            return factory.BuildCharacterThumbSprite(_characterThumbnail.CharacterID, _characterThumbnail.SkinLevel)
            .Do(s => _characterImage.sprite = s);
        }

        public void SetProgress(float previousExpRatio, float currentExpRatio, ushort levelUpNum)
        {
            _currentExpRatio = currentExpRatio;
            _levelUpNum = levelUpNum;
            _expGauge.SetValue(previousExpRatio);
        }

        public IObservable<bool> PlaySliderAnimation()
        {
            if (_expGauge == null || _characterThumbnail.IsDead) return Observable.Return(false);
            return Observable.Defer(() =>
            {
                _expGauge.SetActive(true);
                var sumExpRate = _currentExpRatio + (1.0f * _levelUpNum);
                return _expGauge.SetAnimationValue(sumExpRate, _progressionSpeed, true)
                .Do(isNext => 
                {
                    if (isNext)
                    {
                        _lvText.text = _getLevelText((ushort)(_characterThumbnail.Level + 1));
                        _playImageAlphaAnimation(_expGaugeFullEffectImage, 0.0f, 1.0f, 0.3f);
                        _playImageAlphaAnimation(_expLevelupEffectImage, 0.0f, 1.0f, 0.3f);
                        _playImageAnchorMoveAnimation(_levelupImageRectTransform, _defaultLevelupImagePosition, 
                                                  new Vector2(_defaultLevelupImagePosition.x, _defaultLevelupImagePosition.y + 15.0f), 0.3f);
                    }
                });
            });
        }

        void _playImageAlphaAnimation(Image image, float initialAlpha, float endAlpha, float duration)
        {
            if (image == null) return;
            image.DOKill();
            image.color = new Color(image.color.r, image.color.g, image.color.b, initialAlpha);
            var endColor = new Color(image.color.r, image.color.g, image.color.b, endAlpha);
            image.DOColor(endColor, duration).SetLoops(2, LoopType.Yoyo);
        }

        void _playImageAnchorMoveAnimation(RectTransform rectTransform, Vector2 initialAnchorPosition, Vector2 endAnchorPosition, float duration)
        {
            if (rectTransform == null) return;
            rectTransform.DOKill();
            rectTransform.anchoredPosition = initialAnchorPosition;
            rectTransform.DOAnchorPos(endAnchorPosition, duration);
        }

        void OnDestory()
        {
            _characterImage.sprite = null;
        }
    }
}
