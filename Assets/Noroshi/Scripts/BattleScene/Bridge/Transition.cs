﻿using UnityEngine;
using Noroshi.Core.Game.Battle;
using UIConstant = Noroshi.UI.Constant;

namespace Noroshi.BattleScene.Bridge
{
    /// シーン遷移の橋渡しをする。このクラスだけバトルシーン外で利用しても良い。
    public class Transition
    {
        public static BattleType BattleType { get; private set; }
        public static uint[] PlayerCharacterIDs { get; private set; }
        public static BattleCategory BattleCategory { get; private set; }
        public static uint ID { get; private set; }
        public static uint? RentalPlayerCharacterID { get; private set; }
        public static uint PaymentNum { get; private set; }
        public static string AfterBattleSceneName { get; private set; }

        /// <summary>
        /// チュートリアルバトルへ遷移する。
        /// </summary>
        public static void TransitToTutorialBattle()
        {
            BattleType = BattleType.CpuBattle;
            BattleCategory = BattleCategory.Fixed;
            ID = 1; // 選択肢はなく固定。
            PlayerCharacterIDs = new uint[0];
            PaymentNum = 0;
            AfterBattleSceneName = UIConstant.SCENE_MAIN;
            Application.LoadLevel(UIConstant.SCENE_BATTLE);
        }
        /// <summary>
        /// ストーリーバトルへ遷移する。
        /// </summary>
        /// <param name="storyStageId">ストーリーステージID</param>
        /// <param name="playerCharacterIds">選択プレイヤーキャラクターID配列</param>
        /// <param name="rentalPlayerCharacterId">選択傭兵プレイヤーキャラクターID</param>
        public static void TransitToStoryBattle(uint storyStageId, uint[] playerCharacterIds, uint? rentalPlayerCharacterId = null)
        {
            _transitToCpuBattle(BattleCategory.Story, storyStageId, playerCharacterIds, rentalPlayerCharacterId, 0);
        }
        /// <summary>
        /// アリーナバトルへ遷移する。
        /// </summary>
        /// <param name="enemyPlayerId">対戦相手プレイヤーID</param>
        /// <param name="playerCharacterIds">選択プレイヤーキャラクターID配列</param>
        /// <param name="rentalPlayerCharacterId">選択傭兵プレイヤーキャラクターID</param>
        public static void TransitToArenaBattle(uint enemyPlayerId, uint[] playerCharacterIds, uint? rentalPlayerCharacterId = null)
        {
            _transitToPlayerBattle(BattleCategory.Arena, enemyPlayerId, playerCharacterIds, rentalPlayerCharacterId, 0);
        }
        /// <summary>
        /// 修行バトルへ遷移する。
        /// </summary>
        /// <param name="trainingStageId">修行ステージID</param>
        /// <param name="playerCharacterIds">選択プレイヤーキャラクターID配列</param>
        /// <param name="rentalPlayerCharacterId">選択傭兵プレイヤーキャラクターID</param>
        public static void TransitToTrainingBattle(uint trainingStageId, uint[] playerCharacterIds, uint? rentalPlayerCharacterId = null)
        {
            _transitToCpuBattle(BattleCategory.Training, trainingStageId, playerCharacterIds, rentalPlayerCharacterId, 0);
        }
        /// <summary>
        /// 試練バトルへ遷移する。
        /// </summary>
        /// <param name="trialStageId">試練ステージID</param>
        /// <param name="playerCharacterIds">選択プレイヤーキャラクターID配列</param>
        /// <param name="rentalPlayerCharacterId">選択傭兵プレイヤーキャラクターID</param>
        public static void TransitToTrialBattle(uint trialStageId, uint[] playerCharacterIds, uint? rentalPlayerCharacterId = null)
        {
            _transitToCpuBattle(BattleCategory.Trial, trialStageId, playerCharacterIds, rentalPlayerCharacterId, 0);
        }
        /// <summary>
        /// レイドボスバトルへ遷移する。
        /// </summary>
        /// <param name="guildRaidBossId">ギルドレイドボス ID</param>
        /// <param name="playerCharacterIds">選択プレイヤーキャラクターID配列</param>
        /// <param name="bp">消費 BP</param>
        /// <param name="rentalPlayerCharacterId">選択傭兵プレイヤーキャラクターID</param>
        public static void TransitToRaidBossBattle(uint guildRaidBossId, uint[] playerCharacterIds, byte bp, uint? rentalPlayerCharacterId = null)
        {
            _transitToCpuBattle(BattleCategory.RaidBoss, guildRaidBossId, playerCharacterIds, rentalPlayerCharacterId, bp);
        }
        /// <summary>
        /// 冒険バトルへ遷移する。
        /// </summary>
        /// <param name="expeditionStageId">冒険ステージID</param>
        /// <param name="playerCharacterIds">選択プレイヤーキャラクターID配列</param>
        /// <param name="rentalPlayerCharacterId">選択傭兵プレイヤーキャラクターID</param>
        public static void TransitToExpeditionBattle(uint expeditionStageId, uint[] playerCharacterIds, uint? rentalPlayerCharacterId = null)
        {
            _transitToPlayerBattle(BattleCategory.Expedition, expeditionStageId, playerCharacterIds, rentalPlayerCharacterId, 0);
        }
        /// <summary>
        /// デバッグバトルへ遷移する。ただし、事前にデバッグバトル用セッションを作っておかなければならない。
        /// </summary>
        public static void TransitToDebugBattle()
        {
            BattleType = BattleType.CpuBattle;
            BattleCategory = BattleCategory.DebugBattle;
            ID = 1; // 選択肢はなく固定。
            PlayerCharacterIDs = new uint[0];
            PaymentNum = 0;
            AfterBattleSceneName = "BattleDebug";
            Application.LoadLevel(UIConstant.SCENE_BATTLE);
        }

        static void _transitToCpuBattle(BattleCategory battleCategory, uint id, uint[] playerCharacterIds, uint? rentalPlayerCharacterId, uint paymentNum)
        {
            _transitToBattle(BattleType.CpuBattle, battleCategory, id, playerCharacterIds, rentalPlayerCharacterId, paymentNum);
        }
        static void _transitToPlayerBattle(BattleCategory battleCategory, uint id, uint[] playerCharacterIds, uint? rentalPlayerCharacterId, uint paymentNum)
        {
            _transitToBattle(BattleType.PlayerBattle, battleCategory, id, playerCharacterIds, rentalPlayerCharacterId, paymentNum);
        }
        static void _transitToBattle(BattleType battleType, BattleCategory battleCategory, uint id, uint[] playerCharacterIds, uint? rentalPlayerCharacterId, uint paymentNum)
        {
            BattleType = battleType;
            BattleCategory = battleCategory;
            ID = id;
            PlayerCharacterIDs = playerCharacterIds;
            RentalPlayerCharacterID = rentalPlayerCharacterId;
            PaymentNum = paymentNum;
            AfterBattleSceneName = Application.loadedLevelName;
            Application.LoadLevel(UIConstant.SCENE_BATTLE);
        }
    }
}
