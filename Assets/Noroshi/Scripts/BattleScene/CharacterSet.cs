﻿using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.BattleScene.Actions;

namespace Noroshi.BattleScene
{
    /// キャラクター集合を扱うクラス。
    public class CharacterSet
    {
        /// 勢力。
        readonly Force _force;
        /// オートモード。
        bool _auto;
        /// 内包キャラクター。
        readonly List<Character> _characters = new List<Character>();
        /// キャラクター番号 => ダメージ。
        readonly Dictionary<byte, uint> _characterNoToDamageMap = new Dictionary<byte, uint>();
        /// 内包キャラクターが分身を追加する際に対象分身キャラクターが OnNext される Subject。
        readonly Subject<ShadowCharacter> _onAddShadowSubject = new Subject<ShadowCharacter>();
        /// 内包キャラクターが分身を削除する際に対象分身キャラクターが OnNext される Subject。
        readonly Subject<ShadowCharacter> _onRemoveShadowSubject = new Subject<ShadowCharacter>();
        /// 本クラス用 Disposable 集合。
        readonly CompositeDisposable _disposables = new CompositeDisposable();

        /// 勢力とオートモード指定でインスタンス化。
        public CharacterSet(Force force, bool auto = false)
        {
            _force = force;
            _auto = auto;
        }

        /// 必要なデータをロードする。
        public IObservable<CharacterSet> LoadDatas()
        {
            return Observable.WhenAll(_characters.Select(c => c.LoadDatas())).Select(_ => this);
        }
        /// 必要なアセットをロードする。
        public IObservable<CharacterSet> LoadAssets(IActionFactory factory)
        {
            return Observable.WhenAll(_characters.Select(c => c.LoadAssets(factory))).Select(_ => this);
        }

        /// 内包キャラクターを取得する。
        public Character[] GetCharacters()
        {
            return _characters.ToArray();
        }
        /// キャラクター番号指定で内包キャラクターを取得する。
        public Character GetCharacterByNo(byte no)
        {
            return _characters.Where(c => c.No == no).FirstOrDefault();
        }
        /// バトル結果画面で表示するピックアップキャラクターを取得する。
        /// 最もダメージを与えているキャラクターを選出している。
        public Character GetPickUpCharacter()
        {
            return _characterNoToDamageMap.OrderByDescending(kv => kv.Value).Select(kv => GetCharacterByNo(kv.Key)).Where(c => !c.IsDead).FirstOrDefault();
        }
        /// サムネイル表示に利用するオブジェクトを返す。
        public IEnumerable<CharacterThumbnail> GetCharacterThumbnails()
        {
            return _characters.Select(c => c.BuildCharacterThumbnail());
        }
        /// 内包キャラクターいずれかが生きていれば真を返す。
        public bool AreAlive()
        {
            return _characters.Any(pc => !pc.IsDead);
        }

        /// キャラクターをセットする。
        public void SetCharacters(IEnumerable<Character> characters)
        {
            if (_characters.Count() > 0)
            {
                throw new InvalidOperationException();
            }
            foreach (var character in characters)
            {
                AddCharacter(character);
            }
        }
        /// キャラクターを追加する。
        public void AddCharacter(Character character)
        {
            var no = (byte)(_characters.Count > 0 ? _characters.Max(c => c.No) + 1 : 1);

            character.SetNo(no);
            character.SetForce(_force);
            character.SetAuto(_auto);
            _characters.Add(character);

            _characterNoToDamageMap.Add(no, 0);

            _subscribeCharacterObservable(character);
        }
        void _subscribeCharacterObservable(Character character)
        {
            // 与ダメージ紐付け。
            character.GetOnAddDamageObservable()
            .Subscribe(damage => _onAddDamage(character.No, damage)).AddTo(_disposables);
            // 分身追加・削除紐付け。
            character.ShadowHandler.GetOnMakeShadowObservable()
            .Subscribe(shadow => _onAddShadowSubject.OnNext(shadow)).AddTo(_disposables);
            character.ShadowHandler.GetOnRemoveShadowObservable()
            .Subscribe(shadow => _onRemoveShadowSubject.OnNext(shadow)).AddTo(_disposables);
        }
        /// キャラクターを個別に削除する。分身など特殊なライフサイクルなキャラクターでのみ利用する。
        public void RemoveCharacter(Character character)
        {
            _characters.Remove(character);
            var no = character.No;
            if (_characterNoToDamageMap.ContainsKey(no))
            {
                _characterNoToDamageMap.Remove(no);
            }
        }

        /// 現合計 HP を取得する。
        public uint GetTotalCurrentHP()
        {
            return (uint)_characters.Sum(c => c.CurrentHP);
        }
        /// 最大合計 HP を取得する。
        public uint GetTotalMaxHP()
        {
            return (uint)_characters.Sum(c => c.MaxHP);
        }

        /// 所属キャラクター総合 HP が変更される度に HP 情報が OnNext される Observable を取得。
        public IObservable<ChangeableValueEvent> GetOnTotalHPChangeObservable()
        {
            return _characters.Select(c => c.GetOnHPChangeObservable()).Merge()
                .Select(hpEvent => new ChangeableValueEvent(hpEvent.Difference, (int)_characters.Sum(c => c.CurrentHP), (int)_characters.Sum(c => c.MaxHP)));
        }

        /// 全所属キャラクターの死亡演出終了後に OnNext される Observable を取得。
        public IObservable<Character[]> GetOnExitAllDeadAnimationsObservable()
        {
            return _characters.Select(c => c.GetOnExitDeadAnimationObservable()).WhenAll();
        }

        /// 分身を追加する際に対象分身キャラクターが OnNext される Observable を取得。
        public IObservable<ShadowCharacter> GetOnAddShadowObservable()
        {
            return _onAddShadowSubject.AsObservable();
        }
        /// 分身を削除する際に対象分身キャラクターが OnNext される Observable を取得。
        public IObservable<ShadowCharacter> GetOnRemoveShadowObservable()
        {
            return _onRemoveShadowSubject.AsObservable();
        }

        /// ダメージ取得。
        public IEnumerable<uint> GetDamages()
        {
            return _characters.OrderBy(c => c.No).Select(c => _characterNoToDamageMap[c.No]);
        }
        /// ダメージ追加時処理。
        void _onAddDamage(byte characterNo, int damage)
        {
            if (damage <= 0) return;
            _characterNoToDamageMap[characterNo] += (uint)damage;
        }

        /// 内包キャラクターの View のアクティブ・非アクティブセット。
        public void SetViewActive(bool active)
        {
            foreach (var character in _characters)
            {
                character.GetView().SetActive(active);
            }
        }

        /// オートモードセット。
        public void SetAuto(bool auto)
        {
            _auto = auto;
            foreach (var character in _characters)
            {
                character.SetAuto(auto);
            }
        }

        /// インターバルでの回復処理。
        public void RecoveryAtInterval()
        {
            foreach (var character in _characters.Where(c => !c.IsDead))
            {
                character.RecoverAtInterval();
            }
        }
        /// 次 Wave の準備。
        public void PrepareNextWave()
        {
            foreach (var character in _characters)
            {
                character.PrepareNextWave();
                _characterNoToDamageMap[character.No] = 0;
            }
        }

        /// 内容をクリアする。
        public void Clear()
        {
            _disposables.Clear();
            _characters.Clear();
            _characterNoToDamageMap.Clear();
        }

        /// リソースを破棄する。
        public void Dispose()
        {
            _disposables.Dispose();
            foreach (var character in _characters)
            {
                character.Dispose();
            }
            _characters.Clear();
            _characterNoToDamageMap.Clear();
        }
    }
}
