using System.Collections.Generic;
using UniLinq;
using UniRx;
using Flaggs.Extensions.Heam;

namespace Noroshi.BattleScene
{
    public class StoryHandler
    {
        Story _beforeBattleStory;
        Story _beforeBossWaveStory;
        Story _afterBossDieStory;
        Story _afterBattleStory;
        Dictionary<uint, ICharacterView> _characterViewMap = new Dictionary<uint, ICharacterView>();

        public StoryHandler(Core.WebApi.Response.Battle.CpuBattleStory beforeBattleStory, Core.WebApi.Response.Battle.CpuBattleStory beforeBossWaveStory, Core.WebApi.Response.Battle.CpuBattleStory afterBossDieStory,  Core.WebApi.Response.Battle.CpuBattleStory afterBattleStory)
        {
            _beforeBattleStory = beforeBattleStory != null ? new Story(beforeBattleStory) : null;
            _beforeBossWaveStory = beforeBossWaveStory != null ? new Story(beforeBossWaveStory) : null;
            _afterBossDieStory = afterBossDieStory != null ? new Story(afterBossDieStory) : null;
            _afterBattleStory = afterBattleStory != null ? new Story(afterBattleStory) : null;
        }

        public IObservable<StoryHandler> LoadDatas()
        {
            var characterIds = _getStories().SelectMany(story => story.GetCharacterIDs()).Distinct();
            var characterMaster = GlobalContainer.MasterManager.CharacterMaster;
            var characters = characterIds.Select(id => characterMaster.Get(id));
            var characterNameTextKeyMap = characters.ToDictionary(c => c.ID, c => c.TextKey);
            if (_beforeBattleStory != null) _beforeBattleStory.SetCharacterNameTextKeyMap(characterNameTextKeyMap);
            if (_beforeBossWaveStory != null) _beforeBossWaveStory.SetCharacterNameTextKeyMap(characterNameTextKeyMap);
            if (_afterBossDieStory != null) _afterBossDieStory.SetCharacterNameTextKeyMap(characterNameTextKeyMap);
            if (_afterBattleStory != null) _afterBattleStory.SetCharacterNameTextKeyMap(characterNameTextKeyMap);
            return Observable.Return(this);
        }

        public IObservable<StoryHandler> LoadAssets(IFactory factory)
        {
            var characterIds = _getStories().SelectMany(story => story.GetCharacterIDs()).Distinct();
            return Observable.WhenAll(
                characterIds.Select(characterId =>
                {
                    return factory.BuildCharacterViewForUI(characterId).Do(view => _onLoadCharacterViewForUI(characterId, view));
                })
            )
            .Do(_ =>
            {
                if (_beforeBattleStory != null) _beforeBattleStory.SetCharacterViewMap(_characterViewMap);
                if (_beforeBossWaveStory != null) _beforeBossWaveStory.SetCharacterViewMap(_characterViewMap);
                if (_afterBossDieStory != null) _afterBossDieStory.SetCharacterViewMap(_characterViewMap);
                if (_afterBattleStory != null) _afterBattleStory.SetCharacterViewMap(_characterViewMap);
            })
            .Select(_ => this);
        }
        void _onLoadCharacterViewForUI(uint characterId, ICharacterView view)
        {
            _characterViewMap.Add(characterId, view);
        }
        public void Prepare()
        {
            foreach (var characterId in _characterViewMap.Keys)
            {
                _characterViewMap[characterId].SetUpForUI(CenterPositionForUIType.Story);
            }

            var characterSkinLevelMap = new Dictionary<uint, byte>();
            var characterMaster = GlobalContainer.MasterManager.CharacterMaster;
            var battleCharacters = SceneContainer.GetCharacterManager().GetAllCharacters();
            foreach (var character in _characterViewMap)
            {
                // まず表示対象のキャラクターIDを持つキャラクターを取り出す(重複あり)
                var battleCharacterArray = battleCharacters.ToArrayWithOptimization(c => c.CharacterID == character.Key, c => c);
                // 該当キャラクターを選出する
                // 敵味方ともに同一のCharacterIDを持っていた場合の対応をここでする
                // 一旦は重複したら敵を優先する(基本重複する時、味方キャラは喋らないはずなので)
                // 戦闘に参加しないキャラクターの時はnull
                // 一人だけ該当したらそいつを利用
                // 複数の同一IDがいた場合で、敵が一体以上いた場合そいつを利用
                // 複数いたとしても、味方しかいない場合はそのまま先頭を利用
                var battleCharacter = battleCharacterArray.Length == 0 ? null : 
                    battleCharacterArray.Length == 1 ? battleCharacterArray[0] :
                    battleCharacterArray.Count(c => c.Force == Force.Enemy) > 0 ? battleCharacterArray.FirstOrDefault(c => c.Force == Force.Enemy) :
                    battleCharacterArray.FirstOrDefault();

                if (battleCharacter != null)
                {
                    characterSkinLevelMap.Add(character.Key, battleCharacter.SkinLevel);
                }
                else
                {
                    // 戦闘に参加しないキャラクターのスキンレベルをマスターから取ってくる
                    var characterData = characterMaster.Get(character.Key);
                    var characterTagSet = new Noroshi.Core.Game.Character.CharacterTagSet(characterData.TagFlags);
                    var skin = new Noroshi.Core.Game.Character.Skin(characterData.InitialEvolutionLevel, characterTagSet.IsDeca);
                    characterSkinLevelMap.Add(character.Key, skin.GetSkinLevel());
                }
            }
            if (_beforeBattleStory != null) _beforeBattleStory.SetCharacterSkinLevelMap(characterSkinLevelMap);
            if (_beforeBossWaveStory != null) _beforeBossWaveStory.SetCharacterSkinLevelMap(characterSkinLevelMap);
            if (_afterBossDieStory != null) _afterBossDieStory.SetCharacterSkinLevelMap(characterSkinLevelMap);
            if (_afterBattleStory != null) _afterBattleStory.SetCharacterSkinLevelMap(characterSkinLevelMap);
        }

        public bool HasStory
        {
            get { return _getStories().Count() > 0; }
        }

        public Story GetBeforeBattleStory()
        {
            return _beforeBattleStory;
        }
        public Story GetBeforeBossWaveStory()
        {
            return _beforeBossWaveStory;
        }
        public Story GetAfterBossDieStory()
        {
            return _afterBossDieStory;
        }
        public Story GetAfterBattleStory()
        {
            return _afterBattleStory;
        }

        public ICharacterView GetCharacterView(uint characterId)
        {
            return _characterViewMap[characterId];
        }
        // ストーリーで使用するBGMのSoundId配列を取得する
        public uint[] GetStoryBgmSoundIDs()
        {
            // 値が設定されているところだけを取り出して配列化
            return _getStories().ToArrayWithOptimization(s => s.GetBgmSoundID().HasValue, s => s.GetBgmSoundID().Value);
        }

        IEnumerable<Story> _getStories()
        {
            return (new Story[4]{_beforeBattleStory, _beforeBossWaveStory, _afterBossDieStory, _afterBattleStory}).Where(story => story != null);
        }
    }
}
