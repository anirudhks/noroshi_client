using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.Game.Character;

namespace Noroshi.BattleScene.CharacterEffect
{
    /// <summary>
    /// キャラクターエフェクトを管理するクラス。
    /// </summary>
    public class CharacterEffectManager : IManager
    {
        /// <summary>
        /// （キャラクタービューから相対的に）前面描画順最大値。
        /// </summary>
        const int FRONT_MAX_ORDER =  300;
        /// <summary>
        /// （キャラクタービューから相対的に）前面描画順最小値。
        /// </summary>
        const int FRONT_MIN_ORDER =    6;
        /// <summary>
        /// （キャラクタービューから相対的に）後面描画順最大値。
        /// </summary>
        const int BACK_MAX_ORDER  =   -6;
        /// <summary>
        /// （キャラクタービューから相対的に）後面描画順最小値。
        /// </summary>
        const int BACK_MIN_ORDER  = -300;

        /// <summary>
        /// 再生中のビュー集合（対象キャラクタービュー => キャラクターエフェクトID => キャラクターエフェクトアニメーションID => 再生中のビュー）。
        /// </summary>
        Dictionary<ICharacterView, Dictionary<uint, Dictionary<uint, ICharacterEffectView>>> _playingViews = new Dictionary<ICharacterView, Dictionary<uint, Dictionary<uint, ICharacterEffectView>>>();
        /// <summary>
        /// （キャラクタービューから相対的に）前面描画順の現最大値集合（対象キャラクタービュー => 前面描画順の現最大値）。
        /// </summary>
        Dictionary<ICharacterView, int> _maxFrontOrders = new Dictionary<ICharacterView, int>();
        /// <summary>
        /// （キャラクタービューから相対的に）後面描画順の現最小値集合（対象キャラクタービュー => 後面描画順の現最小値）。
        /// </summary>
        Dictionary<ICharacterView, int> _minBackOrders = new Dictionary<ICharacterView, int>();
        /// <summary>
        /// Dispose 集合。
        /// </summary>
        CompositeDisposable _disposables = new CompositeDisposable();

        /// <summary>
        /// 初期化。
        /// </summary>
        public void Initialize()
        {
            // BattleManager が OnNext してくるキャラクターエフェクト命令に処理を紐付ける。
            SceneContainer.GetBattleManager().GetOnCommandCharacterEffectObservable()
            .Subscribe(_onCommandCharacterEffect).AddTo(_disposables);
        }
        /// <summary>
        /// 必要なデータの初期ロード。
        /// </summary>
        public IObservable<IManager> LoadDatas()
        {
            // 必要な情報は全てマスターマネージャー経由で取得できるのでここでは何もしない。
            return Observable.Return((IManager)this);
        }
        /// <summary>
        /// 必要なアセットの初期ロード。
        /// </summary>
        public IObservable<IManager> LoadAssets(IFactory factory)
        {
            // 事前に必要な Prefab を十分なだけロードしておく。（足りない分はその場でロード）
            var prefabIds = GlobalContainer.MasterManager.CharacterMaster.GetCharacterEffects().SelectMany(ce => ce.Animations.Select(a => a.PrefabID)).Distinct();
            return SceneContainer.GetCacheManager().PreloadCharacterEffectView(factory, prefabIds).Select(_ => (IManager)this);
        }
        /// <summary>
        /// 初期化最終処理。
        /// </summary>
        public void Prepare()
        {
        }

        /// <summary>
        /// キャラクターエフェクト操作命令に対する処理。
        /// </summary>
        /// <param name="characterEffectEvent">キャラクターエフェクトイベント</param>
        void _onCommandCharacterEffect(CharacterEffectEvent characterEffectEvent)
        {
            var masterData = GlobalContainer.MasterManager.CharacterMaster.GetCharacterEffect(characterEffectEvent.CharacterEffectID);
            // マスター設定ミスの水際チェック
            if (masterData == null)
            {
                if (characterEffectEvent.CharacterEffectID != 0) GlobalContainer.Logger.Error(string.Format("No Master 'CharacterEffect' (ID = {0})", characterEffectEvent.CharacterEffectID));
                return;
            }
            switch (characterEffectEvent.Command)
            {
                case CharacterEffectCommand.Play:
                    _play(characterEffectEvent.CharacterView, masterData);
                    break;
                case CharacterEffectCommand.Stop:
                    _stop(characterEffectEvent.CharacterView, masterData);
                    break;
                case CharacterEffectCommand.Interrupt:
                    _interrupt(characterEffectEvent.CharacterView, masterData);
                    break;
                default:
                    throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// キャラクターエフェクト再生処理。
        /// </summary>
        /// <param name="characterView">対象キャラクタービュー</param>
        /// <param name="masterData">キャラクターエフェクトのマスターデータ</param>
        void _play(ICharacterView characterView, Core.WebApi.Response.Master.CharacterEffect masterData)
        {
            foreach (var animationData in masterData.Animations)
            {
                _play(characterView, animationData);
            }
        }
        /// <summary>
        /// キャラクターエフェクト個別アニメーション再生処理。
        /// </summary>
        /// <param name="characterView">対象キャラクタービュー</param>
        /// <param name="animationData">キャラクターエフェクトアニメーションのマスターデータ</param>
        void _play(ICharacterView characterView, Core.WebApi.Response.Master.CharacterEffectAnimation animationData)
        {
            _loadView(animationData.PrefabID)
            .SelectMany(view => 
            {
                // 再生中の同一ビューがあればストップ。
                var playingView = _getPlayingView(characterView, animationData.CharacterEffectID, animationData.ID);
                if (playingView != null)
                {
                    playingView.Stop();
                }
                // 再生中のビューとしてセット。
                _setPlayingView(characterView, animationData.CharacterEffectID, animationData.ID, view);
                // 再生。
                var order = _getOrder(characterView, animationData.OrderInCharacterLayer);
                switch (animationData.PlayType)
                {
                    case CharacterEffectAnimationPlayType.Loop:
                        return view.Play(characterView, animationData.AnimationName, (CharacterEffectAnimationPosition)animationData.Position, order, animationData.FixedRotationY);
                    case CharacterEffectAnimationPlayType.Once:
                        return view.PlayOnce(characterView, animationData.AnimationName, (CharacterEffectAnimationPosition)animationData.Position, order, animationData.FixedRotationY);
                    case CharacterEffectAnimationPlayType.MultiAnimationLoop:
                        return view.Play(characterView, animationData.AnimationName + "_appear", animationData.AnimationName + "_wait", (CharacterEffectAnimationPosition)animationData.Position, order);
                }
                throw new InvalidOperationException();
            })
            // 再生が終わったタイミングで該当ビューはストックしておく。
            .Subscribe(_ => _stockPlayingView(characterView, animationData)).AddTo(_disposables);
        }

        /// <summary>
        /// キャラクターエフェクト割り込み再生処理。
        /// </summary>
        /// <param name="characterView">対象キャラクタービュー</param>
        /// <param name="masterData">キャラクターエフェクトのマスターデータ</param>
        void _interrupt(ICharacterView characterView, Core.WebApi.Response.Master.CharacterEffect masterData)
        {
            foreach (var animationData in masterData.Animations)
            {
                if (_canInterrupt(characterView, animationData)) _interrupt(characterView, animationData);
            }
        }
        /// <summary>
        /// キャラクターエフェクト個別アニメーション割り込み再生可否チェック。
        /// </summary>
        /// <param name="characterView">対象キャラクタービュー</param>
        /// <param name="animationData">キャラクターエフェクトアニメーションのマスターデータ</param>
        bool _canInterrupt(ICharacterView characterView, Core.WebApi.Response.Master.CharacterEffectAnimation animationData)
        {
            // 今は複数アニメーション再生方式のものイコール割り込み可能とする。
            return animationData.PlayType == CharacterEffectAnimationPlayType.MultiAnimationLoop;
        }
        /// <summary>
        /// キャラクターエフェクト個別アニメーション割り込み再生処理。
        /// </summary>
        /// <param name="characterView">対象キャラクタービュー</param>
        /// <param name="animationData">キャラクターエフェクトアニメーションのマスターデータ</param>
        void _interrupt(ICharacterView characterView, Core.WebApi.Response.Master.CharacterEffectAnimation animationData)
        {
            _loadView(animationData.PrefabID)
            .SelectMany(view =>
            {
                // 再生中の同一ビューがあればストップ。
                var playingView = _getPlayingView(characterView, animationData.CharacterEffectID, animationData.ID);
                if (playingView != null)
                {
                    playingView.Stop();
                }
                // 再生中のビューとしてセット。
                _setPlayingView(characterView, animationData.CharacterEffectID, animationData.ID, view);
                // 割り込み再生。
                if (animationData.PlayType == CharacterEffectAnimationPlayType.MultiAnimationLoop)
                {
                    var order = _getOrder(characterView, animationData.OrderInCharacterLayer);
                    return view.Play(characterView, animationData.AnimationName + "_damage", animationData.AnimationName + "_wait", (CharacterEffectAnimationPosition)animationData.Position, order)
                        .Select(_ => view);
                }
                else
                {
                    // 未対応
                    throw new InvalidOperationException();
                }
            })
            // 再生が終わったタイミングで該当ビューはストックしておく。
            .Subscribe(_ => _stockPlayingView(characterView, animationData)).AddTo(_disposables);
        }

        /// <summary>
        /// キャラクターエフェクト再生停止処理。
        /// </summary>
        /// <param name="characterView">対象キャラクタービュー</param>
        /// <param name="masterData">キャラクターエフェクトのマスターデータ</param>
        void _stop(ICharacterView characterView, Core.WebApi.Response.Master.CharacterEffect masterData)
        {
            foreach (var animationData in masterData.Animations)
            {
                _stop(characterView, animationData);
            }
        }
        /// <summary>
        /// キャラクターエフェクト個別アニメーション再生停止処理。
        /// </summary>
        /// <param name="characterView">対象キャラクタービュー</param>
        /// <param name="animationData">キャラクターエフェクトアニメーションのマスターデータ</param>
        void _stop(ICharacterView characterView, Core.WebApi.Response.Master.CharacterEffectAnimation animationData)
        {
            var view = _getPlayingView(characterView, animationData.CharacterEffectID, animationData.ID);
            if (view == null) return;
            if (animationData.PlayType == CharacterEffectAnimationPlayType.MultiAnimationLoop)
            {
                view.StopWithPlayOnce(animationData.AnimationName + "_disappear");
            }
            else
            {
                view.Stop();
            }
        }

        /// <summary>
        /// ビューをロードする。極力ロード済みのプールのものを使い回すが、不足分はリアルタイムでロードする。
        /// </summary>
        /// <param name="prefabId">プレハブID</param>
        IObservable<ICharacterEffectView> _loadView(uint prefabId)
        {
            return SceneContainer.GetCacheManager().ForceGetCharacterEffectCache(SceneContainer.GetFactory(), prefabId).Get()
                .Do(v => v.SetActive(true));
        }
        /// <summary>
        /// 再生中のビューをプールへストックする。
        /// </summary>
        /// <param name="characterView">対象キャラクタービュー</param>
        /// <param name="animationData">キャラクターエフェクトアニメーションのマスターデータ</param>
        void _stockPlayingView(ICharacterView characterView, Core.WebApi.Response.Master.CharacterEffectAnimation animationData)
        {
            var view = _removePlayingView(characterView, animationData.CharacterEffectID, animationData.ID);
            view.RemoveParent();
            view.SetActive(false);
            SceneContainer.GetCacheManager().GetCharacterEffectCache(animationData.PrefabID).Stock(view);
        }

        /// <summary>
        /// 再生中のビューを取得する。
        /// </summary>
        /// <param name="characterView">対象キャラクタービュー</param>
        /// <param name="characterEffectId">キャラクターエフェクトID</param>
        /// <param name="characterEffectAnimationId">キャラクターエフェクトアニメーションID</param>
        ICharacterEffectView _getPlayingView(ICharacterView characterView, uint characterEffectId, uint characterEffectAnimationId)
        {
            if (!_playingViews.ContainsKey(characterView)) return null;
            if (!_playingViews[characterView].ContainsKey(characterEffectId)) return null;
            if (!_playingViews[characterView][characterEffectId].ContainsKey(characterEffectAnimationId)) return null;
            return _playingViews[characterView][characterEffectId][characterEffectAnimationId];
        }
        /// <summary>
        /// 再生中のビューをセットする。
        /// </summary>
        /// <param name="characterView">対象キャラクタービュー</param>
        /// <param name="characterEffectId">キャラクターエフェクトID</param>
        /// <param name="characterEffectAnimationId">キャラクターエフェクトアニメーションID</param>
        /// <param name="characterEffectView">セットするビュー</param>
        void _setPlayingView(ICharacterView characterView, uint characterEffectId, uint characterEffectAnimationId, ICharacterEffectView characterEffectView)
        {
            if (!_playingViews.ContainsKey(characterView))
            {
                _playingViews.Add(characterView, new Dictionary<uint, Dictionary<uint, ICharacterEffectView>>());
            }
            if (!_playingViews[characterView].ContainsKey(characterEffectId))
            {
                _playingViews[characterView].Add(characterEffectId, new Dictionary<uint, ICharacterEffectView>());
            }
            _playingViews[characterView][characterEffectId].Add(characterEffectAnimationId, characterEffectView);
        }
        /// <summary>
        /// 再生中のビューを取り除く。
        /// </summary>
        /// <param name="characterView">対象キャラクタービュー</param>
        /// <param name="characterEffectId">キャラクターエフェクトID</param>
        /// <param name="characterEffectAnimationId">キャラクターエフェクトアニメーションID</param>
        ICharacterEffectView _removePlayingView(ICharacterView characterView, uint characterEffectId, uint characterEffectAnimationId)
        {
            var playingView = _getPlayingView(characterView, characterEffectId, characterEffectAnimationId);
            if (playingView != null)
            {
                _playingViews[characterView][characterEffectId].Remove(characterEffectAnimationId);
            }
            return playingView;
        }

        /// <summary>
        /// 描画順情報を取得する。
        /// </summary>
        /// <param name="characterView">Character view.</param>
        /// <param name="orderInCharacterLayer">Order in character layer.</param>
        int _getOrder(ICharacterView characterView, int orderInCharacterLayer)
        {
            if (orderInCharacterLayer > 0)
            {
                if (!_maxFrontOrders.ContainsKey(characterView) || _maxFrontOrders[characterView] >= FRONT_MAX_ORDER)
                {
                    _maxFrontOrders[characterView] = FRONT_MIN_ORDER;
                }
                else
                {
                    _maxFrontOrders[characterView]++;
                }
                return _maxFrontOrders[characterView];
            }
            else
            {
                if (!_minBackOrders.ContainsKey(characterView) || _minBackOrders[characterView] <= BACK_MIN_ORDER)
                {
                    _minBackOrders[characterView] = BACK_MAX_ORDER;
                }
                else
                {
                    _minBackOrders[characterView]--;
                }
                return _minBackOrders[characterView];
            }
        }

        /// <summary>
        /// リソースを破棄する。
        /// </summary>
        public void Dispose()
        {
            _disposables.Dispose();
        }
    }
}
