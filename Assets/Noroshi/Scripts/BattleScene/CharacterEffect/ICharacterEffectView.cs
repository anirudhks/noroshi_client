﻿using UniRx;
using Noroshi.Core.Game.Character;
using Noroshi.MonoBehaviours;

namespace Noroshi.BattleScene.CharacterEffect
{
    public interface ICharacterEffectView : IView
    {
        IObservable<bool> PlayOnce(ICharacterView characterView, string animationName, CharacterEffectAnimationPosition position, int orderInCharacterLayer, bool unreversible = false);
        IObservable<bool> Play(ICharacterView characterView, string animationName, CharacterEffectAnimationPosition position, int orderInCharacterLayer, bool unreversible = false);
        IObservable<bool> Play(ICharacterView characterView, string onceAnimationName, string loopAnimationName, CharacterEffectAnimationPosition position, int orderInCharacterLayer);
        void Stop();
        void StopWithPlayOnce(string animationName);
    }
}
