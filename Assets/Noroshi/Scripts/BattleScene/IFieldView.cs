﻿namespace Noroshi.BattleScene
{
    public interface IFieldView
    {
        void Brighten();
        void Darken();
        void AdjustAfterSwitchWave();
        void EnterSwitchWave(byte waveNo, float duration);
        void SetForegroundVisible(bool isActive);
    }
}
