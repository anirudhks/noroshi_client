using UniLinq;
using UniRx;
using Noroshi.Core.WebApi.Response.Battle;
using Noroshi.Core.Game.Battle;
using Noroshi.BattleScene.Camera;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene
{
    public class CpuBattleManager : AbstractBattleManager
    {
        StoryHandler _storyHandler;

        Subject<Story> _onEnterBeforeBossWaveStory = new Subject<Story>();
        Subject<bool> _onExitWaitBeforeBossWaveStory = new Subject<bool>();

        Subject<Story> _onEnterAfterBossDieStory = new Subject<Story>();
        Subject<bool> _onExitWaitAfterBossDieStory = new Subject<bool>();

        Subject<Story> _onEnterAfterBattleStory = new Subject<Story>();
        Subject<bool> _onExitWaitAfterBattleStory = new Subject<bool>();

        bool _hasPlayedBeforeBossWaveStory = false;
        bool _hasPlayedAfterBossDieStory = false;
        bool _hasSlowTimeAtFinish;

        public CpuBattleManager(BattleCategory battleCategory, uint battleContentId, uint[] ownPlayerCharacterIds, uint? rentalPlayerCharacterId, uint paymentNum) : base(battleCategory, battleContentId, ownPlayerCharacterIds, rentalPlayerCharacterId, paymentNum)
        {
        }

        public override IObservable<IManager> LoadDatas()
        {
            // バトル開始用データをロード
            return _requestBattleStartWebAPI()
            .Do(response =>
            {
                _onLoadDatas(response, response.OwnCharacters);
                SceneContainer.GetCharacterManager().SetCurrentEnemyCpuCharacterSets(response.Waves.Select(w => w.BattleCharacters.Where(c => c != null)), UnlimitedHPEnemyBattle);
                _storyHandler = new StoryHandler(response.AdditionalInformation.BeforeBattleStory, response.AdditionalInformation.BeforeBossWaveStory, response.AdditionalInformation.AfterBossDieStory, response.AdditionalInformation.AfterBattleStory);
            })
            .SelectMany(_ => _storyHandler.LoadDatas())
            .Select(_ => (IManager)this);
        }
        public override IObservable<IManager> LoadAssets(IFactory factory)
        {
            return base.LoadAssets(factory).SelectMany(_storyHandler.LoadAssets(factory)).Select(_ => (IManager)this);
        }

        public override uint[] GetBattleBgmSoundIDs()
        {
            var commonBgmSoundIds = base.GetBattleBgmSoundIDs();
            var storyBgmSoundIds = _storyHandler.GetStoryBgmSoundIDs();
            var loadBgmSoundIds = new uint[commonBgmSoundIds.Length + storyBgmSoundIds.Length];
            commonBgmSoundIds.CopyTo(loadBgmSoundIds, 0);
            storyBgmSoundIds.CopyTo(loadBgmSoundIds, commonBgmSoundIds.Length);
            return loadBgmSoundIds;
        }

        // ストーリーが存在する場合はBGM再生をそちらに任せる
        protected override bool _shouldPlayBattleBgmAtDefaultRoutine { get { return GetBeforeBattleStory() == null; } }
        public override bool ShouldPlayResultBgmAtDefaultRoutine (VictoryOrDefeat victoryOrDefeat)
        {
            return victoryOrDefeat != VictoryOrDefeat.Win || GetAfterBattleStory() == null;
        }

        public bool HasStory
        {
            get { return _storyHandler.HasStory; }
        }

        public Story GetBeforeBattleStory()
        {
            return _storyHandler.GetBeforeBattleStory();
        }
        public IObservable<Story> GetOnEnterBeforeBossWaveStory()
        {
            return _onEnterBeforeBossWaveStory.AsObservable();
        }
        public IObservable<Story> GetOnEnterAfterBossDieStory()
        {
            return _onEnterAfterBossDieStory.AsObservable();
        }
        public Story GetAfterBattleStory()
        {
            return _storyHandler.GetAfterBattleStory();
        }
        public IObservable<Story> GetOnEnterAfterBattleStory()
        {
            return _onEnterAfterBattleStory.AsObservable();
        }

        public override void Prepare()
        {
            base.Prepare();
            // バトル前ストーリーが存在する場合、
            // 演出上、キャラクターの初期ポジションを変える必要がある。
            if (GetBeforeBattleStory() != null)
            {
                foreach (var ownCharacter in SceneContainer.GetCharacterManager().CurrentOwnCharacterSet.GetCharacters())
                {
                    ownCharacter.SetViewToStoryWavePosition();
                    ownCharacter.SetVisible(false, true);
                }
            }
            _storyHandler.Prepare();
        }

        protected override void _onPauseAtFinish()
        {
            if (_hasSlowTimeAtFinish) base._onPauseAtFinish();
        }

        /// Ready 状態の終了を待つべきかどうか判定。事前ストーリーが存在しているもしくは修行モードの場合待つ。
        public override bool ShouldWaitToFinishReady() { return GetBeforeBattleStory() != null || BattleCategory == BattleCategory.Training; }

        public override void StartWave()
        {
            if (BattleCategory != BattleCategory.Training)
            {
                base.StartWave();
            }
            else
            {
                // 修行の時は制限時間がWaveごとにリセットしないため時間を引き継ぐ
                var prevWavecNo = CurrentWaveNo - 2;
                if (prevWavecNo < 0)
                {
                    // １バトル目の時は初期値をセット
                    CurrentWave.Start(_battleTime);
                }
                else
                {
                    // ２バトル目以降は一個前のWaveの残り時間から開始する
                    var remainingTime = _waves[prevWavecNo].RemainingTime;
                    CurrentWave.Start(remainingTime);
                }
            }
            // 敵のボスキャラクターにはフラグを立てておく。
            // ボス撃破時後ストーリー演出が「死亡ではなく逃走」の場合もフラグを立てておく。
            if (_storyHandler.GetAfterBossDieStory() == null) return;
            var bossEnemyCharacter = SceneContainer.GetCharacterManager().GetCurrentEnemyCharacters()
            .Select(character => (CpuCharacter)character)
            .Where(character => character.IsBoss)
            .FirstOrDefault(); 
            if (bossEnemyCharacter != null)
            {
                var dramaType = _storyHandler.GetAfterBossDieStory().GetDramaType();
                if (dramaType.HasValue && dramaType.Value == Story.DramaType.BossEscape)
                {
                   bossEnemyCharacter.SetEscapeBeforeDeadOn();
                }
            }
        }

        /// WaveBattle 状態へ遷移する直前に差し込まれる処理。
        protected override IObservable<bool> _processBeforeTransitionToWaveBattle()
        {
            // 最後のバトルであればボス前ストーリーを差し込む。
            return CurrentWaveNo == WaveNum ? _enterBeforeBossWaveStory() : Observable.Return(true);
        }

        protected override bool _isMovingToNextWaveWithWalking()
        {
            return CurrentWave.No == WaveNum && _storyHandler.GetBeforeBossWaveStory() != null;
        }

        IObservable<bool> _enterBeforeBossWaveStory()
        {
            if (_storyHandler.GetBeforeBossWaveStory() == null) return Observable.Return<bool>(false);
            if (_hasPlayedBeforeBossWaveStory) return Observable.Return<bool>(false);
            _hasPlayedBeforeBossWaveStory = true;
            _onEnterBeforeBossWaveStory.OnNext(_storyHandler.GetBeforeBossWaveStory());
            _onEnterBeforeBossWaveStory.OnCompleted();
            return _onExitWaitBeforeBossWaveStory.AsObservable();
        }
        public void ExitWaitBeforeBossWaitStory()
        {
            _onExitWaitBeforeBossWaveStory.OnNext(true);
            _onExitWaitBeforeBossWaveStory.OnCompleted();
        }

        IObservable<bool> _enterAfterBossDieStory()
        {
            if (_storyHandler.GetAfterBossDieStory() == null) return Observable.Return<bool>(false);
            if (_hasPlayedAfterBossDieStory) return Observable.Return<bool>(false);
            _hasPlayedAfterBossDieStory = true;
            _enterPause().Subscribe(_ => {}).AddTo(_disposables);
            // まだ敵が残っているケースもあるので、ここでカメラや残り時間などを一時停止させる
            CurrentWave.StopTimer();
            _onEnterAfterBossDieStory.OnNext(_storyHandler.GetAfterBossDieStory());
            _onEnterAfterBossDieStory.OnCompleted();
            // バトル中ボスが死んだ際に差し込まれるストーリー時にもカメラをズームアウトしておく
            _onCommandCameraSubject.OnNext(new CameraEvent()
            {
                Command = CameraEvent.CameraCommand.Reset,
                Type = CameraEvent.CameraType.BattleSituation
            });
            return _onExitWaitAfterBossDieStory.AsObservable();
        }
        public void ExitWaitAfterBossDieStory()
        {
            _onExitWaitAfterBossDieStory.OnNext(true);
            _onExitWaitAfterBossDieStory.OnCompleted();
            // カメラ処理及びカウントダウン再開
            _onCommandCameraSubject.OnNext(new CameraEvent()
            {
                Command = CameraEvent.CameraCommand.Play,
                Type = CameraEvent.CameraType.BattleSituation
            });
            CurrentWave.RestartTimer();
            _exitPause();
        }

        public IObservable<bool> EnterAfterBattleStory()
        {
            if (_storyHandler.GetAfterBattleStory() == null) return Observable.Return<bool>(false);
            _onEnterAfterBattleStory.OnNext(_storyHandler.GetAfterBattleStory());
            _onEnterAfterBattleStory.OnCompleted();
            return _onExitWaitAfterBattleStory.AsObservable();
        }
        public void ExitWaitAfterBattleStory()
        {
            _onExitWaitAfterBattleStory.OnNext(true);
            _onExitWaitAfterBattleStory.OnCompleted();
        }

        public void TryCommandCamera(CameraEvent cameraEvent)
        {
            _onCommandCameraSubject.OnNext(cameraEvent);
        }

        /// キャラクター死亡時処理。
        protected override void _onCharacterDie(Character character)
        {
            base._onCharacterDie(character);

            // 以降は敵 CPU キャラクターが死亡した処理。
            if (character.Force != Force.Enemy || !(character is CpuCharacter)) return;
            var cpuCharacter = (CpuCharacter)character;
            // ボス撃破後ストーリーが存在し、該当ボスが（死亡の代わりに）逃走演出設定の場合、
            // 死亡時にストーリー開始処理を差し込む。
            if (_storyHandler.GetAfterBossDieStory() != null && cpuCharacter.IsBoss && cpuCharacter.IsDeadEscape)
            {
                _enterAfterBossDieStory().Subscribe(_ => cpuCharacter.EscapeBeforeDead());
                _hasSlowTimeAtFinish = false;
            }
            else
            {
                _hasSlowTimeAtFinish = true;
            }
        }
    }
}
