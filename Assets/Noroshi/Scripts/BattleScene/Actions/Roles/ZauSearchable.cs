﻿using System;
using System.Collections.Generic;
using UniLinq;

namespace Noroshi.BattleScene.Actions.Roles
{
    // ID指定のキャラクターを探し自分自身とともに渡す、対象が存在しない場合は自身を除きもっとも前線にいるキャラクターを探す。
    public class ZauSearchable : AbstractSearchable
    {
        public uint BuddyCharacterID { get; set; }
        public override int? MaxTargetNum { get { return IsBuddy ? 2 : 1; } set { } }
        public override TargetSortType? SortType { get { return TargetSortType.PositionFront; } set { } }

        public bool IsBuddy { get; private set; }

        public IEnumerable<IActionTarget> Search(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            // 味方のキャラクターを探す
            var targets = actionTargetFinder.GetAllTargets().Where(t => t.Force == executor.CurrentForce);
            // その中からID指定のキャラクターが存在するか確認する
            var buddies = targets.Where(t => t.CharacterID == BuddyCharacterID || t.CharacterID == executor.CharacterID);
            IsBuddy = buddies.Where(t => t.CharacterID == BuddyCharacterID).Count() > 0;

            return FilterTargets(actionTargetFinder, IsBuddy ? buddies : _sort(targets.Where(t => t.CharacterID != executor.CharacterID), executor.Force));
        }
    }
}
