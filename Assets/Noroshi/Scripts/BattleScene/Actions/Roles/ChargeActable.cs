using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;
using Noroshi.Core.Game.Battle;

namespace Noroshi.BattleScene.Actions.Roles
{
    public class ChargeActable
    {
        int _executeNum;
        int _originalHorizontalIndex;
        protected RangeSearchable _attackRangeSearchable;
        public RelativeForce? Force { get; set; }
        public Noroshi.MonoBehaviours.AnimationInformation Animation { get; set; }
        public int FirstActionMinRange { get; set; }
        public int FirstActionMaxRange { get; set; }
        public TargetSortType? TargetSortType { get; set; }
        public int? MaxTargetNum { get; set; }
        public DamageType DamageType { get; set; }
        public int DamageBoost { get; set; }
        public DamageMagicalAttribute? DamageMagicalAttribute { get; set; }
        public Dictionary<uint, float> AttributeIDToLevelDrivenAttributeParamMap { get; set; }
        public uint? HitCharacterEffectID { get; set; }
        public uint? HitSoundID { get; set; }
        public TargetStateID? TargetStateID { get; set; }

        public virtual void Initialize()
        {
            _attackRangeSearchable = new RangeSearchable()
            {
                Force = Force,
                MinRange = FirstActionMinRange,
                MaxRange = FirstActionMaxRange,
                MaxTargetNum = MaxTargetNum,
                SortType = TargetSortType,
            };
        }

        public virtual void PreProcess()
        {
            _executeNum = 0;
        }

        void _executeHorizontalMove(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IEnumerable<IActionTarget> targets)
        {
            var k = executor.GetDirection() == Grid.Direction.Left ? -1 : 1;
            _originalHorizontalIndex = executor.GetGridPosition().Value.HorizontalIndex;
            Func<short> getHorizontalIndex = () => {
                var filterTargets = _attackRangeSearchable.FilterTargets(actionTargetFinder, targets);
                var toH = filterTargets.Count() > 0 ? (int)filterTargets.Average(t => t.GetGridPosition().Value.HorizontalIndex) : _originalHorizontalIndex;
                return (short)((toH - _originalHorizontalIndex) * k - (FirstActionMinRange + FirstActionMaxRange) / 2);
            };
            if (getHorizontalIndex() != 0) executor.HorizontalMove(getHorizontalIndex, _forwardDuration);
        }

        void _execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IEnumerable<IActionTarget> targets, Action actionExecute, Action<IActionExecutor> onLastExecution)
        {
            _executeNum++;
            
            if (_executeNum == ForwardIndex + 1)
            {
                // 進む
                _executeHorizontalMove(actionTargetFinder, executor, targets);
            }
            else if (_executeNum == ForwardIndex + 2)
            {
                // 止まるだけ
            }
            else if (onLastExecution != null && _executeNum == BackwardIndex + 1 )
            {
                // 戻る
                onLastExecution(executor);
            }
            else if (onLastExecution != null && _executeNum == BackwardIndex + 2)
            {
                // 止まるだけ
            }
            else
            {
                // 行動
                actionExecute();
            }
        }

        public void ExecuteAndGoBack(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IEnumerable<IActionTarget> targets, Action actionExecute)
        {
            _execute(actionTargetFinder, executor, targets, actionExecute, (e) => 
            {
                var h = _originalHorizontalIndex - e.GetGridPosition().Value.HorizontalIndex;
                if (e.GetDirection() == Grid.Direction.Left) h *= -1;
                if (h != 0) e.HorizontalMove((short)h, _backwardDuration, true);
            });
        }

        public void ExecuteAndGoForward(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IEnumerable<IActionTarget> targets, Action actionExecute)
        {
            _execute(actionTargetFinder, executor, targets, actionExecute, (e) => 
            {
                e.GoStraight(_backwardDuration);
            });
        }

        public void ExecuteAndStop(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IEnumerable<IActionTarget> targets, Action actionExecute)
        {
            _execute(actionTargetFinder, executor, targets, actionExecute, null);
        }

        int _forwardIndex = 0;
        int _backwardIndex = -2;
        public int ForwardIndex { get { return _forwardIndex >= 0 ? _forwardIndex : (Animation.TriggerTimes.Length + _forwardIndex); } set { _forwardIndex = value; } }
        public int BackwardIndex { get { return _backwardIndex >= 0 ? _backwardIndex : (Animation.TriggerTimes.Length + _backwardIndex); } set { _backwardIndex = value; } }
        float _forwardDuration { get { return Animation.TriggerTimes[ForwardIndex + 1] - Animation.TriggerTimes[ForwardIndex]; } }
        float _backwardDuration { get  { return Animation.TriggerTimes[BackwardIndex + 1] - Animation.TriggerTimes[BackwardIndex]; } }
    }
}
