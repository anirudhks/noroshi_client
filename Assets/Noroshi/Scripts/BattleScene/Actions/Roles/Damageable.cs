﻿using System.Collections.Generic;
using UniLinq;
using Noroshi.Core.Game.Battle;

namespace Noroshi.BattleScene.Actions.Roles
{
    public class Damageable
    {
        public DamageType? DamageType { get; protected set; }
        public DamageMagicalAttribute? DamageMagicalAttribute { get; protected set; }
        public TargetStateID? TargetStateID { get; protected set; }
        public Dictionary<uint, float> AttributeIDToLevelDrivenAttributeParamMap { get; set; }
        public uint? HitCharacterEffectID { get; protected set; }
        public uint? HitSoundID { get; private set; }
        public ushort ActionLevel { get; private set; }


        public Damageable(
            DamageType? damageType, DamageMagicalAttribute? damageMagicalAttribute,
            TargetStateID? targetStateId,
            Dictionary<uint, float> attributeIdToLevelDrivenAttributeParamMap,
            uint? hitCharacterEffectId, uint? hitSoundId,
            ushort actionLevel
        )
        {
            DamageType = damageType;
            DamageMagicalAttribute = damageMagicalAttribute;
            TargetStateID = targetStateId;
            AttributeIDToLevelDrivenAttributeParamMap = attributeIdToLevelDrivenAttributeParamMap;
            HitCharacterEffectID = hitCharacterEffectId;
            HitSoundID = hitSoundId;
            ActionLevel = actionLevel;
        }


        public ActionEvent[] DamageMulti(IActionExecutor executor, IEnumerable<IActionTarget> targets, int? hpDamage, int? energyDamage)
        {
            var index = 0;
            var actionEvents = new ActionEvent[targets.Count()];
            foreach (var target in new List<IActionTarget>(targets))
            {
                actionEvents[index++] = Damage(executor, target, hpDamage, energyDamage);
            }
            return actionEvents;
        }
        public ActionEvent Damage(IActionExecutor executor, IActionTarget target, int? hpDamage, int? energyDamage)
        {
            var actionEvent = new ActionEvent(executor, target, ActionLevel);
            if (hpDamage.HasValue)
            {
                // 回避
                if (_canDodge(executor, target))
                {
                    actionEvent.SetDodge();
                    target.ReceiveActionEvent(actionEvent);
                    return actionEvent;
                }
                var damageAttributeRatio = 1f;
                if (DamageMagicalAttribute.HasValue)
                {
                    var magicalAttributeEffect = new MagicalAttributeEffect();
                    damageAttributeRatio += magicalAttributeEffect.GetDamageRatio(DamageMagicalAttribute.Value, target.TagSet.GetActionTargetAttributes());
                }
                if (damageAttributeRatio != 1) GlobalContainer.Logger.Debug("Damage Attribute Ratio " + damageAttributeRatio);
                actionEvent.SetHPDamage((int)(hpDamage.Value * damageAttributeRatio), DamageType);
            }
            if (energyDamage.HasValue) actionEvent.SetEnergyDamage(energyDamage.Value);
            if (AttributeIDToLevelDrivenAttributeParamMap != null)
            {
                actionEvent.SetAttributeIDToLevelDrivenAttributeparamMap(AttributeIDToLevelDrivenAttributeParamMap);
            }
            if (TargetStateID.HasValue)
            {
                actionEvent.SetTargetStateID(TargetStateID.Value);
            }
            actionEvent.SetHitCharacterEffectIDAndSoundID(HitCharacterEffectID, HitSoundID);
            target.ReceiveActionEvent(actionEvent);
            return actionEvent;
        }
        
        bool _canDodge(IActionExecutor executor, IActionTarget target)
        {
            if (DamageType != Actions.DamageType.Physical) return false;
            var dodge = target.Dodge + (executor.HasMissDamage() ? 100 : 0);
            var threshold = System.Math.Max(0, dodge - executor.Accuracy) / 100f;
            return GlobalContainer.RandomGenerator.GenerateFloat() < threshold;
        }
    }
}
