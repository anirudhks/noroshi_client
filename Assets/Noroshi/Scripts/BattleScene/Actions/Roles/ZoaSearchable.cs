using System;
using System.Collections.Generic;
using UniLinq;

namespace Noroshi.BattleScene.Actions.Roles
{
    // ID指定のキャラクターを探す、対象が存在しない場合は自身を除きもっとも最大値に近いエネルギーを持つキャラクターを探す。
    public class ZoaSearchable : AbstractSearchable
    {
        public uint BuddyCharacterID { get; set; }
        public override int? MaxTargetNum { get { return 1; } set { } }
        public override TargetSortType? SortType { get { return TargetSortType.EnergyDamageAsc; } set { } }

        public bool IsBuddy { get; private set; }

        public IEnumerable<IActionTarget> Search(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            // 「味方」で「エネルギーが最大でない」キャラクターを探す
            var targets = actionTargetFinder.GetAllTargets().Where(t => t.Force == executor.CurrentForce && t.MaxEnergy > t.CurrentEnergy);
            // その中からID指定のキャラクターを探す
            var buddy = targets.Where(t => t.CharacterID == BuddyCharacterID);
            IsBuddy = buddy.Count() > 0;

            return FilterTargets(actionTargetFinder, IsBuddy ? buddy : _sort(targets.Where(t => t.CharacterID != executor.CharacterID), executor.Force));
        }
    }
}
