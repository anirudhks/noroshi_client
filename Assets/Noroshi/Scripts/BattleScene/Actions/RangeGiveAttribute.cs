﻿using Noroshi.BattleScene.Actions.Roles;
using UniRx;

namespace Noroshi.BattleScene.Actions
{
    public class RangeGiveAttribute : AbstractRangeAction
    {
        public RangeGiveAttribute(Core.WebApi.Response.Master.Action data) : base(data)
        {
        }

        protected override RelativeForce? _targetRelativeForce { get { return _arg1 > 0 ? (RelativeForce?)_arg1 : null; } }
        public override int MinRange { get { return _arg2; } }
        public override int MaxRange { get { return _arg3; } }
        float             _shadowNumBonus   { get { return (float)_arg4 / 100; } }
        protected override ActionEffectType? _actionEffectType { get { return _arg5 > 0 ? (ActionEffectType?)_arg5 : null; } }
        
        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            var attributeIdToLevelDrivenAttributeParamMap = _getAttributeIdToLevelDrivenAttributeParamMap();
            var attributeIdKeys = new uint[attributeIdToLevelDrivenAttributeParamMap.Keys.Count];
            attributeIdToLevelDrivenAttributeParamMap.Keys.CopyTo(attributeIdKeys, 0);
            foreach (var attributeId in attributeIdKeys)
            {
                var levelDrivenAttributeParam = attributeIdToLevelDrivenAttributeParamMap[attributeId];
                attributeIdToLevelDrivenAttributeParamMap[attributeId] = levelDrivenAttributeParam * _getBounusCoefficient(executor);
            }
            foreach (var target in targets)
            {
                var actionEvent = new ActionEvent(executor, target, Level);
                if (attributeIdToLevelDrivenAttributeParamMap.Count > 0)
                {
                    actionEvent.SetAttributeIDToLevelDrivenAttributeparamMap(attributeIdToLevelDrivenAttributeParamMap);
                }
                target.ReceiveActionEvent(actionEvent);
            }
            _tryCameraShake();
        }

        float _getBounusCoefficient(IActionExecutor executor)
        {
            return (1 + _shadowNumBonus * executor.GetShadowNum());
        }
    }
}
