﻿using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using CharacterConstant = Noroshi.Core.Game.Character.Constant;
using Noroshi.Character;
using Noroshi.BattleScene.Actions.Roles;

namespace Noroshi.BattleScene.Actions
{
    public class GamigamiActiveAction : AbstractNoRangeAttack
    {
        const int EXECUTE_MAX_NUM = 4;

        RangeSearchable _rangeSearchable;
        // Executorが呼ばれたタイミングでカウントアップ
        int _executeNum = 0;
        // _attackが呼ばれたタイミングでカウントアップ
        int _attackNum = 0;
        // ランダム選出用
        TargetSelectType[] _targetSelectTypeParttern = new TargetSelectType[]
        {
            TargetSelectType.EnmeyPositionBack,
            TargetSelectType.EnmeyPositionCenter,
            TargetSelectType.EnmeyPositionFront,
        };
        // 現状ガミガミーの攻撃回数分作成
        List<IActionView> _actionViews = new List<IActionView>();
        // 攻撃時それぞれのターゲット位置を保持する
        Grid.GridPosition[] _centerGrids;

        public GamigamiActiveAction(Core.WebApi.Response.Master.Action data) : base(data)
        {
            _rangeSearchable = new RangeSearchable()
            {
                Force = RelativeForce.Enemy,
                MinRange = _attackMinRange,
                MaxRange = _attackMaxRange,
                MaxTargetNum = _maxTargetNum,
                SortType = _targetSortType,
            };
            _centerGrids = new Noroshi.Grid.GridPosition[EXECUTE_MAX_NUM];
        }

        protected override TargetSelectType? _targetSelectType { get { return _arg1 > 0 ? (TargetSelectType?)_arg1 : null; } }
        protected override int? _targetTagFlags { get { return _arg2 > 0 ? (int?)_arg2 : null; } }
        protected override float? _damageCoefficient { get { return _arg3 > 0 ? (float?)_arg3 / 100.0f : null; } }
        int _attackMinRange { get { return _arg4; } }
        int _attackMaxRange { get { return _arg5;} }

        public override IObservable<IAction> LoadAssets(IActionExecutor executor, IActionFactory factory)
        {
            // ここで４つ分ActionViewを読み込む
            var loadActionViewsObservable = new List<IObservable<IActionView>>();
            for (var i = 0; i < EXECUTE_MAX_NUM; i++)
            {
                loadActionViewsObservable.Add(factory.BuildActionView(executor.CharacterID).Do(v => _actionViews.Add(v)));
            }

            return loadActionViewsObservable.Concat().Select(_ => (IAction)this);
        }


        public override void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            base.PreProcess (executor, targetCandidates);
            _executeNum = 0;
            _attackNum = 0;
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            _executeNum++;
            // ActionViewを飛ばすタイミングで、ターゲットをランダム抽出
            _noRangeSearchable.SelectType = _getNextRandomTargetSelectType();
            var targets = GetTargets(actionTargetFinder, executor);
            if (targets.Count() > 0)
            {
                _centerGrids[_executeNum - 1] = _getCenterGridPosition(targets);
            }
            else
            {
                // TODO
                _centerGrids[_executeNum - 1] = executor.GetGridPosition().Value;
            }
            _actionViews[_executeNum - 1].Launch(executor.GetViewAsActionExecutorView(), _centerGrids[_executeNum - 1], executor.GetDirection(), AnimationName, 1000).Subscribe(_ => 
            {
                _attack(actionTargetFinder, executor);
            });
            
        }

        public override void PostProcess(IActionExecutor executor)
        {
            base.PostProcess (executor);
        }

        protected ActionEvent[] _attack(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            _attackNum++;
            var targets = _rangeSearchable.GetTargetsByRange(actionTargetFinder, _centerGrids[_attackNum - 1], executor.GetDirection(), executor.CurrentForce);
            return _attackable.AttackMulti(executor, targets);
        }

        TargetSelectType _getNextRandomTargetSelectType()
        {
            return GlobalContainer.RandomGenerator.Lot(_targetSelectTypeParttern);
        }
    }
}