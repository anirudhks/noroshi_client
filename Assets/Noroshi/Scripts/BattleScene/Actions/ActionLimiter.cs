﻿using System.Collections.Generic;
using UniLinq;

namespace Noroshi.BattleScene.Actions
{
    /// <summary>
    /// アクション制限クラス。
    /// </summary>
    public class ActionLimiter
    {
        /// <summary>
        /// 効果発動中アクション制限因子。
        /// </summary>
        List<Factor> _factors = new List<Factor>();
        /// <summary>
        /// 全アクション制限フラグ。
        /// </summary>
        bool _allActionLimit;
        /// <summary>
        /// 魔法アクション制限フラグ。
        /// </summary>
        bool _magicalActionLimit;

        /// <summary>
        /// アクション発動可否チェック。
        /// </summary>
        /// <param name="damageType">ダメージ種別</param>
        public bool CanInvokeAction(DamageType? damageType)
        {
            if (_allActionLimit) return false;
            if (_magicalActionLimit)
            {
                if (damageType.HasValue && damageType.Value == DamageType.Magical)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// アクション制限因子を追加。
        /// </summary>
        /// <param name="factor">追加するアクション制限因子</param>
        public void AddFactor(Factor factor)
        {
            _factors.Add(factor);
            _calculate();
        }
        /// <summary>
        /// アクション制限因子を削除。
        /// </summary>
        /// <param name="factor">削除するアクション制限因子</param>
        public void RemoveFactor(Factor factor)
        {
            _factors.Remove(factor);
            _calculate();
        }

        /// <summary>
        /// 効果発動中アクション制限因子を踏まえて結果をキャッシュする。
        /// </summary>
        void _calculate()
        {
            _allActionLimit = _factors.Any(f => f.AllAction);
            _magicalActionLimit = _factors.Any(f => f.MagicalAction);
        }

        /// <summary>
        /// アクション制限因子。
        /// </summary>
        public struct Factor
        {
            /// <summary>
            /// 全アクション制限フラグ。
            /// </summary>
            public bool AllAction { get; set; }
            /// <summary>
            /// 魔法アクション制限フラグ。
            /// </summary>
            public bool MagicalAction { get; set; }
        }
    }
}
