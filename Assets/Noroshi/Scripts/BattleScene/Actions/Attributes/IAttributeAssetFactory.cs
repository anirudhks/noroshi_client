﻿using UniRx;

namespace Noroshi.BattleScene.Actions.Attributes
{
    /// <summary>
    /// Attribute 関連素材のファクトリーインターフェース。
    /// </summary>
    public interface IAttributeAssetFactory
    {
        /// <summary>
        /// カエル View を事前ロード。
        /// </summary>
        /// <param name="num">事前ロード数</param>
        IObservable<ICharacterView[]> PreloadFrogView(byte num);
        /// <summary>
        /// カエル View をビルド。
        /// </summary>
        /// <returns>The frog view.</returns>
        IObservable<ICharacterView> BuildFrogView();
        /// <summary>
        /// カエル View を再利用のためにストックする。
        /// </summary>
        /// <param name="frogView">カエル View</param>
        void StockFrogView(ICharacterView frogView);
    }
}
