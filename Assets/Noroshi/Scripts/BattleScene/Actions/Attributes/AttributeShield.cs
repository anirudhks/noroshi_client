﻿
namespace Noroshi.BattleScene.Actions.Attributes
{
    public class AttributeShield : AbstractAttribute 
    {
        public AttributeShield(Core.WebApi.Response.Master.Attribute data, float coefficient) : base(data, coefficient)
        {
        }

        public override void OnReceiveActionEvent(IActionTarget target, ActionEvent actionEvent)
        {
            if (actionEvent.Attributes != null)
            {
                actionEvent.RemoveNegativeAttribute();
            }
        }

        public override bool IsNegative { get { return false; } }
    }
}
