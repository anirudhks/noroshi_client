﻿using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.BattleScene.CharacterEffect;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.Actions.Attributes
{
    public class AttributeHandler
    {
        IActionTarget _target;
        IAttributeAssetFactory _assetFactory;
        List<IAttribute> _attributes = new List<IAttribute>();
        Subject<IAttribute> _onAddAttributeSubject = new Subject<IAttribute>();
        Subject<IAttribute> _onRemoveAttributeSubject = new Subject<IAttribute>();
        Subject<CharacterEffectEvent> _onCommandCharacterEffectSubject = new Subject<CharacterEffectEvent>();
        Subject<SoundEvent> _onCommandSoundSubject = new Subject<SoundEvent>();
        Subject<ChangeableValueEvent> _onChangeHPSubject = new Subject<ChangeableValueEvent>();
        CompositeDisposable _disposables = new CompositeDisposable();
        Noroshi.Core.WebApi.Response.Master.Attribute[] _mastarDatas = null;

        public AttributeHandler(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            _target = target;
            _assetFactory = assetFactory;
        }

        /// Attribute 追加時に該当 Attribute がプッシュされる Observable を取得。
        public IObservable<IAttribute> GetOnAddAttributeObservable()
        {
            return _onAddAttributeSubject.AsObservable();
        }
        /// Attribute 削除時に該当 Attribute がプッシュされる Observable を取得。
        public IObservable<IAttribute> GetOnRemoveAttributeObservable()
        {
            return _onRemoveAttributeSubject.AsObservable();
        }
        public IObservable<CharacterEffectEvent> GetOnCommandCharacterEffectObservable()
        {
            return _onCommandCharacterEffectSubject.AsObservable();
        }
        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _onCommandSoundSubject.AsObservable();
        }
        public IObservable<ChangeableValueEvent> GetOnChangeHPObservable()
        {
            return _onChangeHPSubject.AsObservable();
        }

        public IObservable<AttributeHandler> LoadDatas()
        {
            return Observable.Return(GlobalContainer.MasterManager.CharacterMaster.GetAttributes()).Do(mastarData => _mastarDatas = mastarData).Select(_ => this);
        }

        IAttribute _build(uint attributeId, float coefficient)
        {
            return AttributeBuilder.Build(_mastarDatas.FirstOrDefault(attr => attr.ID == attributeId), coefficient);
        }

        /// Attribute 追加
        public IObservable<IAttribute> AddAttribute(IAttribute attribute)
        {
            if (attribute == null)
            {
                return Observable.Empty<IAttribute>();
            }
            // 重複チェック
            if (attribute.GroupID.HasValue && _attributes.Any(a => a.GroupID == attribute.GroupID))
            {
                var oldAttribute = _attributes.First(a => a.GroupID == attribute.GroupID);
                _removeAttribute(oldAttribute);
            }
            attribute.GetOnForceExit().Subscribe(a => _removeAttribute(a)).AddTo(_disposables);
            _addAttribute(attribute);
            if (!attribute.Lifetime.HasValue) return Observable.Return(attribute);
            return SceneContainer.GetTimeHandler().Timer(attribute.Lifetime.Value).Select(_ =>
            {
                _removeAttribute(attribute);
                return attribute;
            });
        }
        void _addAttribute(IAttribute attribute)
        {
            attribute.GetOnChangeHPObservable().Subscribe(_onChangeHPSubject.OnNext);
            _attributes.Add(attribute);
            attribute.OnEnter(_target, _assetFactory);
            _onAddAttributeSubject.OnNext(attribute);
        }

        /// 全ての Attribute を削除。
        public void RemoveAttributes()
        {
            foreach (var attribute in new List<IAttribute>(_attributes))
            {
                attribute.OnExit(_target, _assetFactory);
                _onRemoveAttributeSubject.OnNext(attribute);
            }
            _attributes.Clear();
            _disposables.Clear();
        }
        void _removeAttribute(IAttribute attribute)
        {
            if (!_attributes.Contains(attribute)) return;
            if (attribute.CharacterEffectIDOnExit.HasValue)
            {
                _onCommandCharacterEffectSubject.OnNext(new CharacterEffectEvent()
                {
                    CharacterEffectID = attribute.CharacterEffectIDOnExit.Value,
                    Command = CharacterEffectCommand.Play
                });
            }
            attribute.OnExit(_target, _assetFactory);
            _onRemoveAttributeSubject.OnNext(attribute);
            _attributes.Remove(attribute);
        }

        /// ActionEvent を受け取った際に呼ばれる。
        public void ReceiveActionEvent(ActionEvent actionEvent)
        {
            if (!actionEvent.HasForceRemoveAttributeIDs)
            {
                _buildAttribute(actionEvent);
            }
            else
            {
                if(actionEvent.ForceRemoveAttributeIDs.Length > 0)
                {
                    _forceRemoveAttribute(actionEvent);
                }
            }
            var attributes = new List<IAttribute>(_attributes);
            foreach (var attribute in attributes)
            {
                attribute.OnReceiveActionEvent(_target, actionEvent);
            }
        }
            
        /// 他ターゲットの死亡をを受け取った際に呼ばれる。
        public void OtherDie(IActionTarget deadTarget)
        {
            var attributes = new List<IAttribute>(_attributes);
            foreach (var attribute in attributes)
            {
                attribute.OnOtherDie(_target, deadTarget);
            }
        }

        void _buildAttribute(ActionEvent actionEvent)
        {
            if (actionEvent.AttributeIDToLevelDrivenAttributeParamMap == null) return;
            foreach(var attributeMap in actionEvent.AttributeIDToLevelDrivenAttributeParamMap)
            {
                actionEvent.AddAttribute(_build(attributeMap.Key, attributeMap.Value));
            }
        }

        // 強制的に指定されたAttributeを破棄する
        void _forceRemoveAttribute(ActionEvent actionEvent)
        {
            foreach (var attributeId in actionEvent.ForceRemoveAttributeIDs)
            {
                var attributes = new List<IAttribute>(_attributes.Where(a => a.AttributeID == attributeId));
                foreach (var attribute in attributes)
                {
                    _removeAttribute(attribute);
                }
            }
            actionEvent.RemoveAttribute();
        }

        public bool HasMissDamage()
        {
            return _attributes.Any(a => a.GetType() == typeof(MissDamage));
        }

        public void Dispose()
        {
            _disposables.Dispose();
        }
    }
}
