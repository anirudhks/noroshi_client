using UniRx;
using Noroshi.Core.Game.Battle;

namespace Noroshi.BattleScene.Actions.Attributes
{
    public interface IAttribute
    {
        /// AttributeID
        uint AttributeID { get; }
        /// 重複チェック用 ID。同じ ID を持つものは二重かけできない（0 はチェックスキップ）。
        uint? GroupID { get; }
        /// 生存期間。
        int? Lifetime { get; }
        /// 属性
        DamageMagicalAttribute? MagicalAttribute { get; }
        /// ネガティブ状態異常かどうか
        bool IsNegative { get; }
        /// 効果発動時の処理
        void OnEnter(IActionTarget target, IAttributeAssetFactory assetFactory);
        /// 効果終了時の処理
        void OnExit(IActionTarget target, IAttributeAssetFactory assetFactory);
        /// アクションイベントを受け取った際の処理
        void OnReceiveActionEvent(IActionTarget target, ActionEvent actionEvent);
        /// 他ターゲットの死亡を受け取った際の処理
        void OnOtherDie(IActionTarget target, IActionTarget deadTarget);
        IObservable<IAttribute> GetOnForceExit();
        IObservable<ChangeableValueEvent> GetOnChangeHPObservable();
        uint? CharacterEffectID { get; }
        uint? SoundID { get; }
        uint? CharacterEffectIDOnExit { get; }
    }
}
