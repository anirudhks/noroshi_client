﻿using Noroshi.Core.Game.Character;
using System;
using UniRx;

namespace Noroshi.BattleScene.Actions.Attributes
{
    /// 双子の自己バフ エネルギー消費軽減の状態異常 指定の自軍キャラクター死亡時に効果が失われる
    public class PikiPikoAttribute : AbstractAttribute
    {
        StatusBoostFactor _factor;

        public PikiPikoAttribute(Core.WebApi.Response.Master.Attribute data, float coefficient) : base(data, coefficient)
        {
            var reduceEnergyCost = _reduceEnergyCost * coefficient;
            var reduceEnergyCostByte = (reduceEnergyCost > byte.MaxValue) ? byte.MaxValue : (reduceEnergyCost < 0) ? (byte)0 : (byte)reduceEnergyCost;

            _factor = new StatusBoostFactor
            {
                ReduceEnergyCost = reduceEnergyCostByte,
            };
        }

        public override bool IsNegative { get { return false; } }

        public override void OnEnter(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            target.AddStatusBoosterFactor(_factor);
        }

        public override void OnExit(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            target.RemoveStatusBoosterFactor(_factor);
        }

        public override void OnOtherDie(IActionTarget target, IActionTarget deadTarget)
        {
            if (deadTarget.CharacterID == _buddyCharacterID && target.Force == deadTarget.Force)
            {
                _forceExit();
            }
        }

        uint _buddyCharacterID { get { return (uint)_arg1; } }
        int _reduceEnergyCost { get { return _arg2; } }
    }
}
