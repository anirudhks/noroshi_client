﻿namespace Noroshi.BattleScene.Actions.Attributes
{
    public class MissDamage : AbstractAttribute
    {
        public MissDamage(Core.WebApi.Response.Master.Attribute data, float coefficient) : base(data, coefficient)
        {
        }

        public override bool IsNegative { get {  return true; } }
    }
}
