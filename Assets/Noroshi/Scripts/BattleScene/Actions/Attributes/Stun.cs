﻿namespace Noroshi.BattleScene.Actions.Attributes
{
    public class Stun : AbstractAttribute
    {
        public Stun(Core.WebApi.Response.Master.Attribute data, float coefficient) : base(data, coefficient)
        {
        }

        public override void OnEnter(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            target.TryToTransitToStop(true);
        }

        public override void OnExit(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            target.TryToTransitFromStop();
        }

        public override bool IsNegative { get { return true; } }
    }
}
