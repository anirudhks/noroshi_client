﻿using UniRx;
using UniLinq;
using Noroshi.BattleScene.CharacterEffect;
using DG.Tweening;
using System.Collections.Generic;

namespace Noroshi.BattleScene.Actions.Attributes
{
    public class ReductionGauge : AbstractAttribute
    {
        const string CHANGE_SKIN_NAME = "a_skill1";
        StateTransitionBlocker.Factor _factor;
        System.IDisposable _disposable = null;
        float _intervalTime = 1.0f / 10.0f;

        public ReductionGauge(Core.WebApi.Response.Master.Attribute data, float coefficient) : base(data, coefficient)
        {
            _factor = new StateTransitionBlocker.Factor()
            {
                ActiveAction = true,
                Proceed = false,
            };
        }

        public override void OnEnter(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            target.AddStateTransitionBlockerFactor(_factor);
            if (_shouldChangeSkin) target.ChangeSkinTemporarily(CHANGE_SKIN_NAME);
            var actionEvent = new ActionEvent(null, target);
            if (_additionalAttributeId1.HasValue)
            {
                actionEvent.SetAttributeIDToLevelDrivenAttributeparamMap(new Dictionary<uint, float>()
                {
                    { _additionalAttributeId1.Value, _coefficient }
                });
                target.ReceiveActionEvent(actionEvent);
                // エネルギー減少時には必要ないので渡し終わったらActionEvent上からは削除する
                actionEvent.RemoveAllAttribute();
            }
            _disposable = SceneContainer.GetTimeHandler().Interval(_intervalTime)
            .Subscribe(_ => 
            {
                actionEvent.SetEnergyDamage((int)(_reductionValue * _intervalTime));
                target.ReceiveActionEvent(actionEvent);
                if (target.CurrentEnergy <= 0) _forceExit();
            });
        }

        public override void OnExit(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            target.RemoveStateTransitionBlockerFactor(_factor);
            if (_additionalAttributeId1.HasValue)
            {
                var actionEvent = new ActionEvent(null, target);
                actionEvent.SetForceRemoveAttributeIDs(new uint[]{ _additionalAttributeId1.Value });
                target.ReceiveActionEvent(actionEvent);
            }
            if (_shouldChangeSkin) target.ResetSkin();
            if (_disposable != null) _disposable.Dispose();
        }

        ushort _reductionValue { get { return (ushort)_arg1; } }
        bool _shouldChangeSkin { get { return _arg2 > 0; } }
        // 終了時に再生するエフェクトID
        public override uint? CharacterEffectIDOnExit { get { return _arg3 > 0 ? (uint?)_arg3 : null; } } 
        // 自身の状態異常付与時に同時に付与する状態異常ID
        uint? _additionalAttributeId1 { get { return _arg4 > 0 ? (uint?)_arg4 : null; } }

        public override bool IsNegative { get { return false; } }
    }
}
