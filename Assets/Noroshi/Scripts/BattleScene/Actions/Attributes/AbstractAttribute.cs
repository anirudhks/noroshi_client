﻿using UniRx;
using Noroshi.Core.Game.Battle;

namespace Noroshi.BattleScene.Actions.Attributes
{
    public abstract class AbstractAttribute : IAttribute
    {
        Core.WebApi.Response.Master.Attribute _data;
        Subject<IAttribute> _onForceExit = new Subject<IAttribute>();
        protected float _coefficient;

        public AbstractAttribute(Core.WebApi.Response.Master.Attribute data, float coefficient)
        {
            _data        = data;
            _coefficient = coefficient;
        }

        public uint AttributeID { get { return _data.ID; } }
        public uint? GroupID { get { return _data.GroupID > 0 ? (uint?)_data.GroupID : null; } }
        public int? Lifetime { get { return _data.Lifetime > 0 ? (int?)_data.Lifetime : null; } }
        public DamageMagicalAttribute? MagicalAttribute { get { return _data.MagicalAttribute > 0 ? (DamageMagicalAttribute?)_data.MagicalAttribute : null; } }
        public abstract bool IsNegative { get; }
        public virtual uint? CharacterEffectIDOnExit { get { return null; } } 

        public virtual void OnEnter(IActionTarget target, IAttributeAssetFactory assetFactory) { }
        public virtual void OnExit (IActionTarget target, IAttributeAssetFactory assetFactory) { }
        public virtual void OnReceiveActionEvent(IActionTarget target, ActionEvent actionEvent) { }
        public virtual void OnOtherDie(IActionTarget target, IActionTarget deadTarget) { }

        public IObservable<IAttribute> GetOnForceExit()
        {
            return _onForceExit.AsObservable();
        }
        protected void _forceExit()
        {
            _onForceExit.OnNext(this);
        }

        public virtual IObservable<ChangeableValueEvent> GetOnChangeHPObservable()
        {
            return Observable.Empty<ChangeableValueEvent>();
        }

        protected int _arg1  { get { return _data.Arg1 ; } }
        protected int _arg2  { get { return _data.Arg2 ; } }
        protected int _arg3  { get { return _data.Arg3 ; } }
        protected int _arg4  { get { return _data.Arg4 ; } }
        protected int _arg5  { get { return _data.Arg5 ; } }
        protected int _arg6  { get { return _data.Arg6 ; } }
        protected int _arg7  { get { return _data.Arg7 ; } }
        protected int _arg8  { get { return _data.Arg8 ; } }
        protected int _arg9  { get { return _data.Arg9 ; } }
        protected int _arg10 { get { return _data.Arg10; } }
        protected int _arg11 { get { return _data.Arg11; } }
        protected int _arg12 { get { return _data.Arg12; } }
        protected int _arg13 { get { return _data.Arg13; } }
        protected int _arg14 { get { return _data.Arg14; } }
        protected int _arg15 { get { return _data.Arg15; } }

        public uint? CharacterEffectID { get { return _data.CharacterEffectID; } }
        protected uint? _receiveDamageCharacterEffectID { get { return _data.ReceiveDamageCharacterEffectID; } }
        public uint? SoundID { get { return _data.SoundID; } }
        protected uint? _receiveDamageSoundId { get { return _data.ReceiveDamageSoundID; } }
    }
}
