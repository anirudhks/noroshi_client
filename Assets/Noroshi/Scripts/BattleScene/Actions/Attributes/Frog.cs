﻿using System;
using UniRx;
using Noroshi.Core.Game.Character;

namespace Noroshi.BattleScene.Actions.Attributes
{
    /// <summary>
    /// カエル状態。
    /// </summary>
    public class Frog : AbstractAttribute
    {
        readonly StatusForceSetter.Factor _factor;
        readonly ActionLimiter.Factor _actionLimiterFactor;
        ICharacterView _view;
        IDisposable _disposable;

        public Frog(Core.WebApi.Response.Master.Attribute data, float coefficient) : base(data, coefficient)
        {
            // ステータス強制設定
            _factor = new StatusForceSetter.Factor()
            {
                Armor = 0,
            };
            // 全アクション発動禁止設定。
            _actionLimiterFactor = new ActionLimiter.Factor
            {
                AllAction = true,
            };
        }

        public override bool IsNegative { get { return true; } }

        public override void OnEnter(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            // ステータス強制開始。
            target.AddStatusBreakerFactor(_factor);
            // アクション制限開始。
            target.AddActionLimiterFactor(_actionLimiterFactor);

            // View を非同期ビルドし、それへ変更。
            _disposable = assetFactory.BuildFrogView().Subscribe(view =>
            {
                _view = view;

                // viewを使い終えてストック時の処理設定
                view.GetOnStockObservable().Subscribe(_ =>
                {
                    if (_view != null)
                    {
                        assetFactory.StockFrogView(_view);
                        _view = null;
                    }
                });
                    
                target.ChangeViewTemporarily(view);
            });
        }

        public override void OnExit(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            // 遅延ロードである以上、ロード中の可能性もあるのでまずはロードを中断。
            _disposable.Dispose();
            if (_view != null && !target.IsDead)
            {
                target.ResetView(_view);
            }

            // ステータス強制終了。
            target.RemoveStatusBreakerFactor(_factor);
            // アクション制限終了。
            target.RemoveActionLimiterFactor(_actionLimiterFactor);
        }
    }
}
