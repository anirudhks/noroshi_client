﻿using UniRx;
using UniLinq;

namespace Noroshi.BattleScene.Actions.Attributes
{
    public class Transparency : AbstractAttribute
    {
        const float TRANSPARENCY_DURATION = 0.3f;
        public Transparency(Core.WebApi.Response.Master.Attribute data, float coefficient) : base(data, coefficient)
        {
        }

        public override void OnEnter(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            target.PlayTransparency(true, 0.3f);
        }

        public override void OnExit(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            target.PlayTransparency(false, 0.3f);
        }

        public override void OnReceiveActionEvent(IActionTarget target, ActionEvent actionEvent)
        {
            // ステルス状態時に物理攻撃を受けたら
            if (actionEvent.DamageType.HasValue && actionEvent.DamageType.Value == DamageType.Physical)
            {
                // HPダメージとエネルギーダメージをクリアして回避する
                actionEvent.ClearHPDamageAndEnergyDamage();
                actionEvent.SetDodge();
            }
        }

        public override bool IsNegative { get { return false; } }
    }
}