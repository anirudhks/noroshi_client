﻿namespace Noroshi.BattleScene.Actions.Attributes
{
    public class StateTransitionBlock : AbstractAttribute
    {
        readonly StateTransitionBlocker.Factor? _stateTransitionBlockerFactor;
        readonly ActionLimiter.Factor? _actionLimiterFactor;

        public StateTransitionBlock(Core.WebApi.Response.Master.Attribute data, float coefficient) : base(data, coefficient)
        {
            if (_proceedBlock || _activeActionBlock)
            {
                _stateTransitionBlockerFactor = new StateTransitionBlocker.Factor
                {
                    ActiveAction = _activeActionBlock,
                    Proceed = _proceedBlock,
                };
            }
            if (_magicActionBlock)
            {
                _actionLimiterFactor = new ActionLimiter.Factor
                {
                    MagicalAction = _magicActionBlock,
                };
            }
        }

        public override void OnEnter(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            if (_stateTransitionBlockerFactor.HasValue) target.AddStateTransitionBlockerFactor(_stateTransitionBlockerFactor.Value);
            if (_actionLimiterFactor.HasValue) target.AddActionLimiterFactor(_actionLimiterFactor.Value);
        }

        public override void OnExit(IActionTarget target, IAttributeAssetFactory assetFactory)
        {
            if (_stateTransitionBlockerFactor.HasValue) target.RemoveStateTransitionBlockerFactor(_stateTransitionBlockerFactor.Value);
            if (_actionLimiterFactor.HasValue) target.RemoveActionLimiterFactor(_actionLimiterFactor.Value);
        }
        
        public override bool IsNegative { get { return false; } }
        bool _activeActionBlock { get { return _arg1 > 0; } }
        bool _magicActionBlock { get { return _arg2 > 0; } }
        bool _proceedBlock { get { return _arg3 > 0; } }
    }
}
