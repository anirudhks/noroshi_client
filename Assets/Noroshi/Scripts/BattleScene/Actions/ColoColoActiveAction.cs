﻿using Noroshi.BattleScene.Actions.Roles;
using UniRx;
using System;
using UniLinq;
using System.Collections.Generic;

namespace Noroshi.BattleScene.Actions
{
    public class ColoColoActiveAction : AbstractRangeAction
    {
        int _executeNum = 0;
        Attackable _attackable = null;
        public ColoColoActiveAction(Core.WebApi.Response.Master.Action data) : base(data)
        {
            _attackable = new Attackable(
                DamageType, _damageMagicalAttribute,
                _targetStateId,
                null,
                _hitCharacterEffectId, _hitSoundId,
                Level,
                _getDamageBoost(),
                _damageCoefficient
            );
        }

        protected override RelativeForce? _targetRelativeForce { get { return _arg1 > 0 ? (RelativeForce?)_arg1 : null; } }
        public override int MinRange { get { return _arg2; } }
        public override int MaxRange { get { return _arg3; } }
        float? _damageCoefficient { get { return _arg4 > 0 ? (float?)_arg4 / 100.0f : null; } }
        uint[] _attributeIds = null;
        float[] _attributeLevelDrivecnAttributeParams = null;

        public override void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            base.PreProcess(executor, targetCandidates);
            _executeNum = 0;
            _attributeIds = _getAttributeIdToLevelDrivenAttributeParamMap().Keys.ToArray();
            _attributeLevelDrivecnAttributeParams = _getAttributeIdToLevelDrivenAttributeParamMap().Values.ToArray();
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            _executeNum++;
            if (_executeNum == 1)
            {
                // 最初に自身にダメージ.
                var exetutorTarget = actionTargetFinder.GetExecutorAsTarget(executor);
                var actionEvent = new ActionEvent(executor, exetutorTarget, Level);
                actionEvent.SetHPDamage(_getSelfFixedDamage());
                exetutorTarget.ReceiveActionEvent(actionEvent);
            }
            else if (_executeNum == 2)
            {
                // 次は移動開始.
                var k = executor.GetDirection() == Grid.Direction.Left ? -1 : 1;
                var originalHorizontalIndex = executor.GetGridPosition().Value.HorizontalIndex;
                Func<short> getHorizontalIndex = () => {
                    var filterTargets = _rangeSearchable.FilterTargets(actionTargetFinder, targets);
                    var toH = filterTargets.Count() > 0 ? (int)filterTargets.Average(t => t.GetGridPosition().Value.HorizontalIndex) : originalHorizontalIndex;
                    return (short)((toH - originalHorizontalIndex) * k - (MinRange + MaxRange) / 2);
                };
                if (getHorizontalIndex() != 0) executor.HorizontalMove(getHorizontalIndex, _forwardDuration);
            }
            else if (_executeNum == 3)
            {
                // 移動終了(何もしない).
            }
            else if (_executeNum == 4)
            {
                // 攻撃(ノックバックあり).
                _attackable.AttackMulti(executor, _rangeSearchable.GetTargetsByRange(actionTargetFinder, executor.GetGridPosition().Value,
                    executor.GetDirection(), executor.CurrentForce));
            }
            else if (_executeNum == 5)
            {
                var attribute = _attributeIds.Length > 0 ? _attributeIds[0] : 0;
                if (attribute != 0)
                {
                    var leveldrivenAttributeParam = _attributeLevelDrivecnAttributeParams.Length > 0 ? _attributeLevelDrivecnAttributeParams[0] : 0.0f;
                    // 攻撃力上昇.
                    var executorTarget = actionTargetFinder.GetExecutorAsTarget(executor);
                    var actionEvent = new ActionEvent(executor, executorTarget, Level);
                    actionEvent.SetAttributeIDToLevelDrivenAttributeparamMap(new Dictionary<uint, float>(){ { attribute, leveldrivenAttributeParam } });
                    executorTarget.ReceiveActionEvent(actionEvent);
                }
            }
            else if (_executeNum == 6)
            {
                var attribute = _attributeIds.Length > 1 ? _attributeIds[1] : 0;
                if (attribute != 0)
                {
                    var leveldrivenAttributeParam = _attributeLevelDrivecnAttributeParams.Length > 1 ? _attributeLevelDrivecnAttributeParams[1] : 0.0f;
                    // 行動速度上昇.
                    var executorTarget = actionTargetFinder.GetExecutorAsTarget(executor);
                    var actionEvent = new ActionEvent(executor, executorTarget, Level);
                    actionEvent.SetAttributeIDToLevelDrivenAttributeparamMap(new Dictionary<uint, float>(){ { attribute, leveldrivenAttributeParam } });
                    executorTarget.ReceiveActionEvent(actionEvent);
                }
            }
        }

        protected int _getDamageBoost()
        {
            return (int)_getLevelDrivenParam1();
        }

        int _getSelfFixedDamage()
        {
            return (int)_getLevelDrivenParam2();
        }

        float _forwardDuration { get { return _animation.TriggerTimes[2] - _animation.TriggerTimes[1]; } }
    }
}
