using Noroshi.BattleScene.Actions.Roles;

namespace Noroshi.BattleScene.Actions
{
    public class FixedDamage : AbstractNoRangeAction
    {
        Damageable _damageable;
        public FixedDamage(Core.WebApi.Response.Master.Action data) : base(data)
        {
            _damageable = new Damageable(
                DamageType, _damageMagicalAttribute,
                _targetStateId,
                _getAttributeIdToLevelDrivenAttributeParamMap(),
                _hitCharacterEffectId, _hitSoundId,
                Level
            );
        }
        protected override TargetSelectType? _targetSelectType { get { return _arg1 > 0 ? (TargetSelectType?)_arg1 : null; } }
        protected override int? _targetTagFlags { get { return _arg2 > 0 ? (int?)_arg2 : null; } }
        protected override ActionEffectType? _actionEffectType { get { return _arg3 > 0 ? (ActionEffectType?)_arg3 : null; } }

        public override void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            base.PreProcess(executor, targetCandidates);
            _tryToSetActionViewSkinLevel(executor.SkinLevel);
            _tryActionViewAppearance(targetCandidates[0].GetGridPosition().Value, executor.GetDirection(), AnimationName, 1000);
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            _damageable.DamageMulti(executor, _noRangeSearchable.FilterTargets(actionTargetFinder, targets), _getDamage(), null);
        }

        int _getDamage()
        {
            return (int)_getLevelDrivenParam1();
        }

        public override void PostProcess(IActionExecutor executor)
        {
            _tryActionViewDisappearance();
        }

        public override void Cancel(IActionExecutor executor)
        {
            _tryActionViewDisappearance();
        }
    }
}
