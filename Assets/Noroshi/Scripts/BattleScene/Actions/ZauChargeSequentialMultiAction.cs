﻿using System;

namespace Noroshi.BattleScene.Actions
{
    public class ZauChargeSequentialMultiAction : ChargeSequentialMultiAction
    {
        public ZauChargeSequentialMultiAction(Core.WebApi.Response.Master.Action data) : base(data)
        {
        }

        public override void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            base.PreProcess(executor, targetCandidates);
            _chargeActable.ForwardIndex = 1;
        }
    }
}
