﻿using Noroshi.BattleScene.Actions.Roles;
using UniRx;
using UniLinq;

namespace Noroshi.BattleScene.Actions
{
    public class GiveAttribute : AbstractNoRangeAction
    {
        public GiveAttribute(Core.WebApi.Response.Master.Action data) : base(data)
        {
        }
        protected override TargetSelectType? _targetSelectType { get { return _arg1 > 0 ? (TargetSelectType?)_arg1 : null; } }
        protected override int? _targetTagFlags { get { return _arg2 > 0 ? (int?)_arg2 : null; } }
        float             _shadowNumBonus   { get { return (float)_arg3 / 100; } }
        protected override ActionEffectType? _actionEffectType { get { return _arg4 > 0 ? (ActionEffectType?)_arg4 : null; } }

        public override void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            base.PreProcess(executor, targetCandidates);
            if (_shouldUseActionView())
            {
                var h = (ushort)targetCandidates.Average(t => t.GetGridPosition().Value.HorizontalIndex);
                var v = (ushort)targetCandidates.Average(t => t.GetGridPosition().Value.VerticalIndex);
                var centerGrid = new Grid.GridPosition(h, v);
                _tryToSetActionViewSkinLevel(executor.SkinLevel);
                _tryActionViewAppearance(centerGrid, executor.GetDirection(), AnimationName, 1000);
            }
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            var attributeIdToLevelDrivenAttributeParamMap = _getAttributeIdToLevelDrivenAttributeParamMap();
            var attributeIdKeys = new uint[attributeIdToLevelDrivenAttributeParamMap.Keys.Count];
            attributeIdToLevelDrivenAttributeParamMap.Keys.CopyTo(attributeIdKeys, 0);
            foreach (var attributeId in attributeIdKeys)
            {
                var levelDrivenAttributeParam = attributeIdToLevelDrivenAttributeParamMap[attributeId];
                attributeIdToLevelDrivenAttributeParamMap[attributeId] = levelDrivenAttributeParam * _getBounusCoefficient(executor);
            }
            foreach (var target in targets)
            {
                var actionEvent = new ActionEvent(executor, target, Level);
                if (attributeIdToLevelDrivenAttributeParamMap.Count > 0)
                {
                    actionEvent.SetAttributeIDToLevelDrivenAttributeparamMap(attributeIdToLevelDrivenAttributeParamMap);
                }
                target.ReceiveActionEvent(actionEvent);
            }
            _tryCameraShake();
        }
        public override void PostProcess(IActionExecutor executor)
        {
            base.PostProcess(executor);
            _tryActionViewDisappearance();
        }

        float _getBounusCoefficient(IActionExecutor executor)
        {
            return (1 + _shadowNumBonus * executor.GetShadowNum());
        }
    }
}
