using Noroshi.BattleScene.Actions.Roles;
using UniLinq;
using UniRx;

namespace Noroshi.BattleScene.Actions
{
    // 「対象のキャラクター」もしくは「もっとも最大値に近いエネルギーを持つキャラクター」のエネルギーを増やす
    // RatioDamageを参考に作成
    public class ZoaEnergyAction : AbstractAction
    {
        ZoaSearchable _zoaSearchable;
        RatioDamageable _ratioDamageable;
        public ZoaEnergyAction(Core.WebApi.Response.Master.Action data) : base(data)
        {
            _zoaSearchable = new ZoaSearchable()
            {
                BuddyCharacterID = _buddyCharacterID,
            };
            _ratioDamageable = new RatioDamageable(
                _targetStateId,
                _getAttributeIdToLevelDrivenAttributeParamMap(),
                _hitCharacterEffectId, _hitSoundId,
                Level
            );
        }
        uint _buddyCharacterID { get { return (uint)_arg1; } }
        float _noBuddyRatio { get { return _arg2 / 100.0f; } }

        public override IActionTarget[] GetTargets(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            return _zoaSearchable.Search(actionTargetFinder, executor).ToArray();
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            _ratioDamageable.RatioDamageMulti(executor, _zoaSearchable.FilterTargets(actionTargetFinder, targets), null, _getEnergyDamageRatio());
        }
            
        float _getEnergyDamageRatio()
        {
            var buddyRatio = _zoaSearchable.IsBuddy ? 1.0f : _noBuddyRatio;
            return _getLevelDrivenParam1() / 100.0f * buddyRatio;
        }
    }
}
