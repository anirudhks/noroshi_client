﻿using System;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.Game.Battle;
using CharacterConstant = Noroshi.Core.Game.Character.Constant;
using UniLinq;
using Noroshi.Character;
using Noroshi.BattleScene.Camera;

namespace Noroshi.BattleScene.Actions
{
    public enum DamageType
    {
        Physical = 1,
        Magical = 2,
    }
    public enum TargetSortType
    {
        CurrentHPAsc = 1,
        MaxHPDesc = 2,
        PositionFront = 3,
        Random = 4,
        HPDamageDesc = 5,
        EnergyDamageAsc = 6,
    }
    public enum ActionEffectType
    {
        ViewOnly = 1,
        ViewAndCameraShake = 2,
        CameraShakeOnly = 3,
    }

    public abstract class AbstractAction : IAction
    {
        Core.WebApi.Response.Master.Action _data;
        Dictionary<byte, int> _overrideArgs = new Dictionary<byte, int>();
        Dictionary<byte, uint> _overrideAttributes = new Dictionary<byte, uint>();
        protected Noroshi.MonoBehaviours.AnimationInformation _animation;
        IActionView _actionView = null;
        protected Subject<CharacterAnimationEvent> _onCommandExecutorAnimationSubject = new Subject<CharacterAnimationEvent>();
        Subject<CameraEvent> _onTryCommandCameraSubject = new Subject<CameraEvent>();
        protected Subject<bool> _onMaxAccumulationSubject = new Subject<bool>();
        protected Subject<bool> _onExitAccumulationTimeSubject = new Subject<bool>();
        public byte Rank { get; private set; }
        public ushort Level { get; private set; }

        public AbstractAction(Core.WebApi.Response.Master.Action data)
        {
            _data = data;
        }
        public void SetRank(byte rank) { Rank = rank; }

        public uint ID { get { return _data.ID; } }

        public Trigger Trigger { get { return (Trigger)_data.TriggerID; } }
        public float? ExecutableProbability { get { return _data.ExecutableProbability; } }
        public byte? ExecutableNum { get { return _data.ExecutableNum; } }
        public byte? ExecutorTargetable { get { return _data.ExecutorTargetable; } }
        protected TargetSortType? _targetSortType { get { return (TargetSortType?)_data.TargetSortType; } }
        protected byte? _maxTargetNum { get { return _data.MaxTargetNum; } }
        public DamageType? DamageType { get { return _data.DamageType > 0 ? (DamageType?)_data.DamageType : null; } }
        protected DamageMagicalAttribute? _damageMagicalAttribute { get { return _data.DamageMagicalAttribute > 0 ? (DamageMagicalAttribute?)_data.DamageMagicalAttribute : null; } }
        protected TargetStateID? _targetStateId { get { return _data.TargetStateID > 0 ? (TargetStateID?)_data.TargetStateID : null; } }
        uint? _attributeId1 { get { return _overrideAttributes.ContainsKey(1) ? (uint?)_overrideAttributes[1] : _data.AttributeID1 > 0 ? (uint?)_data.AttributeID1 : null; } }
        float _attributeIntercept1 { get { return _data.AttributeIntercept1; } }
        float _attributeSlope1 { get { return _data.AttributeSlope1; } }
        uint? _attributeId2 { get { return _overrideAttributes.ContainsKey(2) ? (uint?)_overrideAttributes[2] : _data.AttributeID2 > 0 ? (uint?)_data.AttributeID2 : null; } }
        float _attributeIntercept2 { get { return _data.AttributeIntercept2; } }
        float _attributeSlope2 { get { return _data.AttributeSlope2; } }

        protected int _arg1  { get { return _overrideArgs.ContainsKey(1) ? _overrideArgs[1] : _data.Arg1 ; } }
        protected int _arg2  { get { return _overrideArgs.ContainsKey(2) ? _overrideArgs[2] : _data.Arg2 ; } }
        protected int _arg3  { get { return _overrideArgs.ContainsKey(3) ? _overrideArgs[3] : _data.Arg3 ; } }
        protected int _arg4  { get { return _overrideArgs.ContainsKey(4) ? _overrideArgs[4] : _data.Arg4 ; } }
        protected int _arg5  { get { return _overrideArgs.ContainsKey(5) ? _overrideArgs[5] : _data.Arg5 ; } }
        protected int _arg6  { get { return _overrideArgs.ContainsKey(6) ? _overrideArgs[6] : _data.Arg6 ; } }
        protected int _arg7  { get { return _overrideArgs.ContainsKey(7) ? _overrideArgs[7] : _data.Arg7 ; } }
        protected int _arg8  { get { return _overrideArgs.ContainsKey(8) ? _overrideArgs[8] : _data.Arg8 ; } }
        protected int _arg9  { get { return _overrideArgs.ContainsKey(9) ? _overrideArgs[9] : _data.Arg9 ; } }
        protected int _arg10 { get { return _overrideArgs.ContainsKey(10) ? _overrideArgs[10] : _data.Arg10; } }

        float _intercept1 { get { return _data.Intercept1; } }
        float _slope1     { get { return _data.Slope1    ; } }
        float _intercept2 { get { return _data.Intercept2; } }
        float _slope2     { get { return _data.Slope2    ; } }
        float _intercept3 { get { return _data.Intercept3; } }
        float _slope3     { get { return _data.Slope3    ; } }

        protected uint? _hitCharacterEffectId { get { return _data.HitCharacterEffectID; } }
        protected uint? _hitSoundId { get { return _data.HitSoundID == 0 ? null : (uint?)_data.HitSoundID; } }

        protected float _getLevelDrivenAttributeParam1() { return _attributeIntercept1 + _attributeSlope1 * (Level - 1); }
        protected float _getLevelDrivenAttributeParam2() { return _attributeIntercept2 + _attributeSlope2 * (Level - 1); }
        protected float _getLevelDrivenParam1() { return _intercept1 + _slope1 * (Level - 1); }
        protected float _getLevelDrivenParam2() { return _intercept2 + _slope2 * (Level - 1); }
        protected float _getLevelDrivenParam3() { return _intercept3 + _slope3 * (Level - 1); }

        ushort? _necessaryEnergyToEnterActiveAction { get { return _data.NecessaryEnergy; } }
        // アクティブスキル発動に必要なエネルギー値を取得する
        public ushort GetNecessaryEnergyToEnterActiveAction()
        {
            return ShouldReduceEnergyWhenEnterActiveAction() ? _necessaryEnergyToEnterActiveAction.Value : (ushort)CharacterEnergy.MAX_ENERGY;
        }
        public bool ShouldReduceEnergyWhenEnterActiveAction()
        {
            return _necessaryEnergyToEnterActiveAction.HasValue;
        }

        /// （複数アニメーションで構成される場合）次のアニメーションを取得。
        public virtual CharacterAnimationEvent? GetNextCharacterAnimationEvent() { return null; }
        /// （プレイヤーの意思で）アクションに影響を与える。
        public virtual void Affect() {}
        // チャージ系アクションかどうか
        public virtual bool IsAccumulationAction { get { return false; } }
        // チャージ中かどうか
        public virtual bool IsAccumulating { get { return false; } }

        protected virtual ActionEffectType? _actionEffectType { get { return null; } }
        protected void _tryCameraShake()
        {
            if (_shouldUseCameraShake()) _onTryCommandCameraSubject.OnNext(new CameraEvent()
            {
                Command = CameraEvent.CameraCommand.Play,
                Type = CameraEvent.CameraType.Action,
            });
        }

        public void SetLevel(ushort level)
        {
            Level = level;
        }
        public void SetAnimation(Noroshi.MonoBehaviours.AnimationInformation animation)
        {
            _animation = animation;
        }
        public virtual IObservable<KeyValuePair<byte, Dictionary<byte, int>>> GetOnOverrideArgsObservable()
        {
            return Observable.Empty<KeyValuePair<byte, Dictionary<byte, int>>>();
        }
        public virtual IObservable<KeyValuePair<byte, Dictionary<byte, uint>>> GetOnOverrideAttributeIdsObservable()
        {
            return Observable.Empty<KeyValuePair<byte, Dictionary<byte, uint>>>();
        }
        public void OverrideArg(byte argNo, int overrideValue)
        {
            if (_overrideArgs.ContainsKey(argNo)) _overrideArgs.Remove(argNo);
            _overrideArgs.Add(argNo, overrideValue);
        }
        public void OverrideAttributeId(byte attributeNo, uint attributeId)
        {
            if (_overrideAttributes.ContainsKey(attributeNo)) _overrideAttributes.Remove(attributeNo);
            _overrideAttributes.Add(attributeNo, attributeId);
        }

        protected Dictionary<uint, float> _getAttributeIdToLevelDrivenAttributeParamMap()
        {
            var attributeIdToLevelDrivenAttribuParamMap = new Dictionary<uint, float>();
            // この段階でHasValueチェックを行いIDが設定されているものだけを返す
            if (_attributeId1.HasValue) attributeIdToLevelDrivenAttribuParamMap.Add(_attributeId1.Value, _getLevelDrivenAttributeParam1());
            if (_attributeId2.HasValue) attributeIdToLevelDrivenAttribuParamMap.Add(_attributeId2.Value, _getLevelDrivenAttributeParam2());
            return attributeIdToLevelDrivenAttribuParamMap;
        }

        public abstract IActionTarget[] GetTargets(IActionTargetFinder actionTargetFinder, IActionExecutor executor);
        public virtual void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates) {}
        public abstract void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets);
        public virtual void PostProcess(IActionExecutor executor) {}
        public virtual void Cancel(IActionExecutor executor) {}
        public virtual void Reset(IActionExecutor executor) {}

        public virtual void EnterTimeStop()
        {
            if (_shouldUseActionView()) _actionView.PauseOn();
            if (_shouldUseCameraShake()) _onTryCommandCameraSubject.OnNext(new CameraEvent()
            {
                Command = CameraEvent.CameraCommand.Pause,
                Type = CameraEvent.CameraType.Action,
            });
        }
        public virtual void ExitTimeStop()
        {
            if (_shouldUseActionView()) _actionView.PauseOff();
            if (_shouldUseCameraShake()) _onTryCommandCameraSubject.OnNext(new CameraEvent()
            {
                Command = CameraEvent.CameraCommand.Replay,
                Type = CameraEvent.CameraType.Action,
            });
        }

        public virtual void SetVisible(bool isVisible)
        {
            if (_shouldUseActionView()) _actionView.SetVisible(isVisible);
        }

        // TODO : リポジトリ抽象化
        public virtual IObservable<IAction> LoadAdditionalDatas(Master.CharacterMaster characterMaster)
        {
            return Observable.Return<IAction>(this);
        }
        public virtual IObservable<IAction> LoadAssets(IActionExecutor executor, IActionFactory factory)
        {
            if (!_shouldUseActionView()) return Observable.Return<IAction>(this);
            return factory.BuildActionView(executor.CharacterID).Select(v => 
                                                                        {
                _actionView = v;
                _actionView.Disappear();
                return (IAction)this;
            });
        }

        public IObservable<CharacterAnimationEvent> GetOnCommandExecutorAnimationObservable()
        {
            return _onCommandExecutorAnimationSubject.AsObservable();
        }
        public virtual IObservable<CameraEvent> GetOnTryCommandCameraObservable()
        {
            return _onTryCommandCameraSubject.AsObservable();
        }
        public IObservable<bool> GetOnMaxAccumulationObservable()
        {
            return _onMaxAccumulationSubject.AsObservable();
        }
        public IObservable<bool> GetOnExitAccumulationTimeObservable()
        {
            return _onExitAccumulationTimeSubject.AsObservable();
        }

        public CharacterAnimationEvent CharacterAnimationEvent
        {
            get
            {
                return new CharacterAnimationEvent
                {
                    CharacterAnimation = CharacterConstant.ACTION_ANIMATIONS[Rank],
                    Loop = false,
                    AnimationNameSuffix = _animationNameSuffix,
                };
            }
        }

        public string AnimationName { get { return CharacterConstant.ANIMATION_NAME_MAP[CharacterConstant.ACTION_ANIMATIONS[Rank]]; } }
        protected virtual string _animationNameSuffix { get { return null; } }

        protected void _tryToSetActionViewSkinLevel(int skinLevel)
        {
            if (_shouldUseActionView()) _actionView.SetSkin(skinLevel);
        }

        protected void _tryActionViewAppearance(Grid.GridPosition grid, Grid.Direction direction, string animationName, int sortOrder)
        {
            if (_shouldUseActionView()) _actionView.Appear(grid, direction, animationName, sortOrder);
        }
        protected void _tryActionViewAppearance(UnityEngine.Vector2 position, Grid.Direction direction, string animationName, int sortOrder)
        {
            if (_shouldUseActionView()) _actionView.Appear(position, direction, animationName, sortOrder);
        }
        protected void _tryActionViewUpdatePositionWithSynchronismOnly(Grid.GridPosition grid)
        {
            if (_shouldUseActionView()) _actionView.UpdatePositionWithSynchronismOnly(grid);
        }
        protected IObservable<IActionView> _tryToLaunchActionView(IActionExecutorView executorView, Grid.GridPosition grid, Grid.Direction direction, string animationName, int sortOrder)
        {
            return _shouldUseActionView() ? _actionView.Launch(executorView, grid, direction, animationName, sortOrder) : Observable.Empty<IActionView>();
        }
        protected IObservable<IActionView> _tryToLaunchActionView(IActionExecutorView executorView, Grid.GridPosition grid, Grid.Direction direction, string animationName, int sortOrder, byte? animationSequenceNumber)
        {
            return _shouldUseActionView() ? _actionView.Launch(executorView, grid, direction, animationName, sortOrder, animationSequenceNumber) : Observable.Empty<IActionView>();
        }
        protected void _tryActionViewDisappearance()
        {
            if (_shouldUseActionView()) _actionView.Disappear();
        }

        protected bool _shouldUseCameraShake()
        {
            if (_actionEffectType.HasValue)
            {
                return (_actionEffectType.Value == ActionEffectType.ViewAndCameraShake ||
                        _actionEffectType.Value == ActionEffectType.CameraShakeOnly);
            }
            return false;
        }
        
        protected bool _shouldUseActionView()
        {
            if (_actionEffectType.HasValue)
            {
                return (_actionEffectType.Value == ActionEffectType.ViewOnly || 
                        _actionEffectType.Value == ActionEffectType.ViewAndCameraShake);
            }
            return false;
        }

        protected Grid.GridPosition _getCenterGridPosition(IActionTarget[] targets)
        {
            return _getCenterGridPosition(targets, 0, null);
        }

        protected Grid.GridPosition _getCenterGridPosition(IActionTarget[] targets, short horizontalDiff, Grid.Direction? direction)
        {
            var h = (ushort)targets.Average(t => t.GetGridPosition().Value.HorizontalIndex);
            if (horizontalDiff != 0 && direction.HasValue)
            {
                h = (ushort)((short)h + (direction.Value == Grid.Direction.Right ? -horizontalDiff : horizontalDiff));
            }
            var v = (ushort)targets.Average(t => t.GetGridPosition().Value.VerticalIndex);
            return new Grid.GridPosition(h, v);
        }

        // 発動可能なアクティブアクションの種類を取得する
        public ActiveActionType? GetActiveActionType()
        {
            // 一旦は、発動可能なエネルギー値が設定されているものをゲージ減少系と捉える
            if (ShouldReduceEnergyWhenEnterActiveAction()) return ActiveActionType.ReductionGauge;
            // 特定のフラグが立っていたらチャージ系と捉える
            if (IsAccumulationAction) return ActiveActionType.Accumulation;

            // 特に判断基準を満たしていないなら通常のアクティブアクション
            return ActiveActionType.Normal;
        }

        public override string ToString()
        {
            return String.Format("ID:{0}, Type:{1}", ID, GetType().Name);
        }
    }
}
