﻿using Noroshi.BattleScene.Actions.Roles;
using UniLinq;
using UniRx;

namespace Noroshi.BattleScene.Actions
{
    // 「対象のキャラクターと自分自身」もしくは「もっとも前線にいるキャラクター」の物理防御力を上昇させる
    // GiveAttributeを参考に作成
    public class ZauDefenseAction : AbstractAction
    {
        ZauSearchable _zauSearchable;
        public ZauDefenseAction(Core.WebApi.Response.Master.Action data) : base(data)
        {
            _zauSearchable = new ZauSearchable()
            {
                BuddyCharacterID = _buddyCharacterID,
            };
        }
        uint _buddyCharacterID { get { return (uint)_arg1; } }
        float _noBuddyRatio { get { return _arg2 / 100.0f; } }

        public override IActionTarget[] GetTargets(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            return _zauSearchable.Search(actionTargetFinder, executor).ToArray();
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            var buddyRatio = _zauSearchable.IsBuddy ? 1.0f : _noBuddyRatio;

            var attributeIdToLevelDrivenAttributeParamMap = _getAttributeIdToLevelDrivenAttributeParamMap();
            var attributeIdKeys = new uint[attributeIdToLevelDrivenAttributeParamMap.Keys.Count];
            attributeIdToLevelDrivenAttributeParamMap.Keys.CopyTo(attributeIdKeys, 0);
            foreach (var attributeId in attributeIdKeys)
            {
                var levelDrivenAttributeParam = attributeIdToLevelDrivenAttributeParamMap[attributeId];
                attributeIdToLevelDrivenAttributeParamMap[attributeId] = levelDrivenAttributeParam * buddyRatio;
            }
            foreach (var target in targets)
            {
                var actionEvent = new ActionEvent(executor, target, Level);
                if (attributeIdToLevelDrivenAttributeParamMap.Count > 0)
                {
                    actionEvent.SetAttributeIDToLevelDrivenAttributeparamMap(attributeIdToLevelDrivenAttributeParamMap);
                }
                target.ReceiveActionEvent(actionEvent);
            }
        }
    }
}
