﻿using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using CharacterConstant = Noroshi.Core.Game.Character.Constant;
using Noroshi.Character;
using Noroshi.BattleScene.Actions.Roles;
using TargetStateID = Noroshi.Core.Game.Battle.TargetStateID;

namespace Noroshi.BattleScene.Actions
{

    // 溜め攻撃(Range指定版)
    public class AccumulationRangeAttack : AbstractRangeAttack
    {
        bool _hasExitedAccumulationTime;
        float _accumulationTime = 0.0f;
        float _accumulationCoefficeint = 1.0f;
        IDisposable _timerDisposable = null;
        int _executeNum = 0;
        bool _isPause = false;
        bool _isAccumulating = false;

        public AccumulationRangeAttack(Core.WebApi.Response.Master.Action data) : base(data)
        {
        }

        protected override RelativeForce? _targetRelativeForce { get { return _arg1 > 0 ? (RelativeForce?)_arg1 : null; } }
        public override int MinRange { get { return _arg2; } }
        public override int MaxRange { get { return _arg3; } }
        protected override float? _damageCoefficient { get { return _arg4 > 0 ? (float?)_arg4 / 100.0f : null; } }

        public override void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            base.PreProcess (executor, targetCandidates);
            _hasExitedAccumulationTime = false;
            _accumulationTime = Constant.ACTIVE_ACTION_ACCUMULATION_TIME;
            _accumulationCoefficeint = 1.0f;
            _isAccumulating = true;
            _executeNum = 0;
            // チャージ中の係数上昇処理
            _timerDisposable = SceneContainer.GetTimeHandler().Interval(1.0f)
            .Where(_ => !_isPause)
            .Do(_ => 
            {
                --_accumulationTime;
                _calculateAccumulationCoefficeint();
            })
            .TakeWhile(_ => _accumulationTime > 0.0f && !_hasExitedAccumulationTime)
            .Subscribe(
            _ => {},
            () => 
            {
                // チャージ完了を通知
                _onMaxAccumulationSubject.OnNext(true);
            });
            
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            _executeNum++;
            _attackable.AttackMulti(executor, GetTargets(actionTargetFinder, executor), (float?)_accumulationCoefficeint);
        }

        public override void Cancel(IActionExecutor executor)
        {
            // チャージ中にキャンセルが呼ばれた場合すぐさま実行させる
            if (_isAccumulating)
            {
                _exitAccumulationTime();
                _onExitAccumulationTimeSubject.OnNext(true);
                _isAccumulating = false;
            }
            if (_timerDisposable != null) _timerDisposable.Dispose();
        }

        public override void PostProcess(IActionExecutor executor)
        {
            base.PostProcess (executor);
            if (_timerDisposable != null) _timerDisposable.Dispose();
            _isAccumulating = false;
        }

        public override void EnterTimeStop()
        {
            _isPause = true;
            base.EnterTimeStop();
        }
        public override void ExitTimeStop()
        {
            _isPause = false;
            base.ExitTimeStop();
        }

        public override bool IsAccumulationAction { get { return true; } }
        public override bool IsAccumulating { get { return _isAccumulating; } }
        protected override string _animationNameSuffix { get { return "charge"; } }

        /// （複数アニメーションで構成される場合）次のアニメーションを取得。
        public override CharacterAnimationEvent? GetNextCharacterAnimationEvent()
        {
            if (_hasExitedAccumulationTime) return null;
            return new CharacterAnimationEvent
            {
                CharacterAnimation = CharacterConstant.ACTION_ANIMATIONS[Rank],
                Loop = true,
                AnimationNameSuffix = "wait",
            };
        }

        /// （プレイヤーの意思で）アクションに影響を与える。
        public override void Affect()
        {
            // 実行前なら実行する
            if (!_hasExitedAccumulationTime)
            {
                _onExitAccumulationTimeSubject.OnNext(true);
                _exitAccumulationTime();
            }
        }

        /// 蓄積時間終了処理。
        void _exitAccumulationTime()
        {
            _hasExitedAccumulationTime = true;
            _isAccumulating = false;
            // 蓄積処理終了と同時に発動アニメーションへ切り替える。
            _onCommandExecutorAnimationSubject.OnNext(new CharacterAnimationEvent
            {
                CharacterAnimation = CharacterConstant.ACTION_ANIMATIONS[Rank],
                Loop = false,
                AnimationNameSuffix = "execute",
            });
        }

        void _calculateAccumulationCoefficeint()
        {
            //Easing Tyype : InCubic
            // 経過時間
            var elapsedTime = Constant.ACTIVE_ACTION_ACCUMULATION_TIME - _accumulationTime;
            elapsedTime /= Constant.ACTIVE_ACTION_ACCUMULATION_TIME;
            _accumulationCoefficeint = Constant.ACTIVE_ACTION_ACCUMULATION_MAX_COEFFICEINT *
                elapsedTime * elapsedTime * elapsedTime + 1.0f;
            if (_accumulationCoefficeint > Constant.ACTIVE_ACTION_ACCUMULATION_MAX_COEFFICEINT ) _accumulationCoefficeint = Constant.ACTIVE_ACTION_ACCUMULATION_MAX_COEFFICEINT;
        }

    }
}
