using Noroshi.BattleScene.Actions.Roles;

namespace Noroshi.BattleScene.Actions
{
    public abstract class AbstractRangeAttack : AbstractRangeAction
    {
        protected Attackable _attackable;
        protected abstract float? _damageCoefficient { get; }

        public AbstractRangeAttack(Core.WebApi.Response.Master.Action data) : base(data)
        {
        }

        public override void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            base.PreProcess(executor, targetCandidates);
            // コストはたいしてかからないので、パラメータの最新化を優先して、ここで毎回インスタンス化する。
            _attackable = new Attackable(
                DamageType, _damageMagicalAttribute,
                _targetStateId,
                _getAttributeIdToLevelDrivenAttributeParamMap(),
                _hitCharacterEffectId, _hitSoundId,
                Level,
                _getDamageBoost(),
                _damageCoefficient
            );
        }

        protected int _getDamageBoost()
        {
            return (int)_getLevelDrivenParam1();
        }
    }
}
