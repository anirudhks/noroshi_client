using System.Collections.Generic;
using UniLinq;
using UniRx;
using Flaggs.Extensions.Heam;
using Noroshi.Character;
using Noroshi.BattleScene.Actions.Roles;

namespace Noroshi.BattleScene.Actions
{
    /// <summary>
    /// アクションを扱うクラス。
    /// </summary>
    public class ActionHandler
    {
        /// アクション ID（null 含む）。
        readonly uint?[] _nullableActionIds;
        /// アクションレベル（null 含む）。
        ushort?[] _nullableActionLevels;
        /// オートアクション順序管理オブジェクト。
        readonly ActionSequence[] _sequences;
        ActionSequence _sequence;
        /// 最大利用可能アクションランク。
        byte? _maxAvailableActionRank;
        /// アクション ID。
        readonly uint[] _actionIds;
        /// アクション。
        readonly IAction[] _actions;
        /// 利用可能アクション。
        readonly List<IAction> _availableActions = new List<IAction>();
        /// ランク => オートアクションマップ。
        readonly Dictionary<byte, IAction> _rankToAutomaticActionMap = new Dictionary<byte, IAction>();
        /// ランク => 現発動数マップ。
        readonly Dictionary<byte, uint> _rankToCurrentActionNum = new Dictionary<byte, uint>();
        /// 発動中アクション。
        IAction _currentAction;
        /// 発動中アクションの準備時における対象（実際の対象となるかはまた別問題）。
        IActionTarget[] _currentTargetCandidates;
        /// 準備済みの次アクション。
        IAction _nextAction;
        /// 準備済みの次アクションの準備時における対象（実際の対象となるかはまた別問題）。
        IActionTarget[] _nextTargetCandidates;
        /// アニメーションが正常に終了したかどうかフラグ。
        bool _successInPlayAnimation = false;
        /// アクション制限オブジェクト。
        readonly ActionLimiter _actionLimiter = new ActionLimiter();
        /// Dispose 集合。
        readonly CompositeDisposable _disposables = new CompositeDisposable();


        public ActionHandler(uint?[] nullableActionIds, ushort?[] nullableActionLevels, ActionSequence[] sequences, byte? maxAvailableActionRank)
        {
            _nullableActionIds = nullableActionIds;
            _nullableActionLevels = nullableActionLevels;
            _maxAvailableActionRank = maxAvailableActionRank;
            _sequences = sequences;
            _actionIds = _nullableActionIds.ToArrayWithOptimization(actionId => actionId.HasValue, actionId => actionId.Value);
            _actions = new IAction[_actionIds.Length];
        }

        public IObservable<ActionHandler> LoadDatas()
        {
            // 各アクションをビルド。
            var actionMap = ActionBuilder.BuildMulti(GlobalContainer.MasterManager.CharacterMaster.GetActions(_actionIds))
                .ToDictionary(action => action.ID);
            var actionCount = 0;
            for (byte rank = 0; rank < _nullableActionIds.Length; rank++)
            {
                if (!_nullableActionIds[rank].HasValue) continue;
                var action = actionMap[_nullableActionIds[rank].Value];
                action.SetRank(rank);
                action.SetLevel(_nullableActionLevels[rank].Value);
                if (!_maxAvailableActionRank.HasValue || rank <= _maxAvailableActionRank.Value)
                {
                    _availableActions.Add(action);
                }
                if (action.Trigger == Trigger.Normal)
                {
                    _rankToAutomaticActionMap.Add(rank, action);
                }
                _actions[actionCount++] = action;
                _rankToCurrentActionNum.Add(rank, 0);
            }
            _sequence = _sequences.FirstOrDefault(ase => ase.GetTargetActionNum() == _availableActions.Count);
            return Observable.WhenAll(_actions.Select(a => a.LoadAdditionalDatas(GlobalContainer.MasterManager.CharacterMaster)))
            .Do(_ => 
            {
                _actions.Select(action => action.GetOnOverrideArgsObservable()).Merge().Subscribe(rankToOverrideArgs =>
                {
                    _overrideArgs(rankToOverrideArgs.Key, rankToOverrideArgs.Value);
                })
                .AddTo(_disposables);
                _actions.Select(action => action.GetOnOverrideAttributeIdsObservable()).Merge().Subscribe(rankToOverrideAttributeIds => 
                {
                    _overrideAttributeIds(rankToOverrideAttributeIds.Key, rankToOverrideAttributeIds.Value);
                })
                .AddTo(_disposables);
            })
            .Select(_ => this);
        }
        public IObservable<ActionHandler> LoadAssets(IActionExecutor executor, IActionFactory factory, IEnumerable<Noroshi.MonoBehaviours.AnimationInformation> animations)
        {
            var animationMap = animations.ToDictionary(a => a.Name);
            return Observable.WhenAll(_actions.Select(a =>
            {
                if (animationMap.ContainsKey(a.AnimationName)) a.SetAnimation(animationMap[a.AnimationName]);
                return a.LoadAssets(executor, factory);
            }))
            .Select(_ => this);
        }


        void _overrideArgs(byte rank, Dictionary<byte, int> overrideValues)
        {
            var action = _actions.FirstOrDefault(a => a.Rank == rank);
            if (action != null)
            {
                foreach (var kv in overrideValues)
                {
                    action.OverrideArg(kv.Key, kv.Value);
                }
            }
        }
        void _overrideAttributeIds(byte rank, Dictionary<byte, uint> overrideAttributeValues)
        {
            var action = _actions.FirstOrDefault(a => a.Rank == rank);
            if (action != null)
            {
                foreach (var kv in overrideAttributeValues)
                {
                    action.OverrideAttributeId(kv.Key, kv.Value);
                }
            }
        }

        public void PauseOn()
        {
            if (_currentAction != null) _currentAction.EnterTimeStop();
        }
        public void PauseOff()
        {
            foreach (var action in _actions)
            {
                action.ExitTimeStop();
            }
        }

        /// 最大利用可能アクションランクを上書き。
        public void SetAvailableActionRank(byte? maxAvailableActionRank)
        {
            _maxAvailableActionRank = maxAvailableActionRank;
            _availableActions.Clear();
            var actionCount = 0;
            for (byte rank = 0; rank < _nullableActionIds.Length; rank++)
            {
                if (_nullableActionIds[rank].HasValue)
                {
                    if (!_maxAvailableActionRank.HasValue || rank <= _maxAvailableActionRank.Value)
                    {
                        _availableActions.Add(_actions[actionCount]);
                    }
                    actionCount++;
                }
            }
            // 最大利用可能アクションランクを上書きした際のアクションの増減によってアクション順序を再取得
            if (_maxAvailableActionRank.HasValue) _sequence = _sequences.FirstOrDefault(ase => ase.GetTargetActionNum() == _availableActions.Count);
        }
        /// アクションレベルを上書き。
        public void OverrideActionLevels(ushort?[] nullableActionLevels)
        {
            _nullableActionLevels = nullableActionLevels;
            for (byte rank = 0; rank < _nullableActionIds.Length; rank++)
            {
                var level = _nullableActionLevels[rank];
                if (level.HasValue)
                {
                    foreach (var action in _actions)
                    {
                        if (action.Rank == rank) action.SetLevel(level.Value);
                    }
                }
            }
        }

        /// アニメーション操作時に操作内容が OnNext される Observable を取得。
        public IObservable<CharacterAnimationEvent> GetOnCommandExecutorAnimationObservable()
        {
            return _actions.Select(a => a.GetOnCommandExecutorAnimationObservable()).Merge();
        }
        /// カメラ操作時に操作内容が OnNext される Observable を取得。
        public IObservable<Noroshi.BattleScene.Camera.CameraEvent> GetOnTryCommandCameraObservable()
        {
            return _actions.Select(a => a.GetOnTryCommandCameraObservable()).Merge();
        }
        // チャージアクションにてチャージが最大まで溜まった時に OnNext される Observable
        public IObservable<bool> GetOnMaxAccumulationObservable()
        {
            return _actions.Select(a => a.GetOnMaxAccumulationObservable()).Merge();
        }
        // チャージアクションにてチャージ終了時に OnNext される Observable
        public IObservable<bool> GetOnExitAccumulationTimeObservable()
        {
            return _actions.Select(a => a.GetOnExitAccumulationTimeObservable()).Merge();
        }

        /// 基本レンジを取得。
        public int GetBaseRange()
        {
            return ((AbstractRangeAction)_rankToAutomaticActionMap[0]).MaxRange;
        }

        /// 次（準備済み）アクションのアニメーション情報を取得。
        public CharacterAnimationEvent GetNextCharacterAnimationEvent()
        {
            return _nextAction.CharacterAnimationEvent;
        }
        /// 次（準備済み）アクションのダメージ種別を取得。
        public DamageType? GetNextActionDamageType()
        {
            return _nextAction.DamageType;
        }

        /// 現アクションの（複数アニメーションで構成される場合）次のアニメーション情報を取得。
        public CharacterAnimationEvent? GetCurrentActionNextCharacterAnimationEvent()
        {
            return _currentAction != null ? _currentAction.GetNextCharacterAnimationEvent() : null;
        }

        /// （プレイヤーの意思で）現在のアクションに影響を与える。
        public void AffectCurrentAction()
        {
            if (_currentAction != null) _currentAction.Affect();
        }

        /// アクション対象としての選択可否チェック。
        public bool IsTargetable()
        {
            return _currentAction != null ? !_currentAction.ExecutorTargetable.HasValue : true;
        }

        /// 死亡発動アクションを保有しているかどうか。
        public bool HasDeadTriggerAction()
        {
            return _availableActions.Any(a => a.Trigger == Trigger.Dead);
        }
        /// 遅れて登場アクションを保有しているかどうか。
        public bool HasDelayAppearanceAction()
        {
            // 今は遅れて登場攻撃のみ。
            return _availableActions.Any(a => a.GetType() == typeof(DelayAppearanceAttack));
        }

        /// アクティブアクション ID を取得。
        public uint? GetActiveActionID()
        {
            var activeAction = _getActiveAction();
            return activeAction != null ? (uint?)activeAction.ID : null;
        }
        /// アクティブアクションレベルを取得。
        public ushort? GetActiveActionLevel()
        {
            var activeAction = _getActiveAction();
            return activeAction != null ? (ushort?)activeAction.Level : null;
        }
        /// アクティブアクション種別を取得。
        public ActiveActionType? GetActiveActionType()
        {
            var activeAction = _getActiveAction();
            return activeAction != null ? activeAction.GetActiveActionType() : null;
        }
        /// アクティブアクション状態に遷移した際にエネルギーを減らすべきかどうか。
        public bool ShouldReduceEnergyWhenEnterActiveAction()
        {
            var activeAction = _getActiveAction();
            return (activeAction != null ? activeAction.ShouldReduceEnergyWhenEnterActiveAction() : false) || HasAccumulationActiveAction();
        }
        /// アクティブアクション状態に遷移するのに必要なエネルギーを取得。
        public ushort GetNecessaryEnergyToEnterActiveAction()
        {
            var activeAction = _getActiveAction();
            return activeAction != null ? activeAction.GetNecessaryEnergyToEnterActiveAction() : (ushort)0;
        }
        // チャージ系のアクションかどうか
        public bool HasAccumulationActiveAction()
        {
            var activeAction = _getActiveAction();
            return activeAction != null ? activeAction.IsAccumulationAction : false;
        }

        // チャージ系のアクションでありかつすでにチャージ中の場合か
        public bool IsAccumulating()
        {
            var isAccumulating = _currentAction != null ? _currentAction.IsAccumulating : false;
            return HasAccumulationActiveAction() && isAccumulating;
        }

        /// 表示・非表示する。
        public void SetVisible(bool isVisible)
        {
            foreach (var action in _actions)
            {
                action.SetVisible(isVisible);
            }
        }

        /// アクション制限因子を追加する。
        public void AddActionLimiterFactor(ActionLimiter.Factor factor) { _actionLimiter.AddFactor(factor); }
        /// アクション制限因子を削除する。
        public void RemoveActionLimiterFactor(ActionLimiter.Factor factor) { _actionLimiter.RemoveFactor(factor); }

        /// 次のオートアクションが制限されているかチェック。
        /// PrepareNextAutomaticAction を呼ぶ前に利用すること。
        public bool NextAutomaticActionIsLimited()
        {
            var nextAutomaticAction = _getNextAutomaticAction();
            if (nextAutomaticAction == null) return false;
            return !_actionLimiter.CanInvokeAction(nextAutomaticAction.DamageType);
        }

        /// オートアクションを準備する。
        public bool PrepareNextAutomaticAction(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            var success = _prepareNextAction(_getNextAutomaticAction(), actionTargetFinder, executor);
            // 準備失敗の場合はスキップして次のアクションにしてしまう。
            if (!success) _switchAutomaticAction();
            return success;
        }
        /// アクティブアクションを準備する。
        public bool PrepareNextActiveAction(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            return _prepareNextAction(_getActiveAction(), actionTargetFinder, executor);
        }
        /// 開始時に発動するアクション（アニメーションあり）を準備する。
        public bool PrepareNextFirstWithAnimationAction(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            return _prepareNextAction(_getFirstWithAnimationAction(), actionTargetFinder, executor);
        }
        /// 死亡時に発動するアクションを準備する。
        public bool PrepareNextDeadAction(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            return _prepareNextAction(_getDeadAction(), actionTargetFinder, executor);
        }
        /// アクションイベントを受けた時に発動するアクションを準備する。
        public bool PrepareNextReceiveEventAction(IActionTargetFinder actionTargetFinder, IActionExecutor executor, ActionEvent actionEvent)
        {
            if (actionEvent.DamageType.HasValue && actionEvent.DamageType.Value == DamageType.Physical)
            {
                return _prepareNextAction(_getPhysicalDamageAction(), actionTargetFinder, executor);
            }
            return false;
        }
        /// 次のアクションを準備する。
        public bool _prepareNextAction(IAction nextAction, IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            // アクションがちゃんと存在して、発動可能でなければ失敗。
            if (nextAction == null || !_canInvokeAction(nextAction)) return false;
            // 現状況下でのアクション対象（実際の対象となるかはまた別問題）を取得。
            var targetCandidates = nextAction.GetTargets(actionTargetFinder, executor);
            // 現状況下でのアクション対象がいなければ失敗。
            if (targetCandidates.Length == 0) return false;
            // 次アクションと現状況下での次アクション対象をセット。
            _nextAction = nextAction;
            _nextTargetCandidates = targetCandidates;
            return true;
        }
        /// 発動可否チェック。
        bool _canInvokeAction(IAction action)
        {
            if (!_actionLimiter.CanInvokeAction(action.DamageType)) return false;
            if (action.Trigger == Trigger.PhysicalDamage && action.ExecutableProbability.HasValue)
            {
                if (!GlobalContainer.RandomGenerator.Lot(action.ExecutableProbability.Value))
                {
                    return false;
                }
            }
            return !action.ExecutableNum.HasValue || _rankToCurrentActionNum[action.Rank] < action.ExecutableNum.Value;
        }

        /// アクション系状態へ遷移した直後に呼ばれる事前処理。
        public void PreProcess(IActionExecutor executor)
        {
            // 準備されていた情報を現在のものとしてセットし直し。
            _currentAction = _nextAction;
            _currentTargetCandidates =_nextTargetCandidates;
            _nextAction = null;
            _nextTargetCandidates = null;

            _successInPlayAnimation = false;
            _rankToCurrentActionNum[_currentAction.Rank]++;
            _currentAction.PreProcess(executor, _currentTargetCandidates);
        }

        /// 開始時発動（アニメーションなし）アクションを即発動する。
        public bool ExecuteFirstActionDirectly(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            return _executeActionDirectly(actionTargetFinder, executor, _getFirstAction());
        }
        /// 他死亡アクションを即発動する。
        public bool ExecuteOtherDeadActionDirectly(IActionTargetFinder actionTargetFinder, IActionExecutor executor, RelativeForce force)
        {
            return _executeActionDirectly(actionTargetFinder, executor, _getOtherDeadAction(force));
        }
        /// アクションを即発動する。
        bool _executeActionDirectly(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IAction action)
        {
            if (_prepareNextAction(action, actionTargetFinder, executor))
            {
                // 割り込むことがあり得るので一時的に保存して、
                var currentAction = _currentAction;
                var currentTargetCandidates = _currentTargetCandidates;
                PreProcess(executor);
                ExecuteCurrentAction(actionTargetFinder, executor);
                SucceedInPlayAnimation();
                PostProcess(executor);
                // 戻す。
                _currentAction = currentAction;
                _currentTargetCandidates = currentTargetCandidates;
                return true;
            }
            return false;
        }

        /// アクション効果を実行する。
        public void ExecuteCurrentAction(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            _currentAction.Execute(actionTargetFinder, executor, _currentTargetCandidates);
        }

        /// アニメーション再生が最後まで成功フラグを立てる。
        public void SucceedInPlayAnimation()
        {
            _successInPlayAnimation = true;
            if (_currentAction.Trigger == Trigger.Normal) _switchAutomaticAction();
        }

        /// アクション系状態から遷移する直前に呼ばれる事後処理。
        public void PostProcess(IActionExecutor executor)
        {
            if (_successInPlayAnimation)
            {
                _currentAction.PostProcess(executor);
            }
            else
            {
                _currentAction.Cancel(executor);
            }
            _successInPlayAnimation = false;
            _resetCurrentAction();
        }

        /// 各ウェーブバトル終了時に呼ばれるリセット処理。
        public void Reset(IActionExecutor executor)
        {
            foreach (var action in _actions)
            {
                action.Reset(executor);
                _rankToCurrentActionNum[action.Rank] = 0;
            }
        }

        /// 現在アクションをリセットする。
        void _resetCurrentAction()
        {
            _currentAction = null;
            _currentTargetCandidates = null;
        }

        /// 次のオートアクションを取得。
        IAction _getNextAutomaticAction()
        {
            var rank = _sequence.GetRank();
            // もしも該当ランクのノーマルトリガーアクションがなければランク0のものを利用。
            if (!_rankToAutomaticActionMap.ContainsKey(rank)) rank = 0;
            return _rankToAutomaticActionMap[rank];
        }
        /// オートアクションを切り替える。
        void _switchAutomaticAction()
        {
            _sequence.Switch();
        }
        /// アクティブアクションを取得。
        IAction _getActiveAction() { return _getActionByTrigger(Trigger.Active); }
        /// 開始時発動（アニメーションなし）アクションを取得。
        IAction _getFirstAction() { return _getActionByTrigger(Trigger.First); }
        /// 開始時発動（アニメーションあり）アクションを取得。
        IAction _getFirstWithAnimationAction() { return _getActionByTrigger(Trigger.FirstWithAnimation); }
        /// 被物理ダメージ時発動アクションを取得。
        IAction _getPhysicalDamageAction() { return _getActionByTrigger(Trigger.PhysicalDamage); }
        /// 死亡時発動アクションを取得。
        IAction _getDeadAction() { return _getActionByTrigger(Trigger.Dead); }
        /// 他死亡時発動アクションを取得。
        IAction _getOtherDeadAction(RelativeForce relativeForce)
        {
            return _getActionByTrigger(relativeForce == RelativeForce.Enemy ? Trigger.EnemyDead : Trigger.OwnDead);
        }
        /// トリガー指定でアクションを取得。
        IAction _getActionByTrigger(Trigger trigger)
        {
            foreach (var action in _availableActions)
            {
                if (action.Trigger == trigger) return action;
            }
            return null;
        }

        /// リソース破棄。
        public void Dispose()
        {
            _disposables.Dispose();
        }
    }
}
