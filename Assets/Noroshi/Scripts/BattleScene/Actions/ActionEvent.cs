﻿using Noroshi.BattleScene.Actions.Attributes;
using Noroshi.Core.Game.Battle;
using System.Collections.Generic;
using UniLinq;

namespace Noroshi.BattleScene.Actions
{
    public class ActionEvent
    {
        public ActionEvent(IActionExecutor executor, IActionTarget target, ushort? actionLevel = null)
        {
            Executor    = executor;
            Target      = target;
            ActionLevel = actionLevel;
            Attributes  = new List<IAttribute>();
        }

        public void SetHPDamage(int damage)
        {
            HPDamage = damage;
        }
        public void SetHPDamage(int damage, DamageType? damageType)
        {
            HPDamage   = damage;
            DamageType = damageType;
        }
        public void SetEnergyDamage(int damage)
        {
            EnergyDamage = damage;
        }
        public void SetHitCharacterEffectIDAndSoundID(uint? characterEffectId, uint? soundId, bool isInterruptHitEffect = false)
        {
            if (characterEffectId.HasValue || soundId.HasValue)
            {
                if (characterEffectId.HasValue) HitCharacterEffectID = characterEffectId;
                if (soundId.HasValue) HitSoundID = soundId;
                IsInterruptHitEffect = isInterruptHitEffect;
            }
        }

        public void SetTargetStateID(TargetStateID targetStateId)
        {
            TargetStateID = targetStateId;
        }

        public void AddAttribute(IAttribute attribute)
        {
            Attributes.Add(attribute);
        }

        public void SetAttributeIDToLevelDrivenAttributeparamMap(Dictionary<uint, float> attributeIdToLevelDrivenAttributeParamMap)
        {
            AttributeIDToLevelDrivenAttributeParamMap = attributeIdToLevelDrivenAttributeParamMap;
        }
        public void SetForceRemoveAttributeIDs(uint[] attributeIds)
        {
            ForceRemoveAttributeIDs = attributeIds;
        }

        public void RemoveAttribute()
        {
            if (ForceRemoveAttributeIDs == null) return;
            if (AttributeIDToLevelDrivenAttributeParamMap == null) return;
            if (Attributes == null) return;
            foreach (var attributeId in ForceRemoveAttributeIDs)
            {
                AttributeIDToLevelDrivenAttributeParamMap.Remove(attributeId);
                Attributes.RemoveAll(a => a.AttributeID == attributeId);
            }
        }
        public void RemoveAllAttribute()
        {
            Attributes.Clear();
            AttributeIDToLevelDrivenAttributeParamMap.Clear();
        }

        public void RemoveNegativeAttribute()
        {
            var attributeIds = Attributes.Where(a => a.IsNegative).Select(a => a.AttributeID).ToArray();
            foreach (var attributeId in attributeIds)
            {
                AttributeIDToLevelDrivenAttributeParamMap.Remove(attributeId);
                Attributes.RemoveAll(a => a.AttributeID == attributeId);
            }
        }

        public void SetDodge()
        {
            Dodge = true;
        }

        public void ReduceHPDamage(int damage)
        {
            HPDamage -= damage;
            if (HPDamage < 0) HPDamage = 0;
        }

        public void ClearHPDamageAndEnergyDamage()
        {
            HPDamage = null;
            EnergyDamage = null;
        }

        /// アクション実行者
        public IActionExecutor Executor { get; private set; }
        /// アクション対象者
        public IActionTarget Target { get; private set; }
        /// 回避成立
        public bool Dodge { get; private set; }
        public DamageType? DamageType { get; private set; }
        /// ダメージ有りの場合はダメージ（回復の場合は負で格納）
        public int? HPDamage { get; private set; }
        /// エネルギーダメージ有りの場合はダメージ（回復の場合は負で格納）
        public int? EnergyDamage { get; private set; }
        // key = 付与属性ID（設定されていない場合は付与しない）, value = 付与属性係数
        public Dictionary<uint, float> AttributeIDToLevelDrivenAttributeParamMap { get; private set; }
        /// 対象の状態ID。キャラクター内部状態遷移をさせたいに設定。
        public TargetStateID? TargetStateID { get; private set; }
        /// ヒットに表示するエフェクト
        public uint? HitCharacterEffectID { get; private set; }
        public bool IsInterruptHitEffect { get; private set; }
        public uint? HitSoundID { get; private set; }
        public List<IAttribute> Attributes { get; private set; }
        // アクション経由で生成された場合はアクションレベルが格納される
        public ushort? ActionLevel { get; private set; }

        public uint[] ForceRemoveAttributeIDs { get; private set; }
        public bool HasForceRemoveAttributeIDs { get { return ForceRemoveAttributeIDs != null && ForceRemoveAttributeIDs.Length > 0; } }
    }
}
