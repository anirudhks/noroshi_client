﻿using Noroshi.BattleScene.Actions.Roles;
using UniRx;
using System;
using UniLinq;
using System.Collections.Generic;

namespace Noroshi.BattleScene.Actions
{
    public class KnightActiveAction : AbstractNoRangeAttack
    {
        // 入れ替えのために最大4体用意
        const byte SHADOW_CHARACTER_NUM = 4;
        // shadowcharacterマスターのID
        const uint SHADOW_CHARACTER_MASTER_ID = 3;
        // 全てのチビナイトを格納するためのキュー
        Queue<IShadow> _allShadowQueue = new Queue<IShadow>();
        // 現在画面に表示しているチビナイトを格納するためのリスト
        List<IShadow> _useShadowList = new List<IShadow>();
        int _executeNum = 0;
        Grid.GridPosition _centerGrid;

        public KnightActiveAction(Core.WebApi.Response.Master.Action data) : base(data)
        {
        }

        protected override TargetSelectType? _targetSelectType { get { return _arg1 > 0 ? (TargetSelectType?)_arg1 : null; } }
        protected override int? _targetTagFlags { get { return _arg2 > 0 ? (int?)_arg2 : null; } }
        protected override float? _damageCoefficient { get { return _arg3 > 0 ? (float?)_arg3 / 100.0f : null; } }
        uint? _shadowCharacterAttributeId { get { return _arg4 > 0 ? (uint?)_arg4 : null; } }
        byte? _maxAvailableActionRank { get { return _arg5 > 0 ? (byte?)_arg5 : null; } }

        protected override ActionEffectType? _actionEffectType { get { return ActionEffectType.ViewOnly; } }

        public override IObservable<IAction> LoadAssets(IActionExecutor executor, IActionFactory factory)
        {
            //ActionViewとShadowCharacterを読み込む
            for(var i = 0; i < SHADOW_CHARACTER_NUM; i++)
            {
                _allShadowQueue.Enqueue(executor.BuildShadow(SHADOW_CHARACTER_MASTER_ID));
            }

            return base.LoadAssets(executor, factory)
            .SelectMany(_ => Observable.WhenAll(_allShadowQueue.Select(s => s.LoadDatasAndAssets(factory))))
            .Select(_ => (IAction)this);
        }

        public override void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            base.PreProcess (executor, targetCandidates);
            _executeNum = 0;

            // すでに表示されているキャラクターは死亡させる
            foreach (var useShadow in _useShadowList)
            {
                useShadow.Escape();
            }
            if (_useShadowList.Count > 0) _useShadowList.Clear();

            _centerGrid = _getCenterGridPosition(targetCandidates);
            _tryActionViewAppearance(_centerGrid, executor.GetDirection(), AnimationName, 1000);
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            _executeNum++;
            if (_executeNum == 1)
            {
                // 移動開始
                executor.GoStraight(_duration);
            }
            else if (_executeNum == 2)
            {
                // 移動終了
            }
            else if (_executeNum == 3)
            {
                // 攻撃
                _attackable.AttackMulti(executor, GetTargets(actionTargetFinder, executor));
            }
            else if (_executeNum == 4)
            {
                // 召喚１回目
                _makeShadow(actionTargetFinder, executor, targets);
            }
            else if (_executeNum == 5)
            {
                // 召喚２回目
                _makeShadow(actionTargetFinder, executor, targets);
            }
        }

        public override void PostProcess(IActionExecutor executor)
        {
            base.PostProcess(executor);
            _tryActionViewDisappearance();
            executor.Appear();
        }

        // チビナイトを召喚する
        void _makeShadow(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            var target = actionTargetFinder.GetExecutorAsTarget(executor);
            var shadowCharacter = _allShadowQueue.Dequeue();
            _useShadowList.Add(shadowCharacter);
            target.MakeShadow(shadowCharacter, Level, Level, null, executor.PromotionLevel, executor.EvolutionLevel, _maxAvailableActionRank, _centerGrid.HorizontalIndex, null, executor.SkinLevel)
            .Subscribe(s => 
            {
                _allShadowQueue.Enqueue(s);
                _useShadowList.Remove(s);
            });
            _giveAttributeToShadowCharacter(actionTargetFinder.GetShadowAsTarget(shadowCharacter));
        }

        // 召喚したチビナイトに状態異常を付与する
        void _giveAttributeToShadowCharacter(IActionTarget shadowCharacter)
        {
            if (!_shadowCharacterAttributeId.HasValue) return;
            var actionEvent = new ActionEvent(null, shadowCharacter);
            actionEvent.SetAttributeIDToLevelDrivenAttributeparamMap(new Dictionary<uint, float>()
            {
                { _shadowCharacterAttributeId.Value, _getShadowLevelDrivenAttributeParam() }
            });
            shadowCharacter.ReceiveActionEvent(actionEvent);
        }

        // チビナイトに対し付与する状態異常の係数
        float _getShadowLevelDrivenAttributeParam()
        {
            return _getLevelDrivenParam2();
        }

        float _duration { get { return _animation.TriggerTimes[1] - _animation.TriggerTimes[0]; } }
    }
}