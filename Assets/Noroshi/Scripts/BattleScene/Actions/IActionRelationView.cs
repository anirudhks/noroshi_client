﻿namespace Noroshi.BattleScene.Actions
{
    public interface IActionRelationView
    {
        void Appear(IActionExecutorView executorView, IActionTargetView targetView, float duration);
        void Disappear();
        void SetVisible(bool isVisible);
        void PauseOn();
        void PauseOff();
    }
}
