﻿using Noroshi.BattleScene.Actions.Roles;
using UniRx;
using UniLinq;

namespace Noroshi.BattleScene.Actions
{
    public class EmptyAction : AbstractNoRangeAction
    {
        public EmptyAction(Core.WebApi.Response.Master.Action data) : base(data)
        {
        }
        
        protected override TargetSelectType? _targetSelectType { get { return _arg1 > 0 ? (TargetSelectType?)_arg1 : null; } }
        protected override int? _targetTagFlags { get { return _arg2 > 0 ? (int?)_arg2 : null; } }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            // 何もしない
        }
    }
}