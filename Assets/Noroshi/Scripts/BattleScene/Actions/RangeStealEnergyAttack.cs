﻿using Noroshi.BattleScene.Actions.Roles;
using UniRx;

namespace Noroshi.BattleScene.Actions
{
    public class RangeStealEnergyAttack : AbstractRangeAttack
    {
        int _executeNum = 0;
        public RangeStealEnergyAttack(Core.WebApi.Response.Master.Action data) : base(data)
        {
        }

        protected override RelativeForce? _targetRelativeForce { get { return _arg1 > 0 ? (RelativeForce?)_arg1 : null; } }
        public override int MinRange { get { return _arg2; } }
        public override int MaxRange { get { return _arg3; } }
        protected override float? _damageCoefficient { get { return _arg4 > 0 ? (float?)_arg4 / 100.0f : null; } }
        protected override ActionEffectType? _actionEffectType { get { return _arg5 > 0 ? (ActionEffectType?)_arg5 : null; } }
        uint? _healEnergyCharacterEffectID { get { return _arg6 > 0 ? (uint?)_arg6 : null; } }

        public override void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            base.PreProcess (executor, targetCandidates);
            _executeNum = 0;
            _tryActionViewAppearance(targetCandidates[0].GetGridPosition().Value, executor.GetDirection(), AnimationName, 1000);
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            _executeNum++;
            if (_executeNum == 1)
            {
                var targetCount = targets.Length;
                var stealEnergys = new int[targetCount];
                for (var i = 0; i < targetCount; i++)
                {
                    stealEnergys[i] = _getStealEnergy();
                }
                // 攻撃と同時に対象のエネルギーにもダメージを与える
                _attackable.AttackMulti(executor, targets, null, stealEnergys);
            }
            else
            {
                // エネルギーを回復する
                var executorAsTarget = actionTargetFinder.GetExecutorAsTarget(executor);
                var actionEvent = new ActionEvent(executor, executorAsTarget, Level);
                actionEvent.SetEnergyDamage(-_getStealEnergy());
                actionEvent.SetHitCharacterEffectIDAndSoundID(_healEnergyCharacterEffectID, null);
                executorAsTarget.ReceiveActionEvent(actionEvent);
            }
        }

        int _getStealEnergy()
        {
            var stealEnergyRatio = _getLevelDrivenParam2() / 100.0f;
            return (int)(CharacterEnergy.MAX_ENERGY * stealEnergyRatio);
        }

        public override void PostProcess(IActionExecutor executor)
        {
            _tryActionViewDisappearance();
        }

        public override void Cancel(IActionExecutor executor)
        {
            _tryActionViewDisappearance();
        }
    }
}