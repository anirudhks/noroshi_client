﻿using UniRx;
using Noroshi.Core.Game.Action;
using Noroshi.Core.Game.Character;

namespace Noroshi.BattleScene.Actions
{
    public interface IActionTarget
    {
        IActionTagSet TagSet { get; }
        void ReceiveActionEvent(ActionEvent actionEvent);

        /// キャラクター ID。
        uint CharacterID { get; }
        /// レベル。
        ushort Level { get; }
        /// 最大 HP。
        uint MaxHP { get; }
        /// 現 HP。
        uint CurrentHP { get; }
        /// 最大エネルギー。
        ushort MaxEnergy { get; }
        /// 現エネルギー.
        ushort CurrentEnergy { get; }
        /// 物理防御力。
        uint Armor { get; }
        /// 魔法防御力。
        uint MagicResistance { get; }
        /// 回避。
        byte Dodge { get; }

        bool IsDead { get; }
        bool IsTargetable { get; }

        Force Force { get; }

        Grid.GridPosition? GetGridPosition();

        IObservable<BulletHitEvent> GetOnHitObservable();

        void AddStatusBoosterFactor   (IStatusBoostFactor factor);
        void RemoveStatusBoosterFactor(IStatusBoostFactor factor);
        void AddStateTransitionBlockerFactor   (StateTransitionBlocker.Factor factor);
        void RemoveStateTransitionBlockerFactor(StateTransitionBlocker.Factor factor);
        void AddStatusBreakerFactor   (StatusForceSetter.Factor factor);
        void RemoveStatusBreakerFactor(StatusForceSetter.Factor factor);
        void AddActionLimiterFactor   (ActionLimiter.Factor factor);
        void RemoveActionLimiterFactor(ActionLimiter.Factor factor);

        /// <summary>
        /// 停止状態への遷移を試みる。
        /// </summary>
        /// <param name="isLoopAnimation">アニメーションをループ再生するかどうか</param>
        bool TryToTransitToStop(bool isLoopAnimation);
        /// <summary>
        /// 停止状態からの遷移を試みる。
        /// </summary>
        bool TryToTransitFromStop();

        void SetCurrentForceReverse();
        void SetCurrentForceOriginal();

        void ChangeSkinTemporarily(string skinName);
        void ResetSkin();
        void ChangeViewTemporarily(ICharacterView view);
        void ResetView(ICharacterView view);

        void PlayTransparency(bool isOn, float duration);

        void SetSpeed(float speed);
        IObservable<ShadowCharacter> MakeShadow(IShadow shadow, ushort characterLevel, ushort actionLevel, ushort? actionLevel4, byte promotionLevel, byte evolutionLevel, byte? maxAvailableActionRank, ushort initialHorizontalIndex, ushort? baseVerticalIndex, byte? skinLevel);

        IActionTargetView GetActionTargetView();
    }
}
