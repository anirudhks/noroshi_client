﻿using System.Collections.Generic;
using UniRx;
using Noroshi.Character;
using Noroshi.Core.Game.Character;

namespace Noroshi.BattleScene.Actions
{
    public enum Trigger
    {
        Normal = 1,
        Active = 2,
        First = 3,
        Dead = 4,
        PhysicalDamage = 5,
        EnemyDead = 6,
        FirstWithAnimation = 7,
        OwnDead = 8
    }
    public interface IAction
    {
        uint ID { get; }
        ushort Level { get; }
        byte Rank { get; }
        Trigger Trigger { get; }
        float? ExecutableProbability { get; }
        byte? ExecutableNum { get; }
        byte? ExecutorTargetable { get; }
        CharacterAnimationEvent CharacterAnimationEvent { get; }
        string AnimationName { get; }
        DamageType? DamageType { get; }
        void SetRank(byte rank);
        void SetLevel(ushort level);
        bool IsAccumulationAction { get; }
        bool IsAccumulating{ get; }
        void SetAnimation(Noroshi.MonoBehaviours.AnimationInformation animation);
        IActionTarget[] GetTargets(IActionTargetFinder actionTargetFinder, IActionExecutor executor);
        void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates);
        void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets);
        void PostProcess(IActionExecutor executor);
        void Cancel(IActionExecutor executor);
        void Reset(IActionExecutor executor);
        IObservable<IAction> LoadAdditionalDatas(Master.CharacterMaster characterMaster);
        IObservable<IAction> LoadAssets(IActionExecutor executor, IActionFactory factory);
        IObservable<CharacterAnimationEvent> GetOnCommandExecutorAnimationObservable();
        IObservable<Noroshi.BattleScene.Camera.CameraEvent> GetOnTryCommandCameraObservable();
        IObservable<bool> GetOnMaxAccumulationObservable();
        IObservable<bool> GetOnExitAccumulationTimeObservable();
        IObservable<KeyValuePair<byte, Dictionary<byte, int>>> GetOnOverrideArgsObservable();
        IObservable<KeyValuePair<byte, Dictionary<byte, uint>>> GetOnOverrideAttributeIdsObservable();
        void OverrideArg(byte argNo, int overrideValue);
        void OverrideAttributeId(byte attributeNo, uint attributeId);
        void EnterTimeStop();
        void ExitTimeStop();
        void SetVisible(bool isVisible);
        ushort GetNecessaryEnergyToEnterActiveAction();
        bool ShouldReduceEnergyWhenEnterActiveAction();
        ActiveActionType? GetActiveActionType();

        /// （複数アニメーションで構成される場合）次のアニメーションを取得。
        CharacterAnimationEvent? GetNextCharacterAnimationEvent();
        /// （プレイヤーの意思で）アクションに影響を与える。
        void Affect();
    }
}
