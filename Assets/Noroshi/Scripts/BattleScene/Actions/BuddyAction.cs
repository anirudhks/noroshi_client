﻿using System.Collections.Generic;
using UniLinq;
using UniRx;

namespace Noroshi.BattleScene.Actions
{
    // 自軍キャラクター編成によってアクションが切り替わるアクション
    public class BuddyAction : AbstractMultiAction
    {
        uint _buddyCharacterID { get { return (uint)_arg1; } }
        uint? _buddyActionID { get { return _arg2 > 0 ? (uint?)_arg2 : null; } }
        uint? _aloneActionID { get { return _arg3 > 0 ? (uint?)_arg3 : null; } }

        protected override uint? _actionId1 { get { return _buddyActionID; } }
        protected override uint? _actionId2 { get { return _aloneActionID; } }
        protected override uint? _actionId3 { get { return null; } }
        protected override uint? _actionId4 { get { return null; } }
        protected override uint? _actionId5 { get { return null; } }
        protected override uint? _actionId6 { get { return null; } }
        protected override uint? _actionId7 { get { return null; } }
        protected override uint? _actionId8 { get { return null; } }

        public BuddyAction(Core.WebApi.Response.Master.Action data) : base(data)
        {
        }

        public override IActionTarget[] GetTargets(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            var action = _getBuddyAction(actionTargetFinder, executor);
            if (action == null) return new IActionTarget[]{};
            
            return action.GetTargets(actionTargetFinder, executor);
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            var action = _getBuddyAction(actionTargetFinder, executor);
            if (action == null) return;

            var actionTargets = action.GetTargets(actionTargetFinder, executor);
            action.Execute(actionTargetFinder, executor, actionTargets);
        }

        /// 編成により切り替わるアクションの取得
        IAction _getBuddyAction(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            // 対応するキャラクターが存在するか確認する
            var isBuddy = false;
            var targets = actionTargetFinder.GetAllTargets();
            foreach (var target in targets)
            {
                if (target.CharacterID == _buddyCharacterID && executor.Force == target.Force)
                {
                    isBuddy = true;
                    break;
                }
            }

            // 対応するアクションを探す
            foreach (var action in _actions)
            {
                if (isBuddy && action.ID == _buddyActionID) return action;
                if (!isBuddy && action.ID == _aloneActionID) return action;
            }

            return null;
        }
    }
}
