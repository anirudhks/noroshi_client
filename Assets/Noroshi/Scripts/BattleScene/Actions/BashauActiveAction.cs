﻿using Noroshi.BattleScene.Actions.Roles;
using System;
using UniLinq;

namespace Noroshi.BattleScene.Actions
{
    public class BashauActiveAction : AbstractNoRangeAttack
    {
        int _executeNum = 0;
        int _originalHorizontalIndex = 0;
        Damageable _damageable = null;
        public BashauActiveAction(Core.WebApi.Response.Master.Action data) : base(data)
        {
            _damageable = new Damageable(
                DamageType, _damageMagicalAttribute,
                null,
                _getAttributeIdToLevelDrivenAttributeParamMap(),
                10002, null, // 回復時のヒット用エフェクト
                Level
            );
        }

        protected override TargetSelectType? _targetSelectType { get { return _arg1 > 0 ? (TargetSelectType?)_arg1 : null; } }
        protected override int? _targetTagFlags { get { return _arg2 > 0 ? (int?)_arg2 : null; } }
        protected override float? _damageCoefficient { get { return _arg3 > 0 ? (float?)_arg3 / 100.0f : null; } }
        // ２回目攻撃時の敵との相対距離
        // 基本的にはバスハウの停止位置を設定するための値
        int _enemyRelativeDistance { get { return _arg4; } }
        protected override ActionEffectType? _actionEffectType { get { return ActionEffectType.ViewOnly; } }

        public override void PreProcess(IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            base.PreProcess(executor, targetCandidates);
            _executeNum = 0;
            // 自身の位置からActionViewを表示させる
            _tryActionViewAppearance(executor.GetGridPosition().Value, executor.GetDirection(), AnimationName, 1000);
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targets)
        {
            _executeNum++;
            if (_executeNum == 1)
            {
                // 一度ターゲットを味方に設定する
                _noRangeSearchable.SelectType = TargetSelectType.OwnForceWithoutSelf;
                _damageable.DamageMulti(executor, GetTargets(actionTargetFinder, executor), _getHeal(), null);
                // 終わったら元に戻す
                _noRangeSearchable.SelectType = _targetSelectType;
            }
            else if (_executeNum == 2)
            {
                // 一度ターゲットを敵の先頭にする
                _noRangeSearchable.SelectType = TargetSelectType.EnmeyPositionFront;
                var k = executor.GetDirection() == Grid.Direction.Left ? -1 : 1;
                _originalHorizontalIndex = executor.GetGridPosition().Value.HorizontalIndex;
                Func<short> getHorizontalIndex = () => {
                    var filterTargets = _noRangeSearchable.Search(actionTargetFinder, executor);
                    var toH = filterTargets.Count() > 0 ? (int)filterTargets.First().GetGridPosition().Value.HorizontalIndex : _originalHorizontalIndex;
                    return (short)((toH - _originalHorizontalIndex) * k - _enemyRelativeDistance);
                };
                if (getHorizontalIndex() != 0) executor.HorizontalMove(getHorizontalIndex, _forwardDuration);
                // 終わったら元に戻す
                _noRangeSearchable.SelectType = _targetSelectType;
            }
            else if (_executeNum == 3)
            {
                // 移動終了のタイミングでActionViewの位置を更新
                _tryActionViewUpdatePositionWithSynchronismOnly(executor.GetGridPosition().Value);
            }
            else if (_executeNum == 4)
            {
                _attackable.AttackMulti(executor, _noRangeSearchable.FilterTargets(actionTargetFinder, targets));
            }
            else if (_executeNum == (_animation.TriggerTimes.Length - 1))
            {
                var h = _originalHorizontalIndex - executor.GetGridPosition().Value.HorizontalIndex;
                if (executor.GetDirection() == Grid.Direction.Left) h *= -1;
                if (h != 0) executor.HorizontalMove((short)h, _backwardDuration, true);
            }
            else if (_executeNum == (_animation.TriggerTimes.Length))
            {
            }
        }

        public override void PostProcess(IActionExecutor executor)
        {
            base.PostProcess(executor);
            _tryActionViewDisappearance();
        }

        // 2を回復用として利用
        int _getHeal()
        {
            return (int)_getLevelDrivenParam2();
        }

        float _forwardDuration { get { return _animation.TriggerTimes[2] - _animation.TriggerTimes[1]; } }
        float _backwardDuration { get { return _animation.TriggerTimes[_animation.TriggerTimes.Length - 1] - _animation.TriggerTimes[_animation.TriggerTimes.Length - 2]; } }
    }
}
