﻿using System.Collections.Generic;
using UniLinq;
using UniRx;

namespace Noroshi.BattleScene.Actions
{
    /// <summary>
    /// 他アクションの設定値を上書きするアクション。
    /// </summary>
    public class OverrideArg : AbstractAction
    {
        Subject<KeyValuePair<byte, Dictionary<byte, int>>> _onOverrideArgsSubject = new Subject<KeyValuePair<byte, Dictionary<byte, int>>>();
        Subject<KeyValuePair<byte, Dictionary<byte, uint>>> _onOverrideAttributeIdsSubject = new Subject<KeyValuePair<byte, Dictionary<byte, uint>>>();

        public OverrideArg(Core.WebApi.Response.Master.Action data) : base(data)
        {
        }

        /// 上書き対象アクションランク。1個目
        byte _actionRank1 { get { return (byte)_arg1; } }
        /// 上書き対象アクション番号（$"Arg{_argNo}" が上書かれる）
        byte? _argNo1 { get { return _arg2 > 0 ? (byte?)_arg2 : null; } }

        /// 上書き対象アクションランク。２個目
        byte _actionRank2 { get { return (byte)_arg3; } }
        /// 上書き対象アクション番号（$"Arg{_argNo}" が上書かれる）
        byte? _argNo2 { get { return (byte?)_arg4 > 0 ? (byte?)_arg4 : null; } }

        /// 上書き対象アクションランク。３個目
        byte _actionRank3 { get { return (byte)_arg5; } }
        /// 上書き対象アクション番号（$"Arg{_argNo}" が上書かれる）
        byte? _argNo3 { get { return (byte?)_arg6 > 0 ? (byte?)_arg6 : null; } }

        // 状態異常の上書き対象ランク
        byte _attributeActionRank { get { return (byte)_arg7; } }
        /// 上書き対象の状態異常ID(_attributeIdが上書かれる)
        uint? _overrideAttributeId { get { return _arg8 > 0 ? (uint?)_arg8 : null; } }
        /// 上書き対象の状態異常番号(1 = _attributeId1 or 2 = _attributeId2)
        byte? _attributeNo { get { return _arg9 > 0 ? (byte?)_arg9 : null; } }

        public override IObservable<KeyValuePair<byte, Dictionary<byte, int>>> GetOnOverrideArgsObservable()
        {
            return _onOverrideArgsSubject.AsObservable();
        }
        public override IObservable<KeyValuePair<byte, Dictionary<byte, uint>>> GetOnOverrideAttributeIdsObservable()
        {
            return _onOverrideAttributeIdsSubject.AsObservable();
        }

        public override IActionTarget[] GetTargets(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            return new[] { actionTargetFinder.GetExecutorAsTarget(executor) };
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            // 各ArgNoに値がセットされていれば上書き処理
            if (_argNo1.HasValue)
            {
                _onOverrideArgsSubject.OnNext(new KeyValuePair<byte, Dictionary<byte, int>>(_actionRank1, new Dictionary<byte, int>()
                {
                    { _argNo1.Value, _getOverrideValue1() }
                }));
            }
            if (_argNo2.HasValue)
            {
                _onOverrideArgsSubject.OnNext(new KeyValuePair<byte, Dictionary<byte, int>>(_actionRank2, new Dictionary<byte, int>()
                {
                    { _argNo2.Value, _getOverrideValue2() }
                }));
            }
            if (_argNo3.HasValue)
            {
                _onOverrideArgsSubject.OnNext(new KeyValuePair<byte, Dictionary<byte, int>>(_actionRank3, new Dictionary<byte, int>()
                {
                    { _argNo3.Value, _getOverrideValue3() }
                }));
            }
            // 状態異常の上書き
            if (_overrideAttributeId.HasValue)
            {
                _onOverrideAttributeIdsSubject.OnNext(new KeyValuePair<byte, Dictionary<byte, uint>>(_attributeActionRank, new Dictionary<byte, uint>
                {
                    { _attributeNo.Value, _overrideAttributeId.Value },
                }));
            }
        }

        /// 上書き値 (ActionRank1用)。
        protected int _getOverrideValue1()
        {
            return (int)_getLevelDrivenParam1();
        }
        /// 上書き値 (ActionRank2用)。
        protected int _getOverrideValue2()
        {
            return (int)_getLevelDrivenParam2();
        }
        /// 上書き値 (ActionRank3用)。
        protected int _getOverrideValue3()
        {
            return (int)_getLevelDrivenParam3();
        }
    }
}
