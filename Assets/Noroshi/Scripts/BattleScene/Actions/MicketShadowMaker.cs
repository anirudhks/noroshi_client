﻿using Noroshi.BattleScene.Actions.Roles;
using UniRx;
using System;
using UniLinq;
using System.Collections.Generic;
using Flaggs.Extensions.Heam;

namespace Noroshi.BattleScene.Actions
{
    public class MicketShadowMaker : AbstractNoRangeAction
    {
        public const uint SHADOW_CHARACTER_ID = 4;
        Queue<IShadow> _queue = new Queue<IShadow>();
        Grid.GridPosition _centerGrid = new Noroshi.Grid.GridPosition();
        int _executeAnimationNameSuffixIndex = 0;

        public MicketShadowMaker(Core.WebApi.Response.Master.Action data) : base(data)
        {
        }
        protected override TargetSelectType? _targetSelectType { get { return _arg1 > 0 ? (TargetSelectType?)_arg1 : null; } }
        protected override int? _targetTagFlags { get { return _arg2 > 0 ? (int?)_arg2 : null; } }
        // 召喚出現時ターゲットからどのくらい手前に配置するか
        short _shadowAppearBackHorizontalDistance { get { return (short)_arg3; } }
        byte? _maxAvailableActionRank { get { return _arg4 > 0 ? (byte?)_arg4 : null; } }

        // trueになる確率(1 ~ 100)を100で割り 0.01 ~ 1.0の範囲内にする
        float _successfulProbability { get { return ((float)_arg5 / 100.0f); } }
        // true時に実行するアニメーションの番号
        int _successAnimationNameSuffixIndex { get { return _arg6; } }
        // false時に実行するアニメーションの番号
        int _failureAnimationNameSuffixIndex { get { return _arg7; } }

        protected override ActionEffectType? _actionEffectType { get { return ActionEffectType.ViewOnly; } }

        public override IObservable<IAction> LoadAssets(IActionExecutor executor, IActionFactory factory)
        {
            // １体のみビルド
            _queue.Enqueue(executor.BuildShadow(SHADOW_CHARACTER_ID));
            return base.LoadAssets (executor, factory)
            .SelectMany(_ => Observable.WhenAll(_queue.Select(s => s.LoadDatasAndAssets(factory))))
            .Select(_ => (IAction)this);
        }

        public override IActionTarget[] GetTargets(IActionTargetFinder actionTargetFinder, IActionExecutor executor)
        {
            // 対象分身キャラクターがすでに画面上に出現している場合はアクションをスキップさせる
            if (_queue.Count <= 0) return new IActionTarget[]{};
            // _animationNameSuffixが呼ばれる前に呼ばないとキャラクターアニメーションとActionViewのアニメーションとの整合性が取れないため、PreProcessより前に呼ぶ必要がある
            _executeAnimationNameSuffixIndex = GlobalContainer.RandomGenerator.Lot(_successfulProbability) ? _successAnimationNameSuffixIndex : _failureAnimationNameSuffixIndex;
            return new []{ actionTargetFinder.GetExecutorAsTarget(executor) };
        }

        public override void Execute(IActionTargetFinder actionTargetFinder, IActionExecutor executor, IActionTarget[] targetCandidates)
        {
            var target = actionTargetFinder.GetExecutorAsTarget(executor);
            var appearTargets = _noRangeSearchable.Search(actionTargetFinder, executor).ToArrayWithOptimization();
            if (appearTargets.Count() > 0)
            {
                _centerGrid = _getCenterGridPosition(appearTargets, _shadowAppearBackHorizontalDistance, executor.GetDirection());
            }
            else
            {
                // TODO
                _centerGrid = executor.GetGridPosition().Value;
            }
            _tryToLaunchActionView(executor.GetViewAsActionExecutorView(), _centerGrid, executor.GetDirection(), AnimationName, 1000, (byte?)_executeAnimationNameSuffixIndex)
            .Subscribe(_ => 
            {
                // 成功したときのみホムンクルスを召喚する
                if (_executeAnimationNameSuffixIndex == _successAnimationNameSuffixIndex)
                {
                    var shadow = _queue.Dequeue();
                    target.MakeShadow(shadow, Level, Level, null, executor.PromotionLevel, executor.EvolutionLevel, _maxAvailableActionRank, _centerGrid.HorizontalIndex, (ushort?)_centerGrid.VerticalIndex, null).Subscribe(s => _queue.Enqueue(s));
                }
            });
        }

        public override void PostProcess(IActionExecutor executor)
        {
            _tryActionViewDisappearance();
        }

        public override void Cancel(IActionExecutor executor)
        {
            _tryActionViewDisappearance();
        }

        protected override string _animationNameSuffix { get { return _executeAnimationNameSuffixIndex.ToString(); } }
    }
}
