﻿using System;
using UniLinq;

namespace Noroshi.BattleScene.Actions
{
    public class ActionSequence
    {
        sbyte[] _actionSequences;
        int _secondLoopStartPosition;
        int _currentNo;
        byte _targetActionNum;
        public ActionSequence(Core.WebApi.Response.Master.CharacterActionSequence data)
        {
            _secondLoopStartPosition = data.SecondLoopStartPosition;
            _actionSequences = new sbyte[7]{
                data.ActionSequence1,
                data.ActionSequence2,
                data.ActionSequence3,
                data.ActionSequence4,
                data.ActionSequence5,
                data.ActionSequence6,
                data.ActionSequence7,
            };
            _targetActionNum = data.TargetActionNum;
            // 初回位置を決める
            _currentNo = 0;
            Switch();
        }
        public byte GetRank()
        {
            return (byte)_getActionSequences(_currentNo);
        }
        public byte GetTargetActionNum()
        {
            return _targetActionNum;
        }

        public void Switch()
        {
            ++_currentNo;
            if (_currentNo > _actionSequences.Length)
            {
                _currentNo = _secondLoopStartPosition;
            }
            if (_getActionSequences(_currentNo) < 0)
            {
                Switch();
            }
        }

        sbyte _getActionSequences(int no)
        {
            return _actionSequences[no - 1];
        }
    }
}
