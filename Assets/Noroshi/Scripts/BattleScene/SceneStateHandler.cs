﻿using System;
using System.Collections.Generic;
using UniRx;
using Flaggs.StateTransition;
using Noroshi.Core.Game.Battle;

namespace Noroshi.BattleScene
{
    /// シーン状態遷移操作クラス。
    public class SceneStateHandler
    {
        /// 状態遷移マップ。
        static readonly Dictionary<Type, Type[]> STATE_MAP = new Dictionary<Type, Type[]>
        {
            {typeof(Initialization), new[]{typeof(Ready)}},
            {typeof(Ready), new[]{typeof(Battle)}},
            {typeof(Battle), new[]{typeof(Result)}},
        };


        /// 状態遷移マネージャー。
        StateTransitionManager<SceneState> _stateTransitionManager = new StateTransitionManager<SceneState>(STATE_MAP);
        /// 勝敗。
        VictoryOrDefeat? _victoryOrDefeat = null;


        /// <summary>
        /// Initializes a new instance of the <see cref="Noroshi.BattleScene.SceneStateHandler"/> class.
        /// </summary>
        public SceneStateHandler()
        {
            _stateTransitionManager.AddState<Initialization>();
            _stateTransitionManager.AddState<Ready>();
            _stateTransitionManager.AddState<Battle>();
            _stateTransitionManager.AddState<Result>();
        }

        /// Initialization 状態へ遷移した際に OnNext される Observable を取得。
        public IObservable<SceneState> GetOnEnterInitializationObservable()
        {
            return _stateTransitionManager.GetOnEnterStateObservable().Where(state => state.GetType() == typeof(Initialization));
        }
        /// Initialization 状態から別の状態へ遷移した際に OnNext される Observable を取得。
        public IObservable<SceneState> GetOnExitInitializeObservable()
        {
            return _stateTransitionManager.GetOnExitStateObservable().Where(state => state.GetType() == typeof(Initialization));
        }
        /// Ready 状態へ遷移した際に OnNext される Observable を取得。
        public IObservable<SceneState> GetOnEnterReadyObservable()
        {
            return _stateTransitionManager.GetOnEnterStateObservable().Where(state => state.GetType() == typeof(Ready));
        }
        /// Battle 状態へ遷移した際に OnNext される Observable を取得。
        public IObservable<SceneState> GetOnEnterBattleObservable()
        {
            return _stateTransitionManager.GetOnEnterStateObservable().Where(state => state.GetType() == typeof(Battle));
        }
        /// Result 状態へ遷移した際に勝敗が OnNext される Observable を取得。
        public IObservable<VictoryOrDefeat> GetOnEnterResultObservable()
        {
            return _stateTransitionManager.GetOnEnterStateObservable().Where(state => state.GetType() == typeof(Result)).Select(_ => _victoryOrDefeat.Value);
        }

        /// 状態遷移を開始する。
        public void Start()
        {
            _stateTransitionManager.Start<Initialization>();
        }
        /// Ready 状態へ遷移する。
        public void TransitToReady()
        {
            if (!_stateTransitionManager.TransitTo<Ready>()) throw new InvalidOperationException();
        }
        /// Battle 状態へ遷移する。
        public void TransitToBattle()
        {
            if (!_stateTransitionManager.TransitTo<Battle>()) throw new InvalidOperationException();
        }
        /// Result 状態へ遷移する。
        public void TransitToResult(VictoryOrDefeat victoryOrDefeat)
        {
            _victoryOrDefeat = victoryOrDefeat;
            if (!_stateTransitionManager.TransitTo<Result>()) throw new InvalidOperationException();
        }


        public class SceneState : IState
        {
        }
        public class Initialization : SceneState
        {
        }
        public class Ready : SceneState
        {
        }
        public class Battle : SceneState
        {
        }
        public class Result : SceneState
        {
        }
    }
}
