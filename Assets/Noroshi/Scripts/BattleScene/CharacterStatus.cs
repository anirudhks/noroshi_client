using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.Game.Character;

namespace Noroshi.BattleScene
{
    /// バトル内で利用するためのキャラクターステータスクラス。パフォーマンスを上げるために計算結果はキャッシュする作り。
    public class CharacterStatus : Game.CharacterStatus
    {
        readonly StatusBooster _statusBooster = new StatusBooster();
        int _strength;
        int _intellect;
        int _agility;
        uint _maxHp;
        uint _physicalAttack;
        uint _magicPower;
        uint _armor;
        uint _magicResistance;
        uint _physicalCrit;
        byte _accuracy;
        byte _dodge;
        uint _lifeStealRating;
        float _actionFrequency;
        float _energyHealBonus;
        byte _improveHealings;
        byte _reduceEnergyCost;
        Subject<CharacterStatusBoostEvent> _onChangeStatusBooster = new Subject<CharacterStatusBoostEvent>();

        readonly StatusForceSetter _statusForceSetter = new StatusForceSetter();

        public CharacterStatus(IPersonalCharacter personalData, Core.WebApi.Response.Master.Character masterData) : base(personalData, masterData)
        {
            _calculate();
        }

        public IObservable<CharacterStatusBoostEvent> GetOnChangeStatusBooster()
        {
            return _onChangeStatusBooster.AsObservable();
        }

        public void OverrideLevels(ushort characterLevel, ushort actionLevel, byte promotionLevel, byte evolutionLevel)
        {
            _overrideLevel(characterLevel);
            _overrideActionLevels(actionLevel);
            _overridePromotionLevel(promotionLevel);
            _overrideEvolutionLevel(evolutionLevel);
            _calculate();
        }

        public void AddStatusBreakerFactor(StatusForceSetter.Factor factor)
        {
            _statusForceSetter.AddFactor(factor);
            _calculate();
        }
        public void RemoveStatusBreakerFactor(StatusForceSetter.Factor factor)
        {
            _statusForceSetter.RemoveFactor(factor);
            _calculate();
        }

        public void AddStatusBoosterFactor(IStatusBoostFactor factor)
        {
            _statusBooster.AddFactor(factor);
            _calculate();
            _onChangeStatusBooster.OnNext(new CharacterStatusBoostEvent()
            {
                Type = CharacterStatusBoostEvent.EventType.Add,
                StatusBooster = _statusBooster,
                StatusBoosterFactor = factor,
            });
        }
        public void AddStatusBoosterFactors(IEnumerable<IStatusBoostFactor> factors)
        {
            _statusBooster.AddFactors(factors);
            _calculate();
            foreach (var factor in factors)
            {
                _onChangeStatusBooster.OnNext(new CharacterStatusBoostEvent()
                {
                    Type = CharacterStatusBoostEvent.EventType.Add,
                    StatusBooster = _statusBooster,
                    StatusBoosterFactor = factor,
                });
            }
        }
        public void RemoveStatusBoosterFactor(IStatusBoostFactor factor){
            _statusBooster.RemoveFactor(factor);
            _calculate();
            _onChangeStatusBooster.OnNext(new CharacterStatusBoostEvent()
            {
                Type = CharacterStatusBoostEvent.EventType.Remove,
                StatusBooster = _statusBooster,
                StatusBoosterFactor = factor,
            });
        }

        uint _calculateArmor()
        {
            return (uint)(_statusForceSetter.Armor.HasValue ? _statusForceSetter.Armor.Value : base.Armor + _statusBooster.Armor);
        }

        uint _caluclateMagicResistance()
        {
            return (uint)(_statusForceSetter.MagicResistance.HasValue ? _statusForceSetter.MagicResistance.Value : base.MagicResistance + _statusBooster.MagicRegistance);
        }

        void _calculate()
        {
            _strength           = (int)(base.Strength        + _statusBooster.Strength);
            _intellect          = (int)(base.Intellect       + _statusBooster.Intellect);
            _agility            = (int)(base.Agility         + _statusBooster.Agility);
            var physicalAttack  = (int)base.PhysicalAttack  + _statusBooster.PhysicalAttack;
            _physicalAttack     = (uint)(physicalAttack > 0 ? physicalAttack : 0);
            var magicPower      = (int)base.MagicPower      + _statusBooster.MagicPower;
            _magicPower         = (uint)(magicPower > 0 ? magicPower : 0);
            _armor              = _calculateArmor();
            _magicResistance    = _caluclateMagicResistance();
            _physicalCrit       = base.PhysicalCrit;
            var accuracy        = (int)base.Accuracy        + _statusBooster.Accuracy;
            _accuracy           = (byte)(accuracy > 0 ? accuracy : 0);
            var dodge           = (int)base.Dodge           + _statusBooster.Dodge;
            _dodge              = (byte)(dodge > 0 ? dodge : 0);
            var lifeStealRating = (int)base.LifeStealRating + _statusBooster.LifeStealRating;
            _lifeStealRating    = (uint)(lifeStealRating > 0 ? lifeStealRating : 0);
            _actionFrequency    = base.ActionFrequency + _statusBooster.ActionFrequency;
            var maxHp           = (int)base.MaxHP           + _statusBooster.MaxHp;
            _maxHp              = (uint)(maxHp > 1 ? maxHp : 1);
            _energyHealBonus    = _statusBooster.EnergyHealBonus;
            _improveHealings    = (byte)(base.ImproveHealings + _statusBooster.ImproveHealings);
            _reduceEnergyCost   = (byte)(base.ReduceEnergyCost + _statusBooster.ReduceEnergyCost);
        }

        public override float Strength  { get { return _strength; } }
        public override float Intellect { get { return _intellect; } }
        public override float Agility   { get { return _agility; } }
        /// 最大 HP。
        public override uint MaxHP { get { return _maxHp; } }
        /// 物理攻撃力。
        public override uint PhysicalAttack { get { return _physicalAttack; } }
        /// 魔法攻撃力。
        public override uint MagicPower { get { return _magicPower; } }
        /// 物理防御力。
        public override uint Armor { get { return _armor; } }
        /// 魔法防御力。
        public override uint MagicResistance { get { return _magicResistance; } }
        /// 物理クリティカル。
        public override uint PhysicalCrit { get { return _physicalCrit; } }
        /// 命中。
        public override byte Accuracy { get { return _accuracy; } }
        /// 回避。
        public override byte Dodge { get { return _dodge; } }
        /// ライフ奪取。
        public override uint LifeStealRating { get { return _lifeStealRating; } }
        public override float ActionFrequency { get { return _actionFrequency; } }
        public float EnergyHealBonus { get { return _energyHealBonus;} }
        public byte ImproveHealings { get { return _improveHealings;} }
        public byte ReduceEnergyCost { get { return _reduceEnergyCost;} }

        public BattleScene.Actions.ActionSequence[] ActionSequence
        {
            get
            {
                var actionSequenceArray = new BattleScene.Actions.ActionSequence[_masterData.ActionSequences.Length];
                var actionSequenceNum = 0;
                foreach (var actionSequece in _masterData.ActionSequences)
                {
                    actionSequenceArray[actionSequenceNum++] = new BattleScene.Actions.ActionSequence(actionSequece);
                }
                return actionSequenceArray;
            }
        }
    }
}
