using UniRx;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.UI
{
    public class DropItemUI : UIViewModel<IDropItemUIView>
    {
        public readonly uint ItemID;
        public readonly byte No;

        public DropItemUI(uint itemId, byte dropItemNo)
        {
            ItemID = itemId;
            No = dropItemNo;
        }

        public IObservable<DropItemUI> LoadView(IUIController uiController)
        {
            return LoadView().Select(_ => this);
        }
        protected override IObservable<IDropItemUIView> _loadView()
        {
            return SceneContainer.GetFactory().BuildDropItemUIView(ItemID).Do(v => v.SetActive(false));
        }

        /// サウンド操作時に操作内容が OnNext される Observable を取得。
        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _uiView.GetOnCommandSoundObservable();
        }

        public void Drop(ICharacterView characterView, bool isMultiDrop)
        {
            _uiView.SetActive(true);
            _uiView.Drop(characterView, No, isMultiDrop);
        }
    }
}
