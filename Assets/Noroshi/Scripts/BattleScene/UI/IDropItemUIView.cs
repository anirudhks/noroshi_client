﻿using UniRx;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.UI
{
    public interface IDropItemUIView : MonoBehaviours.IUIView
    {
        void Drop(ICharacterView characterView, byte dropItemNo, bool isMultiDrop);
        IObservable<SoundEvent> GetOnCommandSoundObservable();
    }
}
