﻿namespace Noroshi.BattleScene.UI
{
    public interface ITrainingScoreUIView : MonoBehaviours.IUIView
    {
        void SetLoopBattleData();
        void SetUnlimitedHPEnemyBattleData();
        void UpdateScore(uint score);
    }
}
