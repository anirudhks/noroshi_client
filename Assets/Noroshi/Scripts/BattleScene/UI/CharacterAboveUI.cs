﻿using UniRx;
using System.Collections.Generic;

namespace Noroshi.BattleScene.UI
{
    public class CharacterAboveUI : UIViewModel<ICharacterAboveUIView>
    {
        CompositeDisposable _disposables = new CompositeDisposable();
        MonoBehaviours.IUIView _textUICanvas = null;
        bool _hideEnemy = false;
        Force _force;

        public void SetTarget(Character character, bool hideEnemy)
        {
            _uiView.SetTarget(character.GetView(), character.Force, character.CurrentHP, character.MaxHP);
            // HP 変更イベント処理紐付け
            character.GetOnHPChangeObservable().Where(e => !e.IgnoreAboveUI).Subscribe(_onHPChange).AddTo(_disposables);
            // エネルギー変更イベント処理紐付け
            character.GetOnEnergyChangeObservable().Where(e => !e.IgnoreAboveUI).Subscribe(_onEnergyChange).AddTo(_disposables);
            // シールド割合変化処理紐付け
            character.GetOnChangeShieldRatioObservable().Subscribe(_onChangeShieldRatio).AddTo(_disposables);
            // ステータス増減処理紐付け
            character.GetOnChangeStatusBooster().Subscribe(_onChangeStatusBooster).AddTo(_disposables);
            // uiView表示状態切り替え処理紐付け
            character.GetOnSetVisibleObservable().Subscribe(_setVisible).AddTo(_disposables);
            _hideEnemy = hideEnemy;
            _force = character.Force;
        }

        void _onHPChange(ChangeableValueEvent hpChangeEvent)
        {
            // HP ゲージを変更しつつ
            if (_isUpdateHPGaugeUIView()) _updateHPGauge(hpChangeEvent.Current, hpChangeEvent.Max);
            // ダメージビュー表示
            _sendHPDifference(hpChangeEvent.Difference);
        }
        void _onEnergyChange(ChangeableValueEvent energyChangeEvent)
        {
            _sendEnergyDifference(energyChangeEvent.Difference);
        }
        void _onChangeShieldRatio(ChangeableValueEvent changeEvent)
        {
            _uiView.ChangeShieldRatio((float)changeEvent.Current / changeEvent.Max);
        }
        void _onChangeStatusBooster(CharacterStatusBoostEvent changeEvent)
        {
            // TODO : 実装
        }

        void _sendHPDifference(int difference)
        {
            SceneContainer.GetCacheManager().GetCharacterTextUIViewCache().Get()
            .SelectMany(v => v.AppearHPDifference(_textUICanvas, _uiView.GetLocalPosition(), difference))
            .Subscribe(_stockCharacterTextUIView);
        }
        void _sendEnergyDifference(int difference)
        {
            SceneContainer.GetCacheManager().GetCharacterTextUIViewCache().Get()
            .SelectMany(v => v.AppearEnergyDifference(_textUICanvas, _uiView.GetLocalPosition(), difference))
            .Subscribe(_stockCharacterTextUIView);
        }

        void _stockCharacterTextUIView(ICharacterTextUIView view)
        {
            SceneContainer.GetCacheManager().GetCharacterTextUIViewCache().Stock(view);
        }

        public IObservable<ICharacterAboveUIView> LoadView(IUIController uiController)
        {
            _textUICanvas = uiController.GetTextUICanvas();
            return LoadView().Do(v => uiController.SetToWorldUICanvas(v));
        }

        protected override IObservable<ICharacterAboveUIView> _loadView()
        {
            return SceneContainer.GetCacheManager().GetCharacterAboveUIViewCache().Get().Do(v => v.SetActive(true));
        }

        void _updateHPGauge(int currentHP, int maxHP)
        {
            _uiView.ChangeHPRatio((float)currentHP / maxHP);
        }

        void _setVisible(bool isVisible)
        {
            _uiView.SetVisible(isVisible);
        }

        // キャラクター上部のHPゲージViewを更新するか
        bool _isUpdateHPGaugeUIView()
        {
            // 現状はHPが無限設定されているかつ敵の時のみ非表示にする
            return !_hideEnemy || _force != Force.Enemy;
        }

        public void Stock()
        {
            SetViewActive(false);
            _uiView.Reset();
            _disposables.Clear();
            SceneContainer.GetCacheManager().GetCharacterAboveUIViewCache().Stock(_uiView);
        }
        public override void Dispose()
        {
            _disposables.Dispose();
            base.Dispose();
        }
    }
}
