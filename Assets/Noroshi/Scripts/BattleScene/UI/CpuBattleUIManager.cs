using UniRx;
using Noroshi.Core.Game.Battle;
using Noroshi.Core.Game.Sound;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.UI
{
    public class CpuBattleUIManager : AbstractUIManager
    {
        StoryUI _storyUI = new StoryUI();
        Subject<bool> _onExitBeforeBattleStorySubject = new Subject<bool>();
        public override void Initialize()
        {
            base.Initialize();
            var battleManager = (CpuBattleManager)SceneContainer.GetBattleManager();

            SceneContainer.GetSceneManager().GetOnEnterReadyObservable()
            .Select(_ => battleManager.GetBeforeBattleStory())
            .Where(story => story != null)
            .SelectMany(story =>
            {
                var additionalInformation = battleManager.AdditionalInformation;
                if (!string.IsNullOrEmpty(additionalInformation.BattleTitleTextKey))
                {
                    _onCommandSoundSubject.OnNext(new SoundEvent()
                    {
                        Command = SoundCommand.Play,
                        SoundID = SoundConstant.BATTLE_CHAPTER_UI_SOUND_ID,
                    });
                    return _uiController.ActivateChapterUIView(additionalInformation.BattleTitleTextKey,
                                                               additionalInformation.BattleTitleTextParams,
                                                               additionalInformation.BattleSubtitleTextKey,
                                                               additionalInformation.BattleSubtitleTextParams)
                    .Select(_ => story);
                }
                return Observable.Return<Story>(story);
            })
            .SelectMany(story => _storyUI.Start(story, false))
            .Subscribe(_ => _exitBeforeBattleStory()).AddTo(_disposables);

            _storyUI.GetOnMaskWorldObservable().SelectMany(isOn => {
                if (isOn)
                {
                    return Observable.WhenAll(
                        _uiController.DeactivateHeaderAndFooter(),
                        _uiController.DarkenWorld()
                    );
                }
                else
                {
                    return Observable.WhenAll(
                        _uiController.ActivateHeaderAndFooter(),
                        _uiController.LightenWorld()
                    );
                }
            })
            .Subscribe(_ => {}).AddTo(_disposables);

            _storyUI.GetOnRestoreBgmObservable().Subscribe(isAfterBattleStory => {
                if (isAfterBattleStory)
                {
                    // 結果画面のBGMに変更
                    battleManager.PlayResultBgm(true);
                }
                else
                {
                    // 戦闘画面のBGMに戻す
                    battleManager.PlayBattleBgm();
                }
            }).AddTo(_disposables);

            _storyUI.GetOnTryCameraShakeObservable().Subscribe(cameraEvent => battleManager.TryCommandCamera(cameraEvent));

            battleManager.GetOnEnterBeforeBossWaveStory()
            .SelectMany(story => _storyUI.Start(story, false))
            .Subscribe(_ => battleManager.ExitWaitBeforeBossWaitStory()).AddTo(_disposables);

            battleManager.GetOnEnterAfterBossDieStory()
            .SelectMany(story => _storyUI.Start(story, false))
            .Subscribe(_ => battleManager.ExitWaitAfterBossDieStory()).AddTo(_disposables);

            battleManager.GetOnEnterAfterBattleStory()
            .SelectMany(story => _storyUI.Start(story, true))
            .Subscribe(_ => battleManager.ExitWaitAfterBattleStory()).AddTo(_disposables);
        }

        /// 結果モーダルを開く前の UI をアクティブ化する。
        protected override IObservable<VictoryOrDefeat> _activateBeforeResultModalUIOpen(VictoryOrDefeat victoryOrDefeat)
        {
            var battleManager = (CpuBattleManager)SceneContainer.GetBattleManager();
            // バトル後ストーリーが存在、かつバトルに勝利した場合は、該当ストーリーを差し込む。
            var preprocess = battleManager.GetAfterBattleStory() != null && victoryOrDefeat == VictoryOrDefeat.Win
                ? battleManager.EnterAfterBattleStory()
                : Observable.Return(true);
            return preprocess.SelectMany(_ => base._activateBeforeResultModalUIOpen(victoryOrDefeat)).Select(_ => victoryOrDefeat);
        }

        public override IObservable<IManager> LoadAssets(IFactory factory)
        {
            return base.LoadAssets(factory)
            .SelectMany(_ => _storyUI.LoadView())
            .Select(_ => (IManager)this);
        }

        public override void Prepare()
        {
            base.Prepare();
            var battleManager = (CpuBattleManager)SceneContainer.GetBattleManager();

            // 修行モードの場合開始演出を再生させる
            SceneContainer.GetSceneManager().GetOnEnterReadyObservable()
            .Where(_ => battleManager.BattleCategory == BattleCategory.Training)
            .SelectMany(_ => 
            {
                // 画面上部に表示するUIの設定
                _uiController.InitializeTrainingScoreUIView(battleManager.LoopBattle);
                if (battleManager.LoopBattle)
                {   // 修行 : 敵が無限 HP となるバトル時のスコア表示
                    battleManager.GetOnDefeatingNumEnemyCharacterObservable().Subscribe(defeatingnNum => _uiController.UpdateTrainingScore((uint)defeatingnNum));
                }
                else
                {   // 修行 : ループバトル時のスコア表示
                    battleManager.GetOnOwnCharacterAddDamageObservable().Subscribe(_uiController.UpdateTrainingScore);
                }
                return Observable.WhenAll(
                    _uiController.DeactivateHeaderAndFooter(),
                    _uiController.ActivateBattleStartUIView(false)
                );
            })
            .SelectMany(_ => _uiController.PlayAnimationTrainingStart())
            .SelectMany(_ => 
            {
                return Observable.WhenAll(
                    _uiController.ActivateHeaderAndFooter(),
                    _uiController.DeactivateBattleStartUIView(false)
                );
            })
            .Subscribe(_ => SceneContainer.GetSceneManager().ExitReady())
            .AddTo(_disposables);
        }

        public IObservable<bool> GetOnExitBeforeBattleStoryObservable()
        {
            return _onExitBeforeBattleStorySubject.AsObservable();
        }
        void _exitBeforeBattleStory()
        {
            _onExitBeforeBattleStorySubject.OnNext(true);
            _onExitBeforeBattleStorySubject.OnCompleted();
        }

        public override IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return base.GetOnCommandSoundObservable().Merge(_storyUI.GetOnCommandSoundObservable());
        }
    }
}
