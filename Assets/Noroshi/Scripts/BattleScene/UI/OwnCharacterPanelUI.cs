using UniRx;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.UI
{
    /// 各自キャラクター用パネルクラス。
    public class OwnCharacterPanelUI : UIViewModel<IOwnCharacterPanelUIView>
    {
        byte _characrerNo;
        uint _characrerId;
        uint? _activeActionId;
        uint _initialHp;
        uint _maxHp;
        int _initialEnergy;
        int _maxEnergy;
        int _skinLevel;
        ActiveActionType? _activeActionType;
        Subject<byte> _onClickSubject = new Subject<byte>();
        Subject<byte> _onAccumulationClickSubject = new Subject<byte>();
        Subject<SoundEvent> _onCommandSoundSubject = new Subject<SoundEvent>();
        CompositeDisposable _disposables = new CompositeDisposable();

        public OwnCharacterPanelUI(byte characterNo, uint characterId, uint? activeActionID,
                                   uint initialHp, uint maxHp, int initialEnergy, int maxEnergy, int skinLevel, ActiveActionType? activeActionType)
        {
            _characrerNo = characterNo;
            _characrerId = characterId;
            _activeActionId = activeActionID;
            _initialHp = initialHp;
            _maxHp = maxHp;
            _initialEnergy = initialEnergy;
            _maxEnergy = maxEnergy;
            _skinLevel = skinLevel;
            _activeActionType = activeActionType;
        }

        /// クリックしたらキャラクター番号がプッシュされる Observable を取得
        public IObservable<byte> GetOnClickObservable()
        {
            return _onClickSubject.AsObservable();
        }
        public IObservable<byte> GetOnAccumulationClickObservable()
        {
            return _onAccumulationClickSubject.AsObservable();
        }

        /// サウンド操作時に操作内容が OnNext される Observable を取得。
        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _onCommandSoundSubject.AsObservable();
        }

        public void FinishActiveAction()
        {
            _uiView.FinishActiveAction();
        }

        public void EnterActiveAction()
        {
            _uiView.EnterActiveAction();
        }

        public void ReserveActiveAction()
        {
            _uiView.ReserveActiveAction();
        }

        public void ResetEffectAnimation()
        {
            _uiView.ResetEffectAnimation();
        }

        /// HP 関連表現を変更する。
        public void ChangeHP(ChangeableValueEvent hpEvent)
        {
            _uiView.ChangeHP(hpEvent);
        }

        /// エネルギー関連表現を変更する。
        public void ChangeEnergy(ChangeableValueEvent energyEvent)
        {
            _uiView.ChangeEnergy(energyEvent);
        }

        /// アクティブアクション実行可否表現を切り替える。
        public void ToggleActiveActionAvailable(bool available)
        {
            _uiView.ToggleActiveActionAvailable(available);
        }

        public void ChangeStatusBoost(CharacterStatusBoostEvent boostEvent)
        {
            _uiView.ChangeStatusBoost(boostEvent);
        }

        public void ExecuteAccumulationAction()
        {
            _uiView.ExecuteAccumulationAction();
        }

        public void MaxAccumulation()
        {
            _uiView.MaxAccumulation();
        }

        protected override IObservable<IOwnCharacterPanelUIView> _loadView()
        {
            // ロード直後にクリック処理を紐付ける
            return SceneContainer.GetFactory().BuildOwnCharacterPanelUIView(_characrerId, _skinLevel).Do(_onLoadView);
        }
        void _onLoadView(IOwnCharacterPanelUIView view)
        {
            view.Initialize(_activeActionId, _initialHp, _maxHp, _initialEnergy, _maxEnergy, _activeActionType);
            view.GetOnClickObservable().Subscribe(_ => _onClickSubject.OnNext(_characrerNo)).AddTo(_disposables);
            view.GetOnCommandSoundObservable().Subscribe(_onCommandSoundSubject.OnNext).AddTo(_disposables);
            view.GetOnAccumulationClickObservable().Subscribe(_ => _onAccumulationClickSubject.OnNext(_characrerNo)).AddTo(_disposables);
        }

        public override void Dispose()
        {
            _disposables.Dispose();
            base.Dispose();
        }
    }
}
