using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.Game.Battle;
using Noroshi.Core.Game.Sound;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.UI
{
    public abstract class AbstractUIManager : IUIManager
    {
        protected IUIController _uiController;
        PauseModalUI _pauseModalUI;
        ResultUI _resultUI;
        Dictionary<byte, DropItemUI> _dropItemUIs = new Dictionary<byte, DropItemUI>();
        OwnCharacterPanelUISet _ownCharacterPanelUISet;
        CharacterAboveUISet _characterAboveUISet = null;

        /// シーン遷移時に遷移先シーン名が OnNext される Subject。
        Subject<string> _onTransitSceneSubject = new Subject<string>();
        /// ポーズ時に true が、ポーズ解除時に false が OnNext される Subject。
        Subject<bool> _onTogglePauseSubject = new Subject<bool>();
        /// サウンド操作時に操作内容が OnNext される Subject。
        protected Subject<SoundEvent> _onCommandSoundSubject = new Subject<SoundEvent>();

        protected CompositeDisposable _disposables = new CompositeDisposable();

        public AbstractUIManager()
        {
            _pauseModalUI = new PauseModalUI();
        }

        public virtual void Initialize()
        {
            var battleManager = SceneContainer.GetBattleManager();
            var characterManager = SceneContainer.GetCharacterManager();

            // Now Loading 解除
            SceneContainer.GetSceneManager().GetOnEnterReadyObservable()
            .Subscribe(_ => _uiController.DeactiveLoadingUIView()).AddTo(_disposables);

            battleManager.GetOnEnterWaveBattleObservable().SelectMany(waveNo =>
            {
                _setCurrentWaveNum(waveNo);
                _characterAboveUISet.Clear();
                _ownCharacterPanelUISet.ResetEffectAnimation();
                return _characterAboveUISet.LoadCharacterAboveUIViews(_uiController, characterManager.GetCurrentAllCharacters().Where(c => !c.IsDead).ToArray());
            })
            .Subscribe(_ => {}).AddTo(_disposables);
            // 残り時間表示更新
            battleManager.GetOnCountDownObservable().Subscribe(_updateTime)
            .AddTo(_disposables);
            // 次のウェーブへボタンの可視切り替え
            battleManager.GetOnToggleNextWaveButtonVisibleObservable().Subscribe(_setNextWaveButtonVisible)
            .AddTo(_disposables);
            // 勝利時に再生処理を紐付け
            battleManager.GetOnFinishBattleObservable()
            .Subscribe(_ => _playWinMessage()).AddTo(_disposables);

            // 結果シーケンスを作成する。
            _setUpResultSequence();
        }
        /// 残り時間表記を更新。
        void _updateTime(byte time)
        {
            var battleManager = SceneContainer.GetBattleManager();
            _uiController.UpdateTime(time, battleManager.BattleCategory == BattleCategory.Training);
        }
        // 結果シーケンスを作成する。
        void _setUpResultSequence()
        {
            var battleManager = SceneContainer.GetBattleManager();

            SceneContainer.GetSceneManager().GetOnEnterResultObservable()
            // ヘッダーとフッターを非アクティブ化する。ただし、この処理は待たない。
            .Do(_ => _uiController.DeactivateHeaderAndFooter().Subscribe(__ => {}).AddTo(_disposables))
            // 結果モーダルを開く前の UI をアクティブ化する。
            .SelectMany(victoryOrDefeat => _activateBeforeResultModalUIOpen(victoryOrDefeat))
            // 結果モーダルを開く。
            .SelectMany(victoryOrDefeat => _openResultModalUI(battleManager.BattleCategory, battleManager.BattleResult, battleManager.LoopBattle, battleManager.UnlimitedHPEnemyBattle))
            // プレイヤーレベルアップモーダルを開こうと試みる。
            .SelectMany(_ => _tryToOpenPlayerLevelUpModalUI(battleManager.BattleResult.GetAddPlayerExpResult()))
            // レイドボス出現モーダルを開こうと試みる。
            .SelectMany(_ => _tryToOpenRaidBossModalUI(battleManager.BattleResult.GetRaidBoss()))
            .Subscribe(_ => {}).AddTo(_disposables);
        }
        /// 結果モーダルを開く前の UI をアクティブ化する。
        protected virtual IObservable<VictoryOrDefeat> _activateBeforeResultModalUIOpen(VictoryOrDefeat victoryOrDefeat)
        {
            var battleManager = (AbstractBattleManager)SceneContainer.GetBattleManager();
            if (battleManager.ShouldPlayResultBgmAtDefaultRoutine(victoryOrDefeat))
            {
                // 結果画面のBGMを再生する
                battleManager.PlayResultBgm(victoryOrDefeat == VictoryOrDefeat.Win);
            }

            var pickUpCharacter = battleManager.BattleResult.GetPickUpCharacter();
            var characterView = pickUpCharacter != null ? pickUpCharacter.GetView() : null;
            return _uiController.ActivatePickUPCharacterResultUI(characterView, victoryOrDefeat == VictoryOrDefeat.Win).Select(_ => victoryOrDefeat);
        }
        /// 結果モーダルを開く。
        /// 開ききったタイミングで結果モーダルが OnNext される Observable が返る。
        IObservable<ResultUI> _openResultModalUI(BattleCategory battleCategory, BattleResult battleResult, bool loopBattle, bool unlimitedHPEnemyBattle)
        {
            // 固定バトルの場合、結果モーダルはスキップしてシーン遷移してしまう。
            if (battleCategory == BattleCategory.Fixed)
            {
                _onTransitSceneSubject.OnNext(string.Empty);
                return Observable.Return<ResultUI>(null);
            }
            // 結果モーダル選択。
            if (battleCategory == BattleCategory.Training)
            {
                _resultUI = new ResultUI("TrainingResultUI");
            }
            else if (battleResult.GetVictoryOrDefeat() == VictoryOrDefeat.Win)
            {
                _resultUI = new ResultUI("WinUI");
            }
            else
            {
                _resultUI = new ResultUI("LossUI");
                // 敗北モーダルを表示する場合は暗くする。
                _uiController.DarkenWorld().Subscribe(_ => {}).AddTo(_disposables);
            }
            // 結果モーダルを閉じた際の処理紐付け。
            _resultUI.GetOnCloseExitObservable().Subscribe(_ => 
            {
                // 決定音再生
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    Command = SoundCommand.Play,
                    SoundID = Noroshi.UI.SoundController.SEKeys.DECIDE,
                });
                _onTransitSceneSubject.OnNext(string.Empty);
            }).AddTo(_disposables);
            
            return _loadResultUI()
                .Do(resultModal =>
                {
                    // 修行特有情報。
                    if (battleCategory == BattleCategory.Training)
                    {
                        if (loopBattle)
                        {
                            resultModal.SetDefeatingNum(battleResult.GetDefeatingNum());
                        }
                        else if (unlimitedHPEnemyBattle)
                        {
                            resultModal.SetTotalDamage(battleResult.GetTotalDamage());
                        }
                    }
                })
                .SelectMany(resultModal => resultModal.Open().Select(_ => resultModal));
        }
        /// プレイヤーレベルアップモーダルを開こうと試みる。
        /// 開いた場合は閉じるタイミングでが true が、開かなかった場合は即座に false が OnNext される Observable が返る。
        IObservable<bool> _tryToOpenPlayerLevelUpModalUI(Core.WebApi.Response.Players.AddPlayerExpResult addPlayerExpResult)
        {
            if (addPlayerExpResult == null || !addPlayerExpResult.LevelUp) return Observable.Return(false);
            return _uiController.OpenPlayerLevelUPModal(addPlayerExpResult);
        }
        /// レイドボス出現モーダルを開こうと試みる。
        /// 開いた場合は閉じるタイミングでが true が、開かなかった場合は即座に false が OnNext される Observable が返る。
        IObservable<bool> _tryToOpenRaidBossModalUI(Core.WebApi.Response.RaidBoss.RaidBossAtDiscovery raidBoss)
        {
            if (raidBoss == null) return Observable.Return(false);
            return _uiController.OpenRaidBossModal(raidBoss);
        }

        public IObservable<IManager> LoadDatas()
        {
            // キャラクターパネル
            var ownCharacters = SceneContainer.GetCharacterManager().GetCurrentOwnCharacters();
            _ownCharacterPanelUISet = new OwnCharacterPanelUISet(ownCharacters);
            _characterAboveUISet = new CharacterAboveUISet(SceneContainer.GetBattleManager().UnlimitedHPEnemyBattle);
            return Observable.Return<IManager>((IManager)this);
        }

        public virtual IObservable<IManager> LoadAssets(IFactory factory)
        {
            return SceneContainer.GetFactory().BuildUIController()
            .SelectMany(uiController =>
            {
                _uiController = uiController;
                return Observable.WhenAll(
                    _ownCharacterPanelUISet.LoadAssets(uiController).Select(_ => this),
                    _characterAboveUISet.LoadAssets(factory).Select(_ => this),
                    _loadDropItems(uiController).Select(_ => this),
                    _loadModalUI(GetPauseModalUI()).Do(modal => modal.SetViewActive(false)).Select(_ => this)
                );
            })
            .Select(_ => (IManager)this);
        }
        public virtual void Prepare()
        {
            var battleManager = SceneContainer.GetBattleManager();
            var characterManager = SceneContainer.GetCharacterManager();

            // UI 表示初期化
            _setWaveNum(battleManager.WaveNum);
            _setNextWaveButtonVisible(false);

            // アクティブアクション発動予約処理紐付け
            _ownCharacterPanelUISet.GetOnClickObservable()
            .Subscribe(no => characterManager.CurrentOwnCharacterSet.GetCharacterByNo(no).ReserveTryingActiveAction()).AddTo(_disposables);
            // チャージ系アクティブアクションのチャージを終了し攻撃を開始する処理紐付け
            _ownCharacterPanelUISet.GetOnAccumulationClickObservable()
            .Subscribe(no => characterManager.CurrentOwnCharacterSet.GetCharacterByNo(no).AffectCurrentAction()).AddTo(_disposables);
            
            // サウンド操作処理紐付け
            _ownCharacterPanelUISet.GetOnCommandSoundObservable()
            .Subscribe(_onCommandSoundSubject.OnNext).AddTo(_disposables);

            // 味方キャラクターだけアクティブスキル発動時のカットインを再生させる
            battleManager.GetOnEnterActiveActionObservable().Where(c => c.Force == Force.Own)
            .Subscribe(character => _uiController.SetActiveActionDescription(character.GetActiveActionID())).AddTo(_disposables);

            // ドロップ時の更新
            battleManager.DropHandler.GetOnCommandDropItemObservable()
            .Subscribe(die => _onCommandDropItem(die)).AddTo(_disposables);
            battleManager.DropHandler.GetOnCommandDropMoneyObservable()
            .Subscribe(dme => _onCommandDropMoney(dme)).AddTo(_disposables);
            // ポーズボタン処理紐付け
            _uiController.GetOnClickPauseButtonObservable()
            .SelectMany(_ => _pauseModalUI.Open())
            .Subscribe(_ => _onTogglePauseSubject.OnNext(true)).AddTo(_disposables);
            // ポーズモーダルのクローズ処理紐付け
            _pauseModalUI.GetOnCloseEnterObservable()
            .Subscribe(_ => _onTogglePauseSubject.OnNext(false)).AddTo(_disposables);
            // ポーズモーダル撤退ボタンを押した時の処理を紐付け
            _pauseModalUI.GetOnClickWithdrawalObservable()
            .Subscribe(_ =>
            {
                _onTogglePauseSubject.OnNext(false);
                _onTransitSceneSubject.OnNext(string.Empty);
            }).AddTo(_disposables);
            // オートモード切り替えボタン処理紐付け
            if (battleManager.BattleAutoMode == BattleAutoMode.Selectable)
            {
                _uiController.GetOnToggleAutoModeObservable()
                .Subscribe(_onToggleAutoMode).AddTo(_disposables);
            }
            // Wave切り替えボタンを押した時の動作を紐付け
            _uiController.GetOnClickNextWaveButtonObservable()
            .Subscribe(_ => battleManager.TryToTransitToNextWave()).AddTo(_disposables);
            // 分身キャラクター上部 UI 処理紐付け
            characterManager.GetOnCompleteAddingShadowObservable()
            .SelectMany(c => _characterAboveUISet.LoadCharacterAboveUIView(_uiController, c))
            .Subscribe(_ => {}).AddTo(_disposables);
            characterManager.GetOnRemoveShadowObservable()
            .Subscribe(c => _characterAboveUISet.RemoveCharacterAboveUIView(c)).AddTo(_disposables);

            // レイドボス
            if (battleManager.AdditionalInformation != null && battleManager.AdditionalInformation.WaveGaugeType.HasValue)
            {
                var additionalInfo = battleManager.AdditionalInformation;
                _uiController.InitializeWaveGaugeView(additionalInfo.WaveGaugeLevel, additionalInfo.WaveGaugeTextKey,
                                                          characterManager.GetEnemyCharacterSetTotalCurrentHP(),
                                                          characterManager.GetEnemyCharacterSetTotalMaxHP(),
                                                          additionalInfo.WaveGaugeType.Value);
                characterManager.GetOnEnemyCharacterSetHPChangeObservable()
                .Subscribe(cve => _uiController.ChangeWaveGauge((float)cve.Current / cve.Max)).AddTo(_disposables);
            }

            // uiControllerにサウンドイベント紐付け。
            _uiController.GetOnCommandSoundObservable().Subscribe(_onCommandSoundSubject.OnNext).AddTo(_disposables);
        }
        void _onToggleAutoMode(bool isOn)
        {
            SceneContainer.GetBattleManager().SetAutoMode(isOn);
            SceneContainer.GetCharacterManager().CurrentOwnCharacterSet.SetAuto(isOn);
        }

        IObservable<DropItemUI[]> _loadDropItems(IUIController uiController)
        {
            var dropHandler = SceneContainer.GetBattleManager().DropHandler;
            return Observable.WhenAll(dropHandler.GetDropItems().Select(i =>
            {
                var dropItemUI = new DropItemUI(i.ItemID, i.No);
                _dropItemUIs.Add(dropItemUI.No, dropItemUI);
                return dropItemUI.LoadView(uiController).Do(v =>
                {
                    v.GetOnCommandSoundObservable().Subscribe(_onCommandSoundSubject.OnNext).AddTo(_disposables);
                });
            }));
        }

        void _onCommandDropItem(Drop.DropItemEvent dropItemEvent)
        {
            _dropItemUIs[dropItemEvent.No].Drop(dropItemEvent.CharacterView, dropItemEvent.IsMultiDrop);
            _uiController.SetCurrentItemNum(dropItemEvent.CurrentTotalNumFunc());
        }
        void _onCommandDropMoney(Drop.DropMoneyEvent dropMoneyEvent)
        {
            // お金獲得の効果音
            _onCommandSoundSubject.OnNext(new SoundEvent()
            {
                SoundID = SoundConstant.BATTLE_DROP_MONEY_SOUND_ID,
                Command = SoundCommand.Play,
            });
            _uiController.SetCurrentMoneyNum(dropMoneyEvent.CurrentTotalMoney);
        }

        void _setCurrentWaveNum(int num)
        {
            _uiController.SetCurrentWaveNum(num);
        }
        void _setWaveNum(int num)
        {
            _uiController.SetMaxWaveNum(num);
        }

        void _setNextWaveButtonVisible(bool visible)
        {
            if (visible)
            {
                // 矢印表示の効果音
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    SoundID = SoundConstant.BATTLE_NEXT_WAVE_BUTTON_APPEAR_SOUND_ID,
                    Command = SoundCommand.Play,
                });
            }
            _uiController.SetNextWaveButtonVisible(visible);
        }

        /// シーン遷移時に OnNext される Observable を取得。
        public IObservable<string> GetOnTransitSceneObservable()
        {
            return _onTransitSceneSubject.AsObservable().SelectMany(nextSceneName => _uiController.ActivateSceneTransitionUI().Select(_ => nextSceneName));
        }
        /// ポーズ時に true が、ポーズ解除時に false が OnNext される Observable を取得。
        public IObservable<bool> GetOnTogglePauseObservable()
        {
            return _onTogglePauseSubject.AsObservable();
        }
        /// サウンド操作時に操作内容が OnNext される Observable を取得。
        public virtual IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _onCommandSoundSubject.AsObservable();
        }

        protected virtual void _playWinMessage()
        {
            var battleManager = SceneContainer.GetBattleManager();
            var victoryOrDefeat = battleManager.BattleResult.GetVictoryOrDefeat();
            switch (victoryOrDefeat)
            {
            case VictoryOrDefeat.Win:
                // 修行モード時はFinishアニメーションは再生させない
                if (battleManager.BattleCategory != BattleCategory.Training)
                {
                    _uiController.PlayAnimationDefaultVictory().Subscribe(_ => {}).AddTo(_disposables);
                }
                break;
            case VictoryOrDefeat.Loss:
                break;
            case VictoryOrDefeat.TimeUp:
                _uiController.PlayAnimationTimeUp().Subscribe(_ => {}).AddTo(_disposables);;
                break;
            }

            if (victoryOrDefeat != VictoryOrDefeat.TimeUp) {
                // フィニッシュの効果音
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    SoundID = SoundConstant.BATTLE_FINISH_SOUND_ID,
                    Command = SoundCommand.Play,
                });
            }
        }

        public PauseModalUI GetPauseModalUI() { return _pauseModalUI; }
        IObservable<PauseModalUI> _loadModalUI(PauseModalUI pauseModalUI)
        {
            return pauseModalUI.LoadView().Do(v =>
            {
                _uiController.AddModalUIView(v);
            })
            .Select(_ => pauseModalUI);
        }

        IObservable<ResultUI> _loadResultUI()
        {
            return _resultUI.LoadView(SceneContainer.GetBattleManager().BattleResult).Do(v =>
            {
                // サウンド発動処理紐付け
                v.GetOnCommandSoundObservable().Subscribe(_onCommandSoundSubject.OnNext).AddTo(_disposables);
                // 敗北時のシーン遷移処理紐付け
                v.GetOnTransitToSceneObservable().Subscribe(_onTransitSceneSubject.OnNext).AddTo(_disposables);
                
                _uiController.AddResultUIView(v);
            })
            .Select(_ => _resultUI);
        }

        public void Dispose()
        {
            _disposables.Dispose();
            _ownCharacterPanelUISet.Dispose();
        }
    }
}
