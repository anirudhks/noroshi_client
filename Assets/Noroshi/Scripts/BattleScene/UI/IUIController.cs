﻿using UniRx;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.UI
{
    public interface IUIController
    {
        void DeactiveLoadingUIView();

        IObservable<bool> ActivateHeaderAndFooter();
        IObservable<bool> DeactivateHeaderAndFooter();

        void AddModalUIView(IModalUIView uiView);
        void AddResultUIView(IResultUIView uiView);
        void AddPlayerCharacterPanelUI(IOwnCharacterPanelUIView uiView);

        void SetCurrentMoneyNum(uint num);
        void SetCurrentItemNum(byte num);
        void SetCurrentWaveNum(int num);
        void SetMaxWaveNum(int num);

        void SetNextWaveButtonVisible(bool visible);
        IObservable<bool> GetOnClickNextWaveButtonObservable();

        IObservable<bool> GetOnClickPauseButtonObservable();

        IObservable<bool> GetOnToggleAutoModeObservable();

        void UpdateTime(int time, bool isTrainingBattle);

        void SetToWorldUICanvas(MonoBehaviours.IUIView uiView);
        MonoBehaviours.IUIView GetTextUICanvas();

        IObservable<bool> GetOnEnterBattleEffectFrameOutObservable();
        IObservable<SoundEvent> GetOnCommandSoundObservable();
        IObservable<bool> PlayAnimationDefaultVictory();
        IObservable<bool> PlayAnimationPlayerBattleStart();
        IObservable<bool> PlayAnimationPlayerBattleVictory();
        IObservable<bool> PlayAnimationTimeUp();
        IObservable<bool> PlayAnimationTrainingStart();

        IObservable<IUIController> ActivateChapterUIView(string titleTextKey, int[] titleParams, string subTitleTextKey, int[] subTitleParams);

        IObservable<bool> ActivateBattleStartUIView(bool isPlayerBattle);
        IObservable<bool> DeactivateBattleStartUIView(bool isPlayerBattle);
        void SetPlayerNames(string ownPlayerName, string enemyPlayerName);

        IObservable<bool> DarkenWorld();
        IObservable<bool> LightenWorld();

        void InitializeWaveGaugeView(byte? level, string textKey, uint nowHP, uint maxHP, Noroshi.Core.Game.Battle.WaveGaugeType waveGaugeType);
        void ChangeWaveGauge(float ratio);

        void InitializeTrainingScoreUIView(bool isLoopBattle);
        void UpdateTrainingScore(uint score);

        void SetActiveActionDescription(uint? activeSkillId);

        /// <summary>
        /// ピックアップキャラクター UI をアクティブ化。
        /// </summary>
        /// <param name="characterView">ピックアップキャラクターのビュー</param>
        /// <param name="isOwnCharacter">ピックアップキャラクターが自キャラクターかどうか</param>
        IObservable<bool> ActivatePickUPCharacterResultUI(ICharacterView characterView, bool isOwnCharacter);

        /// プレイヤーレベルアップモーダルを開く。
        IObservable<bool> OpenPlayerLevelUPModal(Core.WebApi.Response.Players.AddPlayerExpResult addPlayerExpResult);
        /// レイドボス出現モーダルを開く。
        IObservable<bool> OpenRaidBossModal(Core.WebApi.Response.RaidBoss.RaidBossAtDiscovery raidBoss);
        /// <summary>
        /// シーン遷移時の UI をアクティブ化する。
        /// </summary>
        IObservable<bool> ActivateSceneTransitionUI();
    }
}
