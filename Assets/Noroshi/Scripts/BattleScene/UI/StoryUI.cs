using UniLinq;
using UniRx;
using Noroshi.Core.Game.Battle;
using Noroshi.BattleScene.Camera;
using System.Collections.Generic;
using Noroshi.BattleScene.Sound;
using Noroshi.Core.Game.Sound;

namespace Noroshi.BattleScene.UI
{
    public class StoryUI : UIViewModel<IStoryUIView>
    {
        Subject<bool> _onMaskWorldSubject = new Subject<bool>();
        Subject<CameraEvent> _onTryCommandCameraSubject = new Subject<CameraEvent>();
        Subject<SoundEvent> _onCommandSoundSubject = new Subject<SoundEvent>();
        /// BGMを元に戻すタイミングで OnNext される Subject。
        Subject<bool> _onRestoreBgmSubject = new Subject<bool>();
        Dictionary<StoryEffectType, uint> _storyEffectToSoundIdMap = null;

        CompositeDisposable _disposables = new CompositeDisposable();

        public StoryUI()
        {
            // 全体演出とサウンドIDを関連付け
            _storyEffectToSoundIdMap = new Dictionary<StoryEffectType, uint>()
            {
                { StoryEffectType.PitchingWithBlow, SoundConstant.STORY_EFFECT_BLOW_SOUND_ID },
                { StoryEffectType.PitchingWithShelling, SoundConstant.STORY_EFFECT_SHELLING_SOUND_ID },
                { StoryEffectType.PitchingWithRoar, SoundConstant.STORY_EFFECT_ROAR_SOUND_ID },
                { StoryEffectType.WhiteOut, SoundConstant.STORY_EFFECT_WHITEOUT_SOUND_ID }
            };
        }
        protected override IObservable<IStoryUIView> _loadView()
        {
            return SceneContainer.GetFactory().BuildStoryUIView();
        }

        public IObservable<bool> GetOnMaskWorldObservable()
        {
            return _onMaskWorldSubject.AsObservable();
        }
        
        public IObservable<CameraEvent> GetOnTryCameraShakeObservable()
        {
            return _onTryCommandCameraSubject.AsObservable();
        }

        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _onCommandSoundSubject.AsObservable();
        }
            
        /// BGMを元に戻すタイミングで OnNext される Observable を取得。
        public IObservable<bool> GetOnRestoreBgmObservable()
        {
            return _onRestoreBgmSubject.AsObservable();
        }

        public IObservable<bool> Start(Story story, bool isAfterBattleStory)
        {
            _onMaskWorldSubject.OnNext(true);
            // BGM 変更
            _playStoryBgm(story);
            // ブラー開始
            _onTryCommandCameraSubject.OnNext(new CameraEvent()
            {
                Command = CameraEvent.CameraCommand.Play,
                Type = CameraEvent.CameraType.Blur
            });
            // サウンドの紐付け
            _uiView.GetOnCommandSoundObservable().Subscribe(_onCommandSoundSubject.OnNext).AddTo(_disposables);
            return _uiView.Begin()
            .SelectMany(_ =>
            {
                // 次のメッセージがあれば表示、なければ何もしない
                if(story.GoNextMessage())
                {
                    return _uiView.GoNext(story.GetCurrentLayoutType(),
                           story.GetCurrentMessageOwnCharacterView(), story.GetCurrentActingTypeOwnCharacter(), story.GetCurrentAnimationTypeOwnCharacter(),
                           story.GetCurrentMessageEnemyCharacterView(), story.GetCurrentActingTypeEnemyCharacter(), story.GetCurrentAnimationTypeEnemyCharacter(),
                           story.GetCurrentOwnCharacterSkinLevel(), story.GetCurrentEnemyCharacterSkinLevel(), story.GetCurrentMessageTextKey(), story.GetCurrentCharacterNameTextKey())
                           .Do(storyUIView => _tryScreenEffect(story.GetCurrentEffectType()));
                }
                else
                {
                    return Observable.Return<IStoryUIView>(_uiView);
                }
            })
            // ストーリーが通常再生されたか、スキップされた場合ストーリーを終了させる
            .Buffer(story.GetMessageNum() + 1).Select(_ => true).Amb(_uiView.GetOnClickSkipButtonObservable())
            .SelectMany(_ => _finish(story))
            .Do(_ =>
            {
                // BGM 戻す
                _playBattleBgm(story, isAfterBattleStory);
                // ブラー終了
                _onTryCommandCameraSubject.OnNext(new CameraEvent()
                {
                    Command = CameraEvent.CameraCommand.Stop,
                    Type = CameraEvent.CameraType.Blur
                });
                _onMaskWorldSubject.OnNext(false);
            })
            .Select(_ => true);
        }

        /// ストーリー用のBGMに変更する
        void _playStoryBgm(Story story)
        {
            var soundId = story.GetBgmSoundID();
            if (soundId.HasValue)
            {
                // ストーリーBGM再生開始
                _onCommandSoundSubject.OnNext(new SoundEvent() {
                    Command = SoundCommand.Play,
                    SoundID = soundId.Value,
                });
            }
            else
            {
                // BGM変更せず音量半分
                _onCommandSoundSubject.OnNext(new SoundEvent() {Command = SoundCommand.FadeBgmVolumeHalf});
            }
        }

        /// ストーリー用のBGMを停止する
        void _stopStoryBgm(Story story)
        {
            var soundId = story.GetBgmSoundID();
            if (soundId.HasValue)
            {
                // 停止というよりは音量をゼロにする
                _onCommandSoundSubject.OnNext(new SoundEvent() {Command = SoundCommand.FadeBgmVolumeZero});
            }
        }

        /// ストーリー用のBGMから戻す
        void _playBattleBgm(Story story, bool isAfterBattleStory)
        {
            var soundId = story.GetBgmSoundID();
            if (isAfterBattleStory)
            {
                // 結果画面のBGMに変更
                _onCommandSoundSubject.OnNext(new SoundEvent() {Command = SoundCommand.ResetBgmVolume});
                _onRestoreBgmSubject.OnNext(true);
            }
            else if (soundId.HasValue)
            {
                // 戦闘画面のBGMに戻す
                _onCommandSoundSubject.OnNext(new SoundEvent() {Command = SoundCommand.ResetBgmVolume});
                _onRestoreBgmSubject.OnNext(false);
            }
            else 
            {
                // BGM変更せず音量戻す
                _onCommandSoundSubject.OnNext(new SoundEvent() {Command = SoundCommand.FadeBgmVolumeMax});
            }
        }

        void _tryScreenEffect(StoryEffectType? currentType)
        {
            if (currentType.HasValue)
            {
                _playScreenEffect(currentType.Value);
            }
        }

        void _playScreenEffect(StoryEffectType storyEffectType)
        {
            switch(storyEffectType)
            {
            case StoryEffectType.PitchingWithBlow:
            case StoryEffectType.PitchingWithShelling:
            case StoryEffectType.PitchingWithRoar:
                _uiView.ShakeByScreenToDrawingCharacter();
                break;
            case StoryEffectType.WhiteOut:
                _uiView.WhiteOut();
                break;
            default:
                throw new System.ArgumentOutOfRangeException();
            }
            _playSound(_storyEffectToSoundIdMap[storyEffectType]);
        }

        void _playSound(uint soundId)
        {
            _onCommandSoundSubject.OnNext(new SoundEvent()
            {
                Command = SoundCommand.Play,
                SoundID = soundId
            });
        }

        IObservable<bool> _finish(Story story)
        {
            _stopStoryBgm(story);
            story.Finish();
            return _uiView.Finish();
        }

        public override void Dispose()
        {
            base.Dispose();
            _disposables.Dispose();
        }
    }
}
