using System.Collections.Generic;
using UniRx;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.UI
{
    public interface IResultUIView : IModalUIView
    {
        void SetRank(byte rank);
        void SetPlayerExp(uint playerExp);
        void SetGold(uint gold);
        void SetRewards(IEnumerable<Noroshi.Core.WebApi.Response.Possession.PossessionObject> possessionObjects);
        void SetOwnCharacterThumbnails(IEnumerable<CharacterThumbnail> characterThumbnail);
        void SetOwnCharacterProgress(byte characterNo, float previousExpRatio, float currentExpRatio, ushort levelUpNum);
        void SetTips(uint tipsId);
        void SetTrainingResult(ushort? defeatingNum, uint? totalDamage);
        void SetActiveLossTransitionSceneButton(uint playerLevel, Core.Game.Player.TutorialStep tutorialStep);
        void SetAddPlayerExpResult(Core.WebApi.Response.Players.AddPlayerExpResult addPlayerExpResult);
        void SetPickupCharacterMessage(uint pickupCharacterId);
        IObservable<IResultUIView> LoadAssets();
        IObservable<SoundEvent> GetOnCommandSoundObservable();
        IObservable<string> GetOnTransitToSceneObservable();
    }
}
