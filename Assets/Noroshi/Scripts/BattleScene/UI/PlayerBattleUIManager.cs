﻿using UniRx;
using Noroshi.Core.Game.Battle;
using Noroshi.Character;
using UnityEngine;
using DG.Tweening;

namespace Noroshi.BattleScene.UI
{
    public class PlayerBattleUIManager : AbstractUIManager
    {
        const float CHARACTER_MOVE_END_POSITION_X = 10.0f;
        const float CHARACTER_MOVE_DURATION = 0.4f;

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Prepare()
        {
            base.Prepare();
            var battleManager = (PlayerBattleManager)SceneContainer.GetBattleManager();
            var ownTopCharacterView = battleManager.OwnTopCharacterViewForUI;
            var enemyTopCharacterView = battleManager.EnemyTopCharacterViewForUI;
            
            // アリーナモードの場合開始演出を再生させる
            SceneContainer.GetSceneManager().GetOnEnterReadyObservable()
            .SelectMany(_ => 
            {
                _uiController.SetPlayerNames(battleManager.AdditionalInformation != null ? battleManager.AdditionalInformation.OwnPlayerName : string.Empty,
                                             battleManager.AdditionalInformation != null ? battleManager.AdditionalInformation.EnemyPlayerName : string.Empty);
                _activateCharacterViewForUI(ownTopCharacterView, battleManager.OwnTopCharacterSkinLevel,
                                                enemyTopCharacterView, battleManager.EnemyTopCharacterSkinLevel);
                return Observable.WhenAll(
                    _uiController.DeactivateHeaderAndFooter(),
                    _uiController.ActivateBattleStartUIView(true)
                );
            })
            .SelectMany(_ => _uiController.PlayAnimationPlayerBattleStart())
            .Subscribe(_ => SceneContainer.GetSceneManager().ExitReady())
            .AddTo(_disposables);

            _uiController.GetOnEnterBattleEffectFrameOutObservable()
            .SelectMany(_ => 
            {
                return Observable.WhenAll(
                    _uiController.ActivateHeaderAndFooter(),
                    _uiController.DeactivateBattleStartUIView(true),
                    _deactivateCharacterViewForUI(ownTopCharacterView, enemyTopCharacterView)
                );
            })
            .Subscribe(_ => {}).AddTo(_disposables);
        }

        void _activateCharacterViewForUI(ICharacterView ownCharacterView, byte ownCharacterSkinLevel,
                                         ICharacterView enemyCharacterView, byte enemyCharacterSkinLevel)
        {
            ownCharacterView.SetUpForUI(CenterPositionForUIType.Story);
            ownCharacterView.SetHorizontalDirection(Noroshi.Grid.Direction.Right);
            ownCharacterView.SetSkin(ownCharacterSkinLevel);
            ownCharacterView.SetActive(true);
            var transform = ((CharacterView)ownCharacterView).GetTransform();
            var reversePosition = new Vector3(-transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
            transform.localPosition = reversePosition;

            enemyCharacterView.SetUpForUI(CenterPositionForUIType.Story);
            enemyCharacterView.SetHorizontalDirection(Noroshi.Grid.Direction.Left);
            enemyCharacterView.SetSkin(enemyCharacterSkinLevel);
            enemyCharacterView.SetActive(true);
        }

        IObservable<bool> _deactivateCharacterViewForUI(ICharacterView ownCharacterView, ICharacterView enemyCharacterView)
        {
            Subject<bool> onCompleteSubject = new Subject<bool>();
            var ownCharacterTransform = ((CharacterView)ownCharacterView).GetTransform();
            var enemyCharacterTransform = ((CharacterView)enemyCharacterView).GetTransform();

            ownCharacterTransform.DOLocalMoveX(-CHARACTER_MOVE_END_POSITION_X, CHARACTER_MOVE_DURATION)
            .OnComplete(() =>
            {
                ownCharacterView.SetActive(false);
                onCompleteSubject.OnNext(true);
                onCompleteSubject.OnCompleted();
            });
            enemyCharacterTransform.DOLocalMoveX(CHARACTER_MOVE_END_POSITION_X, CHARACTER_MOVE_DURATION)
            .OnComplete(() => 
            {
                enemyCharacterView.SetActive(false);
            });
            return onCompleteSubject.AsObservable();
        }
        
        protected override void _playWinMessage()
        {
            var battleManager = SceneContainer.GetBattleManager();
            var victoryOrDefeat = battleManager.BattleResult.GetVictoryOrDefeat();
            switch (victoryOrDefeat)
            {
            case VictoryOrDefeat.Win:
                _uiController.PlayAnimationPlayerBattleVictory().Subscribe(_ => {}).AddTo(_disposables);
                break;
            case VictoryOrDefeat.TimeUp:
                _uiController.PlayAnimationTimeUp().Subscribe(_ => {}).AddTo(_disposables);;
                break;
            }
        }
    }
}
