﻿using UniRx;

namespace Noroshi.BattleScene.UI
{
    public interface IStoryUIView : MonoBehaviours.IUIView
    {
        IObservable<bool> Begin();
        IObservable<IStoryUIView> GoNext(Core.Game.Battle.StoryLayoutType? storyLayoutType,
                                         ICharacterView ownCharacterView, Core.Game.Battle.StoryCharacterActingType? ownCharacterActingType, Core.Game.Battle.StoryCharacterAnimationType? ownCharacterAnimationType,
                                         ICharacterView enemyCharacterView, Core.Game.Battle.StoryCharacterActingType? enemyCharacterActingType, Core.Game.Battle.StoryCharacterAnimationType? enemyCharacterAnimationType,
                                         byte ownSkinLevel, byte enemySkinLevel, string textKey, string characterNameTextKey);
        IObservable<bool> Finish();
        void ShakeByScreenToDrawingCharacter();
        void StopShakeCharacter();
        IObservable<bool> GetOnClickSkipButtonObservable();
        void WhiteOut();
        IObservable<Noroshi.BattleScene.Sound.SoundEvent> GetOnCommandSoundObservable();
    }
}
