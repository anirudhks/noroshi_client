﻿using UniRx;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene.UI
{
    public interface IOwnCharacterPanelUIView : MonoBehaviours.IUIView
    {
        IObservable<bool> GetOnClickObservable();
        IObservable<SoundEvent> GetOnCommandSoundObservable();
        void ChangeHP(ChangeableValueEvent hpEvent);
        void ChangeEnergy(ChangeableValueEvent energyEvent);
        void ToggleActiveActionAvailable(bool available);
        void Initialize(uint? activeActionId, uint initializeHp, uint maxHp, int initializeEnergy, int maxEnergy, ActiveActionType? activeActionType);
        void ChangeStatusBoost(CharacterStatusBoostEvent boostEvent);
        void FinishActiveAction();
        void EnterActiveAction();
        void ReserveActiveAction();
        void SetParentEnterActiveActionEffectFrame(UnityEngine.Transform parent);
        void ResetEffectAnimation();
        IObservable<bool> GetOnAccumulationClickObservable();
        void ExecuteAccumulationAction();
        void MaxAccumulation();
    }
}
