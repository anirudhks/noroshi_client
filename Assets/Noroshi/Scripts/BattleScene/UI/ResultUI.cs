﻿using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.Game.Battle;

namespace Noroshi.BattleScene.UI
{
    public class ResultUI : AbstractModalUIViewModel<IResultUIView>
    {
        readonly string _name;
        public ResultUI(string name)
        {
            _name = name;
        }
        protected override IObservable<IResultUIView> _loadView()
        {
            return SceneContainer.GetFactory().BuildResultUIView(_name);
        }
        public IObservable<IResultUIView> LoadView(BattleResult battleResult)
        {
            return LoadView().SelectMany(view =>
            {
                var ownPlayerCharacters = battleResult.GetOwnPlayerCharacters().ToArray();
                var addCharacterExpResultMap = battleResult.GetAddCharacterExpResults().ToDictionary(acer => acer.PlayerCharacterID);

                view.SetRank(battleResult.GetRank());
                view.SetPlayerExp(battleResult.GetPlayerExp());
                view.SetGold(battleResult.GetGold());
                view.SetAddPlayerExpResult(battleResult.GetAddPlayerExpResult());
                view.SetRewards(battleResult.GetDisplayRewards());
                view.SetOwnCharacterThumbnails(ownPlayerCharacters.Select(pc => pc.BuildCharacterThumbnail()));
                view.SetActiveLossTransitionSceneButton(battleResult.GetPlayerLevel(), battleResult.GetTutorialStep());
                for (byte no = 1; no <= ownPlayerCharacters.Length; no++)
                {
                    var playerCharacter = ownPlayerCharacters[no - 1];
                    if (!addCharacterExpResultMap.ContainsKey(playerCharacter.PlayerCharacterID)) continue;
                    var characterExpResult = addCharacterExpResultMap[playerCharacter.PlayerCharacterID];
                    view.SetOwnCharacterProgress(
                        no, characterExpResult.PreviousExpRatio, characterExpResult.CurrentExpRatio,
                        (ushort)(characterExpResult.CurrentLevel - characterExpResult.PreviousLevel)
                    );
                }
                // TODO : 複数対応。
                view.SetTips(1);
                view.SetPickupCharacterMessage(battleResult.GetPickUpCharacter().CharacterID);
                return view.LoadAssets();
            })
            .Select(_ => _uiView);
        }

        public void SetDefeatingNum(ushort defeatingNum)
        {
            _uiView.SetTrainingResult(defeatingNum, null);
        }
        public void SetTotalDamage(uint totalDamage)
        {
            _uiView.SetTrainingResult(null, totalDamage);
        }
    }
}
