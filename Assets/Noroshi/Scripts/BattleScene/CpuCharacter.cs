using UniRx;
using Noroshi.BattleScene.Actions.Attributes;
using Noroshi.Core.WebApi.Response.Battle;

namespace Noroshi.BattleScene
{
    /// CPU キャラクターを扱うクラス。
    public class CpuCharacter : Character
    {
        readonly bool _unlimitedHp;
        /// 死亡時逃走演出フラグ。
        public bool IsDeadEscape { get; private set; }

        /// サーバーから受け取った BattleCharacter でインスタンス化。
        public CpuCharacter(IAttributeAssetFactory assetFactory, BattleCharacter battleCharacter, bool unlimitedHp) : base(assetFactory)
        {
            _battleCharacter = battleCharacter;
            _unlimitedHp = unlimitedHp;
        }

        /// ボスかどうか。
        public bool IsBoss { get { return _battleCharacter.IsBoss; } }

        protected override CharacterHP _buildHP(uint maxHp, uint hpRegen)
        {
            return new CharacterHP(maxHp, hpRegen, _unlimitedHp);
        }

        /// 死亡前逃走演出フラグを立てる。
        public void SetEscapeBeforeDeadOn()
        {
            IsDeadEscape = true;
            _view.SetEscapeBeforeDeadOn();
        }
        /// 死亡前逃走を実行する。
        public void EscapeBeforeDead()
        {
            _view.PlayEscapeBeforeDead();
        }

        /// 死亡状態へ入った際の処理。
        protected override void _deadOnEnter()
        {
            // 死亡状態に入ったら逃走するキャラクターは見た目変更を解除する。
            if (IsDeadEscape) _forceResetView();
            base._deadOnEnter();
        }
    }
}
