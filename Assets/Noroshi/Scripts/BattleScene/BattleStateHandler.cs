using System;
using System.Collections.Generic;
using UniRx;
using Flaggs.StateTransition;
using Noroshi.Core.Game.Battle;

namespace Noroshi.BattleScene
{
    /// バトル状態遷移操作クラス。
    public class BattleStateHandler
    {
        /// 状態遷移マップ。
        static readonly Dictionary<Type, Type[]> STATE_MAP = new Dictionary<Type, Type[]>
        {
            {typeof(WaveBattle), new[]{typeof(Interval)}},
            {typeof(Interval), new[]{typeof(SwitchWave)}},
            {typeof(SwitchWave), new[]{typeof(WaveBattle)}},
        };


        /// 状態遷移マネージャー。
        StateTransitionManager<BattleState> _stateTransitionManager = new StateTransitionManager<BattleState>(STATE_MAP);
        /// 状態遷移が全て終了した際に、勝敗が OnNext される Subject。
        Subject<VictoryOrDefeat> _onFinishSubject = new Subject<VictoryOrDefeat>();


        /// Initializes a new instance of the <see cref="Noroshi.BattleScene.BattleStateHandler"/> class.
        public BattleStateHandler()
        {
            _stateTransitionManager.AddState<WaveBattle>();
            _stateTransitionManager.AddState<Interval>();
            _stateTransitionManager.AddState<SwitchWave>();
        }

        /// ロジック更新の実行可否チェック。
        public bool CanLogicUpdate() { return _stateTransitionManager.CurrentStateIs<WaveBattle>(); }

        /// WaveBattle 状態へ遷移した際に OnNext される Observable を取得。
        public IObservable<BattleState> GetOnEnterWaveBattleStateObservable()
        {
            return _stateTransitionManager.GetOnEnterStateObservable().Where(state => state.GetType() == typeof(WaveBattle));
        }
        /// Interval 状態へ遷移した際に OnNext される Observable を取得。
        public IObservable<BattleState> GetOnEnterIntervalStateObservable()
        {
            return _stateTransitionManager.GetOnEnterStateObservable().Where(state => state.GetType() == typeof(Interval));
        }
        /// SwitchWave 状態へ遷移した際に OnNext される Observable を取得。
        public IObservable<BattleState> GetOnEnterSwitchWaveStateObservable()
        {
            return _stateTransitionManager.GetOnEnterStateObservable().Where(state => state.GetType() == typeof(SwitchWave));
        }

        /// 状態遷移を開始する。
        /// 状態遷移が全て終了した際に、勝敗が OnNext される Observable を返す。
        public IObservable<VictoryOrDefeat> Start()
        {
            _stateTransitionManager.Start<WaveBattle>();
            return _onFinishSubject.AsObservable();
        }
        /// WaveBattle 状態へ遷移する。
        public void TransitToWaveBattle()
        {
            if (!_stateTransitionManager.TransitTo<WaveBattle>()) throw new InvalidOperationException();
        }
        /// Interval 状態へ遷移する。
        void _transitToInterval()
        {
            if (!_stateTransitionManager.TransitTo<Interval>()) throw new InvalidOperationException();
        }
        /// SwitchWave 状態へ遷移する。
        public void TransitToSwitchWave()
        {
            if (!_stateTransitionManager.TransitTo<SwitchWave>()) throw new InvalidOperationException();
        }

        /// WaveBattle 状態を終了させる。
        public void FinishWaveBattle(VictoryOrDefeat victoryOrDefeat, bool isLastWave)
        {
            if (!isLastWave)
            {
                _transitToInterval();
            }
            else
            {
                _onFinishSubject.OnNext(victoryOrDefeat);
            }
        }


        public class BattleState : IState
        {
        }
        public class WaveBattle : BattleState
        {
        }
        public class Interval : BattleState
        {
        }
        public class SwitchWave : BattleState
        {
        }
    }
}
