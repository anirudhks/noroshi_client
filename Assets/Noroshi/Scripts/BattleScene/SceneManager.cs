﻿using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.Game.Battle;
using Noroshi.BattleScene.Cache;
using Noroshi.BattleScene.UI;
using Noroshi.BattleScene.Camera;
using Noroshi.BattleScene.CharacterEffect;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene
{
    /// バトルシーン内のロジックを管理するルートクラス。全てのバトルロジックはこのクラス以下で行われる。
    public class SceneManager
    {
        /// バトル種別。
        readonly BattleType _battleType;
        /// ロジック更新処理のインターバルフレーム数。
        readonly int _logicUpdateIntervalFrameNum = Constant.ENGINE_FPS / Constant.LOGIC_FPS;
        /// シーン状態遷移操作オブジェクト。
        readonly SceneStateHandler _sceneStateHandler = new SceneStateHandler();
        /// 全管理オブジェクトを格納している配列。
        readonly IManager[] _managers;
        /// バトル管理オブジェクト。
        public readonly IBattleManager BattleManager;
        /// キャラクター管理オブジェクト。
        public readonly CharacterManager CharacterManager = new CharacterManager();
        /// キャラクターエフェクト管理オブジェクト。
        readonly CharacterEffectManager _characterEffectManager = new CharacterEffectManager();
        /// UI管理オブジェクト。
        readonly IUIManager _uiManager;
        /// カメラ管理オブジェクト。
        readonly ICameraManager _cameraManager;
        /// サウンド管理オブジェクト。
        readonly SoundManager _soundManager = new SoundManager();
        /// キャッシュ管理オブジェクト。
        public readonly CacheManager CacheManager = new CacheManager();
        /// 累積フレーム数。
        int _frameCount = 1;
        /// Ready 状態から遷移可能になったタイミングで OnNext される Subject。
        Subject<bool> _onEnableExitReadySubject = new Subject<bool>();
        /// Disposable 集合。
        CompositeDisposable _disposables = new CompositeDisposable();


        /// <summary>
        /// Initializes a new instance of the <see cref="Noroshi.BattleScene.SceneManager"/> class.
        /// </summary>
        /// <param name="battleType">バトル種別</param>
        /// <param name="battleCategory">バトルカテゴリー</param>
        /// <param name="id">バトルコンテンツ内ID</param>
        /// <param name="ownPlayerCharacterIds">連れて行く自キャラクターのプレイヤーキャラクターID</param>
        /// <param name="rentalPlayerCharacterId">レンタルプレイヤーキャラクターID</param>
        /// <param name="paymentNum">支払い数</param>
        public SceneManager(BattleType battleType, BattleCategory battleCategory, uint id, uint[] ownPlayerCharacterIds, uint? rentalPlayerCharacterId, uint paymentNum)
        {
            _battleType = battleType;
            switch (_battleType)
            {
                case BattleType.CpuBattle:
                    BattleManager = new CpuBattleManager(battleCategory, id, ownPlayerCharacterIds, rentalPlayerCharacterId, paymentNum);
                    _uiManager = new CpuBattleUIManager();
                    _cameraManager = new CpuBattleCameraManager();
                    break;
                case BattleType.PlayerBattle:
                    BattleManager = new PlayerBattleManager(battleCategory, id, ownPlayerCharacterIds, rentalPlayerCharacterId, paymentNum);
                    _uiManager = new PlayerBattleUIManager();
                    _cameraManager = new PlayerBattleCameraManager();
                    break;
                default:
                    throw new InvalidOperationException();
            }
            // Manager は配列にも格納し、全 Manager に対する処理はこれを利用する。
            _managers = new IManager[]
            {
                BattleManager,
                CharacterManager,
                _characterEffectManager,
                _uiManager,
                _cameraManager,
                _soundManager,
                CacheManager,
            };
        }


        /// シーン遷移時にシーン名が OnNext される Observable を取得。
        public IObservable<string> GetOnTransitSceneObservable()
        {
            return _uiManager.GetOnTransitSceneObservable();
        }
        /// ポーズ時に true が、ポーズ解除時に false が OnNext される Observable を取得。
        public IObservable<bool> GetOnTogglePauseObservable()
        {
            return _uiManager.GetOnTogglePauseObservable();
        }
        /// ゲーム時間の進みが遅くなる際に Duration が OnNext される Observable を取得。
        public IObservable<float> GetOnSlowObservable()
        {
            return BattleManager.GetOnEnterSlowTimeObservable();
        }
        /// Ready 状態へ遷移した際に OnNext される Observable を取得。
        public IObservable<BattleType> GetOnEnterReadyObservable()
        {
            return _sceneStateHandler.GetOnEnterReadyObservable().Select(_ => _battleType);
        }
        /// Result 状態へ遷移した際に勝敗が OnNext される Observable を取得。
        public IObservable<VictoryOrDefeat> GetOnEnterResultObservable()
        {
            return _sceneStateHandler.GetOnEnterResultObservable();
        }
        /// バトル前ストーリー終了時に OnNext される Observable を取得。
        public IObservable<bool> GetOnExitBeforeBattleStoryObservable()
        {
            var cpuBattleUiManager = _uiManager as CpuBattleUIManager;
            return cpuBattleUiManager != null ? cpuBattleUiManager.GetOnExitBeforeBattleStoryObservable() : Observable.Empty<bool>();
        }
        /// サウンド操作時に操作内容が OnNext される Observable を取得。
        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return (new[]
            {
                BattleManager.GetOnCommandSoundObservable(),
                _uiManager.GetOnCommandSoundObservable(),
            })
            .Merge();
        }


        ///（ブロッキング）初期化処理。各 Manager 毎の相互参照を含んだ初期化が中心。
        public void Initialize()
        {
            for (var i = 0; i < _managers.Length; i++)
            {
                _managers[i].Initialize();
            }
        }

        /// バトルロジックを開始する。
        public void Start()
        {
            // Initialization 状態処理。
            _sceneStateHandler.GetOnEnterInitializationObservable()
            .SelectMany(_ => _onEnterInitialization()).Subscribe(_ => _sceneStateHandler.TransitToReady()).AddTo(_disposables);
            // Ready 状態処理。
            _sceneStateHandler.GetOnEnterReadyObservable()
            .SelectMany(_ => _onEnterReady()).Subscribe(_ => _sceneStateHandler.TransitToBattle()).AddTo(_disposables);
            // Battle 状態処理。
            _sceneStateHandler.GetOnEnterBattleObservable()
            .SelectMany(_ => _onEnterBattle()).Subscribe(_sceneStateHandler.TransitToResult).AddTo(_disposables);

            // 状態遷移開始。
            _sceneStateHandler.Start();
        }

        /// Initialization 状態処理。
        IObservable<bool> _onEnterInitialization()
        {
            // 必要なデータをロードする。
            return _loadDatas()
                // 必要なアセットをロードする。
                .SelectMany(_ => _loadAssets())
                // （ブロッキング）初期化最終処理。
                .Do(_ => _prepare())
                // 各 MonoBehaviour の Start() が呼ばれるようにちょっと待つ。
                .SelectMany(_ => SceneContainer.GetTimeHandler().Timer(0.1f))
                .Select(_ => true);
        }
        /// 必要なデータをロードする。
        IObservable<SceneManager> _loadDatas()
        {
            return _managers.Aggregate(Observable.Return<IManager>(null), (now, next) => now.SelectMany(_ => next.LoadDatas()))
                .Select(_ => this);
        }
        /// 必要なアセットをロードする。
        IObservable<SceneManager> _loadAssets()
        {
            var factory = SceneContainer.GetFactory();
            return _managers.Aggregate(Observable.Return<IManager>(null), (now, next) => now.SelectMany(_ => next.LoadAssets(factory)))
                .Select(_ => this);
        }
        ///（ブロッキング）初期化最終処理。アセットロード後でないと実施できない 各 Manager 毎の相互参照を含んだ処理の紐付けなどを行う。
        void _prepare()
        {
            // 極力メモリ解放を試みておく。
            UnityEngine.Resources.UnloadUnusedAssets();
            for (var i = 0; i < _managers.Length; i++)
            {
                _managers[i].Prepare();
            }
        }

        /// Ready 状態処理。
        IObservable<bool> _onEnterReady()
        {
            // 状態遷移中の処理なので割り込みを避けるべく、待つ必要なくても無理矢理ノンブロッキング処理にする。
            return BattleManager.ShouldWaitToFinishReady() ? _onEnableExitReadySubject.AsObservable() : SceneContainer.GetTimeHandler().Timer(0.01f).Select(_ => true);
        }
        /// Ready 状態を終了させる。
        public void ExitReady()
        {
            _onEnableExitReadySubject.OnNext(true);
            _onEnableExitReadySubject.OnCompleted();
        }

        /// Battle 状態処理。
        IObservable<VictoryOrDefeat> _onEnterBattle()
        {
            return BattleManager.Start();
        }


        /// フレーム毎に呼び出されるロジック更新処理。
        public void UpdatePerFrame()
        {
            // 実際の更新頻度は FPS と一緒とは限らない。
            if (_frameCount % _logicUpdateIntervalFrameNum == 0)
            {
                BattleManager.LogicUpdate();
            }
            _frameCount++;
        }

        /// リソース破棄。
        public void Dispose()
        {
            _disposables.Dispose();
            for (var i = 0; i < _managers.Length; i++)
            {
                _managers[i].Dispose();
            }
        }
    }
}
