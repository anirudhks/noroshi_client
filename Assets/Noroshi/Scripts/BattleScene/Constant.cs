﻿namespace Noroshi.BattleScene
{
    public class Constant
    {
        /// Wave 当たり最低必要な時間。
        public const byte MIN_WAVE_BATTLE = 3;
        // ループバトル時の最大表示Wave(あくまで表示上の上限)
        public const byte LOOP_BATTLE_MAX_WAVE_NUM_FOR_DISPLAY = 99;

        public const int MAX_NORMAL_CHARACTER_NUM_IN_FIELD_PER_FORCE = 5;
        public const int MAX_SHADOW_CHARACTER_NUM_IN_FIELD_PER_FORCE = 8;

        /// フィールドグリッドの横サイズ
        public const int FIELD_HORIZONTAL_GRID_SIZE = 128;
        public const int VISIBLE_FIELD_HORIZONTAL_GRID_SIZE = 56;

        // 1グリッド移動するのにかかる秒数
        public const float WALK_TIME_PER_GRID = 0.15f;

        // ゲームエンジンループの FPS
        public const int ENGINE_FPS = 60;
        // ロジックメインループの FPS
        public const int LOGIC_FPS = 10;

        // アクション間の時間目安
        public const float IDEAL_ACTION_INTERVAL = 2f;
        /// バトル開始前の演出時間
        public const float BATTLE_READY_TIME = 3.0f;
        public const float BATTLE_READY_ANIMATION_TIME = 2.75f;
        public const float BATTLE_READY_ZOOM_OUT_TIME = 0.5f;
        /// バトル終了時の一時停止時間
        public const float BATTLE_FINISH_PAUSE_TIME = 1f;
        public const float BATTLE_FINISH_SLOW_TIME = 2f;
        public const float SLOW_TIME_SCALE = 0.25f;

        public const float SWITCH_WAVE_TIME = 1f;
        public const float SLOW_SWITCH_WAVE_TIME = 4f;

        public const float DAMAGE_TIME = 0.5f;
        public const float KNOCKBACK_TIME = 0.75f;
        public const int   KNOCKBACK_DISTANCE = 10;
        public const float KNOCKBACK_DIFFRENCE_Y = 1.0f;
        public const float TORNADO_FIRST_MOVE_TIME = 1.5f;
        public const float TORNADO_SECOND_MOVE_TIME = 0.15f;
        public const float TORNADO_FIRST_MOVE_DISTANCE = 7f;
        public const float TORNADO_SECOND_MOVE_DISTANCE = 0.3f;

        public const float MOVE_SCREEN_END_DIFFRENCE_Y = 4.0f;

        public const float KACHIAGE_TIME = 1.0f;
        public const int   KACHIAGE_DISTANCE = 10;
        public const float KACHIAGE_DIFFRENCE_Y = 4.0f;
        /// 最後のキャラクターアニメーション演出時間。
        public const float LAST_CHARACTER_ANIMATION_TIME = 3f;
        public const float BOSS_ESCAPE_TIME = 3f;

        public const string UI_SORTING_LAYER_NAME = "UI";
        public const int SPINE_UI_ORDER_IN_LAYER = -1;
        public const int ORDER_RANGE_IN_CHARACTER_LAYER = 1000;

        // チャージアクション時に再生するエフェクトID
        public const uint CHARGE_ACTION_CHARACTER_EFFECT_ID = 1;

        public const uint DROP_COIN_CHARACTER_EFFECT_ID = 2;
        // 回避した時に再生するエフェクトID
        public const uint DODGE_CHARACTER_EFFECT_ID = 4;
        // 変身した時に再生するエフェクトID
        public const uint TRANSFORMATION_EFFECT_ID = 5008;

        public const string ACTION_RANK_0_ANIMATION_NAME = "attack";
        public const string ACTION_RANK_1_ANIMATION_NAME = "a_skill1";
        public const string ACTION_RANK_2_ANIMATION_NAME = "p_skill1";
        public const string ACTION_RANK_3_ANIMATION_NAME = "p_skill2";
        public const string ACTION_RANK_4_ANIMATION_NAME = "p_skill3";
        public const string ACTION_RANK_5_ANIMATION_NAME = "p_skill4";
        // 状態異常攻撃のレベル差による成功確率の底
        public const float ATTRIBUTE_LEVEL_BASE = 0.75f;
        // 画像のPixel Per Unit
        public const float PIXEL_PER_UNIT = 100.0f;
        public const float DEFAULT_BATTLE_SCREEN_WIDTH = 1136.0f;
        public const float DEFAULT_BATTLE_SCREEN_HEIGHT = 640.0f;
        // チャージ系アクティブアクションの最大チャージまでにかかる時間
        public const float ACTIVE_ACTION_ACCUMULATION_TIME = 10.0f;
        // チャージ系アクティブアクション時の最大係数
        public const float ACTIVE_ACTION_ACCUMULATION_MAX_COEFFICEINT = 10.0f;
    }
}
