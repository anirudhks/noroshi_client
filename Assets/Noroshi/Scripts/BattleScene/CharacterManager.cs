using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.Game.Character;
using Noroshi.BattleScene.Actions;
using Noroshi.BattleScene.Actions.Attributes;

namespace Noroshi.BattleScene
{
    /// フィールドに配置されていない状態も含めてキャラクターを管理するクラス。
    /// 逆にフィールドに配置されているキャラクター操作は極力 BattleManager / Wave / Field へ寄せる。
    public class CharacterManager : IManager
    {
        int _currentEnemyCharacterSetIndex = 0;
        List<CharacterSet> _enemyCharacterSets = new List<CharacterSet>();
        readonly CharacterSet _ownShadowSet   = new CharacterSet(Force.Own);
        readonly CharacterSet _enemyShadowSet = new CharacterSet(Force.Enemy, true);
        Subject<ShadowCharacter> _onCompleteAddingShadowSubject = new Subject<ShadowCharacter>();
        CompositeDisposable _disposables = new CompositeDisposable();
        IAttributeAssetFactory _assetFactory;

        public CharacterSet CurrentOwnCharacterSet { get; private set; }
        public CharacterSet CurrentEnemyCharacterSet { get { return _enemyCharacterSets[_currentEnemyCharacterSetIndex]; } }

        public void Initialize()
        {
            _assetFactory = SceneContainer.GetFactory().GetAttributeAssetFactory();
            CurrentOwnCharacterSet = new CharacterSet(Force.Own);
        }
        void _subscribeCharacterSetObservables(CharacterSet characterSet)
        {
            characterSet.GetOnAddShadowObservable().Do(s => _makeShadow(s))
            .Subscribe(_ => {}).AddTo(_disposables);
            characterSet.GetOnRemoveShadowObservable().Do(s => _removeShadow(s))
            .Subscribe(_ => {}).AddTo(_disposables);
        }
        public IObservable<IManager> LoadDatas()
        {
            var observables = _enemyCharacterSets.Select(cs => cs.LoadDatas()).ToList();
            observables.Add(CurrentOwnCharacterSet.LoadDatas());
            return Observable.WhenAll(observables)
                .Select(_ => (IManager)this);
        }
        public IObservable<IManager> LoadAssets(IFactory factory)
        {
            var actionFactory = (IActionFactory)factory;
            var observables = _enemyCharacterSets.Select(cs => cs.LoadAssets(actionFactory)).ToList();
            observables.Add(CurrentOwnCharacterSet.LoadAssets(actionFactory));
            return Observable.WhenAll(observables)
            .Do(_ => _activateCurrentOwnCharacterSetViews())
            .Do(_ => _toggleActiveEnemyCharacterSetViews())
            .Select(_ => (IManager)this);
        }
        public void Prepare()
        {
        }

        void _activateCurrentOwnCharacterSetViews()
        {
            CurrentOwnCharacterSet.SetViewActive(true);
        }
        void _toggleActiveEnemyCharacterSetViews()
        {
            for (var i = 0; i < _enemyCharacterSets.Count(); i++)
            {
                _enemyCharacterSets[i].SetViewActive(i == _currentEnemyCharacterSetIndex);
            }
        }

        public IObservable<ShadowCharacter> GetOnCompleteAddingShadowObservable()
        {
            return _onCompleteAddingShadowSubject.AsObservable();
        }
        public IObservable<ShadowCharacter> GetOnRemoveShadowObservable()
        {
            return Observable.Merge(
                CurrentOwnCharacterSet.GetOnRemoveShadowObservable(),
                CurrentEnemyCharacterSet.GetOnRemoveShadowObservable()
            );
        }

        /// 自キャラセットメソッド。
        public Character[] SetCurrentOwnPlayerCharacters(IEnumerable<Core.WebApi.Response.Battle.BattleCharacter> battleCharacters)
        {
            var characters = battleCharacters.Select(bc => new PlayerCharacter(_assetFactory, bc)).Cast<Character>();
            CurrentOwnCharacterSet.SetCharacters(characters);
            _subscribeCharacterSetObservables(CurrentOwnCharacterSet);
            return CurrentOwnCharacterSet.GetCharacters();
        }
        /// 敵キャラセットメソッド。

        public void SetCurrentEnemyCpuCharacterSets(IEnumerable<IEnumerable<Core.WebApi.Response.Battle.BattleCharacter>> cpuCharacterSets, bool unlimitedHPEnemyBattle)
        {
            foreach (var cpuCharacterSet in cpuCharacterSets)
            {
                var characterSet = new CharacterSet(Force.Enemy, true);
                characterSet.SetCharacters(cpuCharacterSet.Select(cc => new CpuCharacter(_assetFactory, cc, unlimitedHPEnemyBattle)).Cast<Character>());
                _subscribeCharacterSetObservables(characterSet);
                _enemyCharacterSets.Add(characterSet);
            }
        }
        public void SetCurrentEnemyPlayerCharacterSets(IEnumerable<IEnumerable<Core.WebApi.Response.Battle.BattleCharacter>> playerCharacterSets)
        {
            foreach (var playerCharacters in playerCharacterSets)
            {
                var characterSet = new CharacterSet(Force.Enemy, true);
                characterSet.SetCharacters(playerCharacters.Select(pc => new PlayerCharacter(_assetFactory, pc)).Cast<Character>());
                _subscribeCharacterSetObservables(characterSet);
                _enemyCharacterSets.Add(characterSet);
            }
        }
        public void SwitchNextEnemyCharacterSet()
        {
            _currentEnemyCharacterSetIndex++;
            // ループバトル対応。
            if (_currentEnemyCharacterSetIndex >= _enemyCharacterSets.Count())
            {
                _currentEnemyCharacterSetIndex = 0;
            }
            // 過去に利用した EnemyCharacterSet の中身は死亡状態のはずなので復活させる。
            foreach (var character in CurrentEnemyCharacterSet.GetCharacters())
            {
                character.Resurrect();
            }
            _toggleActiveEnemyCharacterSetViews();
        }

        void _makeShadow(ShadowCharacter shadow)
        {
            var shadowSet = shadow.Force == Force.Own ? _ownShadowSet : _enemyShadowSet;
            if (shadowSet.GetCharacters().Count() >= Constant.MAX_SHADOW_CHARACTER_NUM_IN_FIELD_PER_FORCE)
            {
                return;
            }
            shadowSet.AddCharacter(shadow);
            SceneContainer.GetBattleManager().AddShadowCharacter(shadow);
            _onCompleteAddingShadowSubject.OnNext(shadow);
        }
        void _removeShadow(ShadowCharacter shadow)
        {
            var shadowSet = shadow.Force == Force.Own ? _ownShadowSet : _enemyShadowSet;
            shadowSet.RemoveCharacter(shadow);
        }

        // バトルで使用するすべてのキャラクターを取得する(かぶりなし)
        public Character[] GetAllCharacters()
        {
            var characters = new List<Character>(GetCurrentOwnCharacters().Cast<Character>());
            foreach (var enemy in _enemyCharacterSets)
            {
                characters.AddRange(enemy.GetCharacters().Cast<Character>());
            }
            return characters.Distinct().ToArray();
        }

        /// 現在の自・敵キャラを全て取得
        public Character[] GetCurrentAllCharacters()
        {
            var characters = new List<Character>(GetCurrentOwnCharacters().Cast<Character>());
            characters.AddRange(GetCurrentEnemyCharacters().Cast<Character>());
            return characters.ToArray();
        }
        /// 現在の自キャラを全て取得
        public Character[] GetCurrentOwnCharacters() { return CurrentOwnCharacterSet.GetCharacters(); }
        /// 現在の敵キャラを全て取得
        public Character[] GetCurrentEnemyCharacters() { return CurrentEnemyCharacterSet.GetCharacters(); }
        // 現在の自・敵キャラの分身を全て取得
        public Character[] GetCurrentAllShadowCharacters()
        {
            var characters = new List<Character>(_ownShadowSet.GetCharacters().Cast<Character>());
            characters.AddRange(_enemyShadowSet.GetCharacters().Cast<Character>());
            return characters.ToArray();
        }

        public IObservable<ChangeableValueEvent> GetOnEnemyCharacterSetHPChangeObservable()
        {
            return CurrentEnemyCharacterSet.GetOnTotalHPChangeObservable();
        }
        public uint GetEnemyCharacterSetTotalCurrentHP()
        {
            return CurrentEnemyCharacterSet.GetTotalCurrentHP();
        }
        public uint GetEnemyCharacterSetTotalMaxHP()
        {
            return CurrentEnemyCharacterSet.GetTotalMaxHP();
        }

        public void RemoveAttributeAndShadow()
        {
            foreach (var character in GetCurrentAllCharacters())
            {
                character.RemoveAttributeAndShadow();
            }
            _ownShadowSet.Clear();
            _enemyShadowSet.Clear();
        }

        public void Dispose()
        {
            CurrentOwnCharacterSet.Dispose();
            CurrentEnemyCharacterSet.Dispose();
            _ownShadowSet.Dispose();
            _enemyShadowSet.Dispose();
            _disposables.Dispose();
        }
    }
}
