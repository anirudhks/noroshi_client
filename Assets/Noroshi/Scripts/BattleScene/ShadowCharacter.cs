﻿using UniRx;
using Noroshi.BattleScene.Actions;
using Noroshi.BattleScene.Actions.Attributes;

namespace Noroshi.BattleScene
{
    /// 分身キャラクターを扱うクラス。
    public class ShadowCharacter : Character, IShadow
    {
        /// 分身キャラクター ID。
        public readonly uint ShadowCharacterID;
        public ushort InitialHorizontalIndex;
        public ushort? BaseVerticalIndex;

        /// 分身キャラクター ID でインスタンス化。
        public ShadowCharacter(IAttributeAssetFactory assetFactory, uint shadowCharacterId) : base(assetFactory)
        {
            ShadowCharacterID = shadowCharacterId;
        }

        /// データとアセットをロードする。ロード後、View は非表示にしておく。
        public IObservable<IShadow> LoadDatasAndAssets(IActionFactory factory)
        {
            return LoadDatas().SelectMany(_ => LoadAssets(factory).Do(c => _view.SetActive(false))).Select(_ => (IShadow)this);
        }
        /// キャラクターステータスをロードする。
        protected override IObservable<CharacterStatus> _loadCharacterStatus()
        {
            var shadowMaster = GlobalContainer.MasterManager.CharacterMaster.GetShadowCharacter(ShadowCharacterID);
            var masterData = GlobalContainer.MasterManager.CharacterMaster.Get(shadowMaster.CharacterID);
            var characterStatus = new BattleScene.CharacterStatus(shadowMaster, masterData);
            return Observable.Return(characterStatus);
        }

        /// 出現する。
        public void Appear(ushort characterLevel, ushort actionLevel, ushort? actionLevel4, byte promotionLevel, byte evolutionLevel, byte? maxAvailableActionRank, ushort initialHorizontalIndex, ushort? baseVerticalIndex, byte? skinLevel)
        {
            InitialHorizontalIndex = initialHorizontalIndex;
            BaseVerticalIndex = baseVerticalIndex;
            _characterStatus.OverrideLevels(characterLevel, actionLevel, promotionLevel, evolutionLevel);
            // 分身キャラクターの Action4 はレベル可変。
            if (actionLevel4.HasValue) _characterStatus.OverrideActionLevel(4, actionLevel4.Value);
            HP.ChangeMaxHP(_characterStatus.MaxHP);
            HP.Reset();
            _actionHandler.OverrideActionLevels(_characterStatus.NullableActionLevels);
            _actionHandler.SetAvailableActionRank(maxAvailableActionRank);
            _view.SetActive(true);
            if (skinLevel.HasValue) _view.SetSkin(skinLevel.Value);
        }

        /// 逃走。内部的には直接死亡状態遷移となる。
        public void Escape()
        {
            _tryToTransitToDead();
        }
        /// 死亡（死亡時に流れるだけで必ずしも死亡アニメーションとは限らず逃走、待機もあり）アニメーションを再生する。
        /// オーバーライドする場合は、終了時に処理を引っ掛けるので演出完了時に OnNext(this) される Observable を返し、OnCompleted() も忘れずに呼ぶこと。
        protected override IObservable<Character> _playDieAnimation()
        {
            return _view.PlayEscape().Select(_ => (Character)this);
        }
    }
}
