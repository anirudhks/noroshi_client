﻿using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.Game.Battle;
using Noroshi.Core.Game.Possession;

namespace Noroshi.BattleScene
{
    /// <summary>
    /// バトル結果を扱うクラス。
    /// </summary>
    public class BattleResult
    {
        const byte MAX_DISPLAYABLE_REWARD_NUM = 10;

        readonly BattleCategory _battleCategory;
        readonly uint _battleContentId;
        readonly CharacterSet _currentOwnCharacterSet;
        readonly CharacterSet _currentEnemyCharacterSet;
        readonly VictoryOrDefeat _victoryOrDefeat;
        readonly uint _gold;
        readonly IEnumerable<uint> _itemIds;
        readonly ushort _playerExp;
        readonly ushort _defeatingNum;
        readonly uint _totalDamage;
        Core.WebApi.Response.Battle.FinishBattleResponse _finishResponse;

        /// <summary>
        /// Initializes a new instance of the <see cref="Noroshi.BattleScene.BattleResult"/> class.
        /// </summary>
        /// <param name="battleCategory">バトルカテゴリ</param>
        /// <param name="battleContentId">各バトルコンテンツ内ID</param>
        /// <param name="currentOwnCharacterSet">現自キャラクター集合</param>
        /// <param name="currentEnemyCharacterSet">現敵キャラクター集合</param>
        /// <param name="victoryOrDefeat">勝敗</param>
        /// <param name="gold">獲得ゴールド</param>
        /// <param name="itemIds">獲得アイテムID</param>
        /// <param name="playerExp">獲得プレイヤー経験値</param>
        /// <param name="defeatingNum">敵撃破数</param>
        /// <param name="totalDamage">総与ダメージ</param>
        public BattleResult
        (
            BattleCategory battleCategory, uint battleContentId,
            CharacterSet currentOwnCharacterSet, CharacterSet currentEnemyCharacterSet,
            VictoryOrDefeat victoryOrDefeat, uint gold, IEnumerable<uint> itemIds, ushort playerExp,
            ushort defeatingNum, uint totalDamage
        )
        {
            _battleCategory = battleCategory;
            _battleContentId = battleContentId;
            _currentOwnCharacterSet = currentOwnCharacterSet;
            _currentEnemyCharacterSet = currentEnemyCharacterSet;
            _victoryOrDefeat = victoryOrDefeat;
            _gold = gold;
            _itemIds = itemIds;
            _playerExp = playerExp;
            _defeatingNum = defeatingNum;
            _totalDamage = totalDamage;
        }

        /// <summary>
        /// 勝敗を取得する。
        /// </summary>
        public VictoryOrDefeat GetVictoryOrDefeat() { return _victoryOrDefeat; }
        /// <summary>
        /// 獲得ゴールドを取得する。
        /// </summary>
        public uint GetGold() { return _gold; }
        /// <summary>
        /// 獲得アイテム ID を取得する。
        /// </summary>
        public IEnumerable<uint> GetItemIDs() { return _itemIds; }
        /// <summary>
        /// 獲得プレイヤー経験値を取得する。
        /// </summary>
        public ushort GetPlayerExp() { return _playerExp; }
        /// <summary>
        /// 敵撃破数を取得する。
        /// </summary>
        public ushort GetDefeatingNum() { return _defeatingNum; }
        /// <summary>
        /// 総与ダメージを取得する。
        /// </summary>
        public uint GetTotalDamage() { return _totalDamage; }

        /// <summary>
        /// 自キャラクターそれぞれの与ダメージ結果を取得。
        /// </summary>
        public uint[] GetOwnDamages() { return _currentOwnCharacterSet.GetDamages().ToArray(); }
        /// <summary>
        /// 敵キャラクターそれぞれの与ダメージ結果を取得。
        /// </summary>
        public uint[] GetEnemyDamages() { return _currentEnemyCharacterSet.GetDamages().ToArray(); }

        /// <summary>
        /// ユーザのレベルを取得。
        /// </summary>
        public uint GetPlayerLevel() { return _finishResponse.PlayerLevel; }
        /// <summary>
        /// 現在のチュートリアル進行度を取得。
        /// </summary>
        public Core.Game.Player.TutorialStep GetTutorialStep() { return (Core.Game.Player.TutorialStep)_finishResponse.TutorialStep; }

        /// <summary>
        /// プレイヤー経験値付与結果。サーバーロジック通過結果を返す。
        /// </summary>
        public Core.WebApi.Response.Players.AddPlayerExpResult GetAddPlayerExpResult()
        {
            return _finishResponse.PossessionAdditionalResultInformation.AddPlayerExpResult;
        }
        /// <summary>
        /// キャラクター経験値付与結果。サーバーロジック通過結果を返す。
        /// </summary>
        public Core.WebApi.Response.Character.AddCharacterExpResult[] GetAddCharacterExpResults()
        {
            return _finishResponse.AddCharacterExpResults;
        }
        /// <summary>
        /// サーバーロジックを通した最終的な付与報酬を取得する（ただし表示用なので上限あり）。
        /// </summary>
        public Core.WebApi.Response.Possession.PossessionObject[] GetDisplayRewards()
        {
            return _finishResponse.Rewards.Where(reward => _isDisplayableReward(reward))
                .Take(MAX_DISPLAYABLE_REWARD_NUM).ToArray();
        }
        bool _isDisplayableReward(Core.WebApi.Response.Possession.PossessionObject reward)
        {
            var category = (PossessionCategory)reward.Category;
            return category == PossessionCategory.Drug
                || category == PossessionCategory.ExchangeCashGift
                || category == PossessionCategory.Gear
                || category == PossessionCategory.GearEnchantMaterial
                || category == PossessionCategory.GearPiece
                || category == PossessionCategory.MiscItem
                || category == PossessionCategory.RaidTicket
                || category == PossessionCategory.Soul;
        }
        /// <summary>
        /// バトル終了時に発見したレイドボスを取得する。
        /// </summary>
        public Core.WebApi.Response.RaidBoss.RaidBossAtDiscovery GetRaidBoss()
        {
            return _finishResponse.RaidBossAtDiscovery;
        }

        /// <summary>
        /// バトル結果ランク（星の数）を取得する。
        /// </summary>
        public byte GetRank()
        {
            var deadCharacterNum = _currentOwnCharacterSet.GetCharacters().Where(c => c.IsDead).Count();
            return (byte)(deadCharacterNum == 0 ? 3 : deadCharacterNum == 1 ? 2 : 1);
        }

        /// <summary>
        /// バトル結果 UI でピックアップするキャラクターを取得する。
        /// </summary>
        public Character GetPickUpCharacter()
        {
            return GetVictoryOrDefeat() == VictoryOrDefeat.Win
                ? _currentOwnCharacterSet.GetPickUpCharacter()
                : _currentEnemyCharacterSet.GetPickUpCharacter();
        }

        /// <summary>
        /// 自プレイヤーキャラクター取得（サムネイル表示用）。
        /// </summary>
        public IEnumerable<PlayerCharacter> GetOwnPlayerCharacters()
        {
            return _currentOwnCharacterSet.GetCharacters().Select(c => c as PlayerCharacter).Where(pc => pc != null && !pc.IsCpuCharacter);
        }

        /// <summary>
        /// 最後のキャラクターアニメーション演出時間を開始する。
        /// </summary>
        public IObservable<VictoryOrDefeat> StartLastCharacterAnimationTime()
        {
            // （敵味方に関わらず、ただし分身は除く）各生存キャラクターは（無理なく）待機状態へ遷移後、
            // かつ、敗北側キャラクター全員の死亡アニメーション完了後に勝利状態へ遷移。
            var aliveCharacters = _currentOwnCharacterSet.GetCharacters().Concat(_currentEnemyCharacterSet.GetCharacters()).Where(c => !c.IsDead).ToArray();
            aliveCharacters.Select(aliveCharacter =>
            {
                // 内部的に OnCompleted される前提なので、呼び出し元で Dispose はしなくて良い。
                return aliveCharacter.TryToTransitToIdleOrDeadOnExitWaveBattle()
                    .Zip(_getOnExitAllDefeatForceCharacterDeadAnimationsObservable(), (character, _) => character.TryToTransitToWin());
            })
            .WhenAll().Subscribe(_ => {});

            return SceneContainer.GetTimeHandler().Timer(Constant.LAST_CHARACTER_ANIMATION_TIME)
                // 本時間終了時には強制的に勝利状態へ遷移させてしまう。
                // 演出上、このタイミングでキャラクターは一旦見えなくなるので、アニメーションブツ切り問題は表面化しないはず。
                .Do(_ =>
                {
                    foreach (var aliveCharacter in aliveCharacters)
                    {
                        aliveCharacter.TryToTransitToWin();
                    }
                })
                .Select(_ => GetVictoryOrDefeat());
        }
        /// 敗北側キャラクター全員の死亡アニメーション完了時に敗北側キャラクター全員が OnNext される Observable を取得。
        IObservable<Character[]> _getOnExitAllDefeatForceCharacterDeadAnimationsObservable()
        {
            switch (GetVictoryOrDefeat())
            {
                case VictoryOrDefeat.Win: return _currentEnemyCharacterSet.GetOnExitAllDeadAnimationsObservable();
                case VictoryOrDefeat.Loss: return _currentOwnCharacterSet.GetOnExitAllDeadAnimationsObservable();
                default: return Observable.Empty<Character[]>();
            }
        }

        /// <summary>
        /// サーバーへバトル結果を送信する。
        /// </summary>
        /// <param name="currentWaveNo">現ウェーブ番号</param>
        public IObservable<BattleResult> SendResult(byte currentWaveNo)
        {
            var requestParam = new Datas.Request.BattleFinishRequest
            {
                Category = (byte)_battleCategory,
                ID = _battleContentId,
                Result = LitJson.JsonMapper.ToJson(_makeResult(currentWaveNo)),
                // TODO : ハッシュ値計算。
                Hash = "",
            };
            return SceneContainer.GetWebApiRequester().Post<Datas.Request.BattleFinishRequest, Core.WebApi.Response.Battle.FinishBattleResponse>("Battle/FinishBattle", requestParam)
            .Select(response =>
            {
                _finishResponse = response;
                return this;
            });
        }
        /// サーバーへ送信するバトル結果を生成。
        Core.Game.Battle.BattleResult _makeResult(byte currentWaveNo)
        {
            // 自プレイヤー状態データ作成。
            var ownPlayerCharacters = _currentOwnCharacterSet.GetCharacters().Select(c => new AfterBattlePlayerCharacter
            {
                PlayerCharacterID = ((PlayerCharacter)c).PlayerCharacterID,
                HP = (uint)c.CurrentHP,
                Energy = (ushort)c.Energy.Current,
            });
            // 敵プレイヤー状態データ作成。
            var enemyCharacterStates = _currentEnemyCharacterSet.GetCharacters().Select(c => new Core.Game.Battle.BattleResult.EnemyCharacterState
            {
                InitialHP = c.HP.InitialHP,
                RemainingHP = c.CurrentHP,
            });
            // バトル結果データ作成。
            return new Core.Game.Battle.BattleResult
            {
                VictoryOrDefeat = GetVictoryOrDefeat(),
                Rank = GetRank(),
                AfterBattlePlayerCharacters = ownPlayerCharacters.ToArray(),
                CurrentWaveNo = currentWaveNo,
                CurrentWave = new Core.Game.Battle.BattleResult.Wave
                {
                    EnemyCharacterStates = enemyCharacterStates.ToArray(),
                },
                DefeatingNum = _defeatingNum,
                Damage = _totalDamage,
            };
        }
    }
}
