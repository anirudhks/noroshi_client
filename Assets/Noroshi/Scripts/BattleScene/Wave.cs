using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.WebApi.Response.Battle;
using Noroshi.Core.Game.Battle;
using Noroshi.BattleScene.CharacterEffect;
using Noroshi.BattleScene.Sound;
using Noroshi.BattleScene.Camera;
using Noroshi.BattleScene.Actions;
using Noroshi.BattleScene.Actions.Roles;

namespace Noroshi.BattleScene
{
    /// ウェーブ単位のバトルロジックを扱うクラス。
    /// Dispose 時に加え、ウェーブバトル終了時にも内包している Subject を Oncompleted し、
    // 今後 OnNext で外部処理が走らないようにしている。
    public class Wave
    {
        /// サーバーから取得したウェーブ情報。
        readonly BattleWave _battleWave;
        /// ウェーブ番号。
        public readonly byte No;
        /// フィールド。
        public readonly WaveField Field;
        /// 時間停止試行 （時間停止の中で動いている）キャラクター。
        readonly List<Character> _timeStoppers = new List<Character>();
        /// 時間停止キャラクター。
        readonly List<Character> _timeStoppings = new List<Character>();
        /// アクティブアクション発動待ち予約キュー（自キャラクター用）。
        readonly Queue<Subject<bool>> _onTryOwnActiveActionSubjects = new Queue<Subject<bool>>();
        /// アクティブアクション発動待ち予約キュー（敵キャラクター用）。
        readonly Queue<Subject<bool>> _onTryEnemyActiveActionSubjects = new Queue<Subject<bool>>();
        /// キャラクター重複 Subscribe 防止用データ。
        readonly Dictionary<Character, bool> _subscribedCharacterMap = new Dictionary<Character, bool>();

        /// ウェーブバトル内でのカウントダウン（1秒毎）毎に残りウェーブバトル時間が OnNext される Subject。
        readonly Subject<byte> _onCountDownSubject = new Subject<byte>();
        /// Force 毎の排他的なアクティブアクション時間が終了する際に、true が OnNext される Subject。
        readonly Subject<bool> _onExitExclusiveActiveActionTimePerForceSubject = new Subject<bool>();
        /// 自キャラクターがダメージを与えた際に、与ダメージが OnNext される Subject。
        readonly Subject<int> _onOwnCharacterAddDamageSubject = new Subject<int>();
        /// キャラクターがアクティブアクション状態へ遷移する際に、遷移キャラクターが OnNext される Subject。
        readonly Subject<Character> _onEnterActiveActionSubject = new Subject<Character>();
        /// キャラクター死亡演出終了時に死亡キャラクターが OnNext される Subject。
        readonly Subject<Character> _onExitDeadAnimationSubject = new Subject<Character>();
        /// バトル終了時に勝敗が OnNext される Subject。
        readonly Subject<VictoryOrDefeat> _onFinishBattleSubject = new Subject<VictoryOrDefeat>();
        /// キャラクター死亡時に死亡キャラクターが OnNext される Subject。
        readonly Subject<Character> _onCharacterDieSubject = new Subject<Character>();
        /// フィールド暗転時に true が、暗転解除時に false が OnNext される Subject。
        readonly Subject<bool> _onDarkenFieldSubject = new Subject<bool>();
        /// キャラクターエフェクト操作時に操作内容が OnNext される Subject。
        readonly Subject<CharacterEffectEvent> _onCommandCharacterEffectSubject = new Subject<CharacterEffectEvent>();
        /// タイマー以外の Disposable を格納する CompositeDisposable。
        readonly CompositeDisposable _disposables = new CompositeDisposable();
        /// タイマー用 Disposable。
        IDisposable _timerDisposable;

        /// タイムアップまでの残り時間。
        public byte RemainingTime { get; private set; }
        // 残り時間経過許可フラグ true = 許可, false = 一時停止中
        bool _isRemainingTimer = true;


        /// サーバからの情報とウェーブ番号でインスタンス化。
        public Wave(BattleWave battleWave, byte no)
        {
            _battleWave = battleWave;
            No = no;
            Field = new WaveField(no);
        }

        /// 破棄する。
        public void Dispose()
        {
            _onCompletedSubjectsWithoutOnFinishBattle();
            _onFinishBattleSubject.OnCompleted();
            Field.Clear();
            _disposables.Dispose();
            if (_timerDisposable != null) _timerDisposable.Dispose();
        }
        /// バトル終了時に勝敗が OnNext される Subject 以外の Subject を全て終了させる。
        /// これ以降、（バトル終了時に紐付けられている処理以外の）外部で紐付けた各処理は実行されなくなる。
        void _onCompletedSubjectsWithoutOnFinishBattle()
        {
            _onCountDownSubject.OnCompleted();
            _onExitExclusiveActiveActionTimePerForceSubject.OnCompleted();
            _onOwnCharacterAddDamageSubject.OnCompleted();
            _onEnterActiveActionSubject.OnCompleted();
            _onExitDeadAnimationSubject.OnCompleted();
            _onCharacterDieSubject.OnCompleted();
            _onDarkenFieldSubject.OnCompleted();
            _onCommandCharacterEffectSubject.OnCompleted();
        }

        /// ウェーブバトル内でのカウントダウン（1秒毎）毎に残りウェーブバトル時間が OnNext される Observable を取得。
        public IObservable<byte> GetOnCountDownObservable() { return _onCountDownSubject.AsObservable(); }
        /// Force 毎の排他的なアクティブアクション時間が終了する際に、true が OnNext される Observable を取得。
        public IObservable<bool> GetOnExitExclusiveActiveActionTimePerForceObservable() { return _onExitExclusiveActiveActionTimePerForceSubject.AsObservable(); }
        /// 自キャラクターがダメージを与えた際に、与ダメージが OnNext される Observable を取得。
        public IObservable<int> GetOnOwnCharacterAddDamageObservable() { return _onOwnCharacterAddDamageSubject.AsObservable(); }
        /// キャラクターがアクティブアクション状態へ遷移する際に、遷移キャラクターが OnNext される Observable を取得。
        public IObservable<Character> GetOnEnterActiveActionObservable() { return _onEnterActiveActionSubject.AsObservable(); }
        /// キャラクター死亡演出終了時に死亡キャラクターが OnNext される Observable を取得。
        public IObservable<Character> GetOnExitDeadAnimationObservable() { return _onExitDeadAnimationSubject.AsObservable(); }
        /// バトル終了時に勝敗が OnNext される Observable を取得。
        public IObservable<VictoryOrDefeat> GetOnFinishBattleObservable() { return _onFinishBattleSubject.AsObservable(); }
        /// キャラクター死亡時に死亡キャラクターが OnNext される Observable を取得。
        public IObservable<Character> GetOnCharacterDieObservable() { return _onCharacterDieSubject.AsObservable(); }
        /// フィールド暗転時に true が、暗転解除時に false が OnNext される Observable を取得。
        public IObservable<bool> GetOnDarkenFieldObservable() { return _onDarkenFieldSubject.AsObservable(); }
        /// キャラクターエフェクト操作時に操作内容が OnNext される Observable を取得。
        public IObservable<CharacterEffectEvent> GetOnCommandCharacterEffectObservable() { return _onCommandCharacterEffectSubject.AsObservable(); }

        public Dictionary<Grid.Direction, Character> GetScreentEndCharacters()
        {
            return Field.GetScreentEndCharacters();
        }

        /// Wave を残り時間指定で開始する。
        public void Start(byte remainingTime)
        {
            // 残り時間セット。
            RemainingTime = remainingTime;
            // カウントダウン処理（初期時間更新の OnNext 付き）
            _onCountDownSubject.OnNext(RemainingTime);
            _timerDisposable = SceneContainer.GetTimeHandler().Interval(1).Where(_ => _isRemainingTimer).Subscribe(_ => _onTimer());

            // フィールド内に配置されている（生存）キャラクターから各 Observable を取得して処理を紐付ける。
            // そのまま該当 Subject を OnNext するものと、内部処理を呼ぶものが存在する。
            foreach (var character in Field.GetAllCharacters())
            {
                _subscribeCharacterObservables(character);
            }
            // ロジック開始
            foreach (var character in Field.GetAllCharacters())
            {
                character.Start();
            }
            // 開始時に発動するアクションがあれば発動
            foreach (var character in Field.GetAllCharacters())
            {
                character.TryToInvokeFirstAction();
            }
        }
        /// 分身キャラクターを追加する。
        public void AddShadowCharacter(ShadowCharacter shadow)
        {
            Field.SetShadowCharacter(shadow);
            shadow.SetViewToCorrectPosition();
            _subscribeCharacterObservables(shadow);
            shadow.Start();
            shadow.TryToInvokeFirstAction();
        }
        /// キャラクターから必要な Observable を取得して処理を紐付ける。
        void _subscribeCharacterObservables(Character character)
        {
            // 重複処理防止。
            if (_subscribedCharacterMap.ContainsKey(character)) return;
            _subscribedCharacterMap.Add(character, true);

            character.GetOnDieObservable()
            .Subscribe(_onCharacterDie).AddTo(_disposables);

            character.GetOnExitDeadAnimationObservable()
            .Subscribe(_onExitDeadAnimationSubject.OnNext).AddTo(_disposables);

            if (character.Force == Force.Own)
            {
                character.GetOnAddDamageObservable()
                .Subscribe(_onOwnCharacterAddDamageSubject.OnNext).AddTo(_disposables);
            }

            character.GetOnReserveTryingActiveActionObservable()
            .Subscribe(_onReserveTryingActiveAction).AddTo(_disposables);

            character.GetOnEnterActiveActionObservable()
            .SelectMany(c => _onEnterActiveAction(c))
            .Subscribe(_ => {}).AddTo(_disposables);

            character.GetOnCommandCharacterEffectObservable()
            .Subscribe(_onCommandCharacterEffectSubject.OnNext).AddTo(_disposables);
        }
        /// 毎秒処理。
        void _onTimer()
        {
            // 残り時間デクリメント。
            RemainingTime--;
            _onCountDownSubject.OnNext(RemainingTime);
            // タイムアップ時。
            if (_timeIsUp())
            {
                _finishBattle(VictoryOrDefeat.TimeUp);
            }
            else
            {
                // （UI のための）アクティブアクション実行可否チェックも定期的にやりたいのでここで実行。
                foreach (var character in Field.GetAllCharacters())
                {
                    character.CheckActiveActionAvailable();
                }
            }
        }
        /// キャラクター死亡時処理。
        void _onCharacterDie(Character character)
        {
            if (_timeStoppers.Contains(character))
            {
                _restartTime(character);
            }
            _onCharacterDieSubject.OnNext(character);
            // まずは死亡キャラクターをフィールドから取り除く。
            Field.RemoveCharacter(character);

            foreach (var executor in Field.GetAllCharacters())
            {
                executor.OtherDie((IActionTarget)character);
            }

            // 分身キャラクターはここまで。
            if (character.GetType() == typeof(ShadowCharacter)) return;

            // 勝敗判定
            _judge();
        }

        /// アクティブアクション予約時処理。
        void _onReserveTryingActiveAction(Character character)
        {
            // キュー選択。
            var onTryActiveActionSubjects = _getOnTryActiveActionSubjects(character.Force);

            // OnNext されたらアクティブアクション発動を試みる Subject を用意し、キューに詰める。
            var onTryActiveActionSubject = new Subject<bool>();
            onTryActiveActionSubject.AsObservable().Subscribe(_ =>
            {
                if (!character.TryToTransitToActiveAction())
                {
                    // 発動できなければ次へ。
                    _tryNextActiveAction(character.Force);
                }
            })
            .AddTo(_disposables);
            onTryActiveActionSubjects.Enqueue(onTryActiveActionSubject);

            // キューに詰めた結果、中身が一個なのであれば、
            // アクティブアクション時間を占有しているキャラクターがいないということなので OnNext して発動試行へ。
            if (onTryActiveActionSubjects.Count == 1)
            {
                onTryActiveActionSubject.OnNext(true);
                onTryActiveActionSubject.OnCompleted();
            }
        }
        /// アクティブアクション発動時処理。
        IObservable<Character> _onEnterActiveAction(Character character)
        {
            _onEnterActiveActionSubject.OnNext(character);
            // 自キャラクターによる発動であれば、発動キャラクター以外は非表示。
            if (character.Force == Force.Own)
            {
                foreach (var fieldCharacter in Field.GetAllCharacters())
                {
                    if (fieldCharacter == character) continue;
                    fieldCharacter.PauseOn();
                    fieldCharacter.SetVisible(false, false);
                }
            }
            // 排他的なアクティブアクションからの離脱試行処理を予約する。
            _reserveTryingToExitExclusiveActiveActionTime(character);
            // 時間停止。
            return _stopTime(character, character.GetOnExitTimeStopObservable());
        }
        /// 排他的なアクティブアクションからの離脱試行処理を予約する。
        void _reserveTryingToExitExclusiveActiveActionTime(Character character)
        {
            SceneContainer.GetTimeHandler().Timer(1.0f)
            .Subscribe(_ =>
            {
                // 自キャラクターによる発動であれば非表示キャラクターを再表示。
                if (character.Force == Force.Own)
                {
                    foreach (var fieldCharacter in Field.GetAllCharacters())
                    {
                        if (fieldCharacter == character) continue; 
                        fieldCharacter.PauseOff();
                        fieldCharacter.SetVisible(true, false);
                    }
                }
                // 次のアクティブアクション試行処理。
                _tryNextActiveAction(character.Force);
            });
        }
        /// 次のアクティブアクション発動を試行する。
        void _tryNextActiveAction(Force force)
        {
            // キュー選択。
            var onTryActiveActionSubjects = _getOnTryActiveActionSubjects(force);
            // 占有する必要がなくなった発動中 Subject はキューから削除。
            onTryActiveActionSubjects.Dequeue();
            // キューの中身があれば先頭の Subject を OnNext して発動試行へ。
            if (onTryActiveActionSubjects.Count > 0)
            {
                var onTryActiveActionSubject = onTryActiveActionSubjects.Peek();
                onTryActiveActionSubject.OnNext(true);
                onTryActiveActionSubject.OnCompleted();
            }
            // キューの中身が空であれば排他的なアクティブアクション時間は終了。
            else
            {
                _onExitExclusiveActiveActionTimePerForceSubject.OnNext(true);
            }
        }
        /// アクティブアクション試行キュー取得。
        Queue<Subject<bool>> _getOnTryActiveActionSubjects(Force force)
        {
            return force == Force.Own ? _onTryOwnActiveActionSubjects : _onTryEnemyActiveActionSubjects;
        }

        /// ウェーブバトル終了時処理。
        void _finishBattle(VictoryOrDefeat victoryOrDefeat)
        {
            foreach (var timeStopper in new List<Character>(_timeStoppers))
            {
                _restartTime(timeStopper);
            }
            // Attribute を外し、分身も消す。
            SceneContainer.GetCharacterManager().RemoveAttributeAndShadow();
            // バトル終了時処理以外の外部処理が走らないようにする。
            _onCompletedSubjectsWithoutOnFinishBattle();
            // 内部処理も走らないようにする。
            _disposables.Dispose();
            // タイマーを止める。
            _timerDisposable.Dispose();
            // 外部処理のために勝敗を OnNext する。
            _onFinishBattleSubject.OnNext(victoryOrDefeat);
            _onFinishBattleSubject.OnCompleted();
        }

        /// 勝敗判定。
        void _judge()
        {
            if (_hasWonCompletely())
            {
                _finishBattle(VictoryOrDefeat.Win);
            }
            else if (_hasLostCompletely())
            {
                _finishBattle(VictoryOrDefeat.Loss);
            }
            else if (_hasDrawn())
            {
                _finishBattle(VictoryOrDefeat.Draw);
            }
        }
        /// 完全勝利。
        bool _hasWonCompletely()
        {
            var characterManager = SceneContainer.GetCharacterManager();
            return characterManager.CurrentOwnCharacterSet.AreAlive() && !characterManager.CurrentEnemyCharacterSet.AreAlive();
        }
        /// 完全敗北。
        bool _hasLostCompletely()
        {
            var characterManager = SceneContainer.GetCharacterManager();
            return !characterManager.CurrentOwnCharacterSet.AreAlive() && characterManager.CurrentEnemyCharacterSet.AreAlive();
        }
        /// 引き分け
        bool _hasDrawn()
        {
            var characterManager = SceneContainer.GetCharacterManager();
            return !characterManager.CurrentOwnCharacterSet.AreAlive() && !characterManager.CurrentEnemyCharacterSet.AreAlive();
        }
        /// 時間切れ
        bool _timeIsUp()
        {
            var characterManager = SceneContainer.GetCharacterManager();
            return RemainingTime <= 0 && characterManager.CurrentOwnCharacterSet.AreAlive() && characterManager.CurrentEnemyCharacterSet.AreAlive();
        }

        /// キャラクターを初期位置に配置する。
        public void SetCharactersToInitialPosition()
        {
            var characterManager = SceneContainer.GetCharacterManager();
            var aliveOwnCharacters = characterManager.GetCurrentOwnCharacters().Where(c => !c.IsDead);
            var aliveEnemyCharacters = characterManager.GetCurrentEnemyCharacters().Where(c => !c.IsDead);
            _setCharactersToInitialGrid(aliveOwnCharacters, aliveEnemyCharacters);
            foreach (var character in aliveOwnCharacters)
            {
                character.SetViewToCorrectPosition();
            }
            foreach (var character in aliveEnemyCharacters)
            {
                character.SetViewToCorrectPosition();
            }
        }
        /// キャラクターを初期位置に配置する。自キャラクターは歩き、もしくは走りアニメーションで該当位置へ移動する。
        public IObservable<Wave> SetCharactersToInitialPositionWithAnimation(float duration, bool isWalking = false)
        {
            var aliveOwnCharacters = SceneContainer.GetCharacterManager().GetCurrentOwnCharacters().Where(c => !c.IsDead);
            var aliveEnemyCharacters = SceneContainer.GetCharacterManager().GetCurrentEnemyCharacters().Where(c => !c.IsDead);
            _setCharactersToInitialGrid(aliveOwnCharacters, aliveEnemyCharacters);
            foreach (var character in aliveEnemyCharacters)
            {
                character.SetViewToCorrectPosition();
            }
            return isWalking ? Field.SetCharactersToCorrectPositionWithWalking(aliveOwnCharacters, duration).Select(_ => this) : Field.SetCharactersToCorrectPositionWithRunnging(aliveOwnCharacters, duration).Select(_ => this);
        }
        public IObservable<Wave> RunOwnCharactersToCorrectPosition(float duration)
        {
            var aliveOwnCharacters = SceneContainer.GetCharacterManager().GetCurrentOwnCharacters().Where(c => !c.IsDead);
            return Field.SetCharactersToCorrectPositionWithRunnging(aliveOwnCharacters, duration).Select(_ => this);
        }

        void _setCharactersToInitialGrid(IEnumerable<Character> ownCharacters, IEnumerable<Character> enemyCharacters)
        {
            Field.SetOwnCharactersToInitialGrid(ownCharacters);
            Field.SetEnemyCharactersToInitialGrid(enemyCharacters);
        }

        // 一時停止(アクティブアクション以外で)
        public void StopTimer()
        {
            _isRemainingTimer = false;
        }
        // 一時停止解除(アクティブアクション以外で)
        public void RestartTimer()
        {
            _isRemainingTimer = true;
        }

        /// 時間を停止する。
        IObservable<Character> _stopTime(Character timeStopper, IObservable<Character> onExitTimeStop)
        {
            // 最初の時間停止の場合、
            if (_timeStoppers.Count() == 0)
            {
                // 暗くして、
                _onDarkenFieldSubject.OnNext(true);
                // 他キャラクターを止める。
                foreach (var character in Field.GetAllCharacters().Where(c => c != timeStopper))
                {
                    character.PauseOn();
                    _timeStoppings.Add(character);
                }
            }
            // 他に時間停止を行っているキャラクターがいる場合、
            else
            {
                // 時間停止実行キャラクターは強制時間停止解除。
                _timeStoppings.Remove(timeStopper);
                timeStopper.PauseOff();
            }
            _timeStoppers.Add(timeStopper);
            return onExitTimeStop.Do(_restartTime);
        }
        /// 時間を再び動かす。
        void _restartTime(Character timeStopper)
        {
            _timeStoppers.Remove(timeStopper);

            // まだ他に時間停止を行っているキャラクターがいる場合、
            if (_timeStoppers.Count() > 0)
            {
                // 動く権利がなくなったので停止する。
                timeStopper.PauseOn();
                _timeStoppings.Add(timeStopper);
            }
            // 該当キャラクターしか時間停止を行っていない場合、
            else
            {
                // 他キャラクターの停止を解除し、
                foreach (var character in _timeStoppings)
                {
                    character.PauseOff();
                }
                _timeStoppings.Clear();
                // 明るくする。
                _onDarkenFieldSubject.OnNext(false);
            }
        }

        public void LogicUpdate()
        {
            foreach (var character in Field.GetAllCharacters())
            {
                character.LogicUpdate();
            }
        }
    }
}
