﻿using UniLinq;
using UniRx;
using Noroshi.Core.WebApi.Response.Battle;
using Noroshi.Core.Game.Battle;

namespace Noroshi.BattleScene
{
    public class PlayerBattleManager : AbstractBattleManager
    {
        public PlayerBattleManager(BattleCategory battleCategory, uint battleContentId, uint[] ownPlayerCharacterIds, uint? rentalPlayerCharacterId, uint paymentNum) : base(battleCategory, battleContentId, ownPlayerCharacterIds, rentalPlayerCharacterId, paymentNum)
        {
        }

        public ICharacterView OwnTopCharacterViewForUI { get; private set; }
        public ICharacterView EnemyTopCharacterViewForUI { get; private set; }
        public byte OwnTopCharacterSkinLevel { get; private set; }
        public byte EnemyTopCharacterSkinLevel { get; private set; }

        public override IObservable<IManager> LoadDatas()
        {
            // バトル開始用データをロード
            return _requestBattleStartWebAPI()
            .Do(response =>
            {
                _onLoadDatas(response, response.OwnCharacters);
                SceneContainer.GetCharacterManager().SetCurrentEnemyPlayerCharacterSets(response.Waves.Select(w => w.BattleCharacters.Where(c => c != null)));
            })
            .Select(_ => (IManager)this);
        }

        public override IObservable<IManager> LoadAssets(IFactory factory)
        {
            var characterManager = SceneContainer.GetCharacterManager();
            // 表示キャラクターの選出方法
            // １、高レベル
            // ２、同一レベルがいた場合より進化レベルが高い
            // ３、同一進化レベルの場合は先頭にいるキャラクター
            // とする
            var ownTopCharacter = characterManager.GetCurrentOwnCharacters().OrderByDescending(c => c.Level).ThenByDescending(c => c.EvolutionLevel).FirstOrDefault();
            var enemyTopCharacter = characterManager.GetCurrentEnemyCharacters().OrderByDescending(c => c.Level).ThenByDescending(c => c.EvolutionLevel).FirstOrDefault();
            OwnTopCharacterSkinLevel = ownTopCharacter.SkinLevel;
            EnemyTopCharacterSkinLevel = enemyTopCharacter.SkinLevel;
            return base.LoadAssets(factory)
            .SelectMany(_ => 
            {
                // 自キャラの１体を読み込む
                return factory.BuildCharacterViewForUI(ownTopCharacter.CharacterID).Do(v => OwnTopCharacterViewForUI = v);
            })
            .SelectMany(_ => 
            {
                // 対戦相手キャラの１体を読み込む
                return factory.BuildCharacterViewForUI(enemyTopCharacter.CharacterID).Do(v => EnemyTopCharacterViewForUI = v);
            })
            .Select(_ => (IManager)this);
        }

        public override bool ShouldWaitToFinishReady(){ return true; }
    }
}
