using System.Collections.Generic;
using UniRx;
using Noroshi.Core.Game.Battle;
using Noroshi.Core.Game.Character;
using Noroshi.BattleScene.Drop;
using Noroshi.BattleScene.CharacterEffect;
using Noroshi.BattleScene.Sound;

namespace Noroshi.BattleScene
{
    public interface IBattleManager : IManager
    {
        BattleCategory BattleCategory { get; }
        bool LoopBattle { get; }
        bool UnlimitedHPEnemyBattle { get; }
        /// 現在の Wave 番号
        byte CurrentWaveNo { get; }

        DropHandler DropHandler { get; }
        BattleResult BattleResult { get; }

        BattleAutoMode BattleAutoMode { get; }

        IObservable<byte> GetOnCountDownObservable();
        IObservable<bool> GetOnToggleNextWaveButtonVisibleObservable();
        IObservable<byte> GetOnCompletePrepareNextWaveObservable();
       /// バトル終了時にプッシュされる Observable を取得
        IObservable<VictoryOrDefeat> GetOnFinishBattleObservable();
        IObservable<CharacterEffectEvent> GetOnCommandCharacterEffectObservable();
        IObservable<SoundEvent> GetOnCommandSoundObservable();
        IObservable<float> GetOnEnterSlowTimeObservable();
        IObservable<byte> GetOnEnterWaveBattleObservable();
        IObservable<Noroshi.BattleScene.Camera.CameraEvent> GetOnCommandCameraObservable();
        IObservable<uint> GetOnOwnCharacterAddDamageObservable();
        IObservable<ushort> GetOnDefeatingNumEnemyCharacterObservable();
        IObservable<Character> GetOnExitDeadAnimationObservable();
        IObservable<bool> GetOnExitExclusiveActiveActionTimePerForceObservable();
        IObservable<Character> GetOnEnterActiveActionObservable();
        /// 最後のキャラクターアニメーション演出時間が終了する際に OnNext される Observable を取得。
        IObservable<VictoryOrDefeat> GetOnExitLastCharacterAnimationTimeObservable();
        Dictionary<Grid.Direction, Character> GetScreentEndCharacters();

        /// 最後の Wave 番号
        int WaveNum { get; }
        /// 現在の Wave
        Wave CurrentWave { get; }

        IObservable<VictoryOrDefeat> Start();

        void StartWave();

        void TryToTransitToNextWave();

        IObservable<IBattleManager> StartInterval();
        IObservable<Wave> SwitchWave();
        float GetSwitchWaveTime();

        uint[] GetBattleBgmSoundIDs();

        IObservable<IBattleManager> RunOwnCharactersToCorrectPosition();

        void AddShadowCharacter(ShadowCharacter shadow);

        void SetAutoMode(bool isOn);

        /// Ready 状態の終了を待つべきかどうか判定。
        bool ShouldWaitToFinishReady();

        void LogicUpdate();
        Noroshi.Core.WebApi.Response.Battle.AdditionalInformation AdditionalInformation{ get; }

        void AdjustFieldAfterSwitchWave();
        void EnterFieldSwitchWave();
        void EnterFieldSwitchWave(byte waveNo, float duration);
        void SetForegroundFieldVisible(bool isVisible);
    }
}
