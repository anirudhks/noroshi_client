﻿using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Vector2 = UnityEngine.Vector2;
using Noroshi.Core.Game.Character;
using Noroshi.Grid;
using Noroshi.BattleScene.Actions;

namespace Noroshi.BattleScene
{
    /// フィールドにおける各キャラクターの位置情報（グリッド座標系、ワールド座標系）を扱うクラス。
    /// あくまでも位置情報を扱うだけなので内包するキャラクター自体のライフサイクル管理は関知しない。
    /// 内包するキャラクターの View 移動も担当するが、アニメーションなどは扱わない。あくまでもグリッド座標系に紐付くワールド座標系の位置移動のみ。
    /// （一時的に）利用が終了したら Clear して内容するキャラクターへの参照を切っておくこと。
    public class WaveField : IActionTargetFinder
    {
         /// 水平方向のフィールドサイズ（ワールド座標系）。
        public const float HORIZONTAL_LENGTH = 12.80f;
        /// 垂直方向のフィールドサイズ（ワールド座標系）。
        const float VERTICAL_LENGTH   =  2.4f;
        /// 垂直方向のフィールド座標（ワールド座標系）。
        const float VERTICAL_POSITION =  -1.0f;
        /// 同一水平方向グリッド位置であっても垂直方向グリッドが異なれば水平ワールド座標系をずらす。そのずらし幅。
        const float HORIZONTAL_LENGTH_DIFF = 0.01f;

        /// 水平方向のフィールド座標（ワールド座標系）を取得する。
        public static float GetPositionX(byte waveNo)
        {
            return HORIZONTAL_LENGTH * (waveNo - 1);
        }

        /// ウェーブ番号。
        readonly byte _waveNo;
        /// グリッド座標系のコンテナ。
        readonly GridContainer<Character> _gridContainer;
        /// グリッド座標計算機。
        readonly GridPositionCalculator _gridPositionCalculator = new GridPositionCalculator();

        /// ウェーブ番号指定でインスタンス化。
        public WaveField(byte waveNo)
        {
            _waveNo = waveNo;
            _gridContainer = new GridContainer<Character>(Constant.FIELD_HORIZONTAL_GRID_SIZE, _gridPositionCalculator.GetVerticalSize());
        }
        /// 内包物クリア（Disponse は不要）。
        public void Clear()
        {
            _gridContainer.RemoveAllContents();
        }

        /// 水平方向のグリッドサイズ。
        public ushort HorizontalSize { get { return _gridContainer.HorizontalSize; } }
        /// 垂直方向のグリッドサイズ。
        public ushort VerticalSize { get { return _gridContainer.VerticalSize  ; } }

        /// キャラクターを配置する。
        public void SetCharacter(ushort h, ushort v, Character character)
        {
            _gridContainer.SetContent(h, v, character);
        }
        /// キャラクターを移動する。
        public void MoveCharacter(Character character, GridPosition gridPosition)
        {
            _gridContainer.MoveTo(character, gridPosition.HorizontalIndex, gridPosition.VerticalIndex);
        }
        /// 方向指定でキャラクターを移動する。
        public void MoveCharacter(Character character, Direction direction)
        {
            _gridContainer.MoveTo(character, direction);
        }
        /// キャラクターを View とともに開始位置へ移動する。
        public void MoveCharacterToAvailableStartPositionWithView(Character character, ICharacterView characterView)
        {
            MoveCharacter(character, new GridPosition(GetAvailableStartHorizontalIndex(character.Force), character.GetGridPosition().Value.VerticalIndex));
            characterView.SetPosition(GetPosition(character.GetGridPosition().Value));
        }
        /// View 移動を伴いつつ、キャラクターを水平方法へ徐々に移動する。
        public IObservable<GridPosition> MoveCharacterHorizontallyWithView(Character character, ICharacterView characterView, Direction direction, short horizontalDiff, float duration, bool inVisibleArea)
        {
            var sign = horizontalDiff / Math.Abs(horizontalDiff);
            var positions = _getViewPositionsToMove(character, direction, horizontalDiff, inVisibleArea);
            return characterView.MoveWithOnNextBasedXPosition(positions, duration)
            .Select(_ =>
            {
                var nextGridPosition = character.GetGridPosition().Value.BuildNextGrid(direction, sign);
                MoveCharacter(character, nextGridPosition);
                return nextGridPosition;
            });
        }
        /// View 移動を伴いつつ、キャラクターを水平方法へ徐々に移動する。移動差分は毎回計算する方式。
        public IObservable<GridPosition> MoveCharacterHorizontallyWithView(Character character, ICharacterView characterView, Direction direction, Func<short> getHorizontalDiff, float duration, bool inVisibleArea)
        {
            var startGridPosition = character.GetGridPosition().Value;
            var horizontalDiff = getHorizontalDiff();
            var sign = horizontalDiff / Math.Abs(horizontalDiff);
            var positions = _getViewPositionsToMove(character, direction, horizontalDiff, inVisibleArea);
            return characterView.MoveWithOnNextBasedXPosition(positions, duration)
            .Select(_ =>
            {
                var currentGridPosition = character.GetGridPosition().Value;
                var canMove = direction == Direction.Right
                    ? currentGridPosition.HorizontalIndex < startGridPosition.BuildNextGrid(direction, horizontalDiff).HorizontalIndex
                    : currentGridPosition.HorizontalIndex > startGridPosition.BuildNextGrid(direction, horizontalDiff).HorizontalIndex;
                if (!canMove)
                {
                    characterView.StopMove();
                    return currentGridPosition;
                }
                var nextGridPosition = character.GetGridPosition().Value.BuildNextGrid(direction, sign);
                MoveCharacter(character, nextGridPosition);
                return nextGridPosition;
            });
        }
        GridPosition _correctGridPositionInVisibleArea(GridPosition gridPosition)
        {
            var horizontalIndex = gridPosition.HorizontalIndex;
            if (gridPosition.HorizontalIndex < _getMinVisibleHorizontalIndex())
            {
                horizontalIndex = _getMinVisibleHorizontalIndex();
            }
            else if (_getMaxVisibleHorizontalIndex() < gridPosition.HorizontalIndex)
            {
                horizontalIndex = _getMaxVisibleHorizontalIndex();
            }
            return new GridPosition(horizontalIndex, gridPosition.VerticalIndex);
        }
        /// View 移動を伴いつつ、キャラクターを水平方法の終端へ徐々に移動する。
        public IObservable<GridPosition> MoveCharacterToHorizontalEndWithView(Character character, ICharacterView characterView, Direction direction, float duration)
        {
            var horizontalDiff = (short)Math.Abs(GetEndHorizontalIndex(direction) - character.GetGridPosition().Value.HorizontalIndex);
            return MoveCharacterHorizontallyWithView(character, characterView, direction, horizontalDiff, duration, false);
        }
        Vector2[] _getViewPositionsToMove(Character character, Direction direction, short horizontalDiff, bool inVisibleArea)
        {
            var horizontalDiffAbs = Math.Abs(horizontalDiff);
            var sign = _getHorizontalSign(direction, horizontalDiff);
            var initialGridPosition = character.GetGridPosition().Value;
            var lastGridPosition = new GridPosition((ushort)(initialGridPosition.HorizontalIndex + horizontalDiffAbs * sign), initialGridPosition.VerticalIndex);
            if (inVisibleArea) lastGridPosition = _correctGridPositionInVisibleArea(lastGridPosition);
            var size = Math.Abs(lastGridPosition.HorizontalIndex - initialGridPosition.HorizontalIndex);
            var positions = new Vector2[size];
            for (var h = 1; h <= size; h++)
            {
                positions[h - 1] = GetPosition(new GridPosition((ushort)(initialGridPosition.HorizontalIndex + h * sign), initialGridPosition.VerticalIndex));
            }
            // １つ以上の座標が格納されていればそのまま、すでに画面端におり移動する必要がない場合は現在値を返す
            return positions.Length > 0 ? positions : new Vector2[] { GetPosition(initialGridPosition) };
        }
        int _getHorizontalSign(Direction direction, short horizontalDiff)
        {
            var horizontalDiffAbs = Math.Abs(horizontalDiff);
            horizontalDiff = direction == Direction.Right ? horizontalDiff : (short)-horizontalDiff;
            return horizontalDiff / horizontalDiffAbs;            
        }

        /// キャラクターを取り除く。
        public void RemoveCharacter(Character character)
        {
            _gridContainer.RemoveContent(character);
        }
        /// 配置されているキャラクターを全て取得する。
        public List<Character> GetAllCharacters()
        {
            return _gridContainer.GetContents();
        }

        /// 対象キャラクターを前に進めるべきかどうかを判定する。
        /// 進行方向 1 ~ 最大基本アクション レンジ内に敵がいなければ進むべきと判定。
        public bool ShouldMoveForward(Character character, Direction proceedDirection, ushort baseActionRange)
        {
            // 見えない位置対策。
            if (character.GetGridPosition().Value.HorizontalIndex < _getMinVisibleHorizontalIndex())
            {
                if (proceedDirection == Direction.Right) return true;
            }
            if (_getMaxVisibleHorizontalIndex() < character.GetGridPosition().Value.HorizontalIndex)
            {
                if (proceedDirection == Direction.Left) return true;
            }
            return !GetTargetsWithHorizontalRange(character.GetGridPosition().Value, proceedDirection, 1, baseActionRange)
                .Any(target => target.Force != character.CurrentForce);
        }
        /// 変更すべき進行方向があれば取得する。null だと現在の方向のままで問題ないという判定。
        public Direction? GetProceedDirectionToChange(Character character)
        {
            // 見えない位置対策。
            if (character.GetGridPosition().Value.HorizontalIndex < _getMinVisibleHorizontalIndex())
            {
                return character.GetDirection() != Direction.Right ? (Direction?)Direction.Right : null;
            }
            if (_getMaxVisibleHorizontalIndex() < character.GetGridPosition().Value.HorizontalIndex)
            {
                return character.GetDirection() != Direction.Left ? (Direction?)Direction.Left : null;
            }
            // 距離が近い敵キャラクターを選出。
            var target = GetAllCharacters()
                .Where(c => c.Force != character.CurrentForce && c != character)
                .OrderBy(c => _gridContainer.GetHorizontalDistance(character, c))
                .FirstOrDefault();
            if (target == null) return null;
            var direction = _gridContainer.GetHorizontalDirection(character, target);
            if (direction.HasValue && direction.Value != character.GetDirection())
            {
                return direction.Value;
            }
            return null;
        }

        /// 対象実行者をターゲットとして解釈する。
        /// IActionTargetFinder 実装メソッド。
        public IActionTarget GetExecutorAsTarget(IActionExecutor executor)
        {
            return (Character)executor;
        }
        /// 全てのアクション対象を取得する。
        /// IActionTargetFinder 実装メソッド。
        public IEnumerable<IActionTarget> GetAllTargets()
        {
            return GetAllCharacters().Where(at => at.IsTargetable).Cast<IActionTarget>();
        }
        /// レンジ指定でアクション対象を取得する。
        /// IActionTargetFinder 実装メソッド。
        public IEnumerable<IActionTarget> GetTargetsWithHorizontalRange(GridPosition baseGridPosition, Direction horizontalDirection, int minRange, int maxRange)
        {
            return _gridContainer.GetContentsByHorizontalRange(baseGridPosition.HorizontalIndex, horizontalDirection, minRange, maxRange).Select(c => (IActionTarget)c);
        }
        // 分身をターゲットとして取得する。
        /// IActionTargetFinder 実装メソッド。
        public IActionTarget GetShadowAsTarget(IShadow shadow)
        {
            return (Character)shadow;
        }

        /// 自キャラクターを初期配置する。
        public void SetOwnCharactersToInitialGrid(IEnumerable<Character> characters)
        {
            _setCharactersToInitialGrid(
                characters,
                (character, no, characterNum) =>
                {
                    if (character.IsFirstHidden) return 0;
                    return (ushort)(_getMinVisibleHorizontalIndex() - character.GetBaseActionRange() + (characterNum - no) + 24);
                },
                _gridPositionCalculator.GetOwnCharacterVerticalIndex
            );

        }
        /// 敵キャラクターを初期配置する。
        public void SetEnemyCharactersToInitialGrid(IEnumerable<Character> characters)
        {
            _setCharactersToInitialGrid(
                characters,
                (character, no, characterNum) =>
                {
                    if (character.IsFirstHidden) return (ushort)(HorizontalSize - 1);
                    return (ushort)(_getMaxVisibleHorizontalIndex() + character.GetBaseActionRange() - (characterNum - no) - 24);
                },
                _gridPositionCalculator.GetEnemyCharacterVerticalIndex
            );
        }
        public void _setCharactersToInitialGrid(IEnumerable<Character> characters, Func<Character, byte, byte, ushort> getHorizontalIndex, Func<byte, byte, byte, ushort> getVerticalIndex)
        {
            var sortedCharacters = characters.OrderBy(c => (int)c.CharacterPosition).ThenBy(c => c.OrderInLayer).ToArray();
            var characterPositionToCountMap = new Dictionary<CharacterPosition, byte>();
            var characterPositionToTotalCountMap = sortedCharacters
                .ToLookup(character => character.CharacterPosition)
                .ToDictionary(group => group.Key, group => (byte)group.Count());
            var characterNum = (byte)sortedCharacters.Count();
            for (byte no = 1; no <= characterNum; no++)
            {
                var character = sortedCharacters[no - 1];
                if (!characterPositionToCountMap.ContainsKey(character.CharacterPosition))
                {
                    characterPositionToCountMap.Add(character.CharacterPosition, 0);
                }
                characterPositionToCountMap[character.CharacterPosition]++;
                var h = getHorizontalIndex(character, no, characterNum);
                var v = getVerticalIndex(characterPositionToCountMap[character.CharacterPosition], (byte)(no - characterPositionToCountMap[character.CharacterPosition]), characterPositionToTotalCountMap[character.CharacterPosition]);
                SetCharacter(h, v, character);
            }
        }
        /// 分身キャラクターを配置する。
        public void SetShadowCharacter(ShadowCharacter character)
        {
            byte? blankVerticalIndex = null;
            foreach (var v in _gridPositionCalculator.GetShadowCharacterVerticalIndexes(character.Force, character.BaseVerticalIndex))
            {
                if (!_gridContainer.HasContentByVerticalIndex(v))
                {
                    blankVerticalIndex = (byte)v;
                    break;
                }
            }
            if (blankVerticalIndex.HasValue)
            {
                SetCharacter(character.InitialHorizontalIndex, blankVerticalIndex.Value, character);
            }
        }

        /// キャラクターを歩きアニメーションで正しい位置（ワールド座標系）へ移動させる。事前に正しいグリッド座標に設置しておく必要あり。
        public IObservable<bool> SetCharactersToCorrectPositionWithWalking(IEnumerable<Character> characters, float duration)
        {
            return Observable.WhenAll(characters.Select(c => c.SetViewToCorrectPositionWithWalking(duration))).Select(_ => true);
        }
        /// キャラクターを走りアニメーションで正しい位置（ワールド座標系）へ移動させる。事前に正しいグリッド座標に設置しておく必要あり。
        public IObservable<bool> SetCharactersToCorrectPositionWithRunnging(IEnumerable<Character> characters, float duration)
        {
            return Observable.WhenAll(characters.Select(c => c.SetViewToCorrectPositionWithRunning(duration))).Select(_ => true);
        }

        /// グリッド座標から対応するワールド座標を取得する。
        public Vector2 GetPosition(Noroshi.Grid.GridPosition gridPosition)
        {
            return _getPosition(_waveNo, gridPosition);
        }
        /// グリッド座標からストーリ専用初期ウェーブのワールド座標を取得する。
        /// 仮想的な 0 ウェーブ位置を基準にやや調整。
        public Vector2 GetStoryWavePosition(Noroshi.Grid.GridPosition grid)
        {
            return _getPosition(0, grid) + Vector2.right * HORIZONTAL_LENGTH / 2;
        }
        Vector2 _getPosition(byte waveNo, Noroshi.Grid.GridPosition gridPosition)
        {
            var x = HORIZONTAL_LENGTH * 2 * (gridPosition.HorizontalIndex - (HorizontalSize - 1) / 2f) / HorizontalSize + HORIZONTAL_LENGTH * (waveNo - 1);
            var y = VERTICAL_LENGTH   * (gridPosition.VerticalIndex   - (VerticalSize   - 1) / 2f) / VerticalSize + VERTICAL_POSITION;
            // 被らないようにちょっとだけずらす
            x += HORIZONTAL_LENGTH_DIFF * (gridPosition.VerticalIndex - VerticalSize);
            return new Vector2(x, y);
        }

        /// 現Waveで表示されているキャラクターに対しもっとも画面端(左右)にいるキャラクターViewを保持する(最大４対)
        public Dictionary<Direction, Character> GetScreentEndCharacters()
        {
            var screenEndCharacters = new Dictionary<Direction, Character>();
            // 生存キャラクターを取得
            var characters = GetAllCharacters().Where(c => !c.IsDead).ToArray();
            // 左
            var leftPos = 0.0f;
            // 上
            var upPos = 0.0f;
            // 右
            var rightPos = 0.0f;
            // 下
            var bottomPos = 0.0f;
            Character leftPositionCharacter = null;
            Character upPositionCharacter = null;
            Character bottomPositionCharacter = null;
            Character rightPositionCharacter = null;
            if (characters.Count() > 0)
            {
                for(var i = 0; i < characters.Length; i++)
                {
                    var worldPositions = ((MonoBehaviours.CharacterView)characters[i].GetView()).GetDamageAreaWorldPositions();
                    if (i == 0)
                    {
                        // 最初のキャラクターを基準とする
                        leftPos   = worldPositions.Min(v => v.x);
                        upPos     = worldPositions.Max(v => v.y);
                        rightPos  = worldPositions.Max(v => v.x);
                        bottomPos = worldPositions.Min(v => v.y);
                        leftPositionCharacter = upPositionCharacter = 
                            rightPositionCharacter = bottomPositionCharacter = characters[i];
                    }
                    else
                    {
                        // それぞれ最小値最大値を更新と同時に更新した座標を持つICharacterViewも更新
                        var nowLeftPos   = System.Math.Min(leftPos, worldPositions.Min(v => v.x));
                        var nowUpPos     = System.Math.Max(upPos, worldPositions.Max(v => v.y));
                        var nowRightPos  = System.Math.Max(rightPos, worldPositions.Max(v => v.x));
                        var nowBottomPos = System.Math.Min(bottomPos, worldPositions.Min(v => v.y));

                        if (leftPos > nowLeftPos)
                        {
                            leftPositionCharacter = characters[i];
                            leftPos = nowLeftPos;
                        }
                        if (upPos < nowUpPos)
                        {
                            upPositionCharacter = characters[i];
                            upPos = nowUpPos;
                        }
                        if (rightPos < nowRightPos)
                        {
                            rightPositionCharacter = characters[i];
                            rightPos = nowRightPos;
                        }
                        if (bottomPos > nowBottomPos)
                        {
                            bottomPositionCharacter = characters[i];
                            bottomPos = nowBottomPos;
                        }
                    }
                }

                screenEndCharacters.Add(Direction.Left, leftPositionCharacter);
                screenEndCharacters.Add(Direction.Up, upPositionCharacter);
                screenEndCharacters.Add(Direction.Right, rightPositionCharacter);
                screenEndCharacters.Add(Direction.Down, bottomPositionCharacter);
            }
            return screenEndCharacters;
        }

        /// 出現時などに利用する利用可能な開始水平方向グリッド座標を取得する。
        public ushort GetAvailableStartHorizontalIndex(Force force)
        {
            return force == Force.Own ? _getMinVisibleHorizontalIndex() : _getMaxVisibleHorizontalIndex();
        }
        /// 終端の水平方向グリッド座標を取得する。
        public ushort GetEndHorizontalIndex(Direction direction)
        {
            return (ushort)(direction == Direction.Right ? HorizontalSize - 1 : 0);
        }
        /// 見えているグリッド座標かチェック。
        public bool IsVisible(GridPosition gridPosition)
        {
            return _getMinVisibleHorizontalIndex() <= gridPosition.HorizontalIndex && gridPosition.HorizontalIndex <= _getMaxVisibleHorizontalIndex();
        }
        ushort _getMinVisibleHorizontalIndex()
        {
            return (ushort)((HorizontalSize - Constant.VISIBLE_FIELD_HORIZONTAL_GRID_SIZE) / 2 + 1);
        }
        ushort _getMaxVisibleHorizontalIndex()
        {
            return (ushort)(HorizontalSize - (HorizontalSize - Constant.VISIBLE_FIELD_HORIZONTAL_GRID_SIZE) / 2);
        }
        /// 前方向を取得する。
        public static Direction GetForwardDirection(Force force)
        {
            Direction direction;
            if (force == Force.Own)
            {
                direction = Direction.Right;
            }
            else if (force == Force.Enemy)
            {
                direction = Direction.Left;
            }
            else
            {
                throw new Exception();
            }
            return direction;
        }
    }
}
