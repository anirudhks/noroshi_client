﻿using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Flaggs.StateTransition;
using Noroshi.Core.Game.Battle;
using Noroshi.Core.Game.Character;
using Noroshi.Character;
using Noroshi.BattleScene.CharacterEffect;
using Noroshi.BattleScene.Actions;

namespace Noroshi.BattleScene.CharacterState
{
    /// <summary>
    /// キャラクター状態を扱うクラス。
    /// </summary>
    public class CharacterStateHandler
    {
        /// ルート状態内の状態遷移マップ。
        static readonly Dictionary<Type, Type[]> CHARACTER_STATE_MAP = new Dictionary<Type, Type[]>
        {
            {typeof(AliveState), new[]{typeof(DeadState), typeof(WinState)}},
        };
        /// 生存状態内の状態遷移マップ。ルート状態 > 生存状態。
        static readonly Dictionary<Type, Type[]> ALIVE_STATE_MAP = new Dictionary<Type, Type[]>
        {
            {typeof(NormalState), new[]{typeof(ActiveActionState), typeof(InterruptState), typeof(ApparentDeathState)}},
            {typeof(ActiveActionState), new[]{typeof(NormalState), typeof(InterruptState), typeof(ApparentDeathState)}},
            {typeof(InterruptState), new[]{typeof(NormalState), typeof(ApparentDeathState)}},
            {typeof(ApparentDeathState), new[]{typeof(NormalState)}},
        };
        /// 通常状態内の状態遷移マップ。ルート状態 > 生存状態 > 通常状態。
        static readonly Dictionary<Type, Type[]> NORMAL_STATE_MAP = new Dictionary<Type, Type[]>
        {
            {typeof(IdleState), new[]{typeof(ProceedState), typeof(AutomaticActionState)}},
            {typeof(ProceedState), new[]{typeof(IdleState), typeof(AutomaticActionState)}},
            {typeof(AutomaticActionState), new[]{typeof(IdleState),  typeof(AutomaticActionState)}},
        };
        /// 割り込み状態内の状態遷移マップ。ルート状態 > 生存状態 > 割り込み状態。
        static readonly Dictionary<Type, Type[]> INTERRUPT_STATE_MAP = new Dictionary<Type, Type[]>
        {
            {typeof(DamageState), new[]{typeof(StopState)}},
            {typeof(StopState), new[]{typeof(StopState)}},
        };

        /// ルート状態内の状態遷移管理オブジェクト。
        readonly StateTransitionManager<CharacterState> _characterStateTransitionManager = new StateTransitionManager<CharacterState>(CHARACTER_STATE_MAP);
        /// 生存状態内の状態遷移管理オブジェクト。
        readonly StateTransitionManager<CharacterState> _aliveStateTransitionManager = new StateTransitionManager<CharacterState>(ALIVE_STATE_MAP);
        /// 通常状態内の状態遷移管理オブジェクト。
        readonly StateTransitionManager<CharacterState> _normalStateTransitionManager = new StateTransitionManager<CharacterState>(NORMAL_STATE_MAP);
        /// 割り込み状態内の状態遷移管理オブジェクト。
        readonly StateTransitionManager<CharacterState> _interruptStateStateTransitionManager = new StateTransitionManager<CharacterState>(INTERRUPT_STATE_MAP);
        /// キャラクター状態遷移ブロッカー。
        readonly StateTransitionBlocker _stateTransitionBlocker = new StateTransitionBlocker();
        /// 内部状態に係わらず強制的にロジック更新を止めるためのフラグ。
        bool _isStoppingLogicUpdate = true;
        /// 状態連動アニメーションのためにセットされるビュー。
        ICharacterView _characterView;
        /// タイマー由来のものを除く Dispose 集合。
        CompositeDisposable _disposablesWithoutTimer = new CompositeDisposable();
        /// タイマー由来の Dispose。
        IDisposable _timerDisposable = null;
        /// 次のアクション系状態に遷移する際のアニメーションイベント。
        CharacterAnimationEvent _nextActionAnimationEvent;
        /// 次の停止状態のアニメーションがループ再生かどうか。
        bool _nextStopAnimationIsLoop;
        /// 次のダメージ状態の最大時間（その時間を待たずに強制遷移はあり得る）。
        float _nextMaxDamageStateTime = 0.0f;

        /// <summary>
        /// Initializes a new instance of the <see cref="Noroshi.BattleScene.CharacterState.CharacterStateHandler"/> class.
        /// </summary>
        public CharacterStateHandler()
        {
            _characterStateTransitionManager.AddState<AliveState>();
            _characterStateTransitionManager.AddState<DeadState>();
            _characterStateTransitionManager.AddState<WinState>();

            _aliveStateTransitionManager.AddState<NormalState>();
            _aliveStateTransitionManager.AddState<ActiveActionState>();
            _aliveStateTransitionManager.AddState<InterruptState>();
            _aliveStateTransitionManager.AddState<ApparentDeathState>();

            _normalStateTransitionManager.AddState<IdleState>();
            _normalStateTransitionManager.AddState<ProceedState>();
            _normalStateTransitionManager.AddState<AutomaticActionState>();

            _interruptStateStateTransitionManager.AddState<DamageState>();
            _interruptStateStateTransitionManager.AddState<StopState>();

            // それぞれリセット設定。
            GetOnExitStateObservable().Where(state => state.GetType() == typeof(AliveState))
                .Subscribe(_ => _aliveStateTransitionManager.Reset()).AddTo(_disposablesWithoutTimer);
            GetOnExitStateObservable().Where(state => state.GetType() == typeof(NormalState))
                .Subscribe(_ => _normalStateTransitionManager.Reset()).AddTo(_disposablesWithoutTimer);
            GetOnExitStateObservable().Where(state => state.GetType() == typeof(InterruptState))
                .Subscribe(_ => _interruptStateStateTransitionManager.Reset()).AddTo(_disposablesWithoutTimer);

            // 各状態に合わせてアニメーションを再生する。
            var typeToAnimationActionMap = new Dictionary<Type, System.Action>(){
                {typeof(IdleState)           , () => _characterView.PlayIdle()},
                {typeof(ProceedState)        , () => _characterView.PlayWalk()},
                {typeof(DamageState)         , () => _characterView.PlayDamage()},
                {typeof(StopState)           , () => _characterView.PlayStop(_nextStopAnimationIsLoop)},
                {typeof(AutomaticActionState), () => _characterView.PlayAction(_nextActionAnimationEvent)},
                {typeof(ActiveActionState)   , () => _characterView.PlayAction(_nextActionAnimationEvent)},
                {typeof(WinState)            , () => _characterView.PlayIdle()}, // 演出上待機アニメーションと紐付けて、勝利アニメーション再生は別途。
            };
            GetOnEnterStateObservable().Where(state => typeToAnimationActionMap.ContainsKey(state.GetType()))
                .Subscribe(state => typeToAnimationActionMap[state.GetType()].Invoke()).AddTo(_disposablesWithoutTimer);
            // 状態遷移時は必ず遷移前に移動を止めておく。
            GetOnExitStateObservable().Subscribe(_=> _characterView.StopMove()).AddTo(_disposablesWithoutTimer);
            // ダメージ状態からの遷移はタイマーあり。
            GetOnEnterStateObservable().Where(state => state.GetType() == typeof(DamageState))
                .Subscribe(state =>
                {
                    _timerDisposable = SceneContainer.GetTimeHandler().Timer(_nextMaxDamageStateTime)
                        .Subscribe(_ => TryToTransitToIdle());
                }).AddTo(_disposablesWithoutTimer);
            GetOnExitStateObservable().Where(state => state.GetType() == typeof(DamageState))
                .Subscribe(state => _timerDisposable.Dispose()).AddTo(_disposablesWithoutTimer);
        }
        /// 全状態管理オブジェクトを取得。
        StateTransitionManager<CharacterState>[] _getAllStateTransitionManagers()
        {
            return new []
            {
                _characterStateTransitionManager,
                _aliveStateTransitionManager,
                _normalStateTransitionManager,
                _interruptStateStateTransitionManager,
            };
        }

        /// 状態遷移を最初から開始する。
        public void Start()
        {
            _characterStateTransitionManager.Start<AliveState>();
            _aliveStateTransitionManager.Start<NormalState>();
            _normalStateTransitionManager.Start<IdleState>();
            _isStoppingLogicUpdate = false;
        }

        /// リセットする。
        public void Reset()
        {
            // 死亡状態がリセットされると生き返ってしまうので、それは回避。
            if (CurrentStateIsDead()) return;
            ForceReset();
        }
        /// （死亡状態であっても）強制的にリセットする。
        public void ForceReset()
        {
            _isStoppingLogicUpdate = true;
            _characterStateTransitionManager.Reset();
            _aliveStateTransitionManager.Reset();
            _normalStateTransitionManager.Reset();
            _interruptStateStateTransitionManager.Reset();
        }

        /// ビューをセット。
        public void SetCharacterView(ICharacterView view)
        {
            _characterView = view;
        }

        /// 状態遷移した直後に遷移先状態が OnNext される Observable を取得。
        public IObservable<CharacterState> GetOnEnterStateObservable()
        {
            return _getAllStateTransitionManagers().Select(state => state.GetOnEnterStateObservable()).Merge();
        }
        /// 状態遷移する直前に遷移元状態が OnNext される Observable を取得。
        public IObservable<CharacterState> GetOnExitStateObservable()
        {
            return _getAllStateTransitionManagers().Select(state => state.GetOnExitStateObservable()).Merge();
        }
        /// アクション系状態へ遷移した直後に遷移先状態が OnNext される Observable を取得。
        public IObservable<CharacterState> GetOnEnterActionObservable()
        {
            return GetOnEnterStateObservable().Where(state => state.IsActionState);
        }
        /// アクション系状態から遷移する直前に遷移元状態が OnNext される Observable を取得。
        public IObservable<CharacterState> GetOnExitActionObservable()
        {
            return GetOnExitStateObservable().Where(state => state.IsActionState);
        }

        /// ロジック更新の実行可否チェック。
        public bool CanLogicUpdate() { return !_isStoppingLogicUpdate && _characterStateTransitionManager.CurrentStateIs<AliveState>(); }
        /// アクション対象としての選択可否チェック。
        public bool IsTargetable() { return !_isStoppingLogicUpdate && !CurrentStateIsDead() && !CurrentStateIsApparentDeath(); }

        /// アクション系状態かどうか。
        public bool IsActionState()
        {
            if (!_currentStateIsAliveState()) return false;
            if (_aliveStateTransitionManager.GetCurrentState().IsActionState) return true;
            if (_currentStateIsNormalState() && _normalStateTransitionManager.GetCurrentState().IsActionState) return true;
            return false;
        }
        /// オートアクションを発動するためのゲージを蓄積させて良いかどうか。
        public bool CanChargeAutomaticActionGauge()
        {
            if (!_currentStateIsNormalState()) return false;
            return CurrentStateIsIdleState() || CurrentStateIsProceedState();
        }

        /// 生存状態所属かどうか。（ゲームロジックに紐付いた状態ではないので外から呼ぶのは禁止）
        bool _currentStateIsAliveState()
        {
            return _characterStateTransitionManager.CurrentStateIs<AliveState>();
        }
        /// 通常状態所属かどうか。（ゲームロジックに紐付いた状態ではないので外から呼ぶのは禁止）
        bool _currentStateIsNormalState()
        {
            return _currentStateIsAliveState() && _aliveStateTransitionManager.CurrentStateIs<NormalState>();
        }
        /// 割り込み状態所属かどうか。（ゲームロジックに紐付いた状態ではないので外から呼ぶのは禁止）
        bool _currentStateIsInterruptState()
        {
            return _currentStateIsAliveState() && _aliveStateTransitionManager.CurrentStateIs<InterruptState>();
        }

        /// 待機状態かどうか。
        public bool CurrentStateIsIdleState()
        {
            return _currentStateIsNormalState() && _normalStateTransitionManager.CurrentStateIs<IdleState>();
        }
        /// 待機状態への遷移を試みる。
        public bool TryToTransitToIdle()
        {
            if (!_currentStateIsAliveState()) return false;
            if (_currentStateIsNormalState())
            {
                return _normalStateTransitionManager.TransitTo<IdleState>();
            }
            else if (_aliveStateTransitionManager.TransitTo<NormalState>())
            {
                _normalStateTransitionManager.Start<IdleState>();
                return true;
            }
            return false;
        }
        /// 待機状態への遷移を試みる。ただし、Wave バトル終了時限定処理として、前進状態以外は中断しない。
        public bool TryToTransitToIdleOnExitWaveBattle()
        {
            // ロジック更新も止める。
            _isStoppingLogicUpdate = true;
            // 正確には遷移成功とは言わないが、呼び出し元での処理分岐の都合上 true を返す。
            if (CurrentStateIsIdleState()) return true;
            if (!CurrentStateIsProceedState()) return false;
            return TryToTransitToIdle();
        }

        /// 前進状態かどうか。
        public bool CurrentStateIsProceedState()
        {
            return _currentStateIsNormalState() && _normalStateTransitionManager.CurrentStateIs<ProceedState>();
        }
        /// 前進状態への遷移を試みる。
        public bool TryToTransitToProceed()
        {
            if (!CanLogicUpdate()) return false;
            if (_stateTransitionBlocker.ProceedBlock) return false;
            return _currentStateIsNormalState() && _normalStateTransitionManager.TransitTo<ProceedState>();
        }

        /// オートアクション状態への遷移可否チェック。
        public bool CanTransitToAutomaticAction()
        {
            if (!CanLogicUpdate()) return false;
            return _currentStateIsNormalState() && _normalStateTransitionManager.CanTransitTo<AutomaticActionState>();
        }
        /// オートアクション状態への遷移を試みる。
        public bool TryToTransitToAutomaticAction(CharacterAnimationEvent animationEvent)
        {
            if (!CanTransitToAutomaticAction()) return false;
            _nextActionAnimationEvent = animationEvent;
            return _currentStateIsNormalState() && _normalStateTransitionManager.TransitTo<AutomaticActionState>();
        }

        /// アクティブアクション状態への遷移を試みる。
        public bool TryToTransitToActiveAction(CharacterAnimationEvent animationEvent)
        {
            if (_stateTransitionBlocker.ActiveActionBlock) return false;
            _nextActionAnimationEvent = animationEvent;
            return _currentStateIsAliveState() && _aliveStateTransitionManager.TransitTo<ActiveActionState>();
        }

        /// 瀕死状態かどうか。
        public bool CurrentStateIsApparentDeath()
        {
            return _currentStateIsAliveState() && _aliveStateTransitionManager.CurrentStateIs<ApparentDeathState>();
        }
        /// 瀕死状態への遷移を試みる。
        public bool TryToTransitToApparentDeath()
        {
            if (!_characterStateTransitionManager.CurrentStateIs<AliveState>()) return false;
            return _aliveStateTransitionManager.TransitTo<ApparentDeathState>();
        }

        /// ダメージ状態への遷移を試みる。
        public bool TryToTransitToDamage(float stateWaitTime)
        {
            _nextMaxDamageStateTime = stateWaitTime;
            return _tryToTransitToInterruptState<DamageState>();
        }
        /// 停止状態への遷移を試みる。
        public bool TryToTransitToStop(bool stopAnimationIsLoop)
        {
            _nextStopAnimationIsLoop = stopAnimationIsLoop;
            return _tryToTransitToInterruptState<StopState>();
        }
        bool _tryToTransitToInterruptState<T>() where T : CharacterState
        {
            if (!_currentStateIsAliveState()) return false;
            if (_currentStateIsInterruptState())
            {
                return _interruptStateStateTransitionManager.TransitTo<T>();
            }
            else if (_aliveStateTransitionManager.TransitTo<InterruptState>())
            {
                _interruptStateStateTransitionManager.Start<T>();
                return true;
            }
            return false;
        }

        /// 死亡状態かどうか。
        public bool CurrentStateIsDead()
        {
            return _characterStateTransitionManager.CurrentStateIs<DeadState>();
        }
        /// 死亡状態への遷移を試みる。
        public bool TryToTransitToDead()
        {
            return _characterStateTransitionManager.TransitTo<DeadState>();
        }

        /// 勝利状態への遷移を試みる。
        public bool TryToTransitToWin()
        {
            return _characterStateTransitionManager.TransitTo<WinState>();
        }

        /// 状態遷移ブロッカーを追加。
        public void AddBlockerFactor(StateTransitionBlocker.Factor factor) { _stateTransitionBlocker.AddFactor(factor); }
        /// 状態遷移ブロッカーを削除。
        public void RemoveBlockerFactor(StateTransitionBlocker.Factor factor) { _stateTransitionBlocker.RemoveFactor(factor); }

        /// リソース破棄。
        public void Dispose()
        {
            _disposablesWithoutTimer.Dispose();
            if (_timerDisposable != null) _timerDisposable.Dispose();
        }


        public class AliveState : CharacterState
        {
        }
        public class DeadState : CharacterState
        {
        }
        public class WinState : CharacterState
        {
        }
        
        public class NormalState : CharacterState
        {
        }
        public class ActiveActionState : CharacterState
        {
            public override bool IsActionState { get { return true; } }
        }
        public class InterruptState : CharacterState
        {
        }
        public class ApparentDeathState : CharacterState
        {
            public override bool IsActionState { get { return true; } }
        }
        
        public class IdleState : CharacterState
        {
        }
        public class ProceedState : CharacterState
        {
        }
        public class AutomaticActionState : CharacterState
        {
            public override bool IsActionState { get { return true; } }
        }
        
        public class DamageState : CharacterState
        {
        }
        public class StopState : CharacterState
        {
        }
    }
}
