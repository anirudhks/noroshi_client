using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.WebApi.Response.Battle;
using Noroshi.Core.Game.Battle;
using Noroshi.Core.Game.Sound;
using Noroshi.BattleScene.Drop;
using Noroshi.BattleScene.CharacterEffect;
using Noroshi.BattleScene.Sound;
using Noroshi.BattleScene.Camera;

namespace Noroshi.BattleScene
{
    public enum BattleType
    {
        CpuBattle,
        PlayerBattle,
    }
    public abstract class AbstractBattleManager : IBattleManager
    {
        /// バトルカテゴリー。
        readonly BattleCategory _battleCategory;
        /// バトルコンテンツ内 ID。
        readonly uint _battleContentId;
        /// バトルに持ち込んだ自プレイヤーキャラクター ID。
        readonly uint[] _ownPlayerCharacterIds;
        /// バトルに持ち込んだ傭兵プレイヤーキャラクター ID。
        readonly uint? _rentalPlayerCharacterId;
        /// 支払い数（BP など）。
        readonly uint _paymentNum;
        /// バトル状態操作オブジェクト。
        readonly BattleStateHandler _stateHandler = new BattleStateHandler();

        /// ゲーム時間の進みが遅くなる際に Duration が OnNext される Subject。
        readonly Subject<float> _onEnterSlowTimeSubject = new Subject<float>();
        /// ウェーブバトル内でのカウントダウン（1秒毎）毎に残りウェーブバトル時間が OnNext される Subject。
        readonly Subject<byte> _onCountDownSubject = new Subject<byte>();
        /// Force 毎の排他的なアクティブアクション時間が終了する際に、true が OnNext される Subject。
        readonly Subject<bool> _onExitExclusiveActiveActionTimePerForceSubject = new Subject<bool>();
        /// 自キャラクターが（正）ダメージを与えた際に、自キャラクター全員の与ダメージ累積値が OnNext される Subject。
        readonly Subject<uint> _onOwnCharacterAddDamageSubject = new Subject<uint>();
        /// 敵キャラクターが死亡した際に、敵キャラクター累積死亡数が OnNext される Subject。
        readonly Subject<ushort> _onDefeatingNumEnemyCharacterSubject = new Subject<ushort>();
        /// キャラクターがアクティブアクション状態へ遷移する際に、遷移キャラクターが OnNext される Subject。
        readonly Subject<Character> _onEnterActiveActionSubject = new Subject<Character>();
        /// キャラクター死亡演出終了時に死亡キャラクターが OnNext される Subject。
        readonly Subject<Character> _onExitDeadAnimationSubject = new Subject<Character>();
        /// 次ウェーブ準備の完了時に準備完了ウェーブ番号が OnNext される Subject。
        readonly Subject<byte> _onCompletePrepareNextWave = new Subject<byte>();
        /// 次ウェーブへの遷移用ボタン表示時に true が、非表示時に false が OnNext される Subject。
        readonly Subject<bool> _onToggleNextWaveButtonVisible = new Subject<bool>();
        /// 次ウェーブへの遷移試行時に true が OnNext される Subject。
        readonly Subject<bool> _onTryToTransitToNextWaveSubject = new Subject<bool>();
        /// バトル終了時に勝敗が OnNext される Subject。
        readonly Subject<VictoryOrDefeat> _onFinishBattleSubject = new Subject<VictoryOrDefeat>();
        /// 最後のキャラクターアニメーション演出時間が終了する際に OnNext される Subject。
        readonly Subject<VictoryOrDefeat> _onExitLastCharacterAnimationTimeSubject = new Subject<VictoryOrDefeat>();
        /// キャラクターエフェクト操作時に操作内容が OnNext される Subject。
        readonly Subject<CharacterEffectEvent> _onCommandCharacterEffectSubject = new Subject<CharacterEffectEvent>();
        /// サウンド操作時に操作内容が OnNext される Subject。
        protected readonly Subject<SoundEvent> _onCommandSoundSubject = new Subject<SoundEvent>();
        /// カメラ操作時に操作内容が OnNext される Subject。
        protected readonly Subject<CameraEvent> _onCommandCameraSubject = new Subject<CameraEvent>();
        /// Disposable 集合。
        protected readonly CompositeDisposable _disposables = new CompositeDisposable();
        /// 分身キャラクター毎の Disposable 集合。
        readonly Dictionary<ShadowCharacter, CompositeDisposable> _shadowCharacterToDisposablesMap = new Dictionary<ShadowCharacter, CompositeDisposable>();

        /// バトル開始時 Web API レスポンス。
        Core.WebApi.Response.Battle.StartBattleResponse _startResponse;
        /// ウェーブ。
        protected readonly List<Wave> _waves = new List<Wave>();
        /// フィールド View。
        IFieldView _fieldView;
        /// ドロップ操作オブジェクト。
        public DropHandler DropHandler { get; private set; }
        /// バトル結果オブジェクト。
        public BattleResult BattleResult { get; private set; }
        /// 現在のウェーブ番号
        public byte CurrentWaveNo { get; private set; }
        /// オートバトルフラグ。
        bool _isAutoBattle = false;
        /// 敵キャラクター累積死亡数。
        ushort _defeatingNum = 0;
        /// 自キャラクター全員の与ダメージ累積値。
        uint _totalDamage = 0;

        bool _isInPause = false;
        Subject<bool> _onExitPause;


        public AbstractBattleManager(BattleCategory battleCategory, uint battleContentId, uint[] ownPlayerCharacterIds, uint? rentalPlayerCharacterId, uint paymentNum)
        {
            _battleCategory = battleCategory;
            _battleContentId = battleContentId;
            _ownPlayerCharacterIds = ownPlayerCharacterIds;
            _rentalPlayerCharacterId = rentalPlayerCharacterId;
            _paymentNum = paymentNum;
        }
        public void Initialize()
        {
        }
        public abstract IObservable<IManager> LoadDatas();

        protected void _onLoadDatas(StartBattleResponse data, BattleCharacter[] ownCharacters)
        {
            _startResponse = data;
            SceneContainer.GetCharacterManager().SetCurrentOwnPlayerCharacters(ownCharacters);

            var necessaryWaveNumForLoopBattle = _battleTime / Constant.MIN_WAVE_BATTLE + 1;
            var loopNum = LoopBattle ? necessaryWaveNumForLoopBattle / data.Waves.Count() + 1 : 1;

            var waveNo = 1;
            for (var i = 0; i < loopNum; i++)
            {
                foreach (var wave in data.Waves)
                {
                    _waves.Add(new Wave(wave, (byte)waveNo++));
                }
            }
            CurrentWaveNo = 1;
            var dropGoldCharacterNum = (byte)data.Waves.Sum(wave => wave.BattleCharacters.Count());
            DropHandler = new DropHandler(data.DropPossessionObjects, data.Gold, dropGoldCharacterNum);
        }
        
        protected IObservable<StartBattleResponse> _requestBattleStartWebAPI()
        {
            var requestParam = new Datas.Request.BattleStartRequest
            {
                Category = (byte)_battleCategory,
                ID = _battleContentId,
                PlayerCharacterIDs = _ownPlayerCharacterIds,
                RentalPlayerCharacterID = _rentalPlayerCharacterId,
                PaymentNum = _paymentNum,
            };
            return SceneContainer.GetWebApiRequester().Post<Datas.Request.BattleStartRequest, StartBattleResponse>("Battle/StartBattle", requestParam);
        }

        public virtual IObservable<IManager> LoadAssets(IFactory factory)
        {
            return factory.BuildFieldView(_fieldId).Do(v => _fieldView = v)
                .SelectMany(_ => factory.GetAttributeAssetFactory().PreloadFrogView(Constant.MAX_NORMAL_CHARACTER_NUM_IN_FIELD_PER_FORCE))
                .Select(v => (IManager)this);
        }

        /// バトルカテゴリー。
        public BattleCategory BattleCategory { get { return _battleCategory; } }
        /// バトル時間。
        protected byte _battleTime { get { return _startResponse.BattleTimeSecond; } }
        /// フィールド ID。
        uint _fieldId { get { return _startResponse.FieldID; } }
        /// BGM サウンド ID。
        uint _bgmSoundId { get { return _startResponse.BgmSoundID; } }
        /// 獲得ゴールド。
        uint _gold { get { return _startResponse.Gold; } }
        /// 獲得プレイヤー経験値。
        ushort _playerExp { get { return _startResponse.PlayerExp; } }
        /// オートモード。
        public BattleAutoMode BattleAutoMode { get { return (BattleAutoMode)_startResponse.BattleAutoMode; } }
        /// ループバトル。
        public bool LoopBattle { get { return _startResponse.LoopBattle; } }
        /// 敵が無限 HP となるバトル。
        public bool UnlimitedHPEnemyBattle { get { return _startResponse.UnlimitedHPEnemyBattle; } }
        /// バトル追加情報。
        public Noroshi.Core.WebApi.Response.Battle.AdditionalInformation AdditionalInformation{ get { return _startResponse.AdditionalInformation; } }

        /// ゲーム時間の進みが遅くなる際に Duration が OnNext される Observable を取得。
        public IObservable<float> GetOnEnterSlowTimeObservable() { return _onEnterSlowTimeSubject.AsObservable(); }
        /// ウェーブバトル内でのカウントダウン（1秒毎）毎に残りウェーブバトル時間が OnNext される Observable を取得。
        public IObservable<byte> GetOnCountDownObservable() { return _onCountDownSubject.AsObservable(); }
        /// Force 毎の排他的なアクティブアクション時間が終了する際に、true が OnNext される Observable を取得。
        public IObservable<bool> GetOnExitExclusiveActiveActionTimePerForceObservable() { return _onExitExclusiveActiveActionTimePerForceSubject.AsObservable(); }
        /// 自キャラクターが（正）ダメージを与えた際に、自キャラクター全員の与ダメージ累積値が OnNext される Observable を取得。
        public IObservable<uint> GetOnOwnCharacterAddDamageObservable() { return _onOwnCharacterAddDamageSubject.AsObservable(); }
        /// 敵キャラクターが死亡した際に、敵キャラクター累積死亡数が OnNext される Observable を取得。
        public IObservable<ushort> GetOnDefeatingNumEnemyCharacterObservable() { return _onDefeatingNumEnemyCharacterSubject.AsObservable(); }
        /// キャラクターがアクティブアクション状態へ遷移する際に、遷移キャラクターが OnNext される Observable を取得。
        public IObservable<Character> GetOnEnterActiveActionObservable() { return _onEnterActiveActionSubject.AsObservable(); }
        /// キャラクター死亡演出終了時に死亡キャラクターが OnNext される Observable を取得。
        public IObservable<Character> GetOnExitDeadAnimationObservable() { return _onExitDeadAnimationSubject.AsObservable(); }
        /// 次ウェーブ準備の完了時に準備完了ウェーブ番号が OnNext される Observable を取得。
        public IObservable<byte> GetOnCompletePrepareNextWaveObservable() { return _onCompletePrepareNextWave.AsObservable(); }
        /// 次ウェーブへの遷移用ボタン表示時に true が、非表示時に false が OnNext される Observable を取得。
        public IObservable<bool> GetOnToggleNextWaveButtonVisibleObservable() { return _onToggleNextWaveButtonVisible.AsObservable(); }
        /// バトル終了時に勝敗が OnNext される Observable を取得。
        public IObservable<VictoryOrDefeat> GetOnFinishBattleObservable() { return _onFinishBattleSubject.AsObservable(); }
        /// 最後のキャラクターアニメーション演出時間が終了する際に OnNext される Observable を取得。
        public IObservable<VictoryOrDefeat> GetOnExitLastCharacterAnimationTimeObservable() { return _onExitLastCharacterAnimationTimeSubject.AsObservable(); }
        /// キャラクターエフェクト操作時に操作内容が OnNext される Observable を取得。
        public IObservable<CharacterEffectEvent> GetOnCommandCharacterEffectObservable() { return _onCommandCharacterEffectSubject.AsObservable(); }
        /// サウンド操作時に操作内容が OnNext される Observable を取得。
        public IObservable<SoundEvent> GetOnCommandSoundObservable() { return _onCommandSoundSubject.AsObservable(); }
        /// カメラ操作時に操作内容が OnNext される Observable を取得。
        public IObservable<CameraEvent> GetOnCommandCameraObservable() { return _onCommandCameraSubject.AsObservable(); }

        /// ウェーブバトルへ遷移する際に、ウェーブ番号が OnNext される Observable を取得。
        public IObservable<byte> GetOnEnterWaveBattleObservable()
        {
            return _stateHandler.GetOnEnterWaveBattleStateObservable().Select(_ => CurrentWaveNo);
        }

        /// ウェーブ数。
        public int WaveNum { get { return _waves.Count(); } }
        /// 現在のウェーブ。
        public Wave CurrentWave { get { return _waves[CurrentWaveNo - 1]; } }

        /// 準備。
        public virtual void Prepare()
        {
            CurrentWave.SetCharactersToInitialPosition();
            if (BattleAutoMode == BattleAutoMode.Auto)
            {
                SetAutoMode(true);
                SceneContainer.GetCharacterManager().CurrentOwnCharacterSet.SetAuto(true);
            }
            DropHandler.GetOnCommandDropMoneyObservable().Subscribe(e => {
                _onCommandCharacterEffectSubject.OnNext(new CharacterEffectEvent
                {
                    Command = CharacterEffectCommand.Play,
                    CharacterView = e.CharacterView,
                    CharacterEffectID = Constant.DROP_COIN_CHARACTER_EFFECT_ID,
                });
            }).AddTo(_disposables);
        }

        public IObservable<VictoryOrDefeat> Start()
        {
            SceneContainer.GetCharacterManager().GetAllCharacters().Select(character => character.GetOnCommandSoundObservable())
            .Merge().Subscribe(_onCommandSoundSubject.OnNext).AddTo(_disposables);

            SceneContainer.GetCharacterManager().GetAllCharacters().Select(character => character.GetOnTryCommandCameraObservable())
            .Merge().Subscribe(_onCommandCameraSubject.OnNext).AddTo(_disposables);

            _stateHandler.GetOnEnterWaveBattleStateObservable()
            .Subscribe(_ => StartWave()).AddTo(_disposables);
            _stateHandler.GetOnEnterIntervalStateObservable()
            .SelectMany(_ => StartInterval()).Subscribe(_ => {}).AddTo(_disposables);
            _stateHandler.GetOnEnterSwitchWaveStateObservable()
            .SelectMany(_ => SwitchWave()).Subscribe(_ => {}).AddTo(_disposables);

            // 各 Wave の Observable に処理を紐付ける。

            _waves.Select(w => w.GetOnFinishBattleObservable()).Merge()
            .Subscribe(_onFinishWaveBattle).AddTo(_disposables);

            _waves.Select(w => w.GetOnCountDownObservable()).Merge()
            .Subscribe(_onCountDownSubject.OnNext).AddTo(_disposables);

            _waves.Select(w => w.GetOnCharacterDieObservable()).Merge()
            .Subscribe(_onCharacterDie).AddTo(_disposables);

            _waves.Select(w => w.GetOnOwnCharacterAddDamageObservable()).Merge()
            .Subscribe(_onOwnCharacterAddDamage).AddTo(_disposables);

            _waves.Select(w => w.GetOnCommandCharacterEffectObservable()).Merge()
            .Subscribe(_onCommandCharacterEffectSubject.OnNext).AddTo(_disposables);

            _waves.Select(w => w.GetOnDarkenFieldObservable()).Merge()
            .Subscribe(_onDarkenField).AddTo(_disposables);

            _waves.Select(w => w.GetOnExitDeadAnimationObservable()).Merge()
            .Subscribe(_onExitDeadAnimationSubject.OnNext).AddTo(_disposables);

            _waves.Select(w => w.GetOnExitExclusiveActiveActionTimePerForceObservable()).Merge()
            .Subscribe(_onExitExclusiveActiveActionTimePerForceSubject.OnNext).AddTo(_disposables);

            _waves.Select(w => w.GetOnEnterActiveActionObservable()).Merge()
            .Subscribe(_onEnterActiveActionSubject.OnNext).AddTo(_disposables);

            if (_shouldPlayBattleBgmAtDefaultRoutine)
            {
                // 戦闘画面のBGMを再生する
                PlayBattleBgm();
            }

            // 状態遷移開始
            return _stateHandler.Start()
                .Do(victoryOrDefeat =>
                {
                    var characterManager = SceneContainer.GetCharacterManager();
                    BattleResult = new BattleResult(
                        _battleCategory, _battleContentId,
                        characterManager.CurrentOwnCharacterSet, characterManager.CurrentEnemyCharacterSet,
                        victoryOrDefeat, _gold, DropHandler.GetDropItemIDs(), _playerExp,
                        _defeatingNum, _totalDamage
                    );
                })
                .SelectMany(victoryOrDefeat => _waitForBattleFinish().Select(_ => victoryOrDefeat))
                .SelectMany(victoryOrDefeat => _pauseAtFinish(victoryOrDefeat).Select(_ => victoryOrDefeat))
                // 最後のキャラクターアニメーション演出時間。
                .SelectMany(victoryOrDefeat =>
                {
                    return BattleResult.StartLastCharacterAnimationTime()
                    .Do(_ =>
                    {
                        _onExitLastCharacterAnimationTimeSubject.OnNext(victoryOrDefeat);
                        _onExitLastCharacterAnimationTimeSubject.OnCompleted();
                        _onCommandCameraSubject.OnCompleted();
                    });
                })
                // バトル結果送信時間。
                .SelectMany(vd =>  BattleResult.SendResult(CurrentWave.No).Select(_ => vd));
        }
        /// キャラクター死亡時処理。
        protected virtual void _onCharacterDie(Character character)
        {
            // 撃破数カウントアップ。
            if (character.Force == Force.Enemy) _onDefeatingNumEnemyCharacterSubject.OnNext(++_defeatingNum);
            // 死亡キャラクターが敵の場合はドロップ処理
            if (character.GetType() != typeof(ShadowCharacter) && character.Force == Force.Enemy)
            {
                DropHandler.Drop(character.GetView(), CurrentWaveNo, character.No);
            }
            var shadowCharacter = character as ShadowCharacter;
            if (shadowCharacter != null)
            {
                _shadowCharacterToDisposablesMap[shadowCharacter].Dispose();
                _shadowCharacterToDisposablesMap.Remove(shadowCharacter);
            }
        }
        /// 自キャラクター与ダメージ時処理。
        void _onOwnCharacterAddDamage(int damage)
        {
            if (damage > 0) 
            {
                _totalDamage = (uint)(_totalDamage + damage);
                _onOwnCharacterAddDamageSubject.OnNext(_totalDamage);
            }
        }
        /// フィールド暗転、暗転解除時処理。
        void _onDarkenField(bool darken)
        {
            if (darken)
            {
                _fieldView.Darken();
            }
            else
            {
                _fieldView.Brighten();
            }
        }
        /// ウェーブバトル終了時処理。
        void _onFinishWaveBattle(VictoryOrDefeat waveBattleResult)
        {
            // 戦況カメラ処理を止める.
            _onCommandCameraSubject.OnNext(new CameraEvent
            {
                Command = CameraEvent.CameraCommand.Pause,
                Type = CameraEvent.CameraType.BattleSituation
            });
            // 次ウェーブがある場合。
            if (waveBattleResult == VictoryOrDefeat.Win && CurrentWaveNo < WaveNum)
            {
                var ownCharacters = SceneContainer.GetCharacterManager().CurrentOwnCharacterSet.GetCharacters().Where(c => !c.IsDead);
                foreach (var ownCharacter in ownCharacters)
                {
                    ownCharacter.CheckActiveActionAvailable();
                }
                ownCharacters.Select(character => character.TryToTransitToIdleOrDeadOnExitWaveBattle())
                .WhenAll()
                .Subscribe(characters => _stateHandler.FinishWaveBattle(waveBattleResult, false));
            }
            // 次ウェーブがない（バトル終了の）場合。
            else
            {
                _stateHandler.FinishWaveBattle(waveBattleResult, true);
            }
        }

        // バトルで使用するBGMのSoundIdの配列を取得
        public virtual uint[] GetBattleBgmSoundIDs()
        {
            return new uint[]{ _bgmSoundId, SoundConstant.BATTLE_RESULT_WIN_SOUND_ID, SoundConstant.BATTLE_RESULT_LOSE_SOUND_ID };
        }

        /// 戦闘画面のBGMを再生する
        public void PlayBattleBgm()
        {
            _onCommandSoundSubject.OnNext(new SoundEvent()
            {
                Command = SoundCommand.Play,
                SoundID = _bgmSoundId,
            });
        }

        /// 結果画面のBGMを再生する
        public void PlayResultBgm(bool isWin)
        {
            if (isWin)
            {
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    Command = SoundCommand.Play,
                    SoundID = SoundConstant.BATTLE_RESULT_WIN_SOUND_ID,
                });
            }
            else
            {
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    Command = SoundCommand.Play,
                    SoundID = SoundConstant.BATTLE_RESULT_LOSE_SOUND_ID,
                });
            }
        }
            
        protected virtual bool _shouldPlayBattleBgmAtDefaultRoutine { get { return true; } }
        public virtual bool ShouldPlayResultBgmAtDefaultRoutine (VictoryOrDefeat victoryOrDefeat)
        {
            return true;
        }

        IObservable<bool> _waitForBattleFinish()
        {
            return _isInPause ? _onExitPause.AsObservable() : Observable.Return<bool>(true);
        }
        IObservable<bool> _pauseAtFinish(VictoryOrDefeat battleResult)
        {
            // ここでアクション中に起きる画面揺れが起きていたら止める. OnCompleteはしない(このあとに使用するため)
            _onCommandCameraSubject.OnNext(new CameraEvent()
            {
                Command = CameraEvent.CameraCommand.Stop,
                Type = CameraEvent.CameraType.Action
            });
            _onCommandCharacterEffectSubject.OnCompleted();
            _onFinishBattleSubject.OnNext(battleResult);
            return _pauseWithTimeSpan(Constant.BATTLE_FINISH_PAUSE_TIME).Do(_ => _onPauseAtFinish());
        }
        protected virtual void _onPauseAtFinish()
        {
            // スロー開始
            _onEnterSlowTimeSubject.OnNext(Constant.BATTLE_FINISH_SLOW_TIME);
        }

        public IObservable<IBattleManager> RunOwnCharactersToCorrectPosition()
        {
            return CurrentWave.RunOwnCharactersToCorrectPosition(GetSwitchWaveTime()).Select(_ => (IBattleManager)this);
        }

        /// Ready 状態の終了を待つべきかどうか判定。
        public virtual bool ShouldWaitToFinishReady() { return false; }

        public virtual void StartWave()
        {
            _onCommandCameraSubject.OnNext(new CameraEvent
            {
                Command = CameraEvent.CameraCommand.Play,
                Type = CameraEvent.CameraType.BattleSituation
            });
            CurrentWave.Start(_battleTime);
        }
        public IObservable<IBattleManager> StartInterval()
        {
            _onCommandCameraSubject.OnNext(new CameraEvent()
            {
                Command = CameraEvent.CameraCommand.Reset,
                Type = CameraEvent.CameraType.SwitchWave
            });
            _onToggleNextWaveButtonVisible.OnNext(true);
            return _getOnCommandTransitToNextWaveObservable().First()
            .SelectMany(_ =>
            {
                // ウェーブ回復の効果音
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    SoundID = SoundConstant.BATTLE_HEAL_SOUND_ID,
                    Command = SoundCommand.Play,
                });
                _onCommandSoundSubject.OnNext(new SoundEvent()
                {
                    SoundID = SoundConstant.BATTLE_ENERGY_SOUND_ID,
                    Command = SoundCommand.Play,
                });
                        
                SceneContainer.GetCharacterManager().CurrentOwnCharacterSet.RecoveryAtInterval();
                _onToggleNextWaveButtonVisible.OnNext(false);
                return SceneContainer.GetTimeHandler().Timer(1);
            })
            .Do(t => _stateHandler.TransitToSwitchWave())
            .Select(_ => (IBattleManager)this);
        }
        public IObservable<Wave> SwitchWave()
        {
            CurrentWave.Dispose();

            SceneContainer.GetCharacterManager().CurrentOwnCharacterSet.PrepareNextWave();

            CurrentWaveNo++;
            _onCompletePrepareNextWave.OnNext(CurrentWaveNo);
            SceneContainer.GetCharacterManager().SwitchNextEnemyCharacterSet();
            return _setCharactersToInitialPositionWithAnimation()
                .SelectMany(wave => _processBeforeTransitionToWaveBattle().Select(_ => wave))
                .Do(_ => _stateHandler.TransitToWaveBattle());
        }
        /// WaveBattle 状態へ遷移する直前に差し込まれる処理。
        protected virtual IObservable<bool> _processBeforeTransitionToWaveBattle()
        {
            return Observable.Return(true);
        }
        IObservable<Wave> _setCharactersToInitialPositionWithAnimation()
        {
            var isWalking = _isMovingToNextWaveWithWalking();
            // 進行時の歩行もしくはダッシュの効果音
            _onCommandSoundSubject.OnNext(new SoundEvent()
            {
                SoundID = (isWalking ? SoundConstant.BATTLE_WALK_SOUND_ID : SoundConstant.BATTLE_RUN_SOUND_ID),
                Command = SoundCommand.Play,
            });
            // 走る時だけカメラを揺らす
            if (!isWalking)
            {
                _onCommandCameraSubject.OnNext(new CameraEvent()
                {
                    Command = CameraEvent.CameraCommand.Play,
                    Type = CameraEvent.CameraType.SwitchWave
                });
            }
            return CurrentWave.SetCharactersToInitialPositionWithAnimation(GetSwitchWaveTime(), isWalking);
        }
        protected virtual bool _isMovingToNextWaveWithWalking()
        {
            return false;
        }
        public float GetSwitchWaveTime()
        {
            return _isMovingToNextWaveWithWalking() ? Constant.SLOW_SWITCH_WAVE_TIME : Constant.SWITCH_WAVE_TIME;
        }

        public void TryToTransitToNextWave() { _onTryToTransitToNextWaveSubject.OnNext(true); }
        IObservable<bool> _getOnCommandTransitToNextWaveObservable()
        {
            return _isAutoBattle ? Observable.Return<bool>(false) : _onTryToTransitToNextWaveSubject.AsObservable();
        }

        /// 分身キャラクターを追加する。
        public void AddShadowCharacter(ShadowCharacter shadow)
        {
            _shadowCharacterToDisposablesMap.Add(shadow, new CompositeDisposable());
            shadow.GetOnCommandSoundObservable().Subscribe(_onCommandSoundSubject.OnNext).AddTo(_shadowCharacterToDisposablesMap[shadow]);
            shadow.GetOnTryCommandCameraObservable().Subscribe(_onCommandCameraSubject.OnNext).AddTo(_shadowCharacterToDisposablesMap[shadow]);
            CurrentWave.AddShadowCharacter(shadow);
        }

        IObservable<bool> _pauseWithTimeSpan(float timeSpan)
        {
            // 指定時間後に解除設定。
            SceneContainer.GetTimeHandler().Timer(timeSpan).Subscribe(_ => _exitPause());
            return _enterPause();
        }
        protected IObservable<bool> _enterPause()
        {
            _isInPause = true;
            var characterManager = SceneContainer.GetCharacterManager();
            var characters = characterManager.GetCurrentAllCharacters().Concat(characterManager.GetCurrentAllShadowCharacters());
            foreach (var character in characters)
            {
                character.PauseOn();
            }
            var onExitPause = new Subject<bool>();
            _onExitPause = onExitPause;
            return onExitPause.AsObservable();
        }
        protected void _exitPause()
        {
            var characterManager = SceneContainer.GetCharacterManager();
            var characters = characterManager.GetCurrentAllCharacters().Concat(characterManager.GetCurrentAllShadowCharacters());
            foreach (var character in characters)
            {
                character.PauseOff();
            }
            _isInPause = false;
            _onExitPause.OnNext(true);
        }

        public void LogicUpdate()
        {
            if (!_stateHandler.CanLogicUpdate()) return;
            CurrentWave.LogicUpdate();
        }

        public void Dispose()
        {
            _disposables.Dispose();
            CurrentWave.Dispose();
            DropHandler.Dispose();
        }

        /// オートモード切り替え。
        public void SetAutoMode(bool isOn)
        {
            _isAutoBattle = isOn;
        }

        public Dictionary<Grid.Direction, Character> GetScreentEndCharacters()
        {
            return CurrentWave.GetScreentEndCharacters();
        }

        public void AdjustFieldAfterSwitchWave()
        {
            if (LoopBattle) _fieldView.AdjustAfterSwitchWave();
        }

        // FieldLayerの移動を開始させる(SwitchWave連動)
        public void EnterFieldSwitchWave()
        {
            _fieldView.EnterSwitchWave(CurrentWaveNo, GetSwitchWaveTime());
        }
        public void EnterFieldSwitchWave(byte waveNo, float duration)
        {
            _fieldView.EnterSwitchWave(waveNo, duration);
        }

        public void SetForegroundFieldVisible(bool isVisible)
        {
            _fieldView.SetForegroundVisible(isVisible);
        }
    }
}
