﻿namespace Noroshi.BattleScene.Camera
{
    public class CameraEvent
    {
        public enum CameraCommand
        {
            Play = 1,
            Pause,
            Replay,
            Stop,
            Reset,
        }
        public enum CameraType
        {
            Action = 1,
            Blur,
            BattleSituation,
            SwitchWave,
        }

        public CameraCommand Command;
        public CameraType Type;
    }
}
