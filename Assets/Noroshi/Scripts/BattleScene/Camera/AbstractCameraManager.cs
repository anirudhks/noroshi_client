﻿using UniRx;
using System;
using Noroshi.Core.Game.Battle;
using System.Collections.Generic;

namespace Noroshi.BattleScene.Camera
{
    public abstract class AbstractCameraManager : ICameraManager
    {
        // カメラ移動やスケールに使用する共通の再生時間
        const float NORMAL_CAMERA_DURATION = 0.25f;

        protected ICameraController _cameraController;
        protected CompositeDisposable _disposables = new CompositeDisposable();
        // バトル終了フラグ(勝敗関係なし)
        bool _isFinishBattle = false;
        bool _isEnterActiveAction = false;

        public virtual void Initialize()
        {
            var battleManager = SceneContainer.GetBattleManager();
            // 次のWaveが準備完了した際の処理
            battleManager.GetOnCompletePrepareNextWaveObservable()
            .SelectMany(no => 
            {
                battleManager.EnterFieldSwitchWave();
                return _moveCameraSwitchWave(no);
            })
            .Subscribe(_ => battleManager.AdjustFieldAfterSwitchWave())
            .AddTo(_disposables);

            // イベント通知によりWave切り替え時に行う処理
            battleManager.GetOnCommandCameraObservable()
            .Where(cameraEvent => cameraEvent.Type == CameraEvent.CameraType.SwitchWave)
            .SelectMany(cameraEvent => _setSwitchWaveCamera(cameraEvent))
            .Subscribe(_ => {}).AddTo(_disposables);
           
            // バトル終了時の処理
            battleManager.GetOnFinishBattleObservable()
            .Do(_ => _isFinishBattle = true)
            .Where(victoryOrDefeat => victoryOrDefeat == VictoryOrDefeat.Win || victoryOrDefeat == VictoryOrDefeat.Loss)
            .SelectMany(victoryOrDefaet => _onFinishBattle())
            .Subscribe(_ => {}).AddTo(_disposables);

            /// 最後のキャラクターアニメーション演出時間が終了する際の処理。
            battleManager.GetOnExitLastCharacterAnimationTimeObservable()
            .SelectMany(victoryOrDefeat =>
            {
                // カメラ操作。
                return _battleSituationCamera(new CameraEvent
                {
                    Command = CameraEvent.CameraCommand.Reset,
                    Type = CameraEvent.CameraType.BattleSituation
                }, battleManager.CurrentWaveNo)
                // UI 以外をぼかす。
                .SelectMany(_ => _cameraController.SetBlurActive(true));
            })
            .Subscribe(_ => {}).AddTo(_disposables);

            // アクション時にカメラが揺れる場合戦況カメラズームを止める
            // また連続で揺れる場合揺れ終わりから次の揺れ始めまでズームが発生してしまうので、インターバルを設けてすぐにズームを再開させないようにする
            battleManager.GetOnCommandCameraObservable()
            .Where(cameraCommand => cameraCommand.Type == CameraEvent.CameraType.Action)
            .SelectMany(cameraCommand => _shakeCamera(cameraCommand.Command, _cameraController.PauseOnWithBattleSituationCamera))
            .SelectMany(_ => SceneContainer.GetTimeHandler().Timer(1.0f))
            .Subscribe(_ => 
            {
                // すでにバトルが終了している場合は戦況カメラは動かさず別のズームアウト処理の方に任せる
                // それと、アクティブアクションを発動している場合戦況カメラの停止解除はアクティブスキル側のObservableに任せる
                if (!_isFinishBattle && !_isEnterActiveAction) _cameraController.PauseOffWithBattleSituationCamera();
            })
            .AddTo(_disposables);

            // Wave開始時及びキャラクター死亡演出終了時に画面端のキャラクター情報を更新する
            battleManager.GetOnEnterWaveBattleObservable().AsUnitObservable()
            .Merge(battleManager.GetOnExitDeadAnimationObservable().AsUnitObservable())
            .Subscribe(_ => _setScreenEndCharacters(battleManager.GetScreentEndCharacters())).AddTo(_disposables);

            // 戦況カメラのイベントが呼ばれた時の処理
            battleManager.GetOnCommandCameraObservable()
            .Where(cameraCommand => cameraCommand.Type == CameraEvent.CameraType.BattleSituation)
            .SelectMany(cameraCommand => _battleSituationCamera(cameraCommand, battleManager.CurrentWaveNo))
            .Subscribe(_ => {}).AddTo(_disposables);

            // アクティブアクション発動時にカメラをズームインする
            battleManager.GetOnEnterActiveActionObservable().Where(c => c.Force == Force.Own)
            .SelectMany(character => 
            {
                _isEnterActiveAction = true;
                battleManager.SetForegroundFieldVisible(false);
                _cameraController.PauseOnWithBattleSituationCamera();
                return _cameraController.MoveAndZoomWithActiveAction(character, 0.1f);   
            })
            .Subscribe(_ => {}).AddTo(_disposables);

            // 排他的なアクティブアクション時間が終了した時の処理
            battleManager.GetOnExitExclusiveActiveActionTimePerForceObservable()
            .Subscribe(_ => 
            {
                _isEnterActiveAction = false;
                battleManager.SetForegroundFieldVisible(true);
                // すでにバトルが終了している場合は戦況カメラは動かさず別のズームアウト処理の方に任せる
                if (!_isFinishBattle) _cameraController.PauseOffWithBattleSituationCamera();
            })
            .AddTo(_disposables);

            // ぼかし処理
            battleManager.GetOnCommandCameraObservable()
            .Where(cameraEvent => cameraEvent.Type == CameraEvent.CameraType.Blur)
            .SelectMany(cameraEvent => _setBlurCamera(cameraEvent))
            .Subscribe(_ => {}).AddTo(_disposables);
        }

        void _setScreenEndCharacters(Dictionary<Grid.Direction, Character> _screenEndCharacters)
        {
            _cameraController.SetScreenEndCharacters(_screenEndCharacters);
        }

        public IObservable<IManager> LoadDatas()
        {
            return Observable.Return<IManager>(this);
        }

        public IObservable<IManager> LoadAssets(IFactory factory)
        {
            return factory.BuildCameraController().Select(c =>
            {
                _cameraController = c;
                return (IManager)this;
            });
        }
        public virtual void Prepare()
        {
        }

        // Wave切り替えの移動処理
        protected IObservable<bool> _moveCameraSwitchWave(byte waveNo)
        {
            var switchWaveTime = SceneContainer.GetBattleManager().GetSwitchWaveTime();
            return _cameraController.Move(new UnityEngine.Vector3(WaveField.GetPositionX(waveNo), 0.0f, 0.0f), switchWaveTime, DG.Tweening.Ease.InOutQuad)
            .Select(_ => 
            {
                _cameraController.PauseOffWithBattleSituationCamera();
                _cameraController.SetNextWavedefaultCameraVertices();
                return true;
            });
        }
        // Waveバトル終了時のカメラ引き処理
        protected IObservable<bool> _zoomOutCameraFinishWave()
        {
            var currentWaveNo = SceneContainer.GetBattleManager().CurrentWaveNo;
            _cameraController.PauseOnWithBattleSituationCamera();
            return Observable.WhenAll(
                _cameraController.ZoomOut(NORMAL_CAMERA_DURATION),
                _cameraController.Move(new UnityEngine.Vector3(WaveField.GetPositionX(currentWaveNo), 0.0f, 0.0f), NORMAL_CAMERA_DURATION, DG.Tweening.Ease.Linear))
            .Select(_ => true);
        }
        // カメラを通常サイズまでズームアウトする
        protected IObservable<bool> _moveAndZoomOutCamera(byte waveNo)
        {
            _cameraController.PauseOnWithBattleSituationCamera();
            return Observable.WhenAll(
                _cameraController.Move(new UnityEngine.Vector3(WaveField.GetPositionX(waveNo), 0.0f, 0.0f), NORMAL_CAMERA_DURATION, DG.Tweening.Ease.Linear),
                _cameraController.ZoomOut(NORMAL_CAMERA_DURATION)
            )
            .Select(_ => true);
        }

        IObservable<bool> _onFinishBattle()
        {
            _cameraController.ClearShake();
            return _cameraController.Shake();
        }

        IObservable<bool> _shakeCamera(CameraEvent.CameraCommand command, Action cameraPlayAction)
        {
            switch (command)
            {
            case CameraEvent.CameraCommand.Play:
                _cameraController.ClearShake();
                cameraPlayAction();
                return _cameraController.ShakeByAction();
            case CameraEvent.CameraCommand.Pause:
                _cameraController.EnterTimeStop();
                return Observable.Return<bool>(true);
            case CameraEvent.CameraCommand.Replay:
                _cameraController.ExitTimeStop();
                return Observable.Return<bool>(true);
            case CameraEvent.CameraCommand.Stop:
                _cameraController.ClearShake();
                return Observable.Return<bool>(true);
            default:
                throw new ArgumentOutOfRangeException();
            }
        }

        IObservable<bool> _battleSituationCamera(CameraEvent cameraEvent, byte waveNo)
        {
            switch (cameraEvent.Command)
            {
            case CameraEvent.CameraCommand.Pause:
                _cameraController.PauseOnWithBattleSituationCamera();
                return Observable.Return<bool>(true);
            case CameraEvent.CameraCommand.Play:
                _cameraController.PauseOffWithBattleSituationCamera();
                return Observable.Return<bool>(true);
            case CameraEvent.CameraCommand.Reset:
                return _moveAndZoomOutCamera(waveNo);
            default:
                return Observable.Return<bool>(true); 
            }
        }

        IObservable<bool> _setBlurCamera(CameraEvent cameraEvent)
        {
            switch (cameraEvent.Command)
            {
            case CameraEvent.CameraCommand.Play:
                return _cameraController.SetBlurActive(true);
            case CameraEvent.CameraCommand.Stop:
                return _cameraController.SetBlurActive(false);
            }
            return Observable.Return<bool>(true);
        }

        IObservable<bool> _setSwitchWaveCamera(CameraEvent cameraEvent)
        {
            switch(cameraEvent.Command)
            {
            case CameraEvent.CameraCommand.Play:
                return _cameraController.ShakeBySwitchWave();
            case CameraEvent.CameraCommand.Reset:
                return _zoomOutCameraFinishWave();
            }
            return Observable.Return<bool>(true);
        }

        public void Dispose()
        {
            _cameraController.Clear();
            _disposables.Dispose();   
        }
    }
}
