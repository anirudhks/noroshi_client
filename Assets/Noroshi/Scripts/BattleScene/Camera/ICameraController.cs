﻿using UniRx;

namespace Noroshi.BattleScene.Camera
{
    public interface ICameraController
    {
        /// Blur のオンオフをセットする。
        IObservable<bool> SetBlurActive(bool active);
        IObservable<bool> ZoomIn(float originalSize, float duration);
        IObservable<bool> ZoomOut(float duration);
        IObservable<bool> MoveX(float x, float duration);
        IObservable<bool> Move(UnityEngine.Vector3 pos, float duration, DG.Tweening.Ease ease);
        IObservable<bool> Shake();
        IObservable<bool> ShakeByAction();
        IObservable<bool> ShakeBySwitchWave();
        void ClearShake();
        void EnterTimeStop();
        void ExitTimeStop();
        IObservable<bool> MoveAndZoomWithActiveAction(Character targetCharacter, float duration);
        void SetNextWavedefaultCameraVertices();

        void SetScreenEndCharacters(System.Collections.Generic.Dictionary<Noroshi.Grid.Direction, Character> characters);
        void Clear();
        void PauseOnWithBattleSituationCamera();
        void PauseOffWithBattleSituationCamera();
    }
}
