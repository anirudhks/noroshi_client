﻿using UniRx;
using Noroshi.BattleScene.UI;
using System;

namespace Noroshi.BattleScene.Camera
{
    public class CpuBattleCameraManager : AbstractCameraManager
    {
        public override void Prepare()
        {
            base.Prepare();
            var battleManager = (CpuBattleManager)SceneContainer.GetBattleManager();
            if (battleManager.GetBeforeBattleStory() != null)
            {
                // バトル前ストーリーが存在する場合、通常よりも右ポジションから場面がスタートするので、カメラもその位置へ移動しておき、
                // （仮想的に Wave0 とするが、Wave0 にはゲームロジックが存在しない）
                byte waveNoForStory = 0;
                _cameraController.MoveX(WaveField.GetPositionX(waveNoForStory), 0.001f);
                battleManager.EnterFieldSwitchWave(waveNoForStory, 0.001f);
                // ストーリー後に Wave1 へカメラを動かす。
                byte nextWaveNo = 1;
                SceneContainer.GetSceneManager().GetOnExitBeforeBattleStoryObservable()
                .SelectMany(_ => 
                {
                    battleManager.EnterFieldSwitchWave();
                    return _moveCameraSwitchWave(nextWaveNo);
                })
                .SelectMany(_ => 
                {
                    foreach (var ownCharacter in SceneContainer.GetCharacterManager().CurrentOwnCharacterSet.GetCharacters())
                    {
                        ownCharacter.SetVisible(true, true);
                    }
                    return Observable.WhenAll(
                        _cameraController.ShakeBySwitchWave(),
                        SceneContainer.GetBattleManager().RunOwnCharactersToCorrectPosition().Select(__ => true));
                })
                .Subscribe(_ => SceneContainer.GetSceneManager().ExitReady()).AddTo(_disposables);
            }
        }
    }
}
