using System.Collections.Generic;
using Vector2 = UnityEngine.Vector2;
using UniRx;
using Noroshi.Grid;
using Noroshi.Character;
using Noroshi.BattleScene.Actions;
using Noroshi.MonoBehaviours;

namespace Noroshi.BattleScene
{
    public enum CenterPositionForUIType
    {
        Story = 1,
        Result = 2,
    }

    public interface ICharacterView : IView, Actions.IActionExecutorView
    {
        IObservable<bool> GetOnExitActionAnimationObservable();
        IObservable<bool> GetOnExecuteActionObservable();
        IObservable<bool> GetOnRestartTimeObservable();
        IObservable<BulletHitEvent> GetOnHitBulletObservable();
        IObservable<uint> GetOnPlaySoundObservable();
        IObservable<uint> GetOnCancelSoundObservable();
        IEnumerable<AnimationInformation> GetActionAnimationInformations();
        IObservable<ICharacterView> GetOnStockObservable();

        void PauseOn();
        void PauseOff();
        void SetSpeed(float speed);

        void PlayIdle();
        void PlayWalk();
        IObservable<bool> WalkTo(Vector2 position, float duration);
        IObservable<bool> RunTo(Vector2 position, float duration);
        void PlayAction(CharacterAnimationEvent actionAnimationEvent);
        void PlayDamage();
        void PlayStop(bool stopAnimationIsLoop);
        void PlayKnockback(Vector2 position, float duration, float diffrenceY);
        void PlayTornado();
        IObservable<bool> PlayAnimationAtEnterDead();
        IObservable<bool> PlayEscape();
        IObservable<bool> PlayWin();

        void Resurrect();

        void SetAnimationSounds(Core.WebApi.Response.Master.CharacterAnimationSound[] animationSounds);
        void SetSkin(byte skinLevel);
        void SetSkinName(string skinName);
        void SetHorizontalDirection(Direction direction);
        void SetOrderInLayer(int orderInLayer);

        void SetEscapeBeforeDeadOn();
        void PlayEscapeBeforeDead();

        IActionTargetView GetActionTargetView();
        void SetUpForUI(CenterPositionForUIType centerPositionForUIType);

        void SetColor(UnityEngine.Color color);

        void Stock();

        void SetVisible(bool isVisible);

        void PlayTransparency(bool isOn, float duration);
    }
}
