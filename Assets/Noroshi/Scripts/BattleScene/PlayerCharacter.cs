﻿using UniRx;
using Noroshi.BattleScene.Actions.Attributes;
using BattleCharacterType = Noroshi.Core.Game.Battle.BattleCharacterType;

namespace Noroshi.BattleScene
{
    /// プレイヤーキャラクターを扱うクラス。
    public class PlayerCharacter : Character
    {
        /// サーバーから受け取った BattleCharacter でインスタンス化。
        public PlayerCharacter(IAttributeAssetFactory assetFactory, Core.WebApi.Response.Battle.BattleCharacter battleCharacter) : base(assetFactory)
        {
            _battleCharacter = battleCharacter;
        }

        /// プレイヤーキャラクター ID。
        public uint PlayerCharacterID { get { return _battleCharacter.ID; } }
        public bool IsCpuCharacter { get { return (BattleCharacterType)_battleCharacter.Type == BattleCharacterType.Cpu; } }
    }
}
