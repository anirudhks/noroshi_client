﻿using UniRx;
using Noroshi.BattleScene.Actions;
using Noroshi.BattleScene.Actions.Attributes;
using Noroshi.BattleScene.CharacterEffect;
using Noroshi.BattleScene.Sound;
using Noroshi.Core.Game.Battle;
using Noroshi.Core.Game.Sound;

namespace Noroshi.BattleScene
{
    /// Character から切り出した ActionEvent を扱う機能をまとめたクラス。
    public class CharacterActionEventHandler
    {
        readonly ICharacter _character;
        AttributeHandler _attributeHandler;
        readonly Subject<int> _onAddDamageSubject = new Subject<int>();
        readonly Subject<CharacterEffectEvent> _onCommandCharacterEffectSubject = new Subject<CharacterEffectEvent>();
        readonly Subject<SoundEvent> _onCommandSoundSubject = new Subject<SoundEvent>();
        readonly CompositeDisposable _disposables = new CompositeDisposable();
        readonly CompositeDisposable _attributeDisposables = new CompositeDisposable();

        public CharacterActionEventHandler(ICharacter character, IAttributeAssetFactory assetFactory)
        {
            _character = character;
            _attributeHandler = new AttributeHandler(character, assetFactory);
            _attributeHandler.GetOnAddAttributeObservable().Subscribe(attribute =>
            {
                _commandCharacterEffect(CharacterEffectCommand.Play, attribute.CharacterEffectID);
                _commandSound(SoundCommand.Play, attribute.SoundID);
            })
            .AddTo(_disposables);
            _attributeHandler.GetOnRemoveAttributeObservable().Subscribe(attribute =>
            {
                _commandCharacterEffect(CharacterEffectCommand.Stop, attribute.CharacterEffectID);
                _commandSound(SoundCommand.Cancel, attribute.SoundID);
            })
            .AddTo(_disposables);
            // 独自キャラクターエフェクトイベント紐付け。
            _attributeHandler.GetOnCommandCharacterEffectObservable().Subscribe(_onCommandCharacterEffectSubject.OnNext).AddTo(_disposables);
            // 独自サウンドイベント紐付け。
            _attributeHandler.GetOnCommandSoundObservable().Subscribe(_onCommandSoundSubject.OnNext).AddTo(_disposables);
        }
        /// ActionEvent 経由でダメージを与える毎にダメージ数値がプッシュされる Observable を取得。
        public IObservable<int> GetOnAddDamageObservable() { return _onAddDamageSubject.AsObservable(); }
        /// ActionEvent 経由で CharacterEffect を操作する毎に命令がプッシュされる Observable を取得。
        public IObservable<CharacterEffectEvent> GetOnCommandCharacterEffectObservable() { return _onCommandCharacterEffectSubject.AsObservable(); }
        /// ActionEvent 経由で Sound を操作する毎に命令がプッシュされる Observable を取得。
        public IObservable<SoundEvent> GetOnCommandSoundObservable() { return _onCommandSoundSubject.AsObservable(); }
        /// シールド残割合がプッシュされる Observable を取得。
        public IObservable<ChangeableValueEvent> GetOnChangeShieldRatioObservable() { return _attributeHandler.GetOnChangeHPObservable(); }

        public IObservable<AttributeHandler> LoadData()
        {
            return _attributeHandler.LoadDatas();
        }

        /// Attribute を全て外す。
        public void RemoveAttributes()
        {
            _attributeDisposables.Clear();
            _attributeHandler.RemoveAttributes();
        }

        /// Action 実行者が ActionEvent を送る際の処理。
        public void SendActionEvent(ActionEvent actionEvent)
        {
            if (actionEvent.HPDamage.HasValue)
            {
                var damage = actionEvent.HPDamage.Value;
                if (damage > 0)
                {
                    _character.Energy.RecoverWhenSendDamage(damage, actionEvent.Target.MaxHP, actionEvent.Target.IsDead, actionEvent.Executor.EnergyHealBonus);
                    // HP 奪取
                    if (actionEvent.DamageType == DamageType.Physical)
                    {
                        var recoveryHp = damage * _character.LifeStealRating / (_character.LifeStealRating + actionEvent.Target.Level + 100);
                        _character.HP.RecoverWhenLifeSteal((uint)recoveryHp);
                    }
                    _onAddDamageSubject.OnNext(damage);
                }
            }
        }

        /// Action 対象者が ActionEvent を受ける際の処理。
        public TargetStateID? ReceiveActionEvent(ActionEvent actionEvent)
        {
            TargetStateID? stateId = null;
            _attributeHandler.ReceiveActionEvent(actionEvent);
            if (actionEvent.HPDamage.HasValue)
            {
                var damage = actionEvent.HPDamage.Value;
                if (damage > 0)
                {
                    // 与ダメージ係数があれば適用。
                    if (actionEvent.Executor != null && actionEvent.Executor.DamageCoefficient.HasValue) damage = (int)(damage * actionEvent.Executor.DamageCoefficient.Value);
                    _character.Energy.RecoverWhenReceiveDamage(damage, _character.HP.Max);
                }
                else if (damage < 0)
                {
                    // 回復上昇。
                    damage = (int)(damage * (1 + (float)_character.ImproveHealings / 100));
                }
                _character.HP.Damage(damage);
                if (actionEvent.TargetStateID.HasValue) stateId = actionEvent.TargetStateID.Value;

                if (actionEvent.HitCharacterEffectID.HasValue)
                {
                    var type = actionEvent.IsInterruptHitEffect ? CharacterEffectCommand.Interrupt : CharacterEffectCommand.Play;
                    _commandCharacterEffect(type, actionEvent.HitCharacterEffectID.Value);
                }
            }
            if (actionEvent.EnergyDamage.HasValue)
            {
                var damage = actionEvent.EnergyDamage.Value;
                _character.Energy.Damage(damage);

                if (actionEvent.HitCharacterEffectID.HasValue)
                {
                    var type = actionEvent.IsInterruptHitEffect ? CharacterEffectCommand.Interrupt : CharacterEffectCommand.Play;
                    _commandCharacterEffect(type, actionEvent.HitCharacterEffectID.Value);
                }
            }
            if (actionEvent.HitSoundID.HasValue)
            {
                _onCommandSoundSubject.OnNext(new SoundEvent(){
                    SoundID = actionEvent.HitSoundID.Value,
                    Command = SoundCommand.Play,
                });
            }
            if (actionEvent.Dodge)
            {
                _commandSound(SoundCommand.Play, SoundConstant.BATTLE_DODGE_SOUND_ID);
                _commandCharacterEffect(CharacterEffectCommand.Play, Constant.DODGE_CHARACTER_EFFECT_ID);
            }
            if (actionEvent.Attributes != null)
            {
                foreach (var attribute in actionEvent.Attributes)
                {
                    // 悪い状態異常でアクションレベルよりも相手のレベルが高い場合は失敗することもある
                    if (attribute.IsNegative && actionEvent.ActionLevel.HasValue)
                    {
                        var levelDiff = actionEvent.Target.Level - actionEvent.ActionLevel.Value;
                        if (attribute.MagicalAttribute.HasValue)
                        {
                            var attributeEffect = new MagicalAttributeEffect();
                            levelDiff -= attributeEffect.GetAttributeLevel(attribute.MagicalAttribute.Value, actionEvent.Target.TagSet.GetActionTargetAttributes());
                        }
                        if (levelDiff > 0)
                        {
                            var failRatio = 1 - System.Math.Pow(Constant.ATTRIBUTE_LEVEL_BASE, levelDiff);
                            if (GlobalContainer.RandomGenerator.GenerateFloat() < failRatio)
                            {
                                continue;
                            }
                        }
                    }

                    _attributeHandler.AddAttribute(attribute).Subscribe(_ => {}).AddTo(_attributeDisposables);
                }
            }

            if (actionEvent.Executor != null)
            {
                actionEvent.Executor.SendActionEvent(actionEvent);
            }
            return stateId;
        }

        /// 他ターゲットが死亡した際の処理。
        public void OtherDie(IActionTarget deadTarget)
        {
            _attributeHandler.OtherDie(deadTarget);
        }

        public bool HasMissDamage()
        {
            return _attributeHandler.HasMissDamage();
        }

        void _commandCharacterEffect(CharacterEffectCommand command, uint? characterEffectId)
        {
            if (!characterEffectId.HasValue) return;
            _onCommandCharacterEffectSubject.OnNext(new CharacterEffectEvent
            {
                Command = command,
                CharacterEffectID = characterEffectId.Value,
            });
        }
        void _commandSound(SoundCommand command, uint? soundId)
        {
            if (!soundId.HasValue) return;
            _onCommandSoundSubject.OnNext(new SoundEvent
            {
                Command = command,
                SoundID = soundId.Value,
            });
        }

        public interface ICharacter : IActionTarget
        {
            CharacterHP HP { get; }
            CharacterEnergy Energy { get; }
            uint LifeStealRating { get; }
            byte ImproveHealings { get; }
        }
    }
}
