using System;
using System.Collections.Generic;
using Vector2 = UnityEngine.Vector2;
using UniLinq;
using UniRx;
using Noroshi.Core.Game.Character;
using Noroshi.Core.Game.Action;
using Noroshi.Core.Game.Sound;
using Noroshi.Grid;
using Noroshi.BattleScene.CharacterState;
using Noroshi.BattleScene.Actions;
using Noroshi.BattleScene.Actions.Attributes;
using Noroshi.BattleScene.Actions.Roles;
using Noroshi.BattleScene.CharacterEffect;
using Noroshi.BattleScene.Sound;
using Noroshi.Core.Game.Battle;

namespace Noroshi.BattleScene
{
    public enum Force
    {
        Own,
        Enemy
    }

    public enum ActiveActionType
    {
        Normal,
        ReductionGauge,
        Accumulation,
    }

    /// キャラクターを表現するクラス。
    public abstract class Character : ViewModel<ICharacterView>, IActionExecutor, IActionTarget, IGridContent, CharacterActionEventHandler.ICharacter
    {
        /// 所属勢力（敵 or 味方）
        public Force Force { get; private set; }
        /// 一時的な所属勢力（敵 or 味方）
        public Force CurrentForce { get; private set; }
        /// キャラクター番号（同一キャラクターセットの中で一意に割り当てられる）
        byte? _no = null;
        /// サーバーから受け取った非マスターな情報。
        protected Core.WebApi.Response.Battle.BattleCharacter _battleCharacter;
        /// キャラクターステータス。
        protected CharacterStatus _characterStatus;
        /// HP
        public CharacterHP HP { get; private set; }
        /// エネルギー
        public CharacterEnergy Energy { get; private set; }
        /// 与ダメージ係数。レイドボスバトル時の BP 効果や、特攻に利用。
        public float? DamageCoefficient { get; private set; }
        /// フィールド上におけるグリッド座標（配置されていない場合は null）
        GridPosition? _gridPosition = null;
        /// フィールド上における向き（配置されていない場合は null）
        Direction? _direction = null;
        /// キャラクター状態遷移操作オブジェクト。
        readonly CharacterStateHandler _stateHandler = new CharacterStateHandler();
        /// 保有アクションを扱うオブジェクト。
        protected ActionHandler _actionHandler;
        /// アクションイベント操作オブジェクト。
        CharacterActionEventHandler _actionEventHandler;
        /// 通常アクション発動のためのゲージ。
        float _automaticActionGaugeRatio = 0;
        /// 自動的にアクティブアクションを発動するかフラグ。
        bool _autoActiveAction = false;
        /// 分身処理用オブジェクト。
        public readonly ShadowHandler ShadowHandler = new ShadowHandler();
        /// 停止中カウンタ。
        int _pauseCounter = 0;
        /// アクティブアクション発動可能時に true が、発動不可能時に false が OnNext される Subject。
        readonly Subject<bool> _onToggleActiveActionAvailable = new Subject<bool>();
        /// キャラクターエフェクト操作時に操作内容が OnNext される Subject。
        readonly Subject<CharacterEffectEvent> _onCommandCharacterEffect = new Subject<CharacterEffectEvent>();
        /// 時間停止解除時に解除キャラクターが OnNext される Subject。
        readonly Subject<Character> _onExitTimeStopSubject = new Subject<Character>();
        /// 死亡演出が完了した際に死亡キャラクターが OnNext される Subject。
        protected Subject<Character> _onExitDeadAnimationSubject;
        // チャージ系アクティブアクションの攻撃開始時に OnNext される Subject
        protected Subject<bool> _onExecuteAccumulationActionSubject = new Subject<bool>();
        /// アニメーション連動サウンドを再生する際に再生 Sound ID が OnNext される Subject。
        Subject<uint> _onPlayAnimationSoundSubject = new Subject<uint>();
        /// アニメーション連動サウンドをキャンセルする際に対象 Sound ID が OnNext される Subject。
        Subject<uint> _onCancelAnimationSoundSubject = new Subject<uint>();
        // キャラクターの表示非表示を変更した時に OnNext される Subject.
        Subject<bool> _onSetVisible = new Subject<bool>();
        // チャージアクションがMAXになった時に OnNext される Subject.
        Subject<bool> _onMaxAccumulationSubject = new Subject<bool>();
        /// View 以外の disposables。
        protected readonly CompositeDisposable _disposablesWithoutView = new CompositeDisposable();
        /// View の disposables。
        protected readonly CompositeDisposable _disposablesForView = new CompositeDisposable();

        // キャラのステートを一時的に保存する
        TargetStateID _targetStateId = TargetStateID.Damage;
        float _damageStateDuration = 0.0f;
        // アクティブアクション発動を予約したタイミングで自身を OnNext する Subject
        Subject<Character> _onReserveTryingActiveActionSubject = new Subject<Character>();
        // 瀕死状態から死亡状態へ遷移中
        bool _isTransitingToDeadFromApparentDeath = false;

        readonly IAttributeAssetFactory _assetFactory;
        ICharacterView _originalView;

        public Character(IAttributeAssetFactory assetFactory)
        {
            _assetFactory = assetFactory;
            _actionEventHandler = new CharacterActionEventHandler(this, assetFactory);
        }
        void _subscribeWithoutViewObservable()
        {
            _disposablesWithoutView.Clear();
            // 状態遷移イベント設定
            var stateTypeToActionMap = new Dictionary<Type, Action>
            {
                { typeof(CharacterStateHandler.IdleState), _idleOnEnter },
                { typeof(CharacterStateHandler.ProceedState), _proceedOnEnter },
                { typeof(CharacterStateHandler.AutomaticActionState), _actionOnEnter },
                { typeof(CharacterStateHandler.ActiveActionState), _activeActionOnEnter },
                { typeof(CharacterStateHandler.DamageState), _damageOnEnter },
                { typeof(CharacterStateHandler.DeadState), _deadOnEnter },
                { typeof(CharacterStateHandler.ApparentDeathState), _apparentDeathOnEnter }
            };
            _stateHandler.GetOnEnterStateObservable().Where(state => stateTypeToActionMap.ContainsKey(state.GetType()))
            .Subscribe(state => stateTypeToActionMap[state.GetType()].Invoke()).AddTo(_disposablesWithoutView);

            // アクティブアクション状態終了時には時間停止も終了するようにする。死亡などでアクティブアクションを正常に終了できなかった場合のため。
            _stateHandler.GetOnExitStateObservable().Where(state => state.GetType() == typeof(CharacterStateHandler.ActiveActionState))
            .Subscribe(_ => _onExitTimeStopSubject.OnNext(this)).AddTo(_disposablesWithoutView);

            // アクション系状態開始時に PreProcess を実行。
            _stateHandler.GetOnEnterActionObservable().Subscribe(_ => _actionHandler.PreProcess(this)).AddTo(_disposablesWithoutView);
            // アクション系状態終了時に PostProcess を実行。
            _stateHandler.GetOnExitActionObservable().Subscribe(_ => _actionHandler.PostProcess(this)).AddTo(_disposablesWithoutView);

            // アクションイベント操作オブジェクトによるキャラクターエフェクト命令を紐付ける。
            _actionEventHandler.GetOnCommandCharacterEffectObservable().Subscribe(e => _onCommandCharacterEffect.OnNext(e)).AddTo(_disposablesWithoutView);

            // アクションによるアニメーション再生命令を紐付ける。
            _actionHandler.GetOnCommandExecutorAnimationObservable().Subscribe(ae => _view.PlayAction(ae)).AddTo(_disposablesWithoutView);

            // チャージ攻撃時のチャージ完了処理を紐づける
            _actionHandler.GetOnMaxAccumulationObservable()
            .Subscribe(_ => 
            {
                // チャージが終了した場合、自動バトル時はそのまま攻撃へ移行
                if (_autoActiveAction)
                {
                    AffectCurrentAction();
                }
                else
                {
                    // 自動バトルではない時はUIにチャージ完了を通知
                    _onMaxAccumulationSubject.OnNext(true);
                }
            })
            .AddTo(_disposablesWithoutView);
            // チャージ攻撃時のチャージ終了時の処理を紐づける
            _actionHandler.GetOnExitAccumulationTimeObservable()
            .Subscribe(_ => ExecuteOrCancelAccumulationAction()).AddTo(_disposablesWithoutView);
        }
        public IObservable<Character> LoadDatas()
        {
            return _loadCharacterStatus()
            .Do(characterStatus =>
            {
                _characterStatus = characterStatus;
                _actionHandler   = new ActionHandler(characterStatus.NullableActionIDs, characterStatus.NullableActionLevels, characterStatus.ActionSequence, characterStatus.GetMaxAvailableActionRank());
                HP     = _buildHP(_characterStatus.MaxHP, _characterStatus.HPRegen);
                Energy = new CharacterEnergy(_characterStatus.EnergyRegen);
                _applyInitialCondition();
            })
            .SelectMany(_ => _actionHandler.LoadDatas())
            .SelectMany(_ => _actionEventHandler.LoadData())
            .Select(_ => this);
        }
        protected virtual CharacterHP _buildHP(uint maxHp, uint hpRegen)
        {
            return new CharacterHP(maxHp, hpRegen);
        }
        /// キャラクターステータスをロードする。子クラス毎にロード内容を実装する。
        protected virtual IObservable<CharacterStatus> _loadCharacterStatus()
        {
            var masterData = GlobalContainer.MasterManager.CharacterMaster.Get(_battleCharacter.CharacterID);
            var characterStatus = new BattleScene.CharacterStatus(_battleCharacter, masterData);
            return Observable.Return(characterStatus);
        }
        /// サーバーからロードした初期設定を適用する。
        void _applyInitialCondition()
        {
            if (_battleCharacter == null) return;
            if (_battleCharacter.FixedMaxHP.HasValue) HP.ChangeMaxHP(_battleCharacter.FixedMaxHP.Value);
            if (_battleCharacter.InitialHP.HasValue) HP.ForceInitialize(_battleCharacter.InitialHP.Value);
            if (_battleCharacter.InitialEnergy.HasValue) Energy.ForceInitialize(_battleCharacter.InitialEnergy.Value);
            if (_battleCharacter.DamageCoefficient.HasValue) DamageCoefficient = _battleCharacter.DamageCoefficient.Value;
        }

        public virtual IObservable<Character> LoadAssets(IActionFactory actionFactory)
        {
            return LoadView().Do(_ =>
            {
                if (_direction.HasValue) _view.SetHorizontalDirection(_direction.Value);
            })
            .SelectMany(v => _actionHandler.LoadAssets(this, actionFactory, v.GetActionAnimationInformations()))
            .Select(_ => this);
        }
        protected override IObservable<ICharacterView> _loadView()
        {
            return SceneContainer.GetFactory().BuildCharacterView(CharacterID).Do(view =>
            {
                view.SetSkin(_characterStatus.SkinLevel);
                view.SetAnimationSounds(_characterStatus.AnimationSounds);
            });
        }
        void _initializeView()
        {
            _disposablesForView.Clear();
            // 状態遷移操作オブジェクトへ View を引き渡し。
            _stateHandler.SetCharacterView(_view);
            // View に方向を反映。
            _view.SetHorizontalDirection(_direction.Value);
            // View に仕込んである時間停止解除トリガーに本クラス内の Subject を紐付ける。
            _view.GetOnRestartTimeObservable().Subscribe(_ => _onExitTimeStopSubject.OnNext(this)).AddTo(_disposablesForView);
            // View から送られてくるアニメーション連動サウンド再生情報に本クラス内の Subject を紐付ける。
            _view.GetOnPlaySoundObservable().Subscribe(_onPlayAnimationSoundSubject.OnNext).AddTo(_disposablesForView);
            _view.GetOnCancelSoundObservable().Subscribe(_onCancelAnimationSoundSubject.OnNext).AddTo(_disposablesForView);
            // アクションアニメーション終了時に該当処理を紐付ける。
            _view.GetOnExitActionAnimationObservable().Subscribe(_onExitActionAnimation).AddTo(_disposablesForView);
            // アクションアニメーションに仕込んであるアクション実行トリガーに該当処理を紐付ける。
            _view.GetOnExecuteActionObservable().Subscribe(_ => _onInvokeAction()).AddTo(_disposablesForView);
            // 死亡アニメーション再生後の場合に復活させる。
            _view.Resurrect();
        }
        void _onExitActionAnimation(bool success)
        {
            // アクションアニメーションが割り込みなしで終了した場合、
            if (success)
            {
                // 次のアニメーションがあれば引き続き再生。
                var nextAnimation = _actionHandler.GetCurrentActionNextCharacterAnimationEvent();
                if (nextAnimation.HasValue)
                {
                    _view.PlayAction(nextAnimation.Value);
                    return;
                }
                // アニメーション再生が最後まで成功フラグを立てる。
                _actionHandler.SucceedInPlayAnimation();
                // 瀕死状態、かつ、HP が 0 のままアニメーション終了を迎えた場合は死亡。
                if (_stateHandler.CurrentStateIsApparentDeath() && CurrentHP == 0)
                {
                    _isTransitingToDeadFromApparentDeath = true;
                    if (!_stateHandler.TryToTransitToDead())
                    {
                        _isTransitingToDeadFromApparentDeath = false;
                    }
                }
                else
                {
                    _stateHandler.TryToTransitToIdle();
                }
            }
        }
        void _onInvokeAction()
        {
            if (_stateHandler.IsActionState()) _actionHandler.ExecuteCurrentAction(_getField(), this);
        }

        /// タグ。
        public IActionTagSet TagSet { get { return _characterStatus.TagSet; } }
        /// 隊列（前衛 / 中衛 / 後衛）
        public CharacterPosition CharacterPosition { get { return _characterStatus.Position; } }
        /// 順番優先度。
        public uint OrderPriority { get { return _characterStatus.OrderPriority; } }
        /// 奥行き位置。
        public uint OrderInLayer { get { return _characterStatus.OrderInLayer; } }
        /// キャラクター ID。
        public uint CharacterID { get { return _characterStatus.CharacterID; } }
        /// レベル。
        public ushort Level { get { return _characterStatus.Level; } }
        /// 昇格レベル。
        public byte PromotionLevel { get { return _characterStatus.PromotionLevel; } }
        /// 進化レベル。
        public byte EvolutionLevel { get { return _characterStatus.EvolutionLevel; } }
        /// 物理攻撃力。
        public uint PhysicalAttack { get { return _characterStatus.PhysicalAttack; } }
        /// 魔法攻撃力。
        public uint MagicPower { get { return _characterStatus.MagicPower; } }
        /// 物理防御力。
        public uint Armor { get { return _characterStatus.Armor; } }
        /// 魔法防御力。
        public uint MagicResistance { get { return _characterStatus.MagicResistance; } }
        /// 物理クリティカル。
        public uint PhysicalCrit { get { return _characterStatus.PhysicalCrit; } }
        /// 魔法クリティカル。
        public uint MagicCrit { get { return _characterStatus.MagicCrit; } }
        /// 物理防御貫通。
        public uint ArmorPenetration { get { return _characterStatus.ArmorPenetration; } }
        /// 魔法防御貫通。
        public uint IgnoreMagicResistance { get { return _characterStatus.IgnoreMagicResistance; } }
        /// 命中。
        public byte Accuracy { get { return _characterStatus.Accuracy; } }
        /// 回避。
        public byte Dodge { get { return _characterStatus.Dodge; } }
        /// ライフ奪取力。
        public uint LifeStealRating { get { return _characterStatus.LifeStealRating; } }
        /// 回復増加。
        public byte ImproveHealings { get {  return _characterStatus.ImproveHealings; } }
        /// エネルギー回復力ボーナス値.
        public float EnergyHealBonus { get { return _characterStatus.EnergyHealBonus; } }

        public uint MaxHP { get { return (uint)HP.Max; } }
        public uint CurrentHP { get { return (uint)HP.Current; } }
        public ushort MaxEnergy { get { return (ushort)Energy.Max; } }
        public ushort CurrentEnergy { get { return (ushort)Energy.Current; } }
        public byte SkinLevel { get { return _characterStatus.SkinLevel; } }
        public ActiveActionType? ActiveActionType { get { return _actionHandler.GetActiveActionType(); } }

        float  _actionFrequency { get { return 1 / (Constant.IDEAL_ACTION_INTERVAL * Constant.LOGIC_FPS) * _characterStatus.ActionFrequency; } }

        public void AddStatusBoosterFactor   (IStatusBoostFactor factor)
        {
            _characterStatus.AddStatusBoosterFactor(factor);
            HP.ChangeMaxHP(_characterStatus.MaxHP);
        }
        public void RemoveStatusBoosterFactor(IStatusBoostFactor factor) { _characterStatus.RemoveStatusBoosterFactor(factor); }
        public void AddStateTransitionBlockerFactor(StateTransitionBlocker.Factor factor) { _stateHandler.AddBlockerFactor(factor)   ; }
        public void RemoveStateTransitionBlockerFactor(StateTransitionBlocker.Factor factor) { _stateHandler.RemoveBlockerFactor(factor); }
        public void AddStatusBreakerFactor(StatusForceSetter.Factor factor)    { _characterStatus.AddStatusBreakerFactor(factor); }
        public void RemoveStatusBreakerFactor(StatusForceSetter.Factor factor) { _characterStatus.RemoveStatusBreakerFactor(factor); }
        public void AddActionLimiterFactor(Actions.ActionLimiter.Factor factor) { _actionHandler.AddActionLimiterFactor(factor); }
        public void RemoveActionLimiterFactor(Actions.ActionLimiter.Factor factor) { _actionHandler.RemoveActionLimiterFactor(factor); }

        public int GetBaseActionRange()
        {
            return _actionHandler.GetBaseRange();
        }
        public bool IsDead { get { return _stateHandler.CurrentStateIsDead(); } }
        public bool IsTargetable
        {
            get
            {
                var isTargetable = true;
                if (!_actionHandler.IsTargetable()) isTargetable = !_stateHandler.IsActionState();
                return _stateHandler.IsTargetable() && isTargetable; 
            } 
        }
        public bool HasMissDamage()
        {
            return _actionEventHandler.HasMissDamage();
        }

        /// 死亡時に自身が OnNext される Observable を取得。
        public IObservable<Character> GetOnDieObservable() { return _stateHandler.GetOnEnterStateObservable().Where(state => state.GetType() == typeof(CharacterStateHandler.DeadState)).Select(t => this); }
        /// 死亡演出終了後に自身が OnNext される Observable を取得。
        public IObservable<Character> GetOnExitDeadAnimationObservable()
        {
            if (_onExitDeadAnimationSubject == null) _onExitDeadAnimationSubject = new Subject<Character>();
            return _onExitDeadAnimationSubject.AsObservable();
        }
        /// 与ダメージ時にダメージが OnNext される Observable を取得。
        public IObservable<int> GetOnAddDamageObservable() { return _actionEventHandler.GetOnAddDamageObservable(); }
        /// アクティブアクション発動時に自身が OnNext される Observable を取得。
        public IObservable<Character> GetOnEnterActiveActionObservable() { return _stateHandler.GetOnEnterStateObservable().Where(state => state.GetType() == typeof(CharacterStateHandler.ActiveActionState)).Select(t => this); }
        /// TODO : 確認。
        public IObservable<ChangeableValueEvent> GetOnChangeShieldRatioObservable() { return _actionEventHandler.GetOnChangeShieldRatioObservable(); }
        public IObservable<Character> GetOnExitTimeStopObservable() { return _onExitTimeStopSubject.AsObservable(); }
        public IObservable<bool> GetOnToggleActiveActionAvailable() { return _onToggleActiveActionAvailable.AsObservable(); }
        public IObservable<CharacterEffectEvent> GetOnCommandCharacterEffectObservable()
        {
            return _onCommandCharacterEffect.Do(e => e.CharacterView = _view);
        }
        public IObservable<SoundEvent> GetOnCommandSoundObservable()
        {
            return _actionEventHandler.GetOnCommandSoundObservable()
                .Merge(_onPlayAnimationSoundSubject.AsObservable().Select(soundId =>
                {
                    return new SoundEvent
                    {
                        Command = SoundCommand.Play,
                        SoundID = soundId,
                    };
                }))
                .Merge(_onCancelAnimationSoundSubject.AsObservable().Select(soundId =>
                {
                    return new SoundEvent
                    {
                        Command = SoundCommand.Cancel,
                        SoundID = soundId,
                    };
                }));
        }
        public IObservable<Noroshi.BattleScene.Camera.CameraEvent> GetOnTryCommandCameraObservable() { return _actionHandler.GetOnTryCommandCameraObservable(); }
        public IObservable<ChangeableValueEvent> GetOnHPChangeObservable()
        {
            return HP.GetOnChangeObservable().Do(cve =>
            {
                if (cve.Current <= 0) _tryToTransitToApparentDeathOrDead();
            });
        }
        public IObservable<ChangeableValueEvent> GetOnEnergyChangeObservable() { return Energy.GetOnChangeObservable(); }

        public IObservable<CharacterStatusBoostEvent> GetOnChangeStatusBooster()
        {
            return _characterStatus.GetOnChangeStatusBooster();
        }
        /// アクティブアクション状態への遷移試行予約時に予約キャラクターが OnNext される Observable を取得。
        public IObservable<Character> GetOnReserveTryingActiveActionObservable()
        {
            return _onReserveTryingActiveActionSubject.AsObservable();
        }
        public IObservable<bool> GetOnSetVisibleObservable()
        {
            return _onSetVisible.AsObservable();
        }
        public IObservable<bool> GetOnExecuteAccumulationActionObservable()
        {
            return _onExecuteAccumulationActionSubject.AsObservable();
        }
        public IObservable<bool> GetOnMaxAccumulationObservable()
        {
            return _onMaxAccumulationSubject.AsObservable();
        }

        public GridPosition? GetGridPosition() { return _gridPosition; }
        public void SetGridPosition(GridPosition currentGrid)
        {
            _gridPosition = currentGrid;
            _view.SetOrderInLayer((_getField().VerticalSize - currentGrid.VerticalIndex) * Constant.ORDER_RANGE_IN_CHARACTER_LAYER);
        }

        public bool IsFirstHidden { get{ return _actionHandler.HasDelayAppearanceAction(); } }

        public void SetForce(Force force)
        {
            Force = force;
            CurrentForce = force;
            SetHorizontalDirection(_getCurrentForwardDirection());
        }
        public byte No { get { return _no.Value; } }
        public void SetNo(byte no)
        {
            _no = no;
        }
        public Direction GetDirection() { return _direction.Value; }
        public void SetHorizontalDirection(Direction direction)
        {
            _direction = direction;
            if (_view != null) _view.SetHorizontalDirection(_direction.Value);
        }

        /// 死んでいるキャラクターを生き返らせる。
        /// これはあくまでもキャラクターを再利用するために利用するメソッドで、
        /// アクションに組み込んではいけない。
        public void Resurrect()
        {
            _view.Resurrect();
            _stateHandler.ForceReset();
        }
        /// バトルロジック開始。
        public void Start()
        {
            if (_onExitDeadAnimationSubject == null) _onExitDeadAnimationSubject = new Subject<Character>();
            _subscribeWithoutViewObservable();
            _initializeView();
            _stateHandler.Start();
        }
        public void RemoveGridPosition() { _gridPosition = null; }

        public void SendActionEvent(ActionEvent actionEvent)
        {
            _actionEventHandler.SendActionEvent(actionEvent);
        }
        public void ReceiveActionEvent(ActionEvent actionEvent)
        {
            if (!IsDead)
            {
                var stateId = _actionEventHandler.ReceiveActionEvent(actionEvent);
                if (_actionHandler.PrepareNextReceiveEventAction(_getField(), this, actionEvent))
                {
                    _stateHandler.TryToTransitToAutomaticAction(_actionHandler.GetNextCharacterAnimationEvent());
                }
                else if (stateId.HasValue)
                {
                    _targetStateId = stateId.Value;
                    // ダメージ系ステート時の効果時間を渡す必要があるためここで先に取得しておく
                    switch (_targetStateId)
                    {
                    case TargetStateID.Damage:
                        _damageStateDuration = Constant.DAMAGE_TIME;
                        break;
                    case TargetStateID.Knockback:
                    case TargetStateID.MoveScreenEnd:
                        _damageStateDuration = Constant.KNOCKBACK_TIME;
                        break;
                    case TargetStateID.Kachiage:
                        _damageStateDuration = Constant.KACHIAGE_TIME;
                        break;
                    case TargetStateID.Tornado:
                        _damageStateDuration = Constant.TORNADO_FIRST_MOVE_TIME + Constant.TORNADO_SECOND_MOVE_TIME;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                    }
                    _stateHandler.TryToTransitToDamage(_damageStateDuration);
                }
            }
        }
        public IObservable<BulletHitEvent> GetOnHitObservable()
        {
            return _view.GetOnHitBulletObservable().Do(he => he.ActionTarget = this);
        }

        public void SetCurrentForceReverse()
        {
            CurrentForce = Force == Force.Enemy ? Force.Own : Force.Enemy;
        }

        public void SetCurrentForceOriginal()
        {
            CurrentForce = Force;
        }

        /// ウェーブ間遷移時の回復処理。
        public void RecoverAtInterval()
        {
            HP.RecoverWhenMoveNextWave();
            Energy.RecoverWhenMoveNextWave();
        }

        /// 待機状態へ入った際の処理。
        void _idleOnEnter()
        {
            TryToChangeDirection();
        }

        /// 前進状態へ遷移すべきかどうか判定。
        bool _shouldTransitToProceed()
        {
            return _getField().ShouldMoveForward(this, _direction.Value, (ushort)GetBaseActionRange());
        }
        /// 前進状態への遷移を試みる。
        bool _tryToTransitToProceed()
        {
            if (!_shouldTransitToProceed()) return false;
            return _stateHandler.TryToTransitToProceed();
        }
        /// 前進状態へ入った際の処理。
        void _proceedOnEnter()
        {
            _proceed();
        }
        void _proceed()
        {
            _getField().MoveCharacter(this, _direction.Value);
            _view.Move(_getCorrectPosition(), Constant.WALK_TIME_PER_GRID / _characterStatus.ActionFrequency).Subscribe(success =>
            {
                if (_shouldTransitToProceed())
                {
                    if (TryToChangeDirection() && _stateHandler.TryToTransitToIdle()) return;
                    _proceed();
                }
                else
                {
                    _stateHandler.TryToTransitToIdle();
                }
            });
        }

        /// 瀕死状態への遷移を試み、無理なら死亡状態へ遷移する。
        void _tryToTransitToApparentDeathOrDead()
        {
            if (_stateHandler.CurrentStateIsApparentDeath()) return;
            if (_actionHandler.PrepareNextDeadAction(_getField(), this))
            {
                _stateHandler.TryToTransitToApparentDeath();
            }
            else
            {
                _tryToTransitToDead();
            }
        }
        /// 瀕死状態へ入った際の処理。
        void _apparentDeathOnEnter()
        {
            _view.PlayAction(_actionHandler.GetNextCharacterAnimationEvent());
        }
        /// 死亡状態へ遷移する。原則、直接呼び出すのは禁止。
        protected bool _tryToTransitToDead()
        {
            return _stateHandler.TryToTransitToDead();
        }
        /// 死亡状態へ入った際の処理。
        protected virtual void _deadOnEnter()
        {
            Energy.Reset();
            RemoveAttributeAndShadow();
            _disposablesWithoutView.Clear();
            if (_isTransitingToDeadFromApparentDeath)
            {
                _isTransitingToDeadFromApparentDeath = false;
                // 瀕死からそのまま死亡状態へ遷移した際はアニメーションなしでそのままストックへ
                _stock();
            }
            else
            {
                // 内部で OnCompleted() が呼ばれるので Dispose はしなくても良い前提。
                _playDieAnimation().Subscribe(_ => _stock());
            }
        }
        /// 死亡（死亡時に流れるだけで必ずしも死亡アニメーションとは限らず逃走、待機もあり）アニメーションを再生する。
        /// オーバーライドする場合は、終了時に処理を引っ掛けるので演出完了時に OnNext(this) される Observable を返し、OnCompleted() も忘れずに呼ぶこと。
        protected virtual IObservable<Character> _playDieAnimation()
        {
            return _view.PlayAnimationAtEnterDead().Select(_ => this);
        }

        /// オートアクション発動可否チェック。
        bool _canInvokeAutomaticAction()
        {
            return _automaticActionGaugeRatio == 1 && _stateHandler.CanTransitToAutomaticAction();
        }
        /// オートアクションをセットした上でアクション状態への遷移を試みる。
        bool _tryToTransitToAutomaticAction()
        {
            if (!_canInvokeAutomaticAction()) return false;
            if (!_actionHandler.PrepareNextAutomaticAction(_getField(), this)) return false;
            if (_actionHandler.NextAutomaticActionIsLimited())
            {
                // この場合はアクションゲージもリセットしてしまう。
                _automaticActionGaugeRatio = 0;
                return false;
            }
            return _stateHandler.TryToTransitToAutomaticAction(_actionHandler.GetNextCharacterAnimationEvent());
        }
        /// アクション状態へ入った際の処理。
        void _actionOnEnter()
        {
            _automaticActionGaugeRatio = 0;
        }

        /// アクティブアクション発動可否チェック。
        public void CheckActiveActionAvailable()
        {   
            _onNextOnToggleActiveActionAvailable(_canInvokeActiveAction());
        }

        /// アクティブアクション状態への遷移試行を予約する。
        public void ReserveTryingActiveAction()
        {
            if (!_canInvokeActiveAction()) return;
            _onReserveTryingActiveActionSubject.OnNext(this);
        }
        /// アクティブアクション状態への遷移を試みる。
        public bool TryToTransitToActiveAction()
        {
            var canAct = _canInvokeActiveAction();
            // ユーザアクションベースでも更新しておく
            _onNextOnToggleActiveActionAvailable(canAct);
            if (!canAct) return false;
            return _stateHandler.TryToTransitToActiveAction(_actionHandler.GetNextCharacterAnimationEvent());
        }
        /// アクティブアクション発動可否チェック。
        bool _canInvokeActiveAction()
        {
            var isInvokeActiveAction = _actionHandler.GetNecessaryEnergyToEnterActiveAction() <= Energy.Current;
            // エネルギーが規定値以上でないと発動できない
            if (!isInvokeActiveAction) return false;
            // フィールドの見える位置にいないと発動できない
            if (!_getField().IsVisible(GetGridPosition().Value)) return false;
            // チャージ系のアクションでありかつすでにチャージ中の場合はすでに発動中なので新規に発動することはできない
            if (_actionHandler.IsAccumulating()) return false;
            // アクションが実際に発動できるかチェック。
            return _actionHandler.PrepareNextActiveAction(_getField(), this);
        }
        /// アクティブアクション状態へ入った際の処理。
        void _activeActionOnEnter()
        {
            _onPlayAnimationSoundSubject.OnNext(SoundConstant.BATTLE_ENTER_ACTIVE_ACTION_SOUND_ID);

            // ActiveAction 遷移時にエネルギーを消費すべきなら消費する
            if (!_actionHandler.ShouldReduceEnergyWhenEnterActiveAction())
            {
                Energy.Consume(_characterStatus.ReduceEnergyCost * 0.01f);
            }
            _onCommandCharacterEffect.OnNext(new CharacterEffectEvent
            {
                Command           = CharacterEffectCommand.Play,
                CharacterEffectID = Constant.CHARGE_ACTION_CHARACTER_EFFECT_ID,
            });
            if (!_actionHandler.IsAccumulating())
            {
                _onNextOnToggleActiveActionAvailable(false);
            }
        }
        /// アクティブアクション発動可否情報を OnNext する。
        void _onNextOnToggleActiveActionAvailable(bool available)
        {
            _onToggleActiveActionAvailable.OnNext(available);
        }

        /// バトル開始時に発動するアクション呼び出しを試みる（アニメーション有無による違いあり）。
        public void TryToInvokeFirstAction()
        {
            _tryToExecuteFirstActionDirectly();
            _tryToTransitToFirstAction();
        }
        /// バトル開始時に発動するアクション実行を試みる（状態遷移なし）。
        bool _tryToExecuteFirstActionDirectly()
        {
            return _actionHandler.ExecuteFirstActionDirectly(_getField(), this);
        }
        /// バトル開始時に発動するアクションをセットした上でアクション状態への遷移を試みる。
        bool _tryToTransitToFirstAction()
        {
            var canAct = _actionHandler.PrepareNextFirstWithAnimationAction(_getField(), this);
            if (!canAct) return false;
            return _stateHandler.TryToTransitToAutomaticAction(_actionHandler.GetNextCharacterAnimationEvent());
        }
        /// 他のキャラクターが死亡時に発動するアクション実行を試みる（状態遷移なし）。
        bool _tryToExecuteOtherDeadActionDirectly(RelativeForce force)
        {
            return _actionHandler.ExecuteOtherDeadActionDirectly(_getField(), this, force);
        }
        /// 他ターゲットが死亡した際の処理。
        public void OtherDie(IActionTarget deadTarget)
        {
            if (Force != deadTarget.Force)
            {
                _tryToExecuteOtherDeadActionDirectly(RelativeForce.Enemy);
            }
            else
            {
                _tryToExecuteOtherDeadActionDirectly(RelativeForce.Own);
            }

            _actionEventHandler.OtherDie(deadTarget);
        }

        /// （プレイヤーの意思で）現在のアクションに影響を与える。
        /// UI 経由でプレイヤー入力と紐付けられる。
        public void AffectCurrentAction()
        {
            _actionHandler.AffectCurrentAction();
        }

        // チャージ系アクションを攻撃開始時やキャンセル時に呼ばれる
        // エネルギーの減少や、UIへ通知する
        public void ExecuteOrCancelAccumulationAction()
        {
            Energy.Consume(_characterStatus.ReduceEnergyCost);
            _onToggleActiveActionAvailable.OnNext(false);
            _onExecuteAccumulationActionSubject.OnNext(true);
        }

        void _damageOnEnter()
        {
            var distance = 0;
            var diffrenceY = 0.0f;
            switch (_targetStateId)
            {
            case TargetStateID.Damage:
                // ダメージ時はアニメーションのみで移動はなしなのでここでは何もしない
                return;
            case TargetStateID.Knockback:
                distance = Constant.KNOCKBACK_DISTANCE;
                diffrenceY = Constant.KNOCKBACK_DIFFRENCE_Y;
                break;
            case TargetStateID.Kachiage:
                distance = Constant.KACHIAGE_DISTANCE;
                diffrenceY = Constant.KACHIAGE_DIFFRENCE_Y;
                break;
            case TargetStateID.Tornado:
                _view.PlayTornado();
                return;
            case TargetStateID.MoveScreenEnd:
                distance = System.Math.Abs((_getField().GetAvailableStartHorizontalIndex(Force) - _gridPosition.Value.HorizontalIndex)) - 5;
                distance = distance > 0 ? distance : 0;
                diffrenceY = Constant.MOVE_SCREEN_END_DIFFRENCE_Y;
                break;
            default:
                throw new ArgumentOutOfRangeException();
            }
            var direction = _direction.Value == Direction.Right ? Direction.Left : Direction.Right;
            var nextGrid  = GetGridPosition().Value.BuildNextGrid(direction, distance);
            _getField().MoveCharacter(this, nextGrid);
            _view.PlayKnockback(_getCorrectPosition(), _damageStateDuration, diffrenceY);
        }

        /// 停止状態への遷移を試みる。
        public bool TryToTransitToStop(bool isLoopAnimation)
        {
            return _stateHandler.TryToTransitToStop(isLoopAnimation);
        }
        /// 停止状態からの遷移を試みる。
        public bool TryToTransitFromStop()
        {
            return _stateHandler.TryToTransitToIdle();
        }

        public void PlayTransparency(bool isOn, float duration)
        {
            _view.PlayTransparency(isOn, duration);
        }

        public void SetSpeed(float speed)
        {
            _view.SetSpeed(speed);
        }
        public void HorizontalMove(short horizontalDiff, float duration, bool inVisibleArea)
        {
            _getField().MoveCharacterHorizontallyWithView(this, _view, GetDirection(), horizontalDiff, duration, inVisibleArea).Subscribe(_ => {});
        }
        public void HorizontalMove(Func<short> getHorizontalDiff, float duration)
        {
            _getField().MoveCharacterHorizontallyWithView(this, _view, GetDirection(), getHorizontalDiff, duration, true).Subscribe(_ => {});
        }
        public void GoStraight(float duration)
        {
            _getField().MoveCharacterToHorizontalEndWithView(this, _view, GetDirection(), duration).Subscribe(_ => {});
        }
        public void Appear()
        {
            _view.StopMove();
            _getField().MoveCharacterToAvailableStartPositionWithView(this, _view);
            TryToChangeDirection();
        }

        public void SetViewToCorrectPosition()
        {
            if (GetGridPosition().HasValue)
            {
                var correctPosition = _getCorrectPosition();
                _view.SetPosition(correctPosition);
            }
        }
        public IObservable<bool> SetViewToCorrectPositionWithWalking(float duration)
        {
            return _view.WalkTo(_getCorrectPosition(), duration);
        }
        public IObservable<bool> SetViewToCorrectPositionWithRunning(float duration)
        {
            return _view.RunTo(_getCorrectPosition(), duration);
        }

        Vector2 _getCorrectPosition()
        {
            return _getField().GetPosition(GetGridPosition().Value);
        }

        public void SetViewToStoryWavePosition()
        {
            _view.SetPosition(_getField().GetStoryWavePosition(GetGridPosition().Value));
        }


        public IActionTargetView GetActionTargetView()
        {
            return _view.GetActionTargetView();
        }
        public ICharacterView GetView()
        {
            return _view;
        }
        public IActionExecutorView GetViewAsActionExecutorView()
        {
            return (IActionExecutorView)_view;
        }

        public bool TryToChangeDirection()
        {
            var newDirection = _getField().GetProceedDirectionToChange(this);
            if (newDirection.HasValue)
            {
                SetHorizontalDirection(newDirection.Value);
                return true;
            }
            return false;
        }

        /// オートモードセット。
        public void SetAuto(bool auto)
        { 
            _autoActiveAction = auto;
        }
        /// オートモード時処理。
        void _autoModeProcess()
        {
            if (!_autoActiveAction) return;
            ReserveTryingActiveAction();
        }

        /// ロジック更新ループ。FPS で数えられるループと一致しない頻度のループであることに注意。
        public void LogicUpdate()
        {
            if (!_stateHandler.CanLogicUpdate()) return;

            if (_pauseCounter > 0) return;

            _autoModeProcess();
            _tryToChargeNormalActionGauge();
            _tryToTransitToAutomaticAction();
            _tryToTransitToProceed();
        }
        /// 通常アクションゲージのチャージを試みる。
        void _tryToChargeNormalActionGauge()
        {
            if (!_stateHandler.CanChargeAutomaticActionGauge()) return;
            _automaticActionGaugeRatio += _actionFrequency;
            if (_automaticActionGaugeRatio > 1) _automaticActionGaugeRatio = 1;
        }

        public void PauseOn()
        {
            if (_pauseCounter++ > 0) return;
            _actionHandler.PauseOn();
            _view.PauseOn();
        }
        public void PauseOff()
        {
            if (--_pauseCounter > 0) return;
            _view.PauseOff();
            _actionHandler.PauseOff();
        }

        public void SetVisible(bool isVisible, bool isOnlyCharacterView)
        {
            GetView().SetVisible(isVisible);
            _onSetVisible.OnNext(isVisible);
            if (!isOnlyCharacterView) _actionHandler.SetVisible(isVisible);
        }

        public CharacterThumbnail BuildCharacterThumbnail()
        {
            // TODO
            return new CharacterThumbnail(CharacterID, (ushort)_characterStatus.Level, (byte)_characterStatus.EvolutionLevel, (byte)_characterStatus.PromotionLevel, _characterStatus.SkinLevel, IsDead);
        }

        public ushort? GetActiveActionLevel()
        {
            return _actionHandler.GetActiveActionLevel();
        }
        public uint? GetActiveActionID()
        {
            return _actionHandler.GetActiveActionID();
        }

        public int GetShadowNum()
        {
            return ShadowHandler.GetShadowNum();
        }
        public IShadow BuildShadow(uint shadowCharacterId)
        {
            return new ShadowCharacter(_assetFactory, shadowCharacterId);
        }
        public IObservable<ShadowCharacter> MakeShadow(IShadow shadowCharacter, ushort characterLevel, ushort actionLevel, ushort? actionLevel4, byte promotionLevel, byte evolutionLevel, byte? maxAvailableActionRank, ushort initialHorizontalIndex, ushort? baseVerticalIndex, byte? skinLevel)
        {
            return ShadowHandler.MakeShadow((ShadowCharacter)shadowCharacter, Force, characterLevel, actionLevel, actionLevel4, promotionLevel, evolutionLevel, maxAvailableActionRank, initialHorizontalIndex, baseVerticalIndex, skinLevel);
        }

        Direction _getCurrentForwardDirection()
        {
            return WaveField.GetForwardDirection(Force);
        }
        WaveField _getField()
        {
            // TODO : 抽象化
            return SceneContainer.GetBattleManager().CurrentWave.Field;
        }

        public void RemoveAttributeAndShadow()
        {
            _actionEventHandler.RemoveAttributes();
            ShadowHandler.Clear();
        }

        public void PrepareNextWave()
        {
            _stateHandler.Reset();
            _automaticActionGaugeRatio = 0;
            _actionHandler.Reset(this);
            SetHorizontalDirection(_getCurrentForwardDirection());
        }

        /// 待機状態への遷移を試みる。ただし、Wave バトル終了時限定処理として、前進状態以外は中断しない。
        /// 次の待機状態への遷移時に OnNext される Observable を返す。
        public IObservable<Character> TryToTransitToIdleOrDeadOnExitWaveBattle()
        {
            // チャージ系のアクションの時ここで強制実行させる
            if (_actionHandler.IsAccumulating() && !IsDead) _actionHandler.AffectCurrentAction();
            return _stateHandler.TryToTransitToIdleOnExitWaveBattle() ? Observable.Return(this) : _stateHandler.GetOnEnterStateObservable()
                .First(state => state.GetType() == typeof(CharacterStateHandler.IdleState) || state.GetType() == typeof(CharacterStateHandler.DeadState))
                .Select(_ => this);
        }
        /// 勝利状態への遷移を試みる。
        public bool TryToTransitToWin()
        {
            return _stateHandler.TryToTransitToWin();
        }

        public void ChangeSkinTemporarily(string skinName)
        {
            _view.SetSkinName(skinName);
        }
        public void ResetSkin()
        {
            _view.SetSkin(SkinLevel);
        }

        /// 一時的に View を変更をする。
        public void ChangeViewTemporarily(ICharacterView view)
        {
            _view.SetActive(false);
            if (_originalView == null) _originalView = _view;
            _view = view;
            _initializeView();
            _stateHandler.Reset();
            _stateHandler.Start();
            SetViewToCorrectPosition();
            _view.SetActive(true);

            // 変身エフェクト再生
            _onCommandCharacterEffect.OnNext(new CharacterEffectEvent
            {
                Command           = CharacterEffectCommand.Play,
                CharacterEffectID = Constant.TRANSFORMATION_EFFECT_ID,
            });
        }
        /// 変更した view を元に戻す。変更後に、さらに別の view に変更していた場合は後者を優先し、元には戻らない。
        public void ResetView(ICharacterView view)
        {
            if (view == null) throw new InvalidOperationException();
            if (view != _view)
            {
                view.Stock();
                return;
            }
            _forceResetView();
        }
        /// 変更した view を元に戻す。
        protected void _forceResetView()
        {
            if (_originalView == null) return;
            _view.Stock();
            _view = _originalView;
            _originalView = null;
            _initializeView();
            _stateHandler.Reset();
            if (!IsDead) _stateHandler.Start();
            SetViewToCorrectPosition();
            _view.SetActive(true);

            // 変身エフェクト再生
            _onCommandCharacterEffect.OnNext(new CharacterEffectEvent
            {
                Command           = CharacterEffectCommand.Play,
                CharacterEffectID = Constant.TRANSFORMATION_EFFECT_ID,
            });
        }

        protected void _stock()
        {
            _disposablesForView.Clear();
            _view.Stock();
            HP.Reset();
            Energy.Reset();
            _automaticActionGaugeRatio = 0;
            _onExitDeadAnimationSubject.OnNext(this);
            _onExitDeadAnimationSubject.OnCompleted();
            _onExitDeadAnimationSubject = null;
        }

        public override void Dispose()
        {
            RemoveAttributeAndShadow();
            _stateHandler.Dispose();
            _actionHandler.Dispose();
            _disposablesWithoutView.Dispose();
            _disposablesForView.Dispose();
            base.Dispose();
        }
    }
}
