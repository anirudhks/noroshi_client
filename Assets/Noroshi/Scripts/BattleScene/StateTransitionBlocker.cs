﻿using System.Collections.Generic;
using UniLinq;

namespace Noroshi.BattleScene
{
    /// <summary>
    /// 状態遷移をブロックするクラス。
    /// </summary>
    public class StateTransitionBlocker
    {
        /// <summary>
        /// 効果発動中状態遷移ブロック因子。
        /// </summary>
        List<Factor> _factors = new List<Factor>();
        /// <summary>
        /// 前進状態への遷移ブロックフラグ。
        /// </summary>
        public bool ProceedBlock { get; private set; }
        /// <summary>
        /// アクティブアクション状態への遷移ブロックフラグ。
        /// </summary>
        public bool ActiveActionBlock { get; private set; }

        /// <summary>
        /// 状態遷移ブロック因子を追加。
        /// </summary>
        /// <param name="factor">追加する状態遷移ブロック因子</param>
        public void AddFactor(Factor factor)
        {
            _factors.Add(factor);
            _calculate();
        }
        /// <summary>
        /// 状態遷移ブロック因子を削除。
        /// </summary>
        /// <param name="factor">削除する状態遷移ブロック因子</param>
        public void RemoveFactor(Factor factor)
        {
            _factors.Remove(factor);
            _calculate();
        }

        /// <summary>
        /// 効果発動中状態遷移ブロック因子を踏まえて結果をキャッシュする。
        /// </summary>
        void _calculate()
        {
            ProceedBlock = _factors.Any(f => f.Proceed);
            ActiveActionBlock = _factors.Any(f => f.ActiveAction);
        }

        /// <summary>
        /// 状態遷移ブロック因子。
        /// </summary>
        public struct Factor
        {
            /// <summary>
            /// 前進状態への遷移ブロックフラグ。
            /// </summary>
            public bool Proceed { get; set; }
            /// <summary>
            /// アクティブアクション状態への遷移ブロックフラグ。
            /// </summary>
            public bool ActiveAction { get; set; }
        }
    }
}
