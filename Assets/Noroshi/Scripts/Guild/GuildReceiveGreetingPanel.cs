﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class GuildReceiveGreetingPanel : MonoBehaviour {
        [SerializeField] BtnCommon btnCloseReceiveGreeting;
        [SerializeField] GameObject greetingPlayerWrapper;
        [SerializeField] Text greetingPlayerNamePref;

        private void Start() {
            btnCloseReceiveGreeting.OnClickedBtn.Subscribe(_ => {
                TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                    gameObject.SetActive(false);
                });
            });
            btnCloseReceiveGreeting.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        public void SetReceiveGreeting() {
            Noroshi.Guild.WebApiRequester.ReceiveGreetedReward().Do(data => {
                for(int i = 0, l = data.GreetingPlayers.Length; i < l; i++) {
                    var greetingPlayer = Instantiate(greetingPlayerNamePref);
                    greetingPlayer.text = data.GreetingPlayers[i].Name;
                    greetingPlayer.transform.SetParent(greetingPlayerWrapper.transform);
                    greetingPlayer.transform.localScale = Vector3.one;
                }
                gameObject.SetActive(true);
                TweenA.Add(gameObject, 0.1f, 1).From(0);
            }).Subscribe();
        }
    }
}
