﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Noroshi.UI {
    public class GuildRankingPiece : MonoBehaviour {
        [SerializeField] Text txtRank;
        [SerializeField] Text txtGuildName;
        [SerializeField] Text txtPoint;
        [SerializeField] GameObject labelOwn;

        private Color firstColor = new Color(1, 0.9333f, 0);
        private Color secondColor = new Color(0.7765f, 0.9059f, 0.949f);
        private Color thirdColor = new Color(0.9569f, 0.5412f, 0);

        public void SetRankData(Noroshi.Core.WebApi.Response.Ranking.GuildRanking rankData) {
            txtRank.text = rankData.Rank.ToString();
            txtGuildName.text = rankData.Guild.Name;
            txtPoint.text = rankData.Value.ToString();
            if(rankData.Guild.ID == PlayerInfo.Instance.GetPlayerStatus().GuildID) {
                labelOwn.SetActive(true);
            }
            if(rankData.Rank == 1) {
                txtRank.color = firstColor;
            } else if(rankData.Rank == 2) {
                txtRank.color = secondColor;
            } else if(rankData.Rank == 3) {
                txtRank.color = thirdColor;
            } else {
                txtRank.fontSize = 48;
                txtRank.resizeTextMaxSize = 48;
            }
        }
    }
}
