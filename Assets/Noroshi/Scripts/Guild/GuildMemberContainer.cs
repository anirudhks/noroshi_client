﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.WebApi.Response.Guild;

namespace Noroshi.UI {
    public class GuildMemberContainer : MonoBehaviour {
        [SerializeField] BtnCommon btnGuildDetail;
        [SerializeField] Text txtGuildName;
        [SerializeField] Text txtRemainGreeting;
        [SerializeField] Text txtMaxGreeting;
        [SerializeField] Text txtRanking;
        [SerializeField] BtnCommon btnRanking;
        [SerializeField] BtnCommon btnDispatchMercenary;
        [SerializeField] BtnCommon btnGuildShop;
        [SerializeField] AlertModal shopNotOpenAlert;
        [SerializeField] GuildRankingPanel guildRankingPanel;
        [SerializeField] GuildMercenaryPanel guildMercenaryPanel;
        [SerializeField] GuildMemberPanel guildMemberPanelPref;
        [SerializeField] GameObject memberListWrapper;
        [SerializeField] AlertModal alertCannotGreet;
        [SerializeField] AlertModal alertNoTarget;

        public Subject<uint> OnSelectMember = new Subject<uint>();
        public Subject<bool> OnSelectGuildIcon = new Subject<bool>();

        private List<GuildMemberPanel> memberList = new List<GuildMemberPanel>();
        private int remainGreetingNum;

        private void Start() {
            btnGuildDetail.OnClickedBtn.Subscribe(_ => {
                OnSelectGuildIcon.OnNext(true);
            });
            btnGuildDetail.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnRanking.OnClickedBtn.Subscribe(_ => {
                guildRankingPanel.OpenRanking();
            });
            btnRanking.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnDispatchMercenary.OnClickedBtn.Subscribe(_ => {
                guildMercenaryPanel.OpenMercenaryPanel();
            });
            btnDispatchMercenary.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnGuildShop.OnClickedBtn.Subscribe(_ => {
                if(PlayerInfo.Instance.GetTutorialStep()
                    >= Noroshi.Core.Game.Player.TutorialStep.ClearStoryStage_1_3_2) {
                    UILoading.Instance.LoadScene(Constant.SCENE_SHOP);
                } else {
                    shopNotOpenAlert.OnOpen();
                }
            });
            btnGuildShop.OnPlaySE.Subscribe(_ => {
                if(PlayerInfo.Instance.GetTutorialStep()
                    >= Noroshi.Core.Game.Player.TutorialStep.ClearStoryStage_1_3_2) {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
                } else {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                }
            });
        }

        public void ChangeLeader(uint id) {
            foreach(var member in memberList) {
                if((uint)member.id == PlayerInfo.Instance.GetPlayerStatus().PlayerID) {
                    member.ChangeLeader(false);
                }
                if((uint)member.id == id) {
                    member.ChangeLeader(true);
                }
            }
        }

        public void NominateOfficer(uint id) {
            foreach(var member in memberList) {
                if((uint)member.id == id) {
                    member.ChangeOfficer(true);
                    break;
                }
            }
        }

        public void ReleaseOfficer(uint id) {
            foreach(var member in memberList) {
                if((uint)member.id == id) {
                    member.ChangeOfficer(false);
                    break;
                }
            }
        }

        public void Expulsion(uint id) {
            foreach(var member in memberList) {
                if((uint)member.id == id) {
                    member.gameObject.SetActive(false);
                    memberList.Remove(member);
                    break;
                }
            }
        }

        public void SetPanel(Noroshi.Core.WebApi.Response.OtherPlayerStatus status, bool canGreet = false) {
            var panel = Instantiate(guildMemberPanelPref);
            panel.transform.SetParent(memberListWrapper.transform);
            panel.transform.localScale = Vector3.one;
            memberList.Add(panel);
            panel.SetPlayerInfo(status);
            panel.OnClickedBtn.Subscribe(id => {
                OnSelectMember.OnNext((uint)id);
            });
            panel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
            if(canGreet) {
                panel.ChangeState(Noroshi.UI.Guild.PanelState.Greeting);
                panel.OnGreeting.Subscribe(data => {
                    if(remainGreetingNum < 1) {
                        alertCannotGreet.OnOpen();
                    } else {
                        Noroshi.Guild.WebApiRequester.Greet(data.PlayerID).Do(res => {
                            if(res.Error != null) {
                                alertNoTarget.OnOpen();
                            } else {
                                panel.ChangeState(Noroshi.UI.Guild.PanelState.None);
                                remainGreetingNum--;
                                txtRemainGreeting.text = remainGreetingNum.ToString();
                            }
                        }).Subscribe();
                    }
                });
            }
        }

        public void SetInfo(GetOwnResponse guildData, GetRankingSummaryResponse rankData) {
            var guildName = guildData.Guild.Category == Noroshi.Core.Game.Guild.GuildCategory.Beginner
                ? GlobalContainer.LocalizationManager.GetText("UI.Noun.BeginnerGuildName")
                : guildData.Guild.Name;
            txtGuildName.text = guildName;
            remainGreetingNum = guildData.MaxGreetingNum - guildData.GreetingNum;
            txtRemainGreeting.text = remainGreetingNum.ToString();
            txtMaxGreeting.text = guildData.MaxGreetingNum.ToString();
            if(rankData.OwnRanking != null) {
                txtRanking.text = rankData.OwnRanking.Rank.ToString();
            }

            guildRankingPanel.SetRanking(rankData, guildName);
            for(int i = 0, l = guildData.GuildMembers.Length; i < l; i++) {
                SetPanel(guildData.GuildMembers[i], guildData.GuildMembers[i].CanGreet);
            }
        }

        public void UpdateGuildName(string name) {
            txtGuildName.text = name;
        }
    }
}
