﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class GuildConfirmPanel : MonoBehaviour {
        [SerializeField] Text txtPlayerName;
        [SerializeField] BtnCommon btnCancel;
        [SerializeField] BtnCommon btnOK;

        public Subject<bool> OnDecide = new Subject<bool>();

        private bool isOpen = false;

        private void Start() {
            btnCancel.OnClickedBtn.Subscribe(_ => {
                ClosePanel();
            });
            btnCancel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnOK.OnClickedBtn.Subscribe(_ => {
                OnDecide.OnNext(true);
                ClosePanel();
            });
            btnOK.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                ClosePanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void ClosePanel() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }

        public void OpenPanel(string name) {
            txtPlayerName.text = name;

            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }
    }
}
