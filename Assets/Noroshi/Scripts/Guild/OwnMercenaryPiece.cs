﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Noroshi.Core.WebApi.Response;

namespace Noroshi.UI {
    public class OwnMercenaryPiece : MonoBehaviour {
        [SerializeField] CharacterStatusIcon characterStatusIcon;
        [SerializeField] Text txtCharacterName;
        [SerializeField] Text txtRemainDispatchTime;

        private bool isComplete = false;

        private CharacterStatusIcon.CharacterData SetCharacterStatus(PlayerCharacter characterStatus) {
            var characterData = new CharacterStatusIcon.CharacterData();

            characterData.CharacterID = characterStatus.CharacterID;
            characterData.Level = characterStatus.Level;
            characterData.EvolutionLevel = characterStatus.EvolutionLevel;
            characterData.PromotionLevel = characterStatus.PromotionLevel;
            return characterData;
        }

        public bool IsComplete() {
            return isComplete;
        }

        public void SetOwnMercenaryPiece(PlayerCharacter characterData, uint createdAt = 0) {
            var masterData = GlobalContainer.MasterManager.CharacterMaster.Get(characterData.CharacterID);
            var data = SetCharacterStatus(characterData);
            txtCharacterName.text = GlobalContainer.LocalizationManager.GetText(masterData.TextKey + ".Name");
            characterStatusIcon.SetInfo(data);
            if(createdAt == 0) {
                isComplete = false;
                txtRemainDispatchTime.text = Noroshi.Core.Game.Guild.Constant.RENTAL_SPAN.Hours + ":0" + Noroshi.Core.Game.Guild.Constant.RENTAL_SPAN.Minutes;
            } else {
                var remainSeconds = Noroshi.Core.Game.Guild.Constant.RENTAL_SPAN.TotalSeconds - (GlobalContainer.TimeHandler.UnixTime - createdAt);
                var hour = remainSeconds / 3600 < 10 ? "0" + (int)(remainSeconds / 3600) : ((int)(remainSeconds / 3600)).ToString();
                var minute = remainSeconds % 3600 / 60 < 10 ? "0" + (int)((remainSeconds % 3600) / 60) : ((int)((remainSeconds % 3600) / 60)).ToString();
                if(remainSeconds < 1) {
                    isComplete = true;
                    txtRemainDispatchTime.text = GlobalContainer.LocalizationManager.GetText("UI.Heading.DispatchComplete");
                } else {
                    isComplete = false;
                    txtRemainDispatchTime.text = hour + ":" + minute;
                }
            }
        }
    }
}
