﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class GuildRankingPanel : MonoBehaviour {
        [SerializeField] BtnCommon[] tabList;
        [SerializeField] GameObject[] contentList;
        [SerializeField] Text txtGuildName;
        [SerializeField] Text txtMyRank;
        [SerializeField] Text txtRatingPoint;
        [SerializeField] GuildRankingPiece guildRankingPiecePref;
        [SerializeField] GameObject top50Wrapper;
        [SerializeField] GameObject myRankWrapper;
        [SerializeField] GameObject noRankingTop50;
        [SerializeField] GameObject noRankingMyRank;
        [SerializeField] BtnCommon btnOverlay;

        private bool isOpen = false;

        private void Start() {
            for(int i = 0, l = tabList.Length; i < l; i++) {
                tabList[i].OnClickedBtn.Subscribe(SwitchTab);
                tabList[i].OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnOverlay.OnClickedBtn.Subscribe(_ => {
                CloseRanking();
            });
            btnOverlay.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseRanking();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void SwitchTab(int index) {
            for(int i = 0, l = contentList.Length; i < l; i++) {
                var isSelect = index == i;
                tabList[i].SetSelect(isSelect);
                contentList[i].SetActive(isSelect);
            }
        }

        public void SetRanking(Noroshi.Core.WebApi.Response.Guild.GetRankingSummaryResponse rankData, string guildName) {
            txtGuildName.text = guildName;
            if(rankData.OwnRanking != null) {
                txtMyRank.text = rankData.OwnRanking.Rank.ToString();
                txtRatingPoint.text = rankData.OwnRanking.Value.ToString();
            }

            if(rankData.TopRankers != null && rankData.TopRankers.Length > 0) {
                foreach(var guild in rankData.TopRankers) {
                    var piece = Instantiate(guildRankingPiecePref);
                    piece.transform.SetParent(top50Wrapper.transform);
                    piece.transform.localScale = Vector3.one;
                    piece.SetRankData(guild);
                }
            } else {
                noRankingTop50.SetActive(true);
            }
            if(rankData.Rivals != null && rankData.Rivals.Length > 0) {
                foreach(var guild in rankData.Rivals) {
                    var piece = Instantiate(guildRankingPiecePref);
                    piece.transform.SetParent(myRankWrapper.transform);
                    piece.transform.localScale = Vector3.one;
                    piece.SetRankData(guild);
                }
            } else {
                noRankingMyRank.SetActive(true);
            }
        }

        public void OpenRanking() {
            SwitchTab(0);
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }

        public void CloseRanking() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }
    }
}
