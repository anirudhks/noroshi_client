﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.WebApi.Response;

namespace Noroshi.UI {
    public class GuildManagementContainer : MonoBehaviour {
        [SerializeField] BtnCommon btnGuildDetail;
        [SerializeField] Text txtGuildName;
        [SerializeField] BtnCommon btnApproveJoin;
        [SerializeField] BtnCommon btnExpulsion;
        [SerializeField] BtnCommon btnChangeLeader;
        [SerializeField] BtnCommon btnChangeOfficer;
        [SerializeField] BtnCommon btnChangeGuildInfo;
        [SerializeField] GameObject memberListScroller;
        [SerializeField] GameObject requesterListScroller;
        [SerializeField] GuildMemberPanel guildMemberPanelPref;
        [SerializeField] GameObject memberListWrapper;
        [SerializeField] GameObject requesterListWrapper;
        [SerializeField] GameObject alertNoRequest;
        [SerializeField] GuildConfirmPanel nominateLeaderPanel;
        [SerializeField] GuildConfirmPanel nominateOfficerPanel;
        [SerializeField] GuildConfirmPanel releaseOfficerPanel;
        [SerializeField] GuildConfirmPanel expulsionPanel;
        [SerializeField] AlertModal alertCannotAcceptRequest;
        [SerializeField] AlertModal alertNoTarget;

        public Subject<uint> OnSelectMember = new Subject<uint>();
        public Subject<bool> OnSelectGuildIcon = new Subject<bool>();
        public Subject<bool> OnChangeGuildInfo = new Subject<bool>();
        public Subject<OtherPlayerStatus> OnJoin = new Subject<OtherPlayerStatus>();
        public Subject<uint> OnChangeLeader = new Subject<uint>();
        public Subject<uint> OnNominateOfficer = new Subject<uint>();
        public Subject<uint> OnReleaseOfficer = new Subject<uint>();
        public Subject<uint> OnExpulsion = new Subject<uint>();

        private List<GuildMemberPanel> memberList = new List<GuildMemberPanel>();
        private List<GuildMemberPanel> requesterList = new List<GuildMemberPanel>();
        private uint targetPlayerID;
        
        private void Start() {
            btnGuildDetail.OnClickedBtn.Subscribe(_ => {
                OnSelectGuildIcon.OnNext(true);
            });
            btnGuildDetail.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnApproveJoin.OnClickedBtn.Subscribe(_ => {
                memberListScroller.SetActive(false);
                requesterListScroller.SetActive(true);
            });
            btnApproveJoin.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnExpulsion.OnClickedBtn.Subscribe(_ => {
                memberListScroller.SetActive(true);
                requesterListScroller.SetActive(false);
                foreach(var member in memberList) {
                    member.ChangeState(Noroshi.UI.Guild.PanelState.Expulsion);
                }
            });
            btnExpulsion.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnChangeLeader.OnClickedBtn.Subscribe(_ => {
                memberListScroller.SetActive(true);
                requesterListScroller.SetActive(false);
                foreach(var member in memberList) {
                    member.ChangeState(Noroshi.UI.Guild.PanelState.NominateLeader);
                }
            });
            btnChangeLeader.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnChangeOfficer.OnClickedBtn.Subscribe(_ => {
                memberListScroller.SetActive(true);
                requesterListScroller.SetActive(false);
                foreach(var member in memberList) {
                    member.ChangeState(Noroshi.UI.Guild.PanelState.NominateOfficer);
                }
            });
            btnChangeOfficer.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnChangeGuildInfo.OnClickedBtn.Subscribe(_ => {
                OnChangeGuildInfo.OnNext(true);
            });
            btnChangeGuildInfo.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            nominateLeaderPanel.OnDecide.Subscribe(_ => {
                Noroshi.Guild.WebApiRequester.ChangeLeader(targetPlayerID).Do(data => {
                    if(data.Error != null) {
                        alertNoTarget.OnOpen();
                    } else {
                        OnChangeLeader.OnNext(targetPlayerID);
                        foreach(var member in memberList) {
                            if((uint)member.id == PlayerInfo.Instance.GetPlayerStatus().PlayerID) {
                                member.ChangeLeader(false);
                            }
                            if((uint)member.id == targetPlayerID) {
                                member.ChangeLeader(true);
                            }
                        }
                    }
                }).Subscribe();
            });

            nominateOfficerPanel.OnDecide.Subscribe(_ => {
                Noroshi.Guild.WebApiRequester.AddExecutiveRole(targetPlayerID).Do(data => {
                    if(data.Error != null) {
                        alertNoTarget.OnOpen();
                    } else {
                        OnNominateOfficer.OnNext(targetPlayerID);
                        foreach(var member in memberList) {
                            if((uint)member.id == targetPlayerID) {
                                member.ChangeOfficer(true);
                                break;
                            }
                        }
                    }
                }).Subscribe();
            });

            releaseOfficerPanel.OnDecide.Subscribe(_ => {
                Noroshi.Guild.WebApiRequester.RemoveExecutiveRole(targetPlayerID).Do(data => {
                    if(data.Error != null) {
                        alertNoTarget.OnOpen();
                    } else {
                        OnReleaseOfficer.OnNext(targetPlayerID);
                        foreach(var member in memberList) {
                            if((uint)member.id == targetPlayerID) {
                                member.ChangeOfficer(false);
                                break;
                            }
                        }
                    }
                }).Subscribe();
            });

            expulsionPanel.OnDecide.Subscribe(_ => {
                Noroshi.Guild.WebApiRequester.LayOff(targetPlayerID).Do(data => {
                    if(data.Error != null) {
                        alertNoTarget.OnOpen();
                    } else {
                        OnExpulsion.OnNext(targetPlayerID);
                        foreach(var member in memberList) {
                            if((uint)member.id == targetPlayerID) {
                                member.gameObject.SetActive(false);
                                break;
                            }
                        }
                    }
                }).Subscribe();
            });
        }

        private void SetMemberPanel(OtherPlayerStatus status) {
            var panel = Instantiate(guildMemberPanelPref);
            panel.transform.SetParent(memberListWrapper.transform);
            panel.transform.localScale = Vector3.one;
            memberList.Add(panel);
            panel.SetPlayerInfo(status);
            SetMemberPanelEvent(panel);
        }

        private void SetMemberPanelEvent(GuildMemberPanel panel) {
            panel.OnNominateLeader.Subscribe(data => {
                targetPlayerID = data.PlayerID;
                nominateLeaderPanel.OpenPanel(data.Name);
            });
            panel.OnNominateOfficer.Subscribe(data => {
                targetPlayerID = data.PlayerID;
                nominateOfficerPanel.OpenPanel(data.Name);
            });
            panel.OnReleaseOfficer.Subscribe(data => {
                targetPlayerID = data.PlayerID;
                releaseOfficerPanel.OpenPanel(data.Name);
            });
            panel.OnExpulsion.Subscribe(data => {
                targetPlayerID = data.PlayerID;
                expulsionPanel.OpenPanel(data.Name);
            });
            panel.OnClickedBtn.Subscribe(id => {
                OnSelectMember.OnNext((uint)id);
            });
            panel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        private void SetRequesterEvent(GuildMemberPanel panel) {
            panel.OnApprove.Subscribe(data => {
                Noroshi.Guild.WebApiRequester.AcceptRequest(data.PlayerID).Do(res => {
                    if(res.Error != null) {
                        alertCannotAcceptRequest.OnOpen();
                    } else {
                        OnJoin.OnNext(res.Requester);
                        SetMemberPanel(res.Requester);
                        panel.gameObject.SetActive(false);
                    }
                }).Subscribe();
            });
            panel.OnReject.Subscribe(data => {
                Noroshi.Guild.WebApiRequester.RejectRequest(data.PlayerID).Do(res => {
                    if(res.Error != null) {
                        alertNoTarget.OnOpen();
                    } else {
                        panel.gameObject.SetActive(false);
                    }
                }).Subscribe();
            });
            panel.OnClickedBtn.Subscribe(id => {
                OnSelectMember.OnNext((uint)id);
            });
        }

        public void SetInfo(Noroshi.Core.WebApi.Response.Guild.GetOwnResponse guildData) {
            if(guildData.Guild.Category == Noroshi.Core.Game.Guild.GuildCategory.Beginner) {
                txtGuildName.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.BeginnerGuildName");
            } else {
                txtGuildName.text = guildData.Guild.Name;
            }
            for(int i = 0, l = guildData.GuildMembers.Length; i < l; i++) {
                SetMemberPanel(guildData.GuildMembers[i]);
            }
            for(int i = 0, l = guildData.Requests.Length; i < l; i++) {
                var panel = Instantiate(guildMemberPanelPref);
                panel.transform.SetParent(requesterListWrapper.transform);
                panel.transform.localScale = Vector3.one;
                requesterList.Add(panel);
                panel.SetPlayerInfo(guildData.Requests[i].Requester);
                panel.ChangeState(Noroshi.UI.Guild.PanelState.Request);
                SetRequesterEvent(panel);
            }
            if(guildData.Requests.Length < 1) {
                alertNoRequest.SetActive(true);
            }
        }

        public void SetState() {
            if(PlayerInfo.Instance.GetPlayerStatus().GuildRole == Noroshi.Core.Game.Guild.GuildRole.Executive) {
                btnExpulsion.SetEnable(false);
                btnChangeLeader.SetEnable(false);
                btnChangeOfficer.SetEnable(false);
                btnChangeGuildInfo.SetEnable(false);
            }
        }

        public void UpdateGuildName(string name) {
            txtGuildName.text = name;
        }
    }
}
