﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniLinq;

namespace Noroshi.UI {
    public class SelectMercenaryPanel : MonoBehaviour {
        [SerializeField] GameObject characterListWrapper;
        [SerializeField] SelectMercenaryPiece selectMercenaryPiece;
        [SerializeField] BtnCommon btnDecideMercenary;
        [SerializeField] BtnCommon btnCloseMercenaryList;

        public Subject<uint> OnSelectMercenary = new Subject<uint>();

        private List<SelectMercenaryPiece> btnCharacterList = new List<SelectMercenaryPiece>();
        private List<uint> dispatchCharacterIDList = new List<uint>();
        private uint mercenaryCharacterID;
        private bool isSetCharacterList = false;
        private bool isOpen = false;

        private void Start() {
            var charaList = BattleCharacterSelect.Instance.GetCharacterStatusList();
            charaList = charaList.OrderByDescending(c => c.Level).ToList();
            foreach(var chara in charaList) {
                var btnCharacter = Instantiate(selectMercenaryPiece);
                btnCharacter.SetMercenaryPiece(chara);
                btnCharacter.transform.SetParent(characterListWrapper.transform);
                btnCharacter.transform.localScale = Vector3.one;
                btnCharacterList.Add(btnCharacter);
                btnCharacter.OnClickedBtn.Subscribe(id => {
                    mercenaryCharacterID = (uint)id;
                    foreach(var btn in btnCharacterList) {
                        btn.SetMercenarySelect(btn.id == id);
                    }
                    btnDecideMercenary.SetEnable(true);
                });
                btnCharacter.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }
            foreach(var id in dispatchCharacterIDList) {
                foreach(var chara in btnCharacterList.Where(c => c.id == id)) {
                    chara.gameObject.SetActive(false);
                }
            }
            isSetCharacterList = true;

            btnDecideMercenary.OnClickedBtn.Subscribe(_ => {
                SetMercenary();
            });
            btnDecideMercenary.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnCloseMercenaryList.OnClickedBtn.Subscribe(_ => {
                CloseSelectMercenaryPanel();
            });
            btnCloseMercenaryList.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModalChild.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseSelectMercenaryPanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void SetMercenary() {
            OnSelectMercenary.OnNext(mercenaryCharacterID);
            CloseSelectMercenaryPanel();
        }

        public void OpenSelectMercenaryPanel(List<uint> idList) {
            dispatchCharacterIDList = idList;
            if(isSetCharacterList) {
                foreach(var chara in btnCharacterList) {
                    chara.gameObject.SetActive(true);
                }
                foreach(var id in dispatchCharacterIDList) {
                    foreach(var chara in btnCharacterList.Where(c => c.id == id)) {
                        chara.gameObject.SetActive(false);
                    }
                }
            }
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalChildOpen(true);
        }

        public void CloseSelectMercenaryPanel() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalChildOpen(false);
            });
        }
    }
}
