﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.WebApi.Response;
using Noroshi.Core.WebApi.Response.Guild;

namespace Noroshi.UI {
    public class GuildMercenaryPanel : MonoBehaviour {
        [SerializeField] BtnCommon[] btnTabList;
        [SerializeField] GameObject[] contentList;
        [SerializeField] BtnCommon btnClose;
        [SerializeField] OwnMercenaryPiece[] ownMercenaryPieceList;
        [SerializeField] GameObject addMercenaryPiece;
        [SerializeField] BtnCommon btnAddMercenary;
        [SerializeField] Text txtCurrentDispatchNum;
        [SerializeField] Text txtMaxDispatchNum;
        [SerializeField] BtnCommon btnReceive;
        [SerializeField] GameObject guildMercenaryListWrapper;
        [SerializeField] GuildMercenaryPiece guildMercenaryPiecePref;
        [SerializeField] SelectMercenaryPanel selectMercenaryPanel;
        [SerializeField] CompleteDispatchPanel[] completeDispatchPanelList;
        [SerializeField] GameObject processing;

        private bool isInit = false;
        private int dispatchNum = 0;
        private int maxDispatchNum;
        private List<uint> dispatchCharacterIDList = new List<uint>();
        private List<int> mercenaryNoList = new List<int>();
        private bool isOpen = false;

        private void Start() {
            foreach(var btn in btnTabList) {
                btn.OnClickedBtn.Subscribe(SwitchTab);
                btn.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnAddMercenary.OnClickedBtn.Subscribe(_ => {
                selectMercenaryPanel.OpenSelectMercenaryPanel(dispatchCharacterIDList);
            });
            btnAddMercenary.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnReceive.OnClickedBtn.Subscribe(_ => {
                ReceiveReward();
            });
            btnReceive.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.GET);
            });

            btnClose.OnClickedBtn.Subscribe(_ => {
                CloseMercenaryPanel();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            selectMercenaryPanel.OnSelectMercenary.Subscribe(characterID => {
                var pcID = BattleCharacterSelect.Instance.GetPlayerCharacterId(new uint[]{characterID})[0];
                SetNewMercenary(pcID);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseMercenaryPanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            SwitchTab(0);
        }

        private void SwitchTab(int index) {
            for(int i = 0, l = contentList.Length; i < l; i++) {
                if(i == index) {
                    btnTabList[i].SetSelect(true);
                    contentList[i].SetActive(true);
                } else {
                    btnTabList[i].SetSelect(false);
                    contentList[i].SetActive(false);
                }
            }
        }

        private void LoadMercenaryData() {
            var vipLevel = PlayerInfo.Instance.GetPlayerStatus().VipLevel;
            var vipBonusRequester = new Noroshi.Player.VipLevel.Bonus.WebAPIRequester();
            Noroshi.Guild.WebApiRequester.GetRentalCharacters().Do(own => {
                Noroshi.Guild.WebApiRequester.GetTakableRentalCharacters().Do(guild => {
                    vipBonusRequester.MasterData().Do(vipBonusData => {
                        maxDispatchNum = vipBonusData[vipLevel].MaxRentalCharacterNum;
                        SetOwnMercenary(own.RentalCharacters);
                        SetGuildMercenary(guild.RentalPlayerCharacters);
                        isInit = true;
                        processing.SetActive(false);
                        OpenMercenaryPanel();
                    }).Subscribe();
                }).Subscribe();
            }).Subscribe();
        }

        private void SetOwnMercenary(RentalCharacter[] mercenaryList) {
            txtMaxDispatchNum.text = maxDispatchNum.ToString();
            for(int i = 0, l = ownMercenaryPieceList.Length; i < l; i++) {
                if(i < mercenaryList.Length) {
                    dispatchCharacterIDList.Add(mercenaryList[i].PlayerCharacter.CharacterID);
                    mercenaryNoList.Add((int)mercenaryList[i].No);
                    ownMercenaryPieceList[i].SetOwnMercenaryPiece(mercenaryList[i].PlayerCharacter, mercenaryList[i].CreatedAt);
                    if(GlobalContainer.TimeHandler.UnixTime - mercenaryList[i].CreatedAt > Noroshi.Core.Game.Guild.Constant.RENTAL_SPAN.TotalSeconds) {
                        btnReceive.SetEnable(true);
                    }
                    dispatchNum++;
                    ownMercenaryPieceList[i].gameObject.SetActive(true);
                } else {
                    ownMercenaryPieceList[i].gameObject.SetActive(false);
                }
            }
            if(mercenaryList.Length > maxDispatchNum - 1) {
                addMercenaryPiece.SetActive(false);
            }
            txtCurrentDispatchNum.text = dispatchNum.ToString();
        }

        private void SetGuildMercenary(PlayerCharacter[] mercenaryList) {
            foreach(var mercenary in mercenaryList) {
                var piece = Instantiate(guildMercenaryPiecePref);
                piece.transform.SetParent(guildMercenaryListWrapper.transform);
                piece.transform.localScale = Vector3.one;
                piece.SetGuildMercenaryPiece(mercenary);
            }
        }

        private void SetNewMercenary(uint playerCharacterID) {
            byte no = 0;
            for(int i = 0, l = ownMercenaryPieceList.Length; i < l; i++) {
                if(!mercenaryNoList.Contains(i + 1)) {
                    no = (byte)(i + 1);
                    break;
                }
            }
            if(no == 0) {return;}
            processing.SetActive(true);
            Noroshi.Guild.WebApiRequester.AddRentalCharacter(no, playerCharacterID).Do(res => {
                dispatchCharacterIDList.Add(res.PlayerCharacter.CharacterID);
                mercenaryNoList.Add((int)no);
                dispatchNum++;
                txtCurrentDispatchNum.text = dispatchNum.ToString();
                for(int i = 0, l = ownMercenaryPieceList.Length; i < l; i++) {
                    if(!ownMercenaryPieceList[i].gameObject.activeSelf) {
                        ownMercenaryPieceList[i].SetOwnMercenaryPiece(res.PlayerCharacter);
                        ownMercenaryPieceList[i].gameObject.SetActive(true);
                        if(i == l - 1) {
                            addMercenaryPiece.SetActive(false);
                        }
                        break;
                    }
                }
                if(mercenaryNoList.Count > maxDispatchNum - 1) {
                    addMercenaryPiece.SetActive(false);
                }
                processing.SetActive(false);
            }).Subscribe();
        }

        private void ReceiveReward() {
            Noroshi.Guild.WebApiRequester.RemoveRentalCharacters().Do(res => {
                for(int i = 0, l = completeDispatchPanelList.Length; i < l; i++) {
                    if(i < res.PlayerCharacters.Length) {
                        completeDispatchPanelList[i].SetReward(
                            res.PlayerCharacters[i], res.FixedRewards[i], res.RentalRewards[i]
                        );
                        dispatchCharacterIDList.Remove(res.PlayerCharacters[i].CharacterID);
                    }
                }
                foreach(var own in ownMercenaryPieceList) {
                    if(own.IsComplete()) {
                        dispatchNum--;
                        own.gameObject.SetActive(false);
                    }
                }
                txtCurrentDispatchNum.text = dispatchNum.ToString();
            }).Subscribe();
        }

        public void OpenMercenaryPanel() {
            if(isInit) {
                isOpen = true;
                gameObject.SetActive(true);
                TweenA.Add(gameObject, 0.1f, 1).From(0);
                BackButtonController.Instance.IsModalOpen(true);
            } else {
                processing.SetActive(true);
                LoadMercenaryData();
            }
        }

        public void CloseMercenaryPanel() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }
    }
}
