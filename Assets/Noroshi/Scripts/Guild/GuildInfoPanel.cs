﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class GuildInfoPanel : MonoBehaviour {
        [SerializeField] BtnCommon btnSearch;
        [SerializeField] BtnCommon btnCreateGuild;
        [SerializeField] BtnCommon btnUpdate;
        [SerializeField] BtnCommon btnClose;
        [SerializeField] Text txtGuildID;
        [SerializeField] Text txtGuildName;
        [SerializeField] Text txtCurrentMemberNum;
        [SerializeField] Text txtMaxMemberNum;
        [SerializeField] Text txtRanking;
        [SerializeField] Text txtHighestRanking;
        [SerializeField] Text txtNeedLevel;
        [SerializeField] Text txtGuildIntroduction;
        [SerializeField] GameObject nounBeginner;
        [SerializeField] GameObject nounOpen;
        [SerializeField] GameObject nounClose;
        [SerializeField] GameObject memberNumSeparator;
        [SerializeField] InputField inputIntroduction;
        [SerializeField] Text introductionPlaceholder;
        [SerializeField] InputField inputGuildName;
        [SerializeField] Text guildNamePlaceholder;
        [SerializeField] Dropdown needLevelSelector;
        [SerializeField] Text txtNeedLevelSelector;
        [SerializeField] Dropdown statusSelector;
        [SerializeField] Text txtStatusSelector;
        [SerializeField] AlertModal alertLeaderCreateGuild;
        [SerializeField] GameObject changeInfoAlert;
        [SerializeField] GameObject processing;

        public Subject<bool> OnSelectSearch = new Subject<bool>();
        public Subject<bool> OnSelectCreateGuild = new Subject<bool>();
        public Subject<Noroshi.Core.WebApi.Response.Guild.Guild> OnUpdateGuildInfo = new Subject<Noroshi.Core.WebApi.Response.Guild.Guild>();

        private bool isBeginner;
        private bool isLeader;
        private bool isOpen = false;
        
        private void Start() {
            isLeader = PlayerInfo.Instance.GetPlayerStatus().GuildRole == Noroshi.Core.Game.Guild.GuildRole.Leader;

            btnSearch.OnClickedBtn.Subscribe(_ => {
                Close();
                OnSelectSearch.OnNext(true);
            });
            btnSearch.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnCreateGuild.OnClickedBtn.Subscribe(_ => {
                if(isLeader) {
                    alertLeaderCreateGuild.OnOpen();
                } else {
                    Close();
                    OnSelectCreateGuild.OnNext(true);
                }
            });
            btnCreateGuild.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnUpdate.OnClickedBtn.Subscribe(_ => {
                var isOpen = statusSelector.value == 0;
                var needLevel = ushort.Parse(needLevelSelector.options[needLevelSelector.value].text);
                var name = inputGuildName.text;
                var intro = inputIntroduction.text;

                processing.SetActive(true);
                Noroshi.Guild.WebApiRequester.Configure(isOpen, needLevel, name, intro).Do(data => {
                    processing.SetActive(false);
                    SetGuildInfo(data.Guild);
                    OnUpdateGuildInfo.OnNext(data.Guild);
                    Close();
                }).Subscribe();
            });
            btnUpdate.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnClose.OnClickedBtn.Subscribe(_ => {
                Close();
                BackButtonController.Instance.IsModalOpen(false);
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                Close();
                BackButtonController.Instance.IsModalOpen(false);
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        public void ChangeToNormalMember() {
            isLeader = false;
        }

        public void SetGuildInfo(Noroshi.Core.WebApi.Response.Guild.Guild guildData) {
            var gclm = GlobalContainer.LocalizationManager;
            isBeginner = guildData.Category == Noroshi.Core.Game.Guild.GuildCategory.Beginner;
            txtGuildID.text = guildData.ID.ToString();
            txtGuildName.text = guildData.Name;
            inputGuildName.text = guildData.Name;
            guildNamePlaceholder.text = guildData.Name;
            txtCurrentMemberNum.text = guildData.MemberNum.ToString();
            txtMaxMemberNum.text = guildData.MaxMemberNum.ToString();
            txtNeedLevel.text = guildData.NecessaryPlayerLevel.ToString();
            txtNeedLevelSelector.text = guildData.NecessaryPlayerLevel.ToString();
            needLevelSelector.value = ((int)guildData.NecessaryPlayerLevel - 20) / 4;
            txtGuildIntroduction.text = guildData.Introduction;
            inputIntroduction.text = guildData.Introduction;
            introductionPlaceholder.text = guildData.Introduction;
            statusSelector.options[0].text = gclm.GetText("UI.Noun.OpenGuild");
            statusSelector.options[1].text = gclm.GetText("UI.Noun.CloseGuild");
            if(isBeginner) {
                txtGuildName.text = gclm.GetText("UI.Noun.BeginnerGuildName");
                txtGuildIntroduction.text = gclm.GetText("UI.Alert.BeginnerGuildDescription");
                txtMaxMemberNum.gameObject.SetActive(false);
                memberNumSeparator.SetActive(false);
                nounBeginner.SetActive(true);
                nounOpen.SetActive(false);
                nounClose.SetActive(false);
            } else if(guildData.Category == Noroshi.Core.Game.Guild.GuildCategory.NormalOpen) {
                nounBeginner.SetActive(false);
                nounOpen.SetActive(true);
                nounClose.SetActive(false);
                statusSelector.value = 0;
                txtStatusSelector.text = gclm.GetText("UI.Noun.OpenGuild");
            } else if(guildData.Category == Noroshi.Core.Game.Guild.GuildCategory.NormalClose) {
                nounBeginner.SetActive(false);
                nounOpen.SetActive(false);
                nounClose.SetActive(true);
                statusSelector.value = 1;
                txtStatusSelector.text = gclm.GetText("UI.Noun.CloseGuild");
            }
        }

        public void SetRanking(Noroshi.Core.WebApi.Response.Ranking.GuildRanking data) {
            txtRanking.text = data.Rank.ToString();
            txtHighestRanking.text = data.UniqueRank.ToString();
        }

        public void Open(bool isEdit = false) {
            inputIntroduction.gameObject.SetActive(isEdit);
            inputGuildName.gameObject.SetActive(isEdit);
            needLevelSelector.gameObject.SetActive(isEdit);
            statusSelector.gameObject.SetActive(isEdit);

            btnSearch.gameObject.SetActive(!isEdit);
            if(isBeginner || isEdit) {
                btnCreateGuild.gameObject.SetActive(false);
            } else {
                btnCreateGuild.gameObject.SetActive(true);
            }
            btnUpdate.gameObject.SetActive(isEdit);
            changeInfoAlert.SetActive(isEdit);

            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }
        
        public void Close() {
            alertLeaderCreateGuild.ResetTween();
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
            });
        }

    }
}
