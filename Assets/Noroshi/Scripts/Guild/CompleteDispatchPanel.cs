﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.Core.Game.Possession;
using Noroshi.Core.WebApi.Response;
using Noroshi.Core.WebApi.Response.Possession;

namespace Noroshi.UI {
    public class CompleteDispatchPanel : MonoBehaviour {
        [SerializeField] CharacterStatusIcon characterStatusIcon;
        [SerializeField] Text txtCharacterName;
        [SerializeField] Text txtDispatchTime;
        [SerializeField] Text txtDispatchRewardGold;
        [SerializeField] Text txtDispatchRewardGem;
        [SerializeField] Text txtEmployRewardGold;
        [SerializeField] Text txtEmployRewardGem;
        [SerializeField] BtnCommon btnCloseComplete;

        private void Start() {
            btnCloseComplete.OnClickedBtn.Subscribe(_ => {
                TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                    gameObject.SetActive(false);
                });
            });
            btnCloseComplete.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        private CharacterStatusIcon.CharacterData SetCharacterStatus(PlayerCharacter characterStatus) {
            var characterData = new CharacterStatusIcon.CharacterData();

            characterData.CharacterID = characterStatus.CharacterID;
            characterData.Level = characterStatus.Level;
            characterData.EvolutionLevel = characterStatus.EvolutionLevel;
            characterData.PromotionLevel = characterStatus.PromotionLevel;
            return characterData;
        }

        public void SetReward(PlayerCharacter characterData,
            PossessionObject dispatchReward,
            PossessionObject employReward
        ) {
            var data = SetCharacterStatus(characterData);
            characterStatusIcon.SetInfo(data);
            if(dispatchReward.ID == (byte)PossessionStatusID.Gold) {
                txtDispatchRewardGold.text = dispatchReward.Num.ToString();
            } else {
                txtDispatchRewardGem.text = dispatchReward.Num.ToString();
            }
            if(employReward.ID == (byte)PossessionStatusID.Gold) {
                txtEmployRewardGold.text = employReward.Num.ToString();
            } else {
                txtEmployRewardGem.text = employReward.Num.ToString();
            }

            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
        }
    }
}
