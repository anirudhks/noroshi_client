using UnityEngine;
using System.Collections;
using UniRx;
using Noroshi.Core.Game.Guild;
using Noroshi.Core.Game.GameContent;
using Noroshi.Core.Game.Player;

namespace Noroshi.UI {
    public class GuildController : MonoBehaviour {
        [SerializeField] GuildManagementContainer managementContainer;
        [SerializeField] GuildMemberContainer memberContainer;
        [SerializeField] GuildChatContainer chatContainer;
        [SerializeField] GuildInfoPanel guildInfoPanel;
        [SerializeField] GuildSearchPanel guildSearchPanel;
        [SerializeField] GuildPlayerInfoPanel guildPlayerInfoPanel;
        [SerializeField] GuildCreatePanel guildCreatePanel;
        [SerializeField] GuildReceiveGreetingPanel guildReceiveGreetingPanel;
        [SerializeField] BtnCommon[] btnTabList;
        [SerializeField] GameObject[] contentList;

        private bool isLoad = false;
        private Noroshi.Core.WebApi.Response.Guild.Guild requestingGuild;

        private void Start() {
            if(SoundController.Instance != null) {
                SoundController.Instance.PlayBGM(SoundController.BGMKeys.CHARACTER_LIST);
            }

            foreach(var btn in btnTabList) {
                btn.OnClickedBtn.Subscribe(SwitchTab);
                btn.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            managementContainer.OnSelectMember.Subscribe(id => {
                guildPlayerInfoPanel.Open((uint)id);
            });
            managementContainer.OnSelectGuildIcon.Subscribe(_ => {
                guildInfoPanel.Open();
            });
            managementContainer.OnChangeGuildInfo.Subscribe(_ => {
                guildInfoPanel.Open(true);
            });
            managementContainer.OnJoin.Subscribe(status => {
                memberContainer.SetPanel(status);
            });
            managementContainer.OnChangeLeader.Subscribe(id => {
                btnTabList[0].gameObject.SetActive(false);
                SwitchTab(1);
                guildInfoPanel.ChangeToNormalMember();
                memberContainer.ChangeLeader(id);
            });
            managementContainer.OnNominateOfficer.Subscribe(memberContainer.NominateOfficer);
            managementContainer.OnReleaseOfficer.Subscribe(memberContainer.ReleaseOfficer);
            managementContainer.OnExpulsion.Subscribe(memberContainer.Expulsion);

            memberContainer.OnSelectMember.Subscribe(id => {
                guildPlayerInfoPanel.Open((uint)id);
            });
            memberContainer.OnSelectGuildIcon.Subscribe(_ => {
                guildInfoPanel.Open();
            });

            guildInfoPanel.OnSelectSearch.Subscribe(_ => {
                guildSearchPanel.Open(requestingGuild);
            });
            guildInfoPanel.OnSelectCreateGuild.Subscribe(_ => {
                guildCreatePanel.Open();
            });
            guildInfoPanel.OnUpdateGuildInfo.Subscribe(guildData => {
                managementContainer.UpdateGuildName(guildData.Name);
                memberContainer.UpdateGuildName(guildData.Name);
                chatContainer.UpdateGuildName(guildData.Name);
            });

            BackButtonController.Instance.OnBack.Subscribe(_ => {
                UILoading.Instance.LoadScene(Constant.SCENE_MAIN);
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            StartCoroutine(OnLoading());
        }

        private IEnumerator OnLoading() {
            while(!PlayerInfo.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            LoadGuildData();
            while(!isLoad) {
                yield return new WaitForEndOfFrame();
            }
            SwitchTab(1);
            UILoading.Instance.HideLoading();
        }

        private void LoadGuildData() {
            var isNormalOpen = false;
            var openGameContents = GameContent.BuildOpenGameContentsByPlayerLevelAndTutorialStep(PlayerInfo.Instance.GetPlayerStatus().Level, (TutorialStep)PlayerInfo.Instance.GetPlayerStatus().TutorialStep);
            foreach(var content in openGameContents) {
                if((GameContentID)content.ID == GameContentID.NormalGuild) {
                    isNormalOpen = true;
                    break;
                }
            }
            if(PlayerInfo.Instance.GetPlayerStatus().GuildID != null) {
                Noroshi.Guild.WebApiRequester.GetOwn().Do(data => {
                    if(data.Guild.Category == Noroshi.Core.Game.Guild.GuildCategory.Beginner && isNormalOpen) {
                        Noroshi.Guild.WebApiRequester.JoinAutomatically().Do(_ => {
                            Noroshi.Guild.WebApiRequester.GetOwn().Do(newData => {
                                Init(newData);
                            }).Subscribe();
                        }).Subscribe();
                    } else {
                        Init(data);
                    }
                }).Subscribe();
            } else {
                Noroshi.Guild.WebApiRequester.JoinAutomatically().Do(data => {
                    Noroshi.Guild.WebApiRequester.GetOwn().Do(newData => {
                        Init(newData);
                    }).Subscribe();
                }).Subscribe();
            }
        }

        private void Init(Noroshi.Core.WebApi.Response.Guild.GetOwnResponse guildData) {
            if(PlayerInfo.Instance.GetPlayerStatus().GuildRole != GuildRole.Leader
               && PlayerInfo.Instance.GetPlayerStatus().GuildRole != GuildRole.Executive) {
                btnTabList[0].gameObject.SetActive(false);
            } else {
                managementContainer.SetState();
            }
            if(guildData.UnconfirmedGreetedNum > 0) {
                guildReceiveGreetingPanel.SetReceiveGreeting();
            }
            if(guildData.RequestingGuild != null) {
                requestingGuild = guildData.RequestingGuild;
            }
            chatContainer.Init(guildData.Guild);
            managementContainer.SetInfo(guildData);
            guildInfoPanel.SetGuildInfo(guildData.Guild);
            Noroshi.Guild.WebApiRequester.GetRankingSummary().Do(res => {
                memberContainer.SetInfo(guildData, res);
                if(res.OwnRanking != null) {
                    guildInfoPanel.SetRanking(res.OwnRanking);
                }
                isLoad = true;
            }).Subscribe();
        }

        private void SwitchTab(int index) {
            if(index == 2) {chatContainer.SetInitialView();}
            for(int i = 0, l = contentList.Length; i < l; i++) {
                if(i == index) {
                    btnTabList[i].SetSelect(true);
                    contentList[i].SetActive(true);
                } else {
                    btnTabList[i].SetSelect(false);
                    contentList[i].SetActive(false);
                }
            }
        }

    }
}
