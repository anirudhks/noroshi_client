﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.WebApi.Response.Guild;

namespace Noroshi.UI {
    public class GuildPlayerInfoPanel : MonoBehaviour {
        [SerializeField] Image imgPlayerIcon;
        [SerializeField] Text txtPlayerName;
        [SerializeField] Text txtPlayerLevel;
        [SerializeField] Text txtRanking;
        [SerializeField] Text txtHighestRanking;
        [SerializeField] Text txtTotalPower;
        [SerializeField] Text txtPower;
        [SerializeField] CharacterStatusIcon[] characterIconList;
        [SerializeField] BtnCommon btnClose;
        [SerializeField] GameObject processing;

        private Dictionary<uint, GetMemberResponse> playerDataList = new Dictionary<uint, GetMemberResponse>();
        private bool isOpen = false;

        private void Start() {
            btnClose.OnClickedBtn.Subscribe(_ => {
                Close();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                Close();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void SetPlayerInfo(GetMemberResponse data) {
            var characterLength = data.PlayerArenaForGuildMember.DeckCharacters.Length;
            imgPlayerIcon.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite> (data.OtherPlayerStatus.AvatarCharacterID, "thumb_1");
            txtPlayerName.text = data.OtherPlayerStatus.Name;
            txtPlayerLevel.text = data.OtherPlayerStatus.Level.ToString();
            txtRanking.text = data.PlayerArenaForGuildMember.Rank.ToString();
            txtHighestRanking.text = data.PlayerArenaForGuildMember.BestRank < 1 ? "-" : data.PlayerArenaForGuildMember.BestRank.ToString();
            txtTotalPower.text = data.PlayerArenaForGuildMember.AllStrength.ToString();
            for(int i = 0, l = characterIconList.Length; i < l; i++) {
                if(i < characterLength) {
                    var status = new CharacterStatusIcon.CharacterData();
                    var charaData = data.PlayerArenaForGuildMember.DeckCharacters[i];
                    var masterData = GlobalContainer.MasterManager.CharacterMaster.Get(charaData.CharacterID);
                    var flags = new Noroshi.Core.Game.Character.CharacterTagSet(masterData.TagFlags);
                    status.CharacterID = charaData.CharacterID;
                    status.Level = charaData.Level;
                    status.EvolutionLevel = charaData.EvolutionLevel;
                    status.PromotionLevel = charaData.PromotionLevel;
                    status.IsDeca = flags.HasTag(0);
                    characterIconList[i].SetInfo(status);
                    characterIconList[i].gameObject.SetActive(true);
                } else {
                    characterIconList[i].gameObject.SetActive(false);
                }
            }
        }

        public void Open(uint id) {
            if(playerDataList.ContainsKey(id)) {
                SetPlayerInfo(playerDataList[id]);
                isOpen = true;
                gameObject.SetActive(true);
                TweenA.Add(gameObject, 0.1f, 1).From(0);
                BackButtonController.Instance.IsModalOpen(true);
            } else {
                processing.SetActive(true);
                Noroshi.Guild.WebApiRequester.GetMember(id).Do(res => {
                    SetPlayerInfo(res);
                    playerDataList[id] = res;
                    processing.SetActive(false);
                    isOpen = true;
                    gameObject.SetActive(true);
                    TweenA.Add(gameObject, 0.1f, 1).From(0);
                    BackButtonController.Instance.IsModalOpen(true);
                }).Subscribe();
            }
        }

        public void Close() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }
    }
}
