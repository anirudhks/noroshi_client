﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Noroshi.Core.WebApi.Response;

namespace Noroshi.UI {
    public class GuildMercenaryPiece : MonoBehaviour {
        [SerializeField] CharacterStatusIcon characterStatusIcon;
        [SerializeField] Text txtPlayerName;

        private CharacterStatusIcon.CharacterData SetCharacterStatus(PlayerCharacter characterStatus) {
            var characterData = new CharacterStatusIcon.CharacterData();

            characterData.CharacterID = characterStatus.CharacterID;
            characterData.Level = characterStatus.Level;
            characterData.EvolutionLevel = characterStatus.EvolutionLevel;
            characterData.PromotionLevel = characterStatus.PromotionLevel;
            return characterData;
        }

        public void SetGuildMercenaryPiece(PlayerCharacter data) {
            var masterData = GlobalContainer.MasterManager.CharacterMaster.Get(data.CharacterID);
            var characterData = SetCharacterStatus(data);
            txtPlayerName.text = GlobalContainer.LocalizationManager.GetText(masterData.TextKey + ".Name");
            characterStatusIcon.SetInfo(characterData);
        }
    }
}
