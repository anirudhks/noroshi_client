﻿using UnityEngine;
using System.Collections;
using UniRx;
using Noroshi.Game;

namespace Noroshi.UI {
    public class SelectMercenaryPiece : BtnCommon {
        [SerializeField] CharacterStatusIcon characterStatusIcon;
        [SerializeField] GameObject iconSelect;

        private CharacterStatusIcon.CharacterData SetCharacterStatus(CharacterStatus characterStatus) {
            var characterData = new CharacterStatusIcon.CharacterData();

            characterData.CharacterID = characterStatus.CharacterID;
            characterData.Level = characterStatus.Level;
            characterData.EvolutionLevel = characterStatus.EvolutionLevel;
            characterData.PromotionLevel = characterStatus.PromotionLevel;
            return characterData;
        }

        public void SetMercenaryPiece(CharacterStatus characterStatus) {
            var characterData = SetCharacterStatus(characterStatus);
            id = (int)characterStatus.CharacterID;
            characterStatusIcon.SetInfo(characterData);
        }

        public void SetMercenarySelect(bool isSelect) {
            iconSelect.SetActive(isSelect);
        }
    }
}
