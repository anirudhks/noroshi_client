﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;

namespace Noroshi.UI {
    public class GuildSearchPanel : MonoBehaviour {
        [SerializeField] BtnCommon btnClose;
        [SerializeField] GuildPanel guildPanelPref;
        [SerializeField] GameObject guildListWrapper;
        [SerializeField] InputField inputGuildID;
        [SerializeField] BtnCommon btnSearch;
        [SerializeField] AlertModal noGuildAlert;
        [SerializeField] AlertModal alertJoinError;
        [SerializeField] AlertModal alertRequestJoin;
        [SerializeField] AlertModal alertCancelRequest;
        [SerializeField] GameObject processing;

        private List<GuildPanel> guildList = new List<GuildPanel>();
        private List<GuildPanel> searchList = new List<GuildPanel>();
        private GuildPanel requestingPanel;
        private uint requestingGuildID = 0;
        private bool isInit = false;
        private bool isOpen = false;
        
        private void Start() {
            processing.SetActive(true);
            Noroshi.Guild.WebApiRequester.GetRecommendedGuilds().Do(data => {
                foreach(var guild in data.Guilds) {
                    var panel = Instantiate(guildPanelPref);
                    panel.transform.SetParent(guildListWrapper.transform);
                    panel.transform.localScale = Vector3.one;
                    panel.SetPanel(guild);
                    SetPanelEvent(panel);
                    guildList.Add(panel);
                }
                processing.SetActive(false);
            }).Subscribe();

            btnSearch.OnClickedBtn.Subscribe(_ => {
                processing.SetActive(true);
                SearchGuild();
            });
            btnSearch.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnClose.OnClickedBtn.Subscribe(_ => {
                Close();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                Close();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void SearchGuild() {
            uint id = 0;
            uint.TryParse(inputGuildID.text, out id);
            if(id < 1) {return;}
            for(int i = searchList.Count - 1; i > -1; i--) {
                searchList[i].gameObject.SetActive(false);
                searchList.Remove(searchList[i]);
            }
            foreach(var guild in guildList) {
                guild.gameObject.SetActive(false);
            }
            if(requestingPanel != null) {
                requestingPanel.gameObject.SetActive(false);
            }
            Noroshi.Guild.WebApiRequester.Get(id).Do(data => {
                if(data.Error != null) {
                    noGuildAlert.OnOpen();
                } else {
                    if(data.Guild.ID != requestingGuildID) {
                        var panel = Instantiate(guildPanelPref);
                        panel.transform.SetParent(guildListWrapper.transform);
                        panel.transform.localScale = Vector3.one;
                        panel.SetPanel(data.Guild);
                        SetPanelEvent(panel);
                        searchList.Add(panel);
                    } else {
                        requestingPanel.gameObject.SetActive(true);
                    }
                }
                processing.SetActive(false);
            }).Subscribe();
        }

        private void SetPanelEvent(GuildPanel panel) {
            panel.OnJoin.Subscribe(id => {
                processing.SetActive(true);
                Noroshi.Guild.WebApiRequester.Join(id).Do(data => {
                    processing.SetActive(false);
                    if(data.Error != null) {
                        alertJoinError.OnOpen();
                    } else {
                        UILoading.Instance.LoadScene(Constant.SCENE_GUILD);
                    }
                }).Subscribe();
            });
            panel.OnRequest.Subscribe(id => {
                processing.SetActive(true);
                Noroshi.Guild.WebApiRequester.Request(id).Do(data => {
                    processing.SetActive(false);
                    if(data.Error != null) {
                        alertJoinError.OnOpen();
                    } else {
                        requestingGuildID = data.Guild.ID;
                        alertRequestJoin.OnOpen();
                        searchList.Remove(panel);
                        requestingPanel = panel;
                        panel.transform.SetAsFirstSibling();
                        panel.ChangeStateToCancel();
                    }
                }).Subscribe();
            });
            panel.OnCancelRequest.Subscribe(id => {
                processing.SetActive(true);
                Noroshi.Guild.WebApiRequester.CancelRequest().Do(data => {
                    processing.SetActive(false);
                    alertCancelRequest.OnOpen();
                    requestingPanel = null;
                    requestingGuildID = 0;
                    searchList.Add(panel);
                    panel.ChangeStateToRequest();
                }).Subscribe();
            });
        }

        public void Open(Noroshi.Core.WebApi.Response.Guild.Guild requestingGuild) {
            inputGuildID.text = "";
            if(requestingGuild != null && !isInit) {
                var panel = Instantiate(guildPanelPref);
                panel.transform.SetParent(guildListWrapper.transform);
                panel.transform.localScale = Vector3.one;
                panel.SetPanel(requestingGuild, true);
                SetPanelEvent(panel);
                requestingPanel = panel;
                requestingGuildID = requestingGuild.ID;
            }
            for(int i = searchList.Count - 1; i > -1; i--) {
                searchList[i].gameObject.SetActive(false);
                searchList.Remove(searchList[i]);
            }
            if(requestingPanel != null) {
                requestingPanel.gameObject.SetActive(true);
            }
            foreach(var guild in guildList) {
                guild.gameObject.SetActive(true);
            }
            isInit = true;
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }

        public void Close() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }

    }
}
