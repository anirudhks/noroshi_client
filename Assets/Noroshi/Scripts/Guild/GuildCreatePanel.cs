﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class GuildCreatePanel : MonoBehaviour {
        [SerializeField] BtnCommon btnClose;
        [SerializeField] BtnCommon btnCreate;
        [SerializeField] InputField inputGuildName;
        [SerializeField] InputField inputIntroduction;
        [SerializeField] Dropdown needLevelSelector;
        [SerializeField] Dropdown statusSelector;
        [SerializeField] Text txtStatusSelector;
        [SerializeField] AlertModal alertInputGuildName;
        [SerializeField] AlertModal alertInputGuildIntroduction;
        [SerializeField] GameObject gemContainer;
        [SerializeField] GameObject confirmPanel;
        [SerializeField] Text txtConfirmGuildName;
        [SerializeField] Text txtConfirmNeedLevel;
        [SerializeField] GameObject nounConfirmOpenGuild;
        [SerializeField] GameObject nounConfirmCloseGuild;
        [SerializeField] Text txtConfirmGuildIntroduction;
        [SerializeField] BtnCommon btnOK;
        [SerializeField] BtnCommon btnCancel;
        [SerializeField] GameObject processing;

        private bool isOpenGuild;
        private ushort needLevel;
        private string guildName;
        private string introduction;
        private bool isOpen = false;
        private bool isOpenConfirm = false;
        
        private void Start() {
            var gclm = GlobalContainer.LocalizationManager;
            txtStatusSelector.text = gclm.GetText("UI.Noun.OpenGuild");
            statusSelector.options[0].text = gclm.GetText("UI.Noun.OpenGuild");
            statusSelector.options[1].text = gclm.GetText("UI.Noun.CloseGuild");

            if(PlayerInfo.Instance.HaveGem < Noroshi.Core.Game.Guild.Constant.NECESSARY_GEM_TO_CREATE_NORMAL_GUILD) {
                btnCreate.SetEnable(false);
            } else {
                btnCreate.OnClickedBtn.Subscribe(_ => {
                    OpenConfirmPanel();
                });
                btnCreate.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnClose.OnClickedBtn.Subscribe(_ => {
                Close();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnOK.OnClickedBtn.Subscribe(_ => {
                CreateGuild();
            });
            btnOK.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });

            btnCancel.OnClickedBtn.Subscribe(_ => {
                TweenA.Add(confirmPanel, 0.1f, 0).Then(() => {
                    confirmPanel.SetActive(false);
                });
            });
            btnCancel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                Close();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            BackButtonController.Instance.OnCloseModalChild.Subscribe(_ => {
                if(!isOpenConfirm) {return;}
                TweenA.Add(confirmPanel, 0.1f, 0).Then(() => {
                    isOpenConfirm = false;
                    confirmPanel.SetActive(false);
                    BackButtonController.Instance.IsModalChildOpen(false);
                });
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private bool IsNullOrWhiteSpace(string value) {
            return value == null || value.Trim() == "";
        }

        private void OpenConfirmPanel() {
            isOpenGuild = statusSelector.value == 0;
            needLevel = ushort.Parse(needLevelSelector.options[needLevelSelector.value].text);
            guildName = inputGuildName.text;
            introduction = inputIntroduction.text;

            if(string.IsNullOrEmpty(guildName) || IsNullOrWhiteSpace(guildName)) {
                alertInputGuildName.OnOpen();
                return;
            }
            if(string.IsNullOrEmpty(introduction) || IsNullOrWhiteSpace(introduction)) {
                alertInputGuildIntroduction.OnOpen();
                return;
            }
            txtConfirmGuildName.text = guildName;
            txtConfirmNeedLevel.text = needLevel.ToString();
            txtConfirmGuildIntroduction.text = introduction;
            nounConfirmOpenGuild.SetActive(isOpenGuild);
            nounConfirmCloseGuild.SetActive(!isOpenGuild);
            isOpenConfirm = true;
            confirmPanel.SetActive(true);
            TweenA.Add(confirmPanel, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalChildOpen(true);
        }

        private void CreateGuild() {
            processing.SetActive(true);
            Noroshi.Guild.WebApiRequester.Create(isOpenGuild, needLevel, guildName, introduction).Do(data => {
                if(data.Error != null) {

                } else {
                    processing.SetActive(false);
                    UILoading.Instance.LoadScene(Constant.SCENE_GUILD);
                }
            }).Subscribe();
        }
        
        public void Open() {
            isOpen = true;
            gemContainer.SetActive(true);
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }

        public void Close() {
            gemContainer.SetActive(false);
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }

    }
}
