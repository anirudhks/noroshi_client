using System;
using UniRx;
using Noroshi.Repositories;
using Noroshi.WebApi;
using Noroshi.Master;
using Noroshi.Sound;
using Noroshi.Localization;
using Noroshi.PushNotification;
using Noroshi.TimeUtil;
using Noroshi.Random;
using Noroshi.GlobalModal;
using Noroshi.AssetBundle;
using WebApiRequester = Noroshi.WebApi.WebApiRequester;
using Noroshi.Core.WebApi.Response;



namespace Noroshi
{
	public class GlobalContainer
	{
        const string DEFAULT_API_HOST = "https://dev.jupiter-noroshi.net";
		protected static Container _instance;

		static GlobalContainer()
		{
			_instance = new Container();
			_initialize();
		}

        public static uint PlayerID { get; private set; }
        public static string SessionID { get; private set; }
        /// <summary>
        /// ダウンロード済みマスターのバージョン文字列（エラー文字列が入っていることもある）。
        /// </summary>
        public static string MasterVersion { get; private set; }

		public static void SetFactory<T>(Func<T> factory)
		{
			_instance.SetFactory<T>(factory);
		}

        public static void SetPlayerIDAndSessionID(uint playerId, string sessionId)
        {
            PlayerID = playerId;
            SessionID = sessionId;
            // クラッシュレポート用。
            SmartBeat.SmartBeat.setUserId(PlayerID.ToString());
            SmartBeat.SmartBeat.addExtraData("APP_VERSION", AppConstant.APP_VERSION);
        }
        public static void ClearPlayerIDAndSessionID()
        {
            PlayerID = 0;
            SessionID = null;
        }

        /// <summary>
        /// マスターのバージョンを格納する。
        /// </summary>
        /// <param name="masterVersion">マスターバージョン</param>
        public static void SetMasterVersion(string masterVersion)
        {
            if (string.IsNullOrEmpty(MasterVersion))
            {
                MasterVersion = masterVersion;
            }
            else if (MasterVersion != masterVersion)
            {
                MasterVersion = "MultiVersionError";
            }
        }

		public static T Get<T>()
		{
			return _instance.Get<T>();
		}

        public static void Clear()
        {
            ClearPlayerIDAndSessionID();
            MasterVersion = null;
            _instance.Clear();
            _initialize();
        }

		static void _initialize()
		{
			SetFactory<ITimeHandler>(() => new TimeHandler());
            SetFactory<Logger>(() => new Logger());
            SetFactory<IWebApiRequester>(() => new WebApiRequester());
            SetFactory<RepositoryManager>(() => new RepositoryManager());
            SetFactory<MasterManager>(() => new MasterManager());
            SetFactory<AssetBundleManager>(() => new AssetBundleManager());
            SetFactory<SoundManager>(() => new SoundManager(new SoundFactory(), MasterManager.SoundMaster));
            SetFactory<LocalizationManager>(() => new LocalizationManager());
            SetFactory<IRandomGenerator>(() => new RandomGenerator());
            SetFactory<NotificationManager>(() => new NotificationManager());
            SetFactory<IGlobalModalHandler>(() => new GlobalModalHandler());

            _tryToLoadLocalConfig();
		}

        public static RepositoryManager RepositoryManager { get { return Get<RepositoryManager>(); } }
        public static ITimeHandler TimeHandler { get { return Get<ITimeHandler>(); } }
		public static Logger Logger { get { return Get<Logger>(); } }
        public static IWebApiRequester WebApiRequester { get { return Get<IWebApiRequester>(); } }
        public static MasterManager MasterManager { get { return Get<MasterManager>(); } }
        public static AssetBundleManager AssetBundleManager { get { return Get<AssetBundleManager>(); } }
        public static SoundManager SoundManager { get { return Get<SoundManager>(); } }
        public static LocalizationManager LocalizationManager { get { return Get<LocalizationManager>(); } }
        public static IRandomGenerator RandomGenerator { get { return Get<IRandomGenerator>(); } }
        public static Config Config { get { return Get<Config>(); } }
        public static NotificationManager NotificationManager { get { return Get<NotificationManager>(); } }
        public static IGlobalModalHandler GlobalModalHandler { get { return Get<IGlobalModalHandler>(); } }

        public static IObservable<bool> Load(ushort? tutorialStep)
        {
            return MasterManager.LoadAll(WebApiRequester)
                .SelectMany(_ => LocalizationManager.Load())
                .SelectMany(_ => AssetBundleManager.Load(tutorialStep))
                .SelectMany(_ => SoundManager.SetUp(1, 1, 1))
                .Select(_ => true);
        }

        static void _tryToLoadLocalConfig()
        {
            var filePath = "local";
            var textAsset = UnityEngine.Resources.Load(filePath) as UnityEngine.TextAsset;
            if (textAsset != null)
            {
                var localConfig = LitJson.JsonMapper.ToObject<Config>(textAsset.text);
                if (localConfig.WebApiHost == null) {
                    localConfig.SetWebApiHost(DEFAULT_API_HOST);
                }
                SetFactory<Config>(() => localConfig);
            }
            else
            {
                SetFactory<Config>(() => new Config(DEFAULT_API_HOST));
            }
        }
    }
    public class Config
    {
        public Config()
        {
        }
        public Config(string webApiHost)
        {
            WebApiHost = webApiHost;
        }
        public void SetWebApiHost(string webApiHost)
        {
            WebApiHost = webApiHost;
        }
        public string WebApiHost { get; private set; }
        public bool EditView { get; private set; }
        public string AssetBundle { get; private set; }
    }
}
