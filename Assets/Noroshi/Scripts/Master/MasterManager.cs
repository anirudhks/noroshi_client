﻿using System;
using System.Collections.Generic;
using System.IO;
using UniLinq;
using UniRx;
using Flaggs.Security;
using Flaggs.Extensions.Heam;
using Noroshi.WebApi;
using Noroshi.Cache;
using Noroshi.Core.WebApi.Response.Master;
using Noroshi.Core.Game.Possession;

namespace Noroshi.Master
{
    public class MasterManager
    {
        public readonly ConfigMaster ConfigMaster = new ConfigMaster();
        public readonly AssetBundleMaster AssetBundleMaster = new AssetBundleMaster();
        public readonly TextMaster TextMaster = new TextMaster();
        public readonly LevelMaster LevelMaster = new LevelMaster();
        public readonly SoundMaster SoundMaster = new SoundMaster();
        public readonly CharacterMaster CharacterMaster = new CharacterMaster();
        public readonly ItemMaster ItemMaster = new ItemMaster();
        public readonly PossessionLinkMaster PossessionLinkMaster = new PossessionLinkMaster();
        public readonly HelpMaster HelpMaster = new HelpMaster();

        public IObservable<MasterManager> LoadAll(IWebApiRequester webApiRequester)
        {
            var requests = new []
            {
                Observable.Defer(() => ConfigMaster.Load(webApiRequester).Select(_ => this)),
                Observable.Defer(() => AssetBundleMaster.Load(webApiRequester).Select(_ => this)),
                Observable.Defer(() => TextMaster.Load(webApiRequester).Select(_ => this)),
                Observable.Defer(() => LevelMaster.Load(webApiRequester).Select(_ => this)),
                Observable.Defer(() => SoundMaster.Load(webApiRequester).Select(_ => this)),
                Observable.Defer(() => CharacterMaster.Load(webApiRequester).Select(_ => this)),
                Observable.Defer(() => ItemMaster.Load(webApiRequester).Select(_ => this)),
                Observable.Defer(() => PossessionLinkMaster.Load(webApiRequester).Select(_ => this)),
                Observable.Defer(() => HelpMaster.Load(webApiRequester).Select(_ => this)),
            };
            return requests.Concat().Last().Select(_ => this);
        }
    }
    public abstract class AbstractMaster<T>
        where T : class, IMasterResponse
    {
        protected T _response;

        public virtual IObservable<T> Load(IWebApiRequester webApiRequester)
        {
            return _response != null || _loadFromLocalCache() ? Observable.Return(_response): webApiRequester.Get<T>(_url()).Do(_onResponse);
        }
        string _url()
        {
            return "Master/" + GetType().Name;
        }
        void _onResponse(T response)
        {
            _response = response;
            _saveToLocalCache();
        }
        void _saveToLocalCache()
        {
            var localStorage = new LocalStorage(GlobalContainer.PlayerID);
            localStorage.Set(_cachePath(), _response, _response.MasterVersion, TimeSpan.FromDays(1));
        }
        bool _loadFromLocalCache()
        {
            if (string.IsNullOrEmpty(GlobalContainer.MasterVersion)) return false;
            var localStorage = new LocalStorage(GlobalContainer.PlayerID);
            var response = localStorage.GetReferenceType<T>(_cachePath(), GlobalContainer.MasterVersion);
            if (response != null)
            {
                _response = response;
                return true;
            }
            return false;
        }
        string _cachePath()
        {
            return _url().Replace('/', '-');
        }
    }
    public class ConfigMaster : AbstractMaster<ConfigMasterResponse>
    {
        public override IObservable<ConfigMasterResponse> Load(IWebApiRequester webApiRequester)
        {
            return base.Load(webApiRequester).Do(response =>
            {
                GlobalContainer.SetMasterVersion(response.MasterVersion);
            });
        }

        public string GetStoreUrl()
        {
#if UNITY_IOS
            return _response.AppStoreUrl;
#elif UNITY_ANDROID
            return _response.GooglePlayUrl;
#else
            throw new SystemException();
#endif
        }
        public string GetPhotonAppID()
        {
            return _response.PhotonAppID;
        }
        public string GetAssetBundleBaseUrl()
        {
            return _response.AssetBundleBaseUrl;
        }
    }
    public class AssetBundleMaster : AbstractMaster<AssetBundleMasterResponse>
    {
        public Core.WebApi.Response.Master.AssetBundle[] GetAssetBundlesPublic()
        {
            return _response.AssetBundles.Where( asset => asset.Group == Core.Game.AssetBundle.Group.Public).ToArray();
        }

        public Core.WebApi.Response.Master.AssetBundle[] GetAssetBundleTutorial()
        {
            return _response.AssetBundles.Where( asset => asset.Group == Core.Game.AssetBundle.Group.Tutorial).ToArray();
        }
    }
    public class TextMaster : AbstractMaster<TextMasterResponse>
    {
        public DynamicText[] GetAllDynamicTexts()
        {
            return _response.DynamicTexts;
        }
    }
    public class LevelMaster : AbstractMaster<LevelMasterResponse>
    {
        Dictionary<ushort, Core.WebApi.Response.Master.PlayerLevel> _playerLevelCache;
        Dictionary<ushort, Core.WebApi.Response.Master.PlayerVipLevel> _playerVipLevelCache;
        Dictionary<ushort, Core.WebApi.Response.Master.CharacterLevel> _characterLevelCache;

        public Core.WebApi.Response.Master.PlayerLevel GetPlayerLevelByLevel(ushort level)
        {
            _setCacheIfNeed();
            return _playerLevelCache.ContainsKey(level) ? _playerLevelCache[level] : null;
        }
        public Core.WebApi.Response.Master.PlayerVipLevel GetPlayerVipLevelByLevel(ushort level)
        {
            _setCacheIfNeed();
            return _playerVipLevelCache.ContainsKey(level) ? _playerVipLevelCache[level] : null;
        }
        public Core.WebApi.Response.Master.CharacterLevel GetCharacterLevelByLevel(ushort level)
        {
            _setCacheIfNeed();
            return _characterLevelCache.ContainsKey(level) ? _characterLevelCache[level] : null;
        }

        void _setCacheIfNeed()
        {
            if (_playerLevelCache == null) _playerLevelCache = _response.PlayerLevels.ToDictionary(s => s.Level);
            if (_playerVipLevelCache == null) _playerVipLevelCache = _response.PlayerVipLevels.ToDictionary(s => s.Level);
            if (_characterLevelCache == null) _characterLevelCache = _response.CharacterLevels.ToDictionary(s => s.Level);
        }
    }
    public class SoundMaster : AbstractMaster<SoundMasterResponse>
    {
        Dictionary<uint, Core.WebApi.Response.Master.Sound> _cache;
        public Core.WebApi.Response.Master.Sound Get(uint id)
        {
            _setCacheIfNeed();
            return _cache[id];
        }
        void _setCacheIfNeed()
        {
            if (_cache == null) _cache = _response.Sounds.ToDictionary(s => s.ID);
        }
    }
    public class CharacterMaster : AbstractMaster<CharacterMasterResponse>
    {
        Dictionary<uint, Core.WebApi.Response.Master.Character> _characterCache;
        Dictionary<uint, Core.WebApi.Response.Master.Action> _actionCache;
        Dictionary<uint, Core.WebApi.Response.Master.Attribute> _attributeCache;
        Dictionary<uint, Core.WebApi.Response.Master.ShadowCharacter> _shadowCharacterCache;
        Dictionary<uint, Core.WebApi.Response.Master.CharacterEffect> _characterEffectCache;

        public Core.WebApi.Response.Master.Character Get(uint id)
        {
            _setCacheIfNeed();
            return _characterCache[id];
        }

        public Core.WebApi.Response.Master.CharacterEvolutionType GetCharacterEvolutionType(ushort type, byte evolutionLevel)
        {
            return _response.CharacterEvolutionTypes.Where(cet => cet.Type == type && cet.EvolutionLevel == evolutionLevel).FirstOrDefault();
        }

        public Core.WebApi.Response.Master.Action GetAction(uint id)
        {
            _setCacheIfNeed();
            return _actionCache[id];
        }
        public Core.WebApi.Response.Master.Action[] GetActions(IEnumerable<uint> ids)
        {
            _setCacheIfNeed();
            return ids.ToArrayWithOptimization(id => _actionCache[id]);
        }

        public Core.WebApi.Response.Master.Attribute GetAttribute(uint id)
        {
            _setCacheIfNeed();
            return _attributeCache[id];
        }
        public Core.WebApi.Response.Master.Attribute[] GetAttributes()
        {
            return _response.Attributes;
        }

        public Core.WebApi.Response.Master.ShadowCharacter GetShadowCharacter(uint id)
        {
            _setCacheIfNeed();
            return _shadowCharacterCache[id];
        }

        public Core.WebApi.Response.Master.CharacterEffect GetCharacterEffect(uint id)
        {
            _setCacheIfNeed();
            return _characterEffectCache.ContainsKey(id) ? _characterEffectCache[id] : null;
        }
        public Core.WebApi.Response.Master.CharacterEffect[] GetCharacterEffects()
        {
            return _response.CharacterEffects;
        }
        public Core.WebApi.Response.Master.ActionLevelUpPayment[] GetActionLevelUpPayments()
        {
            return _response.ActionLevelUpPayments;
        }
        void _setCacheIfNeed()
        {
            if (_characterCache == null) _characterCache = _response.Characters.ToDictionary(s => s.ID);
            if (_actionCache == null) _actionCache = _response.Actions.ToDictionary(s => s.ID);
            if (_attributeCache == null) _attributeCache = _response.Attributes.ToDictionary(s => s.ID);
            if (_shadowCharacterCache == null) _shadowCharacterCache = _response.ShadowCharacters.ToDictionary(s => s.ID);
            if (_characterEffectCache == null) _characterEffectCache = _response.CharacterEffects.ToDictionary(s => s.ID);
        }
    }
    public class ItemMaster : AbstractMaster<ItemMasterResponse>
    {
        Dictionary<uint, Core.WebApi.Response.Master.Soul> _soulCache;
        Dictionary<uint, Core.WebApi.Response.Master.Drug> _drugCache;
        Dictionary<uint, Core.WebApi.Response.Master.Gear> _gearCache;
        Dictionary<uint, Core.WebApi.Response.Master.GearPiece> _gearPieceCache;
        Dictionary<uint, Core.WebApi.Response.Master.GearEnchantMaterial> _gearEnchantMaterialCache;
        Dictionary<uint, Core.WebApi.Response.Master.ExchangeCashGift> _exchangeCashGiftCache;
        Dictionary<uint, Core.WebApi.Response.Master.RaidTicket> _raidTicketCache;
        Dictionary<uint, Core.WebApi.Response.Master.Soul> _characterToSoulMap;
        Dictionary<uint, Core.WebApi.Response.Master.GearRecipe[]> _craftItemIdToGearRecipesMap;

        public Core.WebApi.Response.Master.Soul GetSoul(uint id)
        {
            _setCacheIfNeed();
            return _soulCache.ContainsKey(id) ? _soulCache[id] : null;
        }
        public Core.WebApi.Response.Master.Soul[] GetSouls()
        {
            return _response.Souls;
        }
        public Core.WebApi.Response.Master.Soul GetSoulByCharacterID(uint characterId)
        {
            _setCacheIfNeed();
            return _characterToSoulMap.ContainsKey(characterId) ? _characterToSoulMap[characterId] : null;
        }

        public Core.WebApi.Response.Master.Drug GetDrug(uint id)
        {
            _setCacheIfNeed();
            return _drugCache.ContainsKey(id) ? _drugCache[id] : null;
        }
        public Core.WebApi.Response.Master.Drug[] GetDrugs()
        {
            return _response.Drugs;
        }

        public Core.WebApi.Response.Master.Gear GetGear(uint id)
        {
            _setCacheIfNeed();
            return _gearCache.ContainsKey(id) ? _gearCache[id] : null;
        }
        public Core.WebApi.Response.Master.Gear[] GetGears()
        {
            return _response.Gears;
        }

        public Core.WebApi.Response.Master.GearPiece GetGearPiece(uint id)
        {
            _setCacheIfNeed();
            return _gearPieceCache.ContainsKey(id) ? _gearPieceCache[id] : null;
        }
        public Core.WebApi.Response.Master.GearPiece[] GetGearPieces()
        {
            return _response.GearPieces;
        }

        public Core.WebApi.Response.Master.GearEnchantMaterial GetGearEnchantMaterial(uint id)
        {
            _setCacheIfNeed();
            return _gearEnchantMaterialCache.ContainsKey(id) ? _gearEnchantMaterialCache[id] : null;
        }
        public Core.WebApi.Response.Master.GearEnchantMaterial[] GetGearEnchantMaterials()
        {
            return _response.GearEnchantMaterials;
        }

        public Core.WebApi.Response.Master.GearRecipe[] GetGearRecipeByCraftItemID(uint craftItemId)
        {
            _setCacheIfNeed();
            return _craftItemIdToGearRecipesMap.ContainsKey(craftItemId) ? _craftItemIdToGearRecipesMap[craftItemId] : new Core.WebApi.Response.Master.GearRecipe[0];
        }

        public Core.WebApi.Response.Master.ExchangeCashGift GetExchangeCashGift(uint id)
        {
            _setCacheIfNeed();
            return _exchangeCashGiftCache.ContainsKey(id) ? _exchangeCashGiftCache[id] : null;
        }
        public Core.WebApi.Response.Master.ExchangeCashGift[] GetExchangeCashGifts()
        {
            return _response.ExchangeCashGifts;
        }

        public Core.WebApi.Response.Master.RaidTicket GetRaidTicket(uint id)
        {
            _setCacheIfNeed();
            return _raidTicketCache.ContainsKey(id) ? _raidTicketCache[id] : null;
        }
        public Core.WebApi.Response.Master.RaidTicket[] GetRaidTickets()
        {
            return _response.RaidTickets;
        }

        void _setCacheIfNeed()
        {
            if (_soulCache == null) _soulCache = _response.Souls.ToDictionary(s => s.ID);
            if (_drugCache == null) _drugCache = _response.Drugs.ToDictionary(s => s.ID);
            if (_gearCache == null) _gearCache = _response.Gears.ToDictionary(s => s.ID);
            if (_gearPieceCache == null) _gearPieceCache = _response.GearPieces.ToDictionary(s => s.ID);
            if (_gearEnchantMaterialCache == null) _gearEnchantMaterialCache = _response.GearEnchantMaterials.ToDictionary(s => s.ID);
            if (_exchangeCashGiftCache == null) _exchangeCashGiftCache = _response.ExchangeCashGifts.ToDictionary(s => s.ID);
            if (_raidTicketCache == null) _raidTicketCache = _response.RaidTickets.ToDictionary(s => s.ID);
            if (_characterToSoulMap == null) _characterToSoulMap = _response.Souls.ToDictionary(s => s.CharacterID);
            if (_craftItemIdToGearRecipesMap == null) _craftItemIdToGearRecipesMap = _response.GearRecipes.ToLookup(gr => gr.CraftGearID).ToDictionary(igrouping => igrouping.Key, igrouping => igrouping.ToArray());
        }
    }
    public class PossessionLinkMaster : AbstractMaster<PossessionLinkMasterResponse>
    {
        Dictionary<PossessionCategory, Dictionary<uint, List<PossessionStoryLink>>> _storyLinkCache;
        public PossessionStoryLink[] GetSoulStoryLinks(uint soulId)
        {
            _setCacheIfNeed();
            return _storyLinkCache[PossessionCategory.Soul][soulId].ToArray();
        }
        public PossessionStoryLink[] GetGearStoryLinks(uint gearId)
        {
            _setCacheIfNeed();
            return _storyLinkCache[PossessionCategory.Gear][gearId].ToArray();
        }
        public PossessionStoryLink[] GetGearPieceStoryLinks(uint gearPieceId)
        {
            _setCacheIfNeed();
            return _storyLinkCache[PossessionCategory.GearPiece][gearPieceId].ToArray();
        }
        void _setCacheIfNeed()
        {
            if (_storyLinkCache != null) return;
            _storyLinkCache = new Dictionary<PossessionCategory, Dictionary<uint, List<PossessionStoryLink>>>();
            foreach (var storyLink in _response.StoryLinks)
            {
                if (!_storyLinkCache.ContainsKey(storyLink.PossessionCategory))
                {
                    _storyLinkCache.Add(storyLink.PossessionCategory, new Dictionary<uint, List<PossessionStoryLink>>());
                }
                if (!_storyLinkCache[storyLink.PossessionCategory].ContainsKey(storyLink.ID))
                {
                    _storyLinkCache[storyLink.PossessionCategory].Add(storyLink.ID, new List<PossessionStoryLink>());
                }
                _storyLinkCache[storyLink.PossessionCategory][storyLink.ID].Add(storyLink);
            }
        }
    }
    public class HelpMaster : AbstractMaster<HelpMasterResponse>
    {
        public HelpCategory[] GetHelpCategories()
        {
            return _response.HelpCategories;
        }
        public Tip LotTip()
        {
            return GlobalContainer.RandomGenerator.Lot(_response.Tips);
        }
    }
}
