﻿using UnityEngine;
using MonoBehaviour = Noroshi.MonoBehaviours.MonoBehaviour;

namespace Noroshi.GlobalModal
{
    class SessionTimeOutPanel : MonoBehaviour
    {
        new void Awake()
        {
            base.Awake();
            SetActive(false);
        }

        /// <summary>
        /// ログインシーンへ遷移。
        /// </summary>
        public void GoToLoginScene()
        {
            GlobalContainer.GlobalModalHandler.Close(new GlobalModalEvent { Command = GlobalModalCommand.TransitToLoginScene });
        }
    }
}
