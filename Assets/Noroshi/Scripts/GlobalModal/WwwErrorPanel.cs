﻿using UnityEngine;
using MonoBehaviour = Noroshi.MonoBehaviours.MonoBehaviour;

namespace Noroshi.GlobalModal
{
    /// <summary>
    /// 通信エラー用パネル。
    /// </summary>
    public class WwwErrorPanel : MonoBehaviour
    {
        new void Awake()
        {
            base.Awake();
            SetActive(false);
        }

        /// <summary>
        /// 通信リトライ。
        /// </summary>
        public void Retry()
        {
            GlobalContainer.GlobalModalHandler.Close(new GlobalModalEvent { Command = GlobalModalCommand.Retry });
        }

        /// <summary>
        /// ログインシーンへ遷移。
        /// </summary>
        public void GoToLoginScene()
        {
            GlobalContainer.GlobalModalHandler.Close(new GlobalModalEvent { Command = GlobalModalCommand.TransitToLoginScene });
        }
    }
}
