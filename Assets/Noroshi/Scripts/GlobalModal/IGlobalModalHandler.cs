﻿using UniRx;

namespace Noroshi.GlobalModal
{
    /// <summary>
    /// グローバルモーダルを扱うインターフェース。
    /// </summary>
    public interface IGlobalModalHandler
    {
        /// <summary>
        /// オープン中のパネル。
        /// </summary>
        GlobalModalPanel? CurrentPanel { get; }
        /// <summary>
        /// モーダルオープン。
        /// </summary>
        /// <param name="panel">オープン対象パネル</param>
        IObservable<GlobalModalEvent> Open(GlobalModalPanel panel);
        /// <summary>
        /// モーダルクローズ。
        /// </summary>
        /// <param name="globalModalEvent">クローズ時に呼び出し元に渡されるイベント</param>
        void Close(GlobalModalEvent globalModalEvent);
    }
}
