﻿using UnityEngine;
using MonoBehaviour = Noroshi.MonoBehaviours.MonoBehaviour;

namespace Noroshi.GlobalModal
{
    /// <summary>
    /// アプリ強制バージョンアップ用パネル。
    /// </summary>
    public class AppVersionPanel : MonoBehaviour
    {
        new void Awake()
        {
            base.Awake();
            SetActive(false);
        }

        /// <summary>
        /// アプリストアへ遷移。
        /// </summary>
        public void GoToAppStore()
        {
            GlobalContainer.GlobalModalHandler.Close(new GlobalModalEvent { Command = GlobalModalCommand.TransitToStore });
        }
    }
}
