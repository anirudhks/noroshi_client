﻿using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Noroshi.GlobalModal
{
    /// <summary>
    /// グローバルモーダルを扱うクラス。
    /// </summary>
    public class GlobalModalHandler : IGlobalModalHandler
    {
        /// <summary>
        /// グローバルモーダルが所属するシーン名。
        /// </summary>
        const string SCENE_NAME = "GlobalModal";

        /// <summary>
        /// モーダルがクローズされる際にイベントが OnNext される Subject。
        /// </summary>
        Subject<GlobalModalEvent> _onCloseSubject = null;
        /// <summary>
        /// オープン中のパネル。
        /// </summary>
        public GlobalModalPanel? CurrentPanel { get; private set; }

        /// <summary>
        /// モーダルオープン。実質的には専用シーンを追加ロード。
        /// </summary>
        /// <param name="panel">オープン対象パネル</param>
        public IObservable<GlobalModalEvent> Open(GlobalModalPanel panel)
        {
            // 重複オープンは禁止。
            if (CurrentPanel != null) return Observable.Return(new GlobalModalEvent { Command = GlobalModalCommand.Cancel });
            CurrentPanel = panel;
            // 該当シーンを追加ロード。
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(SCENE_NAME, UnityEngine.SceneManagement.LoadSceneMode.Additive);
            _onCloseSubject = new Subject<GlobalModalEvent>();
            return _onCloseSubject.AsObservable().Do(globalModalEvent =>
            {
                switch (globalModalEvent.Command)
                {
                    case GlobalModalCommand.TransitToStore:
                        Application.OpenURL(GlobalContainer.MasterManager.ConfigMaster.GetStoreUrl());
                        GlobalContainer.AssetBundleManager.UnloadAll(); //エラー回避の為アンロード [Cannot load cached AssetBundle. A file of the same name is already loaded from another AssetBundle].
                        SceneManager.LoadScene("Login");
                        break;
                    case GlobalModalCommand.TransitToLoginScene:
                        GlobalContainer.AssetBundleManager.UnloadAll(); //エラー回避の為アンロード [Cannot load cached AssetBundle. A file of the same name is already loaded from another AssetBundle].
                        SceneManager.LoadScene("Login");
                        break;
                    default:
                        break;                
                }
            });
        }

        /// <summary>
        /// モーダルクローズ。実質的には専用シーンを破棄。
        /// </summary>
        /// <param name="globalModalEvent">クローズ時に呼び出し元に渡されるイベント</param>
        public void Close(GlobalModalEvent globalModalEvent)
        {
            UnityEngine.SceneManagement.SceneManager.UnloadScene(SCENE_NAME);
            var onCloseSubject = _onCloseSubject;
            _onCloseSubject = null;
            CurrentPanel = null;
            onCloseSubject.OnNext(globalModalEvent);
            onCloseSubject.OnCompleted();
        }
    }
}
