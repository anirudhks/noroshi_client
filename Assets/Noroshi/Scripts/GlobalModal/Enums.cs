﻿namespace Noroshi.GlobalModal
{
    public enum GlobalModalPanel
    {
        AppVersionPanel,
        MasterVersionPanel,
        SessionNotFoundPanel,
        SessionTimeOutPanel,
        WwwErrorPanel,
    }
    public enum GlobalModalCommand
    {
        TransitToLoginScene,
        TransitToStore,
        Retry,
        Cancel,
    }
}
