﻿namespace Noroshi.GlobalModal
{
    public struct GlobalModalEvent
    {
        public GlobalModalCommand Command { get; set; }
    }
}
