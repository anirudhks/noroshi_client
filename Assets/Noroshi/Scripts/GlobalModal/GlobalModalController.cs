﻿using System.Collections.Generic;
using UnityEngine;

namespace Noroshi.GlobalModal
{
    public class GlobalModalController : MonoBehaviour
    {
        [SerializeField] AppVersionPanel _appVersionPanel;
        [SerializeField] MasterVersionPanel _masterVersionPanel;
        [SerializeField] WwwErrorPanel _wwwErrorPanel;
        [SerializeField] SessionTimeOutPanel _sessionTimeOutPanel;
        [SerializeField] SessionNotFoundPanel _sessionNotFoundPanel;

        void Start()
        {
            var _panelMap = new Dictionary<GlobalModalPanel, Noroshi.MonoBehaviours.MonoBehaviour>
            {
                { GlobalModalPanel.AppVersionPanel, _appVersionPanel },
                { GlobalModalPanel.MasterVersionPanel, _masterVersionPanel },
                { GlobalModalPanel.WwwErrorPanel, _wwwErrorPanel },
                { GlobalModalPanel.SessionTimeOutPanel, _sessionTimeOutPanel },
                { GlobalModalPanel.SessionNotFoundPanel, _sessionNotFoundPanel },
            };
            // 現パネルのみをアクティブ化。
            foreach (var kv in _panelMap)
            {
                kv.Value.SetActive(kv.Key == GlobalContainer.GlobalModalHandler.CurrentPanel);
            }
        }
    }
}
