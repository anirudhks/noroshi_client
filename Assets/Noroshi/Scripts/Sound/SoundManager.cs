﻿using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Master;
using Noroshi.Core.Game.Sound;

namespace Noroshi.Sound
{
    /// <summary>
    /// サウンド管理クラス。事前ロード、動的ロード、両方に対応。
    /// シーン横断的に利用するので適切なタイミングでリソース解放をしながら利用すること。
    /// </summary>
    public class SoundManager
    {
        /// <summary>
        /// サウンド関連ファクトリー。
        /// </summary>
        ISoundFactory _soundFactory;
        /// <summary>
        /// サウンドマスター。
        /// </summary>
        SoundMaster _soundMaster;
        /// <summary>
        /// サウンドミキサー。
        /// </summary>
        ISoundMixer _soundMixer;
        /// <summary>
        /// 再生プレイヤープール。
        /// </summary>
        Dictionary<SoundType, Queue<ISoundPlayer>> _soundTypeToSoundPlayerQueue = new Dictionary<SoundType, Queue<ISoundPlayer>>();
        /// <summary>
        /// ロード済みサウンドマップ。
        /// </summary>
        Dictionary<uint, ISound> _soundMap = new Dictionary<uint, ISound>();
        /// <summary>
        /// 利用中の再生プレイヤー。
        /// </summary>
        Dictionary<string, Dictionary<uint, ISoundPlayer>> _soundPlayerGroupKeyToSoundIdToPlayingSoundPlayerMap = new Dictionary<string, Dictionary<uint, ISoundPlayer>>();
        /// <summary>
        /// 再生中BGMのサウンドID。
        /// </summary>
        uint? _currentBgmSoundId;

        /// <summary>
        /// サウンドマスターを渡してインスタンス化する。
        /// </summary>
        /// <param name="soundFactory">サウンド関連ファクトリー</param>
        /// <param name="soundMaster">サウンドマスター</param>
        public SoundManager(ISoundFactory soundFactory, SoundMaster soundMaster)
        {
            _soundFactory = soundFactory;
            _soundMaster = soundMaster;
            foreach (var soundType in Enum.GetValues(typeof(SoundType)).Cast<SoundType>())
            {
                _soundTypeToSoundPlayerQueue[soundType] = new Queue<ISoundPlayer>();
            }
        }

        /// <summary>
        /// 内部に抱えているリソースを解放する。
        /// </summary>
        public void Clear()
        {
            foreach (var kv in _soundPlayerGroupKeyToSoundIdToPlayingSoundPlayerMap)
            {
                foreach (var soundId in kv.Value.Keys)
                {
                    Stop(soundId, kv.Key);
                }
            }
            _soundPlayerGroupKeyToSoundIdToPlayingSoundPlayerMap.Clear();
            foreach (var kv in _soundMap)
            {
                kv.Value.Dispose();
            }
            _soundMap.Clear();
            foreach (var kv in _soundTypeToSoundPlayerQueue)
            {
                var count = kv.Value.Count();
                for (var i = 0; i < count; i++)
                {
                    kv.Value.Dequeue().Dispose();
                }
            }
        }

        /// <summary>
        /// セットアップする。
        /// </summary>
        /// <param name="seVolume">SEボリューム</param>
        /// <param name="bgmVolume">BGMボリューム</param>
        /// <param name="voiceVolume">ボイスボリューム</param>
        /// <returns>自身が OnNext される Observable</returns>
        public IObservable<SoundManager> SetUp(float seVolume, float bgmVolume, float voiceVolume)
        {
            return _soundFactory.LoadSoundMixer().Select(sm =>
            {
                _soundMixer = sm;
                _soundMixer.SetVolume(SoundType.SE, seVolume);
                _soundMixer.SetVolume(SoundType.Bgm, bgmVolume);
                _soundMixer.SetVolume(SoundType.Voice, voiceVolume);
                return this;
            });
        }
        /// <summary>
        /// ボリュームをセットする。
        /// </summary>
        /// <param name="soundType">サウンド種別</param>
        /// <param name="ratio">ボリューム（0 ~ 1.0f）</param>
        public void SetVolume(SoundType soundType, float volume)
        {
            _soundMixer.SetVolume(soundType, volume);
        }

        /// <summary>
        /// サウンドを事前ロードする。
        /// </summary>
        /// <param name="soundIds">対象サウンドID</param>
        /// <returns>ロードされた全サウンドが OnNext される Observable</returns>
        public IObservable<ISound[]> PreloadSounds(IEnumerable<uint> soundIds)
        {
            return _loadSounds(soundIds);
        }
        /// <summary>
        /// サウンドをキャッシュから取得、キャッシュになければ動的にロードして取得する。
        /// </summary>
        /// <param name="soundId">対象サウンドID</param>
        /// <returns>取得したサウンドが OnNext される Observable</returns>
        IObservable<ISound> _getOrLoadSound(uint soundId)
        {
            return _getOrLoadSounds(new[] { soundId }).Select(sounds => sounds[0]);
        }
        /// <summary>
        /// サウンドをキャッシュから取得、キャッシュになければ動的にロードして取得する。
        /// </summary>
        /// <param name="soundIds">対象サウンドID</param>
        /// <returns>取得した全サウンドが OnNext される Observable</returns>
        IObservable<ISound[]> _getOrLoadSounds(IEnumerable<uint> soundIds)
        {
            // まずは既にロードされているかをフィルタリング。
            var loadedSoundIds = new List<uint>();
            var notLoadedSoundIds = new List<uint>();
            foreach (var soundId in soundIds)
            {
                if (_soundMap.ContainsKey(soundId))
                {
                    loadedSoundIds.Add(soundId);
                }
                else
                {
                    notLoadedSoundIds.Add(soundId);
                }
            }
            if (notLoadedSoundIds.Count == 0) return Observable.Return(soundIds.Select(soundId => _soundMap[soundId]).ToArray());
            return _loadSounds(notLoadedSoundIds).Select(_ => soundIds.Select(soundId => _soundMap[soundId]).ToArray());
        }
        /// <summary>
        /// サウンドを動的にロードして取得する。
        /// </summary>
        /// <param name="soundIds">対象サウンドID</param>
        /// <returns>ロードされた全サウンドが OnNext される Observable</returns>
        IObservable<ISound[]> _loadSounds(IEnumerable<uint> soundIds)
        {
            var notLoadedSoundMasters = soundIds.Where(soundId => !_soundMap.ContainsKey(soundId)).Select(soundId => _soundMaster.Get(soundId));
            return _soundFactory.LoadSounds(notLoadedSoundMasters)
            .Select(ss =>
            {
                for (var i = 0; i < ss.Length; i++)
                {
                    // ロード重複ケースは回避できない。
                    if (_soundMap.ContainsKey(ss[i].ID))
                    {
                        ss[i].Dispose();
                    }
                    else
                    {
                        _soundMap.Add(ss[i].ID, ss[i]);
                    }
                }
                return soundIds.Select(soundId => _soundMap[soundId]).ToArray();
            });
        }

        /// <summary>
        /// 再生プレイヤーを事前ロードする。
        /// </summary>
        /// <param name="sePlayerNum">SE再生プレイヤーロード数</param>
        /// <param name="bgmPlayerNum">BGM再生プレイヤーロード数</param>
        /// <param name="voicePlayerNum">ボイス再生プレイヤーロード数</param>
        /// <returns>ロードされた全再生プレイヤーが OnNext される Observable</returns>
        public IObservable<ISoundPlayer[]> PreloadSoundPlayers(byte sePlayerNum, byte bgmPlayerNum, byte voicePlayerNum)
        {
            return Observable.WhenAll(
                _loadSePlayers(sePlayerNum).Do(sps => _enqueueSoundPlayers(SoundType.SE, sps)),
                _loadBgmPlayers(bgmPlayerNum).Do(sps => _enqueueSoundPlayers(SoundType.Bgm, sps)),
                _loadVoicePlayers(voicePlayerNum).Do(sps => _enqueueSoundPlayers(SoundType.Voice, sps))
            )
            .Select(spss => spss.SelectMany(sps => sps).ToArray());
        }
        /// <summary>
        /// 再生プレイヤーをキューに詰める。
        /// </summary>
        /// <param name="soundType">サウンド種別</param>
        /// <param name="soundPlayer">再生プレイヤー</param>
        void _enqueueSoundPlayer(SoundType soundType, ISoundPlayer soundPlayer)
        {
            _soundTypeToSoundPlayerQueue[soundType].Enqueue(soundPlayer);
        }
        /// <summary>
        /// 再生プレイヤーをキューに詰める。
        /// </summary>
        /// <param name="soundType">サウンド種別</param>
        /// <param name="soundPlayers">再生プレイヤー</param>
        void _enqueueSoundPlayers(SoundType soundType, ISoundPlayer[] soundPlayers)
        {
            for (var i = 0; i < soundPlayers.Length; i++)
            {
                _soundTypeToSoundPlayerQueue[soundType].Enqueue(soundPlayers[i]);
            }
        }

        /// <summary>
        /// 再生プレイヤーをキューから取得、足りない分は動的にロードして取得する。
        /// </summary>
        /// <param name="soundType">サウンド種別</param>
        /// <returns>取得した再生プレイヤーが OnNext される Observable</returns>
        IObservable<ISoundPlayer> _dequeueOrLoadSoundPlayer(SoundType soundType)
        {
            return _dequeueOrLoadSoundPlayers(soundType, 1).Select(sps => sps[0]);
        }
        /// <summary>
        /// 再生プレイヤーをキューから取得、足りない分は動的にロードして取得する。
        /// </summary>
        /// <param name="soundType">サウンド種別</param>
        /// <param name="num">再生プレイヤーロード数</param>
        /// <returns>取得した全再生プレイヤーが OnNext される Observable</returns>
        IObservable<ISoundPlayer[]> _dequeueOrLoadSoundPlayers(SoundType soundType, byte num)
        {
            switch (soundType)
            {
                case SoundType.SE:
                    return _dequeueOrLoadSoundPlayers(SoundType.SE, num, _loadSePlayers);
                case SoundType.Bgm:
                    return _dequeueOrLoadSoundPlayers(SoundType.Bgm, num, _loadBgmPlayers);
                case SoundType.Voice:
                    return _dequeueOrLoadSoundPlayers(SoundType.Voice, num, _loadVoicePlayers);
                default:
                    throw new InvalidOperationException();
            }
        }
        /// <summary>
        /// 再生プレイヤーをキューから取得、足りない分は動的にロードして取得する。ただし、本メソッドを直接呼ぶことは禁止。
        /// </summary>
        /// <param name="soundType">サウンド種別</param>
        /// <param name="num">再生プレイヤーロード数</param>
        /// <param name="loadSoundPlayers">再生プレイヤーロード処理</param>
        /// <returns>取得した全再生プレイヤーが OnNext される Observable</returns>
        IObservable<ISoundPlayer[]> _dequeueOrLoadSoundPlayers(SoundType soundType, byte num, Func<byte, IObservable<ISoundPlayer[]>> loadSoundPlayers)
        {
            var soundPlayerNumFromQueue = Math.Min(_soundTypeToSoundPlayerQueue[soundType].Count(), num);
            var soundPlayersFromQueue = new ISoundPlayer[soundPlayerNumFromQueue];
            for (var i = 0; i < soundPlayerNumFromQueue; i++)
            {
                soundPlayersFromQueue[i] = _soundTypeToSoundPlayerQueue[soundType].Dequeue();
            }
            var soundPlayerNumFromLoading = (byte)(num - soundPlayerNumFromQueue);
            if (soundPlayerNumFromLoading == 0) return Observable.Return(soundPlayersFromQueue);
            return loadSoundPlayers(soundPlayerNumFromLoading).SelectMany(soundPlayersFromLoading =>
            {
                // 可読性よりも最適化を優先して concat しないで自前実装。
                var soundPlayers = new ISoundPlayer[soundPlayersFromQueue.Length + soundPlayersFromLoading.Length];
                for (var i = 0; i < soundPlayersFromQueue.Length; i++)
                {
                    soundPlayers[i] = soundPlayersFromQueue[i];
                }
                for (var i = 0; i < soundPlayersFromLoading.Length; i++)
                {
                    soundPlayers[i + soundPlayerNumFromQueue] = soundPlayersFromLoading[i];
                }
                return Observable.Return(soundPlayers);
            });
        }
        /// <summary>
        /// SE再生プレイヤーをロードする。
        /// </summary>
        /// <param name="num">再生プレイヤーロード数</param>
        /// <returns>ロードした全再生プレイヤーが OnNext される Observable</returns>
        IObservable<ISoundPlayer[]> _loadSePlayers(byte num)
        {
            return _soundFactory.LoadSoundPlayers(SoundType.SE, num);
        }
        /// <summary>
        /// BGM再生プレイヤーをロードする。
        /// </summary>
        /// <param name="num">再生プレイヤーロード数</param>
        /// <returns>ロードした全再生プレイヤーが OnNext される Observable</returns>
        IObservable<ISoundPlayer[]> _loadBgmPlayers(byte num)
        {
            return _soundFactory.LoadSoundPlayers(SoundType.Bgm, num);
        }
        /// <summary>
        /// ボイス再生プレイヤーをロードする。
        /// </summary>
        /// <param name="num">再生プレイヤーロード数</param>
        /// <returns>ロードした全再生プレイヤーが OnNext される Observable</returns>
        IObservable<ISoundPlayer[]> _loadVoicePlayers(byte num)
        {
            return _soundFactory.LoadSoundPlayers(SoundType.Voice, num);
        }

        /// <summary>
        /// サウンドを再生する。
        /// </summary>
        /// <param name="soundId">再生するサウンドID</param>
        /// <param name="soundPlayerGroupKey">再生プレイヤーが所属するグループキー</param>
        /// <returns>再生終了時に正常終了かどうかが真偽値で OnNext される Observable</returns>
        public IObservable<bool> Play(uint soundId, string soundPlayerGroupKey = "")
        {
            return _getOrLoadSound(soundId).SelectMany(sound =>
            {
                return _dequeueOrLoadSoundPlayer(sound.Type).SelectMany(soundPlayer =>
                {
                    if (!_soundPlayerGroupKeyToSoundIdToPlayingSoundPlayerMap.ContainsKey(soundPlayerGroupKey))
                    {
                        _soundPlayerGroupKeyToSoundIdToPlayingSoundPlayerMap.Add(soundPlayerGroupKey, new Dictionary<uint, ISoundPlayer>());
                    }
                    // 該当サウンドを再生中のプレイヤーが既に存在すればストップして削除。
                    if (_soundPlayerGroupKeyToSoundIdToPlayingSoundPlayerMap[soundPlayerGroupKey].ContainsKey(soundId))
                    {
                        var previousPlayer = _soundPlayerGroupKeyToSoundIdToPlayingSoundPlayerMap[soundPlayerGroupKey][soundId];
                        previousPlayer.Stop();
                        if (_soundPlayerGroupKeyToSoundIdToPlayingSoundPlayerMap[soundPlayerGroupKey].Remove(soundId))
                        {
                            _enqueueSoundPlayer(sound.Type, previousPlayer);
                        }
                    }
                    if (sound.Type == SoundType.Bgm)
                    {
                        if (_currentBgmSoundId.HasValue) Stop(_currentBgmSoundId.Value);
                        _currentBgmSoundId = sound.ID;
                    }
                    _soundPlayerGroupKeyToSoundIdToPlayingSoundPlayerMap[soundPlayerGroupKey].Add(soundId, soundPlayer);
                    return soundPlayer.Play(sound);
                });
            });
        }
        /// <summary>
        /// 再生中のサウンドを停止する。
        /// 本メソッドによって停止された場合、該当サウンドは正常終了ではないと判断される。
        /// </summary>
        /// <param name="soundId">停止するサウンドID</param>
        /// <param name="soundPlayerGroupKey">再生プレイヤーが所属するグループキー</param>
        public void Stop(uint soundId, string soundPlayerGroupKey = "")
        {
            if (_currentBgmSoundId.HasValue && _currentBgmSoundId.Value == soundId) _currentBgmSoundId = null;
            _soundPlayerGroupKeyToSoundIdToPlayingSoundPlayerMap[soundPlayerGroupKey][soundId].Stop();
        }
        /// <summary>
        /// 再生中のサウンドをキャンセルする。
        /// </summary>
        /// <param name="soundId">キャンセルするサウンドID</param>
        /// <param name="soundPlayerGroupKey">再生プレイヤーが所属するグループキー</param>
        public void Cancel(uint soundId, string soundPlayerGroupKey = "")
        {
            if (!_soundMap.ContainsKey(soundId) || _soundMap[soundId].PlayType != SoundPlayType.CancelableOneShot) return;
            _soundPlayerGroupKeyToSoundIdToPlayingSoundPlayerMap[soundPlayerGroupKey][soundId].Stop();
        }
    }
}
