﻿using System.Collections.Generic;
using UnityEngine;
using Noroshi.Core.Game.Sound;

namespace Noroshi.Sound
{
    public class Sound : ISound
    {
        AudioClip[] _audioClips;
        public string Path { get; private set;}
        public uint ID { get; private set; }
        public SoundType Type { get;  private set; }
        public SoundPlayType PlayType { get;  private set; }

        public Sound(uint id, string path, SoundType type, SoundPlayType playType)
        {
            ID = id;
            Path = path;
            Type = type;
            PlayType = playType;
        }

        public IEnumerable<string> GetAudioClipPathes()
        {
            return PlayType == SoundPlayType.LoopWithIntro ? new[] { Path + "_intro", Path + "_loop" } : new[] { Path };
        }
        public AudioClip[] GetAudioClips()
        {
            return _audioClips;
        }
        public void SetAudioClips(AudioClip[] audioClips)
        {
            _audioClips = audioClips;
        }

        public void Dispose()
        {
        }
    }
}
