﻿using Noroshi.Core.Game.Sound;

namespace Noroshi.Sound
{
    /// <summary>
    /// サウンドミキサー。
    /// </summary>
    public interface ISoundMixer
    {
        /// <summary>
        /// ボリュームをセットする。
        /// </summary>
        /// <param name="soundType">サウンド種別</param>
        /// <param name="ratio">ボリューム（0 ~ 1.0f）</param>
        void SetVolume(SoundType soundType, float volume);

        /// <summary>
        /// リソースを破棄する。
        /// </summary>
        void Dispose();
    }
}
