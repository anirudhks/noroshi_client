﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Noroshi.Core.Game.Sound;

namespace Noroshi.Sound
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundPlayer : Noroshi.MonoBehaviours.MonoBehaviour, ISoundPlayer
    {
        [SerializeField] SoundType _soundType;
        AudioSource[] _audioSources;
        ISound _playingSound;
        Subject<bool> _onFinishSubject;
        IDisposable _timerDisposable;

        new void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(_gameObject);
            var audioSource = GetComponent<AudioSource>();
            var audioSources = new List<AudioSource>{audioSource};
            if (_soundType == SoundType.Bgm)
            {
                var newAudioSource = _gameObject.AddComponent<AudioSource>();
                newAudioSource.outputAudioMixerGroup = audioSource.outputAudioMixerGroup;
                audioSources.Add(newAudioSource);
            }
            _audioSources = audioSources.ToArray();
        }

        public IObservable<bool> Play(ISound sound)
        {
            _playingSound = sound;
            switch (sound.PlayType)
            {
                case SoundPlayType.Loop:
                    return _playLoop(sound);
                case SoundPlayType.LoopWithIntro:
                    return _playLoopWithIntro(sound);
                case SoundPlayType.OneShot:
                    return _playOneShot(sound);
                case SoundPlayType.CancelableOneShot:
                    return _playOneShot(sound);
                default:
                    throw new InvalidOperationException();
            }
        }
        IObservable<bool> _playLoop(ISound sound)
        {
            _audioSources[0].clip = ((Sound)sound).GetAudioClips()[0];
            _audioSources[0].Play();
            _audioSources[0].loop = true;
            _onFinishSubject = new Subject<bool>();
            return _onFinishSubject.AsObservable();
        }

        IObservable<bool> _playLoopWithIntro(ISound sound)
        {
            _audioSources[0].clip = ((Sound)sound).GetAudioClips()[0];
            _audioSources[0].Play();
            _audioSources[0].loop = false;
            _audioSources[1].clip = ((Sound)sound).GetAudioClips()[1];
            _audioSources[1].PlayDelayed(_audioSources[0].clip.length);
            _audioSources[1].loop = true;

            _onFinishSubject = new Subject<bool>();
            return _onFinishSubject.AsObservable();
        }
        IObservable<bool> _playOneShot(ISound sound)
        {
            _audioSources[0].clip = ((Sound)sound).GetAudioClips()[0];
            _audioSources[0].Play();
            _audioSources[0].loop = false;

            _onFinishSubject = new Subject<bool>();
            _timerDisposable = Observable.Timer(TimeSpan.FromSeconds(_audioSources[0].clip.length)).Subscribe(_ => _onFinishSubject.OnNext(true));
            return _onFinishSubject.AsObservable();
        }

        public void Stop()
        {
            _stop(_playingSound, false);
        }
        void _stop(ISound sound, bool success)
        {
            for (var i = 0; i < _audioSources.Length; i++)
            {
                _audioSources[i].Stop();
            }
            if (_timerDisposable != null) _timerDisposable.Dispose();
            _onFinishSubject.OnNext(success);
            _onFinishSubject.OnCompleted();
        }
    }
}
