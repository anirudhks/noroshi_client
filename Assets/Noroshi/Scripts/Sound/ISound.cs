﻿using Noroshi.Core.Game.Sound;

namespace Noroshi.Sound
{
    /// <summary>
    /// サウンド。
    /// </summary>
    public interface ISound
    {
        /// <summary>
        /// サウンドID。
        /// </summary>
        uint ID { get; }
        /// <summary>
        /// サウンド種別。
        /// </summary>
        SoundType Type { get; }
        /// <summary>
        /// 再生方式。
        /// </summary>
        SoundPlayType PlayType { get; }

        /// <summary>
        /// リソースを破棄する。
        /// </summary>
        void Dispose();
    }
}
