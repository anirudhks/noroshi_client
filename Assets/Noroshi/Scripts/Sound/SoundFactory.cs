﻿using System.Collections.Generic;
using UniLinq;
using UniRx;
using UnityEngine;
using Noroshi.Core.Game.Sound;
using Noroshi.MonoBehaviours;

namespace Noroshi.Sound
{
    public class SoundFactory : ISoundFactory
    {
        const string SOUND_DIR = "Sound/";
        const string BGM_SOUND_PLAYER_PATH = "Sound/SoundPlayer/BgmPlayer";
        const string SE_SOUND_PLAYER_PATH = "Sound/SoundPlayer/SEPlayer";
        const string VOICE_SOUND_PLAYER_PATH = "Sound/SoundPlayer/VoicePlayer";
        const string SOUND_MIXER_PATH = "Sound/Mixer/SoundMixer";
        static Dictionary<SoundType, string> SOUND_PLAYER_PATH_MAP = new Dictionary<SoundType, string>
        {
            {SoundType.SE, SE_SOUND_PLAYER_PATH},
            {SoundType.Bgm, BGM_SOUND_PLAYER_PATH},
            {SoundType.Voice, VOICE_SOUND_PLAYER_PATH},
        };

        public IObservable<ISoundPlayer[]> LoadSoundPlayers(SoundType soundType, byte num)
        {
            var path = SOUND_PLAYER_PATH_MAP[soundType];
            return Enumerable.Range(0, num).Select(i => Loader.LoadComponentFromResourceAsync<SoundPlayer>(path).Select(sp => (ISoundPlayer)sp)).WhenAll();
        }

        public IObservable<ISound[]> LoadSounds(IEnumerable<Core.WebApi.Response.Master.Sound> soundMasters)
        {
            var sounds = soundMasters.Select(master =>
            {
                return new Sound(master.ID, master.Path, master.Type, master.PlayType);
            });
            //return sounds.Select(sound => _loadAudioClips(sound.GetAudioClipPathes()).Select(audioClips =>
            return sounds.Select(sound => GlobalContainer.AssetBundleManager.LoadAsyncAudioClips(sound.Type, sound.GetAudioClipPathes()).Select(audioClips =>
            {
                sound.SetAudioClips(audioClips);
                return (ISound)sound;
            })).WhenAll();
        }
        IObservable<AudioClip[]> _loadAudioClips(IEnumerable<string> audioClipPathes)
        {
            return audioClipPathes.Select(path => Loader.LoadObjectFromResourceAsync<AudioClip>(SOUND_DIR + path)).WhenAll();
        }

        public IObservable<ISoundMixer> LoadSoundMixer()
        {
            return Loader.LoadComponentFromResourceAsync<SoundMixer>(SOUND_MIXER_PATH).Select(sp => (ISoundMixer)sp);
        }
    }
}
