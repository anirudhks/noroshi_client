﻿using System.Collections.Generic;
using UniRx;
using Noroshi.Core.Game.Sound;

namespace Noroshi.Sound
{
    /// <summary>
    /// サウンド関連ファクトリー。
    /// </summary>
    public interface ISoundFactory
    {
        /// <summary>
        /// サウンドをロードする。
        /// </summary>
        /// <param name="soundMasters">対象サウンドマスター</param>
        /// <returns>ロードされた全サウンドが OnNext される Observable</returns>
        IObservable<ISound[]> LoadSounds(IEnumerable<Core.WebApi.Response.Master.Sound> soundMasters);

        /// <summary>
        /// 再生プレイヤーをロードする。
        /// </summary>
        /// <param name="soundType">サウンド種別</param>
        /// <param name="num">ロード数</param>
        /// <returns>ロードされた全再生プレイヤーが OnNext される Observable</returns>
        IObservable<ISoundPlayer[]> LoadSoundPlayers(SoundType soundType, byte num);

        /// <summary>
        /// ミキサーをロードする。
        /// </summary>
        /// <returns>ロードされたミキサーが OnNext される Observable</returns>
        IObservable<ISoundMixer> LoadSoundMixer();
    }
}
