﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Noroshi.Core.Game.Sound;

namespace Noroshi.Sound
{
    public class SoundMixer : MonoBehaviour, ISoundMixer
    {
        static readonly Dictionary<SoundType, string> VOLUME_PARAMETER_MAP = new Dictionary<SoundType, string>
        {
            { SoundType.SE, "SEVolume" },
            { SoundType.Bgm, "BgmVolume" },
            { SoundType.Voice, "VoiceVolume" },
        };

        [SerializeField] AudioMixer _audioMixer;

        public void SetVolume(SoundType soundType, float volume)
        {
            _audioMixer.SetFloat(VOLUME_PARAMETER_MAP[soundType], Mathf.Lerp(-80, 0, volume));
        }

        public void Dispose()
        {
            Destroy(_audioMixer);
        }
    }
}
