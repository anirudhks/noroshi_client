﻿using UniRx;

namespace Noroshi.Sound
{
    /// <summary>
    /// サウンド再生プレイヤー。
    /// 同時再生数は1で、複数のサウンドを再生したい場合は複数の再生プレイヤーを利用する必要がある。
    /// </summary>
    public interface ISoundPlayer
    {
        /// <summary>
        /// サウンドを再生する。
        /// </summary>
        /// <param name="sound">再生するサウンド</param>
        /// <returns>再生終了時に正常終了かどうかが真偽値で OnNext される Observable</returns>
        IObservable<bool> Play(ISound sound);

        /// <summary>
        /// 再生中のサウンドを停止する。
        /// 本メソッドによって停止された場合、該当サウンドは正常終了ではないと判断される。
        /// </summary>
        void Stop();

        /// <summary>
        /// リソースを破棄する。
        /// </summary>
        void Dispose();
    }
}
