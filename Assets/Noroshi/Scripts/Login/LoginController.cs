﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Flaggs.Client.Account;
using Noroshi.Core.WebApi.Response.Account;
using Noroshi.Core.WebApi.Response.Players;
using Noroshi.AssetBundle;
using Noroshi.UI;

namespace Noroshi.Login
{
    public class LoginController : MonoBehaviour
    {
        [SerializeField] GameObject splashImg;
        [SerializeField] GameObject uiContainer;
        [SerializeField] GameObject processing;
        [SerializeField] GameObject overlay;
        [SerializeField] Text txtID;
        [SerializeField] Text txtAppVersion;
        [SerializeField] TermsPanel termsPanel;
        [SerializeField] ChangeNamePanel changeNamePanel;
        [SerializeField] BtnCommon btnStartGame;

        Subject<bool> _onRegisterSubject = new Subject<bool>();

        bool _isRegistered = true;

        void Awake()
        {
            GlobalContainer.Clear();
// TODO : 出し分け
//            #if UNITY_IPHONE
//            LoginWithGameCenterAccount();
//            #elif UNITY_ANDROID
//            LoginWithGooglePlayGamesAccount();
//            #endif
            LoginWithNoroshiAccount();
        }

        void Start()
        {
            Screen.autorotateToLandscapeLeft = true;
            Screen.autorotateToLandscapeRight = true;
            Screen.autorotateToPortrait = false;
            Screen.autorotateToPortraitUpsideDown = false;
            Screen.orientation = ScreenOrientation.AutoRotation;

            btnStartGame.OnClickedBtn.Subscribe(_ =>
            {
                processing.SetActive(true);
                _checkRegistered();
            });
            btnStartGame.OnPlaySE.Subscribe(_ =>
            {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });

            termsPanel.OnCloseTerms.Subscribe(_ => {
                changeNamePanel.OpenChangeNamePanel();
            });

            /// <summary>
            /// プレイヤーネーム受け取って登録処理開始
            /// </summary>
            changeNamePanel.OnDecideName.Subscribe(name =>
            {
                processing.SetActive(true);
                _register(name).Subscribe().AddTo(this);
            });
        }

        /// <summary>
        /// Game Center アカウントを利用いてログイン処理を実行する。
        /// </summary>
        public void LoginWithGameCenterAccount()
        {
            _loginWithAccount(GameCenterAccount.Instance, "Account/LoginWithGameCenterAccount")
                .Subscribe().AddTo(this);
        }

        /// <summary>
        /// Google Play Games アカウントを利用いてログイン処理を実行する。
        /// </summary>
        public void LoginWithGooglePlayGamesAccount()
        {
            _loginWithAccount(GooglePlayGamesAccount.Instance, "Account/LoginWithGooglePlayGamesAccount")
                .Subscribe().AddTo(this);
        }

        /// <summary>
        /// ログイン共通処理。
        /// </summary>
        /// <param name="account">ログインに利用するアカウント</param>
        /// <param name="path">バックエンドサーバー認証パス</param>
        public IObservable<bool> _loginWithAccount(IAccount account, string path)
        {
            return account.Authenticate()
                .Where(success => success)
                .SelectMany(_ => account.AuthenticateWithBackendServer<LoginResponse>(_postBackendServer(path)))
                .SelectMany(response => _onAuthenticateWithBackendServer(response));
        }
        /// <summary>
        /// 認証のためにバックエンドサーバーに POST する。
        /// </summary>
        /// <param name="path">認証用パス</param>
        Func<WWWForm, IObservable<LoginResponse>> _postBackendServer(string path)
        {
            return form => GlobalContainer.WebApiRequester.Post<LoginResponse>(path, form);
        }
        /// <summary>
        /// バックエンドサーバー認証後の処理。
        /// </summary>
        /// <returns>The authenticate with backend server.</returns>
        /// <param name="response">Response.</param>
        IObservable<bool> _onAuthenticateWithBackendServer(LoginResponse response)
        {
            Debug.Log(response.SessionID);
            if (response.Error == null)
            {
                GlobalContainer.SetPlayerIDAndSessionID(response.PlayerID, response.SessionID);

                ushort? tutorialStep = null;
                if (response.PlayerStatus != null) tutorialStep = response.PlayerStatus.TutorialStep;
                return GlobalContainer.Load(tutorialStep).SelectMany(_ =>
                    {
                        _showUIParts(response);
                        if (response.PlayerStatus == null) _isRegistered = false;
                        return _waitRegisterIfNeed(response);
                    })
                    .Do(_ =>
                    {
                        var notificationManager = GlobalContainer.NotificationManager;
                        if (response.PlayerStatus == null) {
                            _startGame();
                        } else {
                            _isRegistered = true;
                        }
                    });
            }
            else
            {
                return Observable.Return(false);
            }
        }
        IObservable<bool> _waitRegisterIfNeed(LoginResponse response)
        {
            return response.PlayerStatus != null ? Observable.Return(true) : _onRegisterSubject.AsObservable();
        }

        /// <summary>
        /// プレイヤー登録処理。ログイン処理後に呼び出すこと。
        /// </summary>
        /// <param name="name">プレイヤー名</param>
        IObservable<RegisterResponse> _register(string name)
        {
            var form = new WWWForm();
            form.AddField("name", name);
            return GlobalContainer.WebApiRequester.Post<RegisterResponse>("Player/Register", form).Do(_onRegister);
        }
        void _onRegister(RegisterResponse response)
        {
            _onRegisterSubject.OnNext(true);
        }

        /// <summary>
        /// IDとかボタンとかUIパーツ表示。
        /// </summary>
        /// <param name="response">Response.</param>
        void _showUIParts(LoginResponse response) {
            txtID.text = response.PlayerID.ToString();
            txtAppVersion.text = Noroshi.AppConstant.APP_VERSION;
            uiContainer.SetActive(true);
            TweenS.Add(btnStartGame.gameObject, 1.335f, Vector2.one).From(Vector2.one * 1.5f);
            TweenA.Add(overlay, 0.02f, 1).Delay(1.325f).Then(() =>
            {
                TweenA.Add(overlay, 1.0f, 0);
            });
            TweenA.Add(splashImg, 0.3f, 0).Delay(0.2f).Then(() =>
            {
                splashImg.SetActive(false);
            });
            if(SoundController.Instance != null)
            {
                SoundController.Instance.LoadSetting();
                SoundController.Instance.StopBGM();
                SoundController.Instance.PlayBGM(SoundController.BGMKeys.CHARACTER_LIST);
            }
        }

        void _checkRegistered()
        {
            if(_isRegistered)
            {
                _startGame();
            } else {
                processing.SetActive(false);
                termsPanel.OpenTerms();
            }
        }

        void _startGame() {
            // チュートリアルバトル遷移を仮仕込み。
            if (!_isRegistered)
            {
                processing.SetActive(false);
                GlobalContainer.AssetBundleManager.Load(0, AssetBundleManager.UIType.Tips)
                .Subscribe(_ => {
                    SoundController.Instance.StopBGM();
                    Noroshi.BattleScene.Bridge.Transition.TransitToTutorialBattle();
                });
            } else {
                SceneManager.LoadScene(Constant.SCENE_MAIN);
            }
        }

        public void LoginWithNoroshiAccount()
        {
            var form = new WWWForm();
            form.AddField("deviceIdentifier", SystemInfo.deviceUniqueIdentifier);
            _postBackendServer("Account/LoginWithNoroshiAccount")(form)
                .SelectMany(response => _onAuthenticateWithBackendServer(response))
                .Subscribe().AddTo(this);
        }
    }
}
