﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.UI;


namespace Noroshi.Login {
    public class TermsPanel : MonoBehaviour {
        [SerializeField] BtnCommon btnAgreeTerms;
        [SerializeField] BtnCommon btnNotAgreeTerms;

        public Subject<bool> OnCloseTerms = new Subject<bool>();

        private void Start() {
            btnAgreeTerms.OnClickedBtn.Subscribe(_ => {
                TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                    gameObject.SetActive(false);
                    OnCloseTerms.OnNext(true);
                });
            });
            btnAgreeTerms.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnNotAgreeTerms.OnClickedBtn.Subscribe(_ => {
                TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                    gameObject.SetActive(false);
                });
            });
            btnNotAgreeTerms.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });
        }

        public void OpenTerms() {
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
        }
    }
}
