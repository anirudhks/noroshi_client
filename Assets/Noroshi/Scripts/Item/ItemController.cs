using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.WebApi.Response;

namespace Noroshi.UI {
    public class ItemController : MonoBehaviour {
        [SerializeField] ItemDetail itemDetail;
        [SerializeField] ItemList itemListPref;
        [SerializeField] GameObject itemListWrapper;
        [SerializeField] BtnCommon[] btnFilterList;

        public enum ItemType {
            Gear,
            Piece,
            Soul,
            Other
        }

        private List<ItemList> itemList = new List<ItemList>();
        private bool isLoad = false;


        private void Start() {
            GlobalContainer.SetFactory(() => new Repositories.RepositoryManager());
            var gcrm = GlobalContainer.RepositoryManager;
            gcrm.InventoryRepository.Get().Do(playerItemList => {
                SetGearIconList(playerItemList.Gears);
                SetGearPieceIconList(playerItemList.GearPieces);
                SetSoulIconList(playerItemList.Souls);
                SetDrugIconList(playerItemList.Drugs);
                SetGearEnchantMaterialIconList(playerItemList.GearEnchantMaterials);
                SetExchangeCashGiftIconList(playerItemList.ExchangeCashGifts);
                SetRaidTicketIconList(playerItemList.RaidTickets);
                SwitchTab(0);
                itemList[0].OnClickedBtn.OnNext(itemList[0].id);
                isLoad = true;
            }).Subscribe();

            foreach(var btn in btnFilterList) {
                btn.OnClickedBtn.Subscribe(id => {
                    SwitchTab(id);
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            BackButtonController.Instance.OnBack.Subscribe(_ => {
                UILoading.Instance.LoadScene(Constant.SCENE_MAIN);
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            StartCoroutine(OnLoading());
        }

        private IEnumerator OnLoading() {
            while(!isLoad) {
                yield return new WaitForEndOfFrame();
            }
            UILoading.Instance.HideLoading();
        }

        private void SetGearIconList(PlayerGear[] gearList) {
            foreach(var gear in gearList) {
                var itemData = GlobalContainer.MasterManager.ItemMaster.GetGear(gear.GearID);
                CreateItemIcon(itemData, gear.PossessionsCount, ItemType.Gear);
            }
        }

        private void SetGearPieceIconList(PlayerGearPiece[] gearPieceList) {
            foreach(var gearPiece in gearPieceList) {
                var itemData = GlobalContainer.MasterManager.ItemMaster.GetGearPiece(gearPiece.GearPieceID);
                CreateItemIcon(itemData, gearPiece.PossessionsCount, ItemType.Piece);
            }
        }

        private void SetSoulIconList(PlayerSoul[] soulList) {
            foreach(var soul in soulList) {
                var soulDataList = GlobalContainer.MasterManager.ItemMaster.GetSouls();
                foreach(var soulData in soulDataList.Where(s => s.ID == soul.SoulID)) {
                    CreateItemIcon(soulData, soul.PossessionsCount, ItemType.Soul);
                }
            }
        }

        private void SetDrugIconList(PlayerDrug[] drugList) {
            foreach(var drug in drugList) {
                var itemData = GlobalContainer.MasterManager.ItemMaster.GetDrug(drug.DrugID);
                CreateItemIcon(itemData, drug.PossessionsCount, ItemType.Other);
            }
        }

        private void SetGearEnchantMaterialIconList(PlayerGearEnchantMaterial[] gearEnchantMaterialList) {
            foreach(var gearEnchantMaterial in gearEnchantMaterialList) {
                var itemData = GlobalContainer.MasterManager.ItemMaster.GetGearEnchantMaterial(gearEnchantMaterial.GearEnchantMaterialID);
                CreateItemIcon(itemData, gearEnchantMaterial.PossessionsCount, ItemType.Other);
            }
        }

        private void SetExchangeCashGiftIconList(PlayerExchangeCashGift[] exchangeCashGiftList) {
            foreach(var exchangeCashGift in exchangeCashGiftList) {
                var itemData = GlobalContainer.MasterManager.ItemMaster.GetExchangeCashGift(exchangeCashGift.ExchangeCashGiftID);
                CreateItemIcon(itemData, exchangeCashGift.PossessionsCount, ItemType.Other);
            }
        }

        private void SetRaidTicketIconList(PlayerRaidTicket[] raidTicketList) {
            foreach(var raidTicket in raidTicketList) {
                var itemData = GlobalContainer.MasterManager.ItemMaster.GetRaidTicket(raidTicket.RaidTicketID);
                CreateItemIcon(itemData, raidTicket.PossessionsCount, ItemType.Other);
            }
        }

        private void CreateItemIcon(Core.WebApi.Response.Item data, uint possession, ItemType type) {
            if(possession < 1) {return;}
            var item = Instantiate(itemListPref);
            item.SetItemInfo(data, possession, type);
            item.transform.SetParent(itemListWrapper.transform);
            item.transform.localScale = Vector3.one;
            itemList.Add(item);
            item.OnClickedBtn.Subscribe(id => {
                foreach(var it in itemList) {
                    it.SetItemSelect(id == it.id);
                }
                itemDetail.SetItemDetailData(data, possession, type);
            });
            item.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        private void SwitchTab(int index) {
            foreach(var btn in btnFilterList) {
                btn.SetSelect(btn.id == index);
            }
            foreach(var item in itemList) {
                if(index == 0 || (index == 1 && item.itemType == ItemType.Gear)
                    || (index == 2 && item.itemType == ItemType.Piece)
                    || (index == 3 && item.itemType == ItemType.Soul)
                    || (index == 4 && item.itemType == ItemType.Other)
                ) {
                    item.gameObject.SetActive(true);
                } else {
                    item.gameObject.SetActive(false);
                }
            }
        }
    }
}
