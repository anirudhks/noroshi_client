﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.Core.WebApi.Response;

namespace Noroshi.UI {
    public class ItemList : BtnCommon {
        [SerializeField] Image imgItem;
        [SerializeField] Image imgFrame;
        [SerializeField] Text txtHaveNum;
        [SerializeField] GameObject iconSelect;
        [SerializeField] Sprite[] spriteFrameList;
        [SerializeField] Sprite[] spritePieceFrameList;

        public ItemController.ItemType itemType;

        public void SetItemInfo(Item data, uint possession, ItemController.ItemType type) {
            id = (int)data.ID;
            itemType = type;
            txtHaveNum.text = possession.ToString();
            imgItem.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(data.ID);
            if(type == ItemController.ItemType.Piece || type == ItemController.ItemType.Soul) {
                imgFrame.sprite = spritePieceFrameList[data.Rarity - 1];
            } else {
                imgFrame.sprite = spriteFrameList[data.Rarity - 1];
            }
        }

        public void SetItemSelect(bool isSelect) {
            iconSelect.SetActive(isSelect);
        }
    }
}
