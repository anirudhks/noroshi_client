﻿using System.Collections.Generic;
using UniRx;

namespace Noroshi.Character
{
    public struct CharacterVoiceHandler
    {
        public IObservable<bool> PlayUIVoice(uint characterId)
        {
            // キャラクターマスターに設定されている UI 用サウンド ID を抽選して鳴らすべきサウンドを決める。
            var characterMaster = GlobalContainer.MasterManager.CharacterMaster.Get(characterId);
            var soundIds = new List<uint>();
            if (characterMaster.UISoundID1.HasValue) soundIds.Add(characterMaster.UISoundID1.Value);
            if (characterMaster.UISoundID2.HasValue) soundIds.Add(characterMaster.UISoundID2.Value);
            if (soundIds.Count == 0) return Observable.Empty<bool>();
            var soundId = GlobalContainer.RandomGenerator.Lot(soundIds);
            // 再生。
            return GlobalContainer.SoundManager.Play(soundId);
        }
    }
}
