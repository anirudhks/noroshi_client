﻿using UniRx;
using Noroshi.Core.WebApi.Response.Character;

namespace Noroshi.Character
{
    public class WebApiRequester
    {
        /// <summary>
        /// 保有キャラクターを取得。
        /// </summary>
        public static IObservable<ListResponse> List()
        {
            return GlobalContainer.WebApiRequester.Get<ListResponse>("Character/List");
        }

        /// <summary>
        /// 装備（何を装備するかは設定を持っているので指定不要）。
        /// </summary>
        /// <param name="playerCharacterId">装備対象プレイヤーキャラクターID</param>
        /// <param name="gearNo">装備箇所番号</param>
        public static IObservable<EquipGearResponse> EquipGear(uint playerCharacterId, byte gearNo)
        {
            var request = new EquipGearRequest
            {
                PlayerCharacterID = playerCharacterId,
                GearNo = gearNo,
            };
            return GlobalContainer.WebApiRequester.Post<EquipGearRequest, EquipGearResponse>("Character/EquipGear", request);
        }
        class EquipGearRequest
        {
            public uint PlayerCharacterID { get; set; }
            public byte GearNo { get; set; }
        }

        /// <summary>
        /// 昇格。
        /// </summary>
        /// <param name="playerCharacterId">昇格対象プレイヤーキャラクターID</param>
        public static IObservable<UpPromotionLevelResponse> UpPromotionLevel(uint playerCharacterId)
        {
            var request = new UpPromotionLevelRequest
            {
                PlayerCharacterID = playerCharacterId,
            };
            return GlobalContainer.WebApiRequester.Post<UpPromotionLevelRequest, UpPromotionLevelResponse>("Character/UpPromotionLevel", request);
        }
        class UpPromotionLevelRequest
        {
            public uint PlayerCharacterID { get; set; }
        }

        /// <summary>
        /// アクションレベルを上げる。
        /// </summary>
        /// <param name="playerCharacterId">対象プレイヤーキャラクターID</param>
        /// <param name="actionNo">対象アクション番号</param>
        /// <param name="levelUpNum">レベルアップ回数</param>
        public static IObservable<UpActionLevelResponse> UpActionLevel(uint playerCharacterId, byte actionNo, ushort levelUpNum)
        {
            var request = new UpActionLevelRequest
            {
                PlayerCharacterID = playerCharacterId,
                ActionNo = actionNo,
                LevelUpNum = levelUpNum,
            };
            return GlobalContainer.WebApiRequester.Post<UpActionLevelRequest, UpActionLevelResponse>("Character/UpActionLevel", request);
        }
        class UpActionLevelRequest
        {
            public uint PlayerCharacterID { get; set; }
            public byte ActionNo { get; set; }
            public ushort LevelUpNum { get; set; }
        }

        /// <summary>
        /// キャラクターレベルを上げる。
        /// </summary>
        /// <param name="playerCharacterId">対象プレイヤーキャラクターID</param>
        /// <param name="drugId">使用する強化剤ID</param>
        /// <param name="drugNum">強化剤使用数</param>
        public static IObservable<EnhanceResponse> Enhance(uint characterId, uint drugId, ushort drugNum)
        {
            var request = new EnhanceRequest
            {
                PlayerCharacterID = characterId,
                DrugID = drugId,
                DrugNum = drugNum,
            };
            return GlobalContainer.WebApiRequester.Post<EnhanceRequest, EnhanceResponse>("Character/Enhance", request);
        }
        class EnhanceRequest
        {
            public uint PlayerCharacterID { get; set; }
            public uint DrugID { get; set; }
            public ushort DrugNum { get; set; }
        }
    }
}
