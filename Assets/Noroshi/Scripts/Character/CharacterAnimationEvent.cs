﻿using Noroshi.Core.Game.Character;

namespace Noroshi.Character
{
    public struct CharacterAnimationEvent
    {
        public CharacterAnimation CharacterAnimation { get; set; }
        public string AnimationNameSuffix { get; set; }
        public bool Loop { get; set; }
    }
}
