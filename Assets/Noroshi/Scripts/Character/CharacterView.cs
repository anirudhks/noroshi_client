﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UniLinq;
using UniRx;
using UnityEngine;
using Noroshi.Core.Game.Character;
using CharacterConstant = Noroshi.Core.Game.Character.Constant;
using Noroshi.MonoBehaviours;
using Flaggs.Extensions.Heam;

namespace Noroshi.Character
{
    /// キャラクターアニメーションを管理するクラス。
    /// 特定のシーンに依存しない汎用的な処理をここに記述し、固有処理は本クラスを継承した子クラスで行う。
    public class CharacterView : SpineView
    {
        const string ACTION_EVENT_NAME = "firing";
        const string ACTION_EVENT_NAME_FOR_MOVE = "move";
        const string EXIT_TIME_STOP_EVENT_NAME = "starting";

        [SerializeField] bool _debugSkinChangeOff = false;
        [Header("---子がStep付きのSkeletonAnimationを持つ場合セット")]
        [SerializeField] SkeletonAnimation[] _skinSettableSkeletonAnimations;

        /// アクション処理を実行する際に true が OnNext される Subject。
        Subject<bool> _onExecuteAction = new Subject<bool>();
        /// （アクティブアクション発動後の）停止した時間を再スタートする際に true が OnNext される Subject。
        Subject<bool> _onRestartTime = new Subject<bool>();
        /// サウンドを再生する際に再生 Sound ID が OnNext される Subject。
        Subject<uint> _onPlaySoundSubject = new Subject<uint>();
        /// サウンドをキャンセルする際に対象 Sound ID が OnNext される Subject。
        Subject<uint> _onCancelSoundSubject = new Subject<uint>();
        /// サウンド再生時処理 Observable を Subscribe() した際の Disposable。
        IDisposable _onPlayAnimationSoundDisposable = null;
        /// アニメーション連動サウンド。
        Dictionary<CharacterAnimation, Dictionary<String, Dictionary<byte, Core.WebApi.Response.Master.CharacterAnimationSound[]>>> _animationSoundMap;
        /// 現在のアニメーション連動サウンド。
        Core.WebApi.Response.Master.CharacterAnimationSound[] _currentAnimationSounds;
        /// アニメーション再生開始時間。
        float _animationStartTime = 0;
        /// アニメーション一時停止時間。
        float _animationPauseTime = 0;

        /// GameObject から呼ばれる Start()。
        protected new void Start()
        {
            base.Start();
            _rootSkeletonAnimation.state.Event += _onAnimationEvent;
        }
        /// アニメーションに仕込まれたトリガーフック処理。
        void _onAnimationEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
        {
            if (e.ToString() == ACTION_EVENT_NAME || e.ToString() == ACTION_EVENT_NAME_FOR_MOVE)
            {
                _onExecuteAction.OnNext(true);
            }
            else if (e.ToString() == EXIT_TIME_STOP_EVENT_NAME)
            {
                _onRestartTime.OnNext(true);
            }
        }

        /// アクション処理を実行する際に true が OnNext される Observable を取得。
        public IObservable<bool> GetOnExecuteActionObservable()
        {
            return _onExecuteAction.AsObservable();
        }
        /// （アクティブアクション発動後の）停止した時間を再スタートする際に true が OnNext される Observable を取得。
        public IObservable<bool> GetOnRestartTimeObservable()
        {
            return _onRestartTime.AsObservable();
        }
        /// サウンドを再生する際に再生 Sound ID が OnNext される Observable を取得。
        public IObservable<uint> GetOnPlaySoundObservable()
        {
            return _onPlaySoundSubject.AsObservable();
        }
        /// サウンドをキャンセルする際に対象 Sound ID が OnNext される Observable を取得。
        public IObservable<uint> GetOnCancelSoundObservable()
        {
            return _onCancelSoundSubject.AsObservable();
        }

        /// アクション関連アニメーション情報を取得する。
        public IEnumerable<AnimationInformation> GetActionAnimationInformations()
        {
            var actionAnimationNames = _getAnimationNames().Where(name => Constant.ACTION_ANIMATION_NAME_REGEX.IsMatch(name));
            return actionAnimationNames.Select(name => _getAnimationInformationWithValidEvent(name)).Where(a => a != null);
        }
        AnimationInformation _getAnimationInformationWithValidEvent(string animationName)
        {
            return _getAnimationInformation(animationName, e => e.ToString() == ACTION_EVENT_NAME || e.ToString() == ACTION_EVENT_NAME_FOR_MOVE);
        }

        /// アニメーションを再生する。
        public IObservable<bool> PlayAnimation(CharacterAnimation animation, bool loop, string suffix = null)
        {
            var animationName = CharacterConstant.ANIMATION_NAME_MAP[animation];
            // 特殊アニメーション名対応。
            if (_getAnimation(animationName) == null)
            {
                // suffix 指定ありの場合はそのまま。
                if (!string.IsNullOrEmpty(suffix))
                {
                    animationName = string.Format("{0}_{1}", animationName, suffix);
                }
                // suffix 指定がない場合は一つ選ぶ。
                else
                {
                    var baseAnimationName = animationName;
                    var regex = new Regex(string.Format(@"^{0}_", baseAnimationName));
                    var candidates = _getAnimationNames().Where(name => regex.IsMatch(name));
                    // TODO : ランダム抽選
                    animationName = candidates.First();
                    suffix = animationName.Substring(baseAnimationName.Length + 1);
                }
            }
            // アニメーション再生。
            var playObservable = _play(animationName, loop).Do(success =>
            {
                // 正常終了しなかった場合はサウンドを止める。
                if (!success) _stopPlaySound();
            });
            // アニメーション連動サウンド再生予約
            _currentAnimationSounds = _getAnimationSounds(animation, suffix);
            if (_currentAnimationSounds.Count() > 0)
            {
                _animationStartTime = Time.time;
                _animationPauseTime = 0;
                _onPlayAnimationSoundDisposable = _getAnimationSoundsObservable(0).Subscribe(characterAnimationSound =>
                {
                    _onPlaySoundSubject.OnNext(characterAnimationSound.SoundID);
                });
            }
            return playObservable;
        }
        void _stopPlaySound()
        {
            if (_onPlayAnimationSoundDisposable != null)
            {
                _onPlayAnimationSoundDisposable.Dispose();
                _onPlayAnimationSoundDisposable = null;
                _animationStartTime = 0;
                _animationPauseTime = 0;
            }
            for (var i = 0; i < _currentAnimationSounds.Length; i++)
            {
                _onCancelSoundSubject.OnNext(_currentAnimationSounds[i].SoundID);
            }
        }

        /// アニメーション連動サウンドを止める
        protected void _soundPauseOn()
        {
            if (_animationStartTime > 0)
            {
                _animationPauseTime = Time.time;
                if (_onPlayAnimationSoundDisposable != null)
                {
                    _onPlayAnimationSoundDisposable.Dispose();
                    _onPlayAnimationSoundDisposable = null;
                }
            }
        }

        /// アニメーション連動サウンドを途中から再生する
        protected void _soundPauseOff()
        {
            if (_animationStartTime > 0 && _animationPauseTime > 0)
            {
                var usedSecond = _animationPauseTime - _animationStartTime;
                if (usedSecond > 0)
                {
                    if (_currentAnimationSounds.Count() > 0)
                    {
                        _animationStartTime = _animationStartTime + (Time.time - _animationPauseTime);
                        _animationPauseTime = 0;
                        _onPlayAnimationSoundDisposable = _getAnimationSoundsObservable(usedSecond).Subscribe(characterAnimationSound =>
                        {
                            _onPlaySoundSubject.OnNext(characterAnimationSound.SoundID);
                        });
                    }
                }
            }
        }

        /// アニメーション連動サウンド Observable を取得。
        IObservable<Core.WebApi.Response.Master.CharacterAnimationSound> _getAnimationSoundsObservable(float usedSecond)
        {
            if (_currentAnimationSounds.Count() == 0) return Observable.Empty<Core.WebApi.Response.Master.CharacterAnimationSound>();
            return _currentAnimationSounds.ToArrayWithOptimization(
                animationSound => animationSound.Time - usedSecond >= 0,
                animationSound => GlobalContainer.TimeHandler.Timer(animationSound.Time - usedSecond).Select(_ => animationSound)
            ).Merge();
        }
        Core.WebApi.Response.Master.CharacterAnimationSound[] _getAnimationSounds(CharacterAnimation animation, String suffix)
        {
            if (suffix == null) suffix = "";
            if (_animationSoundMap == null || !_animationSoundMap.ContainsKey(animation) || !_animationSoundMap[animation].ContainsKey(suffix))
            {
                return new Core.WebApi.Response.Master.CharacterAnimationSound[0];
            }
            var map = _animationSoundMap[animation][suffix];
            return map.ElementAt(GlobalContainer.RandomGenerator.GenerateInt(map.Count)).Value;
        }
        /// アニメーション連動サウンドをセット。
        public void SetAnimationSounds(Core.WebApi.Response.Master.CharacterAnimationSound[] animationSounds)
        {
            _animationSoundMap = animationSounds
                .ToLookup(cas => cas.Animation)
                .ToDictionary(igrouping => igrouping.Key, igrouping => igrouping
                    .ToLookup(cas => cas.AnimationNameSuffix == null ? "" : cas.AnimationNameSuffix)
                    .ToDictionary(igrouping2 => igrouping2.Key, igrouping2 => igrouping2
                        .ToLookup(cas => cas.Group)
                        .ToDictionary(igrouping3 => igrouping3.Key, igrouping3 => igrouping3.ToArrayWithOptimization())));
        }

        /// スキンをセット。
        public void SetSkin(byte skinLevel)
        {
            if (_debugSkinChangeOff) return;
            SetSkinName("step" + skinLevel);
        }
        
        public void SetSkinName(string skinName)
        {
            if (_debugSkinChangeOff) return;
            _setRootSkin(skinName);
            // 呼ばれる段階ではまだ_skeletonAnimationsがnullなので別で設定した方を利用する
            foreach (var skeltonAnimation in _skinSettableSkeletonAnimations)
            {
                if (skeltonAnimation.Skeleton.Data.FindSkin(skinName) == null) skinName = "default";
                skeltonAnimation.Skeleton.SetSkin(skinName);
            }
        }
    }
}
