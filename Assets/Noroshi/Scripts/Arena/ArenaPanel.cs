﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using UniLinq;
using Noroshi.Core.Game.Character;

namespace Noroshi.UI {
    public class ArenaPanel : BtnCommon {
        [SerializeField] Text txtName;
        [SerializeField] Text txtLevel;
        [SerializeField] Text txtRate;
        [SerializeField] Text txtHP;
        [SerializeField] Text txtPower;
        [SerializeField] GameObject characterContainer;

        public Subject<bool> OnCreatePanel = new Subject<bool>();

        private GameObject character;
        private SkeletonAnimation skeletonAnimation;
        private byte skinLevel;

        public void SetInfo(Noroshi.Core.WebApi.Response.Players.PlayerArenaOtherResponse data) {
            var viewCharacter = data.DeckCharacters.Aggregate((a, b) => a.Level > b.Level ? a : b);
            var avatarID = viewCharacter.CharacterID;
            var masterData = GlobalContainer.MasterManager.CharacterMaster.Get(avatarID);
            var isDeca = new CharacterTagSet(masterData.TagFlags).IsDeca;

            if(character != null) {Destroy(character);}
            character = Instantiate<GameObject>(GlobalContainer.AssetBundleManager.LoadFromCharacterPrefab(avatarID , "UICharacter"));
            skeletonAnimation = character.GetComponent<SkeletonAnimation>();
            skinLevel = new Skin(viewCharacter.EvolutionLevel, isDeca).GetSkinLevel();

            txtName.text = data.OtherPlayerStatus.Name.ToString();
            txtRate.text = data.RatingPoint.ToString();
            txtHP.text = data.AllHP.ToString();
            txtPower.text = data.AllStrength.ToString();
            txtLevel.text = data.OtherPlayerStatus.Level.ToString();

            character.transform.SetParent(characterContainer.transform);
            character.transform.localScale = new Vector2(25, 25);
            character.transform.localPosition = Vector3.zero;

            var mesh = character.GetComponent<MeshRenderer>();
            mesh.sortingOrder = 1;
            ResetCharacter();

            OnCreatePanel.OnNext(true);
        }

        public void ResetCharacter() {
            if(skeletonAnimation == null) {return;}
            foreach(var atlas in skeletonAnimation.skeletonDataAsset.atlasAssets) {
                foreach(var m in atlas.materials) {
                    var shader = m.shader;
                    m.shader = null;
                    m.shader = shader;
                }
            }
            skeletonAnimation.skeletonDataAsset.Reset();
            skeletonAnimation.skeleton.SetSkin("step" + skinLevel);
            skeletonAnimation.skeleton.SetSlotsToSetupPose();
        }
    }
}
