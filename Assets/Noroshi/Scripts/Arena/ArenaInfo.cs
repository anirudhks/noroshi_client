﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using LitJson;
using UniRx;
using Noroshi.UI;
using Noroshi.Core.WebApi.Response;
using Noroshi.Core.WebApi.Response.Players;

namespace Noroshi.UI {
    public class ArenaInfo : MonoBehaviour {
        [SerializeField] CharacterStatusIcon[] characterList;
        [SerializeField] Text txtBattlePlayerName;
        [SerializeField] Text txtBattlePlayerLevel;
        [SerializeField] BtnCommon overlay;
        [SerializeField] BtnCommon btnFight;

        public Subject<int> OnSelectBattle = new Subject<int>();

        private bool isOpen = false;

        private void Start() {
            btnFight.OnClickedBtn.Subscribe(id => {
                OnSelectBattle.OnNext(id);
            });
            btnFight.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });

            overlay.OnClickedBtn.Subscribe(_ => {
                CloseArenaInfo();
            });
            overlay.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseArenaInfo();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private CharacterStatusIcon.CharacterData SetCharacterStatus(PlayerCharacter characterStatus) {
            var characterData = new CharacterStatusIcon.CharacterData();
            
            characterData.CharacterID = characterStatus.CharacterID;
            characterData.Level = characterStatus.Level;
            characterData.EvolutionLevel = characterStatus.EvolutionLevel;
            characterData.PromotionLevel = characterStatus.PromotionLevel;
            return characterData;
        }

        public void OpenArenaInfo(PlayerArenaOtherResponse data) {
            var charaLength = data.DeckCharacters.Length;

            btnFight.id = (int)data.OtherPlayerStatus.ID;
            txtBattlePlayerName.text = data.OtherPlayerStatus.Name;
            txtBattlePlayerLevel.text = data.OtherPlayerStatus.Level.ToString();
            for(int i = 0, l = characterList.Length; i < l; i++) {
                if(i < charaLength) {
                    var characterData = SetCharacterStatus(data.DeckCharacters[i]);
                    characterList[i].SetInfo(characterData);
                    characterList[i].gameObject.SetActive(true);
                } else {
                    characterList[i].gameObject.SetActive(false);
                }
            }
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }
        
        public void CloseArenaInfo() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }
    }
}
