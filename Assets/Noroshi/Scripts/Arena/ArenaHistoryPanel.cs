﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class ArenaHistoryPanel : MonoBehaviour {
        [SerializeField] GameObject historyWrapper;
        [SerializeField] ArenaHistoryPiece arenaHistoryPiecePref;
        [SerializeField] BtnCommon btnCloseHistory;

        private bool isOpen = false;

        private void Start() {
            btnCloseHistory.OnClickedBtn.Subscribe(_ => {
                CloseHistory();
            });
            btnCloseHistory.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseHistory();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        public void SetHistoryPanel(Noroshi.Core.WebApi.Response.Arena.PlayerArenaHistoryResponse[] dataList) {
            foreach(var data in dataList) {
                var history = Instantiate(arenaHistoryPiecePref);
                history.transform.SetParent(historyWrapper.transform);
                history.transform.localScale = Vector3.one;
                history.SetHistory(data);
            }
        }

        public void OpenHistory() {
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }

        public void CloseHistory() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }
    }
}
