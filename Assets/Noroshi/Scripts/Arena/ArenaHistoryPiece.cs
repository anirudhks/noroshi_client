﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Noroshi.UI {
    public class ArenaHistoryPiece : MonoBehaviour {
        [SerializeField] Image imgPlayer;
        [SerializeField] Text txtPlayerName;
        [SerializeField] Text txtTime;
        [SerializeField] Text txtWinRate;
        [SerializeField] Text txtLoseRate;
        [SerializeField] GameObject winText;
        [SerializeField] GameObject loseText;

        public void SetHistory(Noroshi.Core.WebApi.Response.Arena.PlayerArenaHistoryResponse data) {
            var t = Constant.UNIX_EPOCH.AddSeconds(data.BattledAt).ToLocalTime();
            var month = t.Month < 10 ? "0" + t.Month : t.Month.ToString();
            var day = t.Day < 10 ? "0" + t.Day : t.Day.ToString();
            var hour = t.Hour < 10 ? "0" + t.Hour : t.Hour.ToString();
            var minute = t.Minute < 10 ? "0" + t.Minute : t.Minute.ToString();
            var avatarID = data.OtherPlayerStatus.AvatarCharacterID;
            txtWinRate.gameObject.SetActive(false);
            winText.SetActive(false);
            txtLoseRate.gameObject.SetActive(false);
            loseText.SetActive(false);
            txtPlayerName.text = data.OtherPlayerStatus.Name;
            if(data.VictoryOrDefeat == Noroshi.Core.Game.Battle.VictoryOrDefeat.Win) {
                txtWinRate.text = "+" + data.RattingPoint;
                txtWinRate.gameObject.SetActive(true);
                winText.SetActive(true);
            } else if(data.VictoryOrDefeat == Noroshi.Core.Game.Battle.VictoryOrDefeat.Loss) {
                txtLoseRate.text = "-" + data.RattingPoint;
                txtLoseRate.gameObject.SetActive(true);
                loseText.SetActive(true);
            }
            if(imgPlayer != null) {
                if(avatarID < 1) {
                    avatarID = Noroshi.Core.Game.Character.Constant.FIRST_CHARACTER_ID;
                }
                imgPlayer.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite>(avatarID, "thumb_1");
            }
            if(txtTime != null) {
                txtTime.text = month + "/" + day + " " + hour + ":" + minute;
            }
        }
    }
}
