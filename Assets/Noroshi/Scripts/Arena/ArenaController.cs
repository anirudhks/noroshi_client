using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.WebApi.Response.Arena;

namespace Noroshi.UI {
    public class ArenaController : MonoBehaviour {
        [SerializeField] ArenaBattleDeck arenaBattleDeck;
        [SerializeField] ArenaPanel[] arenaPanelList;
        [SerializeField] Text txtHeroRate;
        [SerializeField] Text txtRank;
        [SerializeField] Text txtRemainNum;
        [SerializeField] Text txtMaxNum;
        [SerializeField] BtnCommon btnChangeBattlePlayer;
        [SerializeField] BtnCommon btnToShop;
        [SerializeField] AlertModal shopNotOpenAlert;
        [SerializeField] GameObject processing;
        [SerializeField] GameObject coolTimePanel;
        [SerializeField] Text txtCoolTime;
        [SerializeField] BtnCommon btnRecoverCoolTime;
        [SerializeField] CoolTimeRecoverModal coolTimeRecoverModal;
        [SerializeField] ArenaPlayCountResetModal arenaPlayCountResetModal;
        [SerializeField] ArenaInfo arenaInfo;
        [SerializeField] BtnCommon btnMoreHistroy;
        [SerializeField] ArenaHistoryPiece[] arenaHistoryPieceList;
        [SerializeField] ArenaHistoryPanel arenaHistoryPanel;

        private bool isLoad = false;
        private bool isLoadOtherPlayer = false;
        private uint playerID;
        private IDisposable timer;
        private double currentTime;
        private uint coolTimeAt;
        private uint coolTimeRecoverNum;
        private uint resetNum;
        private bool isLimitPlayCount = false;
        private Noroshi.Core.WebApi.Response.Players.PlayerArenaOtherResponse[] battlePlayerDataList;

        private void Start() {
            if(SoundController.Instance != null) {
                SoundController.Instance.PlayBGM(SoundController.BGMKeys.TENSION);
            }
            var defaultCharacterIdList = BattleCharacterSelect.Instance.GetDefaultCharacter(SaveKeys.DefaultArenaBattleCharacter);

            foreach(var panel in arenaPanelList) {
                panel.OnClickedBtn.Subscribe(index => {
                    if(isLimitPlayCount) {
                        arenaPlayCountResetModal.Open((ushort)resetNum);
                    } else {
                        arenaInfo.OpenArenaInfo(battlePlayerDataList[index]);
                    }
                });
                panel.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnMoreHistroy.OnClickedBtn.Subscribe(_ => {
                arenaHistoryPanel.OpenHistory();
            });
            btnMoreHistroy.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            arenaInfo.OnSelectBattle.Subscribe(id => {
                playerID = (uint)id;
                BattleCharacterSelect.Instance.OpenPanel(false, defaultCharacterIdList);
            });

            btnChangeBattlePlayer.OnClickedBtn.Subscribe(_ => {
                processing.SetActive(true);
                LoadBattlePlayerInfo();
            });
            btnChangeBattlePlayer.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnRecoverCoolTime.OnClickedBtn.Subscribe(_ => {
                coolTimeRecoverModal.Open((ushort)coolTimeRecoverNum, txtCoolTime.text);
            });
            btnRecoverCoolTime.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnToShop.OnClickedBtn.Subscribe(_ => {
                if(PlayerInfo.Instance.GetTutorialStep()
                    >= Noroshi.Core.Game.Player.TutorialStep.ClearStoryStage_1_3_2) {
                    UILoading.Instance.LoadScene(Constant.SCENE_SHOP);
                } else {
                    shopNotOpenAlert.OnOpen();
                }
            });
            btnToShop.OnPlaySE.Subscribe(_ => {
                if(PlayerInfo.Instance.GetTutorialStep()
                    >= Noroshi.Core.Game.Player.TutorialStep.ClearStoryStage_1_3_2) {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
                } else {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                }
            });

            coolTimeRecoverModal.OnRecoverCoolTime.Subscribe(_ => {
                coolTimePanel.SetActive(false);
            });

            arenaPlayCountResetModal.OnResetPlayCount.Subscribe(_ => {
                txtRemainNum.text = txtMaxNum.text;
                isLimitPlayCount = false;
            });

            BattleCharacterSelect.Instance.OnStartBattle.Subscribe(data => {
                BattleCharacterSelect.Instance.SaveDefaultCharacter(SaveKeys.DefaultArenaBattleCharacter, data.playerCharacterIDs);
                BattleScene.Bridge.Transition.TransitToArenaBattle(playerID, data.playerCharacterIDs, data.mercenaryID);
            }).AddTo(this);

            BattleCharacterSelect.Instance.OnClosePanel.Subscribe(data => {
                Observable.TimerFrame(1).Subscribe(_ => {
                    foreach(var panel in arenaPanelList) {
                        panel.ResetCharacter();
                    }
                });
            }).AddTo(this);

            if(UILoading.Instance.GetPreviousHistory() != Constant.SCENE_BATTLE) {
                BattleCharacterSelect.Instance.ReloadCharacterList();
            }

            BackButtonController.Instance.OnBack.Subscribe(_ => {
                UILoading.Instance.LoadScene(Constant.SCENE_MAIN);
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            StartCoroutine(OnLoading());
        }

        private IEnumerator OnLoading() {
            while(!BattleCharacterSelect.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            LoadMyInfo();
            LoadBattlePlayerInfo();
            while(!isLoad || !isLoadOtherPlayer) {
                yield return new WaitForEndOfFrame();
            }
            UILoading.Instance.HideLoading();
        }

        private void LoadMyInfo() {
            var gcrm = GlobalContainer.RepositoryManager;
            gcrm.PlayerArenaRepository.Get().Do(arenaData => {
                gcrm.PlayerArenaRepository.GetHistory().Do(historyData => {
                    SetMyInfo(arenaData);
                    SetHistory(historyData.PlayerArenaHistoryResponse);
                    arenaBattleDeck.SetBattleDeck(arenaData);
                    isLoad = true;
                }).Subscribe();
            }).Subscribe();
        }

        private void LoadBattlePlayerInfo() {
            GlobalContainer.RepositoryManager.PlayerArenaRepository.GetRandomPlayers().Do(dataList => {
                battlePlayerDataList = dataList;
                for(int i = 0, l = arenaPanelList.Length; i < l; i++) {
                    if(i < dataList.Length) {
                        arenaPanelList[i].SetInfo(dataList[i]);
                        arenaPanelList[i].gameObject.SetActive(true);
                    } else {
                        arenaPanelList[i].gameObject.SetActive(false);
                    }
                }
                processing.SetActive(false);
                isLoadOtherPlayer = true;
            }).Subscribe();
        }

        private void SetMyInfo(PlayerArena data) {
            txtRank.text = data.Rank.ToString();
            txtHeroRate.text = data.RatingPoint.ToString();
            txtMaxNum.text = data.PlayMaxCount.ToString();
            txtRemainNum.text = (data.PlayMaxCount - data.PlayCount).ToString();
            coolTimeRecoverNum = data.CoolTimeResetNum;
            resetNum = data.PlayResetNum;
            currentTime = GlobalContainer.TimeHandler.UnixTime;
            if(data.PlayCount >= data.PlayMaxCount) {isLimitPlayCount = true;}
            if(data.CoolTime - currentTime > 0) {
                coolTimeAt = (uint)data.CoolTime;
                DrawCoolTime();
                StartCoolDownTimer();
                coolTimePanel.SetActive(true);
            }
        }

        private void SetHistory(PlayerArenaHistoryResponse[] dataList) {
            if(dataList == null) {return;}
            if(dataList.Length > 0) {
                arenaHistoryPieceList[0].SetHistory(dataList[0]);
                arenaHistoryPieceList[0].gameObject.SetActive(true);
            }
            if(dataList.Length > 1) {
                arenaHistoryPieceList[1].SetHistory(dataList[1]);
                arenaHistoryPieceList[1].gameObject.SetActive(true);
            }
            arenaHistoryPanel.SetHistoryPanel(dataList);
        }

        private void StartCoolDownTimer() {
            timer = Observable.Interval(TimeSpan.FromSeconds(1)).Subscribe(l => {
                currentTime = GlobalContainer.TimeHandler.UnixTime;
                if(coolTimeAt - currentTime < 0) {
                    timer.Dispose();
                    coolTimePanel.SetActive(false);
                    return;
                }
                DrawCoolTime();
            }).AddTo(this);
        }

        private void DrawCoolTime() {
            var m = (int)((coolTimeAt - currentTime) / 60);
            var s = (int)((coolTimeAt - currentTime) % 60);
            var minute = m < 10 ? "0" + m : m.ToString();
            var second = s < 10 ? "0" + s : s.ToString();
            txtCoolTime.text = minute + ":" + second;
        }
    }
}
