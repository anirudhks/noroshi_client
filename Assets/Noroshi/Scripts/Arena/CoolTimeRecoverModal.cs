﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class CoolTimeRecoverModal : MonoBehaviour {
        [SerializeField] Text txtNeedGem;
        [SerializeField] Text txtCurrentCoolTime;
        [SerializeField] Text txtNewCoolTime;
        [SerializeField] Text txtRecoverTimes;
        [SerializeField] GameObject iconGem;
        [SerializeField] GameObject enoughWrapper;
        [SerializeField] GameObject notEnoughWrapper;
        [SerializeField] BtnCommon btnOK;
        [SerializeField] BtnCommon btnCancel;
        [SerializeField] BtnCommon btnClose;

        public Subject<bool> OnRecoverCoolTime = new Subject<bool>();

        private Noroshi.Core.Game.Player.RepeatablePaymentCalculator repeatablePaymentCalculator;

        private void Start() {
            btnOK.OnClickedBtn.Subscribe(_ => {
                RecoverCoolTime();
            });
            btnOK.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.STATUS_UP);
            });

            btnCancel.OnClickedBtn.Subscribe(_ => {
                Close();
            });
            btnCancel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnClose.OnClickedBtn.Subscribe(_ => {
                Close();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });
        }

        private void RecoverCoolTime() {
            GlobalContainer.RepositoryManager.PlayerArenaRepository.ResetCooltime().Do(data => {
                PlayerInfo.Instance.UpdatePlayerStatus(data.PlayerStatus);
                OnRecoverCoolTime.OnNext(true);
                Close();
            }).Subscribe();
        }

        private void Close() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                gameObject.SetActive(false);
            });
        }

        public void Open(ushort recoverNum, string currentCoolTime) {
            if(repeatablePaymentCalculator == null) {
                repeatablePaymentCalculator = new Noroshi.Core.Game.Player.RepeatablePaymentCalculator(
                    Noroshi.Core.Game.Arena.Constant.GEM_RESET_COOLTIME_NUM
                );
            }

            var needGem = repeatablePaymentCalculator.GetPaymentNum(recoverNum);
            if(needGem > PlayerInfo.Instance.GetPlayerStatus().Gem) {
                txtNeedGem.text = needGem.ToString();
                enoughWrapper.SetActive(false);
                notEnoughWrapper.SetActive(true);
            } else {
                txtNeedGem.text = needGem.ToString();
                txtCurrentCoolTime.text = currentCoolTime;
                txtRecoverTimes.text = (recoverNum + 1).ToString();
                enoughWrapper.SetActive(true);
                notEnoughWrapper.SetActive(false);
            }

            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
        }
    }
}
