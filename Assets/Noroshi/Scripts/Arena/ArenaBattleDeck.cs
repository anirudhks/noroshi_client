using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using LitJson;
using Noroshi.Game;

namespace Noroshi.UI {
    public class ArenaBattleDeck : MonoBehaviour {
        [SerializeField] CharacterStatusIcon[] battleCharacterList;
        [SerializeField] Text txtTotalHP;
        [SerializeField] Text txtTotalPower;
        [SerializeField] BtnCommon btnBattleCharacterEdit;

        private uint[] battleCharacterIdList;

        private void Start() {
            btnBattleCharacterEdit.OnClickedBtn.Subscribe(_ => {
                BattleCharacterSelect.Instance.OpenPanel(true, battleCharacterIdList);
            });
            btnBattleCharacterEdit.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            BattleCharacterSelect.Instance.OnClosePanel.Subscribe(list => {
                if(list != null) {
                    var pcIdList = BattleCharacterSelect.Instance.GetPlayerCharacterId(list);
                    txtTotalHP.text = "";
                    txtTotalPower.text = "";
                    SetBattleCharacterIcon(list);
                    GlobalContainer.RepositoryManager.PlayerArenaRepository.ChangeDeck(pcIdList).Do(data => {
                        txtTotalHP.text = data.AllHP.ToString();
                        txtTotalPower.text = data.AllStrength.ToString();
                    }).Subscribe();
                }
            }).AddTo(this);
        }

        private CharacterStatusIcon.CharacterData SetCharacterStatus(CharacterStatus characterStatus) {
            var characterData = new CharacterStatusIcon.CharacterData();

            characterData.CharacterID = characterStatus.CharacterID;
            characterData.Level = characterStatus.Level;
            characterData.EvolutionLevel = characterStatus.EvolutionLevel;
            characterData.PromotionLevel = characterStatus.PromotionLevel;
            characterData.IsDeca = characterStatus.TagSet.IsDeca;
            return characterData;
        }

        private void SetBattleCharacterIcon(uint[] idList) {
            var statusList = BattleCharacterSelect.Instance.GetCharacterStatusList();
            battleCharacterIdList = idList;
            for(int i = 0, l = battleCharacterList.Length; i < l; i++) {
                if(i < idList.Length) {
                    foreach(var status in statusList) {
                        if(status.CharacterID == idList[i]) {
                            var characterData = SetCharacterStatus(status);
                            battleCharacterList[i].SetInfo(characterData);
                            battleCharacterList[i].gameObject.SetActive(true);
                            break;
                        }
                    }
                } else {
                    battleCharacterList[i].gameObject.SetActive(false);
                }
            }
        }

        public void SetBattleDeck(Noroshi.Core.WebApi.Response.Arena.PlayerArena data) {
            txtTotalHP.text = data.AllHP.ToString();
            txtTotalPower.text = data.AllStrength.ToString();
            if(data.DeckCharacters == null) {return;}

            List<uint> idList = new List<uint>();
            foreach(var chara in data.DeckCharacters) {
                if(chara != null) {
                    idList.Add(chara.CharacterID);
                }
            }
            SetBattleCharacterIcon(idList.ToArray());
        }
    }
}
