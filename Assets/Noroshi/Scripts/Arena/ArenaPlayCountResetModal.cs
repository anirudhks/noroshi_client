﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class ArenaPlayCountResetModal : MonoBehaviour {
        [SerializeField] Text txtNeedGem;
        [SerializeField] Text txtRecoverTimes;
        [SerializeField] GameObject iconGem;
        [SerializeField] GameObject enoughWrapper;
        [SerializeField] GameObject notEnoughWrapper;
        [SerializeField] BtnCommon btnOK;
        [SerializeField] BtnCommon btnCancel;
        [SerializeField] BtnCommon btnClose;

        public Subject<bool> OnResetPlayCount = new Subject<bool>();

        private Noroshi.Core.Game.Player.RepeatablePaymentCalculator repeatablePaymentCalculator;

        private void Start() {
            btnOK.OnClickedBtn.Subscribe(_ => {
                ResetPlayCount();
            });
            btnOK.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.STATUS_UP);
            });

            btnCancel.OnClickedBtn.Subscribe(_ => {
                Close();
            });
            btnCancel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnClose.OnClickedBtn.Subscribe(_ => {
                Close();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });
        }

        private void ResetPlayCount() {
            GlobalContainer.RepositoryManager.PlayerArenaRepository.ResetPlayNum().Do(data => {
                PlayerInfo.Instance.UpdatePlayerStatus(data.PlayerStatus);
                OnResetPlayCount.OnNext(true);
                Close();
            }).Subscribe();
        }

        private void Close() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                gameObject.SetActive(false);
            });
        }

        public void Open(ushort resetNum) {
            if(repeatablePaymentCalculator == null) {
                repeatablePaymentCalculator = new Noroshi.Core.Game.Player.RepeatablePaymentCalculator(
                    Noroshi.Core.Game.Arena.Constant.GEM_RESET_PLAY_NUM
                );
            }

            var needGem = repeatablePaymentCalculator.GetPaymentNum(resetNum);
            if(needGem > PlayerInfo.Instance.GetPlayerStatus().Gem) {
                txtNeedGem.text = needGem.ToString();
                enoughWrapper.SetActive(false);
                notEnoughWrapper.SetActive(true);
            } else {
                txtNeedGem.text = needGem.ToString();
                txtRecoverTimes.text = (resetNum + 1).ToString();
                enoughWrapper.SetActive(true);
                notEnoughWrapper.SetActive(false);
            }

            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
        }
    }
}
