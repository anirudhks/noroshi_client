﻿using UnityEngine;
using System.Collections;

namespace Noroshi.UI {
    public class BtnExpeditionStage : BtnCommon {
        [SerializeField] GameObject unlockImg;
        [SerializeField] GameObject currentImg;
        [SerializeField] GameObject clearedImg;
        [SerializeField] GameObject TextNumber;
        
        public void SetStageState(int state) {
            unlockImg.SetActive(false);
            currentImg.SetActive(false);
            clearedImg.SetActive(false);
            switch (state) {
                case 1:
                    unlockImg.SetActive(true);
                    TweenA.Add(TextNumber, 0.01f, 0.5f);
                    break;
                case 2:
                    currentImg.SetActive(true);
                    TweenA.Add(TextNumber, 0.01f, 1);
                    break;
                case 3:
                    clearedImg.SetActive(true);
                    TweenA.Add(TextNumber, 0.01f, 1);
                    break;
                default : break;
            }
            gameObject.SetActive(true);
        }
        
    }
}
