﻿using UnityEngine;
using System.Collections;

namespace Noroshi.UI {
    public class BtnExpeditionTreasure : BtnCommon {
        [SerializeField] GameObject treasureImg;
        [SerializeField] GameObject currentTreasureImg;
        [SerializeField] GameObject gainedImg;

        public void SetTreasureState(int state) {
            treasureImg.SetActive(false);
            currentTreasureImg.SetActive(false);
            gainedImg.SetActive(false);
            switch (state) {
                case 1: treasureImg.SetActive(true); break;
                case 2: currentTreasureImg.SetActive(true); break;
                case 3: gainedImg.SetActive(true); break;
                default : break;
            }
            gameObject.SetActive(true);
        }
    }
}
