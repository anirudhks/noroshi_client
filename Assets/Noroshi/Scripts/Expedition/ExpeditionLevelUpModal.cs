﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class ExpeditionLevelUpModal : MonoBehaviour {
        [SerializeField] BtnCommon btnOverlay;
        [SerializeField] Text txtPrevLevel;
        [SerializeField] Text txtNewLevel;

        public Subject<bool> OnClose = new Subject<bool>();


        private void Start() {
            btnOverlay.OnClickedBtn.Subscribe(_ => {
                CloseModal();
            });
            btnOverlay.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        public void OpenModal(byte level) {
            txtPrevLevel.text = level.ToString();
            txtNewLevel.text = (level + 1).ToString();
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
        }

        public void CloseModal() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                gameObject.SetActive(false);
                OnClose.OnNext(true);
            });
        }
    }
}
