using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniLinq;
using Noroshi.UI;
using Noroshi.Core.Game.Battle;
using Noroshi.Core.WebApi.Response.Battle;

namespace Noroshi.UI {
    public class ExpeditionController : MonoBehaviour {
        [SerializeField] Text txtExpeditionLevel;
        [SerializeField] Text txtRemainChance;
        [SerializeField] ScrollRect scroller;
        [SerializeField] RectTransform contentWrapper;
        [SerializeField] GameObject treasureListContainer;
        [SerializeField] BtnExpeditionStage[] btnStageList;
        [SerializeField] BtnExpeditionTreasure[] btnTreasureList;
        [SerializeField] GameObject[] roadList;
        [SerializeField] ExpeditionInfo expeditionInfo;
        [SerializeField] GetRewardModal getRewardModal;
        [SerializeField] BtnCommon[] btnSelectLevelList;
        [SerializeField] GameObject restartModal;
        [SerializeField] BtnCommon btnDecideRestart;
        [SerializeField] BtnCommon btnCloseRestart;
        [SerializeField] BtnCommon btnRestart;
        [SerializeField] BtnCommon btnToShop;
        [SerializeField] AlertModal shopNotOpenAlert;
        [SerializeField] GameObject nextArrow;
        [SerializeField] GameObject tapArrow;
        [SerializeField] GameObject bgFarWrapper;
        [SerializeField] ExpeditionLevelUpModal expeditionLevelUpModal;
        [SerializeField] AlertModal alertRestart;
        [SerializeField] GameObject processing;

        private bool isLoad = false;
        private bool canAcquireReward = false;
        private byte expeditionLevel;
        private int currentIndex = 0;
        private int resetNum;
        private uint stageID;
        private uint[] defaultCharacterIdList;
        private InitialCondition.PlayerCharacterCondition[] continuingCharacterList;
        private SkeletonAnimation character;
        private Noroshi.Core.WebApi.Response.Expedition.PlayerExpeditionStage[] stageDataList;
        private bool isRestartModalOpen = false;

        private void Start() {
            if(SoundController.Instance != null) {
                SoundController.Instance.PlayBGM(SoundController.BGMKeys.MAIN);
            }

            foreach(var btn in btnStageList) {
                btn.OnClickedBtn.Subscribe(SetStageInfo);
                btn.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }
            foreach(var btn in btnTreasureList) {
                btn.OnClickedBtn.Subscribe(GetTreasure);
            }

            expeditionInfo.OnSelectBattle.Subscribe(id => {
                stageID = (uint)id;
                BattleCharacterSelect.Instance.OpenPanel(false, defaultCharacterIdList);
                TweenNull.Add(gameObject, 0.3f).Then(() => {
                    expeditionInfo.gameObject.SetActive(false);
                });
            });

            btnRestart.OnClickedBtn.Subscribe(_ => {
                if(resetNum > 0) {
                    alertRestart.OnOpen();
                } else {
                    isRestartModalOpen = true;
                    restartModal.SetActive(true);
                    TweenA.Add(restartModal, 0.1f, 1).From(0);
                    BackButtonController.Instance.IsModalOpen(true);
                }
            });
            btnRestart.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnDecideRestart.OnClickedBtn.Subscribe(_ => {
                processing.SetActive(true);
                Noroshi.Expedition.WebApiRequester.Reset().Do(__ => {
                    Noroshi.Expedition.WebApiRequester.Start().Do(resetData => {
                        TweenA.Add(restartModal, 0.1f, 0).Then(() => {
                            restartModal.SetActive(false);
                        });
                        StartExpedition(resetData.PlayerExpedition);
                        character.gameObject.SetActive(true);
                        nextArrow.SetActive(true);
                        processing.SetActive(false);
                    }).Subscribe();
                }).Subscribe();
            });
            btnDecideRestart.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });

            btnCloseRestart.OnClickedBtn.Subscribe(_ => {
                CloseRestartModal();
            });
            btnCloseRestart.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnToShop.OnClickedBtn.Subscribe(_ => {
                if(PlayerInfo.Instance.GetTutorialStep()
                    >= Noroshi.Core.Game.Player.TutorialStep.ClearStoryStage_1_3_2) {
                    UILoading.Instance.LoadScene(Constant.SCENE_SHOP);
                } else {
                    shopNotOpenAlert.OnOpen();
                }
            });
            btnToShop.OnPlaySE.Subscribe(_ => {
                if(PlayerInfo.Instance.GetTutorialStep()
                    >= Noroshi.Core.Game.Player.TutorialStep.ClearStoryStage_1_3_2) {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
                } else {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                }
            });

            BattleCharacterSelect.Instance.OnStartBattle.Subscribe(data => {
                BattleCharacterSelect.Instance.SaveDefaultCharacter(SaveKeys.DefaultExpeditionBattleCharacter, data.playerCharacterIDs);
                BattleScene.Bridge.Transition.TransitToExpeditionBattle(stageID, data.playerCharacterIDs, data.mercenaryID);
            }).AddTo(this);

            BattleCharacterSelect.Instance.OnClosePanel.Subscribe(idList => {
                expeditionInfo.gameObject.SetActive(true);
                ResetCharacter();
            }).AddTo(this);

            if(UILoading.Instance.GetPreviousHistory() != Constant.SCENE_BATTLE) {
                BattleCharacterSelect.Instance.ReloadCharacterList();
            }

            BackButtonController.Instance.OnBack.Subscribe(_ => {
                UILoading.Instance.LoadScene(Constant.SCENE_MAIN);
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isRestartModalOpen) {return;}
                CloseRestartModal();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            StartCoroutine(OnLoading());
        }
        
        private IEnumerator OnLoading() {
            while(!BattleCharacterSelect.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            Init();
            while(!isLoad) {
                yield return new WaitForEndOfFrame();
            }
            UILoading.Instance.HideLoading();
        }

        private void Init() {
            defaultCharacterIdList = BattleCharacterSelect.Instance.GetDefaultCharacter(SaveKeys.DefaultExpeditionBattleCharacter);
            var defaultPlayerCharacterIdList = BattleCharacterSelect.Instance.GetPlayerCharacterId(defaultCharacterIdList).ToList();
            Noroshi.Expedition.WebApiRequester.Get().Do(data => {
                if(data.PlayerExpedition.IsActive) {
                    stageDataList = data.PlayerExpedition.Stages;
                    resetNum = data.PlayerExpedition.ResetNum;
                    expeditionLevel = data.CurrentExpedition.Level;
                    txtExpeditionLevel.text = data.CurrentExpedition.Level.ToString();
                    txtRemainChance.text = (data.PlayerExpedition.MaxResetNum - data.PlayerExpedition.ResetNum).ToString();
                    continuingCharacterList = data.PlayerExpedition.PlayerCharacterConditions;
                    SetStage(data.PlayerExpedition.ClearStep.Value, data.PlayerExpedition.CanReceiveReward);
                    for(int i = defaultPlayerCharacterIdList.Count - 1; i > -1; i--) {
                        foreach(var character in data.PlayerExpedition.PlayerCharacterConditions) {
                            if(defaultPlayerCharacterIdList[i] == character.PlayerCharacterID && character.HP <= 0) {
                                defaultPlayerCharacterIdList.Remove(character.PlayerCharacterID);
                                break;
                            }
                        }
                    }
                    BattleCharacterSelect.Instance.SetContinuingStatus(continuingCharacterList);
                    defaultCharacterIdList = BattleCharacterSelect.Instance.GetCharacterId(defaultPlayerCharacterIdList.ToArray());
                    isLoad = true;
                } else {
                    processing.SetActive(true);
                    Noroshi.Expedition.WebApiRequester.Start().Do(startData => {
                        StartExpedition(startData.PlayerExpedition);
                        processing.SetActive(false);
                        isLoad = true;
                    }).Subscribe();
                }
            }).Subscribe();
        }

        private void CloseRestartModal() {
            TweenA.Add(restartModal, 0.1f, 0).Then(() => {
                isRestartModalOpen = false;
                restartModal.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }

        private void StartExpedition(Noroshi.Core.WebApi.Response.Expedition.PlayerExpedition data) {
            var step = data.ClearStep == null ? 0 : (uint)data.ClearStep;
            stageDataList = data.Stages;
            resetNum = data.ResetNum;
            txtRemainChance.text = (data.MaxResetNum - data.ResetNum).ToString();
            defaultCharacterIdList = new uint[]{};
            continuingCharacterList = data.PlayerCharacterConditions;
            SetStage(step, data.CanReceiveReward);
            BattleCharacterSelect.Instance.SaveDefaultCharacter(SaveKeys.DefaultExpeditionBattleCharacter, new uint[]{});
            BattleCharacterSelect.Instance.SetContinuingStatus(continuingCharacterList);
        }

        private void SetStage(uint clearStep, bool isAcquire) {
            int stageDataLength = stageDataList.Length;
            var wrapperSizeDelta = contentWrapper.sizeDelta;
            currentIndex = (int)clearStep;
            canAcquireReward = isAcquire;
            if(character == null) {
                var avatarID = PlayerInfo.Instance.GetPlayerStatus().AvatarCharacterID;
                if(avatarID < 1) {
                    avatarID = Noroshi.Core.Game.Character.Constant.FIRST_CHARACTER_ID;
                }
                var chara = Instantiate<GameObject>(GlobalContainer.AssetBundleManager.LoadFromCharacterPrefab(avatarID, "UICharacter"));
                character = chara.GetComponent<SkeletonAnimation>();
                character.transform.SetParent(contentWrapper.transform);
                character.transform.localScale = new Vector3(-20, 20, 20);
                ResetCharacter();
            }
            character.GetComponent<MeshRenderer>().sortingOrder = canAcquireReward ? 2 : 4;

            for(int i = 0, l = btnStageList.Length; i < l; i++) {
                if(i > stageDataLength - 1) {
                    btnStageList[i].gameObject.SetActive(false);
                } else if(i > currentIndex) {
                    btnStageList[i].SetStageState(1);
                } else if(i == currentIndex && !canAcquireReward) {
                    btnStageList[i].SetStageState(2);
                } else {
                    btnStageList[i].SetStageState(3);
                }
            }
            
            for(int i = 0, l = btnTreasureList.Length; i < l; i++) {
                if(i > stageDataLength - 1) {
                    btnTreasureList[i].gameObject.SetActive(false);
                } else if(i == currentIndex && canAcquireReward) {
                    btnTreasureList[i].SetTreasureState(2);
                } else if(i >= currentIndex) {
                    btnTreasureList[i].SetTreasureState(1);
                } else {
                    btnTreasureList[i].SetTreasureState(3);
                }
            }

            for(int i = 0, l = roadList.Length; i < l; i++) {
                if(i > stageDataLength * 2 - 2) {
                    roadList[i].gameObject.SetActive(false);
                } else if(i < currentIndex * 2) {
                    TweenA.Add(roadList[i], 0.01f, 1);
                } else if(i == currentIndex * 2 && canAcquireReward) {
                    TweenA.Add(roadList[i], 0.01f, 1);
                }
            }

            nextArrow.SetActive(false);
            tapArrow.SetActive(false);
            if(currentIndex > stageDataLength - 1) {
                character.transform.localPosition = btnTreasureList[currentIndex - 1].transform.localPosition;
            } else {
                if(canAcquireReward) {
                    character.transform.localPosition = btnTreasureList[currentIndex].transform.localPosition;
                    tapArrow.transform.localPosition = btnTreasureList[currentIndex].transform.localPosition;
                    tapArrow.SetActive(true);
                } else {
                    character.transform.localPosition = btnStageList[currentIndex].transform.localPosition;
                    nextArrow.transform.localPosition = btnStageList[currentIndex].transform.localPosition;
                    nextArrow.SetActive(true);
                }
            }
            wrapperSizeDelta.x = btnTreasureList[stageDataLength - 1].transform.localPosition.x + 150;
            if(wrapperSizeDelta.x < Constant.SCREEN_BASE_WIDTH) {wrapperSizeDelta.x = Constant.SCREEN_BASE_WIDTH;}
            contentWrapper.sizeDelta = wrapperSizeDelta;

            var initialPosition = (btnStageList[currentIndex].transform.localPosition.x - Constant.SCREEN_BASE_WIDTH / 2) / (wrapperSizeDelta.x - Constant.SCREEN_BASE_WIDTH);
            if(initialPosition < 0) {initialPosition = 0;}
            if(initialPosition > 1) {initialPosition = 1;}
            scroller.horizontalNormalizedPosition = initialPosition;
        }

        private void ResetCharacter() {
            foreach(var atlas in character.skeletonDataAsset.atlasAssets) {
                foreach(var m in atlas.materials) {
                    var shader = m.shader;
                    m.shader = null;
                    m.shader = shader;
                }
            }
        }

        private void SetStageInfo(int index) {
            if(index > currentIndex || (index == currentIndex + 1 && canAcquireReward)) {return;}
            var isClear = true;
            if((canAcquireReward && index == currentIndex + 1) || (!canAcquireReward && index == currentIndex)) {
                isClear = false;
            }
            expeditionInfo.OpenExpeditionInfo(stageDataList[index], stageDataList.Length, index, isClear);
        }

        private void GetTreasure(int index) {
            if(canAcquireReward && index == currentIndex) {
                getRewardModal.OpenModal(stageDataList[currentIndex].Rewards);
                btnTreasureList[currentIndex].SetTreasureState(3);
                currentIndex++;
                tapArrow.SetActive(false);
                canAcquireReward = false;
                if(currentIndex > stageDataList.Length - 1 && expeditionLevel < 20) {
                    getRewardModal.OnClose.Subscribe(_ => {
                        expeditionLevelUpModal.OpenModal(expeditionLevel);
                    });
                    expeditionLevelUpModal.OnClose.Subscribe(_ => {
                        character.state.SetAnimation(0, Constant.ANIM_WIN, false);
                        character.state.Complete += (Spine.AnimationState state, int trackIndex, int loopCount) => {
                            character.state.SetAnimation(0, Constant.ANIM_IDLE, true);
                        };
                        txtExpeditionLevel.text = (expeditionLevel + 1).ToString();
                    });
                } else {
                    btnStageList[currentIndex].SetStageState(2);
                    nextArrow.transform.localPosition = btnStageList[currentIndex].transform.localPosition;
                    nextArrow.SetActive(true);
                    character.GetComponent<MeshRenderer>().sortingOrder = 4;
                    character.state.SetAnimation(0, Constant.ANIM_WALK, true);
                    TweenXY.Add(character.gameObject, 0.8f, btnStageList[currentIndex].transform.localPosition).Then(() => {
                        character.state.SetAnimation(0, Constant.ANIM_IDLE, true);
                    });
                }
                Noroshi.Expedition.WebApiRequester.ReceiveReward().Subscribe();
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            }
        }

        public void OnScroll() {
            bgFarWrapper.transform.localPosition = new Vector2(
                -(contentWrapper.transform.localPosition.x + Constant.SCREEN_BASE_WIDTH / 2) * 0.25f, 0
            );
            treasureListContainer.SetActive(false);
            treasureListContainer.SetActive(true);
        }
    }
}
