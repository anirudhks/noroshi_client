﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UniRx;
using Noroshi.Core.WebApi.Response.Possession;
using Noroshi.Core.Game.Possession;

namespace Noroshi.UI {
    public class GachaResultItem : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler {
        [SerializeField] Image imgCardBack;
        [SerializeField] Image imgCardFront;
        [SerializeField] GameObject cardMiddle;
        [SerializeField] GameObject cardMiddle2;
        [SerializeField] GameObject characterIconWrapper;
        [SerializeField] Image imgCharacter;
        [SerializeField] GameObject[] evolutionStarList;
        [SerializeField] GameObject ItemIconWrapper;
        [SerializeField] ItemIcon itemIcon;
        [SerializeField] Text txtResultName;
        [SerializeField] GameObject nameWrapper;
        [SerializeField] Sprite spriteNormalCard;
        [SerializeField] Sprite spriteHeroCard;
        [SerializeField] Sprite spriteFool;
        [SerializeField] Sprite spriteFortune;
        [SerializeField] Sprite spriteSun;
        [SerializeField] ParticleSystem turnParticle;
        [SerializeField] ParticleSystem characterGetParticle;

        public Subject<bool> OnClickResultItem = new Subject<bool>();
        public Subject<uint> OnEndEffect = new Subject<uint>();

        private PossessionObject getItem;
        private bool isTurn;
        private bool canTurn;
        private bool canTap;
        private bool isShake;
        private bool isEndEffect;
        private int effectType;
        private bool isGetCharacter = false;
        private uint getCharacterID;

        private void EndGetEffect() {
            if(effectType > 1) {
                TweenNull.Add(gameObject, 0.4f).Then(() => {
                    isEndEffect = true;
                    OnEndEffect.OnNext(getCharacterID);
                    TweenNull.Add(cardMiddle2, 6.7f).Then(() => {
                        cardMiddle2.SetActive(false);
                        characterIconWrapper.SetActive(true);
                        nameWrapper.SetActive(true);
                    });
                });
            } else if(effectType > 0) {
                imgCardFront.gameObject.SetActive(true);
                TweenRY.Add(imgCardFront.gameObject, 0.2f, 0).From(90).Then(() => {
                    characterIconWrapper.SetActive(true);
                    nameWrapper.SetActive(true);
                    TweenNull.Add(gameObject, 0.4f).Then(() => {
                        TweenNull.Add(imgCardFront.gameObject, 0.9f).Then(() => {
                            imgCardFront.gameObject.SetActive(false);
                        });
                        isEndEffect = true;
                        OnEndEffect.OnNext(getCharacterID);
                    });
                });
            } else {
                imgCardFront.gameObject.SetActive(true);
                TweenRY.Add(imgCardFront.gameObject, 0.2f, 0).From(90).Then(() => {
                    ItemIconWrapper.SetActive(true);
                    nameWrapper.SetActive(true);
                    TweenNull.Add(gameObject, 0.4f).Then(() => {
                        TweenA.Add(imgCardFront.gameObject, 0.3f, 0).Then(() => {
                            var isSoul = getItem.Category == (byte)Noroshi.Core.Game.Possession.PossessionCategory.Soul;
                            ItemListManager.Instance.ChangeItemCount(getItem.ID, (int)getItem.Num, isSoul);
                            imgCardFront.gameObject.SetActive(false);
                            isEndEffect = true;
                            OnEndEffect.OnNext(0);
                        });
                    });
                });
            }
        }

        private void StartShake() {
            var posX = UnityEngine.Random.Range(2, 6);
            var posY = UnityEngine.Random.Range(-3, 3);
            var posXX = UnityEngine.Random.Range(-6, -2);
            var posYY = UnityEngine.Random.Range(-3, 3);
            var posXXX = UnityEngine.Random.Range(2, 6);
            var posYYY = UnityEngine.Random.Range(-3, 3);
            var posXXXX = UnityEngine.Random.Range(-6, -2);
            var posYYYY = UnityEngine.Random.Range(-3, 3);
            TweenXY.Add(cardMiddle, 0.05f, new Vector2(posX, posY)).Then(() => {
                TweenXY.Add(cardMiddle, 0.05f, new Vector2(posXX, posYY)).Then(() => {
                    TweenXY.Add(cardMiddle, 0.05f, new Vector2(posXXX, posYYY)).Then(() => {
                        TweenXY.Add(cardMiddle, 0.05f, new Vector2(posXXXX, posYYYY)).Then(() => {
                            TweenNull.Add(gameObject, 0.3f).Then(() => {
                                if(isShake) {StartShake();}
                            });
                        });
                    });
                });
            });
        }

        public void SkipEffect() {
            imgCardFront.gameObject.SetActive(false);
            imgCardBack.gameObject.SetActive(false);
            cardMiddle.SetActive(false);
            cardMiddle2.SetActive(false);
            if(effectType == 0) {
                ItemIconWrapper.SetActive(true);
            } else {
                characterIconWrapper.SetActive(true);
            }
        }

        public void StartInitialAnimation(float delay) {
            isTurn = false;
            canTurn = false;
            canTap = false;
            isEndEffect = false;
            ItemIconWrapper.SetActive(false);
            characterIconWrapper.SetActive(false);
            nameWrapper.SetActive(false);
            cardMiddle.SetActive(false);
            cardMiddle2.SetActive(false);
            imgCardFront.gameObject.SetActive(false);
            imgCardFront.color = new Color(1, 1, 1, 1);
            imgCardBack.gameObject.SetActive(true);
            imgCardBack.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
            TweenA.Add(gameObject, 1.0f, 1).Delay(delay).EaseInCubic().From(0).Then(() => {
                canTurn = true;
            });
        }

        public void SetItem(PossessionObject result, bool isNormal, bool isTutorial = false) {
            getItem = result;
            if(result.Category == (byte)PossessionCategory.Character) {
                var characterData = GlobalContainer.MasterManager.CharacterMaster.Get(result.ID);
                isGetCharacter = true;
                getCharacterID = result.ID;
                if(isTutorial) {
                    effectType = 2;
                    imgCardFront.sprite = spriteFortune;
                } else if(characterData.InitialEvolutionLevel > 2) {
                    effectType = 3;
                    imgCardFront.sprite = spriteSun;
                } else if(characterData.InitialEvolutionLevel == 1) {
                    effectType = 1;
                    imgCardFront.sprite = spriteFortune;
                } else {
                    var n = UnityEngine.Random.Range(0, 2);
                    if(n == 0) {
                        effectType = 2;
                    } else {
                        effectType = 1;
                    }
                    imgCardFront.sprite = spriteFortune;
                }
                imgCharacter.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite> (result.ID, "thumb_1");
                for(int i = 0, l = evolutionStarList.Length; i < l; i++) {
                    evolutionStarList[i].SetActive(i < characterData.InitialEvolutionLevel);
                }
            } else {
                isGetCharacter = false;
                getCharacterID = 0;
                effectType = 0;
                imgCardFront.sprite = spriteFool;
                itemIcon.SetItemIcon(result);
            }
            if(isNormal) {
                imgCardBack.sprite = spriteNormalCard;
            } else {
                imgCardBack.sprite = spriteHeroCard;
            }
            txtResultName.text = GlobalContainer.LocalizationManager.GetText(result.TextKey + ".Name");
        }

        public int GetEffectType() {
            return effectType;
        }

        public virtual void OnPointerClick(PointerEventData ped) {
            if(!canTap) {return;}
            isShake = false;
            OnClickResultItem.OnNext(true);
            turnParticle.gameObject.SetActive(true);
            TweenNull.Add(turnParticle.gameObject, 2.0f).Then(() => {
                turnParticle.gameObject.SetActive(false);
            });
            cardMiddle.transform.localPosition = Vector3.zero;
            TweenRY.Add(cardMiddle, 0.2f, -90).Then(() => {
                cardMiddle.SetActive(false);
                if(effectType == 1) {
                    EndGetEffect();
                } else {
                    cardMiddle2.SetActive(true);
                    TweenRY.Add(cardMiddle2, 0.2f, 0).From(90).Then(() => {
                        EndGetEffect();
                    });
                }
            });
        }

        public virtual void OnPointerEnter(PointerEventData ped) {
            if(isTurn || !canTurn) {return;}
            isTurn = true;
            turnParticle.gameObject.SetActive(true);
            TweenNull.Add(turnParticle.gameObject, 2.0f).Then(() => {
                turnParticle.gameObject.SetActive(false);
            });
            TweenRY.Add(imgCardBack.gameObject, 0.2f, -90).Then(() => {
                imgCardBack.gameObject.SetActive(false);
                if(effectType == 0) {
                    EndGetEffect();
                } else {
                    cardMiddle.SetActive(true);
                    TweenRY.Add(cardMiddle, 0.2f, 0).From(90).Then(() => {
                        canTap = true;
                        isShake = true;
                        StartShake();
                    });
                }
            });
        }

        public void EnableTap() {
            if(!isEndEffect && effectType > 0) {canTap = true;}
        }

        public void DisableTap() {
            canTap = false;
        }
    }
}
