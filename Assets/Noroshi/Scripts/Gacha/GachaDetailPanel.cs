﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.Core.WebApi.Response.Possession;

namespace Noroshi.UI {
    public class GachaDetailPanel : MonoBehaviour {
        [SerializeField] GameObject detailWrapper;
        [SerializeField] GachaDetailPiece detailPiecePref;
        [SerializeField] BtnCommon btnClose;

        private bool isOpen = false;

        private void Start() {
            btnClose.OnClickedBtn.Subscribe(_ => {
                CloseDetailPanel();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseDetailPanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        public void SetDetailPanel(PossessionObject[] itemList) {
            foreach(var item in itemList) {
                var piece = Instantiate(detailPiecePref);
                piece.SetPiece(item);
                piece.transform.SetParent(detailWrapper.transform);
                piece.transform.localScale = Vector3.one;
            }
        }

        public void OpenDetailPanel() {
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }

        public void CloseDetailPanel() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }
    }
}
