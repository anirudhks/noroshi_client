﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Noroshi.Core.WebApi.Response.Possession;

namespace Noroshi.UI {
    public class GachaDetailPiece : MonoBehaviour {
        [SerializeField] Text txtName;
        [SerializeField] ItemIcon itemIcon;
        [SerializeField] Image imgCharacter;
        [SerializeField] GameObject characterIconWrapper;
        [SerializeField] GameObject[] evolutionStarList;

        public void SetPiece(PossessionObject itemData) {
            txtName.text = GlobalContainer.LocalizationManager.GetText(itemData.TextKey + ".Name");
            if(itemData.Category == (byte)Noroshi.Core.Game.Possession.PossessionCategory.Character) {
                var characterData = GlobalContainer.MasterManager.CharacterMaster.Get(itemData.ID);
                itemIcon.gameObject.SetActive(false);
                imgCharacter.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite> (itemData.ID, "thumb_1");
                for(int i = 0, l = characterData.InitialEvolutionLevel; i < l; i++) {
                    evolutionStarList[i].SetActive(true);
                }
            } else {
                var rarity = ItemListManager.Instance.GetItemRarity(itemData);
                characterIconWrapper.SetActive(false);
                itemIcon.SetItemIcon(itemData);
            }
        }
    }
}
