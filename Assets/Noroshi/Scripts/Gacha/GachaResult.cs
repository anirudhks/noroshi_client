﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.WebApi.Response.Gacha;

namespace Noroshi.UI {
    public class GachaResult : MonoBehaviour {
        [SerializeField] GameObject resultCamera;
        [SerializeField] GameObject tapAlertWrapper;
        [SerializeField] GameObject uiWrapper;
        [SerializeField] GachaResultItem[] resultItemList;
        [SerializeField] GameObject[] catImgList;
        [SerializeField] GameObject cutInImgWrapper;
        [SerializeField] GameObject cutInImg;
        [SerializeField] GameObject whiteOverlay;
        [SerializeField] Image imgNeedCurrency;
        [SerializeField] Text txtNeedCurrency;
        [SerializeField] Image imgHaveCurrency;
        [SerializeField] Text txtHaveCurrency;
        [SerializeField] Image imgGetCurrency;
        [SerializeField] Text txtGetCurrency;
        [SerializeField] BtnCommon btnReturn;
        [SerializeField] BtnCommon btnAgain;
        [SerializeField] BtnCommon btnGetCurrency;
        [SerializeField] BtnLoadLevel btnBackToTutorial;
        [SerializeField] Animator animCutIn;
        [SerializeField] Animator animFlipSun;
        [SerializeField] Animator animFlipFortune;
        [SerializeField] GameObject processing;

        public Subject<uint> OnGachaAgain = new Subject<uint>();
        public Subject<bool> OnEndGacha = new Subject<bool>();

        private int lotNum;
        private bool isShowGetCharacterModal = false;
        private byte maxRarityInResult;
        private byte changeCurrencyType;

        private void Awake() {
            var baseAspect = Constant.SCREEN_BASE_WIDTH / Constant.SCREEN_BASE_HEIGHT;
            var crntAspect = (float)Screen.width / (float)Screen.height;
            var posZ = -1208.127f * (baseAspect / crntAspect) + 18.127f;
            resultCamera.transform.localPosition = new Vector3(0, 0, posZ);
        }

        private void Start() {
            ItemListManager.Instance.OnGetCharacter.Subscribe(getModal => {
                getModal.OnCloseGetCharacterModal.Subscribe(_ => {
                    gameObject.SetActive(true);
                }).AddTo(getModal);
                gameObject.SetActive(false);
            }).AddTo(this);

            ItemListManager.Instance.OnEvolutionCharacter.Subscribe(evolutionModal => {
                evolutionModal.OnCloseEvolutionCharacterModal.Subscribe(_ => {
                    if(!isShowGetCharacterModal) {gameObject.SetActive(true);}
                }).AddTo(evolutionModal);
                gameObject.SetActive(false);
            }).AddTo(this);

            foreach(var resultItem in resultItemList) {
                var result = resultItem;
                result.OnClickResultItem.Subscribe(_ => {
                    foreach(var r in resultItemList) {
                        r.DisableTap();
                    }
                });
                resultItem.OnEndEffect.Subscribe(newCharacterID => {
                    lotNum--;
                    if(lotNum <= 0) {
                        var delay = result.GetEffectType() > 1 ? 7.0f :
                            result.GetEffectType() > 0 ? 1.5f : 0.2f;
                        tapAlertWrapper.SetActive(false);
                        TweenNull.Add(result.gameObject, delay).Then(() => {
                            uiWrapper.SetActive(true);
                        });
                        OnEndGacha.OnNext(true);
                    }
                    foreach(var r in resultItemList) {
                        r.EnableTap();
                    }
                    if(newCharacterID > 0) {
                        var effectType = result.GetEffectType();
                        var pcID = BattleCharacterSelect.Instance.GetPlayerCharacterId(new uint[]{newCharacterID});
                        if(effectType > 1) {
                            animCutIn.gameObject.SetActive(true);
                            animCutIn.Play("GachaCutIn", 0, 0.0f);
                            TweenNull.Add(animCutIn.gameObject, 3.0f).Then(() => {
                                animCutIn.gameObject.SetActive(false);
                                if(effectType > 2) {
                                    animFlipSun.gameObject.SetActive(true);
                                    animFlipSun.Play("GachaFlip", 0, 0.0f);
                                } else {
                                    animFlipFortune.gameObject.SetActive(true);
                                    animFlipFortune.Play("GachaFlip", 0, 0.0f);
                                }
                                TweenNull.Add(gameObject, 4.0f).Then(() => {
                                    var getModal = Instantiate(Resources.Load<GetCharacterModal>("UI/GetCharacterModal"));
                                    isShowGetCharacterModal = true;
                                    getModal.OpenModal(newCharacterID);
                                    gameObject.SetActive(false);
                                    getModal.OnCloseGetCharacterModal.Subscribe(_ => {
                                        isShowGetCharacterModal = false;
                                        gameObject.SetActive(true);
                                    }).AddTo(getModal);
                                    animFlipSun.gameObject.SetActive(false);
                                    animFlipFortune.gameObject.SetActive(false);
                                });
                            });
                        } else {
                            whiteOverlay.SetActive(true);
                            TweenA.Add(whiteOverlay, 0.4f, 1).Delay(0.5f).From(0).EaseInCirc().Then(() => {
                                TweenNull.Add(whiteOverlay, 0.2f).Then(() => {
                                    var getModal = Instantiate(Resources.Load<GetCharacterModal>("UI/GetCharacterModal"));
                                    isShowGetCharacterModal = true;
                                    getModal.OpenModal(newCharacterID);
                                    gameObject.SetActive(false);
                                    getModal.OnCloseGetCharacterModal.Subscribe(_ => {
                                        isShowGetCharacterModal = false;
                                        gameObject.SetActive(true);
                                    }).AddTo(getModal);
                                    whiteOverlay.SetActive(false);
                                });
                            });
                        }
                        if(pcID.Length < 1) {
                            ItemListManager.Instance.UpdateNeedSoul(newCharacterID);
                        }
                    }
                });
            }

            btnReturn.OnClickedBtn.Subscribe(_ => {
                CloseGachaResult();
            });
            btnReturn.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnAgain.OnClickedBtn.Subscribe(id => {
                processing.SetActive(true);
                OnGachaAgain.OnNext((uint)id);
            });
            btnAgain.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });

            btnGetCurrency.OnClickedBtn.Subscribe(_ => {
                CloseGachaResult();
                if(changeCurrencyType == (byte)Noroshi.Core.Game.Possession.PossessionStatusID.Gold) {
                    PlayerInfo.Instance.OpenGoldChangeModal();
                }
            });
            btnGetCurrency.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        private void CloseGachaResult() {
            uiWrapper.SetActive(false);
            TweenA.Add(gameObject, 0.2f, 0).Then(() => {
                gameObject.SetActive(false);
                BackButtonController.Instance.IsEnable(true);
            });
        }

        private void StartAnimation() {
            var n = UnityEngine.Random.Range(0, 3);
            var delay = 1.8f;
            if(maxRarityInResult > 2) {
                n = n > 0 ? 0 : 1;
            } else if(maxRarityInResult > 1) {
                n = n > 1 ? 0 : 1;
            } else {
                n = -1;
                delay = 0.1f;
            }
            uiWrapper.SetActive(false);
            for(int i = 0, l = catImgList.Length; i < l; i++) {
                catImgList[i].SetActive(i == n);
            }
            if(maxRarityInResult > 1) {
                TweenA.Add(catImgList[n], 0.4f, 0).Delay(1.8f).From(1).Then(() => {
                    TweenNull.Add(gameObject, 0.7f).Then(() => {
                        tapAlertWrapper.SetActive(true);
                    });
                });
            } else {
                TweenNull.Add(gameObject, 1.1f).Then(() => {
                    tapAlertWrapper.SetActive(true);
                });
            }
            for(int i = 0, l = resultItemList.Length; i < l; i++) {
                resultItemList[i].StartInitialAnimation(delay);
            }
        }

        public void SetGachaResult(LotResponse result, bool isNormal) {
            maxRarityInResult = 0;
            lotNum = result.LotPossessionObjects.Length;
            for(int i = 0, l = resultItemList.Length; i < l; i++) {
                if(i < lotNum) {
                    resultItemList[i].gameObject.SetActive(true);
                    resultItemList[i].SetItem(result.LotPossessionObjects[i], isNormal, result.GachaEntryPoint.ID == 5);
                    if(result.LotPossessionObjects[i].Category == (byte)Noroshi.Core.Game.Possession.PossessionCategory.Character) {
                        var characterData = GlobalContainer.MasterManager.CharacterMaster.Get(result.LotPossessionObjects[i].ID);
                        if(characterData.InitialEvolutionLevel > maxRarityInResult) {
                            maxRarityInResult = characterData.InitialEvolutionLevel;
                        }
                    }
                } else {
                    resultItemList[i].gameObject.SetActive(false);
                }
            }
            if(result.GachaEntryPoint.ID == 5) {
                btnReturn.gameObject.SetActive(false);
                btnAgain.gameObject.SetActive(false);
                btnGetCurrency.gameObject.SetActive(false);
                btnBackToTutorial.gameObject.SetActive(true);
                imgHaveCurrency.gameObject.SetActive(false);
                txtHaveCurrency.gameObject.SetActive(false);
            } else if(result.GachaEntryPoint.CanLot) {
                imgNeedCurrency.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(result.GachaEntryPoint.Payment.ID);
                txtNeedCurrency.text = string.Format("{0:#,0}\r", result.GachaEntryPoint.Payment.Num);
                imgHaveCurrency.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(result.GachaEntryPoint.Payment.ID);
                txtHaveCurrency.text = string.Format("{0:#,0}\r", result.GachaEntryPoint.Payment.PossessingNum);
                btnAgain.id = (int)result.GachaEntryPoint.ID;
                btnAgain.gameObject.SetActive(true);
                btnGetCurrency.gameObject.SetActive(false);
            } else {
                imgHaveCurrency.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(result.GachaEntryPoint.Payment.ID);
                txtHaveCurrency.text = string.Format("{0:#,0}\r", result.GachaEntryPoint.Payment.PossessingNum);
                btnAgain.gameObject.SetActive(false);
                if(result.GachaEntryPoint.Payment.ID == (byte)Noroshi.Core.Game.Possession.PossessionStatusID.Gold
//                    || result.GachaEntryPoint.Payment.ID == (byte)Noroshi.Core.Game.Possession.PossessionStatusID.CommonGem
//                    || result.GachaEntryPoint.Payment.ID == (byte)Noroshi.Core.Game.Possession.PossessionStatusID.FreeGem
                ) {
                    changeCurrencyType = (byte)result.GachaEntryPoint.Payment.ID;
                    imgGetCurrency.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(result.GachaEntryPoint.Payment.ID);
                    txtGetCurrency.text = string.Format("{0:#,0}\r", result.GachaEntryPoint.Payment.Num);
                    btnGetCurrency.gameObject.SetActive(true);
                } else {
                    btnGetCurrency.gameObject.SetActive(false);
                }
            }
            processing.SetActive(false);
            StartAnimation();
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.2f, 1);
        }
    }
}
