using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using UniRx;
using Noroshi.Core.WebApi.Response.Gacha;

namespace Noroshi.UI {
    public class GachaController : MonoBehaviour {
        [SerializeField] GameObject gachaSelectContainer;
        [SerializeField] BtnCommon[] btnGachaList;
        [SerializeField] BtnCommon btnDecideNormalGacha;
        [SerializeField] BtnCommon btnDecideNormalMultiGacha;
        [SerializeField] BtnCommon btnDecideHeroGacha;
        [SerializeField] BtnCommon btnDecideHeroMultiGacha;
        [SerializeField] BtnCommon btnDecideGuildGacha;
        [SerializeField] BtnCommon btnTutorialGacha;
        [SerializeField] BtnCommon[] tabList;
        [SerializeField] GameObject[] gachaPanelList;
        [SerializeField] GameObject tutorialGachaPanel;
        [SerializeField] BtnCommon[] btnDetailList;
        [SerializeField] GachaDetailPanel[] detailPanelList;
        [SerializeField] GameObject[] confirmPanelList;
        [SerializeField] BtnCommon[] btnCloseConfirmList;
        [SerializeField] GachaResult gachaResult;
        [SerializeField] Text txtNeedGoldToNormal;
        [SerializeField] Text txtNeedGoldToNormalMulti;
        [SerializeField] Text txtNeedGemToHero;
        [SerializeField] Text txtNeedGemToHeroMulti;
        [SerializeField] Text txtNeedMedalToGuild;
        [SerializeField] Text txtHaveGuildMedal;
        [SerializeField] Text txtNormalMultiLotNum;
        [SerializeField] Text txtHeroMultiLotNum;
        [SerializeField] Text txtNeedGoldToNormalConfirm;
        [SerializeField] Text txtNeedGoldToNormalMultiConfirm;
        [SerializeField] Text txtNeedGemToHeroConfirm;
        [SerializeField] Text txtNeedGemToHeroMultiConfirm;
        [SerializeField] Text txtNeedMedalToGuildConfirm;
        [SerializeField] GameObject labelTabNormalFree;
        [SerializeField] GameObject labelNormalFree;
        [SerializeField] GameObject untilNormalFreeWrapper;
        [SerializeField] Text txtNormalTimeToFree;
        [SerializeField] Text txtRemainNum;
        [SerializeField] Text txtMaxNum;
        [SerializeField] GameObject labelTabHeroFree;
        [SerializeField] GameObject labelHeroFree;
        [SerializeField] GameObject untilHeroFreeWrapper;
        [SerializeField] GameObject labelSpecialGacha;
        [SerializeField] Text txtHeroTimeToFree;
        [SerializeField] GameObject startComment;
        [SerializeField] GameObject gachaCommentWrappr;
        [SerializeField] GameObject[] gachaCommentList;
        [SerializeField] GameObject processing;

        public Subject<bool> OnEndGacha = new Subject<bool>();

        private uint needGoldToNormal;
        private uint needGoldToNormalMulti;
        private uint needGemToHero;
        private uint needGemToHeroMulti;
        private bool isFreeNormal;
        private bool isFreeHero;
        private int remainNormalFreeNum;
        private int normalReopenedAt;
        private int heroReopenedAt;
        private IDisposable normalTimer;
        private IDisposable heroTimer;
        private int openIndex;
        private bool isOpenConfirm = false;

        private void Start() {
            if(SoundController.Instance != null) {
                SoundController.Instance.PlayBGM(SoundController.BGMKeys.GACHA);
            }

            Noroshi.Gacha.WebApiRequester.EntryPointList().Do(data => {
                for(int i = 0, l = data.GachaEntryPoints.Length; i < l; i++) {
                    switch(data.GachaEntryPoints[i].ID) {
                        case 1: SetNormalGacha(data.GachaEntryPoints[i]); break;
                        case 2: SetNormalMultiGacha(data.GachaEntryPoints[i]); break;
                        case 3: SetHeroGacha(data.GachaEntryPoints[i]);break;
                        case 4: SetHeroMultiGacha(data.GachaEntryPoints[i]);break;
                        case 6: SetGuildGacha(data.GachaEntryPoints[i]);break;
                        default: continue;
                    }
                }
                SelectTab(0);
                TweenNull.Add(startComment, 2.5f).Then(() => {
                    startComment.SetActive(false);
                    gachaCommentWrappr.SetActive(true);
                });
                UILoading.Instance.HideLoading();
            }).Subscribe();

            foreach(var tab in tabList) {
                tab.OnClickedBtn.Subscribe(SelectTab);
                tab.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            foreach(var btn in btnDetailList) {
                btn.OnClickedBtn.Subscribe(index => {
                    detailPanelList[index].OpenDetailPanel();
                });
                btn.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            for(int i = 0, l = btnCloseConfirmList.Length; i < l; i++) {
                btnGachaList[i].OnClickedBtn.Subscribe(index => {
                    openIndex = index;
                    isOpenConfirm = true;
                    confirmPanelList[index].SetActive(true);
                    TweenA.Add(confirmPanelList[index], 0.1f, 1).From(0);
                    BackButtonController.Instance.IsModalOpen(true);
                    TweenA.Add(gachaSelectContainer, 0.1f, 0).Then(() => {
                        gachaSelectContainer.SetActive(false);
                    });
                });
                btnGachaList[i].OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });

                btnCloseConfirmList[i].OnClickedBtn.Subscribe(CloseConfirm);
                btnCloseConfirmList[i].OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
                });
            }

            btnTutorialGacha.OnClickedBtn.Subscribe(_ => {
                DrawTutorialGacha();
            });
            btnTutorialGacha.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });

            gachaResult.OnGachaAgain.Subscribe(id => {
                switch(id) {
                    case 1: DrawNormalGacha(id); break;
                    case 2: DrawNormalMultiGacha(id); break;
                    case 3: DrawHeroGacha(id); break;
                    case 4: DrawHeroMultiGacha(id); break;
                    case 6: DrawGuildGacha(id); break;
                }
            });
            gachaResult.OnEndGacha.Subscribe(_ => {
                OnEndGacha.OnNext(true);
            });

            PlayerInfo.Instance.OnChangePlayerStatus.Subscribe(_ => {
                if(!btnGachaList[0].isEnable) {
                    if(PlayerInfo.Instance.HaveGold >= needGoldToNormal) {
                        btnGachaList[0].SetEnable(true);
                        txtNeedGoldToNormal.color = Constant.TEXT_COLOR_NORMAL_WHITE;
                    }
                }
                if(!btnGachaList[1].isEnable) {
                    if(PlayerInfo.Instance.HaveGold >= needGoldToNormalMulti) {
                        btnGachaList[1].SetEnable(true);
                        txtNeedGoldToNormalMulti.color = Constant.TEXT_COLOR_NORMAL_WHITE;
                    }
                }
                if(!btnGachaList[2].isEnable) {
                    if(PlayerInfo.Instance.HaveGem >= needGemToHero) {
                        btnGachaList[2].SetEnable(true);
                        txtNeedGemToHero.color = Constant.TEXT_COLOR_NORMAL_WHITE;
                    }
                }
                if(!btnGachaList[3].isEnable) {
                    if(PlayerInfo.Instance.HaveGem >= needGemToHeroMulti) {
                        btnGachaList[3].SetEnable(true);
                        txtNeedGemToHeroMulti.color = Constant.TEXT_COLOR_NORMAL_WHITE;
                    }
                }
            }).AddTo(this);

            BackButtonController.Instance.OnBack.Subscribe(_ => {
                UILoading.Instance.LoadScene(Constant.SCENE_MAIN);
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpenConfirm) {return;}
                CloseConfirm(openIndex);
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void SelectTab(int index) {
            for(int i = 0, l = tabList.Length; i < l; i++) {
                tabList[i].SetSelect(i == index);
                gachaPanelList[i].SetActive(i == index);
                gachaCommentList[i].SetActive(i == index);
            }
        }

        private void CloseConfirm(int index) {
            TweenA.Add(confirmPanelList[index], 0.1f, 0).Then(() => {
                isOpenConfirm = false;
                confirmPanelList[index].SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
            gachaSelectContainer.SetActive(true);
            TweenA.Add(gachaSelectContainer, 0.1f, 1).Delay(0.3f).From(0);
        }

        private void StartNormalCoolDownTimer() {
            normalTimer = Observable.Interval(TimeSpan.FromSeconds(1)).Subscribe(l => {
                var currentTime = GlobalContainer.TimeHandler.UnixTime;
                var m = (int)((normalReopenedAt - currentTime) / 60);
                var s = (int)((normalReopenedAt - currentTime) % 60);
                var minute = m < 10 ? "0" + m : m.ToString();
                var second = s < 10 ? "0" + s : s.ToString();
                txtNormalTimeToFree.text = minute + ":" + second;
                if(normalReopenedAt - currentTime < 0) {
                    normalTimer.Dispose();
                    untilNormalFreeWrapper.SetActive(false);
                    if(remainNormalFreeNum > 0) {
                        isFreeNormal = true;
                        txtNeedGoldToNormal.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.Free");
                        txtNeedGoldToNormalConfirm.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.Free");
                        labelNormalFree.SetActive(true);
                        labelTabNormalFree.SetActive(true);
                        btnGachaList[0].SetEnable(true);
                    }
                    return;
                }
            }).AddTo(this);
        }

        private void StartHeroCoolDownTimer() {
            heroTimer = Observable.Interval(TimeSpan.FromSeconds(1)).Subscribe(l => {
                var currentTime = GlobalContainer.TimeHandler.UnixTime;
                var m = (int)((heroReopenedAt - currentTime) / 60);
                var s = (int)((heroReopenedAt - currentTime) % 60);
                var minute = m < 10 ? "0" + m : m.ToString();
                var second = s < 10 ? "0" + s : s.ToString();
                txtHeroTimeToFree.text = minute + ":" + second;
                if(heroReopenedAt - currentTime < 0) {
                    heroTimer.Dispose();
                    untilHeroFreeWrapper.SetActive(false);
                    isFreeHero = true;
                    txtNeedGemToHero.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.Free");
                    txtNeedGemToHeroConfirm.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.Free");
                    labelHeroFree.SetActive(true);
                    labelTabHeroFree.SetActive(true);
                    btnGachaList[0].SetEnable(true);
                    return;
                }
            }).AddTo(this);
        }

        private void SetNormalGacha(GachaEntryPoint gachaData) {
            needGoldToNormal = gachaData.Payment.Num;
            detailPanelList[0].SetDetailPanel(gachaData.LotCandidates);
            SetNormalGachaState(gachaData);
            btnDecideNormalGacha.OnClickedBtn.Subscribe(_ => {
                DrawNormalGacha(gachaData.ID);
            });
            btnDecideNormalGacha.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });
        }

        private void SetNormalGachaState(GachaEntryPoint gachaData) {
            if(gachaData.CanFreeLot) {
                txtNeedGoldToNormal.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.Free");
                txtNeedGoldToNormalConfirm.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.Free");
                labelNormalFree.SetActive(true);
                labelTabNormalFree.SetActive(true);
                untilNormalFreeWrapper.SetActive(false);
                btnGachaList[0].SetEnable(true);
            } else {
                txtNeedGoldToNormal.text = string.Format("{0:#,0}\r", gachaData.Payment.Num);
                txtNeedGoldToNormalConfirm.text = string.Format("{0:#,0}\r", gachaData.Payment.Num);
                labelNormalFree.SetActive(false);
                labelTabNormalFree.SetActive(false);
                if(gachaData.MaxDailyFreeLotNum - gachaData.TodayFreeLotNum > 0) {
                    var m = (int)((gachaData.FreeReopenedAt - GlobalContainer.TimeHandler.UnixTime) / 60);
                    var s = (int)((gachaData.FreeReopenedAt - GlobalContainer.TimeHandler.UnixTime) % 60);
                    var minute = m < 10 ? "0" + m : m.ToString();
                    var second = s < 10 ? "0" + s : s.ToString();
                    txtNormalTimeToFree.text = minute + ":" + second;
                    normalReopenedAt = (int)gachaData.FreeReopenedAt;
                    untilNormalFreeWrapper.SetActive(true);
                    StartNormalCoolDownTimer();
                } else {
                    untilNormalFreeWrapper.SetActive(false);
                }
                btnGachaList[0].SetEnable(gachaData.CanLot);
                if(gachaData.CanLot) {
                    txtNeedGoldToNormal.color = Constant.TEXT_COLOR_NORMAL_WHITE;
                } else {
                    txtNeedGoldToNormal.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
                }
            }
            isFreeNormal = gachaData.CanFreeLot;
            remainNormalFreeNum = (int)(gachaData.MaxDailyFreeLotNum - gachaData.TodayFreeLotNum);
            txtRemainNum.text = (gachaData.MaxDailyFreeLotNum - gachaData.TodayFreeLotNum).ToString();
            txtMaxNum.text = gachaData.MaxDailyFreeLotNum.ToString();
        }

        private void SetNormalMultiGacha(GachaEntryPoint gachaData) {
            needGoldToNormalMulti = gachaData.Payment.Num;
            SetNomalMultiGachaState(gachaData);
            btnDecideNormalMultiGacha.OnClickedBtn.Subscribe(_ => {
                DrawNormalMultiGacha(gachaData.ID);
            });
            btnDecideNormalMultiGacha.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });
        }

        private void SetNomalMultiGachaState(GachaEntryPoint gachaData) {
            txtNormalMultiLotNum.text = gachaData.LotNum.ToString();
            txtNeedGoldToNormalMulti.text = string.Format("{0:#,0}\r", gachaData.Payment.Num);
            txtNeedGoldToNormalMultiConfirm.text = string.Format("{0:#,0}\r", gachaData.Payment.Num);
            btnGachaList[1].SetEnable(gachaData.CanLot);
            if(gachaData.CanLot) {
                txtNeedGoldToNormalMulti.color = Constant.TEXT_COLOR_NORMAL_WHITE;
            } else {
                txtNeedGoldToNormalMulti.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
            }
        }

        private void SetHeroGacha(GachaEntryPoint gachaData) {
            needGemToHero = gachaData.Payment.Num;
            detailPanelList[1].SetDetailPanel(gachaData.LotCandidates);
            SetHeroGachaState(gachaData);
            btnDecideHeroGacha.OnClickedBtn.Subscribe(_ => {
                DrawHeroGacha(gachaData.ID);
            });
            btnDecideHeroGacha.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });
        }

        private void SetHeroGachaState(GachaEntryPoint gachaData) {
            if(gachaData.CanFreeLot) {
                txtNeedGemToHero.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.Free");
                txtNeedGemToHeroConfirm.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.Free");
                labelHeroFree.SetActive(true);
                labelTabHeroFree.SetActive(true);
                untilHeroFreeWrapper.SetActive(false);
                btnGachaList[2].SetEnable(true);
            } else {
                txtNeedGemToHero.text = string.Format("{0:#,0}\r", gachaData.Payment.Num);
                txtNeedGemToHeroConfirm.text = string.Format("{0:#,0}\r", gachaData.Payment.Num);
                labelHeroFree.SetActive(false);
                labelTabHeroFree.SetActive(false);
//                untilHeroFreeWrapper.SetActive(true);
//                heroReopenedAt = (int)gachaData.FreeReopenedAt;
//                txtHeroTimeToFree.text = (gachaData.FreeReopenedAt - GlobalContainer.TimeHandler.UnixTime).ToString();
                btnGachaList[2].SetEnable(gachaData.CanLot);
                if(gachaData.CanLot) {
                    txtNeedGemToHero.color = Constant.TEXT_COLOR_NORMAL_WHITE;
                } else {
                    txtNeedGemToHero.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
                }
            }
            isFreeHero = gachaData.CanFreeLot;
        }

        private void SetHeroMultiGacha(GachaEntryPoint gachaData) {
            needGemToHeroMulti = gachaData.Payment.Num;
            SetHeroMultiGachaState(gachaData);
            btnDecideHeroMultiGacha.OnClickedBtn.Subscribe(_ => {
                DrawHeroMultiGacha(gachaData.ID);
            });
            btnDecideHeroMultiGacha.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });
        }

        private void SetHeroMultiGachaState(GachaEntryPoint gachaData) {
            txtHeroMultiLotNum.text = gachaData.LotNum.ToString();
            txtNeedGemToHeroMulti.text = string.Format("{0:#,0}\r", gachaData.Payment.Num);
            txtNeedGemToHeroMultiConfirm.text = string.Format("{0:#,0}\r", gachaData.Payment.Num);
            btnGachaList[3].SetEnable(gachaData.CanLot);
            if(gachaData.CanLot) {
                txtNeedGemToHeroMulti.color = Constant.TEXT_COLOR_NORMAL_WHITE;
            } else {
                txtNeedGemToHeroMulti.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
            }
            labelSpecialGacha.SetActive(gachaData.LotNum < 10);
        }

        private void SetGuildGacha(GachaEntryPoint gachaData) {
            detailPanelList[2].SetDetailPanel(gachaData.LotCandidates);
            SetGuildGachaState(gachaData);
            btnDecideGuildGacha.OnClickedBtn.Subscribe(_ => {
                DrawGuildGacha(gachaData.ID);
            });
            btnDecideGuildGacha.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });
        }

        private void SetGuildGachaState(GachaEntryPoint gachaData) {
            txtHaveGuildMedal.text = gachaData.Payment.PossessingNum.ToString();
            txtNeedMedalToGuild.text = string.Format("{0:#,0}\r", gachaData.Payment.Num);
            txtNeedMedalToGuildConfirm.text = string.Format("{0:#,0}\r", gachaData.Payment.Num);
            btnGachaList[4].SetEnable(gachaData.CanLot);
            if(gachaData.CanLot) {
                txtNeedMedalToGuild.color = Constant.TEXT_COLOR_NORMAL_WHITE;
            } else {
                txtNeedMedalToGuild.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
            }
        }

        private void DrawNormalGacha(uint id) {
            processing.SetActive(true);
            isOpenConfirm = false;
            BackButtonController.Instance.IsEnable(false);
            Noroshi.Gacha.WebApiRequester.Lot(id, isFreeNormal).Do(res => {
                var useGold = PlayerInfo.Instance.HaveGold - res.GachaEntryPoint.Payment.PossessingNum;
                PlayerInfo.Instance.ChangeHaveGold(-(int)useGold);
                if(res.GachaEntryPoint.Payment.PossessingNum < needGoldToNormalMulti) {
                    btnGachaList[1].SetEnable(false);
                    txtNeedGoldToNormalMulti.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
                }
                SetNormalGachaState(res.GachaEntryPoint);
                CloseConfirm(0);
                processing.SetActive(false);
                gachaResult.SetGachaResult(res, true);
            }).Subscribe();
        }

        private void DrawNormalMultiGacha(uint id) {
            processing.SetActive(true);
            isOpenConfirm = false;
            BackButtonController.Instance.IsEnable(false);
            Noroshi.Gacha.WebApiRequester.Lot(id).Do(res => {
                var useGold = PlayerInfo.Instance.HaveGold - res.GachaEntryPoint.Payment.PossessingNum;
                PlayerInfo.Instance.ChangeHaveGold(-(int)useGold);
                if(!isFreeNormal && res.GachaEntryPoint.Payment.PossessingNum < needGoldToNormal) {
                    btnGachaList[0].SetEnable(false);
                    txtNeedGoldToNormalMulti.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
                }
                SetNomalMultiGachaState(res.GachaEntryPoint);
                CloseConfirm(1);
                processing.SetActive(false);
                gachaResult.SetGachaResult(res, true);
            }).Subscribe();
        }

        private void DrawHeroGacha(uint id) {
            processing.SetActive(true);
            isOpenConfirm = false;
            BackButtonController.Instance.IsEnable(false);
            Noroshi.Gacha.WebApiRequester.Lot(id, isFreeHero).Do(res => {
                var useGem = PlayerInfo.Instance.HaveGem - res.GachaEntryPoint.Payment.PossessingNum;
                PlayerInfo.Instance.ChangeHaveGem(-(int)useGem);
                if(res.GachaEntryPoint.Payment.PossessingNum < needGemToHeroMulti) {
                    btnGachaList[3].SetEnable(false);
                    txtNeedGemToHeroMulti.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
                }
                SetHeroGachaState(res.GachaEntryPoint);
                CloseConfirm(2);
                processing.SetActive(false);
                gachaResult.SetGachaResult(res, false);
            }).Subscribe();
        }

        private void DrawHeroMultiGacha(uint id) {
            processing.SetActive(true);
            isOpenConfirm = false;
            BackButtonController.Instance.IsEnable(false);
            Noroshi.Gacha.WebApiRequester.Lot(id).Do(res => {
                var useGem = PlayerInfo.Instance.HaveGem - res.GachaEntryPoint.Payment.PossessingNum;
                PlayerInfo.Instance.ChangeHaveGem(-(int)useGem);
                if(res.GachaEntryPoint.Payment.PossessingNum < needGemToHero) {
                    btnGachaList[2].SetEnable(false);
                    txtNeedGemToHero.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
                }
                SetHeroMultiGachaState(res.GachaEntryPoint);
                CloseConfirm(3);
                processing.SetActive(false);
                gachaResult.SetGachaResult(res, false);
            }).Subscribe();
        }

        private void DrawGuildGacha(uint id) {
            processing.SetActive(true);
            isOpenConfirm = false;
            BackButtonController.Instance.IsEnable(false);
            Noroshi.Gacha.WebApiRequester.Lot(id).Do(res => {
                SetGuildGachaState(res.GachaEntryPoint);
                CloseConfirm(4);
                processing.SetActive(false);
                gachaResult.SetGachaResult(res, false);
            }).Subscribe();
        }

        private void DrawTutorialGacha() {
            processing.SetActive(true);
            isOpenConfirm = false;
            BackButtonController.Instance.IsEnable(false);
            Noroshi.Gacha.WebApiRequester.LotTutorialGacha().Do(res => {
                btnTutorialGacha.SetEnable(false);
                processing.SetActive(false);
                gachaResult.SetGachaResult(res, false);
            }).Subscribe();
        }
    }
}
