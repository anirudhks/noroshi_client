﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.Core.WebApi.Response.Possession;
using Noroshi.Core.Game.Possession;

namespace Noroshi.UI {
    public class RewardDetailPanel : MonoBehaviour {
        [SerializeField] BtnCommon[] tabList;
        [SerializeField] GameObject[] contentList;
        [SerializeField] BtnCommon btnOverlay;
        [SerializeField] GameObject[] entryRewardList;
        [SerializeField] GameObject[] discoverRewardList;
        [SerializeField] GameObject[] friendShipRewardList;
        [SerializeField] ItemIcon[] imgEntryRewardList;
        [SerializeField] ItemIcon[] imgDiscoverRewardList;
        [SerializeField] ItemIcon[] imgFriendShipRewardList;
        [SerializeField] Text[] txtEntryRewardList;
        [SerializeField] Text[] txtDiscoverRewardList;
        [SerializeField] Text[] txtFriendShipRewardList;

        private bool isOpen = false;

        private void Start() {
            for(int i = 0, l = tabList.Length; i < l; i++) {
                tabList[i].OnClickedBtn.Subscribe(SwitchTab);
                tabList[i].OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnOverlay.OnClickedBtn.Subscribe(_ => {
                ClosePanel();
            });
            btnOverlay.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                ClosePanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void SwitchTab(int index) {
            for(int i = 0, l = contentList.Length; i < l; i++) {
                var isSelect = index == i;
                tabList[i].SetSelect(isSelect);
                contentList[i].SetActive(isSelect);
            }
        }

        private void SetEntryRewards(PossessionObject[] rewards) {
            var gclm = GlobalContainer.LocalizationManager;
            for(int i = 0, l = entryRewardList.Length; i < l; i++) {
                if(i < rewards.Length) {
                    imgEntryRewardList[i].SetItemIcon(rewards[i]);
                    if(rewards[i].Num > 1) {
                        txtEntryRewardList[i].text = gclm.GetText(rewards[i].TextKey + ".Name") + "  x" + rewards[i].Num;
                    } else {
                        txtEntryRewardList[i].text = gclm.GetText(rewards[i].TextKey + ".Name");
                    }
                    entryRewardList[i].SetActive(true);
                } else {
                    entryRewardList[i].SetActive(false);
                }
            }
        }

        private void SetDiscoveryRewards(PossessionObject[] rewards) {
            var gclm = GlobalContainer.LocalizationManager;
            for(int i = 0, l = discoverRewardList.Length; i < l; i++) {
                if(i < rewards.Length) {
                    imgDiscoverRewardList[i].SetItemIcon(rewards[i]);
                    if(rewards[i].Num > 1) {
                        txtDiscoverRewardList[i].text = gclm.GetText(rewards[i].TextKey + ".Name") + "  x" + rewards[i].Num;
                    } else {
                        txtDiscoverRewardList[i].text = gclm.GetText(rewards[i].TextKey + ".Name");
                    }
                    discoverRewardList[i].SetActive(true);
                } else {
                    discoverRewardList[i].SetActive(false);
                }
            }
        }

        public void ClosePanel() {
            TweenA.Add(gameObject, 0.15f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }

        public void OpenPanel(BossDetailPanel.BossBattleData data) {
            SetEntryRewards(data.EntryRewards);
            SetDiscoveryRewards(data.DiscoveryRewards);
            SwitchTab(0);
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }
    }
}
