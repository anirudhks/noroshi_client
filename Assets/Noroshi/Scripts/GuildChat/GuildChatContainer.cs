﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using UniRx;
using UniLinq;
using Photon.Flaggs;

namespace Noroshi.UI {
    public class GuildChatContainer : MonoBehaviour {
        [SerializeField] Text txtGuildName;
        [SerializeField] Text txtCurrentMemberNum;
        [SerializeField] Text txtMaxMemberNum;
        [SerializeField] GameObject memberSeparator;
        [SerializeField] ScrollRect chatScroller;
        [SerializeField] GameObject chatWrapper;
        [SerializeField] BtnCommon btnPost;
        [SerializeField] InputField inputField;
        [SerializeField] ChatPiece chatPiecePref;
        [SerializeField] MyChatPiece myChatPiecePref;
        [SerializeField] int chatPieceWidth;
        [SerializeField] GameObject processing;

        private Noroshi.GuildChat.WebApiRequester webApiRequester = new Noroshi.GuildChat.WebApiRequester();
        private bool isBeginner;
        private bool isSetInitial = false;
        private uint currentMessageID;
        private uint currentCreatedAt;
        private SyncGuildChat syncGuildChat;
        private Noroshi.Core.WebApi.Response.Guild.GuildChatMessage[] initialMessage;
        private List<Noroshi.Core.WebApi.Response.Guild.GuildChatMessage> viewGuildChatMessages = new List<Noroshi.Core.WebApi.Response.Guild.GuildChatMessage>();
        private Noroshi.Core.WebApi.Response.Guild.Guild guildData;

        private readonly uint INTERVAL_TIME = 60u; // サーバサイドのタイムサーバの誤差を補完する,

        private void ViewComment(Noroshi.Core.WebApi.Response.Guild.GuildChatMessage[] messageLists) {
            
            messageLists = messageLists.OrderBy(o => o.ID).ToArray();

            for(int i = 0; i < messageLists.Length; i++) {

                if (0 < viewGuildChatMessages.Where(vc => vc.ID == messageLists[i].ID).Count()) {
                    continue; // 既読コメントを再描画しないよう飛ばす.
                }

                if(messageLists[i].OtherPlayerStatus.ID == PlayerInfo.Instance.GetPlayerStatus().PlayerID) {
                    var piece = Instantiate(myChatPiecePref);
                    piece.transform.SetParent(chatWrapper.transform);
                    piece.transform.localScale = new Vector2(-1, 1);
                    piece.SetComment(messageLists[i], chatPieceWidth);
                } else {
                    var piece = Instantiate(chatPiecePref);
                    piece.transform.SetParent(chatWrapper.transform);
                    piece.transform.localScale = Vector3.one;
                    piece.SetComment(messageLists[i]);
                }

                // ループの最後がチャットの端であり、次回サーバからの取得の際の起点の認識.
                //if(i == 0) {
                    currentMessageID = messageLists[i].ID;
                    currentCreatedAt = messageLists[i].CreatedAt;
                //}

                // 描画済みのコメントを保持.
                viewGuildChatMessages.Add(messageLists[i]);
            }

            TweenNull.Add(gameObject, 0.1f).Then(() => {
                chatScroller.verticalNormalizedPosition = 0;
            });
        }

        private void PostComment() {
            var text = inputField.text;
            if(text == "") {return;}
            processing.SetActive(true);
            btnPost.SetEnable(false);
            if (isBeginner) {
                webApiRequester.CreateBeginnerGuildMessage(text).Do(res => {
                    /*
                    TweenNull.Add(gameObject, 0.8f).Then(() => {
                        webApiRequester.GetNewBeginnerGuildMessage(currentMessageID, currentCreatedAt).Do(data => {
                            ViewComment(data.Messages);
                            inputField .text = "";
                            btnPost.SetEnable(true);
                            processing.SetActive(false);
                        }).Subscribe();
                    });
                    */
                }).Subscribe(_ => syncGuildChat.SyncBeginnerGuildMessage());
            } else {
                webApiRequester.CreateNormalGuildMessage(text).Do(res => {
                    /*
                    TweenNull.Add(gameObject, 0.8f).Then(() => {
                        webApiRequester.GetNewNormalGuildMessage(currentMessageID, currentCreatedAt).Do(data => {
                            ViewComment(data.Messages);
                            inputField .text = "";
                            btnPost.SetEnable(true);
                            processing.SetActive(false);
                        }).Subscribe();
                    });
                    */
                }).Subscribe(_ => syncGuildChat.SyncNormalGuildMessage());
            }
        }

        public void GetNewBeginnerGuildMessageViewComment()
        {
            TweenNull.Add(gameObject, 0.8f).Then(() => {
                webApiRequester.GetNewBeginnerGuildMessage((currentCreatedAt - INTERVAL_TIME)).Do(data => {
                    ViewComment(data.Messages);
                    inputField .text = "";
                    btnPost.SetEnable(true);
                    processing.SetActive(false);
                }).Subscribe();
            });
        }

        public void GetNewNormalGuildMessageViewComment()
        {
            TweenNull.Add(gameObject, 0.8f).Then(() => {
                webApiRequester.GetNewNormalGuildMessage((currentCreatedAt - INTERVAL_TIME)).Do(data => {
                    ViewComment(data.Messages);
                    inputField .text = "";
                    btnPost.SetEnable(true);
                    processing.SetActive(false);
                }).Subscribe();
            });
        }

        public void Init(Noroshi.Core.WebApi.Response.Guild.Guild guildDataIn) {
            guildData = guildDataIn;
            isBeginner = guildData.Category == Noroshi.Core.Game.Guild.GuildCategory.Beginner;
            if(isBeginner) {
                txtGuildName.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.BeginnerGuildName");
                memberSeparator.SetActive(false);
                txtMaxMemberNum.gameObject.SetActive(false);
                webApiRequester.GetBeginnerGuildMessage().Do(data => {
                    initialMessage = data.Messages;
                }).Subscribe();
            } else {
                txtGuildName.text = guildData.Name;
                txtMaxMemberNum.text = guildData.MaxMemberNum.ToString();
                webApiRequester.GetNormalGuildMessage().Do(data => {
                    initialMessage = data.Messages;
                }).Subscribe();
            }

            btnPost.OnClickedBtn.Subscribe(_ => {
                PostComment();
            });

            txtCurrentMemberNum.text = guildData.MemberNum.ToString();

            // Photonサーバを利用してギルドメンバーとチャット投稿を同期.
            connectedPhoton();
        }

        public void UpdateGuildName(string name) {
            txtGuildName.text = name;
        }

        public void SetInitialView() {
            if(isSetInitial) {return;}
            isSetInitial = true;
            ViewComment(initialMessage);
        }

        private void connectedPhoton()
        {
            // Photonサーバと接続されていなければ接続する.
            if (!syncGuildChat)
            {
                syncGuildChat = gameObject.AddComponent<SyncGuildChat>();
            }

            syncGuildChat.Init(guildData.ID, isBeginner, new UnityAction[]{GetNewBeginnerGuildMessageViewComment, GetNewNormalGuildMessageViewComment});
        }

        void OnDestroy()
        {
            // ユーザのページ遷移の挙動　不要になるとPhoton切断.
            if(syncGuildChat) syncGuildChat.Disconnect();
        }

        void OnApplicationPause(bool pauseStatus)
        {
            // サスペンド/レジューム対策. (アプリ再活性化後、ユーザが発言するまでメンバーの発言を同期できないと不具合報告有).
            if (pauseStatus)
            {
                if (syncGuildChat) syncGuildChat.Disconnect(); // 非活性状態に入る場合　Photon切断.
            }
            else
            {
                if (syncGuildChat) connectedPhoton(); // 活性状態になる場合 Photon再接続.
            }
        }

    }
}
