﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Collections;
using Photon.Flaggs;
using UniRx;
using Noroshi.UI;

namespace Photon.Flaggs
{
    [RequireComponent(typeof(PhotonView))]
    public class SyncGuildChat : Photon.MonoBehaviour
    {

        readonly string ROOM_NAME_DEFAULT = "GuildRoom";    // 初心者ギルドルーム名.
        readonly string ROOM_NAME_BEGINNER = "Beginner";    // 通常ギルドルーム名.

        bool isBeginnerRoom;
        string guildRoomName;
        uint guildID;

        static UnityAction _syncBeginnerGuild;
        static UnityAction _syncNormalGuild;

        string roomName
        {
            get
            {
                if (PhotonNetwork.room == null) return null;
                return PhotonNetwork.room.name;
            }
        }

        /// <summary>
        /// 起動
        /// </summary>
        public void Init(uint guildId, bool isBeginner, UnityAction[] syncGuildChatActions)
        {
            guildID = guildId;
            isBeginnerRoom = isBeginner;

            if(isBeginnerRoom) {
                guildRoomName = string.Format("{0}{1}", ROOM_NAME_DEFAULT, ROOM_NAME_BEGINNER);
            } else {
                guildRoomName = string.Format("{0}{1}", ROOM_NAME_DEFAULT, guildID);
            }

            _syncBeginnerGuild = syncGuildChatActions[0];
            _syncNormalGuild = syncGuildChatActions[1];

            var observedComponents = new List<Component>(){ this };
            this.GetComponent<PhotonView>().ObservedComponents = observedComponents;
            this.GetComponent<PhotonView>().viewID = observedComponents.Count;

            // Photon AppId セット（サーバサイドで開発用IDと本番IDを切り替える）.
            PhotonNetwork.PhotonServerSettings.AppID = Noroshi.GlobalContainer.MasterManager.ConfigMaster.GetPhotonAppID();
            PhotonNetwork.autoJoinLobby = false;

            //ConnectPhoton(gameVersion);// Photonサーバに接続 接続後に行う処理は、コールバック関数 OnJoinedLobby()に記載.
            Observable.FromCoroutine<string>(observer => connectAndJoinCoroutine(observer))
                .Retry(3) //3回までリトライ.
                .Subscribe(x => {/*Debug.Log(String.Format("[{0}] {1}\n", DateTime.Now, x))*/}).AddTo(this.gameObject); //指定GameObjectの寿命に紐付ける
        }

        /// <summary>
        /// 切断.
        /// </summary>
        public void Disconnect()
        {
            if (PhotonNetwork.connected){
                PhotonNetwork.Disconnect();
            }
        }

        IEnumerator connectAndJoinCoroutine(IObserver<string> observer)
        {
            //-----------PhotonCloudへ接続------------
            observer.OnNext("ConnectToServer...");

            //ロビーへの接続結果を通知するストリームを作成

            //接続開始(接続済みならば再利用).
            if (!PhotonNetwork.connected)
            {
                var loginStream = connectUsingSettingsObservable();
                loginStream.Connect();

                var gameVersion = "v1.0";
                PhotonNetwork.ConnectUsingSettings (gameVersion);

                //接続完了するのを待つ.
                var connectedToMaster = default(object);;
                yield return loginStream.StartAsCoroutine(x => connectedToMaster = x, ex => { });

                //接続結果が失敗だった場合は処理終了
                if (connectedToMaster is DisconnectCause)
                {
                    observer.OnNext(((DisconnectCause) connectedToMaster).ToString());
                    observer.OnNext("ConnectionToMasterFailed");
                    observer.OnError(null);
                    yield break;
                }
            }
            observer.OnNext("ConnectToMasterSuccess = "+PhotonNetwork.connected);

            //-----------ここから部屋の接続------------

            if (roomName != null && roomName != guildRoomName)
            {

                var leaveRoom = leftRoomObservable();
                leaveRoom.Connect();
                
                // 部屋を移動する.
                PhotonNetwork.LeaveRoom();
                var isLeaveRoom = false;
                yield return leaveRoom.StartAsCoroutine(x => isLeaveRoom = x, ex => { });
                if (!isLeaveRoom) {
                    observer.OnNext("LeaveRoomFailed");
                    observer.OnError(null);
                    yield break;
                }

            }

            observer.OnNext("JoinGuildRoom... " + guildRoomName);
            if (roomName == null)
            {

                //部屋への接続を待機するストリーム.
                var joinRoomStream = joinOrCreateRoomObservable ();
                joinRoomStream.Connect ();

                //ギルドルームに入室　部屋がなければつくる.
                var roomOptions = new RoomOptions();
                roomOptions.maxPlayers = 0;    // Max number of players that can be in the room at any time. 0 means "no limit".
                PhotonNetwork.JoinOrCreateRoom (guildRoomName, roomOptions, TypedLobby.Default);

                //部屋の参加を待ち受ける
                var isJoined = false;
                yield return joinRoomStream.StartAsCoroutine (x => isJoined = x);

                if (isJoined) {
                    //参加成功したら終了
                    observer.OnNext ("JoinGuildRoomSuccess");
                    observer.OnCompleted();
                    yield break;
                } else {
                    observer.OnNext("JoinGuildRoomFailed");
                    observer.OnError(null);
                    yield break;
                }
            }
            // 既に部屋に入っている　入り直す必要もない.
            observer.OnCompleted();
        }


        public void SyncBeginnerGuildMessage()
        {
            if (PhotonNetwork.connected && roomName != null) {
                photonView.RPC ("SyncBeginnerGuildMessageRpc", PhotonTargets.All, null);
            } else {
                Observable.FromCoroutine<string>(observer => connectAndJoinCoroutine(observer))
                .Retry(3) //3回までリトライ
                .Subscribe(
                x => {},
                ex => Debug.LogError("ExceptionSyncBeginnerGuildMessage : " + ex),
                () => SyncBeginnerGuildMessage()).AddTo(this.gameObject); // 接続に成功したら再挑戦.
            }
        }

        public void SyncNormalGuildMessage()
        {
            if (PhotonNetwork.connected && roomName != null) {
                photonView.RPC("SyncNormalGuildMessageRpc", PhotonTargets.All, null);
            } else {
                Observable.FromCoroutine<string>(observer => connectAndJoinCoroutine(observer))
                .Retry(3) //3回までリトライ
                .Subscribe(
                x => {},
                ex => Debug.LogError("SyncNormalGuildMessage : " + ex),
                () => SyncNormalGuildMessage()).AddTo(this.gameObject); // 接続に成功したら再挑戦.
            }
        }

        /// <summary>
        // 初心者ギルドのチャットを最新状態に同期する.
        /// </summary>
        [PunRPC]
        public void SyncBeginnerGuildMessageRpc()
        {
            _syncBeginnerGuild();
        }

        /// <summary>
        // 通常ギルドのチャットを最新状態に同期する.
        /// </summary>
        [PunRPC]
        public void SyncNormalGuildMessageRpc()
        {
            _syncNormalGuild();
        }


        /// <summary>
        /// ConnectUsingSettingsの結果を監視するストリーム.
        /// </summary>
        IConnectableObservable<object> connectUsingSettingsObservable()
        {
            return Observable.Merge(
                this.onConnectedToMasterAsObservable.Cast(default(object)),
                this.onConnectionFailAsObservable.Cast(default(object)),
                this.onFailedToConnectToPhotonAsObservable.Cast(default(object)))
                .FirstOrDefault() //OnCompletedを発火させるため
                .PublishLast();   //PhotonNetwork.Connectを呼び出すより前にストリームを稼働させるため
        }

        /// <summary>
        /// JoinOrCreateRoomを監視するストリーム
        /// </summary>
        IConnectableObservable<bool> joinOrCreateRoomObservable()
        {
            return Observable.Merge(
                this.onJoinedRoomAsObservable.Select(_ => true),
                this.onPhotonJoinRoomFailedAsObservable.Select(_=>false))
                .FirstOrDefault()
                .PublishLast();
        }


        /// <summary>
        /// 退室完了を監視するストリーム
        /// </summary>
        IConnectableObservable<bool> leftRoomObservable()
        {
            return this.onLeftRoomAsObservable.Select(_ => true)
                .FirstOrDefault()
                .PublishLast();
        }

        /// <summary>
        /// サーバーが使用可能であるとき、クライアントが認証前に呼び出されます。
        /// </summary>
        IObservable<Unit> onConnectedToPhotonAsObservable{ get { return onConnectedToPhoton ?? (onConnectedToPhoton = new Subject<Unit>()); } }
        Subject<Unit> onConnectedToPhoton;
        void OnConnectedToPhoton(){ /*Debug.Log("OnConnectedToPhoton");*/ if (onConnectedToPhoton != null) onConnectedToMaster.OnNext(Unit.Default); }

        /// <summary>
        /// Photon接続系
        // Photon接続に成功したら通知する.
        /// </summary>
        IObservable<Unit> onConnectedToMasterAsObservable{ get { return onConnectedToMaster ?? (onConnectedToMaster = new Subject<Unit>()); } }
        Subject<Unit> onConnectedToMaster;
        public void OnConnectedToMaster() { /*Debug.Log("OnConnectedToMaster");*/ if (onConnectedToMaster != null) onConnectedToMaster.OnNext(Unit.Default); }
  
        /// <summary>
        // Photon接続に失敗したら通知する(Invalid AppId/Network issues).
        /// </summary>
        IObservable<Unit> onConnectionFailAsObservable{ get { return onConnectionFailSubject ?? (onConnectionFailSubject = new Subject<Unit>()); } }
        Subject<Unit> onConnectionFailSubject = new Subject<Unit>();
        public void OnConnectionFail() { /*Debug.Log("OnConnectionFail");*/ onConnectionFailSubject.OnNext(Unit.Default); }

        /// <summary>
        // Photon接続に失敗したら通知する(Invalid region).
        /// </summary>
        public IObservable<DisconnectCause> onFailedToConnectToPhotonAsObservable{ get{ return onFailedToConnectToPhoton ?? (onFailedToConnectToPhoton = new Subject<DisconnectCause>());}}
        Subject<DisconnectCause> onFailedToConnectToPhoton;
        void OnFailedToConnectToPhoton(DisconnectCause cause){ /*Debug.Log("OnFailedToConnectToPhoton");*/ onFailedToConnectToPhoton.OnNext(cause);}

        /// <summary>
        // ルームに入室したら通知する.
        /// </summary>
        IObservable<bool> onJoinedRoomAsObservable { get { return onJoinedRoomSubject.AsObservable(); } }
        Subject<bool> onJoinedRoomSubject = new Subject<bool>();
        public void OnJoinedRoom() { /*Debug.Log("OnJoinedRoom");*/ onJoinedRoomSubject.OnNext (true); }

        /// <summary>
        // ルームの入室に失敗したら通知する.
        /// </summary>
        IObservable<bool> onPhotonJoinRoomFailedAsObservable { get { return onJoinedRoomSubject.AsObservable(); } }
        Subject<bool> onPhotonJoinRoomFaileSubject = new Subject<bool>();
        public void OnPhotonJoinRoomFailed() { /*Debug.Log("OnPhotonJoinRoomFailed");*/ onPhotonJoinRoomFaileSubject.OnNext (true); }

        /// <summary>
        // ルームから退室したら呼ばれる.
        /// </summary>
        IObservable<bool> onLeftRoomAsObservable{ get { return onLeftRoomSubject.AsObservable(); } }
        Subject<bool> onLeftRoomSubject = new Subject<bool>();
        public void OnLeftRoom() { /*Debug.Log("OnLeftRoom");*/ onLeftRoomSubject.OnNext (true); }

    }
}
