﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class GuildChatController : MonoBehaviour {
        [SerializeField] GuildChatContainer chatContainer;
        [SerializeField] BtnCommon btnChat;
        [SerializeField] BtnCommon btnCloseChat;
        [SerializeField] GameObject chatScroller;
        [SerializeField] GameObject inputFieldContainer;
        [SerializeField] Canvas canvas;
        [SerializeField] GameObject overlay;

        private float inputFieldWith;
        private float chatScrollerHeight;
        private float offset = 64;
        private bool isChatOpen = false;

        public void Init(Noroshi.Core.WebApi.Response.Guild.Guild guildData) {
            inputFieldWith = inputFieldContainer.GetComponent<RectTransform>().sizeDelta.x;
            chatScrollerHeight = chatScroller.GetComponent<RectTransform>().sizeDelta.y;
            chatContainer.Init(guildData);
            btnChat.OnClickedBtn.Subscribe(_ => {
                if(isChatOpen) {
                    CloseChat();
                } else {
                    OpenChat();
                }
            });
            btnChat.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnCloseChat.OnClickedBtn.Subscribe(_ => {
                CloseChat();
            });
            btnCloseChat.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isChatOpen) {return;}
                CloseChat();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void OpenChat() {
            canvas.sortingOrder = 30;
            overlay.SetActive(true);
            chatContainer.SetInitialView();
            TweenX.Add(inputFieldContainer, 0.12f, -offset).Then(() => {
                TweenY.Add(chatScroller, 0.1f, chatScrollerHeight + offset).EaseInCubic().Then(() => {
                    isChatOpen = true;
                });
            });
            BackButtonController.Instance.IsModalOpen(true);
        }

        private void CloseChat() {
            TweenY.Add(chatScroller, 0.1f, -offset).Then(() => {
                TweenX.Add(inputFieldContainer, 0.12f, -inputFieldWith - offset).EaseInCubic().Then(() => {
                    isChatOpen = false;
                    canvas.sortingOrder = 5;
                    overlay.SetActive(false);
                    BackButtonController.Instance.IsModalOpen(false);
                });
            });
        }
    }
}
