﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UniRx;
using DG.Tweening;
using Noroshi.Core.Game.Sound;

namespace Noroshi.UI {
    public class SoundController : MonoBehaviour {
        public class BGMKeys {
            public const uint MAIN = SoundConstant.SYSTEM_BGM_MAIN_SOUND_ID;
            public const uint CHARACTER_LIST = SoundConstant.SYSTEM_BGM_CHARACTER_SOUND_ID;
            public const uint GACHA = SoundConstant.SYSTEM_BGM_GACHA_SOUND_ID;
            public const uint TOMB = SoundConstant.SYSTEM_BGM_TOMB_SOUND_ID;
            public const uint TENSION = SoundConstant.SYSTEM_BGM_TENSION_SOUND_ID;
            public const uint COCOPIPI = SoundConstant.SYSTEM_BGM_COCOPIPI_SOUND_ID;
            public const uint ENCOUNTER = SoundConstant.SYSTEM_BGM_ENCOUNTER_SOUND_ID;
        }

        public class SEKeys {
            public const uint DECIDE = SoundConstant.SYSTEM_SE_DECIDE_SOUND_ID;
            public const uint CANCEL = SoundConstant.SYSTEM_SE_CANCEL_SOUND_ID;
            public const uint SELECT = SoundConstant.SYSTEM_SE_SELECT_SOUND_ID;
            public const uint FLICK = SoundConstant.SYSTEM_SE_FLICK_SOUND_ID;
            public const uint GET = SoundConstant.SYSTEM_SE_GET_SOUND_ID;
            public const uint ERROR = SoundConstant.SYSTEM_SE_ERROR_SOUND_ID;
            public const uint MENU = SoundConstant.SYSTEM_SE_MENU_SOUND_ID;
            public const uint PAY = SoundConstant.SYSTEM_SE_PAY_SOUND_ID;
            public const uint LEVEL_UP = SoundConstant.SYSTEM_SE_LEVEL_UP_SOUND_ID;
            public const uint EQUIP = SoundConstant.SYSTEM_SE_EQUIP_SOUND_ID;
            public const uint EVOLUTION = SoundConstant.SYSTEM_SE_EVOLUTION_SOUND_ID;
            public const uint UPGRADE = SoundConstant.SYSTEM_SE_UPGRADE_SOUND_ID;
            public const uint STATUS_UP = SoundConstant.SYSTEM_SE_STATUS_UP_SOUND_ID;
        }

        static SoundController instance;

        public static SoundController Instance {
            get {
                if(instance == null) {
                    instance = (SoundController)FindObjectOfType(typeof(SoundController));
                }
                return instance;
            }
        }

        uint currentBGMID = 0;
        float bgmVolume = 1;
        bool isBGMOn = true;
        bool isSEOn = true;

        void Awake() {
            GameObject[] obj = GameObject.FindGameObjectsWithTag("SoundController");
            if(obj.Length > 1) {
                DestroyImmediate(gameObject);
            } else {
                DontDestroyOnLoad(gameObject);
            }
        }

        void OnLevelWasLoaded(int level) {
            if(SceneManager.GetActiveScene().name == Constant.SCENE_BATTLE) {
                DestroyImmediate(gameObject);
            }
        }

        public void LoadSetting() {
            if(PlayerPrefs.GetInt(SaveKeys.IsBGMOn) == 0) {
                isBGMOn = true;
                GlobalContainer.SoundManager.SetVolume(SoundType.Bgm, 1);
            } else {
                isBGMOn = false;
                GlobalContainer.SoundManager.SetVolume(SoundType.Bgm, 0);
            }
            if(PlayerPrefs.GetInt(SaveKeys.IsSEOn) == 0) {
                isSEOn = true;
                GlobalContainer.SoundManager.SetVolume(SoundType.SE, 1);
            } else {
                isSEOn = false;
                GlobalContainer.SoundManager.SetVolume(SoundType.SE, 0);
            }
            if(PlayerPrefs.GetInt(SaveKeys.IsVoiceOn) == 0) {
                GlobalContainer.SoundManager.SetVolume(SoundType.Voice, 1);
            } else {
                GlobalContainer.SoundManager.SetVolume(SoundType.Voice, 0);
            }
        }

        public void SetBGMEnable(bool isEnable) {
            isBGMOn = isEnable;
            if(!isEnable) {StopBGM();}
        }

        public void SetSEEnable(bool isEnable) {
            isSEOn = isEnable;
        }

        public void PlayBGM(uint soundID) {
            if(!isBGMOn || soundID == currentBGMID) {return;}
            currentBGMID = soundID;
            GlobalContainer.SoundManager.Play(soundID).Subscribe(success => {
                if(success) {

                } else {

                }
            });
        }

        public void StopBGM() {
            if(currentBGMID < 1) {return;}
            GlobalContainer.SoundManager.Stop(currentBGMID);
            currentBGMID = 0;
        }

        public void FadeOut() {
            DOTween.To(() => bgmVolume, (a) => bgmVolume = a, 0, 3.0f)
                .SetEase(Ease.InCirc)
                .OnUpdate(() => {ChangeVolume(bgmVolume);})
                .OnComplete(() => {
                    bgmVolume = 1;
                    ChangeVolume(bgmVolume);
                });
        }

        public void ChangeVolume(float value) {
            GlobalContainer.SoundManager.SetVolume(SoundType.Bgm, value);
        }

        public void PlaySE(uint soundID) {
            if(!isSEOn) {return;}
            GlobalContainer.SoundManager.Play(soundID).Subscribe();
        }

        public void StopSE(uint soundID){
            GlobalContainer.SoundManager.Stop(soundID);
        }

        public void ChangeSetting(SoundType type, bool isOn) {
            var volume = isOn ? 1 : 0;
            if(type == SoundType.Bgm) {
                isBGMOn = isOn;
                GlobalContainer.SoundManager.SetVolume(SoundType.Bgm, volume);
            } else if(type == SoundType.SE) {
                isSEOn = isOn;
                GlobalContainer.SoundManager.SetVolume(SoundType.SE, volume);
            } else if(type == SoundType.Voice) {
                GlobalContainer.SoundManager.SetVolume(SoundType.Voice, volume);
            }
        }
    }
}
