﻿using System;
using System.IO;
using UnityEngine;
using LitJson;
using Flaggs.Cache;
using Flaggs.Security;

namespace Noroshi.Cache
{
    /// <summary>
    /// ローカルストレージを扱うクラス。制約なく使いたいので PlayerPrefs ではなく自前でファイル管理。
    /// ファイル保存先ディレクトリはコンストラクタへの引数で確定するが、下記。
    /// {UnityEngine.Application.temporaryCachePath,UnityEngine.Application.persistentDataPath}/{PlayerID}/
    /// </summary>
    public class LocalStorage : AbstractFileCache
    {
        /// <summary>
        /// ディレクトリパスを取得。
        /// </summary>
        /// <param name="playerId">プレイヤー ID。</param>
        /// <param name="temporary">一時的なものかフラグ。</param>
        static string _getDirectoryPath(uint playerId, bool temporary)
        {
            return string.Format("{0}/{1}/", temporary ? UnityEngine.Application.temporaryCachePath : UnityEngine.Application.persistentDataPath, playerId);
        }


        readonly uint _playerId;

        /// <summary>
        /// Initializes a new instance of the <see cref="Noroshi.Cache.LocalStorage"/> class.
        /// </summary>
        /// <param name="playerId">プレイヤー ID。</param>
        /// <param name="temporary">一時的なものかフラグ。</param>
        public LocalStorage(uint playerId, bool temporary = true)
            : base(_getDirectoryPath(playerId, temporary), true)
        {
            if (playerId == 0) throw new InvalidOperationException();
            var directoryPath =_getDirectoryPath(playerId, temporary);
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            JsonMapper.RegisterImporter<double, float>(input => Convert.ToSingle(input));
            JsonMapper.RegisterExporter<float>((obj, writer) => writer.Write(Convert.ToDouble(obj)));
        }
        protected override DateTime _getCurrentDateTime()
        {
            // TODO : GlobalContainer
            return DateTime.Now;
        }

        protected override ValueWrapper<TValue> _readAndDeserialize<TValue>(Stream stream, string key)
        {
            // 復号化。
            var cryptography = new Cryptography();
            return JsonMapper.ToObject<ValueWrapper<TValue>>(cryptography.Decrypt(stream, _toString));
        }

        protected override void _serializeAndWrite<TValue>(Stream stream, string key, ValueWrapper<TValue> wrapper)
        {
            // 暗号化。
            var cryptography = new Cryptography();
            cryptography.Encrypt(JsonMapper.ToJson(wrapper), _toString, stream);
        }

        // 難読化・・・。
        string _toString
        {
            get { return "2016-01-01 00-00-00 " + _playerId.ToString(); }
        }
    }
}
