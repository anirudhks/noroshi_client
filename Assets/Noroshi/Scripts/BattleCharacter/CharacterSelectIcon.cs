using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using UniRx;
using Noroshi.Game;

namespace Noroshi.UI {
    public class CharacterSelectIcon : BtnCommon {
        [SerializeField] Text txtInfo;
        [SerializeField] Text txtInfoHeading;
        [SerializeField] GameObject iconSelected;
        [SerializeField] GameObject iconRequire;
        [SerializeField] GameObject iconCPU;
        [SerializeField] GameObject iconDisabled;
        [SerializeField] GameObject iconBig;
        [SerializeField] Image imgCharacter;
        [SerializeField] Image imgCharacterFrame;
        [SerializeField] Sprite[] imgFrameList;
        [SerializeField] GameObject[] evolutionStar;
        [SerializeField] GameObject continuingStatusContainer;
        [SerializeField] GameObject hpBar;
        [SerializeField] GameObject spBar;
        [SerializeField] float barWidth;

        public Subject<uint> OnClickedIcon = new Subject<uint>();
        public Subject<bool> OnShowRequire = new Subject<bool>();

        public uint playerCharacterID;
        public int position;
        public int lv;
        public int hp;
        public int power;
        public bool isDeca;
        public bool isDisabled = false;
        public Noroshi.Core.Game.Character.CharacterTagSet tagSet;

        private float hpBarWidth;
        private float spBarWidth;
        private bool isActive = true;
        private bool isRequire = false;

        public void SetInfo(CharacterStatus status, uint pcID) {
            imgCharacter.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite>(status.CharacterID, string.Format("thumb_{0}", status.SkinLevel));
            id = (int)status.CharacterID;
            playerCharacterID = pcID;
            position = (int)status.Position;
            isDeca = status.TagSet.IsDeca;
            tagSet = status.TagSet;
            if(isDeca) {iconBig.SetActive(true);}

            UpdateStatus(status);
        }

        public void UpdateStatus(CharacterStatus status) {
            lv = status.Level;
            hp = (int)status.MaxHP;
            power = (int)(status.PhysicalAttack + status.MagicPower + status.PhysicalCrit + status.MagicCrit);
            imgCharacterFrame.sprite = imgFrameList[status.PromotionLevel - 1];
            SetInfoTxt(0);
            for(int i = 0; i < status.EvolutionLevel; i++) {
                evolutionStar[i].SetActive(true);
            }
            if(continuingStatusContainer != null) {
                continuingStatusContainer.SetActive(false);
            }
        }

        public void SetInfoTxt(int type) {
            var gclm = GlobalContainer.LocalizationManager;
            switch(type) {
                case 0:
                    txtInfo.text = lv.ToString();
                    txtInfoHeading.text = gclm.GetText("UI.Heading.Level");
                    break;
                case 1:
                    txtInfo.text = hp.ToString();
                    txtInfoHeading.text = gclm.GetText("UI.Heading.HP");
                    break;
                case 2:
                    txtInfo.text = power.ToString();
                    txtInfoHeading.text = gclm.GetText("UI.Heading.Power");
                    break;
                default:
                    break;
            }
        }

        public void SetCPU(uint characterID) {
            var chara = GlobalContainer.MasterManager.CharacterMaster.Get(characterID);
            imgCharacter.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite>(characterID, "thumb_1");
            txtInfo.gameObject.SetActive(false);
            txtInfoHeading.gameObject.SetActive(false);
            imgCharacterFrame.sprite = imgFrameList[0];
            position = (int)chara.Position;
            iconCPU.SetActive(true);
        }

        public void SetRequire() {
            isRequire = true;
            iconRequire.SetActive(true);
        }

        public void SetContinuingStatus(float hpRatio, float spRatio) {
            var hpX = barWidth - barWidth * hpRatio;
            var spX = barWidth - barWidth * spRatio;
            if(hpRatio == 0) {
                isActive = false;
                TweenC.Add(gameObject, 0.01f, new Color(0.3f, 0.3f, 0.3f));
            } else {
                isActive = true;
                TweenC.Add(gameObject, 0.01f, new Color(1, 1, 1));
            }
            hpBar.transform.localPosition = new Vector3(-hpX, 0, 0);
            spBar.transform.localPosition = new Vector3(-spX, 0, 0);
            continuingStatusContainer.SetActive(true);
        }

        public void SetDisable() {
            isDisabled = true;
            iconDisabled.SetActive(true);
        }

        public void OnSelect(bool isSelect) {
            if(isSelect) {
                iconSelected.SetActive(true);
            } else {
                iconSelected.SetActive(false);
                iconRequire.SetActive(false);
                iconDisabled.SetActive(false);
                isRequire = false;
                isDisabled = false;
            }
        }

        public override void OnPointerClick(PointerEventData ped) {
            if(isDisabled || !isActive) {return;}
            if(isRequire) {
                OnShowRequire.OnNext(true);
                return;
            }
            isActive = false;
            OnClickedIcon.OnNext(playerCharacterID);
            Observable.Timer(TimeSpan.FromSeconds(0.5f)).Subscribe(_ => {
                isActive = true;
            });
        }
    }
}
