﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using UniRx;
using Noroshi.Game;

namespace Noroshi.UI {
    public class CharacterSelectPanel : MonoBehaviour, IPointerClickHandler {
        [SerializeField] GameObject iconRequire;
        [SerializeField] GameObject iconCPU;
        [SerializeField] GameObject iconMercenary;

        public Subject<uint> OnClickedPanel = new Subject<uint>();
        public Subject<bool> OnShowRequire = new Subject<bool>();
        public Subject<bool> OnStartCreateCharacter = new Subject<bool>();
        public Subject<bool> OnEndCreateCharacter = new Subject<bool>();
        public Subject<bool> OnEndDeleteCharacter = new Subject<bool>();

        public uint characterID;
        public uint playerCharacterID;
        public uint orderPriority;
        public uint orderInLayer;
        public bool isDeca;

        private GameObject animChara = null;
        private SkeletonAnimation skeletonAnimation;
        private Spine.AnimationState state;
        private byte skinLevel = 0;
        private bool isActive = true;
        private bool isRequire = false;
        private float charaSize = 22;
        private Vector3 startPosition;

        private void CreateCharacter(uint characterID) {
            animChara = Instantiate<GameObject>(GlobalContainer.AssetBundleManager.LoadFromCharacterPrefab(characterID, "UICharacter"));
            animChara.transform.SetParent(transform);
            animChara.transform.localScale = new Vector2(-charaSize, charaSize);
            animChara.transform.localPosition = new Vector3(0, -100, 0);
            skeletonAnimation = animChara.GetComponent<SkeletonAnimation>();
            state = skeletonAnimation.state;
            SetCharacterView();
            OnEndCreateCharacter.OnNext(true);
        }

        private void SetCharacterView() {
            foreach(var atlas in skeletonAnimation.skeletonDataAsset.atlasAssets) {
                foreach(var m in atlas.materials) {
                    var shader = m.shader;
                    m.shader = null;
                    m.shader = shader;
                }
            }
            skeletonAnimation.skeletonDataAsset.Reset();
            SetCharacterSkin(skinLevel);
        }

        public void Init(CharacterStatus status, uint pcID, bool isInitialCreate = false) {
            transform.localScale = Vector3.one;
            transform.localPosition = startPosition = new Vector3(-750, 100, -50 + status.OrderInLayer);
            skinLevel = status.SkinLevel;
            characterID = status.CharacterID;
            playerCharacterID = pcID;
            orderPriority = status.OrderPriority;
            orderInLayer = status.OrderInLayer;
            isDeca = status.TagSet.IsDeca;
            if(isInitialCreate) {CreateCharacter(characterID);}
        }

        public void SetCharacterSkin(byte level) {
            if(animChara == null) {return;}
            skinLevel = level;
            if(skeletonAnimation.initialSkinName != "default") {
                skeletonAnimation.skeleton.SetSkin("step" + skinLevel);
                skeletonAnimation.skeleton.SetSlotsToSetupPose();
            }
        }

        public void SetCPU(uint id) {
            var chara = GlobalContainer.MasterManager.CharacterMaster.Get(id);
            animChara = Instantiate<GameObject>(GlobalContainer.AssetBundleManager.LoadFromCharacterPrefab(id, "UICharacter"));
            state = animChara.GetComponent<SkeletonAnimation>().state;
            animChara.transform.SetParent(transform);
            transform.localScale = Vector3.one;
            transform.localPosition = new Vector3(-750, 100, -50 + chara.OrderInLayer);
            animChara.transform.localScale = new Vector2(-charaSize, charaSize);
            animChara.transform.localPosition = new Vector3(0, -100, 0);
            characterID = id;
            orderPriority = chara.OrderPriority;
            orderInLayer = chara.OrderInLayer;
            iconCPU.SetActive(true);
        }

        public void OnPointerClick(PointerEventData ped) {
            if(!isActive) {return;}
            if(isRequire) {
                OnShowRequire.OnNext(true);
                return;
            }
            isActive = false;
            OnClickedPanel.OnNext(playerCharacterID);
            Observable.Timer(TimeSpan.FromSeconds(0.4f)).Subscribe(_ => {
                isActive = true;
            });
        }

        public void MoveCharacter(Vector2 position, bool isAnim = true) {
            var len = Vector2.Distance(transform.localPosition, position);
            var duration = isAnim ? len / 1000f + 0.125f : 0;
            gameObject.PauseTweens<TweenXY>();
            if(Mathf.Abs(len) < 1) {
                return;
            } else if(transform.localPosition.x - position.x > 0) {
                animChara.transform.localScale = new Vector2(charaSize, charaSize);
            } else {
                animChara.transform.localScale = new Vector2(-charaSize, charaSize);
            }

            if(isAnim) {
                state.SetAnimation(0, Constant.ANIM_RUN, true);
                TweenXY.Add(gameObject, duration, position).Then(() => {
                    state.SetAnimation(0, Constant.ANIM_IDLE, true);
                    animChara.transform.localScale = new Vector2(-charaSize, charaSize);
                });
            } else {
                var pos = new Vector3(position.x, position.y, transform.localPosition.z);
                state.SetAnimation(0, Constant.ANIM_IDLE, true);
                transform.localPosition = pos;
                animChara.transform.localScale = new Vector2(-charaSize, charaSize);
            }
        }

        public void AppearCharacter(Vector2 position, bool isAnim) {
            startPosition.y = (position.y - 10) / 2;
            transform.localPosition = startPosition;
            gameObject.SetActive(true);
            if(animChara == null) {
                OnStartCreateCharacter.OnNext(true);
                Observable.TimerFrame(1).Subscribe(_ => {
                    CreateCharacter(characterID);
                    MoveCharacter(position, isAnim);
                });
            } else {
                SetCharacterView();
                MoveCharacter(position, isAnim);
            }
        }

        public void DisappearCharacter(bool isDestroy) {
            var len = Vector2.Distance(transform.localPosition, startPosition);
            var duration = len / 1000f + 0.125f;
            state.SetAnimation(0, Constant.ANIM_RUN, true);
            animChara.transform.localScale = new Vector2(charaSize, charaSize);
            TweenXY.Add(gameObject, duration, startPosition).Then(() => {
                state.SetAnimation(0, Constant.ANIM_IDLE, true);
                animChara.transform.localScale = new Vector2(-charaSize, charaSize);
                InActiveCharacter();
                if(isDestroy) {
                    DestroyCharacter();
                }
            });
        }

        public void InActiveCharacter() {
            isRequire = false;
            iconRequire.SetActive(false);
            Destroy(GetComponent<TweenNull>());
            gameObject.SetActive(false);
        }

        public void DestroyCharacter() {
            var unloader = new Flaggs.Unity.Spine.SkeletonAnimationUnloader();

            unloader.UnloadTextureAndDestroy(animChara);
            animChara = null;
            skeletonAnimation = null;
            state = null;

            OnEndDeleteCharacter.OnNext(true);
        }

        public void SetRequire() {
            isRequire = true;
            iconRequire.SetActive(true);
        }

        public void SetMercenary() {
            iconMercenary.SetActive(true);
        }
    }
}
