using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UniRx;
using UniLinq;
using LitJson;
using Noroshi.Game;
using Noroshi.Core.Game.Battle;
using Noroshi.Core.WebApi.Response.Battle;

namespace Noroshi.UI {
    public class BattleCharacterSelect : MonoBehaviour {
        public class StartBattleData {
            public uint[] playerCharacterIDs;
            public uint? mercenaryID;
        }

        [SerializeField] Canvas canvas;
        [SerializeField] GameObject modal;
        [SerializeField] ScrollRect ownCharacterList;
        [SerializeField] ScrollRect mercenaryList;
        [SerializeField] GameObject enableIconListWrapper;
        [SerializeField] GameObject disableIconListWrapper;
        [SerializeField] GameObject characterPanelListWrapper;
        [SerializeField] GameObject mercenaryIconListWrapper;
        [SerializeField] CharacterSelectPanel selectPanel;
        [SerializeField] CharacterSelectIcon selectIcon;
        [SerializeField] MercenarySelectIcon mercenarySelectIcon;
        [SerializeField] BtnCommon btnClose;
        [SerializeField] BtnCommon btnOK;
        [SerializeField] BtnCommon btnStartBattle;
        [SerializeField] BtnCommon btnOpenFilter;
        [SerializeField] Text txtBtnOpenFilter;
        [SerializeField] GameObject filterContainer;
        [SerializeField] GameObject filterSelector;
        [SerializeField] GameObject filterBtnWrapper;
        [SerializeField] BtnCommon btnFilterBackground;
        [SerializeField] BtnCommon[] btnFilterList;
        [SerializeField] BtnCommon[] tabBtnList;
        [SerializeField] GameObject[] tabContentList;
        [SerializeField] BtnCommon btnSort;
        [SerializeField] Text txtBtnSort;
        [SerializeField] Text txtSelectedPower;
        [SerializeField] Text txtSelectedHp;
        [SerializeField] AlertModal decaCharaAlert;
        [SerializeField] AlertModal requireCharaAlert;
        [SerializeField] AlertModal mercenaryAlert;
        [SerializeField] AlertModal sameCharacterAlert;
        [SerializeField] GameObject noticeCharacterSelect;
        [SerializeField] Text txtBattleCharacterNum;
        [SerializeField] Text txtMaxBattleCharacterNum;
        [SerializeField] Text txtEnableJoinBattle;
        [SerializeField] Text txtRequireJoinBattle;
        [SerializeField] GameObject requireTextWrapper;
        [SerializeField] GameObject notSelectToMaxPanel;
        [SerializeField] BtnCommon btnCancelToSelect;
        [SerializeField] BtnCommon btnBattleAtNotSelectToMax;
        [SerializeField] GameObject processing;

        public static BattleCharacterSelect Instance;

        public Subject<StartBattleData> OnStartBattle = new Subject<StartBattleData>();
        public Subject<uint[]> OnClosePanel = new Subject<uint[]>();
        public Subject<CharacterStatus> OnReloadStatus = new Subject<CharacterStatus>();

        public bool isLoad = false;

        private List<CharacterSelectIcon> characterIconList = new List<CharacterSelectIcon>();
        private List<CharacterSelectPanel> characterPanelList = new List<CharacterSelectPanel>();
        private List<MercenarySelectIcon> mercenaryIconList = new List<MercenarySelectIcon>();
        private List<CharacterSelectPanel> mercenaryPanelList = new List<CharacterSelectPanel>();
        private List<CharacterSelectIcon> cpuIconList = new List<CharacterSelectIcon>();
        private List<CharacterSelectPanel> cpuPanelList = new List<CharacterSelectPanel>();
        private List<uint> battleCharacterIdList = new List<uint>();
        private uint? battleMercenaryID = null;
        private List<CharacterSelectPanel> selectedCharacterList = new List<CharacterSelectPanel>();
        private List<CharacterStatus> characterStatusList = new List<CharacterStatus>();
        private float filterPositionY = 9999;
        private int selectedPower = 0;
        private int selectedHp = 0;
        private int sortTypeLength = 3;
        private int createdCharacterNum = 0;
        private int initialCreateCharacterNum = 5;
        private int maxBattleCharacterNum = Noroshi.Core.Game.Battle.Constant.MAX_CHARACTER_NUM_PER_DECK;
        private int maxDecaCharacterNum = Noroshi.Core.Game.Battle.Constant.MAX_DECA_CHARACTER_NUM_PER_FORCE;
        private int crntDecaCharacterNum = 0;
        private int canSelectNum;
        private bool isFilterOpen = false;
        private bool isMercenaryOpen = false;
        private Noroshi.Character.CharacterVoiceHandler voiceHandler = new Noroshi.Character.CharacterVoiceHandler();

        private float[] selectedPositionXList = new float[] {100, 0, -100, -200, -300};
        private float[] selectedPositionYList = new float[] {-30, -10, 10, 30, 50};

        private void Awake() {
            GameObject[] obj = GameObject.FindGameObjectsWithTag("BattleCharacterSelect");
            if(obj.Length < 2 || UILoading.Instance.GetPreviousHistory() == Constant.SCENE_BATTLE) {
                Instance = this;
                canvas.worldCamera = Camera.main;
                DontDestroyOnLoad(gameObject);
                LoadCharacterList();
                LoadMercenaryList();
            } else {
                DestroyImmediate(gameObject);
            }
        }

        private void OnLevelWasLoaded(int level) {
            if(SceneManager.GetActiveScene().name == Constant.SCENE_BATTLE) {
                DestroyImmediate(gameObject);
            } else {
                canvas.worldCamera = Camera.main;
            }
        }

        private void Start() {
            txtMaxBattleCharacterNum.text = maxBattleCharacterNum.ToString();

            btnClose.OnClickedBtn.Subscribe(_ => {
                ClosePanel(true);
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnOK.OnClickedBtn.Subscribe(_ => {
                ClosePanel(false);
            });
            btnOK.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnStartBattle.OnClickedBtn.Subscribe(_ => {
                if(selectedCharacterList.Count < maxBattleCharacterNum && canSelectNum > selectedCharacterList.Count) {
                    notSelectToMaxPanel.SetActive(true);
                    TweenA.Add(notSelectToMaxPanel, 0.1f, 1).From(0);
                } else {
                    StartBattle();
                }
            });
            btnStartBattle.OnPlaySE.Subscribe(_ => {
                if(selectedCharacterList.Count < maxBattleCharacterNum && canSelectNum > selectedCharacterList.Count) {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.ERROR);
                } else {
                    SoundController.Instance.StopBGM();
                    SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
                }
            });

            foreach(var tab in tabBtnList) {
                tab.OnClickedBtn.Subscribe(index => {
                    SwitchTab(index);
                });
                tab.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnOpenFilter.OnClickedBtn.Subscribe(_ => {
                if(isFilterOpen) {
                    CloseFilter();
                } else {
                    OpenFilter();
                }
            });
            btnOpenFilter.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnFilterBackground.OnClickedBtn.Subscribe(_ => {
                CloseFilter();
            });

            foreach(var btn in btnFilterList) {
                btn.OnClickedBtn.Subscribe(id => {
                    FilterCharacterList(id);
                });
                btn.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnSort.OnClickedBtn.Subscribe(id => {
                SortCharacterList(id);
            });
            btnSort.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnBattleAtNotSelectToMax.OnClickedBtn.Subscribe(_ => {
                notSelectToMaxPanel.SetActive(false);
                StartBattle();
            });
            btnBattleAtNotSelectToMax.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.StopBGM();
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });

            btnCancelToSelect.OnClickedBtn.Subscribe(_ => {
                TweenA.Add(notSelectToMaxPanel, 0.1f, 0).Then(() => {
                    notSelectToMaxPanel.SetActive(false);
                });
            });
            btnCancelToSelect.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });
        }

        private void CreateCharacter(CharacterStatus characterStatus, uint pcID) {
            var icon = Instantiate(selectIcon);
            var panel = Instantiate(selectPanel);

            characterIconList.Add(icon);
            icon.SetInfo(characterStatus, pcID);
            icon.transform.SetParent(enableIconListWrapper.transform);
            icon.transform.localScale = Vector3.one;
            SetIconEvent(icon);

            characterPanelList.Add(panel);
            panel.transform.SetParent(characterPanelListWrapper.transform);
            panel.Init(characterStatus, pcID, characterPanelList.Count <= initialCreateCharacterNum);
            panel.InActiveCharacter();
            SetPanelEvent(panel, icon);
        }

        private void CreateMercenary(CharacterStatus characterStatus, uint pcID) {
            var icon = Instantiate(mercenarySelectIcon);
            var panel = Instantiate(selectPanel);
            var cost = characterStatus.Level * Noroshi.Core.Game.Guild.Constant.RENTAL_REWARD_GOLD_PER_CHARACTER_LEVEL;

            mercenaryIconList.Add(icon);
            icon.SetInfo(characterStatus, pcID);
            icon.SetMercenaryInfo("Player" + pcID, cost);
            icon.transform.SetParent(mercenaryIconListWrapper.transform);
            icon.transform.localScale = Vector3.one;
            SetMercenaryIconEvent(icon);

            mercenaryPanelList.Add(panel);
            panel.transform.SetParent(characterPanelListWrapper.transform);
            panel.Init(characterStatus, pcID);
            panel.InActiveCharacter();
            panel.SetMercenary();
            SetPanelEvent(panel, icon, true);
        }

        private void CreateCPUCharacter(uint characterID) {
            var icon = Instantiate(selectIcon);
            var panel = Instantiate(selectPanel);

            cpuIconList.Add(icon);
            icon.SetCPU(characterID);
            icon.transform.SetParent(enableIconListWrapper.transform);
            icon.transform.localScale = Vector3.one;

            cpuPanelList.Add(panel);
            selectedCharacterList.Add(panel);
            panel.transform.SetParent(characterPanelListWrapper.transform);
            panel.SetCPU(characterID);
        }

        private void SetIconEvent(CharacterSelectIcon icon) {
            icon.OnClickedIcon.Subscribe(playerCharacterID => {
                if(battleCharacterIdList.Contains(playerCharacterID)) {
                    icon.OnSelect(false);
                    if(icon.isDeca) {crntDecaCharacterNum--;}
                    RemoveSelectedCharacter(playerCharacterID);
                    SetSelectedStatus(-icon.power, -icon.hp);
                    MoveSelectedCharacter();
                    SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
                } else {
                    if(selectedCharacterList.Count >= maxBattleCharacterNum) {return;}
                    for(int i = 0, l = selectedCharacterList.Count; i < l; i++) {
                        if(selectedCharacterList[i].characterID == icon.id) {
                            sameCharacterAlert.OnOpen();
                            return;
                        }
                    }
                    if(icon.isDeca) {
                        if(crntDecaCharacterNum > maxDecaCharacterNum - 1) {
                            decaCharaAlert.OnOpen();
                            return;
                        } else {
                            crntDecaCharacterNum++;
                        }
                    }
                    AddSelectedCharacter(icon);
                    MoveSelectedCharacter();
                    voiceHandler.PlayUIVoice((uint)icon.id).Subscribe();
                }
            });
            icon.OnShowRequire.Subscribe(_ => {
                requireCharaAlert.OnOpen();
            });
        }

        private void SetMercenaryIconEvent(CharacterSelectIcon icon) {
            icon.OnClickedIcon.Subscribe(playerCharacterID => {
                if(battleMercenaryID == null) {
                    if(selectedCharacterList.Count >= maxBattleCharacterNum) {return;}
                    for(int i = 0, l = selectedCharacterList.Count; i < l; i++) {
                        if(selectedCharacterList[i].characterID == icon.id) {
                            sameCharacterAlert.OnOpen();
                            return;
                        }
                    }
                    if(icon.isDeca) {
                        if(crntDecaCharacterNum > maxDecaCharacterNum - 1) {
                            decaCharaAlert.OnOpen();
                            return;
                        } else {
                            crntDecaCharacterNum++;
                        }
                    }
                    AddSelectedCharacter(icon, true);
                    MoveSelectedCharacter();
                    voiceHandler.PlayUIVoice((uint)icon.id).Subscribe();
                } else {
                    if(battleMercenaryID == playerCharacterID) {
                        icon.OnSelect(false);
                        battleMercenaryID = null;
                        if(icon.isDeca) {crntDecaCharacterNum--;}
                        RemoveSelectedCharacter(playerCharacterID, true);
                        SetSelectedStatus(-icon.power, -icon.hp);
                        MoveSelectedCharacter();
                        SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
                    } else {
                        mercenaryAlert.OnOpen();
                        return;
                    }
                }
            });
        }

        private void SetPanelEvent(CharacterSelectPanel panel, CharacterSelectIcon icon, bool isMercenary = false) {
            panel.OnClickedPanel.Subscribe(playerCharacterID => {
                if((!isMercenary && battleCharacterIdList.Contains(playerCharacterID))
                    || (isMercenary && battleMercenaryID != null)
                ) {
                    icon.OnSelect(false);
                    if(icon.isDeca) {crntDecaCharacterNum--;}
                    if(isMercenary) {battleMercenaryID = null;}
                    RemoveSelectedCharacter(playerCharacterID);
                    SetSelectedStatus(-icon.power, -icon.hp);
                    MoveSelectedCharacter();
                    SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
                }
            });
            panel.OnShowRequire.Subscribe(_ => {
                requireCharaAlert.OnOpen();
            });
            panel.OnStartCreateCharacter.Subscribe(_ => {
                processing.SetActive(true);
            });
            panel.OnEndCreateCharacter.Subscribe(_ => {
                createdCharacterNum++;
                processing.SetActive(false);
            });
            panel.OnEndDeleteCharacter.Subscribe(_ => {
                createdCharacterNum--;
            });
        }

        private void FilterCharacterList(int n) {
            var gclm = GlobalContainer.LocalizationManager;
            foreach(var chara in characterIconList) {
                if(n == 0 || (n < 4 && n == chara.position) || (n == 4 && chara.isDeca)) {
                    chara.gameObject.SetActive(true);
                } else {
                    chara.gameObject.SetActive(false);
                }
            }
            foreach(var chara in mercenaryIconList) {
                if(n == 0 || (n < 4 && n == chara.position) || (n == 4 && chara.isDeca)) {
                    chara.gameObject.SetActive(true);
                } else {
                    chara.gameObject.SetActive(false);
                }
            }
            foreach(var chara in cpuIconList) {
                if(n == 0 || (n < 4 && n == chara.position) || (n == 4 && chara.isDeca)) {
                    chara.gameObject.SetActive(true);
                } else {
                    chara.gameObject.SetActive(false);
                }
            }
            for(int i = 0, l = btnFilterList.Length; i < l; i++) {
                if (i == n) {
                    btnFilterList[i].SetSelect(true);
                } else {
                    btnFilterList[i].SetSelect(false);
                }
            }
            filterSelector.transform.localPosition = btnFilterList[n].transform.localPosition;
            switch (n) {
                case 0: txtBtnOpenFilter.text = gclm.GetText("UI.Button.All"); break;
                case 1: txtBtnOpenFilter.text = gclm.GetText("UI.Button.Front"); break;
                case 2: txtBtnOpenFilter.text = gclm.GetText("UI.Button.Central"); break;
                case 3: txtBtnOpenFilter.text = gclm.GetText("UI.Button.Back"); break;
                case 4: txtBtnOpenFilter.text = gclm.GetText("UI.Button.Deca"); break;
                default: break;
            }
            CloseFilter();
        }

        private void OpenFilter() {
            isFilterOpen = true;
            filterContainer.SetActive(true);
            if(filterPositionY == 9999) {
                filterPositionY = filterBtnWrapper.transform.localPosition.y;
            }
            filterBtnWrapper.SetActive(true);
            TweenA.Add(btnFilterBackground.gameObject, 0.1f, 0.5f).From(0).EaseOutCubic();
            TweenA.Add(filterBtnWrapper, 0.2f, 1).From(0).EaseOutCubic();
            TweenY.Add(filterBtnWrapper, 0.2f, filterPositionY).From(filterPositionY + 40).EaseOutCubic();
        }

        private void CloseFilter() {
            isFilterOpen = false;
            TweenA.Add(btnFilterBackground.gameObject, 0.25f, 0);
            TweenY.Add(filterBtnWrapper, 0.25f, filterPositionY + 40).EaseOutCubic();
            TweenA.Add(filterBtnWrapper, 0.25f, 0).EaseOutCubic().Then(() => {
                filterBtnWrapper.SetActive(false);
                filterContainer.gameObject.SetActive(false);
            });
        }

        private void SortCharacterList(int type) {
            var gclm = GlobalContainer.LocalizationManager;
            type++;
            if (type > sortTypeLength - 1) {type = 0;}
            btnSort.id = type;

            ownCharacterList.gameObject.SetActive(false);
            mercenaryList.gameObject.SetActive(false);
            switch (type) {
                case 0:
                    txtBtnSort.text = gclm.GetText("UI.Heading.Level");
                    characterIconList = characterIconList.OrderBy(c => c.isDisabled)
                        .ThenByDescending(c => c.lv).ThenBy(c => c.id).ToList();
                    mercenaryIconList = mercenaryIconList.OrderBy(c => c.isDisabled)
                        .ThenByDescending(c => c.lv).ThenBy(c => c.id).ToList();
                    break;
                case 1:
                    txtBtnSort.text = gclm.GetText("UI.Heading.HP");
                    characterIconList = characterIconList.OrderBy(c => c.isDisabled)
                        .ThenByDescending(c => c.hp).ThenBy(c => c.id).ToList();
                    mercenaryIconList = mercenaryIconList.OrderBy(c => c.isDisabled)
                        .ThenByDescending(c => c.hp).ThenBy(c => c.id).ToList();
                    break;
                case 2:
                    txtBtnSort.text = gclm.GetText("UI.Heading.Power");
                    characterIconList = characterIconList.OrderBy(c => c.isDisabled)
                        .ThenByDescending(c => c.power).ThenBy(c => c.id).ToList();
                    mercenaryIconList = mercenaryIconList.OrderBy(c => c.isDisabled)
                        .ThenByDescending(c => c.power).ThenBy(c => c.id).ToList();
                    break;
                default:
                    break;
            }
            foreach(var chara in characterIconList) {
                chara.transform.SetAsLastSibling();
                chara.SetInfoTxt(type);
            }
            foreach(var mercenary in mercenaryIconList) {
                mercenary.transform.SetAsLastSibling();
                mercenary.SetInfoTxt(type);
            }
            if(isMercenaryOpen) {
                mercenaryList.gameObject.SetActive(true);
            } else {
                ownCharacterList.gameObject.SetActive(true);
            }
        }

        private void SetConditions(Noroshi.Core.Game.Character.CharacterTagSet tagFlags) {
            if(tagFlags != null) {
                var gclm = GlobalContainer.LocalizationManager;
                var enableJoin = "";
                if(tagFlags.HasTag(9)) {enableJoin += ("/" + gclm.GetText("UI.Noun.Human"));}
                if(tagFlags.HasTag(10)) {enableJoin += ("/" + gclm.GetText("UI.Noun.Demon"));}
                if(tagFlags.HasTag(11)) {enableJoin += ("/" + gclm.GetText("UI.Noun.Ham"));}
                enableJoin = enableJoin.Substring(1);
                enableJoin.Insert(0, gclm.GetText("UI.Suffix.BeforeOnly"));
                enableJoin += gclm.GetText("UI.Suffix.AfterOnly");
                txtEnableJoinBattle.text = enableJoin;
                foreach(var chara in characterIconList) {
                    if(!tagFlags.GetTags().Any(tag => chara.tagSet.HasTag(tag))) {
                        canSelectNum--;
                        chara.SetDisable();
                        chara.transform.SetParent(disableIconListWrapper.transform);
                    } else {
                        chara.transform.SetParent(enableIconListWrapper.transform);
                    }
                }
                foreach(var chara in mercenaryIconList) {
                    if(!tagFlags.GetTags().Any(tag => chara.tagSet.HasTag(tag))) {
                        chara.SetDisable();
                    }
                }
                disableIconListWrapper.SetActive(true);
            } else {
                foreach(var chara in characterIconList) {
                    chara.transform.SetParent(enableIconListWrapper.transform);
                }
                disableIconListWrapper.SetActive(false);
                txtEnableJoinBattle.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.AllMembers");
            }
        }

        private void SwitchTab(int index) {
            for(int i = 0, l = tabContentList.Length; i < l; i++) {
                tabBtnList[i].SetSelect(i == index);
                tabContentList[i].SetActive(i == index);
            }
            if(index == 0) {
                isMercenaryOpen = false;
            } else {
                isMercenaryOpen = true;
            }
        }

        private void AddSelectedCharacter(CharacterSelectIcon origin, bool isMercenary = false) {
            CharacterSelectPanel panel = null;
            var mercenaryNum = battleMercenaryID == null ? 0 : 1;
            if(battleCharacterIdList.Count > maxBattleCharacterNum - cpuIconList.Count - mercenaryNum - 1
                || battleCharacterIdList.Contains(origin.playerCharacterID)
            ) {return;}

            if(!isMercenary) {
                foreach(var chara in characterPanelList.Where(
                    chara => chara.playerCharacterID == origin.playerCharacterID &&
                    !battleCharacterIdList.Contains(origin.playerCharacterID)
                )) {
                    panel = chara;
                    battleCharacterIdList.Add(origin.playerCharacterID);
                }
            } else {
                foreach(var chara in mercenaryPanelList.Where(
                    chara => chara.playerCharacterID == origin.playerCharacterID
                )) {
                    panel = chara;
                    battleMercenaryID = origin.playerCharacterID;
                }
            }
            origin.OnSelect(true);
            selectedCharacterList.Add(panel);
            SetSelectedStatus(origin.power, origin.hp);
            btnStartBattle.SetEnable(true);
            btnOK.SetEnable(true);
            TweenA.Add(noticeCharacterSelect, 0.01f, 0).Then(() => {
                noticeCharacterSelect.SetActive(false);
            });
        }

        private void RemoveSelectedCharacter(uint id, bool isMercenary = false) {
            if(isMercenary) {
                battleMercenaryID = null;
            } else {
                battleCharacterIdList.Remove(id);
            }
            for(int i = 0, l = selectedCharacterList.Count; i < l; i++) {
                if(selectedCharacterList[i].playerCharacterID == id) {
                    selectedCharacterList[i].DisappearCharacter(createdCharacterNum > 0);
                    selectedCharacterList.RemoveAt(i);
                    break;
                }
            }
            if(selectedCharacterList.Count < 1) {
                btnStartBattle.SetEnable(false);
                btnOK.SetEnable(false);
                noticeCharacterSelect.SetActive(true);
                TweenA.Add(noticeCharacterSelect, 0.05f, 1).Delay(0.8f);
            }
        }

        private void RemoveAllSelectedCharacter() {
            selectedPower = 0;
            selectedHp = 0;
            crntDecaCharacterNum = 0;
            battleMercenaryID = null;

            for(int i = 0, l = characterIconList.Count; i < l; i++) {
                characterIconList[i].OnSelect(false);
            }
            for(int i = 0, l = selectedCharacterList.Count; i < l; i++) {
                selectedCharacterList[i].InActiveCharacter();
                if(createdCharacterNum > 0) {
                    selectedCharacterList[i].DestroyCharacter();
                }
            }
            for(int i = 0, l = mercenaryIconList.Count; i < l; i++) {
                mercenaryIconList[i].OnSelect(false);
                mercenaryPanelList[i].InActiveCharacter();
            }
            battleCharacterIdList = new List<uint>();
            selectedCharacterList = new List<CharacterSelectPanel>();
            btnStartBattle.SetEnable(false);
            btnOK.SetEnable(false);
            SetSelectedStatus(0, 0);
            noticeCharacterSelect.SetActive(true);
            TweenA.Add(noticeCharacterSelect, 0.05f, 1).Delay(0.8f);
        }

        private void MoveSelectedCharacter(bool isAnim = true) {
            var posList = new Vector2[selectedCharacterList.Count];
            var sortY = selectedCharacterList;
            var selectedLength = selectedCharacterList.Count;
            var scale = selectedCharacterList.Count == 0 ? 1 : -selectedCharacterList.Count * 0.1f + 1.5f;
            selectedCharacterList = selectedCharacterList.OrderBy(c => c.orderPriority).ToList();
            for(int i = 0; i < selectedLength; i++) {
                var sn = selectedLength == 1 ? 2
                    : selectedLength == 2 || selectedLength == 3 ? 1
                    : 0;
                var n = i + sn;
                posList[i] = new Vector2(selectedPositionXList[n], 0);
            }
            sortY = sortY.OrderBy(c => c.orderInLayer).ThenBy(c => c.characterID).ToList();
            for(int i = 0; i < selectedLength; i++) {
                var sn = selectedLength == 1 ? 2
                    : selectedLength == 2 || selectedLength == 3 ? 1
                    : 0;
                var n = i + sn;
                for(int j = 0; j < selectedLength; j++) {
                    if(sortY[i] == selectedCharacterList[j]) {
                        posList[j].y = selectedPositionYList[n];
                        break;
                    }
                }
            }

            for(int i = 0; i < selectedLength; i++) {
                var n = i;
                if(selectedCharacterList[n].gameObject.activeSelf) {
                    selectedCharacterList[n].MoveCharacter(posList[n], isAnim);
                } else {
                    selectedCharacterList[n].AppearCharacter(posList[n], isAnim);
                }
            }
            if(isAnim) {
                TweenS3.Add(characterPanelListWrapper, 0.7f, Vector3.one * scale).EaseInOutSine();
            } else {
                characterPanelListWrapper.transform.localScale = Vector3.one * scale;
            }
        }

        private void SetSelectedStatus(int power, int hp) {
            selectedPower += power;
            selectedHp += hp;

            txtBattleCharacterNum.text = selectedCharacterList.Count.ToString();
            txtSelectedPower.text = string.Format("{0:#,0}\r", selectedPower);
            txtSelectedHp.text = string.Format("{0:#,0}\r", selectedHp);
            if(selectedHp > 0) {
                TweenS.Add(txtSelectedPower.gameObject, 0.1f, 1.15f).EaseOutExpo()
                    .Then(() => {
                        TweenS.Add(txtSelectedPower.gameObject, 0.2f, 1.0f).EaseInQuart();
                    });
                TweenS.Add(txtSelectedHp.gameObject, 0.1f, 1.15f).EaseOutExpo()
                    .Then(() => {
                        TweenS.Add(txtSelectedHp.gameObject, 0.2f, 1.0f).EaseInQuart();
                    });
            }
        }

        private void SetRequireCharacter(uint[] idList) {
            var requireText = "";
            for(int i = 0; i < idList.Length; i++) {
                foreach(var icon in characterIconList.Where(icon => icon.id == idList[i])) {
                    var chara = GlobalContainer.MasterManager.CharacterMaster.Get((uint)icon.id);
                    requireText += ("/" + GlobalContainer.LocalizationManager.GetText(chara.TextKey + ".Name"));
                    icon.SetRequire();
                    AddSelectedCharacter(icon);
                }
                foreach(var panel in characterPanelList.Where(panel => panel.characterID == idList[i])) {
                    panel.SetRequire();
                }
            }
            requireText = requireText.Substring(1);
            txtRequireJoinBattle.text = requireText;
            requireTextWrapper.SetActive(true);
        }

        private void SetCPUCharacter(uint[] idList) {
            for(int i = 0; i < idList.Length; i++) {
                bool haveCharacter = false;
                foreach(var icon in characterIconList) {
                    if(icon.id == idList[i]) {
                        haveCharacter = true;
                        break;
                    }
                }
                if(!haveCharacter) {
                    canSelectNum++;
                    CreateCPUCharacter(idList[i]);
                }
            }
            txtBattleCharacterNum.text = selectedCharacterList.Count.ToString();
        }

        private void SetDefaultCharacter(uint[] idList) {
            for(int i = 0; i < idList.Length; i++) {
                foreach(var icon in characterIconList.Where(icon => icon.id == idList[i])) {
                    AddSelectedCharacter(icon);
                }
            }
        }

        private void SetReloadStatus(CharacterStatus status) {
            characterStatusList.Add(status);
            foreach(var icon in characterIconList.Where(icon => icon.id == status.CharacterID)) {
                icon.UpdateStatus(status);
            }
            foreach(var panel in characterPanelList.Where(panel => panel.characterID == status.CharacterID)) {
                panel.SetCharacterSkin(status.SkinLevel);
            }
        }

        private void StartBattle() {
            var data = new StartBattleData();
            data.playerCharacterIDs = battleCharacterIdList.ToArray();
            data.mercenaryID = battleMercenaryID;
            foreach(var chara in selectedCharacterList) {
                chara.MoveCharacter(new Vector2(5000, chara.transform.localPosition.y), true);
            }
            UILoading.Instance.AddHistory(SceneManager.GetActiveScene().name);
            UILoading.Instance.AddHistory(Constant.SCENE_BATTLE);
            TweenNull.Add(UILoading.Instance.gameObject, 0.8f).Then(() => {
                UILoading.Instance.ShowLoading();
            });
            TweenNull.Add(gameObject, 1.2f).Then(() => {
                OnStartBattle.OnNext(data);
                Instance = null;
                Resources.UnloadUnusedAssets();
            });
        }

        public void LoadCharacterList() {
            var d = 0;
            characterStatusList = new List<CharacterStatus>();
            GlobalContainer.RepositoryManager.PlayerCharacterRepository.GetAll().Do(list => {
                list = list.OrderByDescending(c => c.Level).ToArray();
                for(int i = 0, l = list.Length; i < l; i++) {
                    var n = i;
                    var status = new Game.CharacterStatus(list[n], GlobalContainer.MasterManager.CharacterMaster.Get(list[n].CharacterID));

                    if(i % 2 == 0) {d++;}
                    characterStatusList.Add(status);
                    Observable.TimerFrame(d).Subscribe(_ => {
                        CreateCharacter(status, list[n].ID);
                        if(n >= l - 1) {
                            SwitchTab(0);
                            isLoad = true;
                        }
                    });
                }
            }).Subscribe();
        }

        public void ReloadCharacterList() {
            isLoad = false;
            characterStatusList = new List<CharacterStatus>();
            GlobalContainer.RepositoryManager.PlayerCharacterRepository.GetAll().Do(list => {
                foreach(var chara in list) {
                    var status = new Game.CharacterStatus(chara, GlobalContainer.MasterManager.CharacterMaster.Get(chara.CharacterID));
                    SetReloadStatus(status);
                    if(characterStatusList.Count >= list.Length) {
                        isLoad = true;
                    }
                }
            }).Subscribe();
        }

        public void ReloadCharacter(uint characterID, CharacterStatus characterStatus = null) {
            for(int i = characterStatusList.Count - 1; i > -1; i--) {
                if(characterStatusList[i].CharacterID == characterID) {
                    characterStatusList.RemoveAt(i);
                    break;
                }
            }
            if(characterStatus != null) {
                SetReloadStatus(characterStatus);
                OnReloadStatus.OnNext(characterStatus);
            } else {
                GlobalContainer.RepositoryManager.PlayerCharacterRepository.GetAll().Do(list => {
                    foreach(var chara in list.Where(c => c.CharacterID == characterID)) {
                        var status = new Game.CharacterStatus(chara, GlobalContainer.MasterManager.CharacterMaster.Get(chara.CharacterID));
                        SetReloadStatus(status);
                        OnReloadStatus.OnNext(status);
                    }
                }).Subscribe();
            }
        }

        public void LoadMercenaryList() {
            Noroshi.Guild.WebApiRequester.GetTakableRentalCharacters().Do(res => {
                var list = res.RentalPlayerCharacters.OrderByDescending(c => c.Level).ToArray();
                for(int i = 0, l = list.Length; i < l; i++) {
                    var status = new Game.CharacterStatus(list[i], GlobalContainer.MasterManager.CharacterMaster.Get(list[i].CharacterID));
                    CreateMercenary(status, list[i].ID);
                }
            }).Subscribe();
        }

//        public void ReloadMercenaryList() {
//            foreach(var icon in mercenaryIconList) {
//                Destroy(icon.gameObject);
//            }
//            foreach(var panel in mercenaryPanelList) {
//                Destroy(panel.gameObject);
//            }
//            mercenaryIconList = new List<MercenarySelectIcon>();
//            mercenaryPanelList = new List<CharacterSelectPanel>();
//            LoadMercenaryList();
//        }

        public void SetContinuingStatus(InitialCondition.PlayerCharacterCondition[] characterConditionList) {
            foreach(var character in characterIconList) {
                character.SetContinuingStatus(1, 0);
                if(characterConditionList == null) {continue;}
                foreach(var initChara in characterConditionList) {
                    if(character.playerCharacterID == initChara.PlayerCharacterID) {
                        var hpRatio = (float)initChara.HP / (float)character.hp;
                        var spRatio = (float)initChara.Energy / Noroshi.Core.Game.Battle.Constant.MAX_ENERGY;
                        character.SetContinuingStatus(hpRatio, spRatio);
                        break;
                    }
                }
            }
        }

        public void SetNewCharacter(uint characterID) {
            var gcrm = GlobalContainer.RepositoryManager;
            gcrm.PlayerCharacterRepository.GetAll().Do(list => {
                foreach(var chara in list.Where(character => character.CharacterID == characterID)) {
                    var status = new Game.CharacterStatus(chara, GlobalContainer.MasterManager.CharacterMaster.Get(chara.CharacterID));
                    characterStatusList.Add(status);
                    CreateCharacter(status, chara.ID);
                }
            }).Subscribe();
        }

        public uint[] GetPlayerCharacterId(uint[] idList) {
            List<uint> pcIdList = new List<uint>();
            List<CharacterSelectPanel> tempList = new List<CharacterSelectPanel>();
            for(int i = 0, l = idList.Length; i < l; i++) {
                foreach(var chara in characterPanelList.Where(chara => chara.characterID == idList[i])) {
                    tempList.Add(chara);
                }
            }
            tempList = tempList.OrderBy(c => c.orderPriority).ToList();
            for(int i = 0, l = tempList.Count; i < l; i++) {
                pcIdList.Add(tempList[i].playerCharacterID);
            }
            return pcIdList.ToArray();
        }

        public uint[] GetCharacterId(uint[] pcIdList) {
            List<uint> idList = new List<uint>();
            List<CharacterSelectPanel> tempList = new List<CharacterSelectPanel>();
            for(int i = 0, l = pcIdList.Length; i < l; i++) {
                foreach(var chara in characterPanelList.Where(chara => chara.playerCharacterID == pcIdList[i])) {
                    tempList.Add(chara);
                }
            }
            tempList = tempList.OrderBy(c => c.orderPriority).ToList();
            for(int i = 0, l = tempList.Count; i < l; i++) {
                idList.Add(tempList[i].characterID);
            }
            return idList.ToArray();
        }

        public List<CharacterStatus> GetCharacterStatusList() {
            var list = characterStatusList.OrderBy(c => c.CharacterID).ToList();
            return list;
        }

        public uint[] GetDefaultCharacter(string key) {
            var list = new List<uint>();
            var value = PlayerPrefs.GetString(key);
            if(value != "" && value != null) {
                list = value.Split(new char[]{','}).Select(v => uint.Parse(v)).ToList();
            }
            return list.ToArray();
        }

        public void SaveDefaultCharacter(string key, uint[] pcIdList) {
            var idList = GetCharacterId(pcIdList);
            var list = idList.Select(v => v.ToString()).ToArray();
            var value = string.Join(",", list);
            PlayerPrefs.SetString(key, value);
            PlayerPrefs.Save();
        }

        public void OpenPanel(bool isSetting,
            uint[] defaultList = null,
            uint[] requireList = null,
            uint[] cpuList = null,
            Noroshi.Core.Game.Character.CharacterTagSet tagFlags = null
        ) {
            canSelectNum = characterIconList.Count;
            SetConditions(tagFlags);
            FilterCharacterList(0);
            modal.SetActive(true);
            btnOK.gameObject.SetActive(isSetting);
            btnStartBattle.gameObject.SetActive(!isSetting);
            if(isSetting || SceneManager.GetActiveScene().name == Constant.SCENE_ARENA) {
                tabBtnList[1].gameObject.SetActive(false);
            } else {
                tabBtnList[1].gameObject.SetActive(true);
            }
            if(requireList != null && requireList.Length > 0) {
                SetRequireCharacter(requireList);
            } else {
                requireTextWrapper.SetActive(false);
            }
            if(cpuList != null && cpuList.Length > 0) {
                SetCPUCharacter(cpuList);
            }
            if(defaultList != null && defaultList.Length > 0) {
                SetDefaultCharacter(defaultList);
            }
            if((defaultList != null && defaultList.Length > 0)
                || (requireList != null && requireList.Length > 0)
                || (cpuList != null && cpuList.Length > 0)) {
                MoveSelectedCharacter(false);
            }
            SortCharacterList(sortTypeLength - 1);
            TweenA.Add(modal, 0.3f, 1).EaseOutCubic();
            BackButtonController.Instance.IsCharacterEditOpen(true);
        }

        public void ClosePanel(bool isBack = true) {
            if(isBack) {
                OnClosePanel.OnNext(null);
            } else {
                List<uint> idList = new List<uint>();
                foreach(var chara in selectedCharacterList) {
                    idList.Add(chara.characterID);
                }
                OnClosePanel.OnNext(idList.ToArray());
            }
            for(int i = cpuPanelList.Count - 1; i > -1; i--) {
                Destroy(cpuPanelList[i].gameObject);
            }
            cpuPanelList = new List<CharacterSelectPanel>();
            RemoveAllSelectedCharacter();
            TweenA.Add(modal, 0.2f, 0).Then(() => {
                for(int i = cpuIconList.Count - 1; i > -1; i--) {
                    Destroy(cpuIconList[i].gameObject);
                }
                characterPanelListWrapper.transform.localScale = Vector3.one;
                cpuIconList = new List<CharacterSelectIcon>();
                ownCharacterList.verticalNormalizedPosition = 1;
                mercenaryList.verticalNormalizedPosition = 1;
                SwitchTab(0);
                modal.SetActive(false);
                BackButtonController.Instance.IsCharacterEditOpen(false);
            });
        }
    }
}
