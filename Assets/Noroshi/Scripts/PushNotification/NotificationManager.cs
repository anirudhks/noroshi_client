﻿using UniRx;
using Prime31;
using Flaggs.Unity.PushNotification;
using Noroshi.Core.Game.Player;

namespace Noroshi.PushNotification
{
    public class NotificationManager
    {
        static readonly string _productNumber = "1037735443286";
        public NotificationManager()
        {
#if UNITY_IOS
			EtceteraManager.remoteRegistrationSucceededEvent += RemoteRegistrationSucceeded;
			EtceteraManager.remoteRegistrationFailedEvent += RemoteRegistrationFailed;
			EtceteraBinding.registerForRemoteNotifications( P31RemoteNotificationType.Alert | P31RemoteNotificationType.Badge | P31RemoteNotificationType.Sound );
#endif
#if UNITY_ANDROID
            var gcm = new CloudMessaging(_productNumber);
            gcm.Initialize(new PushNotificationCommandHandlerBuilder());
            gcm.GetIdentifier().Subscribe(RemoteRegistrationSucceeded);
#endif
        }

        public static void RemoteRegistrationFailed(string error)
        {
#if UNITY_IOS
#endif
#if UNITY_ANDROID
#endif
        }

        public static void RemoteRegistrationSucceeded(string identifier)
        {
#if UNITY_IOS
            WebApiRequester.SetIdentifier(ClientPlatform.iOS, identifier).Subscribe();
#endif
#if UNITY_ANDROID
            WebApiRequester.SetIdentifier(ClientPlatform.Android, identifier).Subscribe();
#endif
        }
    }
}
