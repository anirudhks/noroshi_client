﻿using UniRx;
using Noroshi.Core.Game.Player;
using Noroshi.Core.WebApi.Response.PushNotification;

namespace Noroshi.PushNotification
{
    class WebApiRequester
    {
        public static IObservable<SetIdentifierResponse> SetIdentifier(ClientPlatform clientPlatform, string identifier)
        {
            var request = new SetIdentifierRequest { ClientPlatform = (byte)clientPlatform, Identifier = identifier };
            return GlobalContainer.WebApiRequester.Post<SetIdentifierRequest, SetIdentifierResponse>("PushNotification/SetIdentifier", request);
        }
        class SetIdentifierRequest
        {
            public byte ClientPlatform { get; set; }
            public string Identifier { get; set; }
        }
    }
}
