﻿namespace Noroshi.UI {
    public class QueryKeys {
        public const string SelectedCharacter = "SelectedCharacter";
        public const string SelectedEquipIndex = "SelectedEquipIndex";
        public const string SelectedEquipGearList = "SelectedEquipGearList";
        public const string SelectedUnAcquiredCharacter = "SelectedUnAcquiredCharacter";
        public const string IsOpenGetSoulPanel = "IsOpenGetSoulPanel";

        public const string SelectedChapter = "SelectedChapter";
        public const string SelectedEpisode = "SelectedEpisode";
        public const string SelectedStage = "SelectedStage";
        public const string WorldType = "WorldType";
        public const string FromCharacterList = "FromCharacterList";
        public const string IsChallengeFinalStage = "IsChallengeFinalStage";
        public const string IsChallengeNewStage = "IsChallengeNewStage";

        public const string IsShop = "IsShop";
        public const string IsMedalShop = "IsMedalShop";

        public const string IsFirstStoryBattle = "IsFirstStoryBattle";
    }
}
