﻿using UnityEngine;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class BackButtonController : MonoBehaviour {
        public Subject<bool> OnBack = new Subject<bool>();
        public Subject<bool> OnCloseModal = new Subject<bool>();
        public Subject<bool> OnCloseModalChild = new Subject<bool>();
        public Subject<bool> OnCloseCommonParts = new Subject<bool>();
        public Subject<bool> OnCloseCommonPartsChild = new Subject<bool>();
        public Subject<bool> OnCloseCharacterEdit = new Subject<bool>();

        public static BackButtonController Instance;

        private bool isEnable = true;
        private bool isModalOpen = false;
        private bool isModalChildOpen = false;
        private bool isCommonPartsOpen = false;
        private bool isCommonPartsChildOpen = false;
        private bool isCharacterEditOpen = false;

        private void Awake() {
            if(Instance == null) {Instance = this;}
        }

        void Update () {
            if(!isEnable) {return;}
            if(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.OSXEditor) {
                if(Input.GetKeyUp(KeyCode.Escape)) {
                    if(isCommonPartsChildOpen) {
                        OnCloseCommonPartsChild.OnNext(true);
                    } else if(isCommonPartsOpen) {
                        OnCloseCommonParts.OnNext(true);
                    } else if(isCharacterEditOpen) {
                        OnCloseCharacterEdit.OnNext(true);
                        BattleCharacterSelect.Instance.ClosePanel();
                        SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
                    } else if(isModalChildOpen) {
                        OnCloseModalChild.OnNext(true);
                    } else if(isModalOpen) {
                        OnCloseModal.OnNext(true);
                    } else {
                        OnBack.OnNext(true);
                    }
                }
            }
        }

        public void IsEnable(bool flag) {
            isEnable = flag;
        }

        public void IsModalOpen(bool flag) {
            isModalOpen = flag;
        }

        public void IsModalChildOpen(bool flag) {
            isModalChildOpen = flag;
        }

        public void IsCommonPartsOpen(bool flag) {
            isCommonPartsOpen = flag;
        }

        public void IsCommonPartsChildOpen(bool flag) {
            isCommonPartsChildOpen = flag;
        }

        public void IsCharacterEditOpen(bool flag) {
            isCharacterEditOpen = flag;
        }
    }
}
