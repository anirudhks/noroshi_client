using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class BtnLoadLevel : BtnCommon {
        [SerializeField] string sceneName;
        [SerializeField] GameObject enableObject;
        [SerializeField] GameObject disableObject;
        [SerializeField] string[] queryKeyList;
        [SerializeField] int[] queryValueList;
        [SerializeField] uint soundID;

        public Subject<string> OnChangeLevel = new Subject<string>();

        private bool isLoading = true;

        private void Start() {
            if(UILoading.Instance != null) {
                UILoading.Instance.SetLoadBtn(this);
            } else {
                isLoading = false;
            }

            OnClickedBtn.Subscribe(id => {
                if(isLoading) {
                    for(int i = 0, l = queryKeyList.Length; i < l; i++) {
                        UILoading.Instance.SetQuery(queryKeyList[i], queryValueList[i]);
                    }
                    OnChangeLevel.OnNext(sceneName);
                } else {
                    SceneManager.LoadScene(sceneName);
                }
            });
            OnPlaySE.Subscribe(_ => {
                if(SoundController.Instance != null) {
                    if(soundID > 0) {
                        SoundController.Instance.PlaySE(soundID);
                    } else {
                        SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
                    }
                }
            });
        }

        public void SetEnable(bool _isEnable, bool isChangeColor = true) {
            base.SetEnable(_isEnable);
            if(enableObject != null) {enableObject.SetActive(_isEnable);}
            if(disableObject != null) {disableObject.SetActive(!_isEnable);}
            if(!isChangeColor) {return;}
            if(_isEnable) {
                if(transform.childCount > 0) {
                    TweenC.Add(transform.GetChild(0).gameObject, 0.01f, new Color(1, 1, 1));
                }
                TweenC.Add(gameObject, 0.01f, new Color(1, 1, 1));
            } else {
                if(transform.childCount > 0) {
                    TweenC.Add(transform.GetChild(0).gameObject, 0.01f, new Color(0.5f, 0.5f, 0.5f));
                }
                TweenC.Add(gameObject, 0.01f, new Color(0.5f, 0.5f, 0.5f));
            }
        }

        public void SetSceneName(string sname) {
            sceneName = sname;
        }
    }
}
