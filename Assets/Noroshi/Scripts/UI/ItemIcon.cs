﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Noroshi.Core.Game.Possession;
using Noroshi.Core.WebApi.Response.Possession;

namespace Noroshi.UI {
    public class ItemIcon : BtnCommon {
        [SerializeField] Image imgItem;
        [SerializeField] Image imgFrame;
        [SerializeField] Sprite spriteGold;
        [SerializeField] Sprite spriteGem;
        [SerializeField] Sprite spriteStamina;
        [SerializeField] Sprite spriteBP;
        [SerializeField] Sprite spriteExp;
        [SerializeField] Sprite spriteVipExp;
        [SerializeField] Sprite spriteArenaCoin;
        [SerializeField] Sprite spriteGuildCoin;
        [SerializeField] Sprite spriteExpeditionCoin;
        [SerializeField] Sprite spriteCharacterFrame;
        [SerializeField] Sprite[] spriteItemFrameList;
        [SerializeField] Sprite[] spritePieceFrameList;

        public void SetItemIcon(PossessionObject itemData) {
            imgItem.transform.localScale = Vector3.one;
            if(itemData.Category == (byte)PossessionCategory.Status) {
                if(itemData.ID == (byte)PossessionStatusID.PlayerExp) {
                    imgItem.sprite = spriteExp;
                } else if(itemData.ID == (byte)PossessionStatusID.Gold) {
                    imgItem.sprite = spriteGold;
                    imgItem.transform.localScale = Vector3.one * 0.64f;
                } else if(itemData.ID == (byte)PossessionStatusID.CommonGem
                          || itemData.ID == (byte)PossessionStatusID.FreeGem) {
                    imgItem.sprite = spriteGem;
                    imgItem.transform.localScale = Vector3.one * 0.64f;
                } else if(itemData.ID == (byte)PossessionStatusID.BP) {
                    imgItem.sprite = spriteBP;
                    imgItem.transform.localScale = Vector3.one * 0.64f;
                } else if(itemData.ID == (byte)PossessionStatusID.Stamina) {
                    imgItem.sprite = spriteStamina;
                    imgItem.transform.localScale = Vector3.one * 0.64f;
                } else if(itemData.ID == (byte)PossessionStatusID.PlayerVipExp) {
                    imgItem.sprite = spriteVipExp;
                } else if(itemData.ID == (byte)PossessionStatusID.ArenaPoint) {
                    imgItem.sprite = spriteArenaCoin;
                    imgItem.transform.localScale = Vector3.one * 0.64f;
                } else if(itemData.ID == (byte)PossessionStatusID.GuildPoint) {
                    imgItem.sprite = spriteGuildCoin;
                    imgItem.transform.localScale = Vector3.one * 0.64f;
                } else if(itemData.ID == (byte)PossessionStatusID.ExpeditionPoint) {
                    imgItem.sprite = spriteExpeditionCoin;
                    imgItem.transform.localScale = Vector3.one * 0.64f;
                }
                imgFrame.gameObject.SetActive(false);
            } else if(itemData.Category == (byte)PossessionCategory.Character) {
                imgItem.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite>(itemData.ID, "thumb_1");
                imgFrame.sprite = spriteCharacterFrame;
                imgFrame.transform.localScale = Vector3.one * 1.25f;
                imgFrame.gameObject.SetActive(true);
            } else {
                var rarity = ItemListManager.Instance.GetItemRarity(itemData);
                imgItem.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(itemData.ID);
                if(itemData.Category == (byte)PossessionCategory.GearPiece
                   || itemData.Category == (byte)PossessionCategory.Soul) {
                    imgFrame.sprite = spritePieceFrameList[rarity - 1];
                } else {
                    imgFrame.sprite = spriteItemFrameList[rarity - 1];
                }
                imgFrame.transform.localScale = Vector3.one;
                imgFrame.gameObject.SetActive(true);
            }
        }
    }
}
