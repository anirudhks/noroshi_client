﻿namespace Noroshi.UI {
    public class SaveKeys {
        public const string OpenTutorialQuest = "OpenQuest";
        public const string OpenTutorialDailyQuest = "OpenDailyQuest";
        public const string OpenTutorialArena = "OpenTutorialArena";
        public const string OpenTutorialGuild = "OpenGuild";
        public const string OpenTutorialEvolution = "OpenTutorialEvolution";
        public const string OpenBackStory = "OpenBackStory";

        public const string IsBGMOn = "IsBGMOn";
        public const string IsSEOn = "IsSEOn";
        public const string IsVoiceOn = "IsVoiceOn";

        public const string DefaultStoryBattleCharacter = "DefaultStoryBattleCharacter";
        public const string DefaultArenaBattleCharacter = "DefaultArenaBattleCharacter";
        public const string DefaultExpeditionBattleCharacter = "DefaultExpeditionBattleCharacter";
        public const string DefaultTrainingBattleCharacter = "DefaultTrainingBattleCharacter";
        public const string DefaultTrialBattleCharacter = "DefaultTrialBattleCharacter";
    }
}
