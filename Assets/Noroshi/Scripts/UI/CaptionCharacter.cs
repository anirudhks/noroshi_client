﻿using UnityEngine;
using UnityEngine .UI;
using System.Collections;

namespace Noroshi.UI {
    public class CaptionCharacter : MonoBehaviour {
        public class CharacterInfo {
            public string Name;
            public uint Lv;
            public uint PromotionLevel;
            public uint ID;
            public bool IsBoss;
            public string Description;
            public int Index;
        }

        [SerializeField] Text txtName;
        [SerializeField] Text txtLv;
        [SerializeField] Image thumbCharacter;
        [SerializeField] Image imgCharacterFrame;
        [SerializeField] Sprite[] imgFrameList;
        [SerializeField] Text txtDescription;

        public void ShowCaption(CharacterInfo characterInfo, Vector3 position) {
            position.y += 1.2f;
            txtName.text = characterInfo.Name;
            txtLv.text = characterInfo.Lv.ToString();
            thumbCharacter.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite>(characterInfo.ID, "thumb_1");
            imgCharacterFrame.sprite = imgFrameList[characterInfo.PromotionLevel- 1];
            txtDescription.text = characterInfo.Description;
            gameObject.transform.position = position;
            gameObject.SetActive(true);
        }

        public void HideCaption() {
            gameObject.SetActive(false);
        }
    }
}
