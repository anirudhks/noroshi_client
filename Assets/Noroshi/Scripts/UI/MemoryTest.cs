﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MemoryTest : MonoBehaviour {
    [SerializeField] Text txt1;
    [SerializeField] Text txt2;

    void Awake() {
        DontDestroyOnLoad(gameObject);
    }

    void Update() {
        uint monoUsed = Profiler.GetMonoUsedSize ();
        uint monoSize = Profiler.GetMonoHeapSize ();
        uint totalUsed = Profiler.GetTotalAllocatedMemory (); // == Profiler.usedHeapSize
        uint totalSize = Profiler.GetTotalReservedMemory ();
        string text1 = string.Format(
            "mono:{0}/{1}kb({2:f1}%)", monoUsed/1024, monoSize/1024, 100.0*monoUsed/monoSize
        );
        string text2 = string.Format(
            "total:{0}/{1}kb({2:f1}%)", totalUsed/1024, totalSize/1024, 100.0*totalUsed/totalSize
        );
        txt1.text = text1;
        txt2.text = text2;
    }
}
