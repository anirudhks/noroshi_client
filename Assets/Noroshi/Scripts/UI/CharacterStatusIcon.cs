﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Noroshi.UI {
    public class CharacterStatusIcon : MonoBehaviour {
        public class CharacterData {
            public uint CharacterID;
            public uint Level;
            public uint EvolutionLevel;
            public uint PromotionLevel;
            public bool IsDeca;
        }

        [SerializeField] Text txtLv;
        [SerializeField] GameObject iconBig;
        [SerializeField] Image imgCharacter;
        [SerializeField] Image imgCharacterFrame;
        [SerializeField] Sprite[] imgFrameList;
        [SerializeField] GameObject[] evolutionStar;

        public void SetInfo(CharacterData status) {
            var skinLevel = new Noroshi.Core.Game.Character.Skin((byte)status.EvolutionLevel, status.IsDeca).GetSkinLevel();
            imgCharacter.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite>(status.CharacterID,  string.Format("thumb_{0}", skinLevel));
            txtLv.text = string.Format("Lv{0}", status.Level);

            imgCharacterFrame.sprite = imgFrameList[status.PromotionLevel - 1];
            iconBig.SetActive(status.IsDeca);
            for(int i = 0, l = evolutionStar.Length; i < l; i++) {
                if(i < status.EvolutionLevel) {
                    evolutionStar[i].SetActive(true);
                } else {
                    evolutionStar[i].SetActive(false);
                }
            }
        }

    }
}
