using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.WebApi.Response;
using Noroshi.Core.Game.Possession;

namespace Noroshi.UI {
    public class ItemListManager : MonoBehaviour {

        public static ItemListManager Instance;

        public Subject<GetCharacterModal> OnGetCharacter = new Subject<GetCharacterModal>();
        public Subject<EvolutionCharacterModal> OnEvolutionCharacter = new Subject<EvolutionCharacterModal>();
        public bool IsLoad {get {return isLoad;}}

        private static bool isLoad = false;
        private static Dictionary<string, uint> itemNumList;
        private static Dictionary<string, uint> needSoulList;
        private static Dictionary<string, Core.WebApi.Response.Master.Gear> gearDetailList;
        private static Dictionary<string, List<Dictionary<string, uint>>> recipeList;

        private bool isItemNumLoad = false;
        private bool isRecipeListLoad = false;
        private bool isNeedSoulLoad = false;

        private void Awake() {
            GlobalContainer.SetFactory(() => new Repositories.RepositoryManager());
            if(Instance == null) {Instance = this;}
            if(!IsLoad || UILoading.Instance.GetPreviousHistory() == Constant.SCENE_BATTLE) {
                isLoad = false;
                SetItemNumList();
                SetGearDetailList();
                StartCoroutine(OnLoading());
            }
        }

        private IEnumerator OnLoading() {
            while(BattleCharacterSelect.Instance == null || !BattleCharacterSelect.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            SetNeedSoulList();
            while(!isItemNumLoad || !isRecipeListLoad || !isNeedSoulLoad) {
                yield return new WaitForEndOfFrame();
            }
            isLoad = true;
        }

        private void SetItemNumList() {
            var gcrm = GlobalContainer.RepositoryManager;
            itemNumList = new Dictionary<string, uint>();
            gcrm.InventoryRepository.Get().Do(itemList => {
                foreach(var gear in itemList.Gears) {
                    itemNumList["item" + gear.GearID] = gear.PossessionsCount;
                }
                foreach(var gearPiece in itemList.GearPieces) {
                    itemNumList["item" + gearPiece.GearPieceID] = gearPiece.PossessionsCount;
                }
                foreach(var gearEnchantMaterial in itemList.GearEnchantMaterials) {
                    itemNumList["item" + gearEnchantMaterial.GearEnchantMaterialID] = gearEnchantMaterial.PossessionsCount;
                }
                foreach(var drug in itemList.Drugs) {
                    itemNumList["item" + drug.DrugID] = drug.PossessionsCount;
                }
                foreach(var soul in itemList.Souls) {
                    itemNumList["item" + soul.SoulID] = soul.PossessionsCount;
                }
                foreach(var exchangeCashGift in itemList.ExchangeCashGifts) {
                    itemNumList["item" + exchangeCashGift.ExchangeCashGiftID] = exchangeCashGift.PossessionsCount;
                }
                foreach(var raidTicket in itemList.RaidTickets) {
                    itemNumList["item" + raidTicket.RaidTicketID] = raidTicket.PossessionsCount;
                }
                foreach(var misc in itemList.MiscItems) {
                    itemNumList["item" + misc.MiscItemID] = misc.PossessionsCount;
                }
                isItemNumLoad = true;
            }).Subscribe();
        }

        private void SetNeedSoulList() {
            var gcmm = GlobalContainer.MasterManager;
            var soulList = GlobalContainer.MasterManager.ItemMaster.GetSouls();
            var characterStatusList = BattleCharacterSelect.Instance.GetCharacterStatusList();
            needSoulList = new Dictionary<string, uint>();
            foreach(var soul in soulList) {
                var isHaveCharacter = false;
                byte targetEvolutionLevel = 0;
                var characterData = gcmm.CharacterMaster.Get(soul.CharacterID);
                foreach(var chara in characterStatusList) {
                    if(soul.CharacterID == chara.CharacterID) {
                        isHaveCharacter = true;
                        targetEvolutionLevel = (byte)(chara.EvolutionLevel + 1);
                        break;
                    }
                }
                if(!isHaveCharacter) {targetEvolutionLevel = characterData.InitialEvolutionLevel;}
                var characterEvolutionType = gcmm.CharacterMaster.GetCharacterEvolutionType(characterData.EvolutionType, targetEvolutionLevel);
                var needSoul = characterEvolutionType != null ? characterEvolutionType.Soul : uint.MaxValue;
                needSoulList["chara" + soul.CharacterID] = needSoul;
                needSoulList["targetEvolutionLevel" + soul.CharacterID] = targetEvolutionLevel;
            }
            isNeedSoulLoad = true;
        }

        private void SetGearDetailList() {
            var gears = GlobalContainer.MasterManager.ItemMaster.GetGears();
            gearDetailList = new Dictionary<string, Core.WebApi.Response.Master.Gear>();
            recipeList = new Dictionary<string, List<Dictionary<string, uint>>>();
            foreach(var gear in gears) {
                if(gear.ID == 0) {continue;}
                var recipes = GlobalContainer.MasterManager.ItemMaster.GetGearRecipeByCraftItemID(gear.ID);
                var tempList = new List<Dictionary<string, uint>>();
                gearDetailList["gear" + gear.ID] = gear;
                foreach(var recipe in recipes) {
                    tempList.Add(new Dictionary<string, uint>(){
                        {"id", recipe.MaterialItemID}, {"num", recipe.NecessaryMaterialNum}
                    });
                }
                recipeList["recipe" + gear.ID] = tempList;
            }
            isRecipeListLoad = true;
        }

        private void SetEvolutionCharacter(uint itemID, uint characterID) {
            var gcmm = GlobalContainer.MasterManager;
            var characterData = gcmm.CharacterMaster.Get(characterID);
            var targetEvolutionLevel = (byte)(needSoulList["targetEvolutionLevel" + characterID] + 1);
            var characterEvolutionType = gcmm.CharacterMaster.GetCharacterEvolutionType(characterData.EvolutionType, targetEvolutionLevel);
            var needSoul = characterEvolutionType != null ? characterEvolutionType.Soul : uint.MaxValue;
            var pcID = BattleCharacterSelect.Instance.GetPlayerCharacterId(new uint[]{characterID});
            itemNumList["item" + itemID] = itemNumList["item" + itemID] - needSoulList["chara" + characterID];
            needSoulList["chara" + characterID] = needSoul;
            needSoulList["targetEvolutionLevel" + characterID] = targetEvolutionLevel;
            if(pcID.Length < 1) {
                var getModal = Instantiate(Resources.Load<GetCharacterModal>("UI/GetCharacterModal"));
                getModal.OpenModal(characterID);
                OnGetCharacter.OnNext(getModal);
            } else {
                var evolutionModal = Instantiate(Resources.Load<EvolutionCharacterModal>("UI/EvolutionCharacterModal"));
                evolutionModal.OpenModal(characterID);
                OnEvolutionCharacter.OnNext(evolutionModal);
            }
        }

        public uint GetItemCount(uint id) {
            return itemNumList.ContainsKey("item" + id) ? itemNumList["item" + id] : 0;
        }

        public void ChangeItemCount(uint id, int num, bool isSoul = false) {
            if(itemNumList.ContainsKey("item" + id)) {
                int tempNum = (int)itemNumList["item" + id];
                tempNum += num;
                itemNumList["item" + id] = (uint)tempNum;
            } else {
                itemNumList["item" + id] = (uint)num;
            }
            if(isSoul) {
                var characterID = GlobalContainer.MasterManager.ItemMaster.GetSoul(id).CharacterID;
                if(itemNumList["item" + id] >= needSoulList["chara" + characterID]) {
                    SetEvolutionCharacter(id, characterID);
                }
            }
        }

        public Core.WebApi.Response.Master.Gear GetGearInfo(uint id) {
            return gearDetailList.ContainsKey("gear" + id) ? gearDetailList["gear" + id] : null;
        }

        public List<Dictionary<string, uint>> GetGearRecipe(uint id) {
            return recipeList.ContainsKey("recipe" + id) ? recipeList["recipe" + id] : null;
        }

        public uint GetItemRarity(Noroshi.Core.WebApi.Response.Possession.PossessionObject reward) {
            Noroshi.Core.WebApi.Response.Item item = null;
            if(reward.Category == (byte)PossessionCategory.Gear) {
                item = GlobalContainer.MasterManager.ItemMaster.GetGear(reward.ID);
            } else if(reward.Category == (byte)PossessionCategory.GearPiece) {
                item = GlobalContainer.MasterManager.ItemMaster.GetGearPiece(reward.ID);
            } else if(reward.Category == (byte)PossessionCategory.GearEnchantMaterial) {
                item = GlobalContainer.MasterManager.ItemMaster.GetGearEnchantMaterial(reward.ID);
            } else if(reward.Category == (byte)PossessionCategory.Soul) {
                item = GlobalContainer.MasterManager.ItemMaster.GetSoul(reward.ID);
            } else if(reward.Category == (byte)PossessionCategory.Drug) {
                item = GlobalContainer.MasterManager.ItemMaster.GetDrug(reward.ID);
            } else if(reward.Category == (byte)PossessionCategory.ExchangeCashGift) {
                item = GlobalContainer.MasterManager.ItemMaster.GetExchangeCashGift(reward.ID);
            } else if(reward.Category == (byte)PossessionCategory.RaidTicket) {
                item = GlobalContainer.MasterManager.ItemMaster.GetRaidTicket(reward.ID);
            }
            return item != null ? item.Rarity : 1;
        }

        public void UpdateNeedSoul(uint characterID) {
            var gcmm = GlobalContainer.MasterManager;
            var characterData = gcmm.CharacterMaster.Get(characterID);
            var targetEvolutionLevel = (byte)(characterData.InitialEvolutionLevel + 1);
            var characterEvolutionType = gcmm.CharacterMaster.GetCharacterEvolutionType(characterData.EvolutionType, targetEvolutionLevel);
            var needSoul = characterEvolutionType != null ? characterEvolutionType.Soul : uint.MaxValue;
            needSoulList["chara" + characterID] = needSoul;
            needSoulList["targetEvolutionLevel" + characterID] = targetEvolutionLevel;
        }
    }
}
