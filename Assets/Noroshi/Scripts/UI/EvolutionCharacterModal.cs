﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using UniLinq;

namespace Noroshi.UI {
    public class EvolutionCharacterModal : MonoBehaviour {
        [SerializeField] Canvas canvas;
        [SerializeField] Image imgBackground;
        [SerializeField] Image imgRibbon;
        [SerializeField] Text txtName;
        [SerializeField] Text txtPopularName;
        [SerializeField] Text txtStatus;
        [SerializeField] Text txtEvolution;
        [SerializeField] Text txtAlertEvolutionUp;
        [SerializeField] Text txtPrevStrength;
        [SerializeField] Text txtNewStrength;
        [SerializeField] Text txtPrevStrengthGrowth;
        [SerializeField] Text txtNewStrengthGrowth;
        [SerializeField] Text txtPrevIntellect;
        [SerializeField] Text txtNewIntellect;
        [SerializeField] Text txtPrevIntellectGrowth;
        [SerializeField] Text txtNewIntellectGrowth;
        [SerializeField] Text txtPrevAgility;
        [SerializeField] Text txtNewAgility;
        [SerializeField] Text txtPrevAgilityGrowth;
        [SerializeField] Text txtNewAgilityGrowth;
        [SerializeField] GameObject characterContainer;
        [SerializeField] GameObject[] evolutionStarList;
        [SerializeField] BtnCommon btnCloseEvolutionCharacter;
        [SerializeField] Sprite[] spriteBgList;
        [SerializeField] Sprite[] spriteRibbonList;
        [SerializeField] ParticleSystem characterGetParticle;

        public Subject<bool> OnCloseEvolutionCharacterModal = new Subject<bool>();

        private int characterSize = 40;
        private SkeletonAnimation skeletonAnimation;

        private void Start() {
            TweenNull.Add(btnCloseEvolutionCharacter.gameObject, 2.0f).Then(() => {
                btnCloseEvolutionCharacter.SetEnable(true);
            });
            btnCloseEvolutionCharacter.OnClickedBtn.Subscribe(_ => {
                OnCloseEvolutionCharacterModal.OnNext(true);
                gameObject.SetActive(false);
            });
            btnCloseEvolutionCharacter.OnPlaySE.Subscribe(_ => {
                GlobalContainer.SoundManager.Play(SoundController.SEKeys.SELECT).Subscribe();
            });
        }

        private void SetStatus(Core.WebApi.Response.Master.Character characterData) {
            var gclm = GlobalContainer.LocalizationManager;
            var tagFlags = new Noroshi.Core.Game.Character.CharacterTagSet(characterData.TagFlags);
            var positionText = "";
            var typeText = "";
            var tribeText = "";
            var statusList = BattleCharacterSelect.Instance.GetCharacterStatusList();

            foreach(var status in statusList.Where(s => s.CharacterID == characterData.ID)) {
                txtPrevStrength.text = status.Strength.ToString();
                txtPrevIntellect.text = status.Intellect.ToString();
                txtPrevAgility.text = status.Agility.ToString();
                txtPrevStrengthGrowth.text = (Mathf.Round(status.StrengthGrowth * 10) / 10).ToString();
                txtPrevIntellectGrowth.text = (Mathf.Round(status.IntellectGrowth * 10) / 10).ToString();
                txtPrevAgilityGrowth.text = (Mathf.Round(status.AgilityGrowth * 10) / 10).ToString();
            }
            txtName.text = gclm.GetText(characterData.TextKey + ".Name");
            txtPopularName.text = gclm.GetText(characterData.TextKey + ".Title." + characterData.InitialEvolutionLevel);
            txtEvolution.text = gclm.GetText("UI.Noun.EvolutionLevel" + characterData.InitialEvolutionLevel);
            for(int i = 0, l = characterData.InitialEvolutionLevel; i < l; i++) {
                evolutionStarList[i].SetActive(true);
            }
            if((byte)characterData.Position == 1) {
                positionText = gclm.GetText("UI.Noun.PositionFront");
            } else if((byte)characterData.Position == 2) {
                positionText = gclm.GetText("UI.Noun.PositionCentral");
            } else {
                positionText = gclm.GetText("UI.Noun.PositionBack");
            }
            if((byte)characterData.Type == 1) {
                typeText = gclm.GetText("UI.Noun.TypeStrength");
            } else if((byte)characterData.Type == 2) {
                typeText = gclm.GetText("UI.Noun.TypeIntellect");
            } else {
                typeText = gclm.GetText("UI.Noun.TypeAgility");
            }
            if(tagFlags.HasTag(9)) {
                tribeText = gclm.GetText("UI.Noun.Human");
                imgRibbon.sprite = spriteRibbonList[0];
                imgBackground.sprite = spriteBgList[0];
            } else if(tagFlags.HasTag(10)) {
                tribeText = gclm.GetText("UI.Noun.Demon");
                imgRibbon.sprite = spriteRibbonList[1];
                imgBackground.sprite = spriteBgList[1];
            } else {
                tribeText = gclm.GetText("UI.Noun.Ham");
                imgRibbon.sprite = spriteRibbonList[2];
                imgBackground.sprite = spriteBgList[2];
            }
            txtStatus.text = positionText + " / " + typeText + " / " + tribeText;
            BattleCharacterSelect.Instance.ReloadCharacter(characterData.ID);
        }

        private void SetNewStatus(Noroshi.Game.CharacterStatus newStatus, Core.WebApi.Response.Master.Character characterData) {
            var gclm = GlobalContainer.LocalizationManager;

            if(newStatus.EvolutionLevel == 2) {
                txtAlertEvolutionUp.text = gclm.GetText("UI.Alert.ChangeSkinAtEvolutionLevel3");
            } else if(newStatus.EvolutionLevel == 3) {
                if(newStatus.TagSet.IsDeca) {
                    txtAlertEvolutionUp.text = gclm.GetText("UI.Alert.ChangeSkinAtEvolutionLevel4");
                } else {
                    txtAlertEvolutionUp.text = gclm.GetText("UI.Alert.ChangeSkinAtEvolutionLevel5");
                }
            } else if(newStatus.EvolutionLevel == 4) {
                txtAlertEvolutionUp.text = gclm.GetText("UI.Alert.ChangeSkinAtEvolutionLevel5");
            } else {
                txtAlertEvolutionUp.text = gclm.GetText("UI.Alert.MaxEvolutionLevel");
            }
            txtAlertEvolutionUp.gameObject.SetActive(true);
            txtPopularName.text = gclm.GetText(characterData.TextKey + ".Title." + newStatus.EvolutionLevel);
            txtEvolution.text = gclm.GetText("UI.Noun.EvolutionLevel" + newStatus.EvolutionLevel);
            for(int i = 0, l = newStatus.EvolutionLevel; i < l; i++) {
                evolutionStarList[i].SetActive(true);
            }
            txtNewStrength.text = newStatus.Strength.ToString();
            txtNewIntellect.text = newStatus.Intellect.ToString();
            txtNewAgility.text = newStatus.Agility.ToString();
            txtNewStrengthGrowth.text = (Mathf.Round(newStatus.StrengthGrowth * 10) / 10).ToString();
            txtNewIntellectGrowth.text = (Mathf.Round(newStatus.IntellectGrowth * 10) / 10).ToString();
            txtNewAgilityGrowth.text = (Mathf.Round(newStatus.AgilityGrowth * 10) / 10).ToString();

            skeletonAnimation.skeleton.SetSkin("step" + newStatus.SkinLevel);
            skeletonAnimation.skeleton.SetSlotsToSetupPose();
            skeletonAnimation.gameObject.SetActive(true);
        }

        public void OpenModal(uint characterID, Camera camera = null) {
            var characterData = GlobalContainer.MasterManager.CharacterMaster.Get(characterID);
            var character = Instantiate<GameObject>(GlobalContainer.AssetBundleManager.LoadFromCharacterPrefab(characterID, "UICharacter"));

            BattleCharacterSelect.Instance.OnReloadStatus.Subscribe(newStatus => {
                SetNewStatus(newStatus, characterData);
            }).AddTo(this);
            canvas.worldCamera = camera != null ? camera : Camera.main;
            skeletonAnimation = character.GetComponent<SkeletonAnimation>();
            foreach(var atlas in skeletonAnimation.skeletonDataAsset.atlasAssets) {
                foreach(var m in atlas.materials) {
                    var shader = m.shader;
                    m.shader = null;
                    m.shader = shader;
                }
            }
            character.SetActive(false);
            character.transform.SetParent(characterContainer.transform);
            character.transform.localScale = new Vector3(-characterSize, characterSize, characterSize);
            character.transform.localPosition = Vector3.zero;
            character.GetComponent<MeshRenderer>().sortingOrder = 352;

            characterGetParticle.gameObject.SetActive(false);
            TweenNull.Add(gameObject, 0.2f).Then(() => {
                characterGetParticle.gameObject.SetActive(true);
            });
            SetStatus(characterData);

            GlobalContainer.SoundManager.Play(SoundController.SEKeys.EVOLUTION).Subscribe();
        }
    }
}
