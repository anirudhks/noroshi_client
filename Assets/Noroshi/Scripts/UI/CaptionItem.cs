﻿using UnityEngine;
using UnityEngine .UI;
using System.Collections;

namespace Noroshi.UI {
    public class CaptionItem : MonoBehaviour {
        public class ItemInfo {
            public string Name;
            public uint HaveNum;
            public uint GetNum;
            public uint NeedLevel = 1;
            public uint Price;
            public uint ID;
            public uint Rarity;
            public string Description;
            public int Index;
            public byte Category;
        }

        [SerializeField] Text txtName;
        [SerializeField] Text txtHaveNum;
        [SerializeField] Text txtNeedLv;
        [SerializeField] Image thumbItem;
        [SerializeField] Image imgItemFrame;
        [SerializeField] Text txtDescription;
        [SerializeField] Sprite[] spriteItemFrameList;
        [SerializeField] Sprite[] spritePieceFrameList;

        public void ShowCaption(ItemInfo itemInfo, Vector3 position) {
            position.y += 1.2f;
            txtName.text = itemInfo.Name;
            txtHaveNum.text = itemInfo.HaveNum.ToString();
//            txtNeedLv.text = itemInfo.NeedLevel.ToString();
            thumbItem.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(itemInfo.ID);
            if(itemInfo.Category == (byte)Noroshi.Core.Game.Possession.PossessionCategory.GearPiece
               || itemInfo.Category == (byte)Noroshi.Core.Game.Possession.PossessionCategory.Soul) {
                imgItemFrame.sprite = spritePieceFrameList[itemInfo.Rarity - 1];
            } else {
                imgItemFrame.sprite = spriteItemFrameList[itemInfo.Rarity - 1];
            }
            txtDescription.text = itemInfo.Description;
            gameObject.transform.position = position;
            gameObject.SetActive(true);
        }
        
        public void HideCaption() {
            gameObject.SetActive(false);
        }
    }
}
