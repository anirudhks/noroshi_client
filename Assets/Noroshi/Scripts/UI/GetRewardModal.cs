﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.Core.Game.Possession;

namespace Noroshi.UI {
    public class GetRewardModal : MonoBehaviour {
        [SerializeField] BtnCommon btnOverlay;
        [SerializeField] Text txtRewardTitle;
        [SerializeField] GameObject[] getRewardList;
        [SerializeField] ItemIcon[] itemIconList;
        [SerializeField] Text[] txtGetRewardList;

        public Subject<bool> OnClose = new Subject<bool>();

        private void Start() {
            btnOverlay.OnClickedBtn.Subscribe(_ => {
                CloseModal();
            });
            btnOverlay.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        public void OpenModal(Noroshi.Core.WebApi.Response.Possession.PossessionObject[] getList, string title = null) {
            var gclm = GlobalContainer.LocalizationManager;
            var getItemLength = getList.Length;
            txtRewardTitle.text = title == null ? gclm.GetText("UI.Heading.Reward") : title;
            for(int i = 0, l = getRewardList.Length; i < l; i++) {
                if(i < getItemLength) {
                    itemIconList[i].SetItemIcon(getList[i]);
                    if(getList[i].Category == (byte)PossessionCategory.Status) {
                        if(getList[i].ID == (byte)PossessionStatusID.Gold) {
                            PlayerInfo.Instance.ChangeHaveGold((int)getList[i].Num);
                        } else if(getList[i].ID == (byte)PossessionStatusID.CommonGem
                            || getList[i].ID == (byte)PossessionStatusID.FreeGem) {
                            PlayerInfo.Instance.ChangeHaveGem((int)getList[i].Num);
                        }
                    } else if(getList[i].Category == (byte)PossessionCategory.Character) {
                        var pcID = BattleCharacterSelect.Instance.GetPlayerCharacterId(new uint[]{getList[i].ID});
                        var getModal = Instantiate(Resources.Load<GetCharacterModal>("UI/GetCharacterModal"));
                        getModal.OpenModal(getList[i].ID);
                        if(pcID.Length < 1) {
                            ItemListManager.Instance.UpdateNeedSoul(getList[i].ID);
                        }
                    } else if(getList[i].Category != (byte)PossessionCategory.Status) {
                        var isSoul = getList[i].Category == (byte)PossessionCategory.Soul;
                        ItemListManager.Instance.ChangeItemCount(getList[i].ID, (int)getList[i].Num, isSoul);
                    }
                    if(getList[i].Num > 1) {
                        txtGetRewardList[i].text = gclm.GetText(getList[i].TextKey + ".Name") + " x" + getList[i].Num;
                    } else {
                        txtGetRewardList[i].text = gclm.GetText(getList[i].TextKey + ".Name");
                    }
                    getRewardList[i].SetActive(true);
                } else {
                    getRewardList[i].SetActive(false);
                }
            }
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
        }

        public void CloseModal() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                gameObject.SetActive(false);
                OnClose.OnNext(true);
            });
        }
    }
}
