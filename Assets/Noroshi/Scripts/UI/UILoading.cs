﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UniRx;

namespace Noroshi.UI {
    public class UILoading : MonoBehaviour {
        [SerializeField] Canvas canvas;
        [SerializeField] GameObject loading;
        [SerializeField] GameObject loadingBar;
        [SerializeField] GameObject transitionOverlay;
        [SerializeField] Text txtTitle;
        [SerializeField] Text txtDescription;
        [SerializeField] GameObject btnBack;

        public static UILoading Instance;

        public List<string> HistoryList {
            get { return historyList; }
        }
        public Canvas Canvas { get { return canvas; } }

        private static readonly List<string> historyList = new List<string>();
        private static Dictionary<string, int> queryList = new Dictionary<string, int>();
        private static Dictionary<string, List<int>> multiQueryList = new Dictionary<string, List<int>>();
        private AsyncOperation async;
        private string sceneName;
        private float loadingBarWidth;
        private bool isEndTransition;


        private void Awake() {
            if(Instance == null) {Instance = this;}
            GameObject[] obj = GameObject.FindGameObjectsWithTag("UILoading");
            if(obj.Length > 1) {
                DestroyImmediate(gameObject);
            } else {
                ShowTips();
                DontDestroyOnLoad(gameObject);
                loadingBarWidth = loadingBar.GetComponent<RectTransform>().sizeDelta.x;
            }
        }

        private void OnLevelWasLoaded(int level) {
            canvas.worldCamera = Camera.main;
            if(SceneManager.GetActiveScene().name == Constant.SCENE_LOGIN) {
                loading.SetActive(false);
            } else {
                loading.SetActive(true);
                loadingBar.transform.localPosition = new Vector3(0, 0, 0);
            }
            if(HistoryList.Count > 0) {
                if(GetPreviousHistory() == Constant.SCENE_BATTLE
                    || GetPreviousHistory() == Constant.SCENE_CHARACTER_LIST) {
                    if(SoundController.Instance != null) {
                        SoundController.Instance.StopBGM();
                    }
                    GlobalContainer.SoundManager.Clear();
                }
                if(GetPreviousHistory() == Constant.SCENE_BATTLE
                    && SceneManager.GetActiveScene().name != Constant.SCENE_BATTLE) {
                    Resources.UnloadUnusedAssets();
                    System.GC.Collect();
                }
            }
            TweenNull.Add(loading, 8).Then(() => {btnBack.SetActive(true);});
        }

        private IEnumerator ChangeScene() {
            isEndTransition = false;
            ShowTips();
            transitionOverlay.SetActive(true);
            TweenX.Add(transitionOverlay, 0.4f, 0).EaseOutCubic()
                .From(Constant.SCREEN_BASE_WIDTH * 1.5f).Then(() => {
                    loading.SetActive(true);
                    transitionOverlay.SetActive(false);
                    isEndTransition = true;
                    loadingBar.transform.localPosition = new Vector3(-loadingBarWidth, 0, 0);
                    async = SceneManager.LoadSceneAsync(sceneName);
                    async.allowSceneActivation= false;
                });

            while(!isEndTransition || async.progress < 0.9f) {
                if(isEndTransition) {
                    var xx = loadingBarWidth - loadingBarWidth * async.progress;
                    loadingBar.transform.localPosition = new Vector3(-xx, 0, 0);
                }
                yield return new WaitForEndOfFrame();
            }
            HistoryList.Add(SceneManager.GetActiveScene().name);
            async.allowSceneActivation = true;
        }

        private void ShowTips() {
            var tips = GlobalContainer.MasterManager.HelpMaster.LotTip();
            txtTitle.text = GlobalContainer.LocalizationManager.GetText(tips.TextKey + ".Title");
            txtDescription.text = GlobalContainer.LocalizationManager.GetText(tips.TextKey + ".Body");
        }

        public void LoadScene(string scene) {
            sceneName = scene;
            StartCoroutine(ChangeScene());
        }

        public void SetLoadBtn(BtnLoadLevel btn) {
            btn.OnChangeLevel.Subscribe(name => {
                sceneName = name;
                StartCoroutine(ChangeScene());
            }).AddTo(btn);
        }

        public void ShowLoading() {
            ShowTips();
            transitionOverlay.SetActive(true);
            TweenX.Add(transitionOverlay, 0.4f, 0).EaseOutCubic()
                .From(Constant.SCREEN_BASE_WIDTH * 1.5f).Then(() => {
                    loading.SetActive(true);
                    transitionOverlay.SetActive(false);
            });
        }

        public void HideLoading() {
            TweenNull.Add(gameObject, 0.2f).Then(() => {
                transitionOverlay.transform.localPosition = Vector3.zero;
                transitionOverlay.SetActive(true);
                loading.SetActive(false);
                TweenX.Add(transitionOverlay, 0.4f, -Constant.SCREEN_BASE_WIDTH * 1.5f)
                    .EaseInCubic().Then(() => {
                        transitionOverlay.SetActive(false);
                    });
            });
        }

        public bool GetLoadingEnd() {
            return !loading.activeSelf;
        }

        public void AddHistory(string sceneName) {
            HistoryList.Add(sceneName);
        }

        public string GetPreviousHistory() {
            if(HistoryList.Count > 0) {
                return HistoryList[HistoryList.Count - 1];
            } else {
                return "";
            }
        }

        public void ResetHistory() {
            HistoryList.Clear();
        }

        public int GetQuery(string key) {
            return queryList.ContainsKey(key) ? queryList[key] : -1;
        }

        public void SetQuery(string key, int value) {
            queryList[key] = value;
        }

        public List<int> GetMultiQuery(string key) {
            return multiQueryList.ContainsKey(key) ? multiQueryList[key] : new List<int>();
        }

        public void SetMultiQuery(string key, List<int> values) {
            multiQueryList[key] = values;
        }

        public void RemoveQuery(string key) {
            if(queryList.ContainsKey(key)) {
                queryList.Remove(key);
            }
            if(multiQueryList.ContainsKey(key)) {
                multiQueryList.Remove(key);
            }
        }

        public void ClearQuery() {
            queryList.Clear();
            multiQueryList.Clear();
        }

        public void Destroy() {
            DestroyImmediate(gameObject);
            Instance = null;
        }
    }
}
