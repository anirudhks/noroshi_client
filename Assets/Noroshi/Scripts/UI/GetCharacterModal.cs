﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class GetCharacterModal : MonoBehaviour {
        [SerializeField] Canvas canvas;
        [SerializeField] Image imgBackground;
        [SerializeField] Image imgRibbon;
        [SerializeField] Text txtName;
        [SerializeField] Text txtPopularName;
        [SerializeField] Text txtStatus;
        [SerializeField] Text txtCharacterDescription;
        [SerializeField] Text txtSkillName;
        [SerializeField] Text txtSkillDescription;
        [SerializeField] Text txtEvolution;
        [SerializeField] GameObject characterContainer;
        [SerializeField] GameObject[] evolutionStarList;
        [SerializeField] BtnCommon btnCloseGetCharacter;
        [SerializeField] Sprite[] spriteBgList;
        [SerializeField] Sprite[] spriteRibbonList;
        [SerializeField] GameObject acquiredAlert;
        [SerializeField] ParticleSystem characterGetParticle;

        public Subject<bool> OnCloseGetCharacterModal = new Subject<bool>();

        private uint soulID;
        private int getNum;
        private int characterSize = 40;

        private void Start() {
            TweenNull.Add(btnCloseGetCharacter.gameObject, 2.0f).Then(() => {
                btnCloseGetCharacter.SetEnable(true);
            });
            btnCloseGetCharacter.OnClickedBtn.Subscribe(_ => {
                OnCloseGetCharacterModal.OnNext(true);
                gameObject.SetActive(false);
            });
            btnCloseGetCharacter.OnPlaySE.Subscribe(_ => {
                GlobalContainer.SoundManager.Play(SoundController.SEKeys.SELECT).Subscribe();
            });
        }

        private void SetStatus(Core.WebApi.Response.Master.Character characterData) {
            var gclm = GlobalContainer.LocalizationManager;
            var skill = GlobalContainer.MasterManager.CharacterMaster.GetAction(characterData.ActionID1);
            var tagFlags = new Noroshi.Core.Game.Character.CharacterTagSet(characterData.TagFlags);
            var positionText = "";
            var typeText = "";
            var tribeText = "";
            txtName.text = gclm.GetText(characterData.TextKey + ".Name");
            txtPopularName.text = gclm.GetText(characterData.TextKey + ".Title." + characterData.InitialEvolutionLevel);
            txtEvolution.text = gclm.GetText("UI.Noun.EvolutionLevel" + characterData.InitialEvolutionLevel);
            for(int i = 0, l = characterData.InitialEvolutionLevel; i < l; i++) {
                evolutionStarList[i].SetActive(true);
            }
            if((byte)characterData.Position == 1) {
                positionText = gclm.GetText("UI.Noun.PositionFront");
            } else if((byte)characterData.Position == 2) {
                positionText = gclm.GetText("UI.Noun.PositionCentral");
            } else {
                positionText = gclm.GetText("UI.Noun.PositionBack");
            }
            if((byte)characterData.Type == 1) {
                typeText = gclm.GetText("UI.Noun.TypeStrength");
            } else if((byte)characterData.Type == 2) {
                typeText = gclm.GetText("UI.Noun.TypeIntellect");
            } else {
                typeText = gclm.GetText("UI.Noun.TypeAgility");
            }
            if(tagFlags.HasTag(9)) {
                tribeText = gclm.GetText("UI.Noun.Human");
                imgRibbon.sprite = spriteRibbonList[0];
                imgBackground.sprite = spriteBgList[0];
            } else if(tagFlags.HasTag(10)) {
                tribeText = gclm.GetText("UI.Noun.Demon");
                imgRibbon.sprite = spriteRibbonList[1];
                imgBackground.sprite = spriteBgList[1];
            } else {
                tribeText = gclm.GetText("UI.Noun.Ham");
                imgRibbon.sprite = spriteRibbonList[2];
                imgBackground.sprite = spriteBgList[2];
            }
            txtStatus.text = positionText + " / " + typeText + " / " + tribeText;
            txtSkillName.text = gclm.GetText(skill.TextKey + ".Name");
            txtSkillDescription.text = gclm.GetText(skill.TextKey + ".Description");
        }

        public void OpenModal(uint characterID, Camera camera = null) {
            var characterData = GlobalContainer.MasterManager.CharacterMaster.Get(characterID);
            var pcID = BattleCharacterSelect.Instance.GetPlayerCharacterId(new uint[]{characterID});
            var character = Instantiate<GameObject>(GlobalContainer.AssetBundleManager.LoadFromCharacterPrefab(characterID, "UICharacter"));
            var skeletonAnimation = character.GetComponent<SkeletonAnimation>();
            var tagFlags = new Noroshi.Core.Game.Character.CharacterTagSet(characterData.TagFlags);
            var skinLevel = new Noroshi.Core.Game.Character.Skin((byte)characterData.InitialEvolutionLevel, tagFlags.HasTag(0)).GetSkinLevel();

            canvas.worldCamera = camera != null ? camera : Camera.main;
            foreach(var atlas in skeletonAnimation.skeletonDataAsset.atlasAssets) {
                foreach(var m in atlas.materials) {
                    var shader = m.shader;
                    m.shader = null;
                    m.shader = shader;
                }
            }
            skeletonAnimation.skeleton.SetSkin("step" + skinLevel);
            skeletonAnimation.skeleton.SetSlotsToSetupPose();
            skeletonAnimation.state.SetAnimation(0, Constant.ANIM_RUN, true);

            character.transform.SetParent(characterContainer.transform);
            character.transform.localScale = new Vector3(-characterSize, characterSize, characterSize);
            character.transform.localPosition = Vector3.zero;
            character.GetComponent<MeshRenderer>().sortingOrder = 302;

            characterGetParticle.gameObject.SetActive(false);
            TweenNull.Add(gameObject, 0.2f).Then(() => {
                if(pcID.Length < 1) {characterGetParticle.gameObject.SetActive(true);}
            });
            SetStatus(characterData);

            if(pcID.Length < 1) {
                acquiredAlert.SetActive(false);
                BattleCharacterSelect.Instance.SetNewCharacter(characterID);
            } else {
                soulID = GlobalContainer.MasterManager.ItemMaster.GetSoulByCharacterID(characterID).ID;
                if(tagFlags.HasTag(0)) {
                    getNum = Noroshi.Core.Game.Character.Constant.DECA_SOUL_NUM_AT_DUPLICATE;
                } else if(characterData.InitialEvolutionLevel == 1) {
                    getNum = Noroshi.Core.Game.Character.Constant.INITIAL_EVOLUTION_LEVEL1_SOUL_NUM_AT_DUPLICATE;
                } else if(characterData.InitialEvolutionLevel == 2) {
                    getNum = Noroshi.Core.Game.Character.Constant.INITIAL_EVOLUTION_LEVEL2_SOUL_NUM_AT_DUPLICATE;
                } else {
                    getNum = Noroshi.Core.Game.Character.Constant.INITIAL_EVOLUTION_LEVEL3_SOUL_NUM_AT_DUPLICATE;
                }
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    ItemListManager.Instance.ChangeItemCount(soulID, getNum, true);
                });
                acquiredAlert.SetActive(true);
            }
            GlobalContainer.SoundManager.Play(SoundController.SEKeys.EVOLUTION).Subscribe();
        }
    }
}
