using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using MsgPack.Serialization;
using UniRx;
using UnityEngine;
using Noroshi.Core.WebApi.Response;
using Noroshi.MessagePackSerializers;
using Noroshi.GlobalModal;

namespace Noroshi.WebApi
{
    /// <summary>
    /// Web API リクエストを扱うクラス。
    /// </summary>
    public class WebApiRequester : IWebApiRequester
    {
        /// <summary>
        /// タイムアウト時間。
        /// </summary>
        readonly TimeSpan DUE_TIME = TimeSpan.FromSeconds(10);

        /// <summary>
        /// リクエスト時のヘッダー。
        /// </summary>
        Dictionary<string, string> _headers
        {
            get
            {
                var headers = new Dictionary<string, string>();
                headers["x-device-os"] = SystemInfo.operatingSystem;
                headers["x-device-model"] = SystemInfo.deviceModel;
                headers["x-device-unique-identifier"] = SystemInfo.deviceUniqueIdentifier;
                headers["x-noroshi-app-version"] = AppConstant.APP_VERSION;
                if (!string.IsNullOrEmpty(GlobalContainer.SessionID)) headers["x-noroshi-session-id"] = GlobalContainer.SessionID;
                if (!string.IsNullOrEmpty(GlobalContainer.MasterVersion)) headers["x-noroshi-master-version"] = GlobalContainer.MasterVersion;
                return headers;
            }
        }

        /// <summary>
        /// 開発効率化のために Unity Editor 上で利用するログイン処理。
        /// </summary>
        public IObservable<bool> LoginForUnityEditor()
        {
            if (!string.IsNullOrEmpty(GlobalContainer.SessionID)) return Observable.Return(true);
#if UNITY_EDITOR || UNITY_STANDALONE
            var form = new WWWForm();
            form.AddField("deviceIdentifier", SystemInfo.deviceUniqueIdentifier);
            return Post<Noroshi.Core.WebApi.Response.Account.LoginResponse>("Account/LoginWithNoroshiAccount", form)
                .SelectMany(response =>
                {
                    if (response.Error == null)
                    {
                        GlobalContainer.SetPlayerIDAndSessionID(response.PlayerID, response.SessionID);
                        return GlobalContainer.Load(0);
                    }
                    else
                    {
                        return Observable.Return(false);
                    }
                });
#else
            return Observable.Return(false);
#endif
        }


        readonly string _baseUrl;


        public WebApiRequester()
        {
            _baseUrl = GlobalContainer.Config.WebApiHost + "/api/";
        }

        /// <summary>
        /// GET リクエストを送信する。
        /// </summary>
        /// <param name="path">相対パス（クエリストリング含む）</param>
        public IObservable<TRes> Get<TRes>(string path)
        {
            var url = _baseUrl + path;
            GlobalContainer.Logger.Debug("[REQUEST]" + url);
            return _request<TRes>(ObservableWWW.GetAndGetBytes(url, _headers))
                .Do(_ => GlobalContainer.Logger.Debug("[RESPONSE]" + url));
        }
        /// <summary>
        /// GET リクエストを送信する。
        /// </summary>
        /// <param name="path">相対パス（クエリストリング含まず）</param>
        /// <param name="requestParam">クエリストリング生成のためのパラメータ</param>
        public IObservable<TRes> Get<TReq, TRes>(string path, TReq requestParam)
        {
            var strBuilder = new System.Text.StringBuilder();
            var propertyInfos = typeof(TReq).GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                var value = propertyInfo.GetGetMethod().Invoke(requestParam, null);
                // TODO : ちゃんと実装
                if (propertyInfo.PropertyType == typeof(uint[]))
                {
                    foreach (var v in (uint[])value)
                    {
                        strBuilder.Append(propertyInfo.Name + "=" + v.ToString() + "&");
                    }
                    strBuilder.Remove(strBuilder.Length - 1, 1);
                }
                else
                {
                    strBuilder.Append(propertyInfo.Name + "=" + value.ToString() + "&");
                }
            }
            strBuilder.Remove(strBuilder.Length - 1, 1);

            return Get<TRes>(path + "?" + strBuilder.ToString());
        }

        /// <summary>
        /// POST リクエストを送信する。
        /// </summary>
        /// <param name="path">相対パス</param>
        /// <param name="requestParam">POST パラメータ</param>
        public IObservable<TRes> Post<TReq, TRes>(string path, TReq requestParam)
        {
            var form = new WWWForm();
            // 動的にパラメータを構築してみる。
            var propertyInfos = typeof(TReq).GetProperties();
            foreach (var propertyInfo in propertyInfos) {
                var value = propertyInfo.GetGetMethod().Invoke(requestParam, null);
                // TODO : ちゃんと実装
                if (propertyInfo.PropertyType == typeof(uint[])) {
                    foreach (var v in (uint[])value) {
                        form.AddField(propertyInfo.Name, v.ToString());
                    }
                } else if (propertyInfo.PropertyType == typeof(ushort[])) {
                    foreach (var v in (ushort[])value)
                    {
                        form.AddField(propertyInfo.Name, v.ToString());
                    }
                } else {
                    form.AddField(propertyInfo.Name, value != null ? value.ToString() : "null");
                }
            }
            return Post<TRes>(path, form);
        }
        /// <summary>
        /// POST リクエストを送信する。
        /// </summary>
        /// <param name="path">相対パス</param>
        public IObservable<T> Post<T>(string path)
        {
            var form = new WWWForm();
            form.AddField("_d", 1); // Dummy これがないと動かない
            return Post<T>(path, form);
        }
        /// <summary>
        /// POST リクエストを送信する。
        /// </summary>
        /// <param name="path">相対パス</param>
        /// <param name="form">フォームデータ（POST パラメータ）</param>
        public IObservable<T> Post<T>(string path, WWWForm form)
        {
            var url = _baseUrl + path;
            GlobalContainer.Logger.Debug("[REQUEST]" + url);
            return _request<T>(ObservableWWW.PostAndGetBytes(url, form, _headers))
                .Do(_ => GlobalContainer.Logger.Debug("[RESPONSE]" + url));
        }

        IObservable<T> _request<T>(IObservable<byte[]> requestAndGetBytes)
        {
            IObservable<T> request = null;
            request = requestAndGetBytes
                .Timeout(DUE_TIME)
                .Select(bytes => _deserialize<T>(bytes))
                .Catch((InvalidAppVersionException error) =>
                {
                    return GlobalContainer.GlobalModalHandler.Open(GlobalModalPanel.AppVersionPanel)
                        .SelectMany(globalModalEvent =>
                        {
                            return Observable.Empty<T>();
                        });
                })
                .Catch((InvalidMasterVersionException error) =>
                {
                    return GlobalContainer.GlobalModalHandler.Open(GlobalModalPanel.MasterVersionPanel)
                        .SelectMany(globalModalEvent =>
                        {
                            return Observable.Empty<T>();
                        });
                })
                .Catch((SessionTimeOutException error) =>
                {
                    return GlobalContainer.GlobalModalHandler.Open(GlobalModalPanel.SessionTimeOutPanel)
                        .SelectMany(globalModalEvent =>
                        {
                            return Observable.Empty<T>();
                        });
                })
                .Catch((SessionNotFoundException error) =>
                {
                    return GlobalContainer.GlobalModalHandler.Open(GlobalModalPanel.SessionNotFoundPanel)
                        .SelectMany(globalModalEvent =>
                        {
                            return Observable.Empty<T>();
                        });
                })
                .Catch((TimeoutException error) => Observable.Throw<T>(error))
                .Catch((WWWErrorException error) => Observable.Throw<T>(error))
                .Catch((Exception error) =>
                {
                    // 例外が発生した場合は専用グローバルモーダルを表示。
                    return GlobalContainer.GlobalModalHandler.Open(GlobalModalPanel.WwwErrorPanel)
                        .SelectMany(globalModalEvent =>
                        {
                            if (globalModalEvent.Command == GlobalModalCommand.Retry)
                            {
                                return request;
                            }
                            else
                            {
                                return Observable.Throw<T>(error);
                            }
                        });
                });
            return request.PublishLast().RefCount();
        }

        static T _deserialize<T>(byte[] bytes)
        {
            var stream = new MemoryStream(bytes);
            var serializer = SerializationContext.Default.GetSerializer<ServerResponse<T>>();
            var serverResponse = serializer.Unpack(stream);
            GlobalContainer.TimeHandler.SynchronizeWithServerTime(serverResponse.ServerUnixTime);
            if (serverResponse.GlobalError != null)
            {
                if (serverResponse.GlobalError.InvalidAppVersion)
                {
                    throw new InvalidAppVersionException();
                }
                else if (serverResponse.GlobalError.InvalidMasterVersion)
                {
                    throw new InvalidMasterVersionException();
                }
                else if (serverResponse.GlobalError.SessionNotFound)
                {
                    throw new SessionNotFoundException();
                }
                else if (serverResponse.GlobalError.SessionTimeout)
                {
                    throw new SessionTimeOutException();
                }
            }
            return serverResponse.Content;
        }

        class InvalidAppVersionException : Exception
        {
        }
        class InvalidMasterVersionException : Exception
        {
        }
        class SessionTimeOutException : Exception
        {
        }
        class SessionNotFoundException : Exception
        {
        }
    }
}
