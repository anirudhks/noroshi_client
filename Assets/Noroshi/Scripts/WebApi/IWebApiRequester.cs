using UniRx;
using UnityEngine;

namespace Noroshi.WebApi
{
    public interface IWebApiRequester
    {
        IObservable<RES> Get<RES>(string path);
        IObservable<RES> Get<REQ, RES>(string path, REQ requestParam);
        IObservable<RES> Post<RES>(string path);
        IObservable<RES> Post<REQ, RES>(string path, REQ requestParam);
        IObservable<RES> Post<RES>(string path, WWWForm from);
        IObservable<bool> LoginForUnityEditor();
    }
}
