﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class ChangeNamePanel : MonoBehaviour {
        [SerializeField] InputField inputField;
        [SerializeField] BtnCommon btnDecideName;
        [SerializeField] BtnCommon btnCloseChangeName;

        public Subject<string> OnDecideName = new Subject<string>();

        private bool isOpen = false;

        private void Start() {
            btnDecideName.OnClickedBtn.Subscribe(_ => {
                SetPlayerName();
            });
            btnDecideName.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnCloseChangeName.OnClickedBtn.Subscribe(_ => {
                CloseChangeNamePanel();
            });
            btnCloseChangeName.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModalChild.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseChangeNamePanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private bool IsNullOrWhiteSpace(string value) {
            return value == null || value.Trim() == "";
        }

        private void SetPlayerName() {
            var text = inputField.text;
            OnDecideName.OnNext(text);
            CloseChangeNamePanel();
        }

        public void OpenChangeNamePanel() {
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
        }

        public void CloseChangeNamePanel() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalChildOpen(false);
            });
        }

        public void OnChangeInputValue(string name) {
            var text = inputField.text;
            if(string.IsNullOrEmpty(text) || IsNullOrWhiteSpace(text)) {
                btnDecideName.SetEnable(false);
            } else {
                btnDecideName.SetEnable(true);
            }
        }
    }
}
