﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;

namespace Noroshi.UI {
    public class InformationController : MonoBehaviour {
        [SerializeField] ScrollRect informationPanelScroller;
        [SerializeField] GameObject informationPanelWrapper;
        [SerializeField] GameObject informationDetailWrapper;
        [SerializeField] InformationPanel informationPanelPref;
        [SerializeField] InformationDetail informationDetailPref;
        [SerializeField] Text txtUnreadNum;
        [SerializeField] GameObject alertNoInformation;
        [SerializeField] BtnCommon btnInformation;
        [SerializeField] BtnCommon btnBulkRead;
        [SerializeField] BtnCommon btnClose;
        [SerializeField] GameObject iconNotice;
        [SerializeField] Text txtNoticeNum;

        private List<InformationPanel> panelList = new List<InformationPanel>();
        private List<GameObject> detailList = new List<GameObject>();
        private int openIndex = 0;
        private int unreadCount = 0;
        private bool isCreateInformation = false;
        private bool isSendEventRead = false;
        private bool isSendImportantRead = false;
        private bool isSendCampaignRead = false;
        private bool isSendApologyRead = false;
        private Noroshi.Core.WebApi.Response.Information.Information[] informationDataList;
        private bool isOpen = false;

        private void Start() {
            btnBulkRead.OnClickedBtn.Subscribe(_ => {
                foreach(var panel in panelList) {
                    panel.SetHasRead();
                }
            });
            btnBulkRead.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnClose.OnClickedBtn.Subscribe(_ => {
                CloseInformation();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseInformation();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void CreateInformation() {
            for(int i = 0, l = informationDataList.Length; i < l; i++) {
                var panel = Instantiate(informationPanelPref);
                var detail = Instantiate(informationDetailPref);

                panel.transform.SetParent(informationPanelWrapper.transform);
                panel.transform.localScale = Vector3.one;
                panelList.Add(panel);
                panel.OnReadInformation.Subscribe(SendReadFlag);
                panel.SetPanel(informationDataList[i], i);
                panel.OnClickedBtn.Subscribe(index => {
                    if(index == openIndex) {return;}
                    SelectPanel(index);
                });
                panel.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });

                detail.transform.SetParent(informationDetailWrapper.transform);
                detail.transform.localScale = Vector3.one;
                detail.transform.localPosition = new Vector2(-210, 0);
                detail.gameObject.SetActive(false);
                detailList.Add(detail.gameObject);
                detail.SetDetail(informationDataList[i]);
            }
            txtUnreadNum.text = unreadCount.ToString();
            if(panelList.Count > 0) {
                SelectPanel(0);
            } else {
                alertNoInformation.SetActive(true);
            }
            if(unreadCount <= 0) {btnBulkRead.SetEnable(false);}
        }

        private void SelectPanel(int index) {
            var currentIndex = openIndex;
            openIndex = index;
            for(int i = 0, l = panelList.Count; i < l; i++) {
                panelList[i].SetSelect(i == index);
            }
            if(detailList[currentIndex].activeSelf) {
                TweenX.Add(detailList[currentIndex], 0.15f, -210).EaseOutCubic().Then(() => {
                    detailList[currentIndex].SetActive(false);
                    detailList[index].SetActive(true);
                    TweenX.Add(detailList[index], 0.2f, 250).EaseOutCubic();
                });
            } else {
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    detailList[index].SetActive(true);
                    TweenX.Add(detailList[index], 0.2f, 250).EaseOutCubic();
                });
            }
        }

        private void SendReadFlag(Noroshi.Core.Game.Information.InformationCategory category) {
            unreadCount--;
            if(unreadCount <= 0) {
                unreadCount = 0;
                btnBulkRead.SetEnable(false);
                iconNotice.SetActive(false);
            }
            txtUnreadNum.text = unreadCount.ToString();
            txtNoticeNum.text = unreadCount.ToString();
            if(category == Noroshi.Core.Game.Information.InformationCategory.Event) {
                if(isSendEventRead) {return;} else {isSendEventRead = true;}
            }
            if(category == Noroshi.Core.Game.Information.InformationCategory.Important) {
                if(isSendImportantRead) {return;} else {isSendImportantRead = true;}
            }
            if(category == Noroshi.Core.Game.Information.InformationCategory.Campaign) {
                if(isSendCampaignRead) {return;} else {isSendCampaignRead = true;}
            }
            if(category == Noroshi.Core.Game.Information.InformationCategory.Apology) {
                if(isSendApologyRead) {return;} else {isSendApologyRead = true;}
            }
            var categories = new List<Noroshi.Core.Game.Information.InformationCategory>() {category};
            Noroshi.Information.WebApiRequester.Read(categories).Subscribe();
        }

        public void Init() {
            btnInformation.OnClickedBtn.Subscribe(_ => {
                OpenInformation();
            });
            btnInformation.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            Noroshi.Information.WebApiRequester.List().Do(res => {
                informationDataList = res.Informations;
                btnInformation.SetEnable(true);
                TweenC.Add(btnInformation.gameObject, 0.01f, Color.white);
                for(int i = 0, l = informationDataList.Length; i < l; i++) {
                    if(!informationDataList[i].HasReadInformation) {unreadCount++;}
                    txtNoticeNum.text = unreadCount.ToString();
                    if(unreadCount > 0) {iconNotice.SetActive(true);}
                }
            }).Subscribe();
        }

        private void CloseInformation() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }

        public void OpenInformation() {
            if(!isCreateInformation) {
                CreateInformation();
                isCreateInformation = true;
            }
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            informationPanelScroller.verticalNormalizedPosition = 1;
            if(panelList.Count > 0) {
                panelList[0].OnClickedBtn.OnNext(0);
            }
            BackButtonController.Instance.IsModalOpen(true);
        }
    }
}
