﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using UniLinq;
using DG.Tweening;
using Noroshi.Core.WebApi.Response.Master;

namespace Noroshi.UI {
    public class HelpPiece : MonoBehaviour {
        [SerializeField] LayoutElement layoutElement;
        [SerializeField] BtnCommon headerPanel;
        [SerializeField] Text txtTitle;
        [SerializeField] GameObject openArrow;
        [SerializeField] GameObject closeArrow;
        [SerializeField] GameObject subPanelWrapper;
        [SerializeField] BtnCommon[] subPanelList;
        [SerializeField] Text[] txtSubTitleList;

        public Subject<HelpContent> OnSelectHelp = new Subject<HelpContent>();

        private int childCount = 3;
        private bool isOpen = false;
        private bool isMoving = false;
        private float subPanelWrapperPosition;
        private HelpContent[] helpContentList;

        private void Start() {
            subPanelWrapper.GetComponent<GridLayoutGroup>().enabled = false;
            subPanelWrapper.GetComponent<ContentSizeFitter>().enabled = false;

            headerPanel.OnClickedBtn.Subscribe(_ => {
                if(isMoving) {return;}
                isMoving = true;
                if(isOpen) {
                    ClosePanel();
                } else {
                    OpenPanel();
                }
            });
            headerPanel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.FLICK);
            });

            foreach(var panel in subPanelList) {
                panel.OnClickedBtn.Subscribe(index => {
                    OnSelectHelp.OnNext(helpContentList[index]);
                });
                panel.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }
        }

        private void OpenPanel() {
            isOpen = true;
            openArrow.SetActive(true);
            closeArrow.SetActive(false);
            subPanelWrapper.SetActive(true);
            DOTween.To(() => layoutElement.minHeight, (a) => layoutElement.minHeight = a, childCount * 65 + 85, 0.2f)
                .SetEase(Ease.InCubic);
            TweenY.Add(subPanelWrapper, 0.25f, childCount * 65 + 15).From(subPanelWrapperPosition).EaseInCubic().Then(() => {
                isMoving = false;
            });
        }

        public void ClosePanel() {
            openArrow.SetActive(false);
            closeArrow.SetActive(true);
            DOTween.To(() => layoutElement.minHeight, (a) => layoutElement.minHeight = a, 82, 0.2f)
                .SetEase(Ease.InCubic);
            TweenY.Add(subPanelWrapper, 0.12f, subPanelWrapperPosition).EaseInCubic().Then(() => {
                isOpen = false;
                isMoving = false;
                subPanelWrapper.SetActive(false);
            });
        }

        public void SetContent(HelpCategory data) {
            helpContentList = data.HelpContents.OrderBy(h => h.OrderPriority).ToArray();
            txtTitle.text = GlobalContainer.LocalizationManager.GetText(data.TextKey);
            childCount = data.HelpContents.Length;
            subPanelWrapperPosition = childCount * 65 + 210;
            for(int i = 0, l = subPanelList.Length; i < l; i++) {
                if(i < data.HelpContents.Length) {
                    txtSubTitleList[i].text = GlobalContainer.LocalizationManager.GetText(helpContentList[i].TitleTextKey);
                    subPanelList[i].gameObject.SetActive(true);
                } else {
                    subPanelList[i].gameObject.SetActive(false);
                }
            }
        }
    }
}
