using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.Core.Game.Player;
using Noroshi.Core.Game.GameContent;

namespace Noroshi.UI {
    public class MyPageController : MonoBehaviour {
        [SerializeField] BtnCommon btnOpenPlayerStatus;
        [SerializeField] PlayerStatusController playerStatusController;
        [SerializeField] BtnLoadLevel btnShop;
        [SerializeField] BtnLoadLevel btnMedalShop;
        [SerializeField] BtnLoadLevel btnGacha;
        [SerializeField] BtnLoadLevel btnGuild;
        [SerializeField] BtnLoadLevel btnStory;
        [SerializeField] BtnLoadLevel btnArena;
        [SerializeField] BtnLoadLevel btnTrial;
        [SerializeField] BtnLoadLevel btnTraining;
        [SerializeField] BtnLoadLevel btnExpedition;
        [SerializeField] BtnLoadLevel btnRaidBoss;
        [SerializeField] BtnLoadLevel btnGvG;
        [SerializeField] BtnLoadLevel btnSideStory;
        [SerializeField] BtnCommon btnInformation;
        [SerializeField] GuildChatController guildChatController;
        [SerializeField] InformationController informationController;
        [SerializeField] LoginBonusController loginBonusController;
        [SerializeField] PresentBoxController presentBoxController;
        [SerializeField] QuitApplicationPanel quitApplicationPanel;
        [SerializeField] GameObject debugButtonContainer;

        private void Start() {
            if(SoundController.Instance != null) {
                SoundController.Instance.PlayBGM(SoundController.BGMKeys.MAIN);
            }

            informationController.Init();
            loginBonusController.Init();
            presentBoxController.Init();

            PlayerInfo.Instance.OnUpPlayerLevel.Subscribe(contents => {
                foreach(var content in contents) {
                    CheckUnLock((GameContentID)content.ID, (ushort)PlayerInfo.Instance.PlayerLevel, PlayerInfo.Instance.GetTutorialStep());
                }
            }).AddTo(this);

            PlayerInfo.Instance.OnChangePlayerStatus.Subscribe(_ => {
                playerStatusController.SetPlayerStatus(PlayerInfo.Instance.GetPlayerStatus());
            }).AddTo(this);

            BackButtonController.Instance.OnBack.Subscribe(_ => {
                quitApplicationPanel.OpenQuitApplication();
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            }).AddTo(this);

            StartCoroutine(OnLoading());

            if(Debug.isDebugBuild) {
                debugButtonContainer.SetActive(true);
            }
        }

        private IEnumerator OnLoading() {
            while(!BattleCharacterSelect.Instance.isLoad || !PlayerInfo.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            BattleCharacterSelect.Instance.ClosePanel();
            UILoading.Instance.ClearQuery();
            UILoading.Instance.HideLoading();

            SetStageButton(PlayerInfo.Instance.GetPlayerStatus().Level);
            SetGuild();
            if(PlayerInfo.Instance.GetTutorialStep() >= TutorialStep.ClearStoryStage_1_2_3) {
                btnGacha.SetEnable(true, false);
            }
        }

        private void SetStageButton(ushort playerLevel) {
            var tutorialStep = PlayerInfo.Instance.GetTutorialStep();

            btnStory.SetEnable(true, false);
            if(tutorialStep >= Noroshi.Core.Game.Player.TutorialStep.ClearStoryStage_1_3_2) {
                btnShop.SetEnable(true);
                btnMedalShop.SetEnable(true);
            }
            if(GameContent.IsOpen(GameContentID.Arena, playerLevel, tutorialStep)) {
                btnArena.SetEnable(true, false);
            };
            if(GameContent.IsOpen(GameContentID.Training, playerLevel, tutorialStep)) {
                btnTraining.SetEnable(true, false);
            };
            if(GameContent.IsOpen(GameContentID.Expedition, playerLevel, tutorialStep)) {
                btnExpedition.SetEnable(true, false);
            };
            if(GameContent.IsOpen(GameContentID.Trial, playerLevel, tutorialStep)) {
                btnTrial.SetEnable(true, false);
            };
            if(GameContent.IsOpen(GameContentID.BeginnerGuild, playerLevel, tutorialStep)) {
                btnGuild.SetEnable(true, false);
            };
            if(GameContent.IsOpen(GameContentID.NormalGuild, playerLevel, tutorialStep)) {
                btnGuild.SetEnable(true, false);
            };
        }

        private void SetGuild() {
            if(PlayerInfo.Instance.GetPlayerStatus().GuildID == null) {return;}
            guildChatController.gameObject.SetActive(true);
            Noroshi.Guild.WebApiRequester.GetOwn().Do(data => {
                guildChatController.Init(data.Guild);
                playerStatusController.SetGuildInfo(data.Guild);
            }).Subscribe();
            btnRaidBoss.SetEnable(true, false);
        }

        private void CheckUnLock(GameContentID categoryName, ushort playerLevel, TutorialStep tutorialStep) {
            var isOpen = GameContent.IsOpen(categoryName, playerLevel, tutorialStep);
            switch(categoryName) {
                case GameContentID.Arena: btnArena.SetEnable(isOpen, false); break;
                case GameContentID.Training: btnTraining.SetEnable(isOpen, false); break;
                case GameContentID.Trial: btnTrial.SetEnable(isOpen, false); break;
                case GameContentID.Expedition: btnExpedition.SetEnable(isOpen, false); break;
                case GameContentID.BeginnerGuild: btnGuild.SetEnable(isOpen, false); break;
                case GameContentID.NormalGuild: btnGuild.SetEnable(isOpen, false); break;
                default: break;
            }
        }
    }
}
