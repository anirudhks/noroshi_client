﻿using UnityEngine;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class QuitApplicationPanel : MonoBehaviour {
        [SerializeField] BtnCommon btnCloseApplication;
        [SerializeField] BtnCommon btnCancelCloseApplication;

        private bool isOpen = false;

        private void Start() {
            btnCloseApplication.OnClickedBtn.Subscribe(_ => {
                Application.Quit();
            });

            btnCancelCloseApplication.OnClickedBtn.Subscribe(_ => {
                CloseQuitApplication();
            });
            btnCancelCloseApplication.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseQuitApplication();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void CloseQuitApplication() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }

        public void OpenQuitApplication() {
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }
    }
}
