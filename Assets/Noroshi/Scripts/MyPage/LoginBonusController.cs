﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.Core.Game.Possession;
using Noroshi.Core.WebApi.Response.Possession;
using Noroshi.Core.WebApi.Response.LoginBonus;

namespace Noroshi.UI {
    public class LoginBonusController : MonoBehaviour {
        [SerializeField] BtnCommon btnLoginBonus;
        [SerializeField] LoginBonusGetPanel loginBonusGetPanel;

        [SerializeField] Text txtHeadingMonthlyLoginBonus;
        [SerializeField] GameObject monthlyLoginBonusContainer;
        [SerializeField] ScrollRect monthlyLoginBonusScroller;
        [SerializeField] GameObject[] monthlyLoginBonusItemList;
        [SerializeField] ItemIcon[] monthlyLoginBonusIconList;
        [SerializeField] GameObject[] iconGetMonthlyLoginBonusList;
        [SerializeField] Text[] txtMonthlyLoginBonusNumList;
        [SerializeField] BtnCommon btnMonthlyLoginBonusPanel;
        [SerializeField] GameObject monthlyCurrentEffect;
        [SerializeField] GameObject separatorVip7Bonus;
        [SerializeField] GameObject separatorVip10Bonus;
        [SerializeField] GameObject separatorVip13Bonus;
        [SerializeField] GameObject separatorVip15Bonus;

        [SerializeField] Text txtHeadingStartUpLoginBonus;
        [SerializeField] GameObject startUpLoginBonusContainer;
        [SerializeField] GameObject[] startUpLoginBonusItemList;
        [SerializeField] ItemIcon[] startUpLoginBonusIconList;
        [SerializeField] GameObject[] iconGetStartUpLoginBonusList;
        [SerializeField] Text[] txtStartUpLoginBonusNumList;
        [SerializeField] BtnCommon btnStartUpLoginBonusPanel;
        [SerializeField] GameObject startUpCurrentEffect;

        private bool canReceiveMonthlyLoginBonus = false;
        private bool canReceiveStartUpLoginBonus = false;
        private string monthlyLoginBonusTitle;
        private LoginBonusReward monthlyReward;
        private uint monthlyLoginBonusID;
        private byte monthlyLoginBonusThreshold;
        private string startUpLoginBonusTitle;
        private LoginBonusReward startUpReward;
        private uint startUpLoginBonusID;
        private byte startUpLoginBonusThreshold;
        private bool isOpen = false;

        private void Start() {
            btnMonthlyLoginBonusPanel.OnClickedBtn.Subscribe(_ => {
                if(canReceiveMonthlyLoginBonus) {
                    SendReceiveReward(monthlyLoginBonusID, monthlyLoginBonusThreshold);
                    iconGetMonthlyLoginBonusList[monthlyLoginBonusThreshold - 1].SetActive(true);
                    loginBonusGetPanel.OpenGetPanel(monthlyReward, monthlyLoginBonusTitle, true);
                    canReceiveMonthlyLoginBonus = false;
                } else {
                    CloseLoginBonus();
                }
                monthlyCurrentEffect.SetActive(false);
                monthlyLoginBonusContainer.SetActive(false);
            });
            btnMonthlyLoginBonusPanel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnStartUpLoginBonusPanel.OnClickedBtn.Subscribe(_ => {
                if(canReceiveStartUpLoginBonus) {
                    SendReceiveReward(startUpLoginBonusID, startUpLoginBonusThreshold);
                    iconGetStartUpLoginBonusList[startUpLoginBonusThreshold - 1].SetActive(true);
                    loginBonusGetPanel.OpenGetPanel(startUpReward, startUpLoginBonusTitle);
                    canReceiveStartUpLoginBonus = false;
                } else {
                    CloseLoginBonus();
                }
                startUpCurrentEffect.SetActive(false);
                startUpLoginBonusContainer.SetActive(false);
            });
            btnStartUpLoginBonusPanel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            loginBonusGetPanel.OnCloseGetPanel.Subscribe(_ => {
                if(canReceiveStartUpLoginBonus) {
                    startUpLoginBonusContainer.SetActive(true);
                } else {
                    CloseLoginBonus();
                }
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseLoginBonus();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void SendReceiveReward(uint id, byte threshold) {
            Noroshi.LoginBonus.WebApiRequester.ReceiveReward(id, threshold).Subscribe();
        }

        private void SetMonthlyLoginBonus(Noroshi.Core.WebApi.Response.LoginBonus.LoginBonus data) {
            monthlyLoginBonusID = data.ID;
            monthlyLoginBonusTitle = GlobalContainer.LocalizationManager.GetText(data.TextKey);
            txtHeadingMonthlyLoginBonus.text = monthlyLoginBonusTitle;
            if(data.Rewards.Length < 29) {separatorVip15Bonus.SetActive(false);}
            if(data.Rewards.Length < 22) {separatorVip13Bonus.SetActive(false);}
            if(data.Rewards.Length < 15) {separatorVip10Bonus.SetActive(false);}
            if(data.Rewards.Length < 8) {separatorVip7Bonus.SetActive(false);}
            for(int i = 0, l = monthlyLoginBonusItemList.Length; i < l; i++) {
                if(i < data.Rewards.Length) {
                    var reward = data.Rewards[i].PossessionObjects[0];
                    if(data.Rewards[i].HasAlreadyReceivedReward) {
                        iconGetMonthlyLoginBonusList[i].SetActive(true);
                    } else if(data.Rewards[i].CanReceiveReward) {
                        var vNum = (int)(i / 7);
                        var posY = vNum == 0 ? 1 : 1 - ((float)vNum / (float)3);
                        monthlyLoginBonusScroller.verticalNormalizedPosition = posY;
                        monthlyReward = data.Rewards[i];
                        monthlyLoginBonusThreshold = data.Rewards[i].Threshold;
                        canReceiveMonthlyLoginBonus = true;
                        monthlyCurrentEffect.transform.localPosition = monthlyLoginBonusItemList[i].transform.localPosition;
                        monthlyCurrentEffect.SetActive(true);
                    }
                    if(reward.Num > 1) {
                        txtMonthlyLoginBonusNumList[i].text = "x" + reward.Num;
                        txtMonthlyLoginBonusNumList[i].gameObject.SetActive(true);
                    }
                    monthlyLoginBonusIconList[i].SetItemIcon(reward);
                } else {
                    monthlyLoginBonusItemList[i].gameObject.SetActive(false);
                }
            }
        }

        private void SetStartUpLoginBonus(Noroshi.Core.WebApi.Response.LoginBonus.LoginBonus data) {
            startUpLoginBonusID = data.ID;
            startUpLoginBonusTitle = GlobalContainer.LocalizationManager.GetText(data.TextKey);
            txtHeadingStartUpLoginBonus.text = startUpLoginBonusTitle;
            for(int i = 0, l = startUpLoginBonusItemList.Length; i < l; i++) {
                if(i < data.Rewards.Length) {
                    var reward = data.Rewards[i].PossessionObjects[0];
                    if(data.Rewards[i].HasAlreadyReceivedReward) {
                        iconGetStartUpLoginBonusList[i].SetActive(true);
                    } else if(data.Rewards[i].CanReceiveReward) {
                        startUpReward = data.Rewards[i];
                        startUpLoginBonusThreshold = data.Rewards[i].Threshold;
                        canReceiveStartUpLoginBonus = true;
                        startUpCurrentEffect.transform.localPosition = startUpLoginBonusItemList[i].transform.localPosition;
                        startUpCurrentEffect.SetActive(true);
                    }
                    if(reward.Num > 1) {
                        txtStartUpLoginBonusNumList[i].text = "x" + reward.Num;
                        txtStartUpLoginBonusNumList[i].gameObject.SetActive(true);
                    }
                    startUpLoginBonusIconList[i].SetItemIcon(reward);
                } else {
                    startUpLoginBonusItemList[i].gameObject.SetActive(false);
                }
            }
        }

        public void Init() {
            Noroshi.LoginBonus.WebApiRequester.List().Do(res => {
                foreach(var loginBonus in res.LoginBonuses) {
                    if(loginBonus.Category == Noroshi.Core.Game.LoginBonus.LoginBonusCategory.Monthly) {
                        SetMonthlyLoginBonus(loginBonus);
                        btnLoginBonus.SetEnable(true);
                        TweenC.Add(btnLoginBonus.gameObject, 0.01f, Color.white);
                        btnLoginBonus.OnClickedBtn.Subscribe(_ => {
                            isOpen = true;
                            OpenMonthlyLoginBonus();
                        });
                        btnLoginBonus.OnPlaySE.Subscribe(_ => {
                            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                        });
                    } else if(loginBonus.Category == Noroshi.Core.Game.LoginBonus.LoginBonusCategory.StartUp) {
                        SetStartUpLoginBonus(loginBonus);
                    }
                }
                if(canReceiveMonthlyLoginBonus) {
                    OpenMonthlyLoginBonus();
                } else if(canReceiveStartUpLoginBonus) {
                    OpenStartUpLoginBonus();
                }
            }).Subscribe();
        }

        public void OpenMonthlyLoginBonus() {
            gameObject.SetActive(true);
            monthlyLoginBonusContainer.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }

        public void OpenStartUpLoginBonus() {
            gameObject.SetActive(true);
            startUpLoginBonusContainer.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
        }

        public void CloseLoginBonus() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }
    }
}
