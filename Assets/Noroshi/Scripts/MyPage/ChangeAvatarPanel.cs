﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;

namespace Noroshi.UI {
    public class ChangeAvatarPanel : MonoBehaviour {
        [SerializeField] GameObject avatarListWrapper;
        [SerializeField] BtnCharacterAvatar btnCharacterAvatarPref;
        [SerializeField] BtnCommon btnDecideAvatar;
        [SerializeField] BtnCommon btnCloseAvatarList;

        public Subject<Dictionary<string, uint>> OnChangeAvatar = new Subject<Dictionary<string, uint>>();

        private List<BtnCharacterAvatar> btnAvatarList = new List<BtnCharacterAvatar>();
        private uint playerCharacterID;
        private uint avatarID;
        private bool isOpen = false;

        private void Start() {
            GlobalContainer.RepositoryManager.PlayerCharacterRepository.GetAll().Do(list => {
                foreach(var chara in list) {
                    var btnAvatar = Instantiate(btnCharacterAvatarPref);
                    btnAvatar.SetAvatarImg(chara.CharacterID, chara.ID);
                    btnAvatar.transform.SetParent(avatarListWrapper.transform);
                    btnAvatar.transform.localScale = Vector3.one;
                    btnAvatarList.Add(btnAvatar);
                    if(chara.CharacterID == avatarID) {
                        btnAvatar.SetAvatarSelect(true);
                    }
                    btnAvatar.OnClickedBtn.Subscribe(id => {
                        playerCharacterID = (uint)id;
                        avatarID = btnAvatar.characterID;
                        foreach(var btn in btnAvatarList) {
                            btn.SetAvatarSelect(btn.id == id);
                        }
                    });
                    btnAvatar.OnPlaySE.Subscribe(_ => {
                        SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                    });
                }
            }).Subscribe();

            btnDecideAvatar.OnClickedBtn.Subscribe(_ => {
                SetPlayerAvatar();
            });
            btnDecideAvatar.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnCloseAvatarList.OnClickedBtn.Subscribe(_ => {
                CloseChangeAvatarPanel();
            });
            btnCloseAvatarList.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModalChild.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseChangeAvatarPanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void SetPlayerAvatar() {
            OnChangeAvatar.OnNext(new Dictionary<string, uint> {
                {"PlayerCharacterID", playerCharacterID},
                {"CharacterID", avatarID}
            });
            CloseChangeAvatarPanel();
        }

        public void OpenChangeAvatarPanel(uint id) {
            avatarID = id;
            foreach(var btn in btnAvatarList) {
                btn.SetAvatarSelect(btn.characterID == id);
            }
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalChildOpen(true);
        }

        public void CloseChangeAvatarPanel() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalChildOpen(false);
            });
        }
    }
}
