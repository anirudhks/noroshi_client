﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class PushNotificationSettingPanel : MonoBehaviour {
        [SerializeField] Toggle toggleStaminaMax;
        [SerializeField] Toggle toggleBPMax;
        [SerializeField] Toggle toggleSkillPointMax;
        [SerializeField] Toggle toggleAppearRaidBoss;
        [SerializeField] Toggle toggleAppearBigRaidBoss;
        [SerializeField] Toggle toggleTime0007;
        [SerializeField] Toggle toggleTime0710;
        [SerializeField] Toggle toggleTime1012;
        [SerializeField] Toggle toggleTime1214;
        [SerializeField] Toggle toggleTime1418;
        [SerializeField] Toggle toggleTime1822;
        [SerializeField] Toggle toggleTime2200;
        [SerializeField] BtnCommon btnClosePushNotificationSetting;

        private void Start() {
//            toggleStaminaMax.isOn = false;
            btnClosePushNotificationSetting.OnClickedBtn.Subscribe(_ => {
//                Debug.Log("stamina max: " + toggleStaminaMax.isOn);
                ClosePushNotificationSettingPanel();
            });
            btnClosePushNotificationSetting.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });
        }

        public void OpenPushNotificationSettingPanel() {
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
        }

        public void ClosePushNotificationSettingPanel() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                gameObject.SetActive(false);
            });
        }
    }
}
