﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class BtnCharacterAvatar : BtnCommon {
        [SerializeField] Image imgAvatar;
        [SerializeField] GameObject iconSelect;

        public Subject<uint> OnSelectAvatar = new Subject<uint>();
        public uint characterID;

        public void SetAvatarImg(uint charaID, uint playerCharacterID) {
            imgAvatar.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite> (charaID, "thumb_1");
            id = (int)playerCharacterID;
            characterID = charaID;
        }

        public void SetAvatarSelect(bool isSelect) {
            iconSelect.SetActive(isSelect);
        }
    }
}
