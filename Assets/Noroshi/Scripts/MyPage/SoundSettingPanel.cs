﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class SoundSettingPanel : MonoBehaviour {
        [SerializeField] Toggle toggleBGM;
        [SerializeField] Toggle toggleSE;
        [SerializeField] Toggle toggleVoice;
        [SerializeField] BtnCommon btnCloseSoundSetting;

        private bool isOpen = false;

        private void Start() {
            toggleBGM.isOn = PlayerPrefs.GetInt(SaveKeys.IsBGMOn) == 0;
            toggleSE.isOn = PlayerPrefs.GetInt(SaveKeys.IsSEOn) == 0;
            toggleVoice.isOn = PlayerPrefs.GetInt(SaveKeys.IsVoiceOn) == 0;

            btnCloseSoundSetting.OnClickedBtn.Subscribe(_ => {
                CloseSoundSettingPanel();
            });
            btnCloseSoundSetting.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModalChild.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseSoundSettingPanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        public void OpenSoundSettingPanel() {
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalChildOpen(true);
        }

        public void CloseSoundSettingPanel() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalChildOpen(false);
            });
        }

        public void OnToggleBGM(bool isOn) {
            var value = isOn ? 0 : 1;
            SoundController.Instance.ChangeSetting(Noroshi.Core.Game.Sound.SoundType.Bgm, isOn);
            if(isOn) {
                SoundController.Instance.PlayBGM(SoundController.BGMKeys.MAIN);
            } else {
                SoundController.Instance.StopBGM();
            }
            PlayerPrefs.SetInt(SaveKeys.IsBGMOn, value);
            PlayerPrefs.Save();
        }

        public void OnToggleSE(bool isOn) {
            var value = isOn ? 0 : 1;
            SoundController.Instance.ChangeSetting(Noroshi.Core.Game.Sound.SoundType.SE, isOn);
            PlayerPrefs.SetInt(SaveKeys.IsSEOn, value);
            PlayerPrefs.Save();
        }

        public void OnToggleVoice(bool isOn) {
            var value = isOn ? 0 : 1;
            SoundController.Instance.ChangeSetting(Noroshi.Core.Game.Sound.SoundType.Voice, isOn);
            PlayerPrefs.SetInt(SaveKeys.IsVoiceOn, value);
            PlayerPrefs.Save();
        }
    }
}
