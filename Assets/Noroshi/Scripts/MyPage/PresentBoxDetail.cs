﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.Game.Possession;

namespace Noroshi.UI {
    public class PresentBoxDetail : MonoBehaviour {
        [SerializeField] Text txtTitle;
        [SerializeField] Text txtDescription;
        [SerializeField] GameObject[] presentBoxItemList;
        [SerializeField] ItemIcon[] itemIconList;
        [SerializeField] Text[] txtNumList;
        [SerializeField] BtnCommon btnReceive;

        public Subject<uint> OnReceive = new Subject<uint>();
        public uint presentBoxID;

        private Noroshi.Core.WebApi.Response.Possession.PossessionObject[] getItemList;

        private void Start() {
            btnReceive.OnClickedBtn.Subscribe(_ => {
                OnReceive.OnNext(presentBoxID);
                btnReceive.gameObject.SetActive(false);
            });
            btnReceive.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        public void ReceiveEachPresent() {
            for(int i = 0, l = getItemList.Length; i < l; i++) {
                if(getItemList[i].Category == (byte)PossessionCategory.Status) {
                    if(getItemList[i].ID == (byte)PossessionStatusID.Gold) {
                        PlayerInfo.Instance.ChangeHaveGold((int)getItemList[i].Num);
                    } else if(getItemList[i].ID == (byte)PossessionStatusID.CommonGem
                        || getItemList[i].ID == (byte)PossessionStatusID.FreeGem) {
                        PlayerInfo.Instance.ChangeHaveGem((int)getItemList[i].Num);
                    }
                } else if(getItemList[i].Category == (byte)PossessionCategory.Status) {
                    var pcID = BattleCharacterSelect.Instance.GetPlayerCharacterId(new uint[]{getItemList[i].ID});
                    var getModal = Instantiate(Resources.Load<GetCharacterModal>("UI/GetCharacterModal"));
                    getModal.OpenModal(getItemList[i].ID);
                    if(pcID.Length < 1) {
                        ItemListManager.Instance.UpdateNeedSoul(getItemList[i].ID);
                    }
                } else {
                    var isSoul = getItemList[i].Category == (byte)PossessionCategory.Soul;
                    ItemListManager.Instance.ChangeItemCount(getItemList[i].ID, (int)getItemList[i].Num, isSoul);
                }
            }
        }

        public void SetDetail(Noroshi.Core.WebApi.Response.PresentBox.PresentBox presentData) {
            var gclm = GlobalContainer.LocalizationManager;
            getItemList = presentData.PossessionObjects;
            presentBoxID = presentData.ID;
            txtTitle.text = gclm.GetText(presentData.TitleTextID);
            txtDescription.text = gclm.GetText(presentData.BodyTextID);
            for(int i = 0, l = presentBoxItemList.Length; i < l; i++) {
                if(i < presentData.PossessionObjects.Length) {
                    itemIconList[i].SetItemIcon(presentData.PossessionObjects[i]);
                    txtNumList[i].text = presentData.PossessionObjects[i].Num.ToString();
                    presentBoxItemList[i].SetActive(true);
                } else {
                    presentBoxItemList[i].SetActive(false);
                }
            }
        }
    }
}
