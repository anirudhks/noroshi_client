using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.Core.Game.Possession;
using Noroshi.Core.WebApi.Response.LoginBonus;

namespace Noroshi.UI {
    public class LoginBonusGetPanel : MonoBehaviour {
        [SerializeField] BtnCommon btnOverlay;
        [SerializeField] Text txtLoginBonusTitle;
        [SerializeField] ItemIcon[] itemIconList;
        [SerializeField] Text[] txtRewardList;
        [SerializeField] GameObject iconVipBonus;

        public Subject<bool> OnCloseGetPanel = new Subject<bool>();

        private void Start() {
            btnOverlay.OnClickedBtn.Subscribe(_ => {
                iconVipBonus.SetActive(false);
                gameObject.SetActive(false);
                OnCloseGetPanel.OnNext(true);
            });
            btnOverlay.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        public void OpenGetPanel(LoginBonusReward loginBonus, string title, bool isMonthly = false) {
            var rewards = loginBonus.PossessionObjects;
            txtLoginBonusTitle.text = title;
            for(int i = 0, l = txtRewardList.Length; i < l; i++) {
                if(i < rewards.Length) {
                    itemIconList[i].SetItemIcon(rewards[i]);
                    if(rewards[i].Category == (byte)PossessionCategory.Status) {
                        if(rewards[i].ID == (byte)PossessionStatusID.Gold) {
                            PlayerInfo.Instance.ChangeHaveGold((int)rewards[i].Num);
                        } else if(rewards[i].ID == (byte)PossessionStatusID.CommonGem
                            || rewards[i].ID == (byte)PossessionStatusID.FreeGem) {
                            PlayerInfo.Instance.ChangeHaveGem((int)rewards[i].Num);
                        }
                    } else if(rewards[i].Category == (byte)PossessionCategory.Character) {
                        var pcID = BattleCharacterSelect.Instance.GetPlayerCharacterId(new uint[]{rewards[i].ID});
                        var getModal = Instantiate(Resources.Load<GetCharacterModal>("UI/GetCharacterModal"));
                        getModal.OpenModal(rewards[i].ID);
                        if(pcID.Length < 1) {
                            ItemListManager.Instance.UpdateNeedSoul(rewards[i].ID);
                        }
                    } else if(rewards[i].Category != (byte)PossessionCategory.Status) {
                        var isSoul = rewards[i].Category == (byte)PossessionCategory.Soul;
                        ItemListManager.Instance.ChangeItemCount(rewards[i].ID, (int)rewards[i].Num, isSoul);
                    }
                    txtRewardList[i].text = "x " + rewards[i].Num;
                    txtRewardList[i].gameObject.SetActive(true);
                } else {
                    txtRewardList[i].gameObject.SetActive(false);
                }
            }
            if(isMonthly && loginBonus.HasVipReward) {
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    iconVipBonus.SetActive(true);
                    TweenS.Add(iconVipBonus, 0.6f, 1).From(Vector2.one * 2.0f).EaseOutBounce();
                });
            }
            SoundController.Instance.PlaySE(SoundController.SEKeys.GET);
            gameObject.SetActive(true);
        }
    }
}
