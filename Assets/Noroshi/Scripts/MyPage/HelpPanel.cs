﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniLinq;
using Noroshi.Core.WebApi.Response.Master;

namespace Noroshi.UI {
    public class HelpPanel : MonoBehaviour {
        [SerializeField] Text txtDetailTitle;
        [SerializeField] Text txtBody;
        [SerializeField] ScrollRect helpPanelScroller;
        [SerializeField] GameObject helpAlertWrapper;
        [SerializeField] GameObject helpPanelWrapper;
        [SerializeField] HelpPiece helpPiecePref;
        [SerializeField] BtnCommon btnClose;

        private HelpCategory[] helpContentList;
        private List<HelpPiece> helpPieceList = new List<HelpPiece>();
        private bool isOpen = false;

        private void Start() {
            SetHelpContent();

            btnClose.OnClickedBtn.Subscribe(_ => {
                CloseHelpPanel();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModalChild.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseHelpPanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void SetHelpContent() {
            helpContentList = GlobalContainer.MasterManager.HelpMaster.GetHelpCategories();
            helpContentList = helpContentList.OrderBy(h => h.OrderPriority).ToArray();
            foreach(var help in helpContentList) {
                var helpPiece = Instantiate(helpPiecePref);
                helpPiece.transform.SetParent(helpPanelWrapper.transform);
                helpPiece.transform.localScale = Vector3.one;
                helpPiece.SetContent(help);
                helpPiece.OnSelectHelp.Subscribe(ShowDetail);
                helpPieceList.Add(helpPiece);
            }
        }

        private void ShowDetail(HelpContent content) {
            helpAlertWrapper.SetActive(false);
            txtDetailTitle.text = GlobalContainer.LocalizationManager.GetText(content.TitleTextKey);
            txtBody.text = GlobalContainer.LocalizationManager.GetText(content.BodyTextKey);
        }

        public void OpenHelpPanel() {
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalChildOpen(true);
        }

        public void CloseHelpPanel() {
            foreach(var piece in helpPieceList) {
                piece.ClosePanel();
            }
            helpPanelScroller.verticalNormalizedPosition = 1;
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                helpAlertWrapper.SetActive(true);
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalChildOpen(false);
            });
        }
    }
}
