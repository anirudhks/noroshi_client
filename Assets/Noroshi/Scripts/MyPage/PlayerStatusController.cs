﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.WebApi.Response;

namespace Noroshi.UI {
    public class PlayerStatusController : MonoBehaviour {
        [SerializeField] Image imgPlayerAvatar;
        [SerializeField] BtnCommon btnOpenPlayerStatus;
        [SerializeField] BtnCommon btnPlayerStatusOverlay;
        [SerializeField] BtnCommon btnClose;
        [SerializeField] BtnCommon btnChangeAvatar;
        [SerializeField] BtnCommon btnChangeName;
        [SerializeField] BtnCommon btnHelp;
        [SerializeField] BtnCommon btnPushNotification;
        [SerializeField] BtnCommon btnSoundSetting;
        [SerializeField] GameObject statusContainer;
        [SerializeField] ChangeAvatarPanel changeAvatarPanel;
        [SerializeField] ChangeNamePanel changeNamePanel;
        [SerializeField] HelpPanel helpPanel;
        [SerializeField] PushNotificationSettingPanel pushNotificationSettingPanel;
        [SerializeField] SoundSettingPanel soundSettingPanel;
        [SerializeField] GameObject guildNameWrapper;
        [SerializeField] GameObject guildIDWrapper;
        [SerializeField] Text txtFacePlayerLevel;
        [SerializeField] Text txtFacePlayerName;
        [SerializeField] Text txtPlayerName;
        [SerializeField] Text txtPlayerLevel;
        [SerializeField] Text txtCurrentPlayerExp;
        [SerializeField] Text txtNeedPlayerExp;
        [SerializeField] Text txtAccountId;
        [SerializeField] Text txtVipLevel;
        [SerializeField] Text txtGuildName;
        [SerializeField] Text txtGuildID;
        [SerializeField] GameObject processing;

        private Noroshi.Player.WebApiRequester webAPIRequester;
        private uint avatarID;
        private bool isOpen;

        private void Start() {
            webAPIRequester = new Noroshi.Player.WebApiRequester();

            changeAvatarPanel.OnChangeAvatar.Subscribe(data => {
                processing.SetActive(true);
                webAPIRequester.ChangeAvatar(data["PlayerCharacterID"]).Do(_ => {
                    avatarID = data["CharacterID"];
                    imgPlayerAvatar.sprite =GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite>(avatarID, "thumb_1");
                    btnOpenPlayerStatus.GetComponent<Image>().sprite = imgPlayerAvatar.sprite;
                    processing.SetActive(false);
                }).Subscribe();
            });

            changeNamePanel.OnDecideName.Subscribe(name => {
                processing.SetActive(true);
                webAPIRequester.ChangeName(name).Do(_ => {
                    txtFacePlayerName.text = name;
                    txtPlayerName.text = name;
                    processing.SetActive(false);
                }).Subscribe();
            });

            btnOpenPlayerStatus.OnClickedBtn.Subscribe(_ => {
                OpenPlayerStatus();
            });
            btnOpenPlayerStatus.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnChangeAvatar.OnClickedBtn.Subscribe(_ => {
                changeAvatarPanel.OpenChangeAvatarPanel(avatarID);
            });
            btnChangeAvatar.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnChangeName.OnClickedBtn.Subscribe(_ => {
                changeNamePanel.OpenChangeNamePanel();
                BackButtonController.Instance.IsModalChildOpen(true);
            });
            btnChangeName.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnHelp.OnClickedBtn.Subscribe(_ => {
                helpPanel.OpenHelpPanel();
            });
            btnHelp.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnPushNotification.OnClickedBtn.Subscribe(_ => {
                pushNotificationSettingPanel.OpenPushNotificationSettingPanel();
            });
            btnPushNotification.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnSoundSetting.OnClickedBtn.Subscribe(_ => {
                soundSettingPanel.OpenSoundSettingPanel();
            });
            btnSoundSetting.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnClose.OnClickedBtn.Subscribe(_ => {
                ClosePlayerStatus();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnPlayerStatusOverlay.OnClickedBtn.Subscribe(_ => {
                ClosePlayerStatus();
            });
            btnPlayerStatusOverlay.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                ClosePlayerStatus();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void OpenPlayerStatus() {
            isOpen = true;
            btnPlayerStatusOverlay.gameObject.SetActive(true);
            statusContainer.gameObject.SetActive(true);
            TweenA.Add(statusContainer, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }

        private void ClosePlayerStatus() {
            TweenA.Add(statusContainer, 0.1f, 0).Then(() => {
                isOpen = false;
                statusContainer.gameObject.SetActive(false);
                btnPlayerStatusOverlay.gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }

        public void SetPlayerStatus(PlayerStatus playerStatus) {
            var data= GlobalContainer.MasterManager.LevelMaster.GetPlayerLevelByLevel(playerStatus.Level);
            txtFacePlayerLevel.text = playerStatus.Level.ToString();
            txtFacePlayerName.text = playerStatus.Name.ToString();
            txtPlayerLevel.text = playerStatus.Level.ToString();
            txtPlayerName.text = playerStatus.Name;
            txtCurrentPlayerExp.text = (data.Exp - playerStatus.ExpInLevel).ToString();
            txtNeedPlayerExp.text = data.Exp.ToString();
            txtAccountId.text = playerStatus.PlayerID.ToString();
            txtVipLevel.text = playerStatus.VipLevel.ToString();
            if(avatarID != playerStatus.AvatarCharacterID || playerStatus.AvatarCharacterID == 0) {
                if(playerStatus.AvatarCharacterID == 0) {
                    avatarID = Noroshi.Core.Game.Character.Constant.FIRST_CHARACTER_ID;
                } else {
                    avatarID = playerStatus.AvatarCharacterID;
                }
                imgPlayerAvatar.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite> (avatarID, "thumb_1");
                btnOpenPlayerStatus.GetComponent<Image>().sprite = imgPlayerAvatar.sprite;
            }
        }

        public void SetGuildInfo(Noroshi.Core.WebApi.Response.Guild.Guild guildData) {
            if(guildData.Category == Noroshi.Core.Game.Guild.GuildCategory.Beginner) {
                txtGuildName.text = GlobalContainer.LocalizationManager.GetText("UI.Noun.BeginnerGuildName");
            } else {
                txtGuildName.text = guildData.Name;
            }
            txtGuildID.text = guildData.ID.ToString();
            guildNameWrapper.SetActive(true);
            guildIDWrapper.SetActive(true);
        }
    }
}
