﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class PresentBoxPanel : BtnCommon {
        [SerializeField] ItemIcon itemIcon;
        [SerializeField] Text txtTitle;
        [SerializeField] Text txtDescription;
        [SerializeField] Text txtNum;
        [SerializeField] Text txtDate;

        public void SetPanel(Noroshi.Core.WebApi.Response.PresentBox.PresentBox presentData, int index) {
            var gclm = GlobalContainer.LocalizationManager;
            var item = presentData.PossessionObjects[0];
            var t = Constant.UNIX_EPOCH.AddSeconds(presentData.CreatedAt).ToLocalTime();
            id = (int)presentData.ID;
            itemIcon.SetItemIcon(item);
            txtTitle.text = gclm.GetText(presentData.TitleTextID);
            txtDescription.text = gclm.GetText(presentData.BodyTextID);
            txtNum.text = "x" + item.Num.ToString();
            txtDate.text = t.Month + "/" + t.Day;
        }
    }
}
