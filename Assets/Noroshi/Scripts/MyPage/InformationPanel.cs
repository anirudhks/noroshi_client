﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.Core.Game.Information;

namespace Noroshi.UI {
    public class InformationPanel : BtnCommon {
        [SerializeField] Text txtTitle;
        [SerializeField] Text txtDate;
        [SerializeField] GameObject iconUnread;
        [SerializeField] GameObject labelImportant;
        [SerializeField] GameObject labelEvent;
        [SerializeField] GameObject labelCampaign;
        [SerializeField] GameObject labelApology;

        public Subject<InformationCategory> OnReadInformation = new Subject<InformationCategory>();

        private bool isRead;
        private InformationCategory category;

        public void SetHasRead() {
            isRead = true;
            iconUnread.SetActive(false);
            OnReadInformation.OnNext(category);
        }

        public void SetPanel(Noroshi.Core.WebApi.Response.Information.Information informationData, int index) {
            var t = Constant.UNIX_EPOCH.AddSeconds(informationData.OpenedAt).ToLocalTime();
            id = index;
            isRead = informationData.HasReadInformation;
            category = informationData.Category;
            txtTitle.text = GlobalContainer.LocalizationManager.GetText(informationData.TitleTextKey);
            txtDate.text = t.Year + "/" + t.Month + "/" + t.Day;
            if(!informationData.HasReadInformation) {iconUnread.SetActive(true);}
            switch(informationData.Category) {
                case Noroshi.Core.Game.Information.InformationCategory.Important:
                    labelImportant.SetActive(true);
                    break;
                case Noroshi.Core.Game.Information.InformationCategory.Event:
                    labelEvent.SetActive(true);
                    break;
                case Noroshi.Core.Game.Information.InformationCategory.Campaign:
                    labelCampaign.SetActive(true);
                    break;
                case Noroshi.Core.Game.Information.InformationCategory.Apology:
                    labelApology.SetActive(true);
                    break;
            }

            OnClickedBtn.Subscribe(_ => {
                if(isRead) {return;}
                SetHasRead();
            });
        }
    }
}
