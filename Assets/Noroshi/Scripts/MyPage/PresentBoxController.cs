﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;

namespace Noroshi.UI {
    public class PresentBoxController : MonoBehaviour {
        [SerializeField] ScrollRect presentBoxPanelScroller;
        [SerializeField] GameObject presentBoxPanelWrapper;
        [SerializeField] GameObject presentBoxDetailWrapper;
        [SerializeField] PresentBoxPanel presentBoxPanelPref;
        [SerializeField] PresentBoxDetail presentBoxDetailPref;
        [SerializeField] Text txtUnReceivedNum;
        [SerializeField] GameObject alertNoPresent;
        [SerializeField] BtnCommon btnPresentBox;
        [SerializeField] BtnCommon btnBulkReceive;
        [SerializeField] BtnCommon btnClose;
        [SerializeField] GameObject iconNotice;
        [SerializeField] Text txtNoticeNum;
        [SerializeField] GameObject processing;

        private List<PresentBoxPanel> panelList = new List<PresentBoxPanel>();
        private List<PresentBoxDetail> detailList = new List<PresentBoxDetail>();
        private bool isCreatePresentBox = false;
        private int openID = 0;
        private int unreceivedCount = 0;
        private Noroshi.Core.WebApi.Response.PresentBox.PresentBox[] presentBoxDataList;
        private bool isOpen = false;

        private void Start() {
            btnBulkReceive.OnClickedBtn.Subscribe(_ => {
                processing.SetActive(true);
                Noroshi.PresentBox.WebApiRequester.ReceiveCollectively().Do(res => {
                    var receiveIDs = new List<uint>();
                    foreach(var p in res.ReceivingPresentBoxes) {
                        receiveIDs.Add(p.ID);
                    }
                    RemoveReceivedPresent(receiveIDs);
                    processing.SetActive(false);
                }).Subscribe();
            });
            btnBulkReceive.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnClose.OnClickedBtn.Subscribe(_ => {
                ClosePresentBox();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                ClosePresentBox();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void CreatePresentList(Noroshi.Core.WebApi.Response.PresentBox.PresentBox[] presentBoxes) {
            for(int i = 0, l = presentBoxes.Length; i < l; i++) {
                var panel = Instantiate(presentBoxPanelPref);
                var detail = Instantiate(presentBoxDetailPref);

                panel.transform.SetParent(presentBoxPanelWrapper.transform);
                panel.transform.localScale = Vector3.one;
                panelList.Add(panel);
                panel.SetPanel(presentBoxes[i], i);
                panel.OnClickedBtn.Subscribe(id => {
                    if(id == openID) {return;}
                    SelectPanel(id);
                });
                panel.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });

                detail.transform.SetParent(presentBoxDetailWrapper.transform);
                detail.transform.localScale = Vector3.one;
                detail.transform.localPosition = new Vector2(-210, 0);
                detail.gameObject.SetActive(false);
                detailList.Add(detail);
                detail.SetDetail(presentBoxes[i]);
                detail.OnReceive.Subscribe(ReceivePresent);
            }
            txtUnReceivedNum.text = unreceivedCount.ToString();
            if(panelList.Count > 0) {
                SelectPanel(panelList[0].id);
            } else {
                alertNoPresent.SetActive(true);
                btnBulkReceive.SetEnable(false);
            }
        }

        private void ReceivePresent(uint presentID) {
            var presentIDs = new List<uint>() {presentID};
            processing.SetActive(true);
            Noroshi.PresentBox.WebApiRequester.Receive(presentIDs).Do(res => {
                RemoveReceivedPresent(new List<uint>() {res.ReceivingPresentBoxes[0].ID});
                processing.SetActive(false);
            }).Subscribe();
        }

        private void RemoveReceivedPresent(List<uint> presentIDs) {
            unreceivedCount -= presentIDs.Count;
            if(unreceivedCount <= 0) {
                unreceivedCount = 0;
                btnBulkReceive.SetEnable(false);
                iconNotice.SetActive(false);
                alertNoPresent.SetActive(true);
            }
            txtUnReceivedNum.text = unreceivedCount.ToString();
            txtNoticeNum.text = unreceivedCount.ToString();
            foreach(var id in presentIDs) {
                for(int i = detailList.Count - 1; i > -1; i--) {
                    if(id == detailList[i].presentBoxID) {
                        detailList[i].ReceiveEachPresent();
                        Destroy(detailList[i].gameObject);
                        Destroy(panelList[i].gameObject);
                        detailList.RemoveAt(i);
                        panelList.RemoveAt(i);
                        break;
                    }
                }
            }
            if(panelList.Count > 0) {
                openID = panelList[0].id;
                SelectPanel(panelList[0].id);
            }
        }

        private void SelectPanel(int id) {
            PresentBoxDetail currentDetail = null;
            PresentBoxDetail newDetail = null;
            foreach(var detail in detailList) {
                if(detail.presentBoxID == (uint)openID) {
                    currentDetail = detail;
                }
                if(detail.presentBoxID == (uint)id) {
                    newDetail = detail;
                }
            }

            openID = id;
            for(int i = 0, l = panelList.Count; i < l; i++) {
                panelList[i].SetSelect(panelList[i].id == id);
            }
            if(currentDetail != null && currentDetail.gameObject.activeSelf) {
                TweenX.Add(currentDetail.gameObject, 0.15f, -210).EaseOutCubic().Then(() => {
                    currentDetail.gameObject.SetActive(false);
                    newDetail.gameObject.SetActive(true);
                    TweenX.Add(newDetail.gameObject, 0.2f, 250).EaseOutCubic();
                });
            } else {
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    newDetail.gameObject.SetActive(true);
                    TweenX.Add(newDetail.gameObject, 0.2f, 250).EaseOutCubic();
                });
            }
        }

        public void Init() {
            btnPresentBox.OnClickedBtn.Subscribe(_ => {
                OpenPresentBox();
            });
            btnPresentBox.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            Noroshi.PresentBox.WebApiRequester.List().Do(res => {
                btnPresentBox.SetEnable(true);
                TweenC.Add(btnPresentBox.gameObject, 0.01f, Color.white);
                presentBoxDataList = res.PresentBoxes;
                if(res.PresentBoxes.Length > 0) {
                    
                    unreceivedCount = presentBoxDataList.Length;
                    txtNoticeNum.text = unreceivedCount.ToString();
                    iconNotice.SetActive(true);
                }
            }).Subscribe();
        }

        private void ClosePresentBox() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }

        public void OpenPresentBox() {
            if(!isCreatePresentBox) {
                CreatePresentList(presentBoxDataList);
                isCreatePresentBox = true;
            }
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            presentBoxPanelScroller.verticalNormalizedPosition = 1;
            BackButtonController.Instance.IsModalOpen(true);
        }
    }
}
