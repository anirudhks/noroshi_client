﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Noroshi.UI {
    public class InformationDetail : MonoBehaviour {
        [SerializeField] Text txtTitle;
        [SerializeField] Image imgBanner;
        [SerializeField] Text txtBody;

        public void SetDetail(Noroshi.Core.WebApi.Response.Information.Information informationData) {
            txtTitle.text = GlobalContainer.LocalizationManager.GetText(informationData.TitleTextKey);
            txtBody.text = GlobalContainer.LocalizationManager.GetText(informationData.BodyTextKey);
        }
    }
}
