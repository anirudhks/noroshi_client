using UnityEngine;
using System.Collections;
using UniRx;
using Noroshi.Core.Game.GameContent;

namespace Noroshi.UI {
    public class ShopController : MonoBehaviour {
        [SerializeField] ShopTab[] shopBtnList;
        [SerializeField] ShopPanel[] shopPanelList;
        [SerializeField] ShopDetail shopDetail;
        [SerializeField] GameObject background;
        [SerializeField] ShopConfirm shopConfirm;
        [SerializeField] ShopUpdate shopUpdate;
        [SerializeField] GameObject processing;

        private bool isLoad = false;
        private float startDragPosition;
        private int currentShopType = 0;
        private int openIndex;

        private void Start() {
            if(SoundController.Instance != null) {
                SoundController.Instance.PlayBGM(SoundController.BGMKeys.COCOPIPI);
            }

            shopConfirm.OnBuyItem.Subscribe(data => {
                processing.SetActive(true);
                Noroshi.Shop.WebAPIRequester.Buy(data.Merchandise.DisplayID).Do(res => {
                    var isSoul = data.Merchandise.MerchandisePossessionObject.Category == (byte)Noroshi.Core.Game.Possession.PossessionCategory.Soul;
                    ItemListManager.Instance.ChangeItemCount(
                        data.Merchandise.MerchandisePossessionObject.ID,
                        (int)data.Merchandise.MerchandisePossessionObject.Num,
                        isSoul
                    );
                    shopBtnList[openIndex].SetShopBtn(res.Shop);
                    shopPanelList[openIndex].UpdateShopPanel(res.Shop);
                    PlayerInfo.Instance.UpdatePlayerStatus();
                    shopConfirm.CloseConfirm();
                    processing.SetActive(false);
                }).Subscribe();
            });

            shopUpdate.OnUpdateShop.Subscribe(shopID => {
                Noroshi.Shop.WebAPIRequester.UpdateMerchandises(shopID).Do(shopData => {
                    shopBtnList[openIndex].SetShopBtn(shopData.Shop);
                    shopPanelList[openIndex].UpdateShopPanel(shopData.Shop);
                    shopUpdate.CloseUpdate();
                }).Subscribe();
            });

            BackButtonController.Instance.OnBack.Subscribe(_ => {
                UILoading.Instance.LoadScene(Constant.SCENE_MAIN);
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            StartCoroutine(OnLoading());
        }

        private IEnumerator OnLoading() {
            while(!PlayerInfo.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            LoadShopData();
            while(!isLoad) {
                yield return new WaitForEndOfFrame();
            }
            OpenDefaultShop();
            UILoading.Instance.HideLoading();
        }

        private void LoadShopData() {
            var gclm = GlobalContainer.LocalizationManager;
            Noroshi.Shop.WebAPIRequester.List().Do(shopDataList => {
                for(int i = 0, iz = shopDataList.Shops.Length; i < iz; i++) {
                    var shopData = shopDataList.Shops[i];
                    if(shopData.ID == 6) {
                        if(!GameContent.IsOpen(GameContentID.NormalGuild, (ushort)PlayerInfo.Instance.PlayerLevel, PlayerInfo.Instance.GetTutorialStep())
                            && !GameContent.IsOpen(GameContentID.BeginnerGuild, (ushort)PlayerInfo.Instance.PlayerLevel, PlayerInfo.Instance.GetTutorialStep())) {
                            continue;
                        }
                    }
                    for(int j = 0, jz = shopBtnList.Length; j < jz; j++) {
                        if(shopData.ID == shopBtnList[j].id) {
                            var n = j;
                            shopBtnList[j].SetShopBtn(shopData);
                            shopBtnList[j].OnClickedBtn.Subscribe(id => {
                                shopDetail.OpenDetail(id);
                            });
                            shopBtnList[j].OnPlaySE.Subscribe(_ => {
                                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                            });
                            shopPanelList[j].SetShopPanel(shopData);
                            shopPanelList[j].OnBuyConfirm.Subscribe(data => {
                                shopConfirm.OpenConfirm(data);
                            });
                            shopPanelList[j].OnUpdateConfirm.Subscribe(data => {
                                openIndex = n;
                                shopUpdate.OpenUpdate(data);
                            });
                            break;
                        }
                    }
                }
                isLoad = true;
            }).Subscribe();
        }

        private void ChangeShopType(bool isToShops, float duration = 0.6f) {
            if(isToShops) {
                currentShopType = 1;
                TweenX.Add(background, duration, -Constant.SCREEN_BASE_WIDTH).EaseInOutQuart();
            } else {
                currentShopType = 0;
                TweenX.Add(background, duration, 0).EaseInOutQuart();
            }
        }

        private void OpenDefaultShop() {
            var prevScene = UILoading.Instance.HistoryList[UILoading.Instance.HistoryList.Count - 1];
            var index = -1;
            if(prevScene == Constant.SCENE_ARENA) {
                index = 3;
            } else if(prevScene == Constant.SCENE_EXPEDITION) {
                index = 4;
            } else if(prevScene == Constant.SCENE_GUILD) {
                index = 5;
            }
            if(UILoading.Instance.GetQuery(QueryKeys.IsShop) == 1) {
                ChangeShopType(true, 0.01f);
                UILoading.Instance.RemoveQuery(QueryKeys.IsShop);
            }
            if(index > -1) {
                shopDetail.OpenDetail(index + 1);
            } else {
                shopDetail.CloseDetail(0.01f);
            }
        }
    }
}
