﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class ShopConfirm : MonoBehaviour {
        [SerializeField] GameObject confirmPanel;
        [SerializeField] Text txtName;
        [SerializeField] ItemIcon itemIcon;
        [SerializeField] Text txtDescription;
        [SerializeField] Text txtHaveNum;
        [SerializeField] Text txtBuyNum;
        [SerializeField] Text txtPrice;
        [SerializeField] Image imgCurrency;
        [SerializeField] BtnCommon btnBuy;
        [SerializeField] BtnCommon btnCancel;

        public Subject<ShopItem.ShopItemData> OnBuyItem = new Subject<ShopItem.ShopItemData>();

        private ShopItem.ShopItemData itemData;
        private bool isOpen = false;

        private void Start() {
            btnBuy.OnClickedBtn.Subscribe(_ => {
                OnBuyItem.OnNext(itemData);
            });
            btnBuy.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.GET);
            });

            btnCancel.OnClickedBtn.Subscribe(_ => {
                CloseConfirm();
            });
            btnCancel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModalChild.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseConfirm();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        public void OpenConfirm(ShopItem.ShopItemData data) {
            var gclm = GlobalContainer.LocalizationManager;
            itemData = data;
            txtName.text = gclm.GetText(itemData.Merchandise.MerchandisePossessionObject.TextKey + ".Name");
            itemIcon.SetItemIcon(data.Merchandise.MerchandisePossessionObject);
            imgCurrency.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(itemData.Merchandise.PaymentPossessionObject.ID);
            txtDescription.text = gclm.GetText(itemData.Merchandise.MerchandisePossessionObject.TextKey + ".Description").TrimStart().Replace(" ", "\r\n");
            txtHaveNum.text = itemData.Merchandise.MerchandisePossessionObject.PossessingNum.ToString();
            txtBuyNum.text = itemData.Merchandise.MerchandisePossessionObject.Num.ToString();
            txtPrice.text = itemData.Merchandise.PaymentPossessionObject.Num.ToString();
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.2f, 1).From(0);
            BackButtonController.Instance.IsModalChildOpen(true);

            if(itemData.Merchandise.PaymentPossessionObject.Num > itemData.Merchandise.PaymentPossessionObject.PossessingNum) {
                txtPrice.color = Constant.TEXT_COLOR_NEGATIVE;
                btnBuy.SetEnable(false);
            } else {
                txtPrice.color = Constant.TEXT_COLOR_NORMAL_DARK;
                btnBuy.SetEnable(true);
            }
        }

        public void CloseConfirm() {
            TweenA.Add(gameObject, 0.2f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalChildOpen(false);
            });
        }
    }
}
