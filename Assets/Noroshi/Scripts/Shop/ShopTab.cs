﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UniLinq;

namespace Noroshi.UI {
    public class ShopTab : BtnCommon {
        [SerializeField] Text txtShopName;
        [SerializeField] GameObject charaImg;
        [SerializeField] GameObject enableObject;
        [SerializeField] GameObject disableObject;
        [SerializeField] GameObject iconNew;
        [SerializeField] GameObject iconSale;
        [SerializeField] Text txtHaveCurrency;
        [SerializeField] Text txtRemainTime;
        [SerializeField] ItemIcon itemIcon;
        [SerializeField] GameObject nounSale;
        [SerializeField] GameObject nounSoldOut;
        [SerializeField] GameObject remainTimeWrapper;

        private bool isActive;

        public void SetShopBtn(Noroshi.Core.WebApi.Response.Shop.Shop shopData) {
            txtShopName.text = GlobalContainer.LocalizationManager.GetText(shopData.TextKey + ".Name");
            if(!shopData.IsOpen) {
                SetEnable(false);
                return;
            }
            SetEnable(true);
            disableObject.SetActive(false);
            enableObject.SetActive(true);
            if(charaImg != null) {
                charaImg.SetActive(true);
            }
            if(txtHaveCurrency != null) {
                txtHaveCurrency.text = shopData.PaymentPossessionObject.PossessingNum.ToString();
            }
            if(itemIcon != null) {
                var displayItems = shopData.Merchandises.Where(i => !i.HasAlreadyBought && !i.IsClosedSoul).ToArray();
                var displayItemsLength = displayItems.Length;
                if(displayItemsLength > 0) {
                    var displayItem = displayItems.ElementAt(UnityEngine.Random.Range(0, displayItemsLength));
                    itemIcon.SetItemIcon(displayItem.MerchandisePossessionObject);
                    itemIcon.gameObject.SetActive(true);
                    nounSale.SetActive(true);
                    nounSoldOut.SetActive(false);
                } else {
                    itemIcon.gameObject.SetActive(false);
                    nounSale.SetActive(false);
                    nounSoldOut.SetActive(true);
                }
            }
//            if(isNew) {
//                iconNew.SetActive(true);
//            }
//            if(remainTime > 0) {
//                txtRemainTime.text = remainTime.ToString();
//                iconSale.SetActive(true);
//            }
        }
    }
}
