﻿using UnityEngine;
using System.Collections;
using UniRx;
using DG.Tweening;

namespace Noroshi.UI {
    public class ShopDetail : MonoBehaviour {
        [SerializeField] GameObject[] panelList;
        [SerializeField] GameObject[] characterList;
        [SerializeField] BtnCommon btnBack;
        [SerializeField] GameObject[] detailBgList;
        [SerializeField] GameObject btnBackToMain;

        private bool isOpen = false;

        private void Start() {
            btnBack.OnClickedBtn.Subscribe(_ => {
                CloseDetail();
            });
            btnBack.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseDetail();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        public void CloseDetail(float duration = 0.2f) {
            TweenA.Add(gameObject, duration, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
            btnBack.gameObject.SetActive(false);
            btnBackToMain.SetActive(true);
        }
        
        public void OpenDetail(int id) {
            var type = 0;
            if(id > 6) {
                type = 2;
            } else if(id > 3) {
                type = 1;
            }
            for(int i = 0, l = characterList.Length; i < l; i++) {
                characterList[i].SetActive(i == type);
                detailBgList[i].SetActive(i == type);
            }
            isOpen = true;
            gameObject.SetActive(true);
            BackButtonController.Instance.IsModalOpen(true);
            btnBack.gameObject.SetActive(true);
            btnBackToMain.SetActive(false);
            for(int i = 0, l = panelList.Length; i < l; i++) {
                if(i == id - 1) {
                    panelList[i].SetActive(true);
                    TweenA.Add(panelList[i], 0.3f, 1).From(0).Delay(0.55f).EaseInQuint();
                } else {
                    panelList[i].SetActive(false);
                }
            }
            TweenA.Add(gameObject, 0.05f, 1);
            characterList[type].transform.localPosition = new Vector2(
                -775, characterList[type].transform.localPosition.y
            );
            TweenX.Add(characterList[type], 0.15f, -425).Delay(0.4f);
            TweenA.Add(detailBgList[type], 0.8f, 1).From(0);
        }
    }
}
