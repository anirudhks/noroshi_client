﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class ShopUpdate : MonoBehaviour {
        [SerializeField] Text txtPrice;
        [SerializeField] Text txtMaxUpdateNum;
        [SerializeField] Text txtUpdateNum;
        [SerializeField] Image imgCurrency;
        [SerializeField] GameObject priceWrapper;
        [SerializeField] GameObject confirmText;
        [SerializeField] GameObject anyMoreText;
        [SerializeField] GameObject notEnoughCostText;
        [SerializeField] BtnCommon btnOK;
        [SerializeField] BtnCommon btnCancel;
        [SerializeField] BtnCommon btnClose;

        public Subject<uint> OnUpdateShop = new Subject<uint>();

        private uint shopID;
        private bool isOpen = false;

        private void Start() {
            btnOK.OnClickedBtn.Subscribe(_ => {
                OnUpdateShop.OnNext(shopID);
            });
            btnOK.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });

            btnCancel.OnClickedBtn.Subscribe(_ => {
                CloseUpdate();
            });
            btnCancel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnClose.OnClickedBtn.Subscribe(_ => {
                CloseUpdate();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModalChild.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseUpdate();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        public void OpenUpdate(Noroshi.Core.WebApi.Response.Shop.Shop shopData) {
            shopID = shopData.ID;
            txtPrice.text = shopData.ManualUpdatePossessionObject.Num.ToString();
            txtMaxUpdateNum.text = shopData.MaxMerchandiseManualUpdateNum.ToString();
            txtUpdateNum.text = shopData.CurrentMerchandiseManualUpdateNum.ToString();
            imgCurrency.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(shopData.ManualUpdatePossessionObject.ID);

            if(shopData.ManualUpdatePossessionObject.Num > shopData.ManualUpdatePossessionObject.PossessingNum
                || shopData.MaxMerchandiseManualUpdateNum <= shopData.CurrentMerchandiseManualUpdateNum) {
                btnOK.gameObject.SetActive(false);
                btnCancel.gameObject.SetActive(false);
                btnClose.gameObject.SetActive(true);
                confirmText.SetActive(false);
                if(shopData.MaxMerchandiseManualUpdateNum <= shopData.CurrentMerchandiseManualUpdateNum) {
                    priceWrapper.SetActive(false);
                    anyMoreText.SetActive(true);
                    notEnoughCostText.SetActive(false);
                } else {
                    priceWrapper.SetActive(true);
                    anyMoreText.SetActive(false);
                    notEnoughCostText.SetActive(true);
                }
            } else {
                btnOK.gameObject.SetActive(true);
                btnCancel.gameObject.SetActive(true);
                btnClose.gameObject.SetActive(false);
                confirmText.SetActive(true);
                priceWrapper.SetActive(true);
                anyMoreText.SetActive(false);
                notEnoughCostText.SetActive(false);
            }
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalChildOpen(true);
        }
        
        public void CloseUpdate() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalChildOpen(false);
            });
        }
    }
}
