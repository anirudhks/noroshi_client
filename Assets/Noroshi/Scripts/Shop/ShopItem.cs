﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class ShopItem : BtnCommon {
        public class ShopItemData {
            public int ShopID;
            public Noroshi.Core.WebApi.Response.Shop.Merchandise Merchandise;
        }

        [SerializeField] ItemIcon itemIcon;
        [SerializeField] Image imgPriceIcon;
        [SerializeField] Text txtItemName;
        [SerializeField] Text txtItemPrice;
        [SerializeField] Text txtBuyNum;
        [SerializeField] GameObject soldout;
        [SerializeField] GameObject closedSoul;

        public Subject<ShopItemData> OnItemClicked = new Subject<ShopItemData>();

        private ShopItemData itemData = new ShopItemData();
        private bool isLocked = false;

        public void SetItemInfo(Noroshi.Core.WebApi.Response.Shop.Merchandise merchandise) {
            itemIcon.SetItemIcon(merchandise.MerchandisePossessionObject);
            imgPriceIcon.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(merchandise.PaymentPossessionObject.ID);

            txtItemName.text = GlobalContainer.LocalizationManager.GetText(merchandise.MerchandisePossessionObject.TextKey + ".Name");
            txtItemPrice.text = merchandise.PaymentPossessionObject.Num.ToString();
            txtBuyNum.text = merchandise.MerchandisePossessionObject.Num.ToString();
            if(merchandise.PaymentPossessionObject.Num > merchandise.PaymentPossessionObject.PossessingNum) {
                TweenC.Add(txtItemPrice.gameObject, 0.01f, Constant.TEXT_COLOR_NEGATIVE);
            } else {
                TweenC.Add(txtItemPrice.gameObject, 0.01f, Constant.TEXT_COLOR_NORMAL_DARK);
            }
            if(merchandise.HasAlreadyBought) {
                base.SetEnable(false);
                soldout.SetActive(true);
            } else {
                base.SetEnable(true);
                soldout.SetActive(false);
                if(merchandise.IsClosedSoul) {
                    isLocked = true;
                    closedSoul.SetActive(true);
                } else {
                    isLocked = false;
                    closedSoul.SetActive(false);
                }
            }
            itemData.Merchandise = merchandise;
        }

        public uint GetMerchandiseID() {
            return itemData.Merchandise.ID;
        }

        public override void OnPointerClick(PointerEventData ped) {
            if(!isEnable) {return;}
            if(isLocked) {

            } else {
                base.OnPointerClick(ped);
                OnItemClicked.OnNext(itemData);
            }
        }
    }
}
