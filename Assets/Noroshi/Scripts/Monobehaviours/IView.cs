﻿using System.Collections.Generic;
using UniRx;
using Vector2 = UnityEngine.Vector2;

namespace Noroshi.MonoBehaviours
{
    public interface IView : IMonoBehaviour
    {
        /// <summary>
        /// ポジションを取得する。
        /// </summary>
        /// <returns>ポジション（xy座標）</returns>
        Vector2 GetPosition();
        /// <summary>
        /// ポジションをセットする。
        /// </summary>
        /// <param name="position">ポジション（xy座標）</param>
        void SetPosition(Vector2 position);

        /// <summary>
        /// 移動する。
        /// </summary>
        /// <param name="position">移動先ポジション（xy座標）</param>
        /// <param name="duration">移動時間（s）</param>
        /// <returns>移動先ポジション到達時に true が OnNext される Observable</returns>
        IObservable<bool> Move(Vector2 position, float duration);
        /// <summary>
        /// 移動する。
        /// </summary>
        /// <param name="positions">移動先ポジション（xy座標）</param>
        /// <param name="duration">移動時間（s）</param>
        /// <returns>x座標が移動先ポジション到達する毎に true が OnNext される Observable</returns>
        IObservable<bool> MoveWithOnNextBasedXPosition(IEnumerable<Vector2> positions, float duration);

        /// <summary>
        /// 移動を止める。
        /// </summary>
        void StopMove();

        /// <summary>
        /// 移動を一時的に停止させる。
        /// </summary>
        void SuspendMove();
        /// <summary>
        /// 一時停止中の移動を再開させる。
        /// </summary>
        void ResumeMove();
    }
}
