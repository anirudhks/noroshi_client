﻿using System;

namespace Noroshi.MonoBehaviours
{
    public interface IMonoBehaviour : IDisposable
    {
        bool SetActive(bool active);
        void SetParent(IMonoBehaviour parent, bool worldPositionStays);
        void RemoveParent();
    }
}
