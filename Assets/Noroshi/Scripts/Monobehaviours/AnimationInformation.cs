﻿namespace Noroshi.MonoBehaviours
{
    public class AnimationInformation
    {
        public AnimationInformation(string name, float[] triggerTimes, float finishTime)
        {
            Name = name;
            TriggerTimes = triggerTimes;
            FinishTime = finishTime;
        }

        public readonly string Name;
        public readonly float[] TriggerTimes;
        public readonly float FinishTime;
    }
}
