﻿using UnityEngine;

namespace Noroshi.MonoBehaviours
{
    /// 最低限の機能拡張した MonoBehaviour の子クラス。IF 対応も。
    public class MonoBehaviour : UnityEngine.MonoBehaviour, IMonoBehaviour
    {
        protected GameObject _gameObject;
        protected Transform _transform;

        protected void Awake()
        {
            _gameObject = gameObject;
            _transform = transform;
        }

        public virtual bool SetActive(bool active)
        {
            var res = _gameObject.activeSelf != active;
            _gameObject.SetActive(active);
            return res;
        }

        /// <summary>
        /// レイヤーの変更を行う
        /// </summary>
        /// <param name="layerName">適応するレイヤー名</param>
        /// <param name="isChild">子階層にもレイヤーの変更を適応させるか</param>
        protected void SetLayer(GameObject targetObject, string layerName, bool isChild)
        {
            targetObject.layer = LayerMask.NameToLayer(layerName);
            if (isChild)
            {
                foreach (var obj in targetObject.transform)
                {
                    var ts = obj as Transform;
                    if (ts == null) continue;
                    SetLayer(ts.gameObject, layerName, isChild);
                }
            }
        }

        public virtual void SetParent(IMonoBehaviour parent, bool worldPositionStays)
        {
            var parentMonoBehaviour = (MonoBehaviour)parent;
            _transform.SetParent(parentMonoBehaviour.GetTransform(), worldPositionStays);
        }
        public virtual void RemoveParent()
        {
            _transform.parent = null;
        }

        public virtual void Dispose()
        {
            Destroy(_gameObject);
        }

        public Transform GetTransform()
        {
            return _transform;
        }
    }
}
