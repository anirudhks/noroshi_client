﻿using System.Collections;
using UniRx;
using UnityEngine;

namespace Noroshi.MonoBehaviours
{
    public class Loader
    {
        public static IObservable<T> LoadComponentFromResourceAsync<T>(string path)
            where T : UnityEngine.Component
        {
            return _instantiateGameObjectFromResourceAsync(path).Select(go => go.GetComponent<T>());
        }
        static IObservable<GameObject> _instantiateGameObjectFromResourceAsync(string filePath)
        {
            return LoadObjectFromResourceAsync<GameObject>(filePath).Select(o => GameObject.Instantiate(o) as GameObject);
        }
        public static IObservable<T> LoadObjectFromResourceAsync<T>(string filePath)
            where T : UnityEngine.Object
        {
            return Observable.FromCoroutine<T>((observer, cancellationToken) => _loadObjectFromResourceCoroutine<T>(filePath, observer, cancellationToken));
        }
        static IEnumerator _loadObjectFromResourceCoroutine<T>(string filePath, IObserver<T> observer, CancellationToken cancellationToken)
            where T : UnityEngine.Object
        {
            var resourceRequest = Resources.LoadAsync(filePath, typeof(T));
            while (!resourceRequest.isDone && !cancellationToken.IsCancellationRequested)
            {
                yield return null;
            }
            if (cancellationToken.IsCancellationRequested) yield break;
            observer.OnNext(resourceRequest.asset as T);
            observer.OnCompleted();
        }
    }
}
