﻿using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using UnityEngine;
using DG.Tweening;

namespace Noroshi.MonoBehaviours
{
    /// <summary>
    /// （非 UI）汎用 View クラス。
    /// </summary>
    public class View : MonoBehaviour, IView
    {
        /// <summary>
        /// 移動に利用している Tweener。
        /// </summary>
        List<Tweener> _moveTweeners = new List<Tweener>();
        /// <summary>
        /// 移動終了時に正常終了かどうかが真偽値で OnNext される Subject。
        /// </summary>
        Subject<bool> _onFinishMoveSubject;
        /// <summary>
        /// 移動ロジックで利用する毎フレーム呼び出し用 Disposable。
        /// </summary>
        IDisposable _everyUpdateForMoveDisposable;

        /// <summary>
        /// ポジションを取得する。
        /// </summary>
        /// <returns>ポジション（xy座標）</returns>
        public Vector2 GetPosition()
        {
            return _transform.position;
        }
        /// <summary>
        /// ポジションをセットする。
        /// </summary>
        /// <param name="position">ポジション（xy座標）</param>
        public void SetPosition(Vector2 position)
        {
            _transform.position = position;
        }

        /// <summary>
        /// 移動する。
        /// </summary>
        /// <param name="position">移動先ポジション（xy座標）</param>
        /// <param name="duration">移動時間（s）</param>
        /// <returns>移動先ポジション到達時に true が OnNext される Observable</returns>
        public IObservable<bool> Move(Vector2 position, float duration)
        {
            return MoveWithOnNextBasedXPosition(new[] { position }, duration);
        }
        /// <summary>
        /// 移動する。
        /// </summary>
        /// <param name="positions">移動先ポジション（xy座標）</param>
        /// <param name="duration">移動時間（s）</param>
        /// <returns>x座標が移動先ポジション到達する毎に true が OnNext される Observable</returns>
        public IObservable<bool> MoveWithOnNextBasedXPosition(IEnumerable<Vector2> positions, float duration)
        {
            StopMove();
            var remainingPositionNum = positions.Count();
            var onFinishMoveSubject = new Subject<bool>();
            // Tweener を利用して移動設定。
            var finalPosition = positions.Last();
            _moveTweeners.Add(_transform.DOMove(finalPosition, duration).SetEase(Ease.Linear).OnComplete(() =>
            {
                // 実行タイミングによっては全てのポジション分を OnNext できていないのでここでカバー。
                for (var i = 0; i < remainingPositionNum; i++)
                {
                    onFinishMoveSubject.OnNext(true);
                }
                onFinishMoveSubject.OnCompleted();
            }));

            var movePositionQueue = new Queue<Vector2>(positions);
            _everyUpdateForMoveDisposable = Observable.EveryUpdate().Scan(GetPosition(), (previousPosition, _) =>
            {
                var currentPosition = GetPosition();
                // 最後の一つでなければ OnNext 試行。
                while (remainingPositionNum > 1)
                {
                    var candidate = movePositionQueue.Peek();
                    if ((previousPosition.x <= candidate.x && candidate.x < currentPosition.x) || (currentPosition.x < candidate.x && candidate.x <= previousPosition.x))
                    {
                        movePositionQueue.Dequeue();
                        remainingPositionNum--;
                        onFinishMoveSubject.OnNext(true);
                    }
                    else
                    {
                        break;
                    }
                }
                return currentPosition;
            }).Skip(1).Subscribe();

            _onFinishMoveSubject = onFinishMoveSubject;
            return onFinishMoveSubject.AsObservable();
        }
        /// <summary>
        /// 移動を止める。
        /// </summary>
        public void StopMove()
        {
            for (var i = 0; i < _moveTweeners.Count; i++)
            {
                _moveTweeners[i].Kill();
            }
            _moveTweeners.Clear();
            if (_onFinishMoveSubject != null)
            {
                _onFinishMoveSubject.OnCompleted();
                _onFinishMoveSubject = null;
            }
            if (_everyUpdateForMoveDisposable != null)
            {
                _everyUpdateForMoveDisposable.Dispose();
                _everyUpdateForMoveDisposable = null;
            }
        }
        /// <summary>
        /// 移動を一時的に停止させる。
        /// </summary>
        public void SuspendMove()
        {
            for (var i = 0; i < _moveTweeners.Count; i++)
            {
                _moveTweeners[i].Pause();
            }
        }
        /// <summary>
        /// 一時停止中の移動を再開させる。
        /// </summary>
        public void ResumeMove()
        {
            for (var i = 0; i < _moveTweeners.Count; i++)
            {
                _moveTweeners[i].Play();
            }
        }

        /// <summary>
        /// 移動用 Tweener を追加する。ここで追加された Tweener は本メソッッドの移動制御メソッドに従う。
        /// </summary>
        /// <param name="tweener">Tweener</param>
        protected void _addMoveTweener(Tweener tweener)
        {
            _moveTweeners.Add(tweener);
        }

        /// <summary>
        /// リソースを破棄する。
        /// </summary>
        public override void Dispose()
        {
            StopMove();
            base.Dispose();
        }
    }
}
