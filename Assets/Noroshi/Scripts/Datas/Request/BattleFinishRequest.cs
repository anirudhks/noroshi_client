﻿namespace Noroshi.Datas.Request
{
    // バトル終了時通信リクエスト
    public class BattleFinishRequest
    {
        public byte Category { get; set; }
        public uint ID { get; set; }
        public string Result { get; set; }
        public string Hash { get; set; }
    }
}
