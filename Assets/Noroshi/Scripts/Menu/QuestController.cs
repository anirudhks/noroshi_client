using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Core.WebApi.Response.Quest;

namespace Noroshi.UI {
    public class QuestController : MonoBehaviour {
        [SerializeField] BtnCommon btnOpenDailyQuest;
        [SerializeField] BtnCommon btnOpenQuest;
        [SerializeField] BtnCommon dailyQuestOverlay;
        [SerializeField] BtnCommon questOverlay;
        [SerializeField] BtnCommon btnCloseDailyQuest;
        [SerializeField] BtnCommon btnCloseQuest;
        [SerializeField] GameObject dailyQuestContainer;
        [SerializeField] GameObject dailyQuestWrapper;
        [SerializeField] GameObject questContainer;
        [SerializeField] GameObject questWrapper;
        [SerializeField] QuestPanel questPanel;
        [SerializeField] QuestComplete questComplete;
        [SerializeField] GameObject iconMenuDailyQuestAttention;
        [SerializeField] GameObject iconMenuQuestAttention;
        [SerializeField] GameObject iconDailyQuestAttention;
        [SerializeField] GameObject iconQuestAttention;
        [SerializeField] GameObject noDailyQuestAlert;
        [SerializeField] GameObject noQuestAlert;

        private List<QuestPanel> dailyQuestPanelList;
        private List<QuestPanel> questPanelList;
        private bool isInitialQuestOpen = false;
        private bool isInitialDailyQuestOpen = false;
        private Quest[] initialQuestData;
        private Quest[] initialDailyQuestData;
        private bool isOpenQuest = false;
        private bool isOpenDailyQuest = false;


        private void Start() {
            BackButtonController.Instance.OnCloseCommonPartsChild.Subscribe(_ => {
                if(isOpenQuest) {
                    CloseQuest();
                } else if(isOpenDailyQuest) {
                    CloseDailyQuest();
                } else {
                    return;
                }
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            StartCoroutine(OnLoading());
        }

        private IEnumerator OnLoading() {
            while(!PlayerInfo.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            CheckUnlockQuest();
        }

        private void CheckUnlockQuest() {
            if(PlayerInfo.Instance.GetTutorialStep() >= Noroshi.Core.Game.Player.TutorialStep.ClearStoryStage_1_1_3) {
                btnOpenQuest.SetEnable(true);
                TweenC.Add(btnOpenQuest.gameObject, 0.01f, Color.white);
                LoadQuestList();
                btnOpenQuest.OnClickedBtn.Subscribe(_ => {
                    OpenQuest();
                });
                btnOpenQuest.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
                questOverlay.OnClickedBtn.Subscribe(_ => {
                    CloseQuest();
                });
                questOverlay.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
                });
                btnCloseQuest.OnClickedBtn.Subscribe(_ => {
                    CloseQuest();
                });
                btnCloseQuest.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
                });
            }

            if(PlayerInfo.Instance.GetTutorialStep() >= Noroshi.Core.Game.Player.TutorialStep.ClearStoryStage_1_3_2) {
                btnOpenDailyQuest.SetEnable(true);
                TweenC.Add(btnOpenDailyQuest.gameObject, 0.01f, Color.white);
                LoadDailyQuestList();
                btnOpenDailyQuest.OnClickedBtn.Subscribe(_ => {
                    OpenDailyQuest();
                });
                btnOpenDailyQuest.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
                dailyQuestOverlay.OnClickedBtn.Subscribe(_ => {
                    CloseDailyQuest();
                });
                dailyQuestOverlay.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
                });
                btnCloseDailyQuest.OnClickedBtn.Subscribe(_ => {
                    CloseDailyQuest();
                });
                btnCloseDailyQuest.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
                });
            }
        }

        private void LoadDailyQuestList() {
            var isShowQuestPanel = false;
            dailyQuestPanelList = new List<QuestPanel>();
            foreach(Transform quest in dailyQuestWrapper.transform) {
                Destroy(quest.gameObject);
            }
            Noroshi.Menu.WebApiRequester.DailyList().Do(data => {
                if(!isInitialDailyQuestOpen) {initialDailyQuestData = data.Quests;}
                if(data.Quests.Length > 0) {
                    noDailyQuestAlert.SetActive(false);

                    foreach(var quest in data.Quests) {
                        if(!quest.HasAlreadyReceivedReward) {
                            isShowQuestPanel = true;
                            if(isInitialDailyQuestOpen) {CreateDailyQuestPanel(quest);}
                            if(quest.CanReceiveReward) {
                                iconMenuDailyQuestAttention.SetActive(true);
                                iconDailyQuestAttention.SetActive(true);
                            }
                        }
                    }
                    for(int i = dailyQuestPanelList.Count - 1; i > -1; i--) {
                        dailyQuestPanelList[i].CheckClearState();
                    }
                    if(!isShowQuestPanel) {noDailyQuestAlert.SetActive(true);}
                } else {
                    noDailyQuestAlert.SetActive(true);
                }
            }).Subscribe();
        }

        private void LoadQuestList() {
            var isShowQuestPanel = false;
            questPanelList = new List<QuestPanel>();
            foreach(Transform quest in questWrapper.transform) {
                Destroy(quest.gameObject);
            }
            Noroshi.Menu.WebApiRequester.QuestList().Do(data => {
                if(!isInitialQuestOpen) {initialQuestData = data.Quests;}
                if(data.Quests.Length > 0) {
                    noQuestAlert.SetActive(false);

                    foreach(var quest in data.Quests) {
                        if(!quest.HasAlreadyReceivedReward) {
                            isShowQuestPanel = true;
                            if(isInitialQuestOpen) {CreateQuestPanel(quest);}
                            if(quest.CanReceiveReward) {
                                iconMenuQuestAttention.SetActive(true);
                                iconQuestAttention.SetActive(true);
                            }
                        }
                    }
                    for(int i = questPanelList.Count - 1; i > -1; i--) {
                        questPanelList[i].CheckClearState();
                    }
                    if(!isShowQuestPanel) {noQuestAlert.SetActive(true);}
                } else {
                    noQuestAlert.SetActive(true);
                }
            }).Subscribe();
        }

        private void CreateDailyQuestPanel(Quest questData) {
            var panel = Instantiate(questPanel);
            panel.transform.SetParent(dailyQuestWrapper.transform);
            panel.transform.localScale = Vector3.one;
            panel.SetQuestInfo(questData, true);
            dailyQuestPanelList.Add(panel);
            panel.OnQuestComplete.Subscribe(id => {
                Noroshi.Menu.WebApiRequester.ReceiveDailyReward(questData.ID).Do(rewardData => {
                    iconMenuDailyQuestAttention.SetActive(false);
                    iconDailyQuestAttention.SetActive(false);
                    LoadDailyQuestList();
                    questComplete.OpenQuestResult(rewardData);
                }).Subscribe();
            });
        }

        private void CreateQuestPanel(Quest questData) {
            var panel = Instantiate(questPanel);
            panel.transform.SetParent(questWrapper.transform);
            panel.transform.localScale = Vector3.one;
            panel.SetQuestInfo(questData, false);
            questPanelList.Add(panel);
            panel.OnQuestComplete.Subscribe(id => {
                Noroshi.Menu.WebApiRequester.ReceiveReward(questData.ID).Do(rewardData => {
                    iconMenuQuestAttention.SetActive(false);
                    iconQuestAttention.SetActive(false);
                    LoadQuestList();
                    questComplete.OpenQuestResult(rewardData);
                }).Subscribe();
            });
        }

        private void OpenDailyQuest() {
            if(!isInitialDailyQuestOpen && initialDailyQuestData.Length > 0) {
                foreach(var quest in initialDailyQuestData) {
                    if(!quest.HasAlreadyReceivedReward) {
                        CreateDailyQuestPanel(quest);
                    }
                }
                for(int i = dailyQuestPanelList.Count - 1; i > -1; i--) {
                    dailyQuestPanelList[i].CheckClearState();
                }
                isInitialDailyQuestOpen = true;
            }
            isOpenDailyQuest = true;
            dailyQuestOverlay.gameObject.SetActive(true);
            dailyQuestContainer.SetActive(true);
            TweenA.Add(dailyQuestContainer, 0.2f, 1);
            BackButtonController.Instance.IsCommonPartsChildOpen(true);
        }

        private void OpenQuest() {
            if(!isInitialQuestOpen && initialQuestData.Length > 0) {
                foreach(var quest in initialQuestData) {
                    if(!quest.HasAlreadyReceivedReward) {
                        CreateQuestPanel(quest);
                    }
                }
                for(int i = questPanelList.Count - 1; i > -1; i--) {
                    questPanelList[i].CheckClearState();
                }
                isInitialQuestOpen = true;
            }
            isOpenQuest = true;
            questOverlay.gameObject.SetActive(true);
            questContainer.SetActive(true);
            TweenA.Add(questContainer, 0.2f, 1);
            BackButtonController.Instance.IsCommonPartsChildOpen(true);
        }

        private void CloseDailyQuest() {
            TweenA.Add(dailyQuestContainer, 0.2f, 0).Then(() => {
                isOpenDailyQuest = false;
                dailyQuestOverlay.gameObject.SetActive(false);
                dailyQuestContainer.SetActive(false);
                BackButtonController.Instance.IsCommonPartsChildOpen(false);
            });
        }

        private void CloseQuest() {
            TweenA.Add(questContainer, 0.2f, 0).Then(() => {
                isOpenQuest = false;
                questOverlay.gameObject.SetActive(false);
                questContainer.SetActive(false);
                BackButtonController.Instance.IsCommonPartsChildOpen(false);
            });
        }
    }
}
