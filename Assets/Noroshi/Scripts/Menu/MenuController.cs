﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class MenuController : MonoBehaviour {
        [SerializeField] BtnCommon btnMenu;
        [SerializeField] BtnCommon btnOverlay;
        [SerializeField] GameObject menuListContainer;
        [SerializeField] GameObject menuListFrame;
        [SerializeField] GameObject[] menuList;
        [SerializeField] GameObject iconMenuNotification;
        [SerializeField] GameObject iconCharacterNotification;

        private bool isOpen = false;
        private bool isActive = true;
        private int menuNum;
        private float duration = 0.2f;
        private float delay = 0.03f;

        private void Start() {
            menuNum = menuList.Length;
            btnMenu.OnClickedBtn.Subscribe(_ => {
                if(!isActive) {return;}
                isActive = false;
                if(isOpen) {
                    CloseMenu();
                } else {
                    OpenMenu();
                }
            }).AddTo(this);
            btnMenu.OnPlaySE.Subscribe(_ => {
                if(isOpen) {
                    TweenNull.Add(gameObject, 0.2f).Then(() => {
                        SoundController.Instance.PlaySE(SoundController.SEKeys.MENU);
                    });
                } else {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.MENU);
                }
            });

            btnOverlay.OnClickedBtn.Subscribe(_ => {
                if(!isActive) {return;}
                isActive = false;
                CloseMenu();
            }).AddTo(this);
            btnOverlay.OnPlaySE.Subscribe(_ => {
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.MENU);
                });
            });

            BackButtonController.Instance.OnCloseCommonParts.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseMenu();
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.MENU);
                });
            }).AddTo(this);

            if(SceneManager.GetActiveScene().name != Constant.SCENE_CHARACTER_LIST) {
                StartCoroutine(OnLoadingItemList());
            }
        }

        private IEnumerator OnLoadingItemList() {
            while(!ItemListManager.Instance.IsLoad || !BattleCharacterSelect.Instance.isLoad || !PlayerInfo.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            CheckCharacterNotification();
        }

        private void OpenMenu() {
            isOpen = true;

            btnOverlay.gameObject.SetActive(true);
            menuListContainer.SetActive(true);
            TweenA.Add(btnOverlay.gameObject, 0.1f, 0.5f).From(0);
            TweenSY.Add(menuListFrame, 0.15f, 1).EaseOutCubic();
            for(var i = 0; i < menuNum; i++) {
                menuList[i].SetActive(true);
                if(i == menuNum - 1) {
                    TweenY.Add(menuList[i], duration, i * -110 - 110)
                        .Delay(i * -delay + delay * (menuNum + 4))
                        .EaseOutBackWith(1.1f)
                        .Then(() => {isActive = true;});
                } else {
                    TweenY.Add(menuList[i], duration, i * -110 - 110)
                        .Delay(i * -delay + delay * (menuNum + 4))
                        .EaseOutBackWith(1.1f);
                }
            }
            BackButtonController.Instance.IsCommonPartsOpen(true);
        }

        private void CloseMenu() {
            isOpen = false;
            TweenSY.Add(menuListFrame, 0.12f, 0).Delay(delay * (menuNum + 7)).EaseInCubic().Then(() => {
                TweenA.Add(btnOverlay.gameObject, 0.1f, 0).Then(() => {
                    menuListContainer.SetActive(false);
                    btnOverlay.gameObject.SetActive(false);
                    isActive = true;
                    BackButtonController.Instance.IsCommonPartsOpen(false);
                });
            });
            for(var i = 0; i < menuNum; i++) {
                var index = i;
                TweenY.Add(menuList[index], duration, 50)
                    .Delay(i * delay)
                    .EaseInBackWith(1.2f)
                    .Then(() => {
                        menuList[index].SetActive(false);
                    });
            }
            if(!btnOverlay.gameObject.activeSelf) {isActive = true;}
        }

        private void CheckCharacterNotification() {
            if(PlayerInfo.Instance.GetTutorialStep() < Noroshi.Core.Game.Player.TutorialStep.ClearStoryStage_1_1_2) {
                return;
            }
            var characterStatusList = BattleCharacterSelect.Instance.GetCharacterStatusList();

            foreach(var chara in characterStatusList) {
                var masterData = GlobalContainer.MasterManager.CharacterMaster.Get(chara.CharacterID);
                for(int i = 0, l = chara.GearIDs.Length; i < l; i++) {
                    if(chara.GearIDs[i] < 1) {
                        if(chara.Level >= ItemListManager.Instance.GetGearInfo(masterData.GearIDs[chara.PromotionLevel - 1][i]).Level) {
                            if(ItemListManager.Instance.GetItemCount(masterData.GearIDs[chara.PromotionLevel - 1][i]) > 0 ||
                                CheckCreateGear(masterData.GearIDs[chara.PromotionLevel - 1][i])) {
                                iconMenuNotification.SetActive(true);
                                iconCharacterNotification.SetActive(true);
                                return;
                            }
                        }
                    }
                }
            }
        }

        private bool CheckCreateGear(uint id) {
            var recipes = ItemListManager.Instance.GetGearRecipe(id);
            var isCreate = true;
            if(recipes.Count < 1) {
                isCreate = false;
            } else {
                foreach(var recipe in recipes) {
                    if(recipe["num"] > ItemListManager.Instance.GetItemCount(recipe["id"])) {
                        if(ItemListManager.Instance.GetGearRecipe(recipe["id"]) != null) {
                            isCreate = CheckCreateGear(recipe["id"]);
                            if(!isCreate) {break;}
                        } else {
                            isCreate = false;
                            break;
                        }
                    }
                }
            }
            return isCreate;
        }
    }
}
