using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UniRx;
using Noroshi.Core.Game.GameContent;
using Noroshi.Core.WebApi.Response.Quest;
using Noroshi.Core.WebApi.Response.Possession;

namespace Noroshi.UI {
    public class QuestPanel : MonoBehaviour,  IPointerClickHandler {
        [SerializeField] Text txtTitle;
        [SerializeField] Text txtRemainNum;
        [SerializeField] Text txtNeedNum;
        [SerializeField] Image imgQuest;
        [SerializeField] GameObject iconGo;
        [SerializeField] GameObject iconReceive;
        [SerializeField] ItemIcon[] itemIconList;
        [SerializeField] Text[] txtRewardList;
        [SerializeField] Sprite[] spriteQuestList;

        public Subject<bool> OnQuestComplete = new Subject<bool>();

        private string sceneName;
        private bool isGoToEliteWorld = false;
        private bool isClear = false;

        private void SetReward(PossessionObject[] rewards) {
            for(int i = 0, l = txtRewardList.Length; i < l; i++) {
                if(i < rewards.Length) {
                    itemIconList[i].SetItemIcon(rewards[i]);
                    txtRewardList[i].text = " x" + rewards[i].Num;
                    txtRewardList[i].gameObject.SetActive(true);
                } else {
                    txtRewardList[i].gameObject.SetActive(false);
                }
            }
        }

        private void SetQuestIcon(uint id) {
            if(id > 55) {
                imgQuest.sprite = spriteQuestList[9];
            } else if(id > 50) {
                imgQuest.sprite = spriteQuestList[1];
            } else if(id > 45) {
                imgQuest.sprite = spriteQuestList[5];
            } else if(id > 40) {
                imgQuest.sprite = spriteQuestList[4];
            } else if(id > 35) {
                imgQuest.sprite = spriteQuestList[4];
            } else if(id > 30) {
                imgQuest.sprite = spriteQuestList[4];
            } else if(id > 25) {
                imgQuest.sprite = spriteQuestList[4];
            } else if(id > 20) {
                imgQuest.sprite = spriteQuestList[4];
            } else if(id > 15) {
                imgQuest.sprite = spriteQuestList[4];
            } else if(id > 10) {
                imgQuest.sprite = spriteQuestList[4];
            } else if(id > 5) {
                imgQuest.sprite = spriteQuestList[4];
            } else {
                imgQuest.sprite = spriteQuestList[4];
            }
        }

        private void SetDailyQuestIcon(uint id) {
            if(id > 16) {
                imgQuest.sprite = spriteQuestList[12];
            } else if(id > 13) {
                imgQuest.sprite = spriteQuestList[11];
            } else if(id == 13) {
                imgQuest.sprite = spriteQuestList[10];
            } else if(id == 12) {
                imgQuest.sprite = spriteQuestList[9];
            } else if(id == 11) {
                imgQuest.sprite = spriteQuestList[8];
            } else if(id == 10) {
                imgQuest.sprite = spriteQuestList[7];
            } else if(id == 9) {
                imgQuest.sprite = spriteQuestList[6];
            } else if(id == 8) {
                imgQuest.sprite = spriteQuestList[4];
            } else if(id == 7) {
                imgQuest.sprite = spriteQuestList[5];
            } else if(id == 6) {
                imgQuest.sprite = spriteQuestList[4];
            } else if(id == 5) {
                imgQuest.sprite = spriteQuestList[3];
            } else if(id == 4) {
                imgQuest.sprite = spriteQuestList[2];
            } else if(id == 3) {
                imgQuest.sprite = spriteQuestList[1];
            } else {
                imgQuest.sprite = spriteQuestList[0];
            }
        }

        private void SetSceneName(GameContentID id) {
            switch(id) {
                case GameContentID.Story: sceneName = Constant.SCENE_STORY; break;
                case GameContentID.BackStory:
                    sceneName = Constant.SCENE_STORY;
                    isGoToEliteWorld = true;
                    break;
                case GameContentID.Arena: sceneName = Constant.SCENE_ARENA; break;
                case GameContentID.Training: sceneName = Constant.SCENE_TRAINING; break;
                case GameContentID.Expedition: sceneName = Constant.SCENE_EXPEDITION; break;
                case GameContentID.Trial: sceneName = Constant.SCENE_TRIAL; break;
                default: iconGo.SetActive(false); break;
            }
        }

        public void SetQuestInfo(Quest questData, bool isDailyQuest) {
            txtTitle.text = GlobalContainer.LocalizationManager.GetText(questData.TextKey + ".Name");
            txtRemainNum.text = questData.Current.ToString();
            txtNeedNum.text = questData.Threshold.ToString();
            SetReward(questData.PossessionObjects);
            SetSceneName((GameContentID)questData.GameContentID);
            if(isDailyQuest) {
                SetDailyQuestIcon(questData.ID);
            } else {
                SetQuestIcon(questData.ID);
            }
            if(questData.CanReceiveReward) {
                isClear = true;
            }
        }

        public void CheckClearState() {
            if(isClear) {
                iconReceive.SetActive(true);
                iconGo.SetActive(false);
                transform.SetAsFirstSibling();
            }
        }

        public virtual void OnPointerClick(PointerEventData ped) {
            if(isClear) {
                OnQuestComplete.OnNext(true);
                gameObject.SetActive(false);
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            } else if(iconGo.activeSelf) {
                if(isGoToEliteWorld) {
                    UILoading.Instance.SetQuery(QueryKeys.WorldType, 1);
                }
                UILoading.Instance.LoadScene(sceneName);
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            }
        }
    }
}
