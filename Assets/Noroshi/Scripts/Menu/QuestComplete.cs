using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.Core.Game.Possession;
using Noroshi.Core.WebApi.Response.Players;
using Noroshi.Core.WebApi.Response.Quest;

namespace Noroshi.UI {
    public class QuestComplete : MonoBehaviour {
        [SerializeField] Text txtQuestName;
        [SerializeField] BtnCommon btnOverlay;
        [SerializeField] ItemIcon[] itemIconList;
        [SerializeField] Text[] txtRewardList;

        private AddPlayerExpResult addPlayerExpResult;

        private void Start() {
            btnOverlay.OnClickedBtn.Subscribe(_ => {
                gameObject.SetActive(false);
                PlayerInfo.Instance.UpdatePlayerExp(addPlayerExpResult);
            });
            btnOverlay.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        public void OpenQuestResult(ReceiveRewardResponse questData) {
            var rewards = questData.Quest.PossessionObjects;
            txtQuestName.text = GlobalContainer.LocalizationManager.GetText(questData.Quest.TextKey + ".Name");
            for(int i = 0, l = txtRewardList.Length; i < l; i++) {
                if(i < rewards.Length) {
                    itemIconList[i].SetItemIcon(rewards[i]);
                    if(rewards[i].Category == (byte)PossessionCategory.Status) {
                        if(rewards[i].ID == (byte)PossessionStatusID.Gold) {
                            PlayerInfo.Instance.ChangeHaveGold((int)rewards[i].Num);
                        } else if(rewards[i].ID == (byte)PossessionStatusID.CommonGem
                            || rewards[i].ID == (byte)PossessionStatusID.FreeGem) {
                            PlayerInfo.Instance.ChangeHaveGem((int)rewards[i].Num);
                        }
                    } else if(rewards[i].Category == (byte)PossessionCategory.Character) {
                        var getModal = Instantiate(Resources.Load<GetCharacterModal>("UI/GetCharacterModal"));
                        getModal.OpenModal(rewards[i].ID);
                    } else if(rewards[i].Category != (byte)PossessionCategory.Status) {
                        var isSoul = rewards[i].Category == (byte)PossessionCategory.Soul;
                        ItemListManager.Instance.ChangeItemCount(rewards[i].ID, (int)rewards[i].Num, isSoul);
                    }
                    txtRewardList[i].text = "x " + rewards[i].Num;
                    txtRewardList[i].gameObject.SetActive(true);
                } else {
                    txtRewardList[i].gameObject.SetActive(false);
                }
            }
            addPlayerExpResult = questData.AddPlayerExpResult;
            gameObject.SetActive(true);
        }
    }
}
