using UniLinq;
using UniRx;
using Noroshi.Core.Game.Character;

namespace Noroshi.Game
{
    public class CharacterStatus : Core.Game.Character.CharacterStatus
    {
        public CharacterStatus(IPersonalCharacter personalData, Core.WebApi.Response.Master.Character masterData) : base(personalData, masterData)
        {
            for (var i = 0; i < personalData.EquippedGears.Length; i ++)
            {
                var equippedGear = personalData.EquippedGears[i];
                var gearMaster = GlobalContainer.MasterManager.ItemMaster.GetGear(equippedGear.GearID);
                _gearContainer.Add(equippedGear.No, new Core.Game.Character.Gear(gearMaster, equippedGear.Level));                
            }
        }
    }
}
