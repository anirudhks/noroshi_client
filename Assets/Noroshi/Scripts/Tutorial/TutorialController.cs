using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.CharacterList;
using Noroshi.Core.Game.Player;

namespace Noroshi.UI {
    public class TutorialController : MonoBehaviour {
        [SerializeField] Canvas canvas;
        [SerializeField] GameObject overlay;
        [SerializeField] BtnCommon btnOverlay;
        [SerializeField] GameObject pointer;
        [SerializeField] GameObject descriptionContainer;
        [SerializeField] GameObject characterBalloon;
        [SerializeField] GameObject balloon;
        [SerializeField] Text txtDescription;
        [SerializeField] GameObject iconNextDescription;
        [SerializeField] BtnLoadLevel btnTutorialTransition;
        [SerializeField] SkeletonAnimation animCharacter;
        [SerializeField] GameObject openContentPanel;
        [SerializeField] GameObject[] contentTextList;

        private bool haveCanvas;
        private int defaultOrder;

        private CompositeDisposable disposables = new CompositeDisposable();
        private CompositeDisposable panelDisposables = new CompositeDisposable();
        private CompositeDisposable overlayDisposables = new CompositeDisposable();

        private void SetFocus(GameObject obj, float offsetX = 0, float offsetY = 0, float rotation = 0) {
            Canvas objCanvas = obj.GetComponent<Canvas>();
            var objPosition = obj.transform.position;
            if(objCanvas == null) {
                haveCanvas = false;
                obj.AddComponent<GraphicRaycaster>();
                objCanvas = obj.GetComponent<Canvas>();
            } else {
                haveCanvas = true;
                defaultOrder = objCanvas.sortingOrder;
            }
            objCanvas.overrideSorting = true;
            objCanvas.overridePixelPerfect = true;
            objCanvas.pixelPerfect = true;
            objCanvas.sortingOrder = 150;
            objPosition.x += offsetX;
            objPosition.y += offsetY;
            pointer.transform.position = objPosition;
            pointer.transform.rotation = Quaternion.Euler(new Vector3(0, 0, rotation));
            pointer.SetActive(true);
            gameObject.SetActive(false);
            gameObject.SetActive(true);
        }

        private void RemoveFocus(GameObject obj) {
            if(haveCanvas) {
                obj.GetComponent<Canvas>().sortingOrder = defaultOrder;
            } else {
                obj.GetComponent<Canvas>().overrideSorting = false;
            }
        }

        private void SetDescription(int index, float positionX, float positionY, bool isShowChara, bool isReverse, int actionNum = 1) {
            txtDescription.text = GlobalContainer.LocalizationManager.GetText("Tutorial.Description" + index);
            descriptionContainer.transform.localPosition = new Vector2(positionX, positionY);
            if(isShowChara) {
                characterBalloon.SetActive(true);
                balloon.SetActive(false);
                TweenNull.Add(characterBalloon, 0.2f).Then(() => {
                    if(animCharacter.state != null) {
                        animCharacter.state.SetAnimation(0, "action" + actionNum, true);
                    }
                });
            } else {
                characterBalloon.SetActive(false);
                balloon.SetActive(true);
            }
            if(isReverse) {
                characterBalloon.transform.localScale = new Vector3(-1, 1, 1);
            } else {
                characterBalloon.transform.localScale = Vector3.one;
            }
            descriptionContainer.SetActive(true);
        }

        private void HideDescription() {
            descriptionContainer.SetActive(false);
        }

        private IEnumerator OnCheckingGetCharacter(int index, uint characterID) {
            while(!BattleCharacterSelect.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            gameObject.SetActive(false);
            var pcID = BattleCharacterSelect.Instance.GetPlayerCharacterId(new uint[]{characterID});
            if(index == 10) {
                var evolutionModal = Instantiate(Resources.Load<EvolutionCharacterModal>("UI/EvolutionCharacterModal"));
                evolutionModal.OpenModal(characterID);
            } else {
                if(pcID.Length < 1) {
                    var getModal = Instantiate(Resources.Load<GetCharacterModal>("UI/GetCharacterModal"));
                    getModal.OpenModal(characterID);
                }
            }
        }

        private IEnumerator OnCheckingCharacterPanel(int index, uint characterID) {
            var characterList = GameObject.FindObjectOfType<CharacterListViewModel>();
            while(!characterList.CheckCharacterCreated()) {
                yield return new WaitForEndOfFrame();
            }
            var characterPanelList = GameObject.FindObjectsOfType<CharacterPanel>();
            var scroller = GameObject.Find("CharacterListScroller").GetComponent<ScrollRect>();
            var content = GameObject.Find("CharacterListContainer").GetComponent<RectTransform>();
            var btnCloseCharacterDetail = GameObject.Find("BtnCloseCharacterDetail").GetComponent<BtnCommon>();
            btnCloseCharacterDetail.OnClickedBtn.Subscribe(_ => {
                UILoading.Instance.LoadScene(Constant.SCENE_STORY);
                Dispose();
            }).AddTo(disposables);
            foreach(var panel in characterPanelList) {
                if(panel.charaData.characterID == characterID) {
                    scroller.enabled = false;
                    var diffY = -panel.transform.localPosition.y - 190;
                    float initialPosition = 1 - (diffY / (content.sizeDelta.y));
                    if(initialPosition < 0) {initialPosition = 0;}
                    if(initialPosition > 1) {initialPosition = 1;}
                    scroller.verticalNormalizedPosition = initialPosition;
                    SetFocus(panel.gameObject, 0.1f, 0.6f);
                    if(index == 2) {
                        SetDescription(7, 0, -140, false, false);
                    } else if(index == 5) {
                        SetDescription(14, 0, -140, false, false);
                    } else if(index == 11) {
                        SetDescription(33, 0, -140, false, false);
                    }
                    panel.OnOpenDetail.Subscribe(_ => {
                        RemoveFocus(panel.gameObject);
                        scroller.enabled = true;
                        if(index == 2) {
                            SetCharacterTutorial2();
                        } else if(index == 5) {
                            SetCharacterTutorial5();
                        } else if(index == 6) {
                            SetCharacterTutorial6();
                        } else if(index == 11) {
                            SetCharacterTutorial11();
                        }
                        DisposePanel();
                    }).AddTo(panelDisposables);
                    break;
                }
            }
        }

        private void StartGetCharacterTutorial(int index, uint characterID) {
            StartCoroutine(OnCheckingGetCharacter(index, characterID));
        }

        private void StartCharacterTutorial(int index, uint characterID) {
            if(SceneManager.GetActiveScene().name != Constant.SCENE_CHARACTER_LIST) {
                var btnMenu = GameObject.Find("BtnMenu");
                if(index == 2) {
                    SetDescription(6, 80, -210, true, false, 3);
                } else if(index == 5) {
                    SetDescription(13, 80, -210, true, false, 4);
                } else if(index == 6) {
                    SetDescription(19, 80, -210, true, false, 4);
                } else if(index == 11) {
                    SetDescription(32, 80, -210, true, false, 1);
                }
                if(btnMenu == null) {
                    gameObject.SetActive(false);
                    return;
                }
                TweenNull.Add(gameObject, 0.1f).Then(() => {
                    SetFocus(btnMenu, -1.2f, -0.6f, 120);
                });
                btnMenu.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(_ => {
                    pointer.SetActive(false);
                    RemoveFocus(btnMenu);
                    TweenNull.Add(gameObject, 0.4f).Then(() => {
                        var btnMenu1 = GameObject.Find("Menu1");
                        SetFocus(btnMenu1, -0.5f, -0.2f, 110);
                        btnMenu1.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
                            Dispose();
                        }).AddTo(disposables);
                    });
                }).AddTo(disposables);
            } else {
                StartCoroutine(OnCheckingCharacterPanel(index, characterID));
            }
        }

        /// <summary>
        /// Tutorial Start
        /// </summary>
        private void StartTutorial1() {
            if(SceneManager.GetActiveScene().name == Constant.SCENE_MAIN) {
                var btnStory = GameObject.Find("BtnStory");
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    SetFocus(btnStory.gameObject, 0, 1.5f);
                    SetDescription(1, 210, -220, true, false);
                });
            } else if(SceneManager.GetActiveScene().name == Constant.SCENE_STORY) {
                var playerInfo = GameObject.Find("PlayerInfo");
                var storySelect = playerInfo.transform.parent.Find("StorySelect");
                var storyMap = playerInfo.transform.parent.Find("StoryMap");
                if(UILoading.Instance.GetQuery(QueryKeys.IsFirstStoryBattle) < 1) {
                    storySelect.GetComponent<StorySelect>().OnCreatePanel.Subscribe(_ => {
                        var btnEpisode = GameObject.Find("BtnEpisode");
                        TweenNull.Add(gameObject, 0.5f).Then(() => {
                            SetFocus(btnEpisode, 0, 0.4f);
                            SetDescription(2, 230, 0, false, false);
                        });
                        btnEpisode.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
                            RemoveFocus(btnEpisode);
                            pointer.SetActive(false);
                            HideDescription();
                        });
                    }).AddTo(disposables);
                }
                storyMap.GetComponent<StoryMap>().OnOpenMap.Subscribe(_ => {
                    TweenNull.Add(gameObject, 0.5f).Then(() => {
                        var btnStage = GameObject.Find("StageButton1");
                        var btnStage2 = GameObject.Find("StageButton2");
                        var chara = GameObject.Find("EpisodeCharacter");
                        chara.GetComponent<MeshRenderer>().sortingOrder = 151;
                        if(!btnStage2.GetComponent<BtnStage>().isEnable) {
                            SetFocus(btnStage, 0, 1.5f);
                            SetDescription(3, 120, -245, true, false, 2);
                            btnStage.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
                                pointer.SetActive(false);
                                RemoveFocus(btnStage);
                                chara.GetComponent<MeshRenderer>().sortingOrder = 2;
                                TweenNull.Add(gameObject, 0.5f).Then(() => {
                                    var btnBattle = GameObject.Find("BtnBattle");
                                    SetFocus(btnBattle, -0.81f, 1.6f);
                                    btnBattle.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(___ => {
                                        pointer.SetActive(false);
                                        RemoveFocus(btnBattle);
                                        HideDescription();
                                        TweenNull.Add(gameObject, 0.5f).Then(() => {
                                            var btnStartBattle = GameObject.Find("BtnStartBattle");
                                            SetFocus(btnStartBattle, 0, 0.6f);
                                            SetDescription(4, -50, -220, false, false);
                                            UILoading.Instance.SetQuery(QueryKeys.IsFirstStoryBattle, 1);
                                            Dispose();
                                        });
                                    }).AddTo(disposables);
                                });
                            }).AddTo(disposables);
                        } else {
                            SetFocus(btnStage2, 0, 0.3f);
                            SetDescription(5, 120, -245, true, false);
                            UILoading.Instance.RemoveQuery(QueryKeys.IsFirstStoryBattle);
                            btnStage2.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
                                pointer.SetActive(false);
                                RemoveFocus(btnStage2);
                                chara.GetComponent<MeshRenderer>().sortingOrder = 2;
                                TweenNull.Add(gameObject, 0.5f).Then(() => {
                                    var btnBattle = GameObject.Find("BtnBattle");
                                    SetFocus(btnBattle, -0.81f, 1.6f);
                                    btnBattle.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(___ => {
                                        pointer.SetActive(false);
                                        RemoveFocus(btnBattle);
                                        HideDescription();
                                        TweenNull.Add(gameObject, 0.5f).Then(() => {
                                            var btnStartBattle = GameObject.Find("BtnStartBattle");
                                            SetFocus(btnStartBattle, 0, 0.6f);
                                            Dispose();
                                        });
                                    }).AddTo(disposables);
                                });
                            }).AddTo(disposables);
                        }
                    });
                }).AddTo(disposables);
            } else {
                gameObject.SetActive(false);
                BackButtonController.Instance.IsEnable(true);
            }
        }

        /// <summary>
        /// 1-1-2 ~ EquipGear
        /// </summary>
        private void SetCharacterTutorial2() {
            var equipList = GameObject.Find("EquipList");
            SetDescription(8, -220, -220, false, false);
            for(int i = 0, l = equipList.transform.childCount - 1; i < l; i++) {
                var equip = equipList.transform.GetChild(i).GetComponent<CharacterList.BtnEquip>();
                if(equip.equipState == -2) {
                    SetFocus(equip.gameObject, 0, 0.6f);
                    equip.OnClickedBtn.Subscribe(_ => {
                        var gearStatusPanel = GameObject.Find("GearStatusPanel");
                        var btnEquip = GameObject.Find("BtnEquip").GetComponent<BtnCommon>();
                        HideDescription();
                        RemoveFocus(equip.gameObject);
                        SetFocus(gearStatusPanel, 0, 0);
                        SetFocus(btnEquip.gameObject, 0, 0.5f);
                        gearStatusPanel.GetComponent<Canvas>().sortingOrder = 149;
                        gearStatusPanel.GetComponent<GraphicRaycaster>().enabled = false;
                        btnEquip.OnClickedBtn.Subscribe(__ => {
                            RemoveFocus(btnEquip.gameObject);
                            gearStatusPanel.GetComponent<Canvas>().overrideSorting = false;
                            pointer.SetActive(false);
                            TweenNull.Add(gameObject, 0.2f).Then(() => {
                                var btnCloseCharacterDetail = GameObject.Find("BtnCloseCharacterDetail");
                                SetDescription(9, 80, -210, false, false);
                                SetFocus(btnCloseCharacterDetail, 0.8f, -0.8f, -135);
                            });
                        }).AddTo(disposables);
                    }).AddTo(disposables);
                    break;
                }
            }
        }

        /// <summary>
        /// Tutorial EquipGear ~ 1-1-3
        /// </summary>
        private void StartTutorial3() {
            if(SceneManager.GetActiveScene().name == Constant.SCENE_MAIN) {
                var btnStory = GameObject.Find("BtnStory");
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    SetFocus(btnStory.gameObject, 0, 1.5f);
                });
            } else if(SceneManager.GetActiveScene().name == Constant.SCENE_STORY) {
                var playerInfo = GameObject.Find("PlayerInfo");
                var storyMap = playerInfo.transform.parent.Find("StoryMap");
                gameObject.SetActive(false);
                storyMap.GetComponent<StoryMap>().OnOpenMap.Subscribe(_ => {
                    gameObject.SetActive(true);
                    TweenNull.Add(gameObject, 0.5f).Then(() => {
                        var btnStage3 = GameObject.Find("StageButton3");
                        var chara = GameObject.Find("EpisodeCharacter");
                        var boss = GameObject.Find("EpisodeBoss");
                        var label = GameObject.Find("BossLabel");
                        SetFocus(btnStage3, 0, 1.5f);
                        chara.GetComponent<MeshRenderer>().sortingOrder = 152;
                        boss.GetComponent<MeshRenderer>().sortingOrder = 151;
                        label.GetComponent<Canvas>().sortingOrder = 153;
                        SetDescription(10, -100, -220, true, false);
                        btnStage3.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
                            RemoveFocus(btnStage3);
                            pointer.SetActive(false);
                            HideDescription();
                            chara.GetComponent<MeshRenderer>().sortingOrder = 2;
                            boss.GetComponent<MeshRenderer>().sortingOrder = 1;
                            label.GetComponent<Canvas>().sortingOrder = 3;
                            TweenNull.Add(gameObject, 0.1f).Then(() => {
                                gameObject.SetActive(false);
                                Dispose();
                            });
                        }).AddTo(disposables);
                    });
                }).AddTo(disposables);
            } else {
                gameObject.SetActive(false);
                BackButtonController.Instance.IsEnable(true);
            }
        }

        /// <summary>
        /// Tutorial 1-1-3 ~ ReceiveQuestReward
        /// </summary>
        private void StartTutorial4() {
            if(SceneManager.GetActiveScene().name == Constant.SCENE_STORY) {
                var playerInfo = GameObject.Find("PlayerInfo");
                var storyMap = playerInfo.transform.parent.Find("StoryMap");
                var clearEpisodePanel = storyMap.transform.Find("ClearEpisodePanel");
                gameObject.SetActive(false);
                clearEpisodePanel.GetComponent<ClearEpisodePanel>().OnCloseClearEpisodePanel.Subscribe(_ => {
                    gameObject.SetActive(true);
                    TweenA.Add(overlay, 0.01f, 0);
                    TweenNull.Add(gameObject, 0.8f).Then(() => {
                        TweenA.Add(overlay, 0.01f, 0.4f);
                        contentTextList[0].SetActive(true);
                        openContentPanel.SetActive(true);
                        btnOverlay.OnClickedBtn.Subscribe(__ => {
                            TweenA.Add(openContentPanel, 0.2f, 0).Then(() => {
                                openContentPanel.SetActive(false);
                                SetQuestTutorial();
                                DisposeOverlay();
                            });
                        }).AddTo(overlayDisposables);
                    });
                }).AddTo(disposables);
            } else {
                SetQuestTutorial();
            }
        }
        private void SetQuestTutorial() {
            var btnMenu = GameObject.Find("BtnMenu");
            TweenNull.Add(gameObject, 0.1f).Then(() => {
                SetFocus(btnMenu, -1.2f, -0.6f, 120);
            });
            SetDescription(11, 65, -220, true, false);
            btnMenu.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
                pointer.SetActive(false);
                btnMenu.GetComponent<Canvas>().sortingOrder = 7;
                TweenNull.Add(gameObject, 0.4f).Then(() => {
                    var btnMenu4 = GameObject.Find("Menu4");
                    SetFocus(btnMenu4, -0.5f, -0.2f, 110);
                    btnMenu4.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(___ => {
                        RemoveFocus(btnMenu4);
                        pointer.SetActive(false);
                        HideDescription();
                        btnMenu4.SetActive(false);
                        TweenNull.Add(gameObject, 0.01f).Then(() => {
                            btnMenu4.SetActive(true);
                            SetDescription(12, -100, -200, true, false);
                            btnOverlay.OnClickedBtn.Subscribe(____ => {
                                PlayerPrefs.SetInt(SaveKeys.OpenTutorialQuest, 1);
                                gameObject.SetActive(false);
                                BackButtonController.Instance.IsEnable(true);
                                Dispose();
                            });
                        });
                    }).AddTo(disposables);
                });
            }).AddTo(disposables);
        }

        /// <summary>
        /// 1-2-2 ~ UPPromotionLevel
        /// </summary>
        private void SetCharacterTutorial5() {
            var equipList = GameObject.Find("EquipList");
            var btnRaisePromotion = equipList.transform.Find("BtnRaisePromotionLv");
            var unequipIndex = 0;
            var unequipList = new List<CharacterList.BtnEquip>();
            BtnCommon btnEquip = null;

            SetDescription(15, -220, 220, false, false);
            for(int i = 0, l = equipList.transform.childCount - 1; i < l; i++) {
                var equip = equipList.transform.GetChild(i).GetComponent<CharacterList.BtnEquip>();
                if(equip.equipState == -2) {
                    unequipList.Add(equip);
                    equip.OnClickedBtn.Subscribe(_ => {
                        var gearStatusPanel = GameObject.Find("GearStatusPanel");
                        if(btnEquip == null) {
                            btnEquip = GameObject.Find("BtnEquip").GetComponent<BtnCommon>();
                            btnEquip.OnClickedBtn.Subscribe(__ => {
                                unequipIndex++;
                                RemoveFocus(btnEquip.gameObject);
                                gearStatusPanel.GetComponent<Canvas>().overrideSorting = false;
                                if(unequipIndex < unequipList.Count) {
                                    SetFocus(unequipList[unequipIndex].gameObject, 0, 0.6f);
                                    descriptionContainer.SetActive(true);
                                } else {
                                    btnRaisePromotion.gameObject.SetActive(true);
                                    SetFocus(btnRaisePromotion.gameObject, 0, 0.3f);
                                    SetDescription(16, 220, -240, false, false);
                                }
                            }).AddTo(disposables);
                        }
                        HideDescription();
                        RemoveFocus(equip.gameObject);
                        SetFocus(gearStatusPanel, 0, 0);
                        gearStatusPanel.GetComponent<Canvas>().sortingOrder = 149;
                        gearStatusPanel.GetComponent<GraphicRaycaster>().enabled = false;
                        SetFocus(btnEquip.gameObject, 0, 0.5f);
                    }).AddTo(disposables);
                }
            }
            btnRaisePromotion.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(_ => {
                RemoveFocus(btnRaisePromotion.gameObject);
                HideDescription();
                TweenNull.Add(gameObject, 0.25f).Then(() => {
                    var btnDecidePromotion = GameObject.Find("BtnDecidePromotion");
                    SetFocus(btnDecidePromotion, 0, 0.5f);
                    btnDecidePromotion.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
                        RemoveFocus(btnDecidePromotion);
                        pointer.SetActive(false);
                        TweenA.Add(overlay, 0.001f, 0);
                        TweenNull.Add(gameObject, 1.0f).Then(() => {
                            SetDescription(17, 250, -180, false, false);
                            TweenA.Add(overlay, 0.001f, 0.4f);
                            SetCharacterTutorial6();
                        });
                    }).AddTo(disposables);
                });
            }).AddTo(disposables);

            if(unequipList.Count > 0) {
                SetFocus(unequipList[0].gameObject, 0, 0.6f);
            } else {
                SetFocus(btnRaisePromotion.gameObject, 0, 0.3f);
                SetDescription(16, 220, -240, false, false);
            }
        }

        /// <summary>
        /// UPPromotionLevel ~ UPActionLevel
        /// </summary>
        private void SetCharacterTutorial6() {
            var tabSkill = GameObject.Find("TabSkill");
            SetFocus(tabSkill, -0.4f, 0, 120);
            tabSkill.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(_ => {
                var skillUpContainer = GameObject.Find("SkillUpContainer");
                var btnSkillUp = GameObject.Find("BtnSkillUp");
                var skillUpPanel2 = GameObject.Find("SkillUpPanel2");
                var scroller = GameObject.Find("SkillUpScroller");
                scroller.GetComponent<ScrollRect>().enabled = false;
                RemoveFocus(tabSkill);
                TweenA.Add(overlay, 0.01f, 0.3f);
                SetFocus(skillUpPanel2, -0.2f, 0.4f, 0);
                SetDescription(18, 260, -220, false, false);
                skillUpPanel2.GetComponent<SkillPanel>().OnPanelClick.Subscribe(__ => {
                    RemoveFocus(skillUpPanel2);
                    SetFocus(btnSkillUp, -0.61f, 0.15f);
                    SetDescription(19, 260, -220, false, false);
                    btnSkillUp.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(___ => {
                        RemoveFocus(btnSkillUp);
                        pointer.SetActive(false);
                        scroller.GetComponent<ScrollRect>().enabled = true;
                        skillUpContainer.SetActive(false);
                        skillUpContainer.SetActive(true);
                        TweenNull.Add(gameObject, 0.2f).Then(() => {
                            var btnCloseCharacterDetail = GameObject.Find("BtnCloseCharacterDetail");
                            SetDescription(20, 80, -210, false, false);
                            TweenA.Add(overlay, 0.01f, 0.4f);
                            SetFocus(btnCloseCharacterDetail, 0.8f, -0.8f, -135);
                        });
                    }).AddTo(disposables);
                }).AddTo(disposables);
            }).AddTo(disposables);
        }

        /// <summary>
        /// 1-2-3 ~ LotGacha
        /// </summary>
        private void StartTutorial7() {
            if(SceneManager.GetActiveScene().name == Constant.SCENE_MAIN) {
                var gachaBtn = GameObject.Find("BtnGacha");
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    SetFocus(gachaBtn.gameObject, 0, 1.5f);
                });
                SetDescription(22, -120, -220, true, false);
            } else if(SceneManager.GetActiveScene().name == Constant.SCENE_GACHA) {
                var container = GameObject.Find("GachaPanelContainer");
                var panel = container.transform.Find("TutorialGachaPanel");
                var btnTutorialGacha = panel.transform.Find("BtnTutorialGacha");
                var gachaController = GameObject.Find("GachaController");

                panel.gameObject.SetActive(true);
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    SetFocus(btnTutorialGacha.gameObject, 0, 0.5f);
                    SetDescription(23, 60, 120, true, false);
                });
                btnTutorialGacha.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(_ => {
                    pointer.SetActive(false);
                    gameObject.SetActive(false);
                }).AddTo(disposables);
                gachaController.GetComponent<GachaController>().OnEndGacha.Subscribe(_ => {
                    var resultCamera = GameObject.Find("ResultCamera");
                    gameObject.SetActive(true);
                    HideDescription();
                    TweenNull.Add(gameObject, 7.1f).Then(() => {
                        canvas.worldCamera = resultCamera.GetComponent<Camera>();
                        overlay.SetActive(false);
                        SetDescription(24, -230, 190, false, false);
                    });
                }).AddTo(disposables);
            } else {
                var btnMenu = GameObject.Find("BtnMenu");
                TweenNull.Add(gameObject, 0.1f).Then(() => {
                    SetFocus(btnMenu, -1.2f, -0.6f, 120);
                });
                btnMenu.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
                    pointer.SetActive(false);
                    RemoveFocus(btnMenu);
                    TweenNull.Add(gameObject, 0.4f).Then(() => {
                        var btnMenu3 = GameObject.Find("Menu3");
                        SetFocus(btnMenu3, -0.5f, -0.2f, 110);
                    });
                }).AddTo(disposables);
                SetDescription(21, 120, -220, true, false);
            }
        }

        /// <summary>
        /// 1-2-4 ~ ArenaBattle
        /// </summary>
        private void StartTutorial8() {
            if(SceneManager.GetActiveScene().name == Constant.SCENE_MAIN) {
                var btnArena = GameObject.Find("BtnArena");
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    SetFocus(btnArena.gameObject, 0, 1.5f);
                });
                SetDescription(26, -50, -150, true, false, 2);
            } else if(SceneManager.GetActiveScene().name == Constant.SCENE_ARENA) {
                var step = 0;
                SetDescription(27, 80, -220, true, true);
                iconNextDescription.SetActive(true);
                btnOverlay.OnClickedBtn.Subscribe(_ => {
                    if(step == 0) {
                        SetDescription(28, 80, -220, true, true, 3);
                        iconNextDescription.SetActive(false);
                        step++;
                    } else {
                        gameObject.SetActive(false);
                        BackButtonController.Instance.IsEnable(true);
                        PlayerPrefs.SetInt(SaveKeys.OpenTutorialArena, 1);
                    }
                });
            } else {
                var btnMenu = GameObject.Find("BtnMenu");
                btnMenu.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
                    pointer.SetActive(false);
                    RemoveFocus(btnMenu);
                    TweenNull.Add(gameObject, 0.4f).Then(() => {
                        var btnMenu3 = GameObject.Find("Menu3");
                        SetFocus(btnMenu3, -0.5f, -0.2f, 110);
                    });
                }).AddTo(disposables);
                contentTextList[1].SetActive(true);
                openContentPanel.SetActive(true);
                btnOverlay.OnClickedBtn.Subscribe(_ => {
                    TweenA.Add(openContentPanel, 0.2f, 0).Then(() => {
                        openContentPanel.SetActive(false);
                        SetFocus(btnMenu, -1.2f, -0.6f, 120);
                        SetDescription(25, 120, -220, true, false);
                        DisposeOverlay();
                    });
                }).AddTo(overlayDisposables);
            }
        }

        /// <summary>
        /// 1_3_2
        /// </summary>
        private void StartTutorial10() {
            var btnMenu = GameObject.Find("BtnMenu");
            btnMenu.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
                pointer.SetActive(false);
                btnMenu.GetComponent<Canvas>().sortingOrder = 7;
                TweenNull.Add(gameObject, 0.4f).Then(() => {
                    var btnMenu5 = GameObject.Find("Menu5");
                    SetFocus(btnMenu5, -0.5f, -0.2f, 110);
                    btnMenu5.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(___ => {
                        RemoveFocus(btnMenu5);
                        pointer.SetActive(false);
                        HideDescription();
                        btnMenu5.SetActive(false);
                        PlayerPrefs.SetInt(SaveKeys.OpenTutorialDailyQuest, 1);
                        TweenNull.Add(gameObject, 0.01f).Then(() => {
                            var wrapper = GameObject.Find("DailyQuestWrapper");
                            var panel = wrapper.transform.GetChild(0);
                            btnMenu5.SetActive(true);
                            if(panel != null) {
                                var isClear = panel.transform.Find("IconReceive");
                                if(isClear.gameObject.activeSelf) {
                                    SetFocus(isClear.gameObject, -0.65f, 0.35f);
                                    SetDescription(30, -70, -170, true, false);
                                    panel.GetComponent<QuestPanel>().OnQuestComplete.Subscribe(____ => {
                                        RemoveFocus(isClear.gameObject);
                                        pointer.SetActive(false);
                                        gameObject.SetActive(false);
                                        BackButtonController.Instance.IsEnable(true);
                                        Dispose();
                                    });
                                } else {
                                    SetDescription(31, -100, -200, true, false);
                                    btnOverlay.OnClickedBtn.Subscribe(____ => {
                                        PlayerPrefs.SetInt(SaveKeys.OpenTutorialQuest, 1);
                                        gameObject.SetActive(false);
                                        BackButtonController.Instance.IsEnable(true);
                                    });
                                }
                            } else {
                                gameObject.SetActive(false);
                                BackButtonController.Instance.IsEnable(true);
                            }
                        });
                        Dispose();
                    }).AddTo(disposables);
                });
            }).AddTo(disposables);
            contentTextList[2].SetActive(true);
            contentTextList[3].SetActive(true);
            openContentPanel.SetActive(true);
            btnOverlay.OnClickedBtn.Subscribe(_ => {
                TweenA.Add(openContentPanel, 0.2f, 0).Then(() => {
                    openContentPanel.SetActive(false);
                    SetFocus(btnMenu, -1.2f, -0.6f, 120);
                    SetDescription(29, -10, -220, true, false);
                    DisposeOverlay();
                });
            }).AddTo(overlayDisposables);
        }

        /// <summary>
        /// 1_3_4 ~ ConsumeDrug
        /// </summary>
        private void SetCharacterTutorial11() {
            var btnExpUp = GameObject.Find("BtnExpUp");
            SetFocus(btnExpUp, 0, 0.3f);
            SetDescription(34, -220, -220, false, false);
            btnExpUp.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(_ => {
                TweenNull.Add(gameObject, 0.25f).Then(() => {
                    var expUpPanel = GameObject.Find("ExpUpPanel");
                    var expUpItem = GameObject.Find("ExpUpItem1");
                    RemoveFocus(btnExpUp);
                    SetFocus(expUpItem, 0, 0.5f);
                    expUpItem.GetComponent<CharacterList.ExpUpItem>().OnPanelClick.Subscribe(__ => {
                        RemoveFocus(expUpItem);
                        pointer.SetActive(false);
                        iconNextDescription.SetActive(true);
                        SetDescription(35, -220, -220, false, false);
                        TweenNull.Add(gameObject, 0.2f).Then(() => {
                            expUpPanel.GetComponent<Noroshi.CharacterList.ExpUpPanel>().CloseExpPanel();
                            btnOverlay.OnClickedBtn.Subscribe(___ => {
                                iconNextDescription.SetActive(false);
                                SetDescription(36, -220, -220, false, false);
                                pointer.SetActive(false);
                                TweenNull.Add(gameObject, 0.2f).Then(() => {
                                    var btnCloseCharacterDetail = GameObject.Find("BtnCloseCharacterDetail");
                                    SetFocus(btnCloseCharacterDetail, 0.8f, -0.8f, -135);
                                    DisposeOverlay();
                                });
                            }).AddTo(overlayDisposables);
                        });
                    }).AddTo(disposables);
                });
            }).AddTo(disposables);
        }

        /// <summary>
        /// 1_4_2 ~ AddRentalCharacter
        /// </summary>
        private void StartTutorial12() {
            if(SceneManager.GetActiveScene().name == Constant.SCENE_MAIN) {
                var btnGuild = GameObject.Find("BtnGuild");
                TweenNull.Add(gameObject, 0.2f).Then(() => {
                    SetFocus(btnGuild.gameObject, 0, 1.5f);
                });
                SetDescription(38, 50, -150, true, true);
            } else if(SceneManager.GetActiveScene().name == Constant.SCENE_GUILD) {
//                var btnDispatchMercenary = GameObject.Find("BtnDispatchMercenary");
//                TweenNull.Add(gameObject, 0.2f).Then(() => {
//                    SetFocus(btnDispatchMercenary, 0, 0.5f);
//                });
//                btnDispatchMercenary.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(_ => {
//                    var guildMercenaryPanel = GameObject.Find("GuildContentsContainer").transform.parent.Find("GuildMercenaryPanel");
//                    var btnAddMercenary = guildMercenaryPanel.Find("Panel/OwnMercenaryContainer/AddMercenaryPiece/BtnAddMercenary");
//                    HideDescription();
//                    RemoveFocus(btnDispatchMercenary);
//                    pointer.SetActive(false);
//                    TweenNull.Add(gameObject, 0.5f).Then(() => {
//                        SetFocus(btnAddMercenary.gameObject, 0, 0.5f);
//                        btnAddMercenary.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
//                            RemoveFocus(btnAddMercenary.gameObject);
//                            pointer.SetActive(false);
//                            SetDescription(39, 270, 235, false, false);
//                        }).AddTo(disposables);
//                    });
//                }).AddTo(disposables);
                var step = 0;
                SetDescription(39, -10, -150, true, true);
                iconNextDescription.SetActive(true);
                btnOverlay.OnClickedBtn.Subscribe(_ => {
                    if(step == 0) {
                        SetDescription(40, -10, -150, true, true);
                        iconNextDescription.SetActive(false);
                        step++;
                    } else {
                        gameObject.SetActive(false);
                        BackButtonController.Instance.IsEnable(true);
                        PlayerPrefs.SetInt(SaveKeys.OpenTutorialGuild, 1);
                    }
                });
            } else {
                var btnMenu = GameObject.Find("BtnMenu");
                contentTextList[4].SetActive(true);
                openContentPanel.SetActive(true);
                btnOverlay.OnClickedBtn.Subscribe(__ => {
                    TweenA.Add(openContentPanel, 0.2f, 0).Then(() => {
                        openContentPanel.SetActive(false);
                        TweenNull.Add(gameObject, 0.1f).Then(() => {
                            SetFocus(btnMenu, -1.2f, -0.6f, 120);
                        });
                        SetDescription(37, 120, -220, true, false);
                        DisposeOverlay();
                    });
                }).AddTo(overlayDisposables);

                btnMenu.GetComponent<BtnCommon>().OnClickedBtn.Subscribe(__ => {
                    pointer.SetActive(false);
                    RemoveFocus(btnMenu);
                    TweenNull.Add(gameObject, 0.4f).Then(() => {
                        var btnMenu3 = GameObject.Find("Menu3");
                        SetFocus(btnMenu3, -0.5f, -0.2f, 110);
                    });
                }).AddTo(disposables);
            }
        }

        /// <summary>
        /// 1_5_11
        /// </summary>
        private void StartTutorial13() {
            if(SceneManager.GetActiveScene().name == Constant.SCENE_STORY) {
                var playerInfo = GameObject.Find("PlayerInfo");
                var storyMap = playerInfo.transform.parent.Find("StoryMap");
                var clearEpisodePanel = storyMap.transform.Find("ClearEpisodePanel");
                gameObject.SetActive(false);
                clearEpisodePanel.GetComponent<ClearEpisodePanel>().OnCloseClearEpisodePanel.Subscribe(_ => {
                    gameObject.SetActive(true);
                    TweenA.Add(overlay, 0.01f, 0);
                    TweenNull.Add(gameObject, 0.8f).Then(() => {
                        TweenA.Add(overlay, 0.01f, 0.4f);
                        SetEvolutionTutorial();
                    });
                }).AddTo(disposables);
            } else {
                SetEvolutionTutorial();
            }
        }
        private void SetEvolutionTutorial() {
            var step = 0;
            SetDescription(42, 65, -200, true, false);
            iconNextDescription.SetActive(true);

            btnOverlay.OnClickedBtn.Subscribe(_ => {
                if(step == 0) {
                    step++;
                    SetDescription(43, 65, -200, true, false);
                    iconNextDescription.SetActive(false);
                } else if(step == 1) {
                    step++;
                    var evolutionModal = Instantiate(Resources.Load<EvolutionCharacterModal>("UI/EvolutionCharacterModal"));
                    evolutionModal.OpenModal(206);
                    TweenNull.Add(gameObject, 0.2f).Then(() => {
                        SetDescription(44, 65, -200, true, false);
                        iconNextDescription.SetActive(true);
                    });
                } else if(step == 2) {
                    step++;
                    SetDescription(45, 65, -200, true, false);
                    iconNextDescription.SetActive(false);
                } else if(step == 3) {
                    PlayerPrefs.SetInt(SaveKeys.OpenTutorialEvolution, 1);
                    gameObject.SetActive(false);
                    BackButtonController.Instance.IsEnable(true);
                    DisposeOverlay();
                }
            }).AddTo(overlayDisposables);
        }

        /// <summary>
        /// 1_6_19
        /// </summary>
        private void StartTutorial14() {
            if(SceneManager.GetActiveScene().name == Constant.SCENE_STORY) {
                var playerInfo = GameObject.Find("PlayerInfo");
                var storyMap = playerInfo.transform.parent.Find("StoryMap");
                var clearEpisodePanel = storyMap.transform.Find("ClearEpisodePanel");
                gameObject.SetActive(false);
                clearEpisodePanel.GetComponent<ClearEpisodePanel>().OnCloseClearEpisodePanel.Subscribe(_ => {
                    gameObject.SetActive(true);
                    TweenNull.Add(gameObject, 0.2f).Then(() => {
                        contentTextList[5].SetActive(true);
                        openContentPanel.SetActive(true);
                        btnOverlay.OnClickedBtn.Subscribe(__ => {
                            TweenA.Add(openContentPanel, 0.2f, 0).Then(() => {
                                openContentPanel.SetActive(false);
                                gameObject.SetActive(false);
                                BackButtonController.Instance.IsEnable(true);
                                PlayerPrefs.SetInt(SaveKeys.OpenBackStory, 1);
                                DisposeOverlay();
                            });
                        }).AddTo(overlayDisposables);
                    });
                }).AddTo(disposables);
            } else {
                gameObject.SetActive(false);
                BackButtonController.Instance.IsEnable(true);
            }
        }


        private void Dispose() {
            disposables.Dispose();
        }

        private void DisposePanel() {
            panelDisposables.Dispose();
        }

        private void DisposeOverlay() {
            overlayDisposables.Dispose();
        }

        public void Init(TutorialStep step) {
            canvas.worldCamera = Camera.main;
            BackButtonController.Instance.IsEnable(false);

            if(step == TutorialStep.Start) {
                StartTutorial1();
            } else if(step < TutorialStep.EquipGear) {
                StartCharacterTutorial(2, 105);
            } else if(step < TutorialStep.ClearStoryStage_1_1_3) {
                StartTutorial3();
            } else if(step == TutorialStep.ClearStoryStage_1_1_3) {
                StartTutorial4();
            } else if(step < TutorialStep.UPPromotionLevel) {
                StartCharacterTutorial(5, 101);
            } else if(step < TutorialStep.UPActionLevel) {
                StartCharacterTutorial(6, 101);
            } else if(step < TutorialStep.LotGacha) {
                StartTutorial7();
            } else if(step < TutorialStep.ExecuteArenaBattle) {
                StartTutorial8();
            } else if(step == TutorialStep.ClearStoryStage_1_3_2) {
                StartTutorial10();
            } else if(step < TutorialStep.ConsumeDrug) {
                StartCharacterTutorial(11, 303);
            } else if(step == TutorialStep.ClearStoryStage_1_4_2) {
                StartTutorial12();
            } else if(step == TutorialStep.ClearStoryStage_1_5_11) {
                StartTutorial13();
            } else if(step == TutorialStep.ClearStoryStage_1_6_19) {
                StartTutorial14();
            }
        }
    }
}
