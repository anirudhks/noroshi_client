﻿//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Noroshi.MessagePackSerializers {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("MsgPack.Serialization.CodeDomSerializers.CodeDomSerializerBuilder", "0.6.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public class Noroshi_Core_WebApi_Response_Battle_BattleCharacterSerializer : MsgPack.Serialization.MessagePackSerializer<Noroshi.Core.WebApi.Response.Battle.BattleCharacter> {
        
        private MsgPack.Serialization.MessagePackSerializer<ushort> _serializer0;
        
        private MsgPack.Serialization.MessagePackSerializer<uint> _serializer1;
        
        private MsgPack.Serialization.MessagePackSerializer<System.Nullable<float>> _serializer2;
        
        private MsgPack.Serialization.MessagePackSerializer<Noroshi.Core.WebApi.Response.Character.EquippedGear[]> _serializer3;
        
        private MsgPack.Serialization.MessagePackSerializer<byte> _serializer4;
        
        private MsgPack.Serialization.MessagePackSerializer<System.Nullable<uint>> _serializer5;
        
        private MsgPack.Serialization.MessagePackSerializer<System.Nullable<ushort>> _serializer6;
        
        private MsgPack.Serialization.MessagePackSerializer<bool> _serializer7;
        
        public Noroshi_Core_WebApi_Response_Battle_BattleCharacterSerializer(MsgPack.Serialization.SerializationContext context) : 
                base(context) {
            MsgPack.Serialization.PolymorphismSchema schema0 = default(MsgPack.Serialization.PolymorphismSchema);
            schema0 = null;
            this._serializer0 = context.GetSerializer<ushort>(schema0);
            MsgPack.Serialization.PolymorphismSchema schema1 = default(MsgPack.Serialization.PolymorphismSchema);
            schema1 = null;
            this._serializer1 = context.GetSerializer<uint>(schema1);
            MsgPack.Serialization.PolymorphismSchema schema2 = default(MsgPack.Serialization.PolymorphismSchema);
            schema2 = null;
            this._serializer2 = context.GetSerializer<System.Nullable<float>>(schema2);
            MsgPack.Serialization.PolymorphismSchema schema3 = default(MsgPack.Serialization.PolymorphismSchema);
            schema3 = null;
            this._serializer3 = context.GetSerializer<Noroshi.Core.WebApi.Response.Character.EquippedGear[]>(schema3);
            MsgPack.Serialization.PolymorphismSchema schema4 = default(MsgPack.Serialization.PolymorphismSchema);
            schema4 = null;
            this._serializer4 = context.GetSerializer<byte>(schema4);
            MsgPack.Serialization.PolymorphismSchema schema5 = default(MsgPack.Serialization.PolymorphismSchema);
            schema5 = null;
            this._serializer5 = context.GetSerializer<System.Nullable<uint>>(schema5);
            MsgPack.Serialization.PolymorphismSchema schema6 = default(MsgPack.Serialization.PolymorphismSchema);
            schema6 = null;
            this._serializer6 = context.GetSerializer<System.Nullable<ushort>>(schema6);
            MsgPack.Serialization.PolymorphismSchema schema7 = default(MsgPack.Serialization.PolymorphismSchema);
            schema7 = null;
            this._serializer7 = context.GetSerializer<bool>(schema7);
        }
        
        protected override void PackToCore(MsgPack.Packer packer, Noroshi.Core.WebApi.Response.Battle.BattleCharacter objectTree) {
            packer.PackArrayHeader(17);
            this._serializer0.PackTo(packer, objectTree.ActionLevel1);
            this._serializer0.PackTo(packer, objectTree.ActionLevel2);
            this._serializer0.PackTo(packer, objectTree.ActionLevel3);
            this._serializer0.PackTo(packer, objectTree.ActionLevel4);
            this._serializer0.PackTo(packer, objectTree.ActionLevel5);
            this._serializer1.PackTo(packer, objectTree.CharacterID);
            this._serializer2.PackTo(packer, objectTree.DamageCoefficient);
            this._serializer3.PackTo(packer, objectTree.EquippedGears);
            this._serializer4.PackTo(packer, objectTree.EvolutionLevel);
            this._serializer5.PackTo(packer, objectTree.FixedMaxHP);
            this._serializer1.PackTo(packer, objectTree.ID);
            this._serializer6.PackTo(packer, objectTree.InitialEnergy);
            this._serializer5.PackTo(packer, objectTree.InitialHP);
            this._serializer7.PackTo(packer, objectTree.IsBoss);
            this._serializer0.PackTo(packer, objectTree.Level);
            this._serializer4.PackTo(packer, objectTree.PromotionLevel);
            this._serializer4.PackTo(packer, objectTree.Type);
        }
        
        protected override Noroshi.Core.WebApi.Response.Battle.BattleCharacter UnpackFromCore(MsgPack.Unpacker unpacker) {
            Noroshi.Core.WebApi.Response.Battle.BattleCharacter result = default(Noroshi.Core.WebApi.Response.Battle.BattleCharacter);
            result = new Noroshi.Core.WebApi.Response.Battle.BattleCharacter();
            if (unpacker.IsArrayHeader) {
                int unpacked = default(int);
                int itemsCount = default(int);
                itemsCount = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                System.Nullable<ushort> nullable = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 ActionLevel1");
                }
                if (nullable.HasValue) {
                    result.ActionLevel1 = nullable.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable0 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable0 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 ActionLevel2");
                }
                if (nullable0.HasValue) {
                    result.ActionLevel2 = nullable0.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable1 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable1 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 ActionLevel3");
                }
                if (nullable1.HasValue) {
                    result.ActionLevel3 = nullable1.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable2 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable2 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 ActionLevel4");
                }
                if (nullable2.HasValue) {
                    result.ActionLevel4 = nullable2.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable3 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable3 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 ActionLevel5");
                }
                if (nullable3.HasValue) {
                    result.ActionLevel5 = nullable3.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<uint> nullable4 = default(System.Nullable<uint>);
                if ((unpacked < itemsCount)) {
                    nullable4 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt32 CharacterID");
                }
                if (nullable4.HasValue) {
                    result.CharacterID = nullable4.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<float> nullable5 = default(System.Nullable<float>);
                if ((unpacked < itemsCount)) {
                    nullable5 = MsgPack.Serialization.UnpackHelpers.UnpackNullableSingleValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "System.Nullable`1[System.Single] DamageCoefficient");
                }
                if (nullable5.HasValue) {
                    result.DamageCoefficient = nullable5;
                }
                unpacked = (unpacked + 1);
                Noroshi.Core.WebApi.Response.Character.EquippedGear[] nullable6 = default(Noroshi.Core.WebApi.Response.Character.EquippedGear[]);
                if ((unpacked < itemsCount)) {
                    if ((unpacker.Read() == false)) {
                        throw MsgPack.Serialization.SerializationExceptions.NewMissingItem(7);
                    }
                    if (((unpacker.IsArrayHeader == false) 
                                && (unpacker.IsMapHeader == false))) {
                        nullable6 = this._serializer3.UnpackFrom(unpacker);
                    }
                    else {
                        MsgPack.Unpacker disposable = default(MsgPack.Unpacker);
                        disposable = unpacker.ReadSubtree();
                        try {
                            nullable6 = this._serializer3.UnpackFrom(disposable);
                        }
                        finally {
                            if (((disposable == null) 
                                        == false)) {
                                disposable.Dispose();
                            }
                        }
                    }
                }
                if (((nullable6 == null) 
                            == false)) {
                    result.EquippedGears = nullable6;
                }
                unpacked = (unpacked + 1);
                System.Nullable<byte> nullable7 = default(System.Nullable<byte>);
                if ((unpacked < itemsCount)) {
                    nullable7 = MsgPack.Serialization.UnpackHelpers.UnpackNullableByteValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "Byte EvolutionLevel");
                }
                if (nullable7.HasValue) {
                    result.EvolutionLevel = nullable7.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<uint> nullable8 = default(System.Nullable<uint>);
                if ((unpacked < itemsCount)) {
                    nullable8 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "System.Nullable`1[System.UInt32] FixedMaxHP");
                }
                if (nullable8.HasValue) {
                    result.FixedMaxHP = nullable8;
                }
                unpacked = (unpacked + 1);
                System.Nullable<uint> nullable9 = default(System.Nullable<uint>);
                if ((unpacked < itemsCount)) {
                    nullable9 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt32 ID");
                }
                if (nullable9.HasValue) {
                    result.ID = nullable9.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable10 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable10 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "System.Nullable`1[System.UInt16] InitialEnergy");
                }
                if (nullable10.HasValue) {
                    result.InitialEnergy = nullable10;
                }
                unpacked = (unpacked + 1);
                System.Nullable<uint> nullable11 = default(System.Nullable<uint>);
                if ((unpacked < itemsCount)) {
                    nullable11 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "System.Nullable`1[System.UInt32] InitialHP");
                }
                if (nullable11.HasValue) {
                    result.InitialHP = nullable11;
                }
                unpacked = (unpacked + 1);
                System.Nullable<bool> nullable12 = default(System.Nullable<bool>);
                if ((unpacked < itemsCount)) {
                    nullable12 = MsgPack.Serialization.UnpackHelpers.UnpackNullableBooleanValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "Boolean IsBoss");
                }
                if (nullable12.HasValue) {
                    result.IsBoss = nullable12.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable13 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable13 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 Level");
                }
                if (nullable13.HasValue) {
                    result.Level = nullable13.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<byte> nullable14 = default(System.Nullable<byte>);
                if ((unpacked < itemsCount)) {
                    nullable14 = MsgPack.Serialization.UnpackHelpers.UnpackNullableByteValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "Byte PromotionLevel");
                }
                if (nullable14.HasValue) {
                    result.PromotionLevel = nullable14.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<byte> nullable15 = default(System.Nullable<byte>);
                if ((unpacked < itemsCount)) {
                    nullable15 = MsgPack.Serialization.UnpackHelpers.UnpackNullableByteValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "Byte Type");
                }
                if (nullable15.HasValue) {
                    result.Type = nullable15.Value;
                }
                unpacked = (unpacked + 1);
            }
            else {
                int itemsCount0 = default(int);
                itemsCount0 = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                for (int i = 0; (i < itemsCount0); i = (i + 1)) {
                    string key = default(string);
                    string nullable16 = default(string);
                    nullable16 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "MemberName");
                    if (((nullable16 == null) 
                                == false)) {
                        key = nullable16;
                    }
                    else {
                        throw MsgPack.Serialization.SerializationExceptions.NewNullIsProhibited("MemberName");
                    }
                    if ((key == "Type")) {
                        System.Nullable<byte> nullable33 = default(System.Nullable<byte>);
                        nullable33 = MsgPack.Serialization.UnpackHelpers.UnpackNullableByteValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "Byte Type");
                        if (nullable33.HasValue) {
                            result.Type = nullable33.Value;
                        }
                    }
                    else {
                        if ((key == "PromotionLevel")) {
                            System.Nullable<byte> nullable32 = default(System.Nullable<byte>);
                            nullable32 = MsgPack.Serialization.UnpackHelpers.UnpackNullableByteValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "Byte PromotionLevel");
                            if (nullable32.HasValue) {
                                result.PromotionLevel = nullable32.Value;
                            }
                        }
                        else {
                            if ((key == "Level")) {
                                System.Nullable<ushort> nullable31 = default(System.Nullable<ushort>);
                                nullable31 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 Level");
                                if (nullable31.HasValue) {
                                    result.Level = nullable31.Value;
                                }
                            }
                            else {
                                if ((key == "IsBoss")) {
                                    System.Nullable<bool> nullable30 = default(System.Nullable<bool>);
                                    nullable30 = MsgPack.Serialization.UnpackHelpers.UnpackNullableBooleanValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "Boolean IsBoss");
                                    if (nullable30.HasValue) {
                                        result.IsBoss = nullable30.Value;
                                    }
                                }
                                else {
                                    if ((key == "InitialHP")) {
                                        System.Nullable<uint> nullable29 = default(System.Nullable<uint>);
                                        nullable29 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "System.Nullable`1[System.UInt32] InitialHP");
                                        if (nullable29.HasValue) {
                                            result.InitialHP = nullable29;
                                        }
                                    }
                                    else {
                                        if ((key == "InitialEnergy")) {
                                            System.Nullable<ushort> nullable28 = default(System.Nullable<ushort>);
                                            nullable28 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "System.Nullable`1[System.UInt16] InitialEnergy");
                                            if (nullable28.HasValue) {
                                                result.InitialEnergy = nullable28;
                                            }
                                        }
                                        else {
                                            if ((key == "ID")) {
                                                System.Nullable<uint> nullable27 = default(System.Nullable<uint>);
                                                nullable27 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt32 ID");
                                                if (nullable27.HasValue) {
                                                    result.ID = nullable27.Value;
                                                }
                                            }
                                            else {
                                                if ((key == "FixedMaxHP")) {
                                                    System.Nullable<uint> nullable26 = default(System.Nullable<uint>);
                                                    nullable26 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "System.Nullable`1[System.UInt32] FixedMaxHP");
                                                    if (nullable26.HasValue) {
                                                        result.FixedMaxHP = nullable26;
                                                    }
                                                }
                                                else {
                                                    if ((key == "EvolutionLevel")) {
                                                        System.Nullable<byte> nullable25 = default(System.Nullable<byte>);
                                                        nullable25 = MsgPack.Serialization.UnpackHelpers.UnpackNullableByteValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "Byte EvolutionLevel");
                                                        if (nullable25.HasValue) {
                                                            result.EvolutionLevel = nullable25.Value;
                                                        }
                                                    }
                                                    else {
                                                        if ((key == "EquippedGears")) {
                                                            Noroshi.Core.WebApi.Response.Character.EquippedGear[] nullable24 = default(Noroshi.Core.WebApi.Response.Character.EquippedGear[]);
                                                            if ((unpacker.Read() == false)) {
                                                                throw MsgPack.Serialization.SerializationExceptions.NewMissingItem(i);
                                                            }
                                                            if (((unpacker.IsArrayHeader == false) 
                                                                        && (unpacker.IsMapHeader == false))) {
                                                                nullable24 = this._serializer3.UnpackFrom(unpacker);
                                                            }
                                                            else {
                                                                MsgPack.Unpacker disposable0 = default(MsgPack.Unpacker);
                                                                disposable0 = unpacker.ReadSubtree();
                                                                try {
                                                                    nullable24 = this._serializer3.UnpackFrom(disposable0);
                                                                }
                                                                finally {
                                                                    if (((disposable0 == null) 
                                                                                == false)) {
                                                                        disposable0.Dispose();
                                                                    }
                                                                }
                                                            }
                                                            if (((nullable24 == null) 
                                                                        == false)) {
                                                                result.EquippedGears = nullable24;
                                                            }
                                                        }
                                                        else {
                                                            if ((key == "DamageCoefficient")) {
                                                                System.Nullable<float> nullable23 = default(System.Nullable<float>);
                                                                nullable23 = MsgPack.Serialization.UnpackHelpers.UnpackNullableSingleValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "System.Nullable`1[System.Single] DamageCoefficient");
                                                                if (nullable23.HasValue) {
                                                                    result.DamageCoefficient = nullable23;
                                                                }
                                                            }
                                                            else {
                                                                if ((key == "CharacterID")) {
                                                                    System.Nullable<uint> nullable22 = default(System.Nullable<uint>);
                                                                    nullable22 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt32 CharacterID");
                                                                    if (nullable22.HasValue) {
                                                                        result.CharacterID = nullable22.Value;
                                                                    }
                                                                }
                                                                else {
                                                                    if ((key == "ActionLevel5")) {
                                                                        System.Nullable<ushort> nullable21 = default(System.Nullable<ushort>);
                                                                        nullable21 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 ActionLevel5");
                                                                        if (nullable21.HasValue) {
                                                                            result.ActionLevel5 = nullable21.Value;
                                                                        }
                                                                    }
                                                                    else {
                                                                        if ((key == "ActionLevel4")) {
                                                                            System.Nullable<ushort> nullable20 = default(System.Nullable<ushort>);
                                                                            nullable20 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 ActionLevel4");
                                                                            if (nullable20.HasValue) {
                                                                                result.ActionLevel4 = nullable20.Value;
                                                                            }
                                                                        }
                                                                        else {
                                                                            if ((key == "ActionLevel3")) {
                                                                                System.Nullable<ushort> nullable19 = default(System.Nullable<ushort>);
                                                                                nullable19 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 ActionLevel3");
                                                                                if (nullable19.HasValue) {
                                                                                    result.ActionLevel3 = nullable19.Value;
                                                                                }
                                                                            }
                                                                            else {
                                                                                if ((key == "ActionLevel2")) {
                                                                                    System.Nullable<ushort> nullable18 = default(System.Nullable<ushort>);
                                                                                    nullable18 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 ActionLevel2");
                                                                                    if (nullable18.HasValue) {
                                                                                        result.ActionLevel2 = nullable18.Value;
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    if ((key == "ActionLevel1")) {
                                                                                        System.Nullable<ushort> nullable17 = default(System.Nullable<ushort>);
                                                                                        nullable17 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Battle.BattleCharacter), "UInt16 ActionLevel1");
                                                                                        if (nullable17.HasValue) {
                                                                                            result.ActionLevel1 = nullable17.Value;
                                                                                        }
                                                                                    }
                                                                                    else {
                                                                                        unpacker.Skip();
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }
        
        private static T @__Conditional<T>(bool condition, T whenTrue, T whenFalse)
         {
            if (condition) {
                return whenTrue;
            }
            else {
                return whenFalse;
            }
        }
    }
}
