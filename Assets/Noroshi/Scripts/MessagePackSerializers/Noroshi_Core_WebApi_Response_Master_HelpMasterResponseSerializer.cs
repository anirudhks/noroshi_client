﻿//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Noroshi.MessagePackSerializers {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("MsgPack.Serialization.CodeDomSerializers.CodeDomSerializerBuilder", "0.6.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public class Noroshi_Core_WebApi_Response_Master_HelpMasterResponseSerializer : MsgPack.Serialization.MessagePackSerializer<Noroshi.Core.WebApi.Response.Master.HelpMasterResponse> {
        
        private MsgPack.Serialization.MessagePackSerializer<Noroshi.Core.WebApi.Response.Master.HelpCategory[]> _serializer0;
        
        private MsgPack.Serialization.MessagePackSerializer<string> _serializer1;
        
        private MsgPack.Serialization.MessagePackSerializer<Noroshi.Core.WebApi.Response.Master.Tip[]> _serializer2;
        
        public Noroshi_Core_WebApi_Response_Master_HelpMasterResponseSerializer(MsgPack.Serialization.SerializationContext context) : 
                base(context) {
            MsgPack.Serialization.PolymorphismSchema schema0 = default(MsgPack.Serialization.PolymorphismSchema);
            schema0 = null;
            this._serializer0 = context.GetSerializer<Noroshi.Core.WebApi.Response.Master.HelpCategory[]>(schema0);
            MsgPack.Serialization.PolymorphismSchema schema1 = default(MsgPack.Serialization.PolymorphismSchema);
            schema1 = null;
            this._serializer1 = context.GetSerializer<string>(schema1);
            MsgPack.Serialization.PolymorphismSchema schema2 = default(MsgPack.Serialization.PolymorphismSchema);
            schema2 = null;
            this._serializer2 = context.GetSerializer<Noroshi.Core.WebApi.Response.Master.Tip[]>(schema2);
        }
        
        protected override void PackToCore(MsgPack.Packer packer, Noroshi.Core.WebApi.Response.Master.HelpMasterResponse objectTree) {
            packer.PackArrayHeader(3);
            this._serializer0.PackTo(packer, objectTree.HelpCategories);
            this._serializer1.PackTo(packer, objectTree.MasterVersion);
            this._serializer2.PackTo(packer, objectTree.Tips);
        }
        
        protected override Noroshi.Core.WebApi.Response.Master.HelpMasterResponse UnpackFromCore(MsgPack.Unpacker unpacker) {
            Noroshi.Core.WebApi.Response.Master.HelpMasterResponse result = default(Noroshi.Core.WebApi.Response.Master.HelpMasterResponse);
            result = new Noroshi.Core.WebApi.Response.Master.HelpMasterResponse();
            if (unpacker.IsArrayHeader) {
                int unpacked = default(int);
                int itemsCount = default(int);
                itemsCount = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                Noroshi.Core.WebApi.Response.Master.HelpCategory[] nullable = default(Noroshi.Core.WebApi.Response.Master.HelpCategory[]);
                if ((unpacked < itemsCount)) {
                    if ((unpacker.Read() == false)) {
                        throw MsgPack.Serialization.SerializationExceptions.NewMissingItem(0);
                    }
                    if (((unpacker.IsArrayHeader == false) 
                                && (unpacker.IsMapHeader == false))) {
                        nullable = this._serializer0.UnpackFrom(unpacker);
                    }
                    else {
                        MsgPack.Unpacker disposable = default(MsgPack.Unpacker);
                        disposable = unpacker.ReadSubtree();
                        try {
                            nullable = this._serializer0.UnpackFrom(disposable);
                        }
                        finally {
                            if (((disposable == null) 
                                        == false)) {
                                disposable.Dispose();
                            }
                        }
                    }
                }
                if (((nullable == null) 
                            == false)) {
                    result.HelpCategories = nullable;
                }
                unpacked = (unpacked + 1);
                string nullable0 = default(string);
                if ((unpacked < itemsCount)) {
                    nullable0 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.HelpMasterResponse), "System.String MasterVersion");
                }
                if (((nullable0 == null) 
                            == false)) {
                    result.MasterVersion = nullable0;
                }
                unpacked = (unpacked + 1);
                Noroshi.Core.WebApi.Response.Master.Tip[] nullable1 = default(Noroshi.Core.WebApi.Response.Master.Tip[]);
                if ((unpacked < itemsCount)) {
                    if ((unpacker.Read() == false)) {
                        throw MsgPack.Serialization.SerializationExceptions.NewMissingItem(2);
                    }
                    if (((unpacker.IsArrayHeader == false) 
                                && (unpacker.IsMapHeader == false))) {
                        nullable1 = this._serializer2.UnpackFrom(unpacker);
                    }
                    else {
                        MsgPack.Unpacker disposable0 = default(MsgPack.Unpacker);
                        disposable0 = unpacker.ReadSubtree();
                        try {
                            nullable1 = this._serializer2.UnpackFrom(disposable0);
                        }
                        finally {
                            if (((disposable0 == null) 
                                        == false)) {
                                disposable0.Dispose();
                            }
                        }
                    }
                }
                if (((nullable1 == null) 
                            == false)) {
                    result.Tips = nullable1;
                }
                unpacked = (unpacked + 1);
            }
            else {
                int itemsCount0 = default(int);
                itemsCount0 = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                for (int i = 0; (i < itemsCount0); i = (i + 1)) {
                    string key = default(string);
                    string nullable2 = default(string);
                    nullable2 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.HelpMasterResponse), "MemberName");
                    if (((nullable2 == null) 
                                == false)) {
                        key = nullable2;
                    }
                    else {
                        throw MsgPack.Serialization.SerializationExceptions.NewNullIsProhibited("MemberName");
                    }
                    if ((key == "Tips")) {
                        Noroshi.Core.WebApi.Response.Master.Tip[] nullable5 = default(Noroshi.Core.WebApi.Response.Master.Tip[]);
                        if ((unpacker.Read() == false)) {
                            throw MsgPack.Serialization.SerializationExceptions.NewMissingItem(i);
                        }
                        if (((unpacker.IsArrayHeader == false) 
                                    && (unpacker.IsMapHeader == false))) {
                            nullable5 = this._serializer2.UnpackFrom(unpacker);
                        }
                        else {
                            MsgPack.Unpacker disposable2 = default(MsgPack.Unpacker);
                            disposable2 = unpacker.ReadSubtree();
                            try {
                                nullable5 = this._serializer2.UnpackFrom(disposable2);
                            }
                            finally {
                                if (((disposable2 == null) 
                                            == false)) {
                                    disposable2.Dispose();
                                }
                            }
                        }
                        if (((nullable5 == null) 
                                    == false)) {
                            result.Tips = nullable5;
                        }
                    }
                    else {
                        if ((key == "MasterVersion")) {
                            string nullable4 = default(string);
                            nullable4 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.HelpMasterResponse), "System.String MasterVersion");
                            if (((nullable4 == null) 
                                        == false)) {
                                result.MasterVersion = nullable4;
                            }
                        }
                        else {
                            if ((key == "HelpCategories")) {
                                Noroshi.Core.WebApi.Response.Master.HelpCategory[] nullable3 = default(Noroshi.Core.WebApi.Response.Master.HelpCategory[]);
                                if ((unpacker.Read() == false)) {
                                    throw MsgPack.Serialization.SerializationExceptions.NewMissingItem(i);
                                }
                                if (((unpacker.IsArrayHeader == false) 
                                            && (unpacker.IsMapHeader == false))) {
                                    nullable3 = this._serializer0.UnpackFrom(unpacker);
                                }
                                else {
                                    MsgPack.Unpacker disposable1 = default(MsgPack.Unpacker);
                                    disposable1 = unpacker.ReadSubtree();
                                    try {
                                        nullable3 = this._serializer0.UnpackFrom(disposable1);
                                    }
                                    finally {
                                        if (((disposable1 == null) 
                                                    == false)) {
                                            disposable1.Dispose();
                                        }
                                    }
                                }
                                if (((nullable3 == null) 
                                            == false)) {
                                    result.HelpCategories = nullable3;
                                }
                            }
                            else {
                                unpacker.Skip();
                            }
                        }
                    }
                }
            }
            return result;
        }
        
        private static T @__Conditional<T>(bool condition, T whenTrue, T whenFalse)
         {
            if (condition) {
                return whenTrue;
            }
            else {
                return whenFalse;
            }
        }
    }
}
