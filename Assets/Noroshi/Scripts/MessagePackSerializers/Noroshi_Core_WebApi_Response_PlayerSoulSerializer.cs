﻿//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Noroshi.MessagePackSerializers {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("MsgPack.Serialization.CodeDomSerializers.CodeDomSerializerBuilder", "0.6.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public class Noroshi_Core_WebApi_Response_PlayerSoulSerializer : MsgPack.Serialization.MessagePackSerializer<Noroshi.Core.WebApi.Response.PlayerSoul> {
        
        private MsgPack.Serialization.MessagePackSerializer<uint> _serializer0;
        
        public Noroshi_Core_WebApi_Response_PlayerSoulSerializer(MsgPack.Serialization.SerializationContext context) : 
                base(context) {
            MsgPack.Serialization.PolymorphismSchema schema0 = default(MsgPack.Serialization.PolymorphismSchema);
            schema0 = null;
            this._serializer0 = context.GetSerializer<uint>(schema0);
        }
        
        protected override void PackToCore(MsgPack.Packer packer, Noroshi.Core.WebApi.Response.PlayerSoul objectTree) {
            packer.PackArrayHeader(2);
            this._serializer0.PackTo(packer, objectTree.PossessionsCount);
            this._serializer0.PackTo(packer, objectTree.SoulID);
        }
        
        protected override Noroshi.Core.WebApi.Response.PlayerSoul UnpackFromCore(MsgPack.Unpacker unpacker) {
            Noroshi.Core.WebApi.Response.PlayerSoul result = default(Noroshi.Core.WebApi.Response.PlayerSoul);
            result = new Noroshi.Core.WebApi.Response.PlayerSoul();
            if (unpacker.IsArrayHeader) {
                int unpacked = default(int);
                int itemsCount = default(int);
                itemsCount = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                System.Nullable<uint> nullable = default(System.Nullable<uint>);
                if ((unpacked < itemsCount)) {
                    nullable = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.PlayerSoul), "UInt32 PossessionsCount");
                }
                if (nullable.HasValue) {
                    result.PossessionsCount = nullable.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<uint> nullable0 = default(System.Nullable<uint>);
                if ((unpacked < itemsCount)) {
                    nullable0 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.PlayerSoul), "UInt32 SoulID");
                }
                if (nullable0.HasValue) {
                    result.SoulID = nullable0.Value;
                }
                unpacked = (unpacked + 1);
            }
            else {
                int itemsCount0 = default(int);
                itemsCount0 = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                for (int i = 0; (i < itemsCount0); i = (i + 1)) {
                    string key = default(string);
                    string nullable1 = default(string);
                    nullable1 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.PlayerSoul), "MemberName");
                    if (((nullable1 == null) 
                                == false)) {
                        key = nullable1;
                    }
                    else {
                        throw MsgPack.Serialization.SerializationExceptions.NewNullIsProhibited("MemberName");
                    }
                    if ((key == "SoulID")) {
                        System.Nullable<uint> nullable3 = default(System.Nullable<uint>);
                        nullable3 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.PlayerSoul), "UInt32 SoulID");
                        if (nullable3.HasValue) {
                            result.SoulID = nullable3.Value;
                        }
                    }
                    else {
                        if ((key == "PossessionsCount")) {
                            System.Nullable<uint> nullable2 = default(System.Nullable<uint>);
                            nullable2 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.PlayerSoul), "UInt32 PossessionsCount");
                            if (nullable2.HasValue) {
                                result.PossessionsCount = nullable2.Value;
                            }
                        }
                        else {
                            unpacker.Skip();
                        }
                    }
                }
            }
            return result;
        }
        
        private static T @__Conditional<T>(bool condition, T whenTrue, T whenFalse)
         {
            if (condition) {
                return whenTrue;
            }
            else {
                return whenFalse;
            }
        }
    }
}
