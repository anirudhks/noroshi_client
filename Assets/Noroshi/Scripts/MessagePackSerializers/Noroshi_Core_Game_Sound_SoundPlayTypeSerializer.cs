﻿//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Noroshi.MessagePackSerializers {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("MsgPack.Serialization.CodeDomSerializers.CodeDomSerializerBuilder", "0.6.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public class Noroshi_Core_Game_Sound_SoundPlayTypeSerializer : MsgPack.Serialization.EnumMessagePackSerializer<Noroshi.Core.Game.Sound.SoundPlayType> {
        
        public Noroshi_Core_Game_Sound_SoundPlayTypeSerializer(MsgPack.Serialization.SerializationContext context) : 
                this(context, MsgPack.Serialization.EnumSerializationMethod.ByName) {
        }
        
        public Noroshi_Core_Game_Sound_SoundPlayTypeSerializer(MsgPack.Serialization.SerializationContext context, MsgPack.Serialization.EnumSerializationMethod enumSerializationMethod) : 
                base(context, enumSerializationMethod) {
        }
        
        protected override void PackUnderlyingValueTo(MsgPack.Packer packer, Noroshi.Core.Game.Sound.SoundPlayType enumValue) {
            packer.Pack(((int)(enumValue)));
        }
        
        protected override Noroshi.Core.Game.Sound.SoundPlayType UnpackFromUnderlyingValue(MsgPack.MessagePackObject messagePackObject) {
            return ((Noroshi.Core.Game.Sound.SoundPlayType)(messagePackObject.AsInt32()));
        }
        
        private static T @__Conditional<T>(bool condition, T whenTrue, T whenFalse)
         {
            if (condition) {
                return whenTrue;
            }
            else {
                return whenFalse;
            }
        }
    }
}
