﻿//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Noroshi.MessagePackSerializers {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("MsgPack.Serialization.CodeDomSerializers.CodeDomSerializerBuilder", "0.6.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public class Noroshi_Core_WebApi_Response_Master_ShadowCharacterSerializer : MsgPack.Serialization.MessagePackSerializer<Noroshi.Core.WebApi.Response.Master.ShadowCharacter> {
        
        private MsgPack.Serialization.MessagePackSerializer<ushort> _serializer0;
        
        private MsgPack.Serialization.MessagePackSerializer<uint> _serializer1;
        
        private MsgPack.Serialization.MessagePackSerializer<Noroshi.Core.WebApi.Response.Character.EquippedGear[]> _serializer2;
        
        private MsgPack.Serialization.MessagePackSerializer<byte> _serializer3;
        
        public Noroshi_Core_WebApi_Response_Master_ShadowCharacterSerializer(MsgPack.Serialization.SerializationContext context) : 
                base(context) {
            MsgPack.Serialization.PolymorphismSchema schema0 = default(MsgPack.Serialization.PolymorphismSchema);
            schema0 = null;
            this._serializer0 = context.GetSerializer<ushort>(schema0);
            MsgPack.Serialization.PolymorphismSchema schema1 = default(MsgPack.Serialization.PolymorphismSchema);
            schema1 = null;
            this._serializer1 = context.GetSerializer<uint>(schema1);
            MsgPack.Serialization.PolymorphismSchema schema2 = default(MsgPack.Serialization.PolymorphismSchema);
            schema2 = null;
            this._serializer2 = context.GetSerializer<Noroshi.Core.WebApi.Response.Character.EquippedGear[]>(schema2);
            MsgPack.Serialization.PolymorphismSchema schema3 = default(MsgPack.Serialization.PolymorphismSchema);
            schema3 = null;
            this._serializer3 = context.GetSerializer<byte>(schema3);
        }
        
        protected override void PackToCore(MsgPack.Packer packer, Noroshi.Core.WebApi.Response.Master.ShadowCharacter objectTree) {
            packer.PackArrayHeader(11);
            this._serializer0.PackTo(packer, objectTree.ActionLevel1);
            this._serializer0.PackTo(packer, objectTree.ActionLevel2);
            this._serializer0.PackTo(packer, objectTree.ActionLevel3);
            this._serializer0.PackTo(packer, objectTree.ActionLevel4);
            this._serializer0.PackTo(packer, objectTree.ActionLevel5);
            this._serializer1.PackTo(packer, objectTree.CharacterID);
            this._serializer2.PackTo(packer, objectTree.EquippedGears);
            this._serializer3.PackTo(packer, objectTree.EvolutionLevel);
            this._serializer1.PackTo(packer, objectTree.ID);
            this._serializer0.PackTo(packer, objectTree.Level);
            this._serializer3.PackTo(packer, objectTree.PromotionLevel);
        }
        
        protected override Noroshi.Core.WebApi.Response.Master.ShadowCharacter UnpackFromCore(MsgPack.Unpacker unpacker) {
            Noroshi.Core.WebApi.Response.Master.ShadowCharacter result = default(Noroshi.Core.WebApi.Response.Master.ShadowCharacter);
            result = new Noroshi.Core.WebApi.Response.Master.ShadowCharacter();
            if (unpacker.IsArrayHeader) {
                int unpacked = default(int);
                int itemsCount = default(int);
                itemsCount = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                System.Nullable<ushort> nullable = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 ActionLevel1");
                }
                if (nullable.HasValue) {
                    result.ActionLevel1 = nullable.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable0 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable0 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 ActionLevel2");
                }
                if (nullable0.HasValue) {
                    result.ActionLevel2 = nullable0.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable1 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable1 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 ActionLevel3");
                }
                if (nullable1.HasValue) {
                    result.ActionLevel3 = nullable1.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable2 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable2 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 ActionLevel4");
                }
                if (nullable2.HasValue) {
                    result.ActionLevel4 = nullable2.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable3 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable3 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 ActionLevel5");
                }
                if (nullable3.HasValue) {
                    result.ActionLevel5 = nullable3.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<uint> nullable4 = default(System.Nullable<uint>);
                if ((unpacked < itemsCount)) {
                    nullable4 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt32 CharacterID");
                }
                if (nullable4.HasValue) {
                    result.CharacterID = nullable4.Value;
                }
                unpacked = (unpacked + 1);
                Noroshi.Core.WebApi.Response.Character.EquippedGear[] nullable5 = default(Noroshi.Core.WebApi.Response.Character.EquippedGear[]);
                if ((unpacked < itemsCount)) {
                    if ((unpacker.Read() == false)) {
                        throw MsgPack.Serialization.SerializationExceptions.NewMissingItem(6);
                    }
                    if (((unpacker.IsArrayHeader == false) 
                                && (unpacker.IsMapHeader == false))) {
                        nullable5 = this._serializer2.UnpackFrom(unpacker);
                    }
                    else {
                        MsgPack.Unpacker disposable = default(MsgPack.Unpacker);
                        disposable = unpacker.ReadSubtree();
                        try {
                            nullable5 = this._serializer2.UnpackFrom(disposable);
                        }
                        finally {
                            if (((disposable == null) 
                                        == false)) {
                                disposable.Dispose();
                            }
                        }
                    }
                }
                if (((nullable5 == null) 
                            == false)) {
                    result.EquippedGears = nullable5;
                }
                unpacked = (unpacked + 1);
                System.Nullable<byte> nullable6 = default(System.Nullable<byte>);
                if ((unpacked < itemsCount)) {
                    nullable6 = MsgPack.Serialization.UnpackHelpers.UnpackNullableByteValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "Byte EvolutionLevel");
                }
                if (nullable6.HasValue) {
                    result.EvolutionLevel = nullable6.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<uint> nullable7 = default(System.Nullable<uint>);
                if ((unpacked < itemsCount)) {
                    nullable7 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt32 ID");
                }
                if (nullable7.HasValue) {
                    result.ID = nullable7.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable8 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable8 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 Level");
                }
                if (nullable8.HasValue) {
                    result.Level = nullable8.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<byte> nullable9 = default(System.Nullable<byte>);
                if ((unpacked < itemsCount)) {
                    nullable9 = MsgPack.Serialization.UnpackHelpers.UnpackNullableByteValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "Byte PromotionLevel");
                }
                if (nullable9.HasValue) {
                    result.PromotionLevel = nullable9.Value;
                }
                unpacked = (unpacked + 1);
            }
            else {
                int itemsCount0 = default(int);
                itemsCount0 = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                for (int i = 0; (i < itemsCount0); i = (i + 1)) {
                    string key = default(string);
                    string nullable10 = default(string);
                    nullable10 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "MemberName");
                    if (((nullable10 == null) 
                                == false)) {
                        key = nullable10;
                    }
                    else {
                        throw MsgPack.Serialization.SerializationExceptions.NewNullIsProhibited("MemberName");
                    }
                    if ((key == "PromotionLevel")) {
                        System.Nullable<byte> nullable21 = default(System.Nullable<byte>);
                        nullable21 = MsgPack.Serialization.UnpackHelpers.UnpackNullableByteValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "Byte PromotionLevel");
                        if (nullable21.HasValue) {
                            result.PromotionLevel = nullable21.Value;
                        }
                    }
                    else {
                        if ((key == "Level")) {
                            System.Nullable<ushort> nullable20 = default(System.Nullable<ushort>);
                            nullable20 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 Level");
                            if (nullable20.HasValue) {
                                result.Level = nullable20.Value;
                            }
                        }
                        else {
                            if ((key == "ID")) {
                                System.Nullable<uint> nullable19 = default(System.Nullable<uint>);
                                nullable19 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt32 ID");
                                if (nullable19.HasValue) {
                                    result.ID = nullable19.Value;
                                }
                            }
                            else {
                                if ((key == "EvolutionLevel")) {
                                    System.Nullable<byte> nullable18 = default(System.Nullable<byte>);
                                    nullable18 = MsgPack.Serialization.UnpackHelpers.UnpackNullableByteValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "Byte EvolutionLevel");
                                    if (nullable18.HasValue) {
                                        result.EvolutionLevel = nullable18.Value;
                                    }
                                }
                                else {
                                    if ((key == "EquippedGears")) {
                                        Noroshi.Core.WebApi.Response.Character.EquippedGear[] nullable17 = default(Noroshi.Core.WebApi.Response.Character.EquippedGear[]);
                                        if ((unpacker.Read() == false)) {
                                            throw MsgPack.Serialization.SerializationExceptions.NewMissingItem(i);
                                        }
                                        if (((unpacker.IsArrayHeader == false) 
                                                    && (unpacker.IsMapHeader == false))) {
                                            nullable17 = this._serializer2.UnpackFrom(unpacker);
                                        }
                                        else {
                                            MsgPack.Unpacker disposable0 = default(MsgPack.Unpacker);
                                            disposable0 = unpacker.ReadSubtree();
                                            try {
                                                nullable17 = this._serializer2.UnpackFrom(disposable0);
                                            }
                                            finally {
                                                if (((disposable0 == null) 
                                                            == false)) {
                                                    disposable0.Dispose();
                                                }
                                            }
                                        }
                                        if (((nullable17 == null) 
                                                    == false)) {
                                            result.EquippedGears = nullable17;
                                        }
                                    }
                                    else {
                                        if ((key == "CharacterID")) {
                                            System.Nullable<uint> nullable16 = default(System.Nullable<uint>);
                                            nullable16 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt32 CharacterID");
                                            if (nullable16.HasValue) {
                                                result.CharacterID = nullable16.Value;
                                            }
                                        }
                                        else {
                                            if ((key == "ActionLevel5")) {
                                                System.Nullable<ushort> nullable15 = default(System.Nullable<ushort>);
                                                nullable15 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 ActionLevel5");
                                                if (nullable15.HasValue) {
                                                    result.ActionLevel5 = nullable15.Value;
                                                }
                                            }
                                            else {
                                                if ((key == "ActionLevel4")) {
                                                    System.Nullable<ushort> nullable14 = default(System.Nullable<ushort>);
                                                    nullable14 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 ActionLevel4");
                                                    if (nullable14.HasValue) {
                                                        result.ActionLevel4 = nullable14.Value;
                                                    }
                                                }
                                                else {
                                                    if ((key == "ActionLevel3")) {
                                                        System.Nullable<ushort> nullable13 = default(System.Nullable<ushort>);
                                                        nullable13 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 ActionLevel3");
                                                        if (nullable13.HasValue) {
                                                            result.ActionLevel3 = nullable13.Value;
                                                        }
                                                    }
                                                    else {
                                                        if ((key == "ActionLevel2")) {
                                                            System.Nullable<ushort> nullable12 = default(System.Nullable<ushort>);
                                                            nullable12 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 ActionLevel2");
                                                            if (nullable12.HasValue) {
                                                                result.ActionLevel2 = nullable12.Value;
                                                            }
                                                        }
                                                        else {
                                                            if ((key == "ActionLevel1")) {
                                                                System.Nullable<ushort> nullable11 = default(System.Nullable<ushort>);
                                                                nullable11 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.ShadowCharacter), "UInt16 ActionLevel1");
                                                                if (nullable11.HasValue) {
                                                                    result.ActionLevel1 = nullable11.Value;
                                                                }
                                                            }
                                                            else {
                                                                unpacker.Skip();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }
        
        private static T @__Conditional<T>(bool condition, T whenTrue, T whenFalse)
         {
            if (condition) {
                return whenTrue;
            }
            else {
                return whenFalse;
            }
        }
    }
}
