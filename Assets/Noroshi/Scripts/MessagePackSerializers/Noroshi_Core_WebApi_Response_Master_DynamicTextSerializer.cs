﻿//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Noroshi.MessagePackSerializers {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("MsgPack.Serialization.CodeDomSerializers.CodeDomSerializerBuilder", "0.6.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public class Noroshi_Core_WebApi_Response_Master_DynamicTextSerializer : MsgPack.Serialization.MessagePackSerializer<Noroshi.Core.WebApi.Response.Master.DynamicText> {
        
        private MsgPack.Serialization.MessagePackSerializer<string> _serializer0;
        
        public Noroshi_Core_WebApi_Response_Master_DynamicTextSerializer(MsgPack.Serialization.SerializationContext context) : 
                base(context) {
            MsgPack.Serialization.PolymorphismSchema schema0 = default(MsgPack.Serialization.PolymorphismSchema);
            schema0 = null;
            this._serializer0 = context.GetSerializer<string>(schema0);
        }
        
        protected override void PackToCore(MsgPack.Packer packer, Noroshi.Core.WebApi.Response.Master.DynamicText objectTree) {
            packer.PackArrayHeader(2);
            this._serializer0.PackTo(packer, objectTree.ID);
            this._serializer0.PackTo(packer, objectTree.Text);
        }
        
        protected override Noroshi.Core.WebApi.Response.Master.DynamicText UnpackFromCore(MsgPack.Unpacker unpacker) {
            Noroshi.Core.WebApi.Response.Master.DynamicText result = default(Noroshi.Core.WebApi.Response.Master.DynamicText);
            result = new Noroshi.Core.WebApi.Response.Master.DynamicText();
            if (unpacker.IsArrayHeader) {
                int unpacked = default(int);
                int itemsCount = default(int);
                itemsCount = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                string nullable = default(string);
                if ((unpacked < itemsCount)) {
                    nullable = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.DynamicText), "System.String ID");
                }
                if (((nullable == null) 
                            == false)) {
                    result.ID = nullable;
                }
                unpacked = (unpacked + 1);
                string nullable0 = default(string);
                if ((unpacked < itemsCount)) {
                    nullable0 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.DynamicText), "System.String Text");
                }
                if (((nullable0 == null) 
                            == false)) {
                    result.Text = nullable0;
                }
                unpacked = (unpacked + 1);
            }
            else {
                int itemsCount0 = default(int);
                itemsCount0 = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                for (int i = 0; (i < itemsCount0); i = (i + 1)) {
                    string key = default(string);
                    string nullable1 = default(string);
                    nullable1 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.DynamicText), "MemberName");
                    if (((nullable1 == null) 
                                == false)) {
                        key = nullable1;
                    }
                    else {
                        throw MsgPack.Serialization.SerializationExceptions.NewNullIsProhibited("MemberName");
                    }
                    if ((key == "Text")) {
                        string nullable3 = default(string);
                        nullable3 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.DynamicText), "System.String Text");
                        if (((nullable3 == null) 
                                    == false)) {
                            result.Text = nullable3;
                        }
                    }
                    else {
                        if ((key == "ID")) {
                            string nullable2 = default(string);
                            nullable2 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Master.DynamicText), "System.String ID");
                            if (((nullable2 == null) 
                                        == false)) {
                                result.ID = nullable2;
                            }
                        }
                        else {
                            unpacker.Skip();
                        }
                    }
                }
            }
            return result;
        }
        
        private static T @__Conditional<T>(bool condition, T whenTrue, T whenFalse)
         {
            if (condition) {
                return whenTrue;
            }
            else {
                return whenFalse;
            }
        }
    }
}
