﻿//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Noroshi.MessagePackSerializers {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("MsgPack.Serialization.CodeDomSerializers.CodeDomSerializerBuilder", "0.6.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public class Noroshi_Core_WebApi_Response_Story_StoryEpisodeSerializer : MsgPack.Serialization.MessagePackSerializer<Noroshi.Core.WebApi.Response.Story.StoryEpisode> {
        
        private MsgPack.Serialization.MessagePackSerializer<bool> _serializer0;
        
        private MsgPack.Serialization.MessagePackSerializer<uint> _serializer1;
        
        private MsgPack.Serialization.MessagePackSerializer<ushort> _serializer2;
        
        private MsgPack.Serialization.MessagePackSerializer<System.Nullable<ushort>> _serializer3;
        
        private MsgPack.Serialization.MessagePackSerializer<Noroshi.Core.WebApi.Response.Story.StoryStage[]> _serializer4;
        
        private MsgPack.Serialization.MessagePackSerializer<string> _serializer5;
        
        public Noroshi_Core_WebApi_Response_Story_StoryEpisodeSerializer(MsgPack.Serialization.SerializationContext context) : 
                base(context) {
            MsgPack.Serialization.PolymorphismSchema schema0 = default(MsgPack.Serialization.PolymorphismSchema);
            schema0 = null;
            this._serializer0 = context.GetSerializer<bool>(schema0);
            MsgPack.Serialization.PolymorphismSchema schema1 = default(MsgPack.Serialization.PolymorphismSchema);
            schema1 = null;
            this._serializer1 = context.GetSerializer<uint>(schema1);
            MsgPack.Serialization.PolymorphismSchema schema2 = default(MsgPack.Serialization.PolymorphismSchema);
            schema2 = null;
            this._serializer2 = context.GetSerializer<ushort>(schema2);
            MsgPack.Serialization.PolymorphismSchema schema3 = default(MsgPack.Serialization.PolymorphismSchema);
            schema3 = null;
            this._serializer3 = context.GetSerializer<System.Nullable<ushort>>(schema3);
            MsgPack.Serialization.PolymorphismSchema schema4 = default(MsgPack.Serialization.PolymorphismSchema);
            schema4 = null;
            this._serializer4 = context.GetSerializer<Noroshi.Core.WebApi.Response.Story.StoryStage[]>(schema4);
            MsgPack.Serialization.PolymorphismSchema schema5 = default(MsgPack.Serialization.PolymorphismSchema);
            schema5 = null;
            this._serializer5 = context.GetSerializer<string>(schema5);
        }
        
        protected override void PackToCore(MsgPack.Packer packer, Noroshi.Core.WebApi.Response.Story.StoryEpisode objectTree) {
            packer.PackArrayHeader(10);
            this._serializer0.PackTo(packer, objectTree.BackStoryIsClear);
            this._serializer0.PackTo(packer, objectTree.BackStoryIsOpen);
            this._serializer1.PackTo(packer, objectTree.ChapterID);
            this._serializer1.PackTo(packer, objectTree.ID);
            this._serializer2.PackTo(packer, objectTree.No);
            this._serializer0.PackTo(packer, objectTree.NormalStoryIsClear);
            this._serializer0.PackTo(packer, objectTree.NormalStoryIsOpen);
            this._serializer3.PackTo(packer, objectTree.Rank);
            this._serializer4.PackTo(packer, objectTree.Stages);
            this._serializer5.PackTo(packer, objectTree.TextKey);
        }
        
        protected override Noroshi.Core.WebApi.Response.Story.StoryEpisode UnpackFromCore(MsgPack.Unpacker unpacker) {
            Noroshi.Core.WebApi.Response.Story.StoryEpisode result = default(Noroshi.Core.WebApi.Response.Story.StoryEpisode);
            result = new Noroshi.Core.WebApi.Response.Story.StoryEpisode();
            if (unpacker.IsArrayHeader) {
                int unpacked = default(int);
                int itemsCount = default(int);
                itemsCount = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                System.Nullable<bool> nullable = default(System.Nullable<bool>);
                if ((unpacked < itemsCount)) {
                    nullable = MsgPack.Serialization.UnpackHelpers.UnpackNullableBooleanValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "Boolean BackStoryIsClear");
                }
                if (nullable.HasValue) {
                    result.BackStoryIsClear = nullable.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<bool> nullable0 = default(System.Nullable<bool>);
                if ((unpacked < itemsCount)) {
                    nullable0 = MsgPack.Serialization.UnpackHelpers.UnpackNullableBooleanValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "Boolean BackStoryIsOpen");
                }
                if (nullable0.HasValue) {
                    result.BackStoryIsOpen = nullable0.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<uint> nullable1 = default(System.Nullable<uint>);
                if ((unpacked < itemsCount)) {
                    nullable1 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "UInt32 ChapterID");
                }
                if (nullable1.HasValue) {
                    result.ChapterID = nullable1.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<uint> nullable2 = default(System.Nullable<uint>);
                if ((unpacked < itemsCount)) {
                    nullable2 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "UInt32 ID");
                }
                if (nullable2.HasValue) {
                    result.ID = nullable2.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable3 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable3 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "UInt16 No");
                }
                if (nullable3.HasValue) {
                    result.No = nullable3.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<bool> nullable4 = default(System.Nullable<bool>);
                if ((unpacked < itemsCount)) {
                    nullable4 = MsgPack.Serialization.UnpackHelpers.UnpackNullableBooleanValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "Boolean NormalStoryIsClear");
                }
                if (nullable4.HasValue) {
                    result.NormalStoryIsClear = nullable4.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<bool> nullable5 = default(System.Nullable<bool>);
                if ((unpacked < itemsCount)) {
                    nullable5 = MsgPack.Serialization.UnpackHelpers.UnpackNullableBooleanValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "Boolean NormalStoryIsOpen");
                }
                if (nullable5.HasValue) {
                    result.NormalStoryIsOpen = nullable5.Value;
                }
                unpacked = (unpacked + 1);
                System.Nullable<ushort> nullable6 = default(System.Nullable<ushort>);
                if ((unpacked < itemsCount)) {
                    nullable6 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "System.Nullable`1[System.UInt16] Rank");
                }
                if (nullable6.HasValue) {
                    result.Rank = nullable6;
                }
                unpacked = (unpacked + 1);
                Noroshi.Core.WebApi.Response.Story.StoryStage[] nullable7 = default(Noroshi.Core.WebApi.Response.Story.StoryStage[]);
                if ((unpacked < itemsCount)) {
                    if ((unpacker.Read() == false)) {
                        throw MsgPack.Serialization.SerializationExceptions.NewMissingItem(8);
                    }
                    if (((unpacker.IsArrayHeader == false) 
                                && (unpacker.IsMapHeader == false))) {
                        nullable7 = this._serializer4.UnpackFrom(unpacker);
                    }
                    else {
                        MsgPack.Unpacker disposable = default(MsgPack.Unpacker);
                        disposable = unpacker.ReadSubtree();
                        try {
                            nullable7 = this._serializer4.UnpackFrom(disposable);
                        }
                        finally {
                            if (((disposable == null) 
                                        == false)) {
                                disposable.Dispose();
                            }
                        }
                    }
                }
                if (((nullable7 == null) 
                            == false)) {
                    result.Stages = nullable7;
                }
                unpacked = (unpacked + 1);
                string nullable8 = default(string);
                if ((unpacked < itemsCount)) {
                    nullable8 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "System.String TextKey");
                }
                if (((nullable8 == null) 
                            == false)) {
                    result.TextKey = nullable8;
                }
                unpacked = (unpacked + 1);
            }
            else {
                int itemsCount0 = default(int);
                itemsCount0 = MsgPack.Serialization.UnpackHelpers.GetItemsCount(unpacker);
                for (int i = 0; (i < itemsCount0); i = (i + 1)) {
                    string key = default(string);
                    string nullable9 = default(string);
                    nullable9 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "MemberName");
                    if (((nullable9 == null) 
                                == false)) {
                        key = nullable9;
                    }
                    else {
                        throw MsgPack.Serialization.SerializationExceptions.NewNullIsProhibited("MemberName");
                    }
                    if ((key == "TextKey")) {
                        string nullable19 = default(string);
                        nullable19 = MsgPack.Serialization.UnpackHelpers.UnpackStringValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "System.String TextKey");
                        if (((nullable19 == null) 
                                    == false)) {
                            result.TextKey = nullable19;
                        }
                    }
                    else {
                        if ((key == "Stages")) {
                            Noroshi.Core.WebApi.Response.Story.StoryStage[] nullable18 = default(Noroshi.Core.WebApi.Response.Story.StoryStage[]);
                            if ((unpacker.Read() == false)) {
                                throw MsgPack.Serialization.SerializationExceptions.NewMissingItem(i);
                            }
                            if (((unpacker.IsArrayHeader == false) 
                                        && (unpacker.IsMapHeader == false))) {
                                nullable18 = this._serializer4.UnpackFrom(unpacker);
                            }
                            else {
                                MsgPack.Unpacker disposable0 = default(MsgPack.Unpacker);
                                disposable0 = unpacker.ReadSubtree();
                                try {
                                    nullable18 = this._serializer4.UnpackFrom(disposable0);
                                }
                                finally {
                                    if (((disposable0 == null) 
                                                == false)) {
                                        disposable0.Dispose();
                                    }
                                }
                            }
                            if (((nullable18 == null) 
                                        == false)) {
                                result.Stages = nullable18;
                            }
                        }
                        else {
                            if ((key == "Rank")) {
                                System.Nullable<ushort> nullable17 = default(System.Nullable<ushort>);
                                nullable17 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "System.Nullable`1[System.UInt16] Rank");
                                if (nullable17.HasValue) {
                                    result.Rank = nullable17;
                                }
                            }
                            else {
                                if ((key == "NormalStoryIsOpen")) {
                                    System.Nullable<bool> nullable16 = default(System.Nullable<bool>);
                                    nullable16 = MsgPack.Serialization.UnpackHelpers.UnpackNullableBooleanValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "Boolean NormalStoryIsOpen");
                                    if (nullable16.HasValue) {
                                        result.NormalStoryIsOpen = nullable16.Value;
                                    }
                                }
                                else {
                                    if ((key == "NormalStoryIsClear")) {
                                        System.Nullable<bool> nullable15 = default(System.Nullable<bool>);
                                        nullable15 = MsgPack.Serialization.UnpackHelpers.UnpackNullableBooleanValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "Boolean NormalStoryIsClear");
                                        if (nullable15.HasValue) {
                                            result.NormalStoryIsClear = nullable15.Value;
                                        }
                                    }
                                    else {
                                        if ((key == "No")) {
                                            System.Nullable<ushort> nullable14 = default(System.Nullable<ushort>);
                                            nullable14 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt16Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "UInt16 No");
                                            if (nullable14.HasValue) {
                                                result.No = nullable14.Value;
                                            }
                                        }
                                        else {
                                            if ((key == "ID")) {
                                                System.Nullable<uint> nullable13 = default(System.Nullable<uint>);
                                                nullable13 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "UInt32 ID");
                                                if (nullable13.HasValue) {
                                                    result.ID = nullable13.Value;
                                                }
                                            }
                                            else {
                                                if ((key == "ChapterID")) {
                                                    System.Nullable<uint> nullable12 = default(System.Nullable<uint>);
                                                    nullable12 = MsgPack.Serialization.UnpackHelpers.UnpackNullableUInt32Value(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "UInt32 ChapterID");
                                                    if (nullable12.HasValue) {
                                                        result.ChapterID = nullable12.Value;
                                                    }
                                                }
                                                else {
                                                    if ((key == "BackStoryIsOpen")) {
                                                        System.Nullable<bool> nullable11 = default(System.Nullable<bool>);
                                                        nullable11 = MsgPack.Serialization.UnpackHelpers.UnpackNullableBooleanValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "Boolean BackStoryIsOpen");
                                                        if (nullable11.HasValue) {
                                                            result.BackStoryIsOpen = nullable11.Value;
                                                        }
                                                    }
                                                    else {
                                                        if ((key == "BackStoryIsClear")) {
                                                            System.Nullable<bool> nullable10 = default(System.Nullable<bool>);
                                                            nullable10 = MsgPack.Serialization.UnpackHelpers.UnpackNullableBooleanValue(unpacker, typeof(Noroshi.Core.WebApi.Response.Story.StoryEpisode), "Boolean BackStoryIsClear");
                                                            if (nullable10.HasValue) {
                                                                result.BackStoryIsClear = nullable10.Value;
                                                            }
                                                        }
                                                        else {
                                                            unpacker.Skip();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }
        
        private static T @__Conditional<T>(bool condition, T whenTrue, T whenFalse)
         {
            if (condition) {
                return whenTrue;
            }
            else {
                return whenFalse;
            }
        }
    }
}
