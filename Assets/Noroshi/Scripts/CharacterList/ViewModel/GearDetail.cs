﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UniRx;
using Noroshi.Game;
using Noroshi.UI;
using Noroshi.Core.WebApi.Response;
using Noroshi.Core.WebApi.Response.Character;

namespace Noroshi.CharacterList {
    public class GearDetail : MonoBehaviour {
        [SerializeField] BtnCommon btnBackground;
        [SerializeField] GearStatusPanel gearStatusPanel;
        [SerializeField] GearRecipePanel gearRecipePanel;
        [SerializeField] GetGearPanel gearGetPanel;
        [SerializeField] GearEnchantPanel gearEnchantPanel;

        public Subject<uint> OnEquip = new Subject<uint>();
        public Subject<uint> OnCreate = new Subject<uint>();
        public Subject<EquippedGear> OnEnchant = new Subject<EquippedGear>();

        private List<GameObject> childPanelList = new List<GameObject>();
        private List<int> selectedGearIDList = new List<int>();
        private Core.WebApi.Response.Master.Gear gearData;
        private EquippedGear playerGearData;
        private int depth = 0;
        private bool isOpen = false;

        private void Start() {
            btnBackground.OnClickedBtn.Subscribe(_ => {
                UILoading.Instance.RemoveQuery(QueryKeys.SelectedEquipGearList);
                CloseGearSet();
            });
            btnBackground.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            gearStatusPanel.OnOpenRecipe.Subscribe(_ => {
                if(childPanelList.Count > 0) {return;}
                OpenRecipe(gearData.ID);
            });

            gearStatusPanel.OnEquip.Subscribe(gearID => {
                UILoading.Instance.RemoveQuery(QueryKeys.SelectedEquipGearList);
                OnEquip.OnNext(gearID);
                CloseGearSet();
            });

            gearGetPanel.OnBack.Subscribe(_ => {
                IncreaseDepth();
            });

            gearEnchantPanel.OnEnchant.Subscribe(playerGear => {
                OnEnchant.OnNext(playerGear);
                gearStatusPanel.SetEnchantLevel(playerGear);
                gearEnchantPanel.OpenEnchantPanel(gearData, playerGear);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseGearSet();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            gearEnchantPanel.Init();
            CloseGearSet();
        }

        private void OpenRecipe(uint id) {
            var recipes = ItemListManager.Instance.GetGearRecipe(id);
            depth++;
            gearEnchantPanel.gameObject.SetActive(false);
            if(recipes != null && recipes.Count > 0) {
                CreateRecipePanel(id);
            } else {
                OpenGetPanel(id);
            }
            selectedGearIDList.Add((int)id);
            UILoading.Instance.SetMultiQuery(QueryKeys.SelectedEquipGearList, selectedGearIDList);
            MoveStatusPanel();
            MoveChildPanelList();
        }

        private void CreateRecipePanel(uint id) {
            var panel = Instantiate(gearRecipePanel);
            panel.transform.SetParent(transform);
            panel.transform.SetAsFirstSibling();
            btnBackground.transform.SetAsFirstSibling();
            panel.transform.localScale = Vector3.one;
            panel.transform.localPosition = new Vector3(0, -30, 0);
            childPanelList.Add(panel.gameObject);
            panel.OnMaterialSelect.Subscribe(materialID => {
                OpenRecipe(materialID);
            }).AddTo(panel);
            panel.OnBack.Subscribe(_ => {
                IncreaseDepth();
            }).AddTo(panel);
            panel.OnCreate.Subscribe(craftID => {
                IncreaseDepth();
                if(depth > 0) {
                    childPanelList[depth - 1].GetComponent<GearRecipePanel>().SetMaterialNum();
                } else {
                    gearStatusPanel.UpdateHaveNum();
                }
                OnCreate.OnNext(craftID);
            }).AddTo(panel);
            panel.SetCreateGearInfo(id);
        }

        private void OpenGetPanel(uint id) {
            gearGetPanel.transform.SetAsFirstSibling();
            btnBackground.transform.SetAsFirstSibling();
            gearGetPanel.transform.localPosition = new Vector3(0, -30, 0);
            childPanelList.Add(gearGetPanel.gameObject);
            gearGetPanel.gameObject.SetActive(true);
            gearGetPanel.SetInfo(id);
        }

        private void IncreaseDepth() {
            depth--;
            if(childPanelList[childPanelList.Count - 1].GetComponent<GetGearPanel>() == null) {
                Destroy(childPanelList[childPanelList.Count - 1]);
            } else {
                childPanelList[childPanelList.Count - 1].SetActive(false);
            }
            selectedGearIDList.RemoveAt(selectedGearIDList.Count - 1);
            childPanelList.RemoveAt(childPanelList.Count - 1);
            MoveStatusPanel();
            MoveChildPanelList();
        }

        private void MoveStatusPanel() {
            if(depth == 0) {
                TweenX.Add(gearStatusPanel.gameObject, 0.3f, 0).EaseOutCubic();
            } else {
                TweenX.Add(gearStatusPanel.gameObject, 0.3f, -175 - depth * 25).EaseOutCubic();
            }
        }

        private void MoveChildPanelList() {
            for(int i = 0, l = childPanelList.Count; i < l; i++) {
                if(i < l - 1) {
                    TweenX.Add(childPanelList[i], 0.3f, -depth * 25 - 155 + i * 50).EaseOutCubic();
                } else {
                    TweenX.Add(childPanelList[i], 0.3f, 180 + depth * 25).EaseOutCubic();
                }
            }
        }

        private void CloseGearSet() {
            depth = 0;
            selectedGearIDList = new List<int>();
            foreach(var panel in childPanelList) {
                if(panel.GetComponent<GetGearPanel>() == null) {
                    Destroy(panel);
                } else {
                    panel.SetActive(false);
                }
            }
            childPanelList = new List<GameObject>();
            gearStatusPanel.CloseGearStatus();
            gearEnchantPanel.gameObject.SetActive(false);
            isOpen = false;
            gameObject.SetActive(false);
            BackButtonController.Instance.IsModalOpen(false);
        }

        public void OpenGearDetail(Core.WebApi.Response.Master.Gear gear, EquippedGear playerGear, int state, int lv) {
            gearData = gear;
            playerGearData = playerGear;

            isOpen = true;
            gameObject.SetActive(true);
            BackButtonController.Instance.IsModalOpen(true);
            gearStatusPanel.SetGearStatusPanel(gearData, playerGearData, state, lv);
            if(state > 0) {
                if(gear.NecessaryEnchantExpMaps.Length > 0
                    && playerGear.Exp < gear.NecessaryEnchantExpMaps[gear.NecessaryEnchantExpMaps.Length - 1]) {
                    gearEnchantPanel.OpenEnchantPanel(gear, playerGear);
                    TweenX.Add(gearStatusPanel.gameObject, 0.3f, -305).EaseOutCubic();
                }
            } else if(state != -2 && UILoading.Instance.GetMultiQuery(QueryKeys.SelectedEquipGearList).Count < 1) {
                OpenRecipe(gear.ID);
            }
        }

        public void ReverseGearSet() {
            TweenX.Add(gearStatusPanel.gameObject, 0.3f, 0).EaseOutCubic();
        }

        public void SetDefault(List<int> ids) {
            for(int i = 0, l = ids.Count; i < l; i++) {
                OpenRecipe((uint)ids[i]);
            }
        }
    }
}
