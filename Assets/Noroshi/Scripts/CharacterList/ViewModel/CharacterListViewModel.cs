using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Game;
using Noroshi.UI;
using Noroshi.Core.WebApi.Response;
using Noroshi.Core.WebApi.Response.Character;

namespace Noroshi.CharacterList {
    public class CharacterListViewModel : MonoBehaviour {
        [SerializeField] GameObject characterListContainer;
        [SerializeField] GameObject acquiredCharacterListContainer;
        [SerializeField] GameObject unAcquiredCharacterListContainer;
        [SerializeField] CharacterPanel characterPanel;
        [SerializeField] CharacterDetailPanel characterDetailPanel;
        [SerializeField] BtnCommon btnOpenFilter;
        [SerializeField] Text txtBtnOpenFilter;
        [SerializeField] GameObject filterContainer;
        [SerializeField] GameObject filterSelector;
        [SerializeField] GameObject filterBtnWrapper;
        [SerializeField] BtnCommon btnFilterBackground;
        [SerializeField] BtnCommon[] btnFilterList;
        [SerializeField] BtnCommon btnSort;
        [SerializeField] Text txtBtnSort;
        [SerializeField] Text txtHaveCharacterNum;
        [SerializeField] Text txtMaxCharacterNum;
        [SerializeField] CharacterEquipTab equipTab;
        [SerializeField] CharacterSkillTab skillTab;
        [SerializeField] CharacterStatusTab statusTab;
        [SerializeField] GetSoulPanel getSoulPanel;
        [SerializeField] Canvas menuCanvas;

        private List<uint> acquiredCharacterIDList = new List<uint>();
        private List<CharacterPanel> originCharacterList = new List<CharacterPanel>();
        private List<CharacterPanel> characterList = new List<CharacterPanel>();
        private List<CharacterPanel> unAcquiredCharacterList = new List<CharacterPanel>();
        private int selectedIndex;
        private float filterPositionY = 9999;
        private int initialCreateCharacterNum = 0;
        private bool isFilterOpen = false;
        private bool isCharaCreated;
        private bool isUnAcquiredCharaCreated;
        private bool isOpenDetail = false;

        private void Start() {
            btnOpenFilter.OnClickedBtn.Subscribe(_ => {
                if(isFilterOpen) {
                    CloseFilter();
                } else {
                    OpenFilter();
                }
            });
            btnOpenFilter.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnFilterBackground.OnClickedBtn.Subscribe(_ => {
                CloseFilter();
            });

            foreach (var btn in btnFilterList) {
                btn.OnClickedBtn.Subscribe(id => {
                    FilterCharacterList(id);
                });
                btn.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnSort.OnClickedBtn.Subscribe(id => {
                SortCharacterList(id);
            });
            btnSort.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            characterDetailPanel.OnCloseDetail.Subscribe(_ => {
                isOpenDetail = false;
                menuCanvas.sortingOrder = 17;
                characterListContainer.SetActive(true);
            });

            characterDetailPanel.OnSwitchDetail.Subscribe(direction => {
                selectedIndex += direction;
                if(selectedIndex < 0) {selectedIndex = characterList.Count - 1;}
                if(selectedIndex > characterList.Count - 1) {selectedIndex = 0;}
                characterList[selectedIndex].SelectCharacter();
            });

            equipTab.OnChangeExp.Subscribe(id => {
                characterList[selectedIndex].UpdateLevel();
            });

            equipTab.OnClickedGetSoul.Subscribe(charaData => {
                getSoulPanel.SetInfo(charaData.soulID);
            });

            equipTab.OnEquip.Subscribe(id => {
                foreach (var panel in originCharacterList) {
                    var isOpen = panel.charaData.characterID == id;
                    panel.UPdateGear(isOpen);
                }
                characterDetailPanel.PlayCharacterAnimation(0);
            });

            equipTab.OnRaisePromotionLv.Subscribe(id => {
                characterList[selectedIndex].RaisePromotionLv();
                characterDetailPanel.PlayCharacterAnimation(0);
            });

            equipTab.OnCreate.Subscribe(_ => {
                foreach (var panel in originCharacterList) {
                    panel.UPdateGear();
                }
            });

            skillTab.OnSelectSkill.Subscribe(index => {
                int n = -1;
                if(index == 0) {
                    n = 4;
                } else if(index == 1) {
                    n = 5;
                } else if(index == 2) {
                    n = 6;
                } else if(index == 3) {
                    n = 7;
                } else if(index == 4) {
                    n = 8;
                }
                characterDetailPanel.PlayCharacterAnimation(n);
            });

            BackButtonController.Instance.OnBack.Subscribe(_ => {
                if(isOpenDetail) {
                    characterDetailPanel.CloseCharacterDetail();
                } else {
                    UILoading.Instance.LoadScene(Constant.SCENE_MAIN);
                }
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            StartCoroutine(OnLoading());
        }

        private IEnumerator OnLoading() {
            while (!ItemListManager.Instance.IsLoad || !PlayerInfo.Instance.isLoad || !PlayerInfo.Instance.IsSetLastStory) {
                yield return new WaitForEndOfFrame();
            }
            LoadCharacterList();
            LoadUnAcquiredCharacterList();
            while (!isCharaCreated || !isUnAcquiredCharaCreated) {
                yield return new WaitForEndOfFrame();
            }
            characterDetailPanel.CloseCharacterDetail();
            characterDetailPanel.Init();
            if(UILoading.Instance.GetQuery(QueryKeys.SelectedCharacter) > -1) {
                SetDefaultAcquiredCharacterOpen();
            } else if(UILoading.Instance.GetQuery(QueryKeys.SelectedUnAcquiredCharacter) > -1) {
                SetDefaultUnAcquiredCharacterOpen();
            }
            UILoading.Instance.HideLoading();
            SoundController.Instance.PlayBGM(SoundController.BGMKeys.CHARACTER_LIST);
        }

        private void SetDefaultAcquiredCharacterOpen() {
            var charaID = UILoading.Instance.GetQuery(QueryKeys.SelectedCharacter);
            var equipIndex = UILoading.Instance.GetQuery(QueryKeys.SelectedEquipIndex);
            foreach(var chara in characterList.Where(chara => chara.charaData.characterID == charaID)) {
                chara.SelectCharacter();
            }
            if(equipIndex > -1) {
                equipTab.OpenDefaultGearPanel(equipIndex);
            }
            if(UILoading.Instance.GetQuery(QueryKeys.IsOpenGetSoulPanel) > -1) {
                equipTab.OpenDefaultGetSoulPanel();
            }
        }

        private void SetDefaultUnAcquiredCharacterOpen() {
            var charaID = UILoading.Instance.GetQuery(QueryKeys.SelectedUnAcquiredCharacter);

            foreach(var chara in unAcquiredCharacterList.Where(chara => chara.charaData.characterID == charaID)) {
                chara.OnOpenGetSoul.OnNext(chara.charaData);
            }
        }

        private void LoadCharacterList() {
            GlobalContainer.RepositoryManager.PlayerCharacterRepository.GetAll().Do(list => {
                list = list.OrderByDescending(c => c.Level).ToArray();
                for(int i = 0, l = list.Length; i < l; i++) {
                    var status = new Game.CharacterStatus(list[i], GlobalContainer.MasterManager.CharacterMaster.Get(list[i].CharacterID));
                    var lv = GlobalContainer.MasterManager.LevelMaster.GetCharacterLevelByLevel((ushort)status.Level);
                    acquiredCharacterIDList.Add(list[i].CharacterID);
                    CreateCharacterList(status, list[i].ID, (int)(lv.Exp - list[i].ExpInLevel), list[i].EquippedGears);
                    if(i >= l - 1) {
                        characterList = originCharacterList;
                        txtHaveCharacterNum.text = list.Length.ToString();
                        FilterCharacterList(0);
                        SortCharacterList(1);
                    }
                }
                isCharaCreated = true;
            }).Subscribe();
        }

        private void LoadUnAcquiredCharacterList() {
            GlobalContainer.RepositoryManager.PlayerCharacterRepository.GetAllCharacters().Do(allList => {
                for(int i = 0, l = allList.Length; i < l; i++) {
                    var soul = GlobalContainer.MasterManager.ItemMaster.GetSoulByCharacterID(allList[i].CharacterID);
                    if(!acquiredCharacterIDList.Contains(allList[i].CharacterID)
                       && soul != null
                       ) {
                        var character = GlobalContainer.MasterManager.CharacterMaster.Get(allList[i].CharacterID);
                        CreateUnAcquiredCharacterList(character);
                    }
                    if(i == l - 1) {
                        unAcquiredCharacterList = unAcquiredCharacterList
                            .OrderByDescending(c => ((float)c.charaData.haveSoul / (float)c.charaData.needSoul))
                                .ThenBy(c => c.charaData.characterID).ToList();
                        foreach(var chara in unAcquiredCharacterList) {
                            chara.transform.SetAsLastSibling();
                        }
                        txtMaxCharacterNum.text = (acquiredCharacterIDList.Count + unAcquiredCharacterList.Count).ToString();
                    }
                }
                isUnAcquiredCharaCreated = true;
            }).Subscribe();
        }

        private void CreateCharacterList(CharacterStatus character, uint id, int exp, EquippedGear[] gearList = null) {
            var charaPanel = Instantiate(characterPanel);
            charaPanel.transform.SetParent(acquiredCharacterListContainer.transform);
            charaPanel.transform.localScale = Vector3.one;
            charaPanel.SetCharacterPanelData(character, id, exp, gearList);
            charaPanel.OnOpenDetail.Subscribe(data => {
                isOpenDetail = true;
                UILoading.Instance.SetQuery(QueryKeys.SelectedCharacter, (int)data.characterID);
                menuCanvas.sortingOrder = 6;
                characterDetailPanel.OpenCharacterDetail(data, characterList.Count > 1);
                equipTab.SetData(data);
                skillTab.SetData(data);
                statusTab.SetData(data);
                FindIndex(data.characterID);
                TweenNull.Add(gameObject, 0.15f).Then(() => {
                    characterListContainer.SetActive(false);
                });
            });

            originCharacterList.Add(charaPanel);
            if(originCharacterList.Count <= initialCreateCharacterNum) {
                characterDetailPanel.CreateCharaImage(character);
            }
        }

        private void CreateUnAcquiredCharacterList(Core.WebApi.Response.Master.Character character) {
            var charaPanel = Instantiate(characterPanel);
            charaPanel.transform.SetParent(unAcquiredCharacterListContainer.transform);
            charaPanel.transform.localScale = Vector3.one;
            charaPanel.SetUnAcquiredCharacter(character);
            charaPanel.OnOpenGetSoul.Subscribe(charaData => {
                UILoading.Instance.SetQuery(QueryKeys.SelectedUnAcquiredCharacter, (int)charaData.characterID);
                getSoulPanel.SetInfo(charaData.soulID);
            });

            unAcquiredCharacterList.Add(charaPanel);
        }

        private void FindIndex(uint charaID) {
            for(int i = 0, l = characterList.Count; i < l; i++) {
                if(characterList[i].charaData.characterID == charaID) {
                    selectedIndex = i;
                    return;
                }
            }
        }

        private void FilterCharacterList(int n) {
            var gclm = GlobalContainer.LocalizationManager;
            var tempList = new List<CharacterPanel>();

            foreach(var chara in originCharacterList) {
                if(n == 0 || (n < 4 && n == chara.charaData.position) || (n == 4 && chara.charaData.isDeca)) {
                    tempList.Add(chara);
                    chara.gameObject.SetActive(true);
                } else {
                    chara.gameObject.SetActive(false);
                }
            }
            foreach(var chara in unAcquiredCharacterList) {
                if(n == 0 || (n < 4 && n == chara.charaData.position) || (n == 4 && chara.charaData.isDeca)) {
                    chara.gameObject.SetActive(true);
                } else {
                    chara.gameObject.SetActive(false);
                }
            }
            characterList = tempList;
            filterSelector.transform.localPosition = btnFilterList[n].transform.localPosition;
            switch (n) {
                case 0: txtBtnOpenFilter.text = gclm.GetText("UI.Button.All"); break;
                case 1: txtBtnOpenFilter.text = gclm.GetText("UI.Button.Front"); break;
                case 2: txtBtnOpenFilter.text = gclm.GetText("UI.Button.Central"); break;
                case 3: txtBtnOpenFilter.text = gclm.GetText("UI.Button.Back"); break;
                case 4: txtBtnOpenFilter.text = gclm.GetText("UI.Button.Deca"); break;
                default: break;
            }
            CloseFilter();
        }

        private void OpenFilter() {
            isFilterOpen = true;
            btnOpenFilter.GetComponent<Canvas>().sortingOrder = 20;
            filterContainer.SetActive(true);
            if(filterPositionY == 9999) {
                filterPositionY = filterBtnWrapper.transform.localPosition.y;
            }
            filterBtnWrapper.SetActive(true);
            TweenA.Add(btnFilterBackground.gameObject, 0.1f, 0.5f).From(0).EaseOutCubic();
            TweenA.Add(filterBtnWrapper, 0.2f, 1).From(0).EaseOutCubic();
            TweenY.Add(filterBtnWrapper, 0.2f, filterPositionY).From(filterPositionY + 40).EaseOutCubic();
        }

        private void CloseFilter() {
            isFilterOpen = false;
            TweenA.Add(btnFilterBackground.gameObject, 0.25f, 0);
            TweenY.Add(filterBtnWrapper, 0.25f, filterPositionY + 40).EaseOutCubic();
            TweenA.Add(filterBtnWrapper, 0.25f, 0).EaseOutCubic().Then(() => {
                filterBtnWrapper.SetActive(false);
                filterContainer.gameObject.SetActive(false);
                btnOpenFilter.GetComponent<Canvas>().sortingOrder = 5;
            });
        }

        private void SortCharacterList(int type) {
            type++;
            if(type > 1) {type = 0;}
            btnSort.id = type;

            switch (type) {
                case 0:
                    txtBtnSort.text = GlobalContainer.LocalizationManager.GetText("UI.Heading.Level");
                    characterList = characterList
                        .OrderByDescending(c => c.charaData.lv).ThenBy(c => c.charaData.characterID).ToList();
                    originCharacterList = originCharacterList
                        .OrderByDescending(c => c.charaData.lv).ThenBy(c => c.charaData.characterID).ToList();
                    break;
                case 1:
                    txtBtnSort.text = GlobalContainer.LocalizationManager.GetText("UI.Button.Evolution");
                    characterList = characterList
                        .OrderByDescending(c => c.charaData.evolutionLv).ThenBy(c => c.charaData.characterID).ToList();
                    originCharacterList = originCharacterList
                        .OrderByDescending(c => c.charaData.evolutionLv).ThenBy(c => c.charaData.characterID).ToList();
                    break;
                default:
                    break;
            }
            acquiredCharacterListContainer.SetActive(false);
            foreach(var chara in originCharacterList) {
                chara.ChangeSort(type);
                chara.transform.SetAsLastSibling();
            }
            acquiredCharacterListContainer.SetActive(true);
        }

        public bool CheckCharacterCreated() {
            return isCharaCreated;
        }
    }
}