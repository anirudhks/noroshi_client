﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.UI;
using Noroshi.Game;
using Noroshi.Core.Game.Player;

namespace Noroshi.CharacterList {
    public class CharacterSkillTab : MonoBehaviour {
        [SerializeField] SkillPanel[] skillPanelList;
        [SerializeField] RectTransform skillPanelWrapper;
        [SerializeField] ScrollRect scrollContent;
        [SerializeField] BtnCommon btnRecover;
        [SerializeField] Text txtRecoverTime;
        [SerializeField] Text txtRemainNum;
        [SerializeField] Text txtMaxNum;
        [SerializeField] Text txtSkillName;
        [SerializeField] Text txtSkillDescription;
        [SerializeField] GameObject skillDescriptionWrapper;
        [SerializeField] AlertModal notEnoughGoldAlert;
        [SerializeField] AlertModal notSkillLevelUpAnyMoreAlert;
        [SerializeField] ActionLevelPointRecoverModal actionLevelPointRecover;
        [SerializeField] GameObject processing;

        public Subject<int> OnSelectSkill = new Subject<int>();

        private CharacterPanel.CharaData charaData;
        private uint remainNum;
        private int[] skillupValueList = new int[5] {0, 0, 0, 0, 0};
        private bool isRecoverOpen = false;

        private Core.WebApi.Response.Master.ActionLevelUpPayment[] actionLevelUpPaymentList;
        private ActionLevelPointHandler _actionLevelPointHandler = new ActionLevelPointHandler();
        private IDisposable actionLevelPointTimer;
        private byte lastActionLevelPoint;
        private uint lastActionLevelPointUpdatedAt;
        private ushort CurrentActionLevelPoint {
            get {
                return _actionLevelPointHandler.CurrentValue(
                    lastActionLevelPoint,
                    lastActionLevelPointUpdatedAt,
                    MaxActionLevelPoint,
                    GlobalContainer.TimeHandler.UnixTime
                );
            }
        }
        private byte MaxActionLevelPoint {
            // temp
            get {
                byte maxValue;
                if(PlayerInfo.Instance.GetPlayerStatus().VipLevel > 4) {
                    maxValue = Noroshi.Core.Game.Player.Constant.MAX_ACTION_LEVEL_POINT;
                } else {
                    maxValue = Noroshi.Core.Game.Player.Constant.MAX_ACTION_LEVEL_POINT / 2;
                }
                return maxValue;
            }
        }
        private TimeSpan? RecoverTime {
            get {
                return _actionLevelPointHandler.GetTimeToNextIncrease(
                    lastActionLevelPoint,
                    lastActionLevelPointUpdatedAt,
                    MaxActionLevelPoint,
                    GlobalContainer.TimeHandler.UnixTime
                );
            }
        }

        private void Start() {
            var gclm = GlobalContainer.LocalizationManager;

            actionLevelUpPaymentList = GlobalContainer.MasterManager.CharacterMaster.GetActionLevelUpPayments();

            foreach(var panel in skillPanelList) {
                panel.OnPanelClick.Subscribe(index => {
                    txtSkillName.text = gclm.GetText(charaData.skillTextList[index] + ".Name");
                    txtSkillDescription.text = gclm.GetText(charaData.skillTextList[index] + ".MainText");
                    skillDescriptionWrapper.SetActive(true);
                    OnSelectSkill.OnNext(index);
                });
                panel.OnSkillUp.Subscribe(index => {
                    var payment = (int)actionLevelUpPaymentList[charaData.skillLvList[index] - 1].Gold;
                    if(payment > PlayerInfo.Instance.HaveGold) {
                        notEnoughGoldAlert.OnOpen();
                        SoundController.Instance.PlaySE(SoundController.SEKeys.ERROR);
                    } else if(remainNum > 0) {
                        remainNum--;
                        PlayerInfo.Instance.ChangeHaveGold(-payment);
                        charaData.skillLvList[index]++;
                        skillupValueList[index]++;
                        skillPanelList[index].UpdateLevel(charaData.skillLvList[index], actionLevelUpPaymentList[charaData.skillLvList[index] - 1].Gold);
                        UpdateRemainNum();
                        SoundController.Instance.PlaySE(SoundController.SEKeys.STATUS_UP);
                        if(!CheckSkillLimit(index)) {
                            skillPanelList[index].SetActiveLevelUp(false);
                        }
                    }
                });
                panel.OnNotSkillUp.Subscribe(index => {
                    Debug.Log("not level up any more");
                    notSkillLevelUpAnyMoreAlert.OnOpen();
                    SoundController.Instance.PlaySE(SoundController.SEKeys.ERROR);
                });
            }

            btnRecover.OnClickedBtn.Subscribe(_ => {
                isRecoverOpen = true;
                SendSkillUpValue();
            });

            actionLevelPointRecover.OnRecoverActionLevelPoint.Subscribe(_ => {
                lastActionLevelPoint = PlayerInfo.Instance.GetPlayerStatus().LastActionLevelPoint;
                lastActionLevelPointUpdatedAt = PlayerInfo.Instance.GetPlayerStatus().LastActionLevelPointUpdatedAt;
                remainNum = CurrentActionLevelPoint;
                txtMaxNum.text = MaxActionLevelPoint.ToString();
                UpdateRemainNum();
            });

            OnSkillScroll();
            StartCoroutine(OnLoadingPlayerInfo());
        }

        private IEnumerator OnLoadingPlayerInfo() {
            while (!PlayerInfo.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            Init();
        }

        private void Init() {
            lastActionLevelPoint = PlayerInfo.Instance.GetPlayerStatus().LastActionLevelPoint;
            lastActionLevelPointUpdatedAt = PlayerInfo.Instance.GetPlayerStatus().LastActionLevelPointUpdatedAt;
            remainNum = CurrentActionLevelPoint;
            txtMaxNum.text = MaxActionLevelPoint.ToString();
            UpdateRemainNum();
            StartActionLevelPointTimer();
        }

        private void OnDisable() {
            SendSkillUpValue();
        }

        private bool CheckSkillLimit(int index) {
            bool enableLvup = false;

            if(PlayerInfo.Instance.GetTutorialStep() < TutorialStep.ClearStoryStage_1_2_2) {
                enableLvup = false;
            } else {
                enableLvup = charaData.skillLvList[index] < charaData.lv ? true : false;
            }
            return enableLvup;
        }

        private void StartActionLevelPointTimer() {
            txtRecoverTime.gameObject.SetActive(RecoverTime != null);
            actionLevelPointTimer = Observable.Interval(TimeSpan.FromSeconds(1)).Scan(CurrentActionLevelPoint, (prev, _) => {
                if(prev != CurrentActionLevelPoint) {
                    remainNum++;
                    UpdateRemainNum();
                }
                if(RecoverTime != null) {
                    var m = RecoverTime.Value.Minutes < 10 ? "0" + RecoverTime.Value.Minutes : RecoverTime.Value.Minutes.ToString();
                    var s = RecoverTime.Value.Seconds < 10 ? "0" + RecoverTime.Value.Seconds : RecoverTime.Value.Seconds.ToString();
                    txtRecoverTime.text = m + ":" + s;
                } else {
                    actionLevelPointTimer.Dispose();
                }
                return CurrentActionLevelPoint;
            }).Subscribe().AddTo(this);
        }

        private void UpdateRemainNum() {
            if(remainNum >= MaxActionLevelPoint) {
                txtRecoverTime.gameObject.SetActive(false);
            }
            if (remainNum == 0) {
                btnRecover.gameObject.SetActive(true);
            } else {
                btnRecover.gameObject.SetActive(false);
            }
            txtRemainNum.text = remainNum.ToString();
        }

        private void SendSkillUpValue() {
            var sendNum = 0;

            for(int i = 0, l = skillupValueList.Length; i < l; i++) {
                if(skillupValueList[i] > 0) {
                    var index = i + 1;
                    sendNum++;
                    if(isRecoverOpen) {processing.SetActive(true);}
                    Character.WebApiRequester.UpActionLevel(
                        charaData.playerCharacterID, (byte)index, (ushort)skillupValueList[index - 1]
                    ).Do(res => {
                        sendNum--;
                        if(sendNum == 0 && isRecoverOpen) {
                            isRecoverOpen = false;
                            processing.SetActive(false);
                            actionLevelPointRecover.Open(MaxActionLevelPoint);
                        }
                        if(sendNum == 0) {
                            if(RecoverTime == null) {
                                var minutes = (int)Noroshi.Core.Game.Player.Constant.ACTION_LEVEL_POINT_RECOVERY_TIME_SPAN.Minutes;
                                var m = minutes < 10 ? "0" + minutes : minutes.ToString();
                                var seconds = (int)Noroshi.Core.Game.Player.Constant.ACTION_LEVEL_POINT_RECOVERY_TIME_SPAN.Seconds;
                                var s = seconds < 10 ? "0" + seconds : seconds.ToString();
                                txtRecoverTime.text = m + ":" + s;
                                lastActionLevelPoint = res.PlayerStatus.LastActionLevelPoint;
                                lastActionLevelPointUpdatedAt = res.PlayerStatus.LastActionLevelPointUpdatedAt;
                                StartActionLevelPointTimer();
                            } else {
                                lastActionLevelPoint = res.PlayerStatus.LastActionLevelPoint;
                                lastActionLevelPointUpdatedAt = res.PlayerStatus.LastActionLevelPointUpdatedAt;
                            }
                        }
                    }).Subscribe();
                }
            }
            if(sendNum == 0 && isRecoverOpen) {
                isRecoverOpen = false;
                actionLevelPointRecover.Open(MaxActionLevelPoint);
            }

            skillupValueList = new int[5]{0, 0, 0, 0, 0};
        }

        public void SetData(CharacterPanel.CharaData data) {
            var panelCount = 0;
            SendSkillUpValue();
            charaData = data;

            skillDescriptionWrapper.SetActive(false);
            for(int i = 0, l = data.skillLvList.Count; i < l; i++) {
                bool isEnable = data.promotionLv >= Noroshi.Core.Game.Character.Constant.PROMOTION_RANK_MAP[(byte)(i + 1)] ? true : false;
                if(i > data.skillTextList.Count - 1) {
                    skillPanelList[i].gameObject.SetActive(false);
                } else {
                    panelCount++;
                    skillPanelList[i].SetSkillPanel(charaData.characterID, isEnable, data.skillTextList[i]);
                    skillPanelList[i].UpdateLevel(data.skillLvList[i], actionLevelUpPaymentList[data.skillLvList[i] - 1].Gold);
                    skillPanelList[i].SetActiveLevelUp(CheckSkillLimit(i));
                    skillPanelList[i].gameObject.SetActive(true);
                }
            }
            skillPanelWrapper.sizeDelta = new Vector2(570, panelCount * 120 + 110);
            scrollContent.verticalNormalizedPosition = 1;
        }

        public void OnSkillScroll() {
            foreach(var panel in skillPanelList) {
                var posX = panel.transform.position.y;
                var pos = panel.transform.localPosition;
                pos.x = 310 - posX * posX * 12;
                panel.transform.localPosition = pos;
            }
        }
    }
}
