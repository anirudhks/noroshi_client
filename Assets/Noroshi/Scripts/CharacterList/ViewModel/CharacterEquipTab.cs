using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using DG.Tweening;
using Noroshi.Game;
using Noroshi.UI;
using Noroshi.Core.Game.Player;

namespace Noroshi.CharacterList {
    public class CharacterEquipTab : MonoBehaviour {
        [SerializeField] Text txtHp;
        [SerializeField] Text txtPower;
        [SerializeField] Text txtLevel;
        [SerializeField] Text txtPromotionName;
        [SerializeField] Text txtCharacterType;
        [SerializeField] Text txtEvolutionName;
        [SerializeField] Image[] starList;
        [SerializeField] Sprite spriteStarOn;
        [SerializeField] Sprite spriteStarOff;
        [SerializeField] Image imgPromotionFrame;
        [SerializeField] Image imgEmblem;
        [SerializeField] Sprite[] spritePromotionFrameList;
        [SerializeField] Sprite[] spriteEmblemList;
        [SerializeField] Text txtHaveSoul;
        [SerializeField] Text txtNeedSoul;
        [SerializeField] GameObject soulNumWrapper;
        [SerializeField] GameObject fullEvolutionTxt;
        [SerializeField] GameObject soulBar;
        [SerializeField] BtnCommon btnGetSoul;
        [SerializeField] GameObject expBar;
        [SerializeField] BtnCommon btnExpUp;
        [SerializeField] ExpUpPanel expUpPanel;
        [SerializeField] BtnEquip[] btnEquipList;
        [SerializeField] BtnCommon btnRaisePromotionLv;
        [SerializeField] GearDetail gearDetail;
        [SerializeField] CharacterListConfirm promotionConfirm;
        [SerializeField] GameObject newSkillPopUP;
        [SerializeField] Text txtNewSkillName;
        [SerializeField] ParticleSystem promotionUpParticle;
        [SerializeField] GameObject processing;

        public Subject<CharacterPanel.CharaData> OnClickedGetSoul = new Subject<CharacterPanel.CharaData>();
        public Subject<uint> OnEquip = new Subject<uint>();
        public Subject<uint> OnRaisePromotionLv = new Subject<uint>();
        public Subject<uint> OnChangeExp = new Subject<uint>();
        public Subject<bool> OnCreate = new Subject<bool>();

        private CharacterPanel.CharaData charaData;
        private Dictionary<string, Core.WebApi.Response.Master.Gear> detailGearList;
        private int selectedIndex;
        private float soulBarWidth;
        private int tempLv;
        private int needExp;
        private float expBarWidth;

        private void Start() {
            soulBarWidth = soulBar.GetComponent<RectTransform>().sizeDelta.x;
            expBarWidth = expBar.GetComponent<RectTransform>().sizeDelta.x;

            foreach (var btn in btnEquipList) {
                btn.OnClickedBtn.Subscribe(index => {
                    var data = detailGearList.ContainsKey("gear" + index) ?
                        detailGearList["gear" + index] : null;
                    selectedIndex = index;
                    UILoading.Instance.SetQuery(QueryKeys.SelectedEquipIndex, index);
                    gearDetail.OpenGearDetail(data, charaData.gearList[index], btnEquipList[index].equipState, charaData.lv);
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnGetSoul.OnClickedBtn.Subscribe(_ => {
                UILoading.Instance.SetQuery(QueryKeys.IsOpenGetSoulPanel, 1);
                OnClickedGetSoul.OnNext(charaData);
            });
            btnGetSoul.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            gearDetail.OnEquip.Subscribe(gearID => {
                processing.SetActive(true);
                Character.WebApiRequester.EquipGear(
                    charaData.playerCharacterID, (byte)(selectedIndex + 1)
                ).Do(res => {
                    foreach(var gear in res.PlayerCharacter.EquippedGears) {
                        if(gear.No - 1 == selectedIndex) {
                            charaData.gearList[selectedIndex] = gear;
                            break;
                        }
                    }
                    btnEquipList[selectedIndex].SetEquip();
                    UpdateCharaData(res.PlayerCharacter);
                    OnEquip.OnNext(charaData.characterID);
                    processing.SetActive(false);
                }).Subscribe();
            });

            gearDetail.OnCreate.Subscribe(id => {
                processing.SetActive(true);
                Gear.WebApiRequester.CraftGear(id).Do(_ => {
                    SetEquipList();
                    OnCreate.OnNext(true);
                    processing.SetActive(false);
                }).Subscribe();
            });

            gearDetail.OnEnchant.Subscribe(playerGear => {
                GlobalContainer.RepositoryManager.PlayerCharacterRepository.GetAll().Do(list => {
                    foreach(var chara in list) {
                        if(chara.CharacterID == charaData.characterID) {
                            charaData.gearList[selectedIndex] = playerGear;
                            UpdateCharaData(chara);
                            OnEquip.OnNext(charaData.characterID);
                            break;
                        }
                    }
                }).Subscribe();
            });

            btnRaisePromotionLv.OnClickedBtn.Subscribe(_ => {
                promotionConfirm.OpenConfirm(charaData, 0);
            });
            btnRaisePromotionLv.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            promotionConfirm.OnDecide.Subscribe(_ => {
                processing.SetActive(true);
                Character.WebApiRequester.UpPromotionLevel(charaData.playerCharacterID).Do(res => {
                    charaData.gearList = new Noroshi.Core.WebApi.Response.Character.EquippedGear[Noroshi.Core.Game.Character.Constant.MAX_EQUIPPABLE_GEAR_NUM];
                    UpdateCharaData(res.PlayerCharacter);
                    btnRaisePromotionLv.SetEnable(false);
                    OnRaisePromotionLv.OnNext(charaData.characterID);
                    processing.SetActive(false);
                    promotionUpParticle.gameObject.SetActive(false);
                    promotionUpParticle.gameObject.SetActive(true);
                    CheckNewSkill();
                }).Subscribe();
            });
            promotionConfirm.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.UPGRADE);
            });

            expUpPanel.OnRaiseExp.Subscribe(value => {
                charaData.exp += value;
                tempLv = charaData.lv;
                CheckLevelUp();
            });
            expUpPanel.OnSendExp.Subscribe(SendUseDrug);

            StartCoroutine(CheckTutorialStep());
        }

        private IEnumerator CheckTutorialStep() {
            while(!PlayerInfo.Instance.isLoad) {
                yield return new WaitForEndOfFrame();
            }
            if(PlayerInfo.Instance.GetTutorialStep() >= Noroshi.Core.Game.Player.TutorialStep.ClearStoryStage_1_3_4) {
                btnExpUp.OnClickedBtn.Subscribe(_ => {
                    expUpPanel.OpenExpUpPanel(charaData.characterID);
                    if((charaData.lv > PlayerInfo.Instance.PlayerLevel) ||
                        (charaData.lv == PlayerInfo.Instance.PlayerLevel && charaData.exp >= needExp - 1)) {
                        expUpPanel.SetEnableExpUpPanel(false);
                    } else {
                        expUpPanel.SetEnableExpUpPanel(true);
                    }
                });
                btnExpUp.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            } else {
                btnExpUp.SetEnable(false);
            }
        }

        private void OnDisable() {
            promotionUpParticle.gameObject.SetActive(false);
            newSkillPopUP.SetActive(false);
        }

        private void CheckNewSkill() {
            var gclm = GlobalContainer.LocalizationManager;
            if(charaData.promotionLv == Noroshi.Core.Game.Character.Constant.PROMOTION_RANK_MAP[2]) {
                txtNewSkillName.text = gclm.GetText(charaData.skillTextList[1] + ".Name");
            } else if(charaData.promotionLv == Noroshi.Core.Game.Character.Constant.PROMOTION_RANK_MAP[3]) {
                txtNewSkillName.text = gclm.GetText(charaData.skillTextList[2] + ".Name");
            } else if(charaData.promotionLv == Noroshi.Core.Game.Character.Constant.PROMOTION_RANK_MAP[4]) {
                txtNewSkillName.text = gclm.GetText(charaData.skillTextList[3] + ".Name");
            } else if(charaData.promotionLv == Noroshi.Core.Game.Character.Constant.PROMOTION_RANK_MAP[5]) {
                txtNewSkillName.text = gclm.GetText(charaData.skillTextList[4] + ".Name");
            } else {
                return;
            }
            newSkillPopUP.SetActive(true);
            TweenA.Add(newSkillPopUP, 0.1f, 1).From(0).Then(() => {
                TweenA.Add(newSkillPopUP, 0.3f, 0).Delay(5.0f).Then(() => {
                    newSkillPopUP.SetActive(false);
                });
            });
        }

        private bool CheckCreateGear(uint id) {
            var recipes = ItemListManager.Instance.GetGearRecipe(id);
            var isCreate = true;
            if(recipes.Count < 1) {
                isCreate = false;
            } else {
                foreach(var recipe in recipes) {
                    if(recipe["num"] > ItemListManager.Instance.GetItemCount(recipe["id"])) {
                        if(ItemListManager.Instance.GetGearRecipe(recipe["id"]) != null) {
                            isCreate = CheckCreateGear(recipe["id"]);
                            if(!isCreate) {break;}
                        } else {
                            isCreate = false;
                            break;
                        }
                    }
                }
            }
            return isCreate;
        }

        private void SetEquipList() {
            var isPromotion = true;

            detailGearList = new Dictionary<string, Core.WebApi.Response.Master.Gear>();
            if(charaData.promotionLv >= Noroshi.Core.Game.Character.Constant.MAX_PROMOTION_LEVEL) {isPromotion = false;}

            for(int i = 0, l = btnEquipList.Length; i < l; i++) {
                int index = i;
                if(charaData.gearList[index] != null) {
                    btnEquipList[index].SetGearImage(charaData.gearList[index].GearID, 1, charaData.gearList[index].Level);
                    detailGearList["gear" + index] = ItemListManager.Instance.GetGearInfo(charaData.gearList[index].GearID);
                } else {
                    isPromotion = false;
                    var masterData = GlobalContainer.MasterManager.CharacterMaster.Get(charaData.characterID);
                    var gID = masterData.GearIDs[charaData.promotionLv - 1][index];

                    detailGearList["gear" + index] = ItemListManager.Instance.GetGearInfo(gID);
                    if(ItemListManager.Instance.GetItemCount(gID) > 0) { // have
                        if(charaData.lv >= ItemListManager.Instance.GetGearInfo(gID).Level) { // lv tariteru
                            btnEquipList[index].SetGearImage(gID, -2);
                        } else {
                            btnEquipList[index].SetGearImage(gID, -1);
                        }
                    } else {
                        if(CheckCreateGear(gID)) { // can create
                            if(charaData.lv >= ItemListManager.Instance.GetGearInfo(gID).Level) { 
                                btnEquipList[index].SetGearImage(gID, -3);
                            } else {
                                btnEquipList[index].SetGearImage(gID, -4);
                            }
                        } else {
                            btnEquipList[index].SetGearImage(gID, 0);
                        }
                    }
                }
            }
            if(isPromotion && PlayerInfo.Instance.GetTutorialStep() >= TutorialStep.ClearStoryStage_1_2_2) {
                btnRaisePromotionLv.SetEnable(true);
            } else {
                btnRaisePromotionLv.SetEnable(false);
            }
        }

        private void SetEvolution() {
            txtHaveSoul.text = charaData.haveSoul.ToString();
            txtNeedSoul.text = charaData.needSoul.ToString();
            if(charaData.needSoul == 0) {
                soulBar.transform.localPosition = Vector3.zero;
                soulNumWrapper.SetActive(false);
                fullEvolutionTxt.SetActive(true);
            } else {
                var xx = soulBarWidth - soulBarWidth * (float)charaData.haveSoul / (float)charaData.needSoul;
                soulNumWrapper.SetActive(true);
                fullEvolutionTxt.SetActive(false);
                soulBar.transform.localPosition = new Vector3(-xx, 0, 0);
            }
            for(int i = 0, l = starList.Length; i < l; i++) {
                if(i < charaData.evolutionLv) {
                    starList[i].sprite = spriteStarOn;
                } else {
                    starList[i].sprite = spriteStarOff;
                }
            }
        }

        private void CheckLevelUp() {
            var characterLevel = GlobalContainer.MasterManager.LevelMaster.GetCharacterLevelByLevel((ushort)charaData.lv);
            needExp = (int)characterLevel.Exp;
            if(charaData.exp >= needExp) {
                if(charaData.lv >= PlayerInfo.Instance.PlayerLevel) {
                    expUpPanel.SetEnableExpUpPanel(false);
                    charaData.exp = needExp - 1;
                    UpdateExpBar();
                } else {
                    charaData.exp -= needExp;
                    charaData.lv++;
                    CheckLevelUp();
                    SoundController.Instance.PlaySE(SoundController.SEKeys.LEVEL_UP);
                }
            } else {
                UpdateExpBar();
            }
        }

        private void UpdateExpBar() {
            expBar.transform.DOKill();
            if(charaData.lv - tempLv > 0) {
                expBar.transform.DOLocalMoveX(0, 0.1f).SetEase(Ease.Linear).OnComplete(() => {
                    tempLv++;
                    txtLevel.text = tempLv.ToString();
                    expBar.transform.localPosition = new Vector3(-expBarWidth, 0, 0);
                    UpdateExpBar();
                });
            } else {
                var xx = expBarWidth - expBarWidth * (float)charaData.exp / (float)needExp;
                txtLevel.text = charaData.lv.ToString();
                expBar.transform.DOLocalMoveX(-xx, 0.2f).SetEase(Ease.Linear);
            }
        }

        private void SendUseDrug(Dictionary<string, int> data) {
            processing.SetActive(true);
            Character.WebApiRequester.Enhance(
                charaData.playerCharacterID, (uint)data["itemId"], (ushort)data["useNum"]
            ).Do(res => {
                UpdateCharaData(res.PlayerCharacter);
                OnChangeExp.OnNext(charaData.characterID);
                processing.SetActive(false);
            }).Subscribe();
        }

        private void SetCharacterType() {
            var gclm = GlobalContainer.LocalizationManager;
            var positionText = "";
            var typeText = "";
            var tribeText = "";

            imgPromotionFrame.sprite = spritePromotionFrameList[charaData.promotionLv - 1];
            txtEvolutionName.text = gclm.GetText("UI.Noun.EvolutionLevel" + charaData.evolutionLv);
            txtPromotionName.text = gclm.GetText("UI.Noun.PromotionLevel" + charaData.promotionLv);
            if(charaData.position == 1) {
                positionText = gclm.GetText("UI.Noun.PositionFront");
            } else if(charaData.position == 2) {
                positionText = gclm.GetText("UI.Noun.PositionCentral");
            } else {
                positionText = gclm.GetText("UI.Noun.PositionBack");
            }
            if(charaData.type == 1) {
                typeText = gclm.GetText("UI.Noun.TypeStrength");
            } else if(charaData.type == 2) {
                typeText = gclm.GetText("UI.Noun.TypeIntellect");
            } else {
                typeText = gclm.GetText("UI.Noun.TypeAgility");
            }
            if(charaData.tribe == 1) {
                tribeText = gclm.GetText("UI.Noun.Human");
                imgEmblem.sprite = spriteEmblemList[0];
            } else if(charaData.tribe == 2) {
                tribeText = gclm.GetText("UI.Noun.Demon");
                imgEmblem.sprite = spriteEmblemList[1];
            } else {
                tribeText = gclm.GetText("UI.Noun.Ham");
                imgEmblem.sprite = spriteEmblemList[2];
            }
            txtCharacterType.text = positionText + " / " + typeText + " / " + tribeText;
        }

        private void UpdateCharaData(Noroshi.Core.WebApi.Response.PlayerCharacter character) {
            var status = new Game.CharacterStatus(character, GlobalContainer.MasterManager.CharacterMaster.Get(character.CharacterID));
            charaData.status = status;
            charaData.lv = status.Level;
            charaData.hp = (int)status.MaxHP;
            charaData.power = (int)(status.PhysicalAttack + status.MagicPower + status.PhysicalCrit + status.MagicCrit);
            charaData.evolutionLv = status.EvolutionLevel;
            charaData.promotionLv = status.PromotionLevel;
            BattleCharacterSelect.Instance.ReloadCharacter(character.CharacterID, status);
        }

        public void SetData(CharacterPanel.CharaData data) {
            var characterLevel = GlobalContainer.MasterManager.LevelMaster.GetCharacterLevelByLevel((ushort)data.lv);
            float xx = 0;

            charaData = data;

            txtLevel.text = charaData.lv.ToString();
            txtHp.text = charaData.hp.ToString();
            txtPower.text = charaData.power.ToString();
            needExp = (int)characterLevel.Exp;
            xx = expBarWidth - expBarWidth * (float)data.exp / (float)needExp;
            expBar.transform.localPosition = new Vector3(-xx, 0, 0);

            SetCharacterType();
            SetEquipList();
            SetEvolution();
        }

        public void OpenDefaultGearPanel(int index) {
            var ids = UILoading.Instance.GetMultiQuery(QueryKeys.SelectedEquipGearList);
            btnEquipList[index].OnClickedBtn.OnNext(index);
            if(ids.Count > 0) {
                gearDetail.SetDefault(ids);
            }
        }

        public void OpenDefaultGetSoulPanel() {
            btnGetSoul.OnClickedBtn.OnNext(1);
        }
    }
}
