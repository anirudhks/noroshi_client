using UnityEngine;
using System.Collections.Generic;
using Noroshi.Game;
using UniLinq;
using UniRx;
using UnityEngine.UI;
using DG.Tweening;
using Noroshi.UI;
using Noroshi.Random;

namespace Noroshi.CharacterList {
    public class CharacterDetailPanel : MonoBehaviour {
        [SerializeField] BtnCommon[] tabBtnList;
        [SerializeField] GameObject tabBtnContainer;
        [SerializeField] GameObject[] tabContentList;
        [SerializeField] GameObject abilityContainer;
        [SerializeField] GameObject charaImageContainer;
        [SerializeField] Text txtName;
        [SerializeField] Text txtPopularName;
        [SerializeField] BtnCommon btnClose;
        [SerializeField] BtnCommon[] btnSwitch;
        [SerializeField] CharacterSlider characterSlider;
        [SerializeField] GameObject processing;

        public Subject<int> OnSwitchDetail = new Subject<int>();
        public Subject<bool> OnCloseDetail = new Subject<bool>();

        private CharacterPanel.CharaData charaData;
        private Dictionary<string, UICharacter> charaImageList = new Dictionary<string, UICharacter>();
        private float characterSize = 35;
        private UICharacter currentCharacter;
        private UICharacter leaveCharacter;
        private float currentPoint = -1000;
        private float leavePoint;
        private float currentPrevPoint;
        private float leavePrevPoint;
        private float currentOffsetY;
        private float leaveOffsetY;
        private bool isSliding = false;
        private bool canDrag = false;
        private Flaggs.Unity.Spine.SkeletonAnimationUnloader unloader = new Flaggs.Unity.Spine.SkeletonAnimationUnloader();


        public void Init() {
            GlobalContainer.SetFactory<IRandomGenerator>(() => new RandomGenerator());
            foreach(var tab in tabBtnList) {
                tab.OnClickedBtn.Subscribe(index => {
                    OpenTab(index);
                });
                tab.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnClose.OnClickedBtn.Subscribe(_ => {
                UILoading.Instance.RemoveQuery(QueryKeys.SelectedCharacter);
                CloseCharacterDetail();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            foreach(var btn in btnSwitch) {
                btn.OnClickedBtn.Subscribe(direction => {
                    if(isSliding) {return;}
                    isSliding = true;
                    currentPoint = 0;
                    if(direction < 0) {
                        SlideCharacter(1);
                    } else {
                        SlideCharacter(-1);
                    }
                });
                btn.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.FLICK);
                });
            }

            characterSlider.OnTapCharacter.Subscribe(vec2 => {
                if(isSliding) {return;}
                var diffX = vec2.x - charaImageContainer.transform.localPosition.x - Constant.SCREEN_BASE_WIDTH / 2;
                var diffY = vec2.y - charaImageContainer.transform.localPosition.y - Constant.SCREEN_BASE_HEIGHT / 2;
                if(diffX > -120 && diffX < 120 && diffY > 50 && diffY < 370) {
                    PlayCharacterAnimation();
                }
            });

            characterSlider.OnDragCharacter.Subscribe(posX => {
                if(isSliding || !canDrag) {return;}
                currentPoint = posX;
                MoveCharacter(currentCharacter, currentPoint, true);
            });

            characterSlider.OnSlideStart.Subscribe(value => {
                currentPrevPoint = currentPoint;
            });

            characterSlider.OnSlideEnd.Subscribe(value => {
                if(isSliding || !canDrag) {return;}
                if(value < -120) {
                    SlideCharacter(1);
                    SoundController.Instance.PlaySE(SoundController.SEKeys.FLICK);
                } else if(value > 120) {
                    SlideCharacter(-1);
                    SoundController.Instance.PlaySE(SoundController.SEKeys.FLICK);
                } else {
                    DOTween.To(() => currentPoint, (a) => currentPoint = a, 0, 0.3f)
                        .OnUpdate(() => {MoveCharacter(currentCharacter, currentPoint, true);})
                        .OnComplete(() => {
                            currentCharacter.transform.localScale = new Vector2(-characterSize, characterSize);
                        });
                }
            });

            OpenTab(0);
        }

        private void OpenTab(int index) {
            for(int i = 0, l = tabContentList.Length; i < l; i++) {
                tabBtnList[i].SetSelect(i == index);
                tabContentList[i].SetActive(i == index);
            }
        }

        private void SlideCharacter(int direction) {
            var leaveCharacterID = charaData.characterID;
            isSliding = true;
            leavePoint = leavePrevPoint = currentPoint;
            leaveCharacter = currentCharacter;
            leaveOffsetY = currentOffsetY;
            leaveCharacter.PlayRunAnimation();
            DOTween.To(() => leavePoint, (a) => leavePoint = a, -1000 * direction, 0.4f)
                .SetEase(Ease.Linear)
                .OnUpdate(() => {MoveCharacter(leaveCharacter, leavePoint, false);})
                .OnComplete(() => {
                    DestroyCharacter(leaveCharacter, leaveCharacterID);
                });
            currentPoint = 1000 * direction;
            currentPrevPoint = currentPoint;
            OnSwitchDetail.OnNext(1 * direction);
        }

        private void MoveCharacter(UICharacter chara, float crntPos, bool isCome) {
            var offsetY = isCome ? currentOffsetY : leaveOffsetY;
            var posY = 0.00015f * crntPos * crntPos + 50 + offsetY;
            var ratio = crntPos * 0.015f;
            var prevPos = isCome ? currentPrevPoint : leavePrevPoint;
            var c = crntPos > 0 ? 1 - crntPos * 0.001f : 1 + crntPos * 0.001f;
            if(c < 0) {c = 0;}
            chara.transform.localPosition = new Vector2(crntPos, posY);
            if(crntPos != prevPos && Mathf.Abs(crntPos) > 5) {
                if(crntPos - prevPos <= 0) {
                    if(ratio < 0) {
                        chara.transform.localScale = new Vector2(characterSize + ratio, characterSize + ratio);
                    } else {
                        chara.transform.localScale = new Vector2(characterSize - ratio, characterSize - ratio);
                    }
                } else {
                    if(ratio < 0) {
                        chara.transform.localScale = new Vector2(-characterSize - ratio, characterSize + ratio);
                    } else {
                        chara.transform.localScale = new Vector2(-characterSize + ratio, characterSize - ratio);
                    }
                }
            }
            chara.SetCharacterColor(new Color(c, c, c));
            if(isCome) {
                currentPrevPoint = crntPos;
            } else {
                leavePrevPoint = crntPos;
            }
        }

        private void SetCurrentCharacter() {
            var mesh = currentCharacter.GetComponent<MeshRenderer>();
            currentCharacter.transform.localScale = new Vector2(-characterSize, characterSize);
            currentCharacter.transform.localPosition = new Vector2(-1000, 0);
            currentCharacter.gameObject.SetActive(true);
            Observable.TimerFrame(1).Subscribe(_ => {
                currentOffsetY = mesh.bounds.size.y > 2.5f ? 0 : -59 * mesh.bounds.size.y + 147;
                currentCharacter.PlayRunAnimation();
            });
            DOTween.To(() => currentPoint, (a) => currentPoint = a, 0, 0.8f)
                .SetEase(Ease.Linear)
                .OnUpdate(() => {MoveCharacter(currentCharacter, currentPoint, true);})
                .OnComplete(() => {
                    isSliding = false;
                    currentCharacter.PlayIdleAnimation();
                    currentCharacter.transform.localScale = new Vector2(-characterSize, characterSize);
                });
        }

        public void DestroyCharacter(UICharacter character, uint characterID) {
            charaImageList.Remove("chara" + characterID);
            unloader.UnloadTextureAndDestroy(character.gameObject);
        }

        public void CreateCharaImage(CharacterStatus status) {
            var chara = Instantiate<GameObject>(GlobalContainer.AssetBundleManager.LoadFromCharacterPrefab(status.CharacterID, "UICharacter"));
            var skeletonAnimation = chara.GetComponent<SkeletonAnimation>();

            UICharacter uiCharacter = chara.AddComponent<UICharacter>();
            uiCharacter.transform.SetParent(charaImageContainer.transform);
            uiCharacter.transform.localScale = new Vector2(-characterSize, characterSize);
            uiCharacter.transform.localPosition = new Vector2(-1000, 0);
            foreach(var atlas in skeletonAnimation.skeletonDataAsset.atlasAssets) {
                foreach(var m in atlas.materials) {
                    var shader = m.shader;
                    m.shader = null;
                    m.shader = shader;
                }
            }
            uiCharacter.SetCharacterSkin(status.EvolutionLevel, status.TagSet.IsDeca);
            uiCharacter.SetAnimationSounds(status.AnimationSounds);
            charaImageList["chara" + status.CharacterID] = uiCharacter;
            uiCharacter.gameObject.SetActive(false);
            processing.SetActive(false);
        }
        
        public void PlayCharacterAnimation(int index = -1) {
            currentCharacter.PlayActionAnimation(index);
        }

        public void SetCharacterSkin(int evolutionLevel) {
            currentCharacter.SetCharacterSkin(evolutionLevel, charaData.isDeca);
        }

        public void OpenCharacterDetail(CharacterPanel.CharaData data, bool canSwitch) {
            var gclm = GlobalContainer.LocalizationManager;
            if(!charaImageList.ContainsKey("chara" + data.characterID)) {
                processing.SetActive(true);
                Observable.TimerFrame(1).Subscribe(_ => {
                    CreateCharaImage(data.status);
                    currentCharacter = charaImageList["chara" + data.characterID];
                });
            } else {
                currentCharacter = charaImageList["chara" + data.characterID];
            }
            charaData = data;
            txtName.text = gclm.GetText(charaData.textkey + ".Name");
            txtPopularName.text = gclm.GetText(charaData.textkey + ".Title." + charaData.evolutionLv);

            foreach(var btn in btnSwitch) {
                btn.gameObject.SetActive(canSwitch);
            }
            canDrag = canSwitch;
            gameObject.SetActive(true);
            gameObject.PauseTweens();
            isSliding = true;
            TweenA.Add(gameObject, 0.15f, 1).Then(() => {
                Observable.TimerFrame(1).Subscribe(_ => {
                    if(!currentCharacter.gameObject.activeSelf) {
                        SetCurrentCharacter();
                    } else {
                        isSliding = false;
                    }
                });
            });
        }

        public void CloseCharacterDetail() {
            OnCloseDetail.OnNext(true);
            if(currentCharacter != null) {
                DestroyCharacter(currentCharacter, charaData.characterID);
            }
            DOTween.KillAll();
            TweenA.Add(gameObject, 0.3f, 0).Then(() => {
                currentPoint = currentPrevPoint = -1000;
                leavePoint = leavePrevPoint = -1000;
                OpenTab(0);
                gameObject.SetActive(false);
            });
        }
    }
}