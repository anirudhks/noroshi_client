using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.UI;
using Noroshi.Game;
using Noroshi.Core.WebApi.Response;
using Noroshi.Core.WebApi.Response.Character;

namespace Noroshi.CharacterList {
    public class CharacterPanel : MonoBehaviour {
        [SerializeField] Image imgCharacterIcon;
        [SerializeField] Image imgCharacterFrame;
        [SerializeField] Image imgPanelFrame;
        [SerializeField] Image imgEmblem;
        [SerializeField] Sprite[] spriteCharaFrameList;
        [SerializeField] Sprite[] spritePanelFrameList;
        [SerializeField] Sprite spriteUnAcquiredFrame;
        [SerializeField] Text txtCharacterName;
        [SerializeField] Text txtLevel;
        [SerializeField] Text txtHaveStone;
        [SerializeField] Text txtNeedStone;
        [SerializeField] GameObject haveSoulWrapper;
        [SerializeField] GameObject levelTxtWrapper;
        [SerializeField] GameObject fullEvolutionTxt;
        [SerializeField] GameObject soulBar;
        [SerializeField] GameObject[] evolutionStar;
        [SerializeField] Image[] gearList;
        [SerializeField] Image[] gearFrameList;
        [SerializeField] Sprite[] gearStateList;
        [SerializeField] GameObject equipContainer;
        [SerializeField] GameObject evolutionContainer;
        [SerializeField] GameObject evolutionTxtContainer;
        [SerializeField] GameObject iconBig;
        [SerializeField] GameObject iconAttention;
        [SerializeField] Material grayScale;
        [SerializeField] Sprite[] spriteGearFrameList;
        [SerializeField] Sprite[] spriteEmblemList;

        public class CharaData {
            public CharacterStatus status;
            public string textkey;
            public uint characterID;
            public uint playerCharacterID;
            public int lv;
            public int hp;
            public int power;
            public int exp;
            public int position;
            public ushort evolutionType;
            public int evolutionLv;
            public int promotionLv;
            public byte skinLv;
            public uint soulID;
            public int haveSoul;
            public int needSoul;
            public bool isDeca;
            public int tribe;
            public int type;
            public List<ushort> skillLvList = new List<ushort>();
            public List<string> skillTextList = new List<string>();
            public EquippedGear[] gearList = new EquippedGear[Noroshi.Core.Game.Character.Constant.MAX_EQUIPPABLE_GEAR_NUM];
        }

        public Subject<CharacterStatus> OnChangedCharacterModel = new Subject<CharacterStatus>();
        public Subject<CharaData> OnOpenDetail = new Subject<CharaData>();
        public Subject<CharaData> OnOpenGetSoul = new Subject<CharaData>();

        public CharaData charaData = new CharaData();

        private float soulBarWidth;

        private void DrawCharacterPanel() {
            txtCharacterName.text = GlobalContainer.LocalizationManager.GetText(charaData.textkey + ".Name");
            imgCharacterIcon.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite>(charaData.characterID, string.Format("thumb_{0}", charaData.skinLv));
            if(charaData.promotionLv > 0) {
                imgCharacterFrame.sprite = spriteCharaFrameList[charaData.promotionLv - 1];
                imgPanelFrame.sprite = spritePanelFrameList[charaData.promotionLv - 1];
            } else {
                imgPanelFrame.sprite = spriteUnAcquiredFrame;
            }
            if(charaData.tribe == 1) {
                imgEmblem.sprite = spriteEmblemList[0];
            } else if(charaData.tribe == 2) {
                imgEmblem.sprite = spriteEmblemList[1];
            } else {
                imgEmblem.sprite = spriteEmblemList[2];
            }
            if(charaData.lv > 0) {
                txtLevel.text = charaData.lv.ToString();
                SetCharacterEvolutionLv();
                UPdateGear();
            } else {
                levelTxtWrapper.SetActive(false);
                SetUnAcquiredEvolutionLv();
            }
            iconBig.SetActive(charaData.isDeca);
        }

        private void SetCharacterGear(EquippedGear gear, int index) {
            var gcmm = GlobalContainer.MasterManager;
            if(gear != null) {
                var gearData = gcmm.ItemMaster.GetGear(gear.GearID);
                gearList[index].sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(gear.GearID);
                gearFrameList[index].sprite = spriteGearFrameList[gearData.Rarity - 1];
                gearFrameList[index].gameObject.SetActive(true);
            } else {
                var masterData = gcmm.CharacterMaster.Get(charaData.characterID);
                var gID = masterData.GearIDs[charaData.promotionLv - 1][index];

                gearFrameList[index].gameObject.SetActive(false);
                if(ItemListManager.Instance.GetItemCount(gID) > 0) { // have
                    if(charaData.lv >= ItemListManager.Instance.GetGearInfo(gID).Level) { // lv tariteru
                        iconAttention.SetActive(true);
                        gearList[index].sprite = gearStateList[0];
                    } else {
                        gearList[index].sprite = gearStateList[2];
                    }
                } else {
                    if(CheckCreateGear(gID)) { // can create
                        if(charaData.lv >= ItemListManager.Instance.GetGearInfo(gID).Level) {
                            gearList[index].sprite = gearStateList[1];
                            iconAttention.SetActive(true);
                        } else {
                            gearList[index].sprite = gearStateList[2];
                        }
                    } else {
                        gearList[index].sprite = gearStateList[2];
                    }
                }
            }
        }

        private bool CheckCreateGear(uint id) {
            var recipes = ItemListManager.Instance.GetGearRecipe(id);
            var isCreate = true;
            if(recipes.Count < 1) {
                isCreate = false;
            } else {
                foreach(var recipe in recipes) {
                    if(recipe["num"] > ItemListManager.Instance.GetItemCount(recipe["id"])) {
                        if(ItemListManager.Instance.GetGearRecipe(recipe["id"]) != null) {
                            isCreate = CheckCreateGear(recipe["id"]);
                            if(!isCreate) {break;}
                        } else {
                            isCreate = false;
                            break;
                        }
                    }
                }
            }
            return isCreate;
        }

        private void SetCharacterEvolutionLv() {
            var characterEvolutionType = GlobalContainer.MasterManager.CharacterMaster.GetCharacterEvolutionType(charaData.evolutionType, (byte)(charaData.evolutionLv + 1));
            charaData.needSoul = characterEvolutionType != null ? (int)characterEvolutionType.Soul : 0;
            txtHaveStone.text = charaData.haveSoul.ToString();
            txtNeedStone.text = charaData.needSoul.ToString();
            if(charaData.needSoul == 0) {
                soulBar.transform.localPosition = Vector3.zero;
                haveSoulWrapper.SetActive(false);
                fullEvolutionTxt.SetActive(true);
            } else {
                var xx = soulBarWidth - soulBarWidth * (float)charaData.haveSoul / (float)charaData.needSoul;
                soulBar.transform.localPosition = new Vector3(-xx, 0, 0);
            }
            for(int i = 0, l = evolutionStar.Length; i < l; i++) {
                if(i < (int)charaData.evolutionLv) {
                    evolutionStar[i].SetActive(true);
                } else {
                    evolutionStar[i].SetActive(false);
                }
            }
        }

        private void SetUnAcquiredEvolutionLv() {
            var characterEvolutionType = GlobalContainer.MasterManager.CharacterMaster.GetCharacterEvolutionType(charaData.evolutionType, (byte)charaData.evolutionLv);
            charaData.needSoul = characterEvolutionType != null ? (int)characterEvolutionType.Soul : 0;
            txtHaveStone.text = charaData.haveSoul.ToString();
            txtNeedStone.text = charaData.needSoul.ToString();

            var xx = soulBarWidth - soulBarWidth * (float)charaData.haveSoul / (float)charaData.needSoul;
            soulBar.transform.localPosition = new Vector3(-xx, 0, 0);
        }

        public void SetCharacterPanelData(CharacterStatus characterStatus, uint id, int exp, EquippedGear[] gearList) {
            var gcmm = GlobalContainer.MasterManager;
            var masterData = gcmm.CharacterMaster.Get(characterStatus.CharacterID);
            var tagFlags = new Noroshi.Core.Game.Character.CharacterTagSet(masterData.TagFlags);

            soulBarWidth = soulBar.GetComponent<RectTransform>().sizeDelta.x;
            charaData.status = characterStatus;
            charaData.characterID = characterStatus.CharacterID;
            charaData.playerCharacterID = id;
            charaData.lv = characterStatus.Level;
            charaData.hp = (int)characterStatus.MaxHP;
            charaData.power = (int)(characterStatus.PhysicalAttack + characterStatus.MagicPower + characterStatus.PhysicalCrit + characterStatus.MagicCrit);
            charaData.exp = exp;
            charaData.position = (int)characterStatus.Position;
            charaData.evolutionLv = characterStatus.EvolutionLevel;
            charaData.promotionLv = characterStatus.PromotionLevel;
            charaData.skinLv = characterStatus.SkinLevel;
            charaData.isDeca = characterStatus.TagSet.IsDeca;
            charaData.skillLvList = characterStatus.ActionLevels.ToList();
            charaData.skillLvList.RemoveAt(0);
            charaData.skillTextList.Add((gcmm.CharacterMaster.GetAction(masterData.ActionID1)).TextKey);
            charaData.skillTextList.Add((gcmm.CharacterMaster.GetAction(masterData.ActionID2)).TextKey);
            charaData.skillTextList.Add((gcmm.CharacterMaster.GetAction(masterData.ActionID3)).TextKey);
            charaData.skillTextList.Add((gcmm.CharacterMaster.GetAction(masterData.ActionID4)).TextKey);
            if(masterData.ActionID5 > 0) {
                charaData.skillTextList.Add((gcmm.CharacterMaster.GetAction(masterData.ActionID5)).TextKey);
            }
            charaData.type = (int)characterStatus.Type;
            if(tagFlags.HasTag(9)) {
                charaData.tribe = 1;
            } else if(tagFlags.HasTag(10)) {
                charaData.tribe = 2;
            } else {
                charaData.tribe = 3;
            }

            if(gearList != null) {
                foreach(var gear in gearList) {
                    charaData.gearList[gear.No - 1] = gear;
                }
            }

            charaData.textkey = masterData.TextKey;
            charaData.evolutionType = (ushort)masterData.EvolutionType;
            var soul = GlobalContainer.MasterManager.ItemMaster.GetSoulByCharacterID(characterStatus.CharacterID);
            charaData.soulID = soul.ID;
            charaData.haveSoul = (int)ItemListManager.Instance.GetItemCount(soul.ID);
            DrawCharacterPanel();
        }

        public void SetUnAcquiredCharacter(Core.WebApi.Response.Master.Character character) {
            var soul = GlobalContainer.MasterManager.ItemMaster.GetSoulByCharacterID(character.ID);
            var flags = new Noroshi.Core.Game.Character.CharacterTagSet(character.TagFlags);

            soulBarWidth = soulBar.GetComponent<RectTransform>().sizeDelta.x;
            charaData.textkey = character.TextKey;
            charaData.characterID = character.ID;
            charaData.evolutionType = character.EvolutionType;
            charaData.evolutionLv = character.InitialEvolutionLevel;
            charaData.skinLv = 1;
            charaData.position = (int)character.Position;
            charaData.isDeca = flags.HasTag(0);

            if(flags.HasTag(9)) {
                charaData.tribe = 1;
            } else if(flags.HasTag(10)) {
                charaData.tribe = 2;
            } else {
                charaData.tribe = 3;
            }
            if(soul != null) {
                charaData.soulID = soul.ID;
                charaData.haveSoul = (int)ItemListManager.Instance.GetItemCount(soul.ID);
            }
            ChangeSort(1);
            DrawCharacterPanel();
        }

        public void OnClickedBtn() {
            OnOpenGetSoul.OnNext(charaData);
            SelectCharacter();
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }

        public void SelectCharacter() {
            OnOpenDetail.OnNext(charaData);
        }

        public void ChangeSort(int index) {
            if(index == 0) {
                equipContainer.SetActive(true);
                evolutionContainer.SetActive(false);
                evolutionTxtContainer.SetActive(false);
            } else if(index == 1) {
                equipContainer.SetActive(false);
                evolutionContainer.SetActive(true);
                evolutionTxtContainer.SetActive(true);
            }
        }

        public void UPdateGear(bool isOpen = false) {
            iconAttention.SetActive(false);
            for(var i = 0; i < charaData.gearList.Length; i++) {
                SetCharacterGear(charaData.gearList[i], i);
            }
            if(isOpen) {OnOpenDetail.OnNext(charaData);}
        }

        public void UpdateLevel() {
            txtLevel.text = charaData.lv.ToString();
            UPdateGear();
            OnOpenDetail.OnNext(charaData);
        }

        public void RaisePromotionLv() {
            imgCharacterFrame.sprite = spriteCharaFrameList[charaData.promotionLv - 1];
            imgPanelFrame.sprite = spritePanelFrameList[charaData.promotionLv - 1];
            UPdateGear(true);
        }
    }
}