﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UniRx;
using Noroshi.UI;
using Noroshi.Core.Game.Player;

namespace Noroshi.CharacterList {
    public class BtnEquip : MonoBehaviour, IPointerClickHandler {
        [SerializeField] GameObject iconLvShortage;
        [SerializeField] GameObject iconCanEquip;
        [SerializeField] GameObject iconCanCraft;
        [SerializeField] GameObject iconCanCraftLvShortage;
        [SerializeField] Material grayScale;
        [SerializeField] Image imgGear;
        [SerializeField] Image imgFrame;
        [SerializeField] Sprite[] spriteGearFrameList;
        [SerializeField] GameObject[] enchantStarList;
        [SerializeField] ParticleSystem equipParticle;

        public Subject<int> OnClickedBtn = new Subject<int>();

        public int index;
        public int equipState;

        public void OnPointerClick(PointerEventData ped) {
            if(PlayerInfo.Instance.GetTutorialStep() >= TutorialStep.ClearStoryStage_1_1_2) {
                OnClickedBtn.OnNext(index);
            }
        }

        public void SetGearImage(uint gearID, int state, uint level = 1) {
            var gearData = GlobalContainer.MasterManager.ItemMaster.GetGear(gearID);
            equipState = state;
            iconLvShortage.SetActive(false);
            iconCanEquip.SetActive(false);
            iconCanCraft.SetActive(false);
            iconCanCraftLvShortage.SetActive(false);
            imgGear.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(gearID);
            imgFrame.sprite = spriteGearFrameList[gearData.Rarity - 1];
            if(state > 0) {
                imgGear.material = null;
                imgFrame.material = null;
                for(int i = 0, l = enchantStarList.Length; i < l; i++) {
                    enchantStarList[i].SetActive(i < level - 1);
                }
            } else {
                imgGear.material = grayScale;
                imgFrame.material = grayScale;
                foreach(var star in enchantStarList) {
                    star.SetActive(false);
                }
                if(PlayerInfo.Instance.GetTutorialStep() < TutorialStep.ClearStoryStage_1_1_2) {
                    return;
                } if(state < -3) {
                    iconCanCraftLvShortage.SetActive(true);
                } else if(state < -2) {
                    iconCanCraft.SetActive(true);
                } else if(state < -1) {
                    iconCanEquip.SetActive(true);
                } else if(state < 0) {
                    iconLvShortage.SetActive(true);
                } else if(state == 0) {
//                    imgGear.color = new Color(0.5f, 0.5f, 0.5f);
                }
            }
        }

        public void SetEquip() {
            equipState = 1;
            equipParticle.Play();
        }
    }
}
