﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.Character;
using Noroshi.Core.Game.Character;

namespace Noroshi.CharacterList {
    public class UICharacter : CharacterView {

        private int animLoopNum = 0;
        private bool isLoop;

        private List<CharacterAnimation> anims = new List<CharacterAnimation> {
            CharacterAnimation.Win,
            CharacterAnimation.Walk,
            CharacterAnimation.Run,
            CharacterAnimation.Action0,
            CharacterAnimation.Action1,
            CharacterAnimation.Action2,
            CharacterAnimation.Action3
        };

        private new void Start() {
            base.Start();
            gameObject.SetActive(true);
            _rootSkeletonAnimation.state.Complete += OnCompleteSpineAnim;
            GetOnPlaySoundObservable().Do(id => {
                GlobalContainer.SoundManager.Play(id).Subscribe(success => {

                });
            }).Subscribe();
            var animations = _rootSkeletonAnimation.skeletonDataAsset.GetSkeletonData(false).Animations;
            foreach(var anim in animations) {
                if(anim.Name == UI.Constant.ANIM_P_SKILL3) {
                    anims.Add(CharacterAnimation.Action4);
                }
                if(anim.Name == UI.Constant.ANIM_P_SKILL4) {
                    anims.Add(CharacterAnimation.Action5);
                }
            }
        }

        private void OnCompleteSpineAnim(Spine.AnimationState state , int trackIndex , int loopCount) {
            if(state.ToString() == Noroshi.UI.Constant.ANIM_RUN || state.ToString() == Noroshi.UI.Constant.ANIM_WALK) {
                if(!isLoop && animLoopNum > 2) {
                    PlayIdleAnimation();
                }
            } else {
                PlayIdleAnimation();
            }
            animLoopNum++;
        }

        public void SetCharacterSkin(int evolutionLevel, bool isDeca) {
            var skinLevel = new Noroshi.Core.Game.Character.Skin((byte)evolutionLevel, isDeca).GetSkinLevel();
            _rootSkeletonAnimation.skeleton.SetSkin("step" + skinLevel);
            _rootSkeletonAnimation.skeleton.SetSlotsToSetupPose();
        }

        public void SetCharacterColor(Color color) {
            _rootSkeletonAnimation.skeleton.SetColor(color);
        }

        public void PlayActionAnimation(int index = -1) {
            if(index > anims.Count - 1) {return;}
            var n = index < 0 ? UnityEngine.Random.Range(0, anims.Count) : index;
            animLoopNum = 0;
            isLoop = false;
            PlayAnimation(anims[n], true);
        }

        public void PlayIdleAnimation() {
            isLoop = true;
            _rootSkeletonAnimation.state.SetAnimation(0, Noroshi.UI.Constant.ANIM_IDLE, true);
        }

        public void PlayRunAnimation() {
            isLoop = true;
            _rootSkeletonAnimation.state.SetAnimation(0, Noroshi.UI.Constant.ANIM_RUN, true);
        }
    }
}
