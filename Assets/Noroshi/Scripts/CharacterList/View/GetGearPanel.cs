﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.UI;
using Noroshi.Core.WebApi.Response.Master;

namespace Noroshi.CharacterList {
    public class GetGearPanel : MonoBehaviour {
        [SerializeField] Text txtGearName;
        [SerializeField] BtnCommon btnBack;
        [SerializeField] BtnCommon[] btnLinkList;
        [SerializeField] Text[] txtStageNameList;
        [SerializeField] GameObject[] iconLockList;

        public Subject<bool> OnBack = new Subject<bool>();

        private uint gearID;
        private PossessionStoryLink[] linkList;
        private uint lastNormalChapter;
        private uint lastNormalEpisode;
        private uint lastNormalStage;
        private uint lastEliteChapter;
        private uint lastEliteEpisode;
        private uint lastEliteStage;
        private bool isOpenElite;
        private bool isInit = false;

        private void Init() {
            var lastStory = PlayerInfo.Instance.GetLastStory();
            lastNormalChapter = lastStory["NormalChapter"];
            lastNormalEpisode = lastStory["NormalEpisode"];
            lastNormalStage = lastStory["NormalStage"];
            lastEliteChapter = lastStory["EliteChapter"];
            lastEliteEpisode = lastStory["EliteEpisode"];
            lastEliteStage = lastStory["EliteStage"];
            foreach(var btn in btnLinkList) {
                btn.OnClickedBtn.Subscribe(index => {
                    UILoading.Instance.SetQuery(QueryKeys.FromCharacterList, 1);
                    UILoading.Instance.SetQuery(QueryKeys.SelectedChapter, (int)linkList[index].ChapterID);
                    UILoading.Instance.SetQuery(QueryKeys.SelectedEpisode, (int)linkList[index].EpisodeID);
                    UILoading.Instance.SetQuery(QueryKeys.SelectedStage, (int)linkList[index].StageID);
                    if(linkList[index].StoryCategory == Noroshi.Core.Game.Story.StoryCategory.NormalStory) {
                        UILoading.Instance.SetQuery(QueryKeys.WorldType, 0);
                    } else {
                        UILoading.Instance.SetQuery(QueryKeys.WorldType, 1);
                    }
                    UILoading.Instance.LoadScene(Constant.SCENE_STORY);
                });
                btn.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            isOpenElite = Noroshi.Core.Game.GameContent.GameContent.IsOpen(
                Noroshi.Core.Game.GameContent.GameContentID.BackStory,
                (ushort)PlayerInfo.Instance.PlayerLevel,
                PlayerInfo.Instance.GetTutorialStep()
            );

            btnBack.OnClickedBtn.Subscribe(_ => {
                OnBack.OnNext(true);
            });
            btnBack.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });
            isInit = true;
            SetInfo(gearID);
        }

        public void SetInfo(uint id) {
            var gear = GlobalContainer.MasterManager.ItemMaster.GetGear(id);
            gearID = id;
            if(!isInit) {
                Init();
                return;
            }
            if(gear != null) {
                txtGearName.text = GlobalContainer.LocalizationManager.GetText(gear.TextKey + ".Name");
                linkList = GlobalContainer.MasterManager.PossessionLinkMaster.GetGearStoryLinks(id);
            } else {
                var gearPiece = GlobalContainer.MasterManager.ItemMaster.GetGearPiece(id);
                txtGearName.text = GlobalContainer.LocalizationManager.GetText(gearPiece.TextKey + ".Name");
                linkList = GlobalContainer.MasterManager.PossessionLinkMaster.GetGearPieceStoryLinks(id);
            }
            for(int i = 0, l = btnLinkList.Length; i < l; i++) {
                if(i < linkList.Length) {
                    var isLock = false;
                    txtStageNameList[i].text = GlobalContainer.LocalizationManager.GetText(linkList[i].StageTextKey + ".Name");
                    if(linkList[i].StoryCategory == Noroshi.Core.Game.Story.StoryCategory.NormalStory) {
                        if(linkList[i].ChapterID > lastNormalChapter) {isLock = true;}
                        if(linkList[i].EpisodeID > lastNormalEpisode) {isLock = true;}
                        if(linkList[i].StageID > lastNormalStage) {isLock = true;}
                    } else {
                        if(!isOpenElite
                            || linkList[i].ChapterID > lastEliteChapter
                            || linkList[i].EpisodeID > lastEliteEpisode
                            || linkList[i].StageID > lastEliteStage) {
                            isLock = true;
                        }
                    }
                    iconLockList[i].SetActive(isLock);
                    btnLinkList[i].SetEnable(!isLock);
                    btnLinkList[i].gameObject.SetActive(true);
                } else {
                    btnLinkList[i].gameObject.SetActive(false);
                }
            }
        }
    }
}
