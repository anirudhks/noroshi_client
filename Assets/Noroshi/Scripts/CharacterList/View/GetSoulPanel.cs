﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.UI;
using Noroshi.Core.WebApi.Response.Master;

namespace Noroshi.CharacterList {
    public class GetSoulPanel : MonoBehaviour {
        [SerializeField] Text txtSoulName;
        [SerializeField] BtnCommon btnBackground;
        [SerializeField] BtnCommon[] btnLinkList;
        [SerializeField] Text[] txtStageNameList;
        [SerializeField] GameObject[] iconLockList;

        private uint soulID;
        private PossessionStoryLink[] linkList;
        private uint lastNormalChapter;
        private uint lastNormalEpisode;
        private uint lastNormalStage;
        private uint lastEliteChapter;
        private uint lastEliteEpisode;
        private uint lastEliteStage;
        private bool isOpenElite;
        private bool isInit = false;
        private bool isOpen = false;

        private void Init() {
            var lastStory = PlayerInfo.Instance.GetLastStory();
            lastNormalChapter = lastStory["NormalChapter"];
            lastNormalEpisode = lastStory["NormalEpisode"];
            lastNormalStage = lastStory["NormalStage"];
            lastEliteChapter = lastStory["EliteChapter"];
            lastEliteEpisode = lastStory["EliteEpisode"];
            lastEliteStage = lastStory["EliteStage"];
            foreach(var btn in btnLinkList) {
                btn.OnClickedBtn.Subscribe(index => {
                    UILoading.Instance.SetQuery(QueryKeys.FromCharacterList, 1);
                    UILoading.Instance.SetQuery(QueryKeys.SelectedChapter, (int)linkList[index].ChapterID);
                    UILoading.Instance.SetQuery(QueryKeys.SelectedEpisode, (int)linkList[index].EpisodeID);
                    UILoading.Instance.SetQuery(QueryKeys.SelectedStage, (int)linkList[index].StageID);
                    if(linkList[index].StoryCategory == Noroshi.Core.Game.Story.StoryCategory.NormalStory) {
                        UILoading.Instance.SetQuery(QueryKeys.WorldType, 0);
                    } else {
                        UILoading.Instance.SetQuery(QueryKeys.WorldType, 1);
                    }
                    UILoading.Instance.LoadScene(Constant.SCENE_STORY);
                });
                btn.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            isOpenElite = Noroshi.Core.Game.GameContent.GameContent.IsOpen(
                Noroshi.Core.Game.GameContent.GameContentID.BackStory,
                (ushort)PlayerInfo.Instance.PlayerLevel,
                PlayerInfo.Instance.GetTutorialStep()
            );

            btnBackground.OnClickedBtn.Subscribe(StartCoroutine_Auto => {
                UILoading.Instance.RemoveQuery(QueryKeys.IsOpenGetSoulPanel);
                UILoading.Instance.RemoveQuery(QueryKeys.SelectedUnAcquiredCharacter);
                CloseSoulPanel();
            });
            btnBackground.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });
            isInit = true;
            SetInfo(soulID);

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseSoulPanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void CloseSoulPanel() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }

        public void SetInfo(uint id) {
            var soul = GlobalContainer.MasterManager.ItemMaster.GetSoul(id);
            soulID = id;
            if(!isInit) {
                Init();
                return;
            }
            txtSoulName.text = GlobalContainer.LocalizationManager.GetText(soul.TextKey + ".Name");
            linkList = GlobalContainer.MasterManager.PossessionLinkMaster.GetSoulStoryLinks(id);
            for(int i = 0, l = btnLinkList.Length; i < l; i++) {
                if(i < linkList.Length) {
                    var isLock = false;
                    txtStageNameList[i].text = GlobalContainer.LocalizationManager.GetText(linkList[i].StageTextKey + ".Name");
                    if(linkList[i].StoryCategory == Noroshi.Core.Game.Story.StoryCategory.NormalStory) {
                        if(linkList[i].ChapterID > lastNormalChapter) {isLock = true;}
                        if(linkList[i].EpisodeID > lastNormalEpisode) {isLock = true;}
                        if(linkList[i].StageID > lastNormalStage) {isLock = true;}
                    } else {
                        if(!isOpenElite
                            || linkList[i].ChapterID > lastEliteChapter
                            || linkList[i].EpisodeID > lastEliteEpisode
                            || linkList[i].StageID > lastEliteStage) {
                            isLock = true;
                        }
                    }
                    iconLockList[i].SetActive(isLock);
                    btnLinkList[i].SetEnable(!isLock);
                    btnLinkList[i].gameObject.SetActive(true);
                } else {
                    btnLinkList[i].gameObject.SetActive(false);
                }
            }
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }
    }
}
