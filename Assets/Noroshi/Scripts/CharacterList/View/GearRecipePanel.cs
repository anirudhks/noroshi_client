﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.UI;

namespace Noroshi.CharacterList {
    public class GearRecipePanel : MonoBehaviour {

        [SerializeField] Text txtGearName;
        [SerializeField] Image imgGear;
        [SerializeField] Image imgFrame;
        [SerializeField] Text txtCost;
        [SerializeField] RecipeItem[] recipeItemList;
        [SerializeField] BtnCommon btnBack;
        [SerializeField] BtnCommon btnCraft;
        [SerializeField] Image imgRecipeChart;
        [SerializeField] Sprite[] recipeChartSpriteList;
        [SerializeField] Sprite[] spriteItemFrameList;
        [SerializeField] Sprite[] spritePieceFrameList;

        public Subject<uint> OnMaterialSelect = new Subject<uint>();
        public Subject<uint> OnCreate = new Subject<uint>();
        public Subject<bool> OnBack = new Subject<bool>();

        private uint currentID;

        private void Start() {
            foreach(var recipeItem in recipeItemList) {
                recipeItem.OnClickedBtn.Subscribe(id => {
                    OnMaterialSelect.OnNext((uint)id);
                });
                recipeItem.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnBack.OnClickedBtn.Subscribe(_ => {
                OnBack.OnNext(true);
            });
            btnBack.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnCraft.OnClickedBtn.Subscribe(_ => {
                CreateGear();
            });
            btnCraft.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.GET);
            });
        }

        private void CreateGear() {
            var recipe = ItemListManager.Instance.GetGearRecipe(currentID);
            foreach(var r in recipe) {
                ItemListManager.Instance.ChangeItemCount(r["id"], -(int)r["num"]);
            }
            ItemListManager.Instance.ChangeItemCount(currentID, 1);
            SetCreateGearInfo(currentID);
            OnCreate.OnNext(currentID);
        }

        public void SetCreateGearInfo(uint id) {
            var recipes = ItemListManager.Instance.GetGearRecipe(id);

            currentID = id;
            var gear = GlobalContainer.MasterManager.ItemMaster.GetGear(id);
            if(gear != null) {
                txtGearName.text = GlobalContainer.LocalizationManager.GetText(gear.TextKey + ".Name");
                imgFrame.sprite = spriteItemFrameList[gear.Rarity - 1];
            } else {
                var gearPiece = GlobalContainer.MasterManager.ItemMaster.GetGearPiece(id);
                txtGearName.text = GlobalContainer.LocalizationManager.GetText(gearPiece.TextKey + ".Name");
                imgFrame.sprite = spritePieceFrameList[gearPiece.Rarity - 1];
            }
            imgGear.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>((uint)id);
            imgRecipeChart.sprite = recipeChartSpriteList[recipes.Count - 1];
            SetMaterialNum();
        }

        public void SetMaterialNum() {
            var recipes = ItemListManager.Instance.GetGearRecipe(currentID);
            var enableCreate = true;
            for(var i = 0; i < recipeItemList.Length; i++) {
                if(i < recipes.Count) {
                    recipeItemList[i].SetRecipeInfo(recipes[i]["id"], recipes[i]["num"]);
                    recipeItemList[i].gameObject.SetActive(true);
                    if(ItemListManager.Instance.GetItemCount(recipes[i]["id"]) < recipes[i]["num"]) {
                        enableCreate = false;
                    }
                } else {
                    recipeItemList[i].gameObject.SetActive(false);
                }
            }
            btnCraft.SetEnable(enableCreate);
        }
    }
}
