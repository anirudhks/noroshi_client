﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.UI;

namespace Noroshi.CharacterList {
    public class RecipeItem : BtnCommon {

        [SerializeField] Image imgRecipe;
        [SerializeField] Image imgFrame;
        [SerializeField] Text txtHaveNum;
        [SerializeField] Text txtNeedNum;
        [SerializeField] Sprite[] spriteItemFrameList;
        [SerializeField] Sprite[] spritePieceFrameList;

        public void SetRecipeInfo(uint id, uint needNum) {
            var gear = GlobalContainer.MasterManager.ItemMaster.GetGear(id);
            this.id = (int)id;
            if(needNum > ItemListManager.Instance.GetItemCount(id)) {
                txtHaveNum.color = Constant.TEXT_COLOR_NEGATIVE;
            } else {
                txtHaveNum.color = Constant.TEXT_COLOR_NORMAL_DARK;
            }
            if(gear != null) {
                imgFrame.sprite = spriteItemFrameList[gear.Rarity - 1];
            } else {
                var gearPiece = GlobalContainer.MasterManager.ItemMaster.GetGearPiece(id);
                imgFrame.sprite = spritePieceFrameList[gearPiece.Rarity - 1];
            }
            imgRecipe.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(id);
            txtHaveNum.text = ItemListManager.Instance.GetItemCount(id).ToString();
            txtNeedNum.text = needNum.ToString();
        }

    }
}
