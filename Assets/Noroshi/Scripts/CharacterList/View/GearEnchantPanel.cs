﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using Noroshi.UI;
using Noroshi.Core.WebApi.Response;
using Noroshi.Core.WebApi.Response.Character;

namespace Noroshi.CharacterList {
    public class GearEnchantPanel : MonoBehaviour {
        [SerializeField] Text txtEnchantLevel;
        [SerializeField] Text txtMaxEnchantLevel;
        [SerializeField] Text txtCurrentExp;
        [SerializeField] Text txtNeedExp;
        [SerializeField] Text txtCost;
        [SerializeField] GameObject currentExpBar;
        [SerializeField] GameObject newExpBar;
        [SerializeField] GameObject enchantItemWrapper;
        [SerializeField] EnchantItem enchantItemPref;
        [SerializeField] BtnCommon btnEnchant;
        [SerializeField] GameObject enchantConfirmPanel;
        [SerializeField] BtnCommon btnDecide;
        [SerializeField] BtnCommon btnCancel;

        public Subject<EquippedGear> OnEnchant = new Subject<EquippedGear>();

        private Core.WebApi.Response.Master.Gear gearData;
        private EquippedGear playerGearData;
        private List<EnchantItem> enchantItemList = new List<EnchantItem>();
        private List<uint> useGearEnchantMaterialList = new List<uint>();
        private List<uint> useGearList = new List<uint>();
        private List<uint> useGearPieceList = new List<uint>();
        private int currentLevel;
        private int newLevel;
        private int maxLevel;
        private float currentExp;
        private float totalExp;
        private float newExp;
        private float needExp;
        private float currentExpBarWidth;
        private float newExpBarWidth;
        private bool isOpenConfirm = false;

        private void Start() {
            btnEnchant.OnClickedBtn.Subscribe(_ => {
                isOpenConfirm = true;
                enchantConfirmPanel.SetActive(true);
                TweenA.Add(enchantConfirmPanel, 0.1f, 1).From(0);
                BackButtonController.Instance.IsModalChildOpen(true);
            });
            btnEnchant.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnCancel.OnClickedBtn.Subscribe(_ => {
                CloseConfirm();
            });
            btnCancel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnDecide.OnClickedBtn.Subscribe(_ => {
                Noroshi.Gear.WebApiRequester.Enchant(
                    playerGearData.PlayerCharacterID, playerGearData.No, useGearList.ToArray(), useGearPieceList.ToArray(), useGearEnchantMaterialList.ToArray()
                ).Do(data => {
                    PlayerInfo.Instance.ChangeHaveGold(-(int)gearData.GearEnchantLevels[currentLevel].Gold);
                    foreach(var id in useGearList) {
                        ItemListManager.Instance.ChangeItemCount(id, -1);
                    }
                    foreach(var id in useGearPieceList) {
                        ItemListManager.Instance.ChangeItemCount(id, -1);
                    }
                    foreach(var id in useGearEnchantMaterialList) {
                        ItemListManager.Instance.ChangeItemCount(id, -1);
                    }
                    foreach(var item in enchantItemList) {
                        item.SetHaveNum();
                        item.SetOverLimit(false);
                    }
                    TweenA.Add(enchantConfirmPanel, 0.1f, 0).Then(() => {
                        enchantConfirmPanel.SetActive(false);
                    });
                    OnEnchant.OnNext(data.Gear);
                }).Subscribe();
            });

            BackButtonController.Instance.OnCloseModalChild.Subscribe(_ => {
                if(!isOpenConfirm) {return;}
                CloseConfirm();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void CloseConfirm() {
            TweenA.Add(enchantConfirmPanel, 0.1f, 0).Then(() => {
                isOpenConfirm = false;
                enchantConfirmPanel.SetActive(false);
                BackButtonController.Instance.IsModalChildOpen(false);
            });
        }

        private void DrawGearStatus() {
            var cpx = currentExpBarWidth - currentExpBarWidth * currentExp / needExp;
            var npx = newExpBarWidth - newExpBarWidth * newExp / needExp;
            currentExpBar.transform.localPosition = new Vector3(-cpx, 0, 0);
            currentExpBar.SetActive(newLevel == currentLevel);
            if(newLevel != currentLevel) {
                txtEnchantLevel.color = Constant.TEXT_COLOR_POSITIVE_LIGHT;
                txtCurrentExp.color = Constant.TEXT_COLOR_POSITIVE_LIGHT;
            } else {
                txtEnchantLevel.color = Constant.TEXT_COLOR_NORMAL_WHITE;
                if(newExp != currentExp) {
                    txtCurrentExp.color = Constant.TEXT_COLOR_POSITIVE_LIGHT;
                } else {
                    txtCurrentExp.color = Constant.TEXT_COLOR_NORMAL_WHITE;
                }
            }
            if(newExp > needExp) {
                txtCurrentExp.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
                newExpBar.transform.localPosition = Vector3.zero;
            } else {
                newExpBar.transform.localPosition = new Vector3(-npx, 0, 0);
            }
            txtEnchantLevel.text = newLevel.ToString();
            txtMaxEnchantLevel.text = maxLevel.ToString();
            txtCurrentExp.text = newExp.ToString();
            txtNeedExp.text = needExp.ToString();
        }

        private void AddMaterial(EnchantItem.MaterialData materialData) {
            var expMapLength = gearData.NecessaryEnchantExpMaps.Length;
            if(materialData.Type == EnchantItem.ItemType.EnchantMaterial) {
                useGearEnchantMaterialList.Add(materialData.ID);
            } else if(materialData.Type == EnchantItem.ItemType.Gear) {
                useGearList.Add(materialData.ID);
            } else {
                useGearPieceList.Add(materialData.ID);
            }
            totalExp += materialData.Point;

            if(totalExp >= gearData.NecessaryEnchantExpMaps[expMapLength - 1]) {
                newLevel = expMapLength - 1;
                newExp = totalExp - gearData.NecessaryEnchantExpMaps[expMapLength - 2];
                needExp = gearData.NecessaryEnchantExpMaps[expMapLength - 1] - gearData.NecessaryEnchantExpMaps[expMapLength - 2];
                foreach(var item in enchantItemList) {
                    item.SetOverLimit(true);
                }
            } else {
                for(int i = 0; i < expMapLength - 1; i++) {
                    if(gearData.NecessaryEnchantExpMaps[i] <= totalExp && gearData.NecessaryEnchantExpMaps[i + 1] > totalExp) {
                        newLevel = i;
                        newExp = totalExp - gearData.NecessaryEnchantExpMaps[i];
                        needExp = gearData.NecessaryEnchantExpMaps[i + 1] - gearData.NecessaryEnchantExpMaps[i];
                    }
                }
            }
            DrawGearStatus();
            CheckEnableEnchant();
        }

        private void RemoveMaterial(EnchantItem.MaterialData materialData) {
            var expMapLength = gearData.NecessaryEnchantExpMaps.Length;
            if(materialData.Type == EnchantItem.ItemType.EnchantMaterial) {
                useGearEnchantMaterialList.Remove(materialData.ID);
            } else if(materialData.Type == EnchantItem.ItemType.Gear) {
                useGearList.Remove(materialData.ID);
            } else {
                useGearPieceList.Remove(materialData.ID);
            }
            totalExp -= materialData.Point;
            if(totalExp + materialData.Point >= gearData.NecessaryEnchantExpMaps[expMapLength - 1]
                && totalExp < gearData.NecessaryEnchantExpMaps[expMapLength - 1]) {
                foreach(var item in enchantItemList) {
                    item.SetOverLimit(false);
                }
            }
            if(totalExp >= gearData.NecessaryEnchantExpMaps[expMapLength - 1]) {
                newExp = totalExp - gearData.NecessaryEnchantExpMaps[expMapLength - 2];
            } else {
                for(int i = 0; i < expMapLength - 1; i++) {
                    if(gearData.NecessaryEnchantExpMaps[i] <= totalExp && gearData.NecessaryEnchantExpMaps[i + 1] > totalExp) {
                        newLevel = i;
                        newExp = totalExp - gearData.NecessaryEnchantExpMaps[i];
                        needExp = gearData.NecessaryEnchantExpMaps[i + 1] - gearData.NecessaryEnchantExpMaps[i];
                    }
                }
            }
            DrawGearStatus();
            CheckEnableEnchant();
        }

        private void CheckEnableEnchant() {
            var isEnable = true;
            if(useGearEnchantMaterialList.Count < 1 && useGearList.Count < 1 && useGearPieceList.Count < 1) {
                isEnable = false;
            }
            if(gearData.GearEnchantLevels[currentLevel].Gold > PlayerInfo.Instance.HaveGold) {
                isEnable = false;
                txtCost.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
            } else {
                txtCost.color = Constant.TEXT_COLOR_NORMAL_WHITE;
            }
            btnEnchant.SetEnable(isEnable);
        }

        public void Init() {
            currentExpBarWidth = currentExpBar.GetComponent<RectTransform>().sizeDelta.x;
            newExpBarWidth = newExpBar.GetComponent<RectTransform>().sizeDelta.x;
            var gearEnchantMaterials = GlobalContainer.MasterManager.ItemMaster.GetGearEnchantMaterials();
            foreach(var gearEnchantMaterial in gearEnchantMaterials) {
                var item = Instantiate(enchantItemPref);
                item.transform.SetParent(enchantItemWrapper.transform);
                item.transform.localScale = Vector3.one;
                item.SetEnchantItem(gearEnchantMaterial.ID, gearEnchantMaterial.EnchantExp, EnchantItem.ItemType.EnchantMaterial, gearEnchantMaterial.Rarity);
                item.OnAddMaterial.Subscribe(AddMaterial);
                item.OnRemoveMaterial.Subscribe(RemoveMaterial);
                enchantItemList.Add(item);
            }
            var gears = GlobalContainer.MasterManager.ItemMaster.GetGears();
            foreach(var gear in gears) {
                var item = Instantiate(enchantItemPref);
                item.transform.SetParent(enchantItemWrapper.transform);
                item.transform.localScale = Vector3.one;
                item.SetEnchantItem(gear.ID, gear.GearEnchantExp, EnchantItem.ItemType.Gear, gear.Rarity);
                item.OnAddMaterial.Subscribe(AddMaterial);
                item.OnRemoveMaterial.Subscribe(RemoveMaterial);
                enchantItemList.Add(item);
            }
            var gearPieces = GlobalContainer.MasterManager.ItemMaster.GetGearPieces();
            foreach(var gearPiece in gearPieces) {
                var item = Instantiate(enchantItemPref);
                item.transform.SetParent(enchantItemWrapper.transform);
                item.transform.localScale = Vector3.one;
                item.SetEnchantItem(gearPiece.ID, gearPiece.EnchantExp, EnchantItem.ItemType.Piece, gearPiece.Rarity);
                item.OnAddMaterial.Subscribe(AddMaterial);
                item.OnRemoveMaterial.Subscribe(RemoveMaterial);
                enchantItemList.Add(item);
            }
        }

        public void OpenEnchantPanel(Core.WebApi.Response.Master.Gear gear, EquippedGear playerGear) {
            gearData = gear;
            playerGearData = playerGear;
            useGearEnchantMaterialList = new List<uint>();
            useGearList = new List<uint>();
            useGearPieceList = new List<uint>();
            maxLevel = gear.GearEnchantLevels.Length;
            totalExp = playerGear.Exp;
            for(int i = 0, l = gear.NecessaryEnchantExpMaps.Length - 1; i < l; i++) {
                if(gear.NecessaryEnchantExpMaps[i] <= totalExp && gear.NecessaryEnchantExpMaps[i + 1] > totalExp) {
                    currentLevel = newLevel = i;
                    currentExp = newExp = totalExp - gear.NecessaryEnchantExpMaps[i];
                    needExp = gear.NecessaryEnchantExpMaps[i + 1] - gear.NecessaryEnchantExpMaps[i];
                }
            }
            txtCost.text = string.Format("{0:#,0}\r", gear.GearEnchantLevels[currentLevel].Gold);
            DrawGearStatus();
            CheckEnableEnchant();
            gameObject.transform.localPosition = new Vector3(115, -30, 0);
            enchantItemWrapper.SetActive(false);
            foreach(var item in enchantItemList) {
                item.SetHaveNum();
                item.SetOverLimit(false);
            }
            gameObject.SetActive(true);
            TweenX.Add(gameObject, 0.3f, 195).EaseOutQuad().Then(() => {
                enchantItemWrapper.SetActive(true);
            });
        }
    }
}
