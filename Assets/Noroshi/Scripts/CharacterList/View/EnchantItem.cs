﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Noroshi.UI;
using UniRx;

namespace Noroshi.CharacterList {
    public class EnchantItem : MonoBehaviour {
        [SerializeField] Image imgItem;
        [SerializeField] Image imgFrame;
        [SerializeField] Text txtHaveNum;
        [SerializeField] Text txtEnchantPoint;
        [SerializeField] Text txtUseNum;
        [SerializeField] GameObject enchantPointWrapper;
        [SerializeField] GameObject useNumWrapper;
        [SerializeField] BtnCommon btnEnchantItem;
        [SerializeField] BtnCommon btnReduceUseNum;
        [SerializeField] Sprite[] spriteItemFrameList;
        [SerializeField] Sprite[] spritePieceFrameList;

        public class MaterialData {
            public uint ID;
            public ItemType Type;
            public uint Point;
        }

        public enum ItemType {
            EnchantMaterial,
            Gear,
            Piece
        }

        public Subject<MaterialData> OnAddMaterial = new Subject<MaterialData>();
        public Subject<MaterialData> OnRemoveMaterial = new Subject<MaterialData>();

        private MaterialData materialDate = new MaterialData();

        private uint haveNum;
        private uint useNum;
        private bool isOverLimit = false;

        private void Start() {
            btnEnchantItem.OnClickedBtn.Subscribe(_ => {
                if(haveNum <= useNum) {return;}
                AddMaterial();
            });

            btnReduceUseNum.OnClickedBtn.Subscribe(_ => {
                if(useNum < 1) {return;}
                RemoveMaterial();
            });
        }

        private void AddMaterial() {
            useNum++;
            txtUseNum.text = useNum.ToString();
            if(useNum < 2) {
                useNumWrapper.SetActive(true);
                btnReduceUseNum.gameObject.SetActive(true);
            }
            CheckEnable();
            OnAddMaterial.OnNext(materialDate);
        }

        private void RemoveMaterial() {
            useNum--;
            txtUseNum.text = useNum.ToString();
            if(useNum <= 0) {
                useNumWrapper.SetActive(false);
                btnReduceUseNum.gameObject.SetActive(false);
            }
            CheckEnable();
            OnRemoveMaterial.OnNext(materialDate);
        }

        private void CheckEnable() {
            var isEnable = false;
            if(useNum < haveNum && !isOverLimit) {isEnable = true;}
            btnEnchantItem.SetEnable(isEnable);
            if(isEnable) {
                TweenA.Add(btnEnchantItem.gameObject, 0.01f, 1);
            } else {
                TweenA.Add(btnEnchantItem.gameObject, 0.01f, 0.5f);
            }
        }

        public void SetEnchantItem(uint id, uint point, ItemType type, uint rarity) {
            materialDate.ID = id;
            materialDate.Type = type;
            materialDate.Point = point;
            imgItem.sprite = GlobalContainer.AssetBundleManager.LoadFromItemAssetBundle<Sprite>(id);
            txtEnchantPoint.text = point.ToString();
            if(type == ItemType.Piece) {
                imgFrame.sprite = spritePieceFrameList[rarity - 1];
            } else {
                imgFrame.sprite = spriteItemFrameList[rarity - 1];
            }
        }

        public void SetHaveNum() {
            useNum = 0;
            haveNum = ItemListManager.Instance.GetItemCount(materialDate.ID);
            txtHaveNum.text = haveNum.ToString();
            useNumWrapper.SetActive(false);
            btnReduceUseNum.gameObject.SetActive(false);
            gameObject.SetActive(haveNum > 0);
        }

        public void SetOverLimit(bool isOver) {
            isOverLimit = isOver;
            CheckEnable();
        }
    }
}
