﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using Noroshi.UI;
using Noroshi.Core.Game.Player;

namespace Noroshi.CharacterList {
    public class SkillPanel : MonoBehaviour {
        [SerializeField] Image imgSkill;
        [SerializeField] Text txtSkillName;
        [SerializeField] Text txtLevel;
        [SerializeField] Text txtMoney;
        [SerializeField] BtnCommon btnSkillUp;
        [SerializeField] BtnCommon btnNotSkillUp;
        [SerializeField] GameObject unlockedContainer;
        [SerializeField] GameObject lockTxt;

        public Subject<int> OnPanelClick = new Subject<int>();
        public Subject<int> OnSkillUp = new Subject<int>();
        public Subject<int> OnNotSkillUp = new Subject<int>();

        public int index;

        private void Start() {
            btnSkillUp.OnClickedBtn.Subscribe(_ => {
                OnSkillUp.OnNext(index);
            });
            btnNotSkillUp.OnClickedBtn.Subscribe(_ => {
                OnNotSkillUp.OnNext(index);
            });
        }

        public void SetSkillPanel(uint id, bool isEnable, string skillName) {
            imgSkill.sprite = GlobalContainer.AssetBundleManager.LoadFromCharacterAssetBundle<Sprite>(id, "skill" + (index + 1).ToString());
            imgSkill.SetNativeSize();
            txtSkillName.text = GlobalContainer.LocalizationManager.GetText(skillName + ".Name");
            unlockedContainer.SetActive(isEnable);
            lockTxt.SetActive(!isEnable);
            if(isEnable) {
                imgSkill.color = Color.white;
            } else {
                imgSkill.color = new Color(0.2f, 0.2f, 0.2f);
            }
        }

        public void UpdateLevel(int level, uint gold) {
            txtLevel.text = level.ToString();
            txtMoney.text = gold.ToString();
        }

        public void SetActiveLevelUp(bool canLevelUp) {
            btnSkillUp.gameObject.SetActive(canLevelUp);
            btnNotSkillUp.gameObject.SetActive(!canLevelUp);
        }

        public void OnSkillPanelClick() {
            OnPanelClick.OnNext(index);
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }
    }
}
