﻿using System.Collections.Generic;
using UniLinq;

namespace Noroshi.Random
{
    public class RandomGenerator : IRandomGenerator
	{
		System.Random _random;

		public RandomGenerator()
		{
			_random = new System.Random();
		}

		public int GenerateInt(int max)
		{
			return _random.Next(max);
		}
		public float GenerateFloat(int max = 1)
		{
			return (float)(_random.NextDouble() * max);
		}
		public float GenerateFloat(float max)
		{
			return (float)(_random.NextDouble() * max);
		}

        public bool Lot(float probability)
        {
            return GenerateFloat() < probability;
        }
		public T Lot<T>(IEnumerable<T> targets)
		{
            var targetArray = targets as T[] ?? targets.ToArray();
            var index = _random.Next(targetArray.Length);
            return targetArray[index];
		}
	}
}
