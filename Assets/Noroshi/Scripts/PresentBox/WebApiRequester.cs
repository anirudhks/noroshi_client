﻿using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.WebApi.Response.PresentBox;

namespace Noroshi.PresentBox
{
    public class WebApiRequester
    {
        /// <summary>
        /// プレゼントボックス一覧を取得する。
        /// </summary>
        public static IObservable<ListResponse> List()
        {
            return GlobalContainer.WebApiRequester.Get<ListResponse>("PresentBox/List");
        }

        /// <summary>
        /// プレゼントボックス ID 指定で受け取る。
        /// </summary>
        public static IObservable<ReceiveResponse> Receive(IEnumerable<uint> presentBoxIds)
        {
            var request = new ReceiveRequest
            {
                PresentBoxIDs = presentBoxIds.ToArray(),
            };
            return GlobalContainer.WebApiRequester.Post<ReceiveRequest, ReceiveResponse>("PresentBox/Receive", request);
        }

        /// <summary>
        /// プレゼントボックスを一括で規定数上限に受け取る。
        /// </summary>
        public static IObservable<ReceiveResponse> ReceiveCollectively()
        {
            return GlobalContainer.WebApiRequester.Post<ReceiveResponse>("PresentBox/ReceiveCollectively");
        }

        class ReceiveRequest
        {
            public uint[] PresentBoxIDs { get; set; }
        }
    }
}
