﻿using UniRx;
using Noroshi.Core.WebApi.Response.Gear;

namespace Noroshi.Gear
{
    class WebApiRequester
    {
        public static IObservable<CraftGearResponse> CraftGear(uint gearId)
        {
            var request = new CraftGearRequest
            {
                GearID = gearId,
            };
            return GlobalContainer.WebApiRequester.Post<CraftGearRequest, CraftGearResponse>("Gear/CraftGear", request);
        }
        class CraftGearRequest
        {
            public uint GearID { get; set; }
        }
            
        public static IObservable<EnchantResponse> Enchant(uint playerCharacterId, byte gearNo, uint[] gearIds, uint[] gearPieceIds, uint[] gearEnchantMaterialIds)
        {
            var requestParams = new EnchantRequest
            {
                PlayerCharacterID = playerCharacterId,
                GearNo = gearNo,
                GearIDs = gearIds,
                GearPieceIDs = gearPieceIds,
                GearEnchantMaterialIDs = gearEnchantMaterialIds,
            };
            return GlobalContainer.WebApiRequester.Post<EnchantRequest, EnchantResponse>("Gear/Enchant", requestParams);
        }
        class EnchantRequest
        {
            public uint PlayerCharacterID { get; set; }
            public byte GearNo { get; set; }
            public uint[] GearIDs { get; set; }
            public uint[] GearPieceIDs { get; set; }
            public uint[] GearEnchantMaterialIDs { get; set; }
        }
    }
}
