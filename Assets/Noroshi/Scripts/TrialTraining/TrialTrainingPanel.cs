﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UniRx;

namespace Noroshi.UI {
    public class TrialTrainingPanel : BtnCommon {
        public class PanelData {
            public string Name;
            public DayOfWeek[] OpenDayOfWeeks;
            public bool IsOpen;
            public byte BattleNum;
            public byte MaxBattleNum;
            public uint ReopenedAt;
        }

        [SerializeField] GameObject panelImage;
        [SerializeField] Text txtStageTitle;
        [SerializeField] Text txtRemainNum;
        [SerializeField] Text txtMaxNum;
        [SerializeField] GameObject remainNumWrapper;
        [SerializeField] Image imgOpenDateFrame;
        [SerializeField] Text txtOpenDate;
        [SerializeField] GameObject spotLight;
        [SerializeField] GameObject iconLock;
        [SerializeField] Sprite spriteOpenFrame;
        [SerializeField] GameObject stagePanel;
        [SerializeField] GameObject separator;
        [SerializeField] GameObject coolTimePanel;
        [SerializeField] Text txtCoolTime;

        public Subject<int> OnPanelClicked = new Subject<int>();
        public Subject<bool> OnNoChance = new Subject<bool>();

        private IDisposable timer;
        private double currentTime;
        private int reopenedAt;
        private bool isCoolTime = false;
        private bool isNoChance = false;

        private void Start() {
            OnClickedBtn.Subscribe(_ => {
                if(isNoChance) {
                    OnNoChance.OnNext(true);
                } else if(isCoolTime) {
                    return;
                } else {
                    OnPanelClicked.OnNext(id);
                }
            });
            OnPlaySE.Subscribe(_ => {
                if(isCoolTime) {return;}
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
        }

        private void StartCoolDownTimer() {
            timer = Observable.Interval(TimeSpan.FromSeconds(1)).Subscribe(l => {
                currentTime = GlobalContainer.TimeHandler.UnixTime;
                if(reopenedAt - currentTime < 0) {
                    isCoolTime = false;
                    timer.Dispose();
                    coolTimePanel.SetActive(false);
                    return;
                }
                DrawCoolTime();
            }).AddTo(this);
        }

        private void DrawCoolTime() {
            var m = (int)((reopenedAt - currentTime) / 60);
            var s = (int)((reopenedAt - currentTime) % 60);
            var minute = m < 10 ? "0" + m : m.ToString();
            var second = s < 10 ? "0" + s : s.ToString();
            txtCoolTime.text = minute + ":" + second;
        }

        public void SetPanel(PanelData panelData) {
            var gclm = GlobalContainer.LocalizationManager;
            var openDayText = "";
            List<string> dayList = new List<string>();
            currentTime = GlobalContainer.TimeHandler.UnixTime;
            txtStageTitle.text = gclm.GetText(panelData.Name);
            foreach(var day in panelData.OpenDayOfWeeks) {
                var dayText = "";
                switch(day) {
                    case System.DayOfWeek.Sunday: dayText = "<color=#f47171>" + gclm.GetText("UI.Noun.Sunday") + "</color>"; break;
                    case System.DayOfWeek.Monday: dayText = gclm.GetText("UI.Noun.Monday"); break;
                    case System.DayOfWeek.Tuesday: dayText = gclm.GetText("UI.Noun.Tuesday"); break;
                    case System.DayOfWeek.Wednesday: dayText = gclm.GetText("UI.Noun.Wednesday"); break;
                    case System.DayOfWeek.Thursday: dayText = gclm.GetText("UI.Noun.Thursday"); break;
                    case System.DayOfWeek.Friday: dayText = gclm.GetText("UI.Noun.Friday"); break;
                    case System.DayOfWeek.Saturday: dayText = "<color=#6eaef5>" + gclm.GetText("UI.Noun.Saturday") + "</color>"; break;
                }
                dayList.Add(dayText);
            }
            openDayText = string.Join(" / ", dayList.ToArray());
            txtOpenDate.text = openDayText;
            if(panelData.IsOpen) {
                imgOpenDateFrame.sprite = spriteOpenFrame;
                iconLock.SetActive(false);
                txtRemainNum.text = (panelData.MaxBattleNum - panelData.BattleNum).ToString();
                txtMaxNum.text = Noroshi.Core.Game.Training.Constant.MAX_BATTLE_NUM.ToString();
                if(panelData.MaxBattleNum == panelData.BattleNum) {
                    isNoChance = true;
                    txtRemainNum.color = Constant.TEXT_COLOR_NEGATIVE_LIGHT;
                } else if(panelData.ReopenedAt - currentTime > 0) {
                    reopenedAt = (int)panelData.ReopenedAt;
                    isCoolTime = true;
                    DrawCoolTime();
                    StartCoolDownTimer();
                    coolTimePanel.SetActive(true);
                }
            } else {
                spotLight.SetActive(false);
                remainNumWrapper.SetActive(false);
                TweenC.Add(stagePanel, 0.01f, new Color(0.5f, 0.5f, 0.5f));
                TweenC.Add(separator, 0.01f, new Color(0.5f, 0.5f, 0.5f));
                TweenC.Add(panelImage, 0.01f, new Color(0.5f, 0.5f, 0.5f));
            }
        }
    }
}
