using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniLinq;
//using LitJson;
using Noroshi.Game;
using Noroshi.Core.WebApi.Response.Battle;
using Noroshi.Core.WebApi.Response.Possession;

namespace Noroshi.UI {
    public class NotOpenModal : MonoBehaviour {
        public class StageData {
            public string Title;
            public string Description;
            public DayOfWeek[] OpenDayOfWeeks;
        }

        [SerializeField] Text txtStageTitle;
        [SerializeField] Text txtOpenDate;
        [SerializeField] Text txtStageDescription;
        [SerializeField] BtnCommon overlay;

        private bool isOpen = false;

        private void Start() {
            overlay.OnClickedBtn.Subscribe(_ => {
                CloseModal();
            });
            overlay.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseModal();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        public void SetStageInfo(StageData data) {
            var gclm = GlobalContainer.LocalizationManager;
            var openDayText = "";
            foreach(var day in data.OpenDayOfWeeks) {
                var dayText = "";
                switch(day) {
                    case System.DayOfWeek.Sunday: dayText = gclm.GetText("UI.Noun.Sunday"); break;
                    case System.DayOfWeek.Monday: dayText = gclm.GetText("UI.Noun.Monday"); break;
                    case System.DayOfWeek.Tuesday: dayText = gclm.GetText("UI.Noun.Tuesday"); break;
                    case System.DayOfWeek.Wednesday: dayText = gclm.GetText("UI.Noun.Wednesday"); break;
                    case System.DayOfWeek.Thursday: dayText = gclm.GetText("UI.Noun.Thursday"); break;
                    case System.DayOfWeek.Friday: dayText = gclm.GetText("UI.Noun.Friday"); break;
                    case System.DayOfWeek.Saturday: dayText = gclm.GetText("UI.Noun.Saturday"); break;
                }
                openDayText += (" " + dayText + " ");
            }
            txtOpenDate.text = openDayText;
            txtStageTitle.text = gclm.GetText(data.Title + ".Name");
            txtStageDescription.text = data.Description;
            OpenModal();
        }

        public void OpenModal() {
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }

        public void CloseModal() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }
    }
}
