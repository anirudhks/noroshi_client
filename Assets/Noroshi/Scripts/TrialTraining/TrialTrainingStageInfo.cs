using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniLinq;
using LitJson;
using Noroshi.Game;
using Noroshi.Core.WebApi.Response.Battle;
using Noroshi.Core.WebApi.Response.Possession;

namespace Noroshi.UI {
    public class TrialTrainingStageInfo : MonoBehaviour {
        public class StageData {
            public string Name;
            public int Level;
            public string Description;
            public int NecessaryPlayerLevel = 0;
            public bool IsOpen;
            public int Rank = 0;
            public int RemainNum = 0;
            public BattleCharacter[] EnemyList = new BattleCharacter[]{};
            public PossessionObject[] ItemList;
            public int Score = -1;
            public uint StageID;
            public uint[] RequireIDList;
            public uint[] CPUIDList;
        }

        [SerializeField] Text txtTitle;
        [SerializeField] Text txtStageLevel;
        [SerializeField] Text txtDescription;
        [SerializeField] Text txtScore;
        [SerializeField] Text txtRemainChance;
        [SerializeField] Text txtMaxChance;
        [SerializeField] Text txtHaveTicket;
        [SerializeField] CharacterStatusIcon[] enemyIconList;
        [SerializeField] BtnCaption[] btnEnemyCaptionList;
        [SerializeField] CaptionCharacter captionCharacter;
        [SerializeField] ItemIcon[] itemIconList;
        [SerializeField] CaptionItem captionItem;
        [SerializeField] BtnCommon overlay;
        [SerializeField] GameObject quickBattle;
        [SerializeField] BtnCommon btnBattle;
        [SerializeField] BtnCommon[] btnSelectLevelList;
        [SerializeField] ScrollRect levelSelectScroller;

        public Subject<int> OnOpenStageInfo = new Subject<int>();
        public Subject<int> OnEditCharacter = new Subject<int>();

        private bool isCaptionOpen = false;
        private List<StageData> stageDataList;
        private List<CaptionCharacter.CharacterInfo> enemyInfoList;
        private List<CaptionItem.ItemInfo> itemInfoList;
        private bool isOpen = false;

        private void Start() {
            foreach(var btnEnemy in btnEnemyCaptionList) {
                btnEnemy.OnTouchBtn.Subscribe(index => {
                    var pos = btnEnemyCaptionList[index].transform.position;
                    if(isCaptionOpen) {return;}
                    isCaptionOpen = true;
                    captionCharacter.ShowCaption(enemyInfoList[index], pos);
                }).AddTo(btnEnemy);
                btnEnemy.OnReleaseBtn.Subscribe(index => {
                    isCaptionOpen = false;
                    captionCharacter.HideCaption();
                }).AddTo(btnEnemy);
            }
            foreach(var btnItem in itemIconList) {
                btnItem.OnTouchBtn.Subscribe(index => {
                    var pos = itemIconList[index].transform.position;
                    if(isCaptionOpen) {return;}
                    isCaptionOpen = true;
                    captionItem.ShowCaption(itemInfoList[index], pos);
                }).AddTo(btnItem);
                btnItem.OnReleaseBtn.Subscribe(index => {
                    isCaptionOpen = false;
                    captionItem.HideCaption();
                }).AddTo(btnItem);
            }
            foreach(var btn in btnSelectLevelList) {
                btn.OnClickedBtn.Subscribe(SelectStageLevel);
                btn.OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            overlay.OnClickedBtn.Subscribe(_ => {
                CloseStageInfo();
            });
            overlay.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnBattle.OnClickedBtn.Subscribe(stageID => {
                OnEditCharacter.OnNext(stageID);
            });
            btnBattle.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.DECIDE);
            });

            BackButtonController.Instance.OnCloseModal.Subscribe(_ => {
                if(!isOpen) {return;}
                CloseStageInfo();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void SelectStageLevel(int index) {
            txtTitle.text = GlobalContainer.LocalizationManager.GetText(stageDataList[index].Name);
            txtStageLevel.text = stageDataList[index].Level.ToString();
            txtDescription.text = stageDataList[index].Description;
            if(stageDataList[index].Score > -1) {
                txtScore.text = string.Format("{0:#,0}\r", stageDataList[index].Score);
                txtScore.gameObject.SetActive(true);
            } else {
                txtScore.gameObject.SetActive(false);
            }
            SetEnemyImg(stageDataList[index].EnemyList);
            SetItemImg(stageDataList[index].ItemList);
            btnBattle.id = (int)stageDataList[index].StageID;
            for(int i = 0, l = btnSelectLevelList.Length; i < l; i++) {
                if(i == index) {
                    btnSelectLevelList[i].SetSelect(true);
                } else {
                    btnSelectLevelList[i].SetSelect(false);
                }
            }
        }

        private void SetEnemyImg(BattleCharacter[] enemyList) {
            enemyInfoList = new List<CaptionCharacter.CharacterInfo>();
            foreach (var btn in btnEnemyCaptionList) {
                btn.gameObject.SetActive(false);
            }

            for(int i = 0, l = Mathf.Min(enemyList.Length, btnEnemyCaptionList.Length); i < l; i++) {
                var enemyIconData = new CharacterStatusIcon.CharacterData();
                var enemyInfo = new CaptionCharacter.CharacterInfo();
                var masterData = GlobalContainer.MasterManager.CharacterMaster.Get(enemyList[i].CharacterID);
                enemyInfo.Name = GlobalContainer.LocalizationManager.GetText(masterData.TextKey + ".Name");
                enemyInfo.Lv = enemyIconData.Level = enemyList[i].Level;
                enemyInfo.PromotionLevel = enemyIconData.PromotionLevel = enemyList[i].PromotionLevel;
                enemyInfo.ID = enemyIconData.CharacterID = enemyList[i].CharacterID;
                enemyInfo.IsBoss = enemyList[i].IsBoss;
                //                enemyInfo.Description = enemyList[i].Description;
                enemyInfo.Index = i;
                enemyInfoList.Add(enemyInfo);
                enemyInfoList = enemyInfoList.OrderBy(c => c.Index).ToList();

                enemyIconData.EvolutionLevel = enemyList[i].EvolutionLevel;
                enemyIconData.IsDeca = new Noroshi.Core.Game.Character.CharacterTagSet(masterData.TagFlags).IsDeca;

                enemyIconList[i].SetInfo(enemyIconData);
                btnEnemyCaptionList[i].gameObject.SetActive(true);
            }
        }

        private void SetItemImg(PossessionObject[] itemList) {
            var gclm = GlobalContainer.LocalizationManager;
            itemInfoList = new List<CaptionItem.ItemInfo>();
            foreach (var btn in itemIconList) {
                btn.gameObject.SetActive(false);
            }
            if(itemList == null || itemList.Length < 1) {return;}
            for(int i = 0, l = Mathf.Min(itemList.Length, itemIconList.Length); i < l; i++) {
                var itemInfo = new CaptionItem.ItemInfo();
                var rarity = ItemListManager.Instance.GetItemRarity(itemList[i]);
                itemIconList[i].SetItemIcon(itemList[i]);
                itemIconList[i].gameObject.SetActive(true);

                itemInfo.Name = gclm.GetText(itemList[i].TextKey + ".Name");
                itemInfo.Description = gclm.GetText(itemList[i].TextKey + ".Description").TrimStart();
                itemInfo.HaveNum = itemList[i].PossessingNum;
//                itemInfo.NeedLevel = gearData != null ? (uint)gearData.Level : 1;
//                itemInfo.Price = itemData.Price;
                itemInfo.ID = itemList[i].ID;
                itemInfo.Rarity = rarity;
                itemInfo.Index = i;
                itemInfo.Category = itemList[i].Category;
                itemInfoList.Add(itemInfo);
                itemInfoList = itemInfoList.OrderBy(c => c.Index).ToList();
            }
        }

        private CharacterStatusIcon.CharacterData SetCharacterStatus(CharacterStatus characterStatus) {
            var characterData = new CharacterStatusIcon.CharacterData();
            
            characterData.CharacterID = characterStatus.CharacterID;
            characterData.Level = characterStatus.Level;
            characterData.EvolutionLevel = characterStatus.EvolutionLevel;
            characterData.PromotionLevel = characterStatus.PromotionLevel;
            characterData.IsDeca = characterStatus.TagSet.IsDeca;
            return characterData;
        }

        public void SetStageInfo(List<StageData> dataList) {
            var selectIndex = 0;
            stageDataList = dataList;
            for(int i = 0, l = btnSelectLevelList.Length; i < l; i++) {
                if(i < dataList.Count && dataList[i].IsOpen && PlayerInfo.Instance.PlayerLevel >= dataList[i].NecessaryPlayerLevel) {
                    selectIndex = i;
                    btnSelectLevelList[i].gameObject.SetActive(true);
                } else {
                    btnSelectLevelList[i].gameObject.SetActive(false);
                }
            }
            levelSelectScroller.verticalNormalizedPosition = 0;
            SelectStageLevel(selectIndex);
            OpenStageInfo();
        }

        public void OpenStageInfo() {
            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsModalOpen(true);
        }

        public void CloseStageInfo() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsModalOpen(false);
            });
        }
    }
}
