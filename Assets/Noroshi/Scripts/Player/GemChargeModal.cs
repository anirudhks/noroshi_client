﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class GemChargeModal : MonoBehaviour {
        [SerializeField] Text txtCurrentGem;
        [SerializeField] Text txtVipLevel;
        [SerializeField] Text txtCurrentVipExp;
        [SerializeField] Text txtNeedVipExp;
        [SerializeField] Text txtRemainVipExp;
        [SerializeField] Text txtNextVipLevel;
        [SerializeField] RectTransform vipExpBar;
        [SerializeField] BtnCommon[] tabList;
        [SerializeField] GameObject[] panelList;
        [SerializeField] ChargesItem[] normalItemList;
        [SerializeField] ChargesItem[] specialItemList;
        [SerializeField] ChargesItem[] setItemList;
        [SerializeField] BtnCommon btnCloseChargesGem;
        [SerializeField] GameObject chargesConfirmPanel;
        [SerializeField] BtnCommon btnDecideCharges;
        [SerializeField] BtnCommon btnCancelCharges;

        private float vipExpBarWidth;
        private bool isOpen = false;
        private bool isOpenConfirm = false;

        private void Start() {
            vipExpBarWidth = vipExpBar.sizeDelta.x;

            for(int i = 0, l = tabList.Length; i < l; i++) {
                tabList[i].OnClickedBtn.Subscribe(SwitchTab);
                tabList[i].OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            for(int i = 0, l = normalItemList.Length; i < l; i++) {
                normalItemList[i].OnClickedBtn.Subscribe(_ => {
                    OpenConfirm();
                });
                normalItemList[i].OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            for(int i = 0, l = specialItemList.Length; i < l; i++) {
                specialItemList[i].OnClickedBtn.Subscribe(_ => {
                    OpenConfirm();
                });
                specialItemList[i].OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            for(int i = 0, l = setItemList.Length; i < l; i++) {
                setItemList[i].OnClickedBtn.Subscribe(_ => {
                    OpenConfirm();
                });
                setItemList[i].OnPlaySE.Subscribe(_ => {
                    SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
                });
            }

            btnCancelCharges.OnClickedBtn.Subscribe(_ => {
                CloseConfirm();
            });
            btnCancelCharges.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnCloseChargesGem.OnClickedBtn.Subscribe(_ => {
                Close();
            });
            btnCloseChargesGem.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseCommonParts.Subscribe(_ => {
                if(!isOpen) {return;}
                if(isOpenConfirm) {
                    CloseConfirm();
                } else {
                    Close();
                }
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);

            SwitchTab(0);
        }

        private void SwitchTab(int index) {
            for(int i = 0, l = tabList.Length; i < l; i++) {
                var isSelect = index == i;
                tabList[i].SetSelect(isSelect);
                panelList[i].SetActive(isSelect);
            }
        }

        private void CloseConfirm() {
            TweenA.Add(chargesConfirmPanel, 0.1f, 0).Then(() => {
                isOpenConfirm = false;
                chargesConfirmPanel.SetActive(false);
            });
        }

        private void OpenConfirm() {
            isOpenConfirm = true;
            chargesConfirmPanel.gameObject.SetActive(true);
            TweenA.Add(chargesConfirmPanel, 0.1f, 1).From(0);
        }

        private void Close() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsCommonPartsOpen(false);
            });
        }

        public void Open() {
            var vipLevel = PlayerInfo.Instance.GetPlayerStatus().VipLevel;
            var nextVipLevelData= GlobalContainer.MasterManager.LevelMaster.GetPlayerVipLevelByLevel(vipLevel);
            var currentVipExp = PlayerInfo.Instance.GetPlayerStatus().VipExpInLevel;
            var xx = vipExpBarWidth - vipExpBarWidth * (float)currentVipExp / (float)nextVipLevelData.Exp;

            vipExpBar.transform.localPosition = new Vector3(-xx, 0, 0);
            txtCurrentGem.text = PlayerInfo.Instance.GetPlayerStatus().Gem.ToString();
            txtVipLevel.text = vipLevel.ToString();
            txtCurrentVipExp.text = currentVipExp.ToString();
            txtNeedVipExp.text = nextVipLevelData.Exp.ToString();
            txtRemainVipExp.text = (nextVipLevelData.Exp - currentVipExp).ToString();
            txtNextVipLevel.text = nextVipLevelData.Level.ToString();

            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsCommonPartsOpen(true);
        }
    }
}
