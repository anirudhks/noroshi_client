﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace Noroshi.UI {
    public class BPRecoverModal : MonoBehaviour {
        [SerializeField] Text txtNeedGem;
        [SerializeField] Text txtCurrentBP;
        [SerializeField] Text txtNewBP;
        [SerializeField] Text txtMaxBP;
        [SerializeField] Text txtRecoverTimes;
        [SerializeField] GameObject needGemWrapper;
        [SerializeField] GameObject enoughWrapper;
        [SerializeField] GameObject notEnoughWrapper;
        [SerializeField] GameObject notEnoughGemAlert;
        [SerializeField] GameObject notRecoverAnyMoreAlert;
        [SerializeField] BtnCommon btnOK;
        [SerializeField] BtnCommon btnCancel;
        [SerializeField] BtnCommon btnClose;
        [SerializeField] GameObject processing;

        private Noroshi.Player.WebApiRequester webAPIRequester;
        private bool isOpen = false;

        private void Start() {
            webAPIRequester = new Noroshi.Player.WebApiRequester();

            btnOK.OnClickedBtn.Subscribe(_ => {
                RecoverBP();
            });
            btnOK.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.STATUS_UP);
            });

            btnCancel.OnClickedBtn.Subscribe(_ => {
                Close();
            });
            btnCancel.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            btnClose.OnClickedBtn.Subscribe(_ => {
                Close();
            });
            btnClose.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            });

            BackButtonController.Instance.OnCloseCommonParts.Subscribe(_ => {
                if(!isOpen) {return;}
                Close();
                SoundController.Instance.PlaySE(SoundController.SEKeys.CANCEL);
            }).AddTo(this);
        }

        private void RecoverBP() {
            processing.SetActive(true);
            webAPIRequester.RecoverBP().Do(data => {
                processing.SetActive(false);
                PlayerInfo.Instance.UpdatePlayerStatus(data.PlayerStatus);
                Close();
            }).Subscribe();
        }

        private void Close() {
            TweenA.Add(gameObject, 0.1f, 0).Then(() => {
                isOpen = false;
                gameObject.SetActive(false);
                BackButtonController.Instance.IsCommonPartsOpen(false);
            });
        }

        public void Open() {
            var playerStatus = PlayerInfo.Instance.GetPlayerStatus();
            var recoverNum = (ushort)playerStatus.LastBPRecoveryNum;
            var needGem = Noroshi.Core.Game.Player.Constant.RECOVER_GEM_POINT;
            if(PlayerInfo.Instance.CurrentBP >= PlayerInfo.Instance.MaxBP) {
                needGemWrapper.SetActive(false);
                txtNeedGem.gameObject.SetActive(false);
                notEnoughGemAlert.SetActive(false);
                notRecoverAnyMoreAlert.SetActive(true);
                enoughWrapper.SetActive(false);
                notEnoughWrapper.SetActive(true);
            } else if(needGem > playerStatus.Gem) {
                needGemWrapper.SetActive(true);
                txtNeedGem.gameObject.SetActive(true);
                txtNeedGem.text = needGem.ToString();
                notEnoughGemAlert.SetActive(true);
                notRecoverAnyMoreAlert.SetActive(false);
                enoughWrapper.SetActive(false);
                notEnoughWrapper.SetActive(true);
            } else {
                needGemWrapper.SetActive(true);
                txtNeedGem.gameObject.SetActive(true);
                txtNeedGem.text = needGem.ToString();
                txtCurrentBP.text = PlayerInfo.Instance.CurrentBP.ToString();
                txtNewBP.text = (PlayerInfo.Instance.CurrentBP + PlayerInfo.Instance.MaxBP).ToString();
                txtMaxBP.text = PlayerInfo.Instance.MaxBP.ToString();
                txtRecoverTimes.text = (recoverNum + 1).ToString();
                enoughWrapper.SetActive(true);
                notEnoughWrapper.SetActive(false);
            }

            isOpen = true;
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.1f, 1).From(0);
            BackButtonController.Instance.IsCommonPartsOpen(true);
        }
    }
}
