﻿using UniRx;
using Noroshi.Core.WebApi.Response.Players;

namespace Noroshi.Player
{
    public class WebApiRequester
    {
		protected string _url()
		{
			return "Player/";
		}

		static WebApi.WebApiRequester _getWebApiRequester()
		{
			return new WebApi.WebApiRequester();
		}

		public IObservable<PlayerServiceResponse> RecoverStamina()
		{
			return _getWebApiRequester().Post<PlayerServiceResponse>(_url() + "RecoverStamina");
		}
		
		public IObservable<PlayerServiceResponse> RecoverBP()
		{
			return _getWebApiRequester().Post<PlayerServiceResponse>(_url() + "RecoverBP");
		}

        public IObservable<PlayerServiceResponse> RecoverGold()
        {
            return _getWebApiRequester().Post<PlayerServiceResponse>(_url() + "RecoverGold");
        }
        public IObservable<PlayerServiceResponse> RecoverActionLevelPoint()
        {
            return _getWebApiRequester().Post<PlayerServiceResponse>(_url() + "RecoverActionLevelPoint");
        }

        /// <summary>
        /// 名前を変更する。
        /// </summary>
        /// <param name="name">名前</param>
        public IObservable<PlayerServiceResponse> ChangeName(string name)
        {
            var request = new ChangeNameRequest
            {
                Name = name,
            };
            return _getWebApiRequester().Post<ChangeNameRequest, PlayerServiceResponse>(_url() + "ChangeName", request);
        }
        class ChangeNameRequest
        {
            public string Name { get; set; }
        }
        /// <summary>
        /// アバターを変更する。
        /// </summary>
        /// <param name="playerCharacterId">プレイヤーキャラクター ID（キャラクター ID ではないことに注意）</param>
        public IObservable<PlayerServiceResponse> ChangeAvatar(uint playerCharacterId)
        {
            var request = new ChangeAvatarRequest
            {
                PlayerCharacterID = playerCharacterId,
            };
            return _getWebApiRequester().Post<ChangeAvatarRequest, PlayerServiceResponse>(_url() + "ChangeAvatar", request);
        }
        class ChangeAvatarRequest
        {
            public uint PlayerCharacterID { get; set; }
        }
    }
}
