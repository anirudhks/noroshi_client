﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Noroshi.UI {
    public class ChargesItem : BtnCommon {
        [SerializeField] Image imgItem;
        [SerializeField] Text txtItemTitle;
        [SerializeField] Text txtItemNum;
        [SerializeField] Text txtVipPoint;
        [SerializeField] Text txtPrice;
    }
}
