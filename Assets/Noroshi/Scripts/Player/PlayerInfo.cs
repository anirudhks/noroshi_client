using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi.Core.Game.Player;
using Noroshi.Core.Game.GameContent;
using Noroshi.Core.WebApi.Response.Players;

namespace Noroshi.UI {
    public class PlayerInfo : MonoBehaviour {
        [SerializeField] Text txtGold;
        [SerializeField] Text txtGem;
        [SerializeField] Text txtStamina;
        [SerializeField] Text txtMaxStamina;
        [SerializeField] Text txtBP;
        [SerializeField] Text txtMaxBP;
        [SerializeField] BtnCommon btnChangeGold;
        [SerializeField] BtnCommon btnChargesGem;
        [SerializeField] BtnCommon btnRecoverStamina;
        [SerializeField] BtnCommon btnRecoverBP;
        [SerializeField] GemChargeModal gemChargeModal;
        [SerializeField] GoldChangeModal goldChangeModal;
        [SerializeField] StaminaRecoverModal staminaRecoverModal;
        [SerializeField] BPRecoverModal bpRecoverModal;
        [SerializeField] PlayerLevelUpPanel playerLevelUpPanel;
        [SerializeField] Text txtTeamLevelUp;
        [SerializeField] Text txtStaminaUp;

        public Subject<uint> OnChangedGold = new Subject<uint>();
        public Subject<uint> OnChangedGem = new Subject<uint>();
        public Subject<ushort> OnChangedStamina = new Subject<ushort>();
        public Subject<ushort> OnChangedBP = new Subject<ushort>();
        public Subject<GameContent[]> OnUpPlayerLevel = new Subject<GameContent[]>();
        public Subject<bool> OnChangePlayerStatus = new Subject<bool>();

        public static PlayerInfo Instance;
        public bool isLoad = false;
        public bool IsSetLastStory {get {return isSetLastStory;}}

        private Noroshi.Core.WebApi.Response.PlayerStatus playerStatus;
        private TutorialStep tutorialStep;
        private static Dictionary<string, uint> lastStory;
        private static bool isSetLastStory;

        StaminaHandler _staminaHandler = new StaminaHandler();
        ushort _lastStamina;
        uint _lastStaminaUpdatedAt;
        BPHandler _bpHandler = new BPHandler();
        byte _lastBP;
        uint _lastBPUpdatedAt;

        IDisposable staminaTimer;
        IDisposable bpTimer;

        private ushort _playerLevel;
        private uint _haveGold;
        private uint _haveGem;

        public uint PlayerLevel {
            get {return _playerLevel;}
        }

        public uint HaveGold {
            get {return _haveGold;}
            private set {
                _haveGold = value;
                OnChangedGold.OnNext(value);
            }
        }

        public uint HaveGem {
            get {return _haveGem;}
            private set {
                _haveGem = value;
                OnChangedGem.OnNext(value);
            }
        }

        public ushort CurrentStamina {
            get {return _staminaHandler.CurrentValue(_playerLevel, _lastStamina, _lastStaminaUpdatedAt, GlobalContainer.TimeHandler.UnixTime);}
        }

        public ushort MaxStamina {
            get {return _staminaHandler.MaxValue(_playerLevel);}
        }

        public ushort CurrentBP {
            get {return _bpHandler.CurrentValue(_lastBP, _lastBPUpdatedAt, GlobalContainer.TimeHandler.UnixTime);}
        }

        public ushort MaxBP {
            get {return _bpHandler.MaxValue();}
        }

        private void Awake() {
            if (Instance == null) {Instance = this;}
            GlobalContainer.SetFactory(() => new Repositories.RepositoryManager());
            GlobalContainer.RepositoryManager.PlayerStatusRepository.Get().Do(data => {
                playerStatus = data;
                UpdateStatus();
                var tutorialStepHandler = new TutorialStepHandler(playerStatus.TutorialStep);
                tutorialStep = (TutorialStep)tutorialStepHandler.Step;
                CheckShowTutorial(tutorialStep);
                isLoad = true;
            }).Subscribe();
            if(lastStory == null) {
                SetLastStory();
            }
        }

        private void Start() {
            staminaTimer = Observable.Interval(TimeSpan.FromSeconds(1)).Where(_ => playerStatus != null).Scan(CurrentStamina, (prev, _) => {
                if(prev != CurrentStamina) OnChangedStamina.OnNext(CurrentStamina);
                if(CurrentStamina >= MaxStamina) {
                    staminaTimer.Dispose();
                }
                return CurrentStamina;
            }).Subscribe().AddTo(this);

            bpTimer = Observable.Interval(TimeSpan.FromSeconds(1)).Where(_ => playerStatus != null).Scan(CurrentBP, (prev, _) => {
                if(prev != CurrentBP) OnChangedBP.OnNext(CurrentBP);
                if(CurrentBP >= MaxBP) {
                    bpTimer.Dispose();
                }
                return CurrentBP;
            }).Subscribe().AddTo(this);

            OnChangedGold.Subscribe(n => {
                txtGold.text = string.Format("{0:#,0}\r", HaveGold);
            }).AddTo(this);
            
            OnChangedGem.Subscribe(n => {
                txtGem.text = string.Format("{0:#,0}\r", HaveGem);
            }).AddTo(this);
            
            OnChangedStamina.Subscribe(n => {
                txtStamina.text = CurrentStamina.ToString();
                txtMaxStamina.text = MaxStamina.ToString();
            }).AddTo(this);

            OnChangedBP.Subscribe(n => {
                txtBP.text = CurrentBP.ToString();
                txtMaxBP.text = MaxBP.ToString();
            }).AddTo(this);

            btnChangeGold.OnClickedBtn.Subscribe(_ => {
                goldChangeModal.Open();
            }).AddTo(this);
            btnChangeGold.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            }).AddTo(this);

            btnChargesGem.OnClickedBtn.Subscribe(_ => {
                gemChargeModal.Open();
            }).AddTo(this);
            btnChargesGem.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            }).AddTo(this);

            btnRecoverStamina.OnClickedBtn.Subscribe(_ => {
                staminaRecoverModal.Open();
            }).AddTo(this);
            btnRecoverStamina.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            }).AddTo(this);

            btnRecoverBP.OnClickedBtn.Subscribe(_ => {
                bpRecoverModal.Open();
            }).AddTo(this);
            btnRecoverBP.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            }).AddTo(this);
        }

        private void UpdateStatus() {
            _playerLevel = playerStatus.Level;
            HaveGold = playerStatus.Gold;
            HaveGem = playerStatus.Gem;
            _lastStamina = playerStatus.LastStamina;
            _lastStaminaUpdatedAt = playerStatus.LastStaminaUpdatedAt;
            _lastBP = (byte)playerStatus.LastBP;
            _lastBPUpdatedAt = playerStatus.LastBPUpdatedAt;
            OnChangedStamina.OnNext(CurrentStamina);
            OnChangedBP.OnNext(CurrentBP);
            OnChangePlayerStatus.OnNext(true);
        }

        private void CheckShowTutorial(TutorialStep step) {
            if(step == TutorialStep.Start
                || (step >= TutorialStep.ClearStoryStage_1_1_2 && step < TutorialStep.EquipGear)
                || (step >= TutorialStep.EquipGear && step < TutorialStep.ClearStoryStage_1_1_3)
                || (step == TutorialStep.ClearStoryStage_1_1_3 && PlayerPrefs.GetInt(SaveKeys.OpenTutorialQuest) < 1)
                || (step >= TutorialStep.ClearStoryStage_1_2_2 && step < TutorialStep.UPPromotionLevel)
                || (step >= TutorialStep.UPPromotionLevel && step < TutorialStep.UPActionLevel)
                || (step >= TutorialStep.ClearStoryStage_1_2_3 && step < TutorialStep.LotGacha)
                || (step == TutorialStep.ClearStoryStage_1_2_4 && PlayerPrefs.GetInt(SaveKeys.OpenTutorialArena) < 1)
                || (step == TutorialStep.ClearStoryStage_1_3_2 && PlayerPrefs.GetInt(SaveKeys.OpenTutorialDailyQuest) < 1)
                || (step >= TutorialStep.ClearStoryStage_1_3_4 && step < TutorialStep.ConsumeDrug)
                || (step == TutorialStep.ClearStoryStage_1_4_2 && PlayerPrefs.GetInt(SaveKeys.OpenTutorialGuild) < 1)
                || (step == TutorialStep.ClearStoryStage_1_5_11 && PlayerPrefs.GetInt(SaveKeys.OpenTutorialEvolution) < 1)
                || (step == TutorialStep.ClearStoryStage_1_6_19 && PlayerPrefs.GetInt(SaveKeys.OpenBackStory) < 1)
            ) {
                var tutorialCanvas = Instantiate(Resources.Load("Tutorial/TutorialCanvas")) as GameObject;
                tutorialCanvas.GetComponent<TutorialController>().Init(step);
            }
        }

        public void SetLastStory() {
            isSetLastStory = false;
            lastStory = new Dictionary<string, uint>();
            Story.WebApiRequester.ChapterList().Do(chapterListResponse => {
                var chapterList = chapterListResponse.StoryChapters;
                var n = chapterList.Length - 1;
                var lastNormalChapter = chapterList.Where(sc => sc.IsOpen)
                    .OrderByDescending(sc => sc.No).FirstOrDefault();
                var lastNormalEpisode = lastNormalChapter.Episodes.Where(se => se.NormalStoryIsOpen)
                    .OrderByDescending(se => se.No).FirstOrDefault();
                lastStory["NormalChapter"] = lastNormalChapter.ID;
                lastStory["NormalEpisode"] = lastNormalEpisode.ID;
                Story.WebApiRequester.Episode(lastNormalEpisode.ID, Noroshi.Core.Game.Story.StoryCategory.NormalStory).Do(episodeResponse => {
                    lastStory["NormalStage"] = episodeResponse.StoryEpisode.Stages
                        .Where(ss => ss.IsOpen && ss.StoryCategory == Noroshi.Core.Game.Story.StoryCategory.NormalStory)
                        .OrderByDescending(ss => ss.No).FirstOrDefault().ID;
                }).Subscribe();

                while(n > -1) {
                    var lastEpisode = chapterList[n].Episodes.Where(se => se.BackStoryIsOpen)
                        .OrderByDescending(se => se.No).FirstOrDefault();
                    if(lastEpisode == null) {
                        n--;
                        if(n < 0) {
                            lastStory["EliteChapter"] = 0;
                            lastStory["EliteEpisode"] = 0;
                            lastStory["EliteStage"] = 0;
                            isLoad = true;
                        }
                    } else {
                        lastStory["EliteChapter"] = chapterList[n].ID;
                        lastStory["EliteEpisode"] = lastEpisode.ID;
                        Story.WebApiRequester.Episode(lastEpisode.ID, Noroshi.Core.Game.Story.StoryCategory.BackStory).Do(episodeResponse => {
                            lastStory["EliteStage"] = episodeResponse.StoryEpisode.Stages
                                .Where(ss => ss.IsOpen && ss.StoryCategory == Noroshi.Core.Game.Story.StoryCategory.BackStory)
                                .OrderByDescending(ss => ss.No).FirstOrDefault().ID;
                            isSetLastStory = true;
                        }).Subscribe();
                        break;
                    }
                }
            }).Subscribe();
        }

        public void ChangeHaveGold(int value) {
            var tempGold = (int)_haveGold + value;
            HaveGold = (uint)tempGold;
        }

        public void ChangeHaveGem(int value) {
            var tempGem = (int)_haveGem + value;
            HaveGem = (uint)tempGem;
        }

        public void OpenGoldChangeModal() {
            goldChangeModal.Open();
        }

        public void OpenGemChargeModal() {
            gemChargeModal.Open();
        }

        public Noroshi.Core.WebApi.Response.PlayerStatus GetPlayerStatus() {
            return playerStatus;
        }

        public void UpdatePlayerStatus(Noroshi.Core.WebApi.Response.PlayerStatus status = null) {
            if(status == null) {
                GlobalContainer.RepositoryManager.PlayerStatusRepository.Get().Do(data => {
                    playerStatus = data;
                    UpdateStatus();
                }).Subscribe();
            } else {
                playerStatus = status;
                UpdateStatus();
            }
        }

        public TutorialStep GetTutorialStep() {
            return tutorialStep;
        }

        public void UpdatePlayerExp(AddPlayerExpResult addPlayerExpResult) {
            UpdatePlayerStatus();
            if(addPlayerExpResult != null && addPlayerExpResult.LevelUp) {
                var openGameContents = Core.Game.GameContent.GameContent.BuildMulti(addPlayerExpResult.OpenGameContentIDs).ToArray();
                OnUpPlayerLevel.OnNext(openGameContents);
                playerLevelUpPanel.ShowPanel(addPlayerExpResult);
            }
        }

        public Dictionary<string, uint> GetLastStory() {
            return lastStory;
        }
    }
}
