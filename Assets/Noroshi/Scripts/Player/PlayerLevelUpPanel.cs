﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;
using UniLinq;
using Noroshi.Core.WebApi.Response.Players;
using Noroshi.Core.Game.GameContent;

namespace Noroshi.UI {
    public class PlayerLevelUpPanel : MonoBehaviour {
        [SerializeField] GameObject levelUpPanel;
        [SerializeField] Text txtPreviousLevel;
        [SerializeField] Text txtNewLevel;
        [SerializeField] Text txtPreviousMaxStamina;
        [SerializeField] Text txtNewMaxStamina;
        [SerializeField] GameObject[] unlockContentList;
        [SerializeField] Text[] txtUnlockContentList;
        [SerializeField] BtnLoadLevel[] btnUnlockContentList;
        [SerializeField] BtnCommon btnOverlay;
        
        public void Start() {
            btnOverlay.OnClickedBtn.Subscribe(_ => {
                TweenA.Add(gameObject, 0.2f, 0).EaseOutCubic().Then(() => {
                    gameObject.SetActive(false);
                });
            }).AddTo(this);
            btnOverlay.OnClickedBtn.Subscribe(_ => {
                GlobalContainer.SoundManager.Play(SoundController.SEKeys.SELECT).Subscribe();
            }).AddTo(this);
        }

        private void SetScene(GameContentID id, int index) {
            btnUnlockContentList[index].gameObject.SetActive(true);
            if(id == GameContentID.Arena) {
                btnUnlockContentList[index].SetSceneName(Constant.SCENE_ARENA);
            } else if(id == GameContentID.Training) {
                btnUnlockContentList[index].SetSceneName(Constant.SCENE_TRAINING);
            } else if(id == GameContentID.Trial) {
                btnUnlockContentList[index].SetSceneName(Constant.SCENE_TRIAL);
            } else if(id == GameContentID.BeginnerGuild) {
                btnUnlockContentList[index].SetSceneName(Constant.SCENE_GUILD);
            } else if(id == GameContentID.Expedition) {
                btnUnlockContentList[index].SetSceneName(Constant.SCENE_EXPEDITION);
            } else if(id == GameContentID.NormalGuild) {
                btnUnlockContentList[index].SetSceneName(Constant.SCENE_GUILD);
            } else {
                btnUnlockContentList[index].gameObject.SetActive(false);
            }
        }

        public void ShowPanel(AddPlayerExpResult addPlayerExpResult) {
            txtPreviousLevel.text = addPlayerExpResult.PreviousPlayerLevel.ToString();
            txtNewLevel.text = addPlayerExpResult.CurrentPlayerLevel.ToString();
            txtPreviousMaxStamina.text = addPlayerExpResult.PreviousMaxStamina.ToString();
            txtNewMaxStamina.text = addPlayerExpResult.CurrentMaxStamina.ToString();
            var openGameContents = GameContent.BuildMulti(addPlayerExpResult.OpenGameContentIDs).ToArray();
            if(openGameContents.Length > 0) {
                for(int i = 0, l = unlockContentList.Length; i < l; i++) {
                    if(i < openGameContents.Length) {
                        txtUnlockContentList[i].text = GlobalContainer.LocalizationManager.GetText(openGameContents[i].TextKey);
                        SetScene((GameContentID)openGameContents[i].ID, i);
                        unlockContentList[i].SetActive(true);
                    } else {
                        unlockContentList[i].SetActive(false);
                    }
                }
                levelUpPanel.transform.localPosition = Vector3.zero;
            } else {
                levelUpPanel.transform.localPosition = new Vector3(0, -50, 0);
            }
            gameObject.SetActive(true);
            TweenA.Add(gameObject, 0.2f, 1).EaseInCubic().From(0);
            GlobalContainer.SoundManager.Play(SoundController.SEKeys.EVOLUTION).Subscribe();
        }

    }
}
