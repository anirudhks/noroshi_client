﻿using NUnit.Framework;
using UniLinq;
using NSubstitute;
using Noroshi.Game;
using Noroshi.BattleScene;

namespace Noroshi.Editor.Test.BattleScene
{
    [TestFixture]
    public class GridPositionCalculatorTest
    {
        [Test]
        [TestCase(1, 0, 1, Result = 13)]
        [TestCase(1, 0, 2, Result = 9)]
        [TestCase(2, 0, 2, Result = 13)]
        [TestCase(1, 0, 3, Result = 9)]
        [TestCase(2, 0, 3, Result = 13)]
        [TestCase(3, 0, 3, Result = 17)]
        [TestCase(1, 1, 1, Result = 9)]
        [TestCase(1, 1, 2, Result = 9)]
        [TestCase(2, 1, 2, Result = 17)]
        [TestCase(1, 1, 3, Result = 5)]
        [TestCase(2, 1, 3, Result = 9)]
        [TestCase(3, 1, 3, Result = 17)]
        /// <summary>
        /// 自キャラクターの垂直方向インデックスを取得テスト。
        /// </summary>
        /// <returns>垂直方向インデックス</returns>
        /// <param name="noInSameCharacterPosition">同ポジション内順番</param>
        /// <param name="usedLineNum">既に埋まっているライン数</param>
        /// <param name="totalNumInSameCharacterPosition">同ポジション内総数</param>
        public ushort GetOwnCharacterVerticalIndexTest(byte noInSameCharacterPosition, byte usedLineNum, byte totalNumInSameCharacterPosition)
        {
            var positionCalculator = new GridPositionCalculator();
            return positionCalculator.GetOwnCharacterVerticalIndex(noInSameCharacterPosition, usedLineNum, totalNumInSameCharacterPosition);
        }

        [Test]
        [TestCase(1, 0, 1, Result = 12)]
        [TestCase(2, 0, 2, Result = 12)]
        [TestCase(2, 0, 3, Result = 12)]
        /// <summary>
        /// 敵キャラクターの垂直方向インデックスを取得テスト。
        /// </summary>
        /// <returns>垂直方向インデックス</returns>
        /// <param name="noInSameCharacterPosition">同ポジション内順番</param>
        /// <param name="usedLineNum">既に埋まっているライン数</param>
        /// <param name="totalNumInSameCharacterPosition">同ポジション内総数</param>
        public int GetEnemyCharacterVerticalIndexTest(byte noInSameCharacterPosition, byte usedLineNum, byte totalNumInSameCharacterPosition)
        {
            var positionCalculator = new GridPositionCalculator();
            return positionCalculator.GetEnemyCharacterVerticalIndex(noInSameCharacterPosition, usedLineNum, totalNumInSameCharacterPosition);
        }

        [Test]
        [TestCase(Force.Own, Result = new uint[]{11,15,7,19,3,23,1,25})]
        [TestCase(Force.Enemy, Result = new uint[]{14,10,18,6,22,2,24,0})]
        public ushort[] GetShadowCharacterVerticalIndexTest(Force force)
        {
            var positionCalculator = new GridPositionCalculator();
            return positionCalculator.GetShadowCharacterVerticalIndexes(force, null).ToArray();
        }
    }
}
