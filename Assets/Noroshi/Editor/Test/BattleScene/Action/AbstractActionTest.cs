﻿using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using NUnit.Framework;
using NSubstitute;
using Noroshi.Random;
using Noroshi.Grid;
using Noroshi.BattleScene;
using Noroshi.BattleScene.Actions;
using Noroshi.BattleScene.Actions.Roles;
using Noroshi.Editor.Test.BattleScene.Action.Mock;


namespace Noroshi.Editor.Test.BattleScene.Action
{
    public abstract class AbstractActionTest
    {
        const int DEFAULT_EXECUTOR_HORIZONTAL_INDEX = 16;

        protected void _actionLevelUpTest(
            IAction action, IActionExecutor executor, MockActionTargetFinder mockTargetFinder,
            ushort beforeLevel, ushort afterLevel,
            Action<int, int> callback
        )
        {
            // ターゲットの ActionEvent を取り出せるように設定。
            ActionEvent actionEvent = null;
            var target = Substitute.For<IActionTarget>();
            target.ReceiveActionEvent(Arg.Do<ActionEvent>(ae => actionEvent = ae));

            // ターゲットを配置。
            mockTargetFinder.SetTarget(DEFAULT_EXECUTOR_HORIZONTAL_INDEX, target, Force.Enemy);

            // ターゲットは引っかからないといけない。
            var targetCandidates = action.GetTargets(mockTargetFinder, executor);
            Assert.AreEqual(1, targetCandidates.Length);
            Assert.AreEqual(target, targetCandidates[0]);

            GlobalContainer.SetFactory<IRandomGenerator>(() => (new MockRandomGenerator(new[] { 0.5f })));

            // 前レベルでダメージ測定。
            action.SetLevel(beforeLevel);
            action.PreProcess(executor, targetCandidates);
            action.Execute(mockTargetFinder, executor, targetCandidates);
            var beforeHpHpDamage = actionEvent.HPDamage.Value;

            // 後レベルえダメージ測定。
            action.SetLevel(afterLevel);
            action.PreProcess(executor, targetCandidates);
            action.Execute(mockTargetFinder, executor, targetCandidates);
            var afterHpDamage = actionEvent.HPDamage.Value;

            // 評価。
            callback(beforeHpHpDamage, afterHpDamage);
        }
    }
}
