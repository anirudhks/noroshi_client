﻿using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using NUnit.Framework;
using NSubstitute;
using Noroshi.Random;
using Noroshi.Grid;
using Noroshi.BattleScene;
using Noroshi.BattleScene.Actions;
using Noroshi.BattleScene.Actions.Roles;
using Noroshi.Editor.Test.BattleScene.Action.Mock;

namespace Noroshi.Editor.Test.BattleScene.Action
{
    [TestFixture]
	public class FixedDamageTest : AbstractActionTest
    {
        static readonly byte? DEFAULT_TARGET_SORT_TYPE = null;
        static readonly byte? DEFAULT_MAX_TARGET_NUM = null;
        const int DEFAULT_TARGET_SELECT_TYPE = 0;
        const int DEFAULT_TARGET_TAG_INDEX = 0;
        const int ACTION_EFFECT_TYPE = 0;
        const int DEFAULT_INTERCEPT1 = 10;
        const int DEFAULT_SLOPE1 = 3;
        const int DEFAULT_EXECUTOR_HORIZONTAL_INDEX = 16;

        IAction _currentAction;
        MockActionTargetFinder _currentMockTargetFinder;
        IActionExecutor _currentExecutor;

        [SetUp]
        public void Initialize()
        {
            _currentAction = null;
            _currentMockTargetFinder = null;
            _currentExecutor = null;
        }

        /// <summary>
        /// 対象者取得のテスト。
        /// </summary>
        [Test]
        [TestCase(0, 0, 0, 0, 1, 1, Result = new byte[]{1,2,3})]
        public byte[] GetTargetsTest(int targetSelectType, int targetTagFlags, int targetrSortType, int maxTargetNum,
                                     int ownForceNum, int enemyForceNum)
        {
            _setUpMockActionTargetFinder();
            _currentAction = new FixedDamage(new Noroshi.Core.WebApi.Response.Master.Action
            {
                Arg1 = targetSelectType,
                Arg2 = targetTagFlags,
                Arg3 = targetrSortType,
                Arg4 = maxTargetNum,
                Intercept1 = DEFAULT_INTERCEPT1,
                Slope1 = DEFAULT_SLOPE1,
            });

            _currentExecutor = Substitute.For<IActionExecutor>();
            _currentExecutor.GetDirection().Returns(Direction.Right);

            _currentMockTargetFinder.SetExecutor(DEFAULT_EXECUTOR_HORIZONTAL_INDEX, _currentExecutor, Force.Own);

            for (var i = 0; i < ownForceNum; i++)
            {
                _currentMockTargetFinder.SetTarget(DEFAULT_EXECUTOR_HORIZONTAL_INDEX, Substitute.For<IActionTarget>(), Force.Own);
            }

            for (var i = 0; i < enemyForceNum; i++)
            {
                _currentMockTargetFinder.SetTarget(DEFAULT_EXECUTOR_HORIZONTAL_INDEX, Substitute.For<IActionTarget>(), Force.Enemy);
            }

            return _currentAction.GetTargets(_currentMockTargetFinder, _currentExecutor)
                .Select(at => _currentMockTargetFinder.GetNoByActionTarge(at)).ToArray();
        }
        
        /// <summary>
        /// Action Level アップ効果テスト。
        /// </summary>
        [Test]
        [TestCase((ushort)1, (ushort)2, Result = 3)]
        [TestCase((ushort)1, (ushort)6, Result = 15)]
        [TestCase((ushort)11, (ushort)16, Result = 15)]
        public int ActionLevelUpTest(ushort beforeLevel, ushort afterLevel)
        {
            _setUpDefault();
			var damageUp = 0;
			_actionLevelUpTest(_currentAction, _currentExecutor, _currentMockTargetFinder, beforeLevel, afterLevel, (beforeHpDamage, afterHpDamage) =>
			{
                damageUp = afterHpDamage - beforeHpDamage;
			});
            return damageUp;
        }
        
        /// <summary>
        /// Action を実行するが、実行するまでの間に対象者がいなくなってしまった場合のテスト。
        /// </summary>
        [Test]
        public void ExecuteButNoTargetTest()
        {
            _setUpDefault();

            // 適当に対象者をセット。
            _currentMockTargetFinder.SetTarget(DEFAULT_EXECUTOR_HORIZONTAL_INDEX, Substitute.For<IActionTarget>(), Force.Enemy);
            _currentMockTargetFinder.SetTarget(DEFAULT_EXECUTOR_HORIZONTAL_INDEX, Substitute.For<IActionTarget>(), Force.Enemy);
            
            // 初回判定時（実際にはアニメーション開始時）には対象が存在していることを確認。
            var targetCandidates = _currentAction.GetTargets(_currentMockTargetFinder, _currentExecutor);
            Assert.IsNotEmpty(targetCandidates);

            // ActionTargetFinder 内のアクション対象者が ActionEvent を受け取った回数を数えられるように処理を挟んだ上で、
            // ActionTargetFinder からはクリア。今後の検索では対象者が引っかからないようにしてしまう。
            var reciveActionEventBum = 0;
            foreach (var target in _currentMockTargetFinder.GetAllTargets())
            {
                target.When(t => t.ReceiveActionEvent(Arg.Any<ActionEvent>())).Do(_ => reciveActionEventBum++);
            }
            _currentMockTargetFinder.ClearTargets();

            // Action を実行し、どの対象者も ActionEvent を受け取っていないことを確認。
            _currentAction.PreProcess(_currentExecutor, targetCandidates);
            _currentAction.Execute(_currentMockTargetFinder, _currentExecutor, targetCandidates);
            Assert.AreEqual(0, reciveActionEventBum);
        }

        void _setUpDefault()
        {
            _setUpDefaultAction();
            _setUpMockActionTargetFinder();
            _setUpDefaultActionExecutor();
        }

        void _setUpDefaultAction()
        {
            _currentAction = new FixedDamage(new Noroshi.Core.WebApi.Response.Master.Action
            {
                TargetSortType = DEFAULT_TARGET_SORT_TYPE,
                MaxTargetNum = DEFAULT_MAX_TARGET_NUM,
                Arg1 = (int)TargetSelectType.OtherForce,
                Arg2 = DEFAULT_TARGET_TAG_INDEX,
                Arg3 = ACTION_EFFECT_TYPE,
                Intercept1 = DEFAULT_INTERCEPT1,
                Slope1 = DEFAULT_SLOPE1,
            });
        }

        void _setUpMockActionTargetFinder()
        {
            _currentMockTargetFinder = new MockActionTargetFinder();
        }

        void _setUpDefaultActionExecutor()
        {
            _currentExecutor = Substitute.For<IActionExecutor>();
            _currentExecutor.GetDirection().Returns(Direction.Right);
            _currentMockTargetFinder.SetExecutor(DEFAULT_EXECUTOR_HORIZONTAL_INDEX, _currentExecutor, Force.Own);
        }
    }
}