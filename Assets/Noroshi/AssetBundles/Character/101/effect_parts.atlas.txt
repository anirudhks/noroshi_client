
effect_parts.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
atk_effect0001
  rotate: false
  xy: 249, 831
  size: 213, 86
  orig: 245, 243
  offset: 28, 149
  index: -1
atk_effect0002
  rotate: true
  xy: 263, 2
  size: 197, 230
  orig: 245, 243
  offset: 7, 3
  index: -1
atk_effect0003
  rotate: true
  xy: 495, 1509
  size: 126, 222
  orig: 245, 243
  offset: 7, 0
  index: -1
atk_effect0004
  rotate: true
  xy: 1512, 1693
  size: 80, 132
  orig: 245, 243
  offset: 7, 0
  index: -1
atk_effect2
  rotate: false
  xy: 1206, 1513
  size: 149, 147
  orig: 149, 149
  offset: 0, 1
  index: -1
ground_effect0001
  rotate: false
  xy: 1378, 1655
  size: 132, 193
  orig: 312, 478
  offset: 96, 2
  index: -1
ground_effect0002
  rotate: true
  xy: 1141, 1850
  size: 191, 375
  orig: 312, 478
  offset: 61, 0
  index: -1
ground_effect0003
  rotate: false
  xy: 2, 786
  size: 245, 418
  orig: 312, 478
  offset: 41, 1
  index: -1
ground_effect0004
  rotate: true
  xy: 304, 1774
  size: 267, 421
  orig: 312, 478
  offset: 30, 30
  index: -1
ground_effect0005
  rotate: false
  xy: 2, 1206
  size: 285, 416
  orig: 312, 478
  offset: 20, 42
  index: -1
ground_effect0006
  rotate: false
  xy: 2, 1624
  size: 300, 417
  orig: 312, 478
  offset: 12, 53
  index: -1
ground_effect0007
  rotate: false
  xy: 1518, 1775
  size: 304, 266
  orig: 312, 478
  offset: 8, 55
  index: -1
ground_effect0008
  rotate: true
  xy: 2, 65
  size: 304, 259
  orig: 312, 478
  offset: 8, 59
  index: -1
ground_effect0009
  rotate: false
  xy: 719, 1520
  size: 301, 250
  orig: 312, 478
  offset: 11, 64
  index: -1
ground_effect0010
  rotate: true
  xy: 194, 491
  size: 293, 244
  orig: 312, 478
  offset: 18, 68
  index: -1
skill_effect10001
  rotate: true
  xy: 1022, 1491
  size: 279, 182
  orig: 543, 235
  offset: 61, 37
  index: -1
skill_effect10002
  rotate: false
  xy: 727, 1836
  size: 412, 205
  orig: 543, 235
  offset: 61, 6
  index: -1
skill_effect10003
  rotate: true
  xy: 2, 371
  size: 413, 190
  orig: 543, 235
  offset: 59, 4
  index: -1
skill_effect10004
  rotate: false
  xy: 304, 1637
  size: 413, 135
  orig: 543, 235
  offset: 59, 4
  index: -1
skill_effect10005
  rotate: false
  xy: 727, 1772
  size: 354, 62
  orig: 543, 235
  offset: 123, 4
  index: -1
skill_effect10006
  rotate: false
  xy: 727, 1772
  size: 354, 62
  orig: 543, 235
  offset: 123, 4
  index: -1
skill_effect1_set0001
  rotate: false
  xy: 1206, 1662
  size: 170, 186
  orig: 215, 290
  offset: 1, 27
  index: -1
skill_effect1_set0002
  rotate: false
  xy: 477, 1244
  size: 211, 263
  orig: 215, 290
  offset: 0, 16
  index: -1
skill_effect1_set0003
  rotate: false
  xy: 1824, 1757
  size: 214, 284
  orig: 215, 290
  offset: 0, 5
  index: -1
skill_effect1_set0004
  rotate: false
  xy: 263, 201
  size: 205, 288
  orig: 215, 290
  offset: 8, 0
  index: -1
skill_effect1_set0005
  rotate: false
  xy: 249, 919
  size: 195, 285
  orig: 215, 290
  offset: 16, 1
  index: -1
skill_effect1_set0006
  rotate: false
  xy: 289, 1206
  size: 186, 283
  orig: 215, 290
  offset: 26, 3
  index: -1
