
chara_parts.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
B_arm_decoline_03
  rotate: false
  xy: 357, 616
  size: 30, 28
  orig: 30, 28
  offset: 0, 0
  index: -1
B_forearm_01_02_03
  rotate: true
  xy: 973, 726
  size: 58, 49
  orig: 58, 49
  offset: 0, 0
  index: -1
B_hair_01_02_03
  rotate: false
  xy: 2, 95
  size: 110, 295
  orig: 110, 295
  offset: 0, 0
  index: -1
B_hand_01_02_03
  rotate: true
  xy: 991, 591
  size: 41, 31
  orig: 41, 31
  offset: 0, 0
  index: -1
B_leg_under_01
  rotate: true
  xy: 114, 91
  size: 46, 98
  orig: 46, 98
  offset: 0, 0
  index: -1
B_leg_under_02
  rotate: true
  xy: 759, 575
  size: 46, 98
  orig: 46, 98
  offset: 0, 0
  index: -1
B_leg_under_03
  rotate: true
  xy: 96, 43
  size: 46, 98
  orig: 46, 98
  offset: 0, 0
  index: -1
B_leg_up_01_02_03
  rotate: true
  xy: 663, 563
  size: 51, 94
  orig: 51, 94
  offset: 0, 0
  index: -1
B_upperarm_01
  rotate: false
  xy: 927, 609
  size: 57, 63
  orig: 57, 63
  offset: 0, 0
  index: -1
B_upperarm_02
  rotate: true
  xy: 926, 550
  size: 57, 63
  orig: 57, 63
  offset: 0, 0
  index: -1
B_upperarm_03
  rotate: false
  xy: 422, 368
  size: 57, 63
  orig: 57, 63
  offset: 0, 0
  index: -1
F_arm_doco_03
  rotate: false
  xy: 496, 282
  size: 34, 34
  orig: 34, 34
  offset: 0, 0
  index: -1
F_forearm_01_02_03
  rotate: false
  xy: 859, 567
  size: 65, 54
  orig: 65, 54
  offset: 0, 0
  index: -1
F_hand_01_02_03
  rotate: false
  xy: 191, 2
  size: 34, 39
  orig: 34, 39
  offset: 0, 0
  index: -1
F_hand_atack_01B_02B_03B
  rotate: false
  xy: 227, 2
  size: 33, 39
  orig: 33, 39
  offset: 0, 0
  index: -1
F_leg_under_01
  rotate: true
  xy: 547, 574
  size: 40, 114
  orig: 40, 114
  offset: 0, 0
  index: -1
F_leg_under_02
  rotate: true
  xy: 547, 532
  size: 40, 114
  orig: 40, 114
  offset: 0, 0
  index: -1
F_leg_under_03
  rotate: true
  xy: 355, 479
  size: 40, 114
  orig: 40, 114
  offset: 0, 0
  index: -1
F_leg_under_OUTLINE_01
  rotate: false
  xy: 939, 905
  size: 46, 117
  orig: 46, 117
  offset: 0, 0
  index: -1
F_leg_under_OUTLINE_02
  rotate: false
  xy: 309, 527
  size: 46, 117
  orig: 46, 117
  offset: 0, 0
  index: -1
F_leg_under_OUTLINE_03
  rotate: false
  xy: 939, 786
  size: 46, 117
  orig: 46, 117
  offset: 0, 0
  index: -1
F_leg_under_walk_01B
  rotate: false
  xy: 309, 415
  size: 44, 110
  orig: 44, 110
  offset: 0, 0
  index: -1
F_leg_under_walk_02B
  rotate: true
  xy: 355, 433
  size: 44, 110
  orig: 44, 110
  offset: 0, 0
  index: -1
F_leg_under_walk_03B
  rotate: false
  xy: 927, 674
  size: 44, 110
  orig: 44, 110
  offset: 0, 0
  index: -1
F_leg_up_01_02_03
  rotate: true
  xy: 96, 3
  size: 38, 93
  orig: 38, 93
  offset: 0, 0
  index: -1
F_leg_up_OUTLINE_01_02_03
  rotate: true
  xy: 471, 477
  size: 42, 93
  orig: 42, 93
  offset: 0, 0
  index: -1
F_leg_up_walk_01_02_03B
  rotate: true
  xy: 663, 515
  size: 46, 94
  orig: 46, 94
  offset: 0, 0
  index: -1
F_upperarm_01
  rotate: false
  xy: 481, 382
  size: 64, 54
  orig: 64, 54
  offset: 0, 0
  index: -1
F_upperarm_02
  rotate: true
  xy: 352, 259
  size: 64, 54
  orig: 64, 54
  offset: 0, 0
  index: -1
F_upperarm_03
  rotate: true
  xy: 408, 259
  size: 64, 54
  orig: 64, 54
  offset: 0, 0
  index: -1
another_ivy
  rotate: false
  xy: 2, 392
  size: 32, 630
  orig: 32, 630
  offset: 0, 0
  index: -1
body_01
  rotate: false
  xy: 755, 623
  size: 84, 177
  orig: 84, 177
  offset: 0, 0
  index: -1
body_02
  rotate: false
  xy: 841, 623
  size: 84, 177
  orig: 84, 177
  offset: 0, 0
  index: -1
body_03
  rotate: false
  xy: 114, 245
  size: 84, 177
  orig: 84, 177
  offset: 0, 0
  index: -1
braid_01_02
  rotate: false
  xy: 311, 169
  size: 37, 82
  orig: 37, 82
  offset: 0, 0
  index: -1
braid_03
  rotate: true
  xy: 467, 438
  size: 37, 82
  orig: 37, 82
  offset: 0, 0
  index: -1
decoA_03
  rotate: false
  xy: 1009, 873
  size: 13, 22
  orig: 13, 22
  offset: 0, 0
  index: -1
decoB_03
  rotate: false
  xy: 88, 396
  size: 14, 26
  orig: 14, 26
  offset: 0, 0
  index: -1
decoC_03
  rotate: true
  xy: 464, 259
  size: 12, 25
  orig: 12, 25
  offset: 0, 0
  index: -1
deco_02
  rotate: false
  xy: 606, 450
  size: 14, 26
  orig: 14, 26
  offset: 0, 0
  index: -1
face_attack
  rotate: false
  xy: 976, 507
  size: 46, 41
  orig: 46, 41
  offset: 0, 0
  index: -1
face_damage
  rotate: false
  xy: 976, 464
  size: 46, 41
  orig: 46, 41
  offset: 0, 0
  index: -1
face_dead
  rotate: false
  xy: 403, 325
  size: 46, 41
  orig: 46, 41
  offset: 0, 0
  index: -1
face_idle
  rotate: false
  xy: 451, 325
  size: 46, 41
  orig: 46, 41
  offset: 0, 0
  index: -1
face_win
  rotate: true
  xy: 196, 43
  size: 46, 41
  orig: 46, 41
  offset: 0, 0
  index: -1
front_hair_01_02_03
  rotate: false
  xy: 987, 863
  size: 20, 59
  orig: 20, 59
  offset: 0, 0
  index: -1
head_01_02_03
  rotate: true
  xy: 215, 147
  size: 96, 94
  orig: 96, 94
  offset: 0, 0
  index: -1
ivy_01
  rotate: false
  xy: 36, 646
  size: 117, 376
  orig: 117, 376
  offset: 0, 0
  index: -1
ivy_02
  rotate: false
  xy: 155, 646
  size: 117, 376
  orig: 117, 376
  offset: 0, 0
  index: -1
ivy_03
  rotate: false
  xy: 274, 646
  size: 117, 376
  orig: 117, 376
  offset: 0, 0
  index: -1
ivy_leaf_1
  rotate: true
  xy: 640, 505
  size: 25, 21
  orig: 25, 21
  offset: 0, 0
  index: -1
ivy_leaf_2
  rotate: false
  xy: 575, 447
  size: 29, 29
  orig: 29, 29
  offset: 0, 0
  index: -1
ivy_leaf_3
  rotate: true
  xy: 987, 810
  size: 51, 28
  orig: 51, 28
  offset: 0, 0
  index: -1
kryl_E
  rotate: true
  xy: 759, 514
  size: 59, 81
  orig: 59, 81
  offset: 0, 0
  index: -1
kryl_I
  rotate: false
  xy: 310, 253
  size: 40, 70
  orig: 40, 70
  offset: 0, 0
  index: -1
kryl_K
  rotate: true
  xy: 566, 478
  size: 52, 72
  orig: 52, 72
  offset: 0, 0
  index: -1
kryl_L
  rotate: false
  xy: 987, 976
  size: 34, 46
  orig: 34, 46
  offset: 0, 0
  index: -1
kryl_N
  rotate: false
  xy: 499, 339
  size: 46, 41
  orig: 46, 41
  offset: 0, 0
  index: -1
kryl_T
  rotate: true
  xy: 200, 245
  size: 76, 108
  orig: 76, 108
  offset: 0, 0
  index: -1
kryl_W
  rotate: false
  xy: 986, 634
  size: 36, 38
  orig: 36, 38
  offset: 0, 0
  index: -1
left_ear_01_02
  rotate: false
  xy: 351, 325
  size: 50, 41
  orig: 50, 41
  offset: 0, 0
  index: -1
left_ear_03
  rotate: true
  xy: 973, 674
  size: 50, 41
  orig: 50, 41
  offset: 0, 0
  index: -1
leftside_hair_01_02_03
  rotate: false
  xy: 306, 325
  size: 43, 88
  orig: 43, 88
  offset: 0, 0
  index: -1
leg_deco_02
  rotate: false
  xy: 987, 786
  size: 34, 22
  orig: 34, 22
  offset: 0, 0
  index: -1
leg_deco_03
  rotate: true
  xy: 551, 441
  size: 34, 22
  orig: 34, 22
  offset: 0, 0
  index: -1
leg_deco_OUTLINE_02
  rotate: true
  xy: 991, 550
  size: 39, 19
  orig: 39, 19
  offset: 0, 0
  index: -1
leg_deco_OUTLINE_03
  rotate: false
  xy: 499, 318
  size: 39, 19
  orig: 39, 19
  offset: 0, 0
  index: -1
monster_wand_01
  rotate: false
  xy: 36, 424
  size: 271, 220
  orig: 271, 220
  offset: 0, 0
  index: -1
monster_wand_02
  rotate: false
  xy: 393, 802
  size: 271, 220
  orig: 271, 220
  offset: 0, 0
  index: -1
monster_wand_03
  rotate: false
  xy: 666, 802
  size: 271, 220
  orig: 271, 220
  offset: 0, 0
  index: -1
plant_02
  rotate: false
  xy: 200, 323
  size: 104, 99
  orig: 104, 99
  offset: 0, 0
  index: -1
plant_03
  rotate: true
  xy: 114, 139
  size: 104, 99
  orig: 104, 99
  offset: 0, 0
  index: -1
reaf_01
  rotate: true
  xy: 842, 502
  size: 63, 65
  orig: 63, 65
  offset: 0, 0
  index: -1
reaf_02
  rotate: true
  xy: 909, 485
  size: 63, 65
  orig: 63, 65
  offset: 0, 0
  index: -1
reaf_03
  rotate: true
  xy: 355, 368
  size: 63, 65
  orig: 63, 65
  offset: 0, 0
  index: -1
right_ear_01_02_03
  rotate: false
  xy: 311, 134
  size: 37, 33
  orig: 37, 33
  offset: 0, 0
  index: -1
rightside_hair_01_02_03
  rotate: false
  xy: 2, 2
  size: 45, 91
  orig: 45, 91
  offset: 0, 0
  index: -1
rightside_hair_OUTLINE_01_02_03
  rotate: false
  xy: 49, 2
  size: 45, 91
  orig: 45, 91
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 1009, 897
  size: 25, 13
  orig: 25, 13
  offset: 0, 0
  index: -1
string_01
  rotate: false
  xy: 36, 392
  size: 50, 30
  orig: 50, 30
  offset: 0, 0
  index: -1
string_02
  rotate: true
  xy: 464, 273
  size: 50, 30
  orig: 50, 30
  offset: 0, 0
  index: -1
string_03
  rotate: true
  xy: 987, 924
  size: 50, 30
  orig: 50, 30
  offset: 0, 0
  index: -1
wand_01
  rotate: true
  xy: 393, 709
  size: 91, 360
  orig: 91, 360
  offset: 0, 0
  index: -1
wand_02_03
  rotate: true
  xy: 393, 616
  size: 91, 360
  orig: 91, 360
  offset: 0, 0
  index: -1
wand_head_01
  rotate: false
  xy: 357, 521
  size: 93, 93
  orig: 93, 93
  offset: 0, 0
  index: -1
wand_head_02_03
  rotate: false
  xy: 452, 521
  size: 93, 93
  orig: 93, 93
  offset: 0, 0
  index: -1
