
chara_parts_sword.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_back_under_01
  rotate: true
  xy: 243, 240
  size: 69, 91
  orig: 69, 91
  offset: 0, 0
  index: -1
arm_back_under_02
  rotate: true
  xy: 305, 76
  size: 69, 91
  orig: 69, 91
  offset: 0, 0
  index: -1
arm_back_under_03
  rotate: false
  xy: 398, 54
  size: 69, 91
  orig: 69, 91
  offset: 0, 0
  index: -1
arm_back_upper_01_02_03
  rotate: false
  xy: 901, 891
  size: 38, 75
  orig: 38, 75
  offset: 0, 0
  index: -1
arm_front_under_01
  rotate: false
  xy: 336, 238
  size: 76, 89
  orig: 76, 89
  offset: 0, 0
  index: -1
arm_front_under_02
  rotate: false
  xy: 305, 147
  size: 76, 89
  orig: 76, 89
  offset: 0, 0
  index: -1
arm_front_under_03
  rotate: false
  xy: 383, 147
  size: 76, 89
  orig: 76, 89
  offset: 0, 0
  index: -1
arm_front_upper_01_02_03
  rotate: false
  xy: 373, 540
  size: 74, 51
  orig: 74, 51
  offset: 0, 0
  index: -1
belt_back_01_02
  rotate: false
  xy: 524, 549
  size: 85, 54
  orig: 85, 54
  offset: 0, 0
  index: -1
belt_back_03
  rotate: true
  xy: 468, 550
  size: 85, 54
  orig: 85, 54
  offset: 0, 0
  index: -1
belt_flont_01
  rotate: false
  xy: 121, 162
  size: 89, 84
  orig: 89, 84
  offset: 0, 0
  index: -1
belt_flont_02
  rotate: false
  xy: 121, 76
  size: 89, 84
  orig: 89, 84
  offset: 0, 0
  index: -1
belt_flont_03
  rotate: false
  xy: 810, 882
  size: 89, 84
  orig: 89, 84
  offset: 0, 0
  index: -1
body_01
  rotate: false
  xy: 124, 311
  size: 97, 157
  orig: 97, 157
  offset: 0, 0
  index: -1
body_02
  rotate: false
  xy: 373, 593
  size: 93, 152
  orig: 93, 152
  offset: 0, 0
  index: -1
body_03
  rotate: false
  xy: 622, 822
  size: 93, 152
  orig: 93, 152
  offset: 0, 0
  index: -1
eye_01_02_03
  rotate: false
  xy: 121, 55
  size: 38, 19
  orig: 38, 19
  offset: 0, 0
  index: -1
foot_back_01_02_03
  rotate: false
  xy: 469, 60
  size: 29, 34
  orig: 29, 34
  offset: 0, 0
  index: -1
foot_flont_01_02_03
  rotate: true
  xy: 461, 147
  size: 36, 32
  orig: 36, 32
  offset: 0, 0
  index: -1
hand_back_01_02_03
  rotate: false
  xy: 976, 904
  size: 42, 51
  orig: 42, 51
  offset: 0, 0
  index: -1
hand_front_1_01_02_03
  rotate: true
  xy: 73, 6
  size: 41, 49
  orig: 41, 49
  offset: 0, 0
  index: -1
hand_front_2_01_02_03
  rotate: true
  xy: 980, 866
  size: 36, 42
  orig: 36, 42
  offset: 0, 0
  index: -1
head_back_01_02_03
  rotate: true
  xy: 461, 185
  size: 51, 37
  orig: 51, 37
  offset: 0, 0
  index: -1
head_flont_01
  rotate: true
  xy: 945, 957
  size: 65, 74
  orig: 65, 74
  offset: 0, 0
  index: -1
head_flont_02
  rotate: false
  xy: 386, 464
  size: 65, 74
  orig: 65, 74
  offset: 0, 0
  index: -1
head_flont_03
  rotate: false
  xy: 453, 474
  size: 65, 74
  orig: 65, 74
  offset: 0, 0
  index: -1
helm_02
  rotate: false
  xy: 717, 841
  size: 91, 133
  orig: 91, 133
  offset: 0, 0
  index: -1
helm_03
  rotate: true
  xy: 251, 446
  size: 91, 133
  orig: 91, 133
  offset: 0, 0
  index: -1
helm_acce_03
  rotate: false
  xy: 316, 329
  size: 101, 115
  orig: 101, 115
  offset: 0, 0
  index: -1
helm_back_02
  rotate: false
  xy: 223, 311
  size: 91, 133
  orig: 91, 133
  offset: 0, 0
  index: -1
jaw_01_02_03
  rotate: false
  xy: 469, 96
  size: 38, 49
  orig: 38, 49
  offset: 0, 0
  index: -1
leg_back_under_01
  rotate: false
  xy: 414, 238
  size: 66, 89
  orig: 66, 89
  offset: 0, 0
  index: -1
leg_back_under_02
  rotate: true
  xy: 810, 814
  size: 66, 89
  orig: 66, 89
  offset: 0, 0
  index: -1
leg_back_under_03
  rotate: true
  xy: 810, 746
  size: 66, 89
  orig: 66, 89
  offset: 0, 0
  index: -1
leg_back_upper_01_02
  rotate: true
  xy: 524, 605
  size: 68, 91
  orig: 68, 91
  offset: 0, 0
  index: -1
leg_back_upper_03
  rotate: true
  xy: 717, 771
  size: 68, 91
  orig: 68, 91
  offset: 0, 0
  index: -1
leg_front_under_01
  rotate: false
  xy: 524, 675
  size: 91, 76
  orig: 91, 76
  offset: 0, 0
  index: -1
leg_front_under_02
  rotate: false
  xy: 212, 162
  size: 91, 76
  orig: 91, 76
  offset: 0, 0
  index: -1
leg_front_under_03
  rotate: false
  xy: 212, 84
  size: 91, 76
  orig: 91, 76
  offset: 0, 0
  index: -1
leg_front_upper_01_02
  rotate: false
  xy: 468, 637
  size: 54, 108
  orig: 54, 108
  offset: 0, 0
  index: -1
leg_front_upper_03
  rotate: true
  xy: 835, 968
  size: 54, 108
  orig: 54, 108
  offset: 0, 0
  index: -1
mant_back_01
  rotate: false
  xy: 2, 747
  size: 247, 275
  orig: 247, 275
  offset: 0, 0
  index: -1
mant_back_02
  rotate: false
  xy: 2, 470
  size: 247, 275
  orig: 247, 275
  offset: 0, 0
  index: -1
mant_back_03
  rotate: false
  xy: 251, 747
  size: 247, 275
  orig: 247, 275
  offset: 0, 0
  index: -1
mant_button_01_02
  rotate: false
  xy: 124, 26
  size: 25, 27
  orig: 25, 27
  offset: 0, 0
  index: -1
mant_button_03
  rotate: true
  xy: 980, 839
  size: 25, 27
  orig: 25, 27
  offset: 0, 0
  index: -1
mant_front_01
  rotate: false
  xy: 500, 753
  size: 117, 61
  orig: 117, 61
  offset: 0, 0
  index: -1
mant_front_02_03
  rotate: false
  xy: 124, 248
  size: 117, 61
  orig: 117, 61
  offset: 0, 0
  index: -1
mant_shoulder_02_03
  rotate: false
  xy: 520, 480
  size: 64, 67
  orig: 64, 67
  offset: 0, 0
  index: -1
pelvis_01
  rotate: true
  xy: 901, 764
  size: 60, 37
  orig: 60, 37
  offset: 0, 0
  index: -1
pelvis_02_03
  rotate: true
  xy: 586, 494
  size: 53, 33
  orig: 53, 33
  offset: 0, 0
  index: -1
scapula_back_01_02_03
  rotate: false
  xy: 943, 838
  size: 35, 54
  orig: 35, 54
  offset: 0, 0
  index: -1
scapula_front_01
  rotate: false
  xy: 901, 826
  size: 40, 63
  orig: 40, 63
  offset: 0, 0
  index: -1
scapula_front_02_03
  rotate: false
  xy: 941, 894
  size: 33, 61
  orig: 33, 61
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 1009, 839
  size: 25, 13
  orig: 25, 13
  offset: 0, 0
  index: -1
shield_01
  rotate: false
  xy: 2, 262
  size: 120, 206
  orig: 120, 206
  offset: 0, 0
  index: -1
shield_02
  rotate: false
  xy: 251, 539
  size: 120, 206
  orig: 120, 206
  offset: 0, 0
  index: -1
shield_03
  rotate: false
  xy: 500, 816
  size: 120, 206
  orig: 120, 206
  offset: 0, 0
  index: -1
sword_01
  rotate: true
  xy: 622, 976
  size: 46, 211
  orig: 46, 211
  offset: 0, 0
  index: -1
sword_02
  rotate: false
  xy: 73, 49
  size: 46, 211
  orig: 46, 211
  offset: 0, 0
  index: -1
sword_03
  rotate: false
  xy: 2, 10
  size: 69, 250
  orig: 69, 250
  offset: 0, 0
  index: -1
