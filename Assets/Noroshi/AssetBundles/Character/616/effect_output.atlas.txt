
effect_output.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
arrow_01_02_03
  rotate: false
  xy: 132, 1245
  size: 162, 30
  orig: 164, 34
  offset: 1, 2
  index: -1
bow_effect
  rotate: true
  xy: 2, 628
  size: 342, 32
  orig: 342, 32
  offset: 0, 0
  index: -1
bow_spark_alpha
  rotate: false
  xy: 885, 1516
  size: 68, 68
  orig: 68, 68
  offset: 0, 0
  index: -1
bow_spark_effect0001
  rotate: false
  xy: 377, 761
  size: 101, 216
  orig: 105, 222
  offset: 0, 6
  index: -1
bow_spark_effect0002
  rotate: false
  xy: 273, 771
  size: 102, 222
  orig: 105, 222
  offset: 3, 0
  index: -1
bow_spark_effect0003
  rotate: false
  xy: 1925, 1807
  size: 92, 217
  orig: 105, 222
  offset: 9, 5
  index: -1
bow_spark_effect0004
  rotate: true
  xy: 717, 1639
  size: 81, 192
  orig: 105, 222
  offset: 22, 22
  index: -1
caster_attack
  rotate: false
  xy: 570, 1224
  size: 37, 37
  orig: 39, 39
  offset: 1, 1
  index: -1
caster_attack_alpha
  rotate: false
  xy: 36, 640
  size: 58, 58
  orig: 79, 79
  offset: 11, 10
  index: -1
effect_lance_01
  rotate: false
  xy: 112, 1517
  size: 56, 507
  orig: 108, 529
  offset: 24, 22
  index: -1
effect_lance_02
  rotate: false
  xy: 2, 972
  size: 65, 521
  orig: 108, 528
  offset: 22, 7
  index: -1
effect_lance_03
  rotate: false
  xy: 2, 1495
  size: 108, 529
  orig: 108, 529
  offset: 0, 0
  index: -1
effect_sword_01
  rotate: false
  xy: 664, 1537
  size: 51, 215
  orig: 51, 215
  offset: 0, 0
  index: -1
effect_sword_02
  rotate: true
  xy: 717, 1586
  size: 51, 215
  orig: 51, 215
  offset: 0, 0
  index: -1
effect_sword_03
  rotate: true
  xy: 351, 1511
  size: 67, 250
  orig: 69, 250
  offset: 1, 0
  index: -1
enchant_alpha
  rotate: false
  xy: 570, 1263
  size: 88, 88
  orig: 88, 88
  offset: 0, 0
  index: -1
enchant_effect0001
  rotate: false
  xy: 592, 1584
  size: 70, 168
  orig: 115, 261
  offset: 20, 61
  index: -1
enchant_effect0002
  rotate: true
  xy: 717, 1518
  size: 66, 166
  orig: 115, 261
  offset: 23, 65
  index: -1
enchant_effect0003
  rotate: false
  xy: 560, 1353
  size: 72, 156
  orig: 115, 261
  offset: 21, 74
  index: -1
enchant_effect0004
  rotate: false
  xy: 182, 541
  size: 68, 116
  orig: 115, 261
  offset: 26, 86
  index: -1
enchant_effect0005
  rotate: true
  xy: 2, 2
  size: 78, 146
  orig: 115, 261
  offset: 16, 55
  index: -1
enchant_effect0006
  rotate: false
  xy: 104, 359
  size: 72, 147
  orig: 115, 261
  offset: 21, 57
  index: -1
enchant_effect0007
  rotate: false
  xy: 104, 508
  size: 76, 149
  orig: 115, 261
  offset: 18, 63
  index: -1
enchant_effect0008
  rotate: true
  xy: 112, 659
  size: 76, 155
  orig: 115, 261
  offset: 17, 68
  index: -1
enchant_slash0001
  rotate: false
  xy: 170, 1773
  size: 251, 251
  orig: 258, 265
  offset: 0, 12
  index: -1
enchant_slash0002
  rotate: false
  xy: 170, 1517
  size: 179, 254
  orig: 258, 265
  offset: 0, 8
  index: -1
enchant_slash0003
  rotate: false
  xy: 503, 995
  size: 81, 222
  orig: 258, 265
  offset: 0, 5
  index: -1
enchant_spear_effect0001
  rotate: true
  xy: 36, 700
  size: 270, 74
  orig: 361, 74
  offset: 29, 0
  index: -1
enchant_spear_effect0002
  rotate: true
  xy: 69, 1211
  size: 282, 61
  orig: 361, 74
  offset: 37, 7
  index: -1
enchant_spear_effect0003
  rotate: false
  xy: 717, 1722
  size: 230, 42
  orig: 361, 74
  offset: 28, 14
  index: -1
enchant_start_effect0001
  rotate: false
  xy: 396, 979
  size: 105, 238
  orig: 174, 377
  offset: 44, 0
  index: -1
enchant_start_effect0002
  rotate: true
  xy: 307, 1219
  size: 130, 261
  orig: 174, 377
  offset: 29, 0
  index: -1
enchant_start_effect0003
  rotate: false
  xy: 2, 82
  size: 142, 268
  orig: 174, 377
  offset: 17, 13
  index: -1
enchant_start_effect0004
  rotate: true
  xy: 307, 1351
  size: 158, 251
  orig: 174, 377
  offset: 7, 51
  index: -1
enchant_start_effect0005
  rotate: false
  xy: 112, 737
  size: 159, 239
  orig: 174, 377
  offset: 7, 69
  index: -1
enchant_start_effect0006
  rotate: true
  xy: 351, 1580
  size: 172, 239
  orig: 174, 377
  offset: 1, 76
  index: -1
enchant_start_effect0007
  rotate: false
  xy: 132, 1277
  size: 173, 238
  orig: 174, 377
  offset: 1, 83
  index: -1
enchant_start_effect0008
  rotate: false
  xy: 69, 978
  size: 168, 231
  orig: 174, 377
  offset: 3, 91
  index: -1
enchant_start_effect0009
  rotate: false
  xy: 239, 995
  size: 155, 222
  orig: 174, 377
  offset: 7, 99
  index: -1
fire_alpha
  rotate: false
  xy: 911, 1652
  size: 68, 68
  orig: 68, 68
  offset: 0, 0
  index: -1
fire_pillar0001
  rotate: true
  xy: 1641, 1876
  size: 148, 282
  orig: 169, 308
  offset: 16, 0
  index: -1
fire_pillar0002
  rotate: true
  xy: 423, 1881
  size: 143, 308
  orig: 169, 308
  offset: 21, 0
  index: -1
fire_pillar0003
  rotate: true
  xy: 733, 1881
  size: 143, 304
  orig: 169, 308
  offset: 21, 0
  index: -1
fire_pillar0004
  rotate: true
  xy: 1039, 1886
  size: 138, 299
  orig: 169, 308
  offset: 20, 0
  index: -1
fire_pillar0005
  rotate: true
  xy: 1340, 1889
  size: 135, 299
  orig: 169, 308
  offset: 24, 0
  index: -1
fire_pillar0006
  rotate: true
  xy: 423, 1754
  size: 125, 292
  orig: 169, 308
  offset: 33, 0
  index: -1
fire_pillar0007
  rotate: true
  xy: 717, 1766
  size: 113, 292
  orig: 169, 308
  offset: 39, 0
  index: -1
fire_pillar0008
  rotate: false
  xy: 2, 352
  size: 100, 274
  orig: 169, 308
  offset: 49, 0
  index: -1
