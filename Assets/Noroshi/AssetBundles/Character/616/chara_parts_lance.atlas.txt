
chara_parts_lance.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_back_under_01
  rotate: true
  xy: 602, 590
  size: 69, 91
  orig: 69, 91
  offset: 0, 0
  index: -1
arm_back_under_02
  rotate: true
  xy: 695, 590
  size: 69, 91
  orig: 69, 91
  offset: 0, 0
  index: -1
arm_back_under_03
  rotate: true
  xy: 687, 519
  size: 69, 91
  orig: 69, 91
  offset: 0, 0
  index: -1
arm_back_upper_01_02_03
  rotate: false
  xy: 409, 125
  size: 26, 72
  orig: 26, 72
  offset: 0, 0
  index: -1
arm_front_under_01
  rotate: false
  xy: 472, 431
  size: 76, 82
  orig: 76, 82
  offset: 0, 0
  index: -1
arm_front_under_02
  rotate: false
  xy: 331, 115
  size: 76, 82
  orig: 76, 82
  offset: 0, 0
  index: -1
arm_front_under_03
  rotate: false
  xy: 788, 577
  size: 76, 82
  orig: 76, 82
  offset: 0, 0
  index: -1
arm_front_upper_01_02_03
  rotate: false
  xy: 700, 467
  size: 64, 50
  orig: 64, 50
  offset: 0, 0
  index: -1
belt_back_01_02
  rotate: true
  xy: 255, 11
  size: 85, 54
  orig: 85, 54
  offset: 0, 0
  index: -1
belt_back_03
  rotate: false
  xy: 344, 199
  size: 85, 54
  orig: 85, 54
  offset: 0, 0
  index: -1
belt_flont_01
  rotate: false
  xy: 752, 661
  size: 89, 84
  orig: 89, 84
  offset: 0, 0
  index: -1
belt_flont_02
  rotate: true
  xy: 843, 663
  size: 89, 84
  orig: 89, 84
  offset: 0, 0
  index: -1
belt_flont_03
  rotate: true
  xy: 344, 255
  size: 89, 84
  orig: 89, 84
  offset: 0, 0
  index: -1
body_01
  rotate: true
  xy: 332, 526
  size: 84, 148
  orig: 84, 148
  offset: 0, 0
  index: -1
body_02
  rotate: true
  xy: 602, 661
  size: 84, 148
  orig: 84, 148
  offset: 0, 0
  index: -1
body_03
  rotate: false
  xy: 251, 345
  size: 84, 148
  orig: 84, 148
  offset: 0, 0
  index: -1
eye_01_02_03
  rotate: false
  xy: 929, 670
  size: 38, 19
  orig: 38, 19
  offset: 0, 0
  index: -1
foot_back_01_02_03
  rotate: true
  xy: 969, 889
  size: 29, 34
  orig: 29, 34
  offset: 0, 0
  index: -1
foot_flont_01_02_03
  rotate: false
  xy: 986, 762
  size: 36, 32
  orig: 36, 32
  offset: 0, 0
  index: -1
hand_back_01_02_03
  rotate: false
  xy: 934, 624
  size: 37, 44
  orig: 37, 44
  offset: 0, 0
  index: -1
hand_flont_1_01_02_03
  rotate: false
  xy: 944, 537
  size: 30, 42
  orig: 30, 42
  offset: 0, 0
  index: -1
hand_front_2_01_02_03
  rotate: false
  xy: 934, 581
  size: 32, 41
  orig: 32, 41
  offset: 0, 0
  index: -1
head_back_01_02_03
  rotate: true
  xy: 970, 836
  size: 51, 37
  orig: 51, 37
  offset: 0, 0
  index: -1
head_flont_01
  rotate: true
  xy: 780, 510
  size: 65, 74
  orig: 65, 74
  offset: 0, 0
  index: -1
head_flont_02
  rotate: false
  xy: 188, 7
  size: 65, 74
  orig: 65, 74
  offset: 0, 0
  index: -1
head_flont_03
  rotate: false
  xy: 586, 436
  size: 65, 74
  orig: 65, 74
  offset: 0, 0
  index: -1
helm_02
  rotate: false
  xy: 141, 83
  size: 91, 133
  orig: 91, 133
  offset: 0, 0
  index: -1
helm_03
  rotate: false
  xy: 830, 754
  size: 91, 133
  orig: 91, 133
  offset: 0, 0
  index: -1
helm_acce_03
  rotate: false
  xy: 471, 612
  size: 129, 133
  orig: 129, 133
  offset: 0, 0
  index: -1
helm_back_02
  rotate: false
  xy: 251, 210
  size: 91, 133
  orig: 91, 133
  offset: 0, 0
  index: -1
jaw_01_02_03
  rotate: true
  xy: 970, 796
  size: 38, 49
  orig: 38, 49
  offset: 0, 0
  index: -1
lance_01
  rotate: false
  xy: 2, 495
  size: 108, 527
  orig: 108, 527
  offset: 0, 0
  index: -1
lance_02
  rotate: false
  xy: 112, 495
  size: 108, 527
  orig: 108, 527
  offset: 0, 0
  index: -1
lance_03
  rotate: false
  xy: 222, 495
  size: 108, 527
  orig: 108, 527
  offset: 0, 0
  index: -1
lance_cloth_03
  rotate: false
  xy: 944, 507
  size: 42, 28
  orig: 42, 28
  offset: 0, 0
  index: -1
lance_ribbon_back_03
  rotate: false
  xy: 381, 56
  size: 33, 57
  orig: 33, 57
  offset: 0, 0
  index: -1
lance_ribbon_front_03
  rotate: false
  xy: 550, 436
  size: 34, 77
  orig: 34, 77
  offset: 0, 0
  index: -1
leg_back_under_01
  rotate: true
  xy: 337, 346
  size: 66, 86
  orig: 66, 86
  offset: 0, 0
  index: -1
leg_back_under_02
  rotate: false
  xy: 866, 575
  size: 66, 86
  orig: 66, 86
  offset: 0, 0
  index: -1
leg_back_under_03
  rotate: true
  xy: 856, 507
  size: 66, 86
  orig: 66, 86
  offset: 0, 0
  index: -1
leg_back_upper_01_02
  rotate: false
  xy: 653, 418
  size: 45, 92
  orig: 45, 92
  offset: 0, 0
  index: -1
leg_back_upper_03
  rotate: false
  xy: 923, 795
  size: 45, 92
  orig: 45, 92
  offset: 0, 0
  index: -1
leg_front_under_01
  rotate: false
  xy: 2, 5
  size: 91, 76
  orig: 91, 76
  offset: 0, 0
  index: -1
leg_front_under_02
  rotate: false
  xy: 95, 5
  size: 91, 76
  orig: 91, 76
  offset: 0, 0
  index: -1
leg_front_under_03
  rotate: false
  xy: 594, 512
  size: 91, 76
  orig: 91, 76
  offset: 0, 0
  index: -1
leg_front_upper_01_02
  rotate: false
  xy: 434, 422
  size: 36, 102
  orig: 36, 102
  offset: 0, 0
  index: -1
leg_front_upper_03
  rotate: false
  xy: 969, 920
  size: 36, 102
  orig: 36, 102
  offset: 0, 0
  index: -1
mant_back_01
  rotate: false
  xy: 2, 218
  size: 247, 275
  orig: 247, 275
  offset: 0, 0
  index: -1
mant_back_02
  rotate: false
  xy: 332, 747
  size: 247, 275
  orig: 247, 275
  offset: 0, 0
  index: -1
mant_back_03
  rotate: false
  xy: 581, 747
  size: 247, 275
  orig: 247, 275
  offset: 0, 0
  index: -1
mant_button_01_02
  rotate: false
  xy: 311, 9
  size: 24, 24
  orig: 24, 24
  offset: 0, 0
  index: -1
mant_button_03
  rotate: false
  xy: 829, 484
  size: 24, 24
  orig: 24, 24
  offset: 0, 0
  index: -1
mant_front_01
  rotate: true
  xy: 482, 515
  size: 95, 110
  orig: 95, 110
  offset: 0, 0
  index: -1
mant_front_02
  rotate: false
  xy: 234, 98
  size: 95, 110
  orig: 95, 110
  offset: 0, 0
  index: -1
mant_front_03
  rotate: false
  xy: 337, 414
  size: 95, 110
  orig: 95, 110
  offset: 0, 0
  index: -1
mant_shoulder_01
  rotate: false
  xy: 700, 426
  size: 61, 39
  orig: 61, 39
  offset: 0, 0
  index: -1
mant_shoulder_02_03
  rotate: false
  xy: 923, 754
  size: 61, 39
  orig: 61, 39
  offset: 0, 0
  index: -1
pelvis_01
  rotate: true
  xy: 929, 691
  size: 61, 39
  orig: 61, 39
  offset: 0, 0
  index: -1
pelvis_02_03
  rotate: false
  xy: 766, 469
  size: 61, 39
  orig: 61, 39
  offset: 0, 0
  index: -1
scapula_back_01_02_03
  rotate: false
  xy: 970, 698
  size: 35, 54
  orig: 35, 54
  offset: 0, 0
  index: -1
scapula_front_01
  rotate: false
  xy: 311, 35
  size: 33, 61
  orig: 33, 61
  offset: 0, 0
  index: -1
scapula_front_02_03
  rotate: false
  xy: 346, 52
  size: 33, 61
  orig: 33, 61
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 1007, 972
  size: 50, 14
  orig: 50, 14
  offset: 0, 0
  index: -1
shield_01
  rotate: false
  xy: 2, 83
  size: 137, 133
  orig: 137, 133
  offset: 0, 0
  index: -1
shield_02
  rotate: false
  xy: 332, 612
  size: 137, 133
  orig: 137, 133
  offset: 0, 0
  index: -1
shield_03
  rotate: false
  xy: 830, 889
  size: 137, 133
  orig: 137, 133
  offset: 0, 0
  index: -1
