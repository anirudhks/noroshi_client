
chara_parts.png
size: 2048,512
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_back
  rotate: true
  xy: 622, 223
  size: 76, 77
  orig: 76, 77
  offset: 0, 0
  index: -1
arm_back_part
  rotate: true
  xy: 542, 101
  size: 39, 47
  orig: 39, 47
  offset: 0, 0
  index: -1
arm_front
  rotate: true
  xy: 1243, 203
  size: 91, 64
  orig: 91, 64
  offset: 0, 0
  index: -1
arm_front_part
  rotate: false
  xy: 494, 363
  size: 44, 49
  orig: 44, 49
  offset: 0, 0
  index: -1
cape_neck
  rotate: false
  xy: 701, 223
  size: 75, 76
  orig: 75, 76
  offset: 0, 0
  index: -1
chiest
  rotate: false
  xy: 802, 302
  size: 109, 68
  orig: 109, 68
  offset: 0, 0
  index: -1
chin
  rotate: false
  xy: 299, 407
  size: 54, 63
  orig: 54, 63
  offset: 0, 0
  index: -1
chin-dead
  rotate: true
  xy: 106, 2
  size: 54, 63
  orig: 54, 63
  offset: 0, 0
  index: -1
evo1
  rotate: false
  xy: 1382, 293
  size: 44, 34
  orig: 44, 34
  offset: 0, 0
  index: -1
evo1-2_leg_front_part
  rotate: false
  xy: 1931, 410
  size: 112, 100
  orig: 112, 100
  offset: 0, 0
  index: -1
evo1_arm_front_pile_01
  rotate: false
  xy: 1896, 369
  size: 20, 21
  orig: 20, 21
  offset: 0, 0
  index: -1
evo1_cape
  rotate: false
  xy: 1465, 380
  size: 149, 130
  orig: 149, 130
  offset: 0, 0
  index: -1
evo1_head_top_part
  rotate: false
  xy: 419, 65
  size: 69, 89
  orig: 69, 89
  offset: 0, 0
  index: -1
evo1_knife_front
  rotate: false
  xy: 913, 323
  size: 124, 47
  orig: 124, 47
  offset: 0, 0
  index: -1
evo2-3_body_pile_01
  rotate: true
  xy: 500, 207
  size: 39, 38
  orig: 39, 38
  offset: 0, 0
  index: -1
evo2-3_body_pile_02
  rotate: true
  xy: 330, 16
  size: 51, 69
  orig: 51, 69
  offset: 0, 0
  index: -1
evo2-3_body_pile_03
  rotate: false
  xy: 500, 248
  size: 38, 69
  orig: 38, 69
  offset: 0, 0
  index: -1
evo2-3_finger_back_01
  rotate: true
  xy: 494, 319
  size: 42, 44
  orig: 42, 44
  offset: 0, 0
  index: -1
evo2-3_finger_back_02
  rotate: true
  xy: 490, 64
  size: 33, 51
  orig: 33, 51
  offset: 0, 0
  index: -1
evo2-3_finger_back_03
  rotate: false
  xy: 546, 142
  size: 49, 36
  orig: 49, 36
  offset: 0, 0
  index: -1
evo2-3_finger_back_04
  rotate: true
  xy: 2022, 369
  size: 39, 24
  orig: 39, 24
  offset: 0, 0
  index: -1
evo2_arm_back_pile
  rotate: false
  xy: 597, 198
  size: 23, 29
  orig: 23, 29
  offset: 0, 0
  index: -1
evo2_arm_front_pile_01
  rotate: true
  xy: 543, 57
  size: 42, 52
  orig: 42, 52
  offset: 0, 0
  index: -1
evo2_arm_front_pile_02
  rotate: false
  xy: 1428, 285
  size: 29, 44
  orig: 29, 44
  offset: 0, 0
  index: -1
evo2_cape
  rotate: false
  xy: 1313, 380
  size: 150, 130
  orig: 150, 130
  offset: 0, 0
  index: -1
evo2_finger_01
  rotate: true
  xy: 1295, 296
  size: 26, 36
  orig: 26, 36
  offset: 0, 0
  index: -1
evo2_finger_02
  rotate: true
  xy: 1554, 344
  size: 34, 47
  orig: 34, 47
  offset: 0, 0
  index: -1
evo2_finger_03
  rotate: false
  xy: 1330, 329
  size: 42, 49
  orig: 42, 49
  offset: 0, 0
  index: -1
evo2_finger_04
  rotate: false
  xy: 533, 28
  size: 44, 27
  orig: 44, 27
  offset: 0, 0
  index: -1
evo2_foot_front_part
  rotate: false
  xy: 299, 332
  size: 50, 73
  orig: 50, 73
  offset: 0, 0
  index: -1
evo2_head_top_part
  rotate: false
  xy: 110, 127
  size: 93, 136
  orig: 93, 136
  offset: 0, 0
  index: -1
evo2_knife_front
  rotate: false
  xy: 913, 274
  size: 124, 47
  orig: 124, 47
  offset: 0, 0
  index: -1
evo2_leg_front_pile_01
  rotate: true
  xy: 401, 14
  size: 49, 59
  orig: 49, 59
  offset: 0, 0
  index: -1
evo2_leg_front_pile_02
  rotate: false
  xy: 1309, 255
  size: 33, 36
  orig: 33, 36
  offset: 0, 0
  index: -1
evo3_arm_back_part
  rotate: false
  xy: 1418, 331
  size: 39, 47
  orig: 39, 47
  offset: 0, 0
  index: -1
evo3_arm_back_pile
  rotate: false
  xy: 349, 205
  size: 95, 93
  orig: 95, 93
  offset: 0, 0
  index: -1
evo3_arm_front
  rotate: true
  xy: 2, 60
  size: 203, 106
  orig: 203, 106
  offset: 0, 0
  index: -1
evo3_cape
  rotate: false
  xy: 1160, 377
  size: 151, 133
  orig: 151, 133
  offset: 0, 0
  index: -1
evo3_cape_neck
  rotate: false
  xy: 328, 69
  size: 89, 85
  orig: 89, 85
  offset: 0, 0
  index: -1
evo3_foot_front
  rotate: true
  xy: 1616, 344
  size: 61, 139
  orig: 61, 139
  offset: 0, 0
  index: -1
evo3_head_part_right
  rotate: false
  xy: 446, 206
  size: 21, 92
  orig: 21, 92
  offset: 0, 0
  index: -1
evo3_head_top_part
  rotate: false
  xy: 205, 127
  size: 93, 136
  orig: 93, 136
  offset: 0, 0
  index: -1
evo3_knife
  rotate: false
  xy: 1760, 392
  size: 156, 46
  orig: 156, 46
  offset: 0, 0
  index: -1
evo3_leg_front
  rotate: false
  xy: 2, 265
  size: 140, 205
  orig: 140, 205
  offset: 0, 0
  index: -1
evo3_neck
  rotate: false
  xy: 494, 153
  size: 50, 52
  orig: 50, 52
  offset: 0, 0
  index: -1
evo3_shoulder_arm_front
  rotate: true
  xy: 1160, 296
  size: 79, 133
  orig: 79, 133
  offset: 0, 0
  index: -1
evo3_shoulder_arm_front_pile
  rotate: false
  xy: 546, 180
  size: 49, 47
  orig: 49, 47
  offset: 0, 0
  index: -1
face_bace
  rotate: true
  xy: 540, 229
  size: 70, 80
  orig: 70, 80
  offset: 0, 0
  index: -1
face_dead
  rotate: false
  xy: 1309, 223
  size: 33, 30
  orig: 33, 30
  offset: 0, 0
  index: -1
face_nomal
  rotate: false
  xy: 1344, 261
  size: 33, 30
  orig: 33, 30
  offset: 0, 0
  index: -1
finger_01
  rotate: false
  xy: 462, 27
  size: 26, 36
  orig: 26, 36
  offset: 0, 0
  index: -1
finger_02
  rotate: true
  xy: 1333, 293
  size: 34, 47
  orig: 34, 47
  offset: 0, 0
  index: -1
finger_03
  rotate: false
  xy: 1374, 329
  size: 42, 49
  orig: 42, 49
  offset: 0, 0
  index: -1
finger_04
  rotate: true
  xy: 597, 152
  size: 44, 27
  orig: 44, 27
  offset: 0, 0
  index: -1
finger_back_01
  rotate: false
  xy: 1459, 334
  size: 42, 44
  orig: 42, 44
  offset: 0, 0
  index: -1
finger_back_02
  rotate: false
  xy: 1295, 324
  size: 33, 51
  orig: 33, 51
  offset: 0, 0
  index: -1
finger_back_03
  rotate: false
  xy: 1503, 342
  size: 49, 36
  orig: 49, 36
  offset: 0, 0
  index: -1
finger_back_04
  rotate: true
  xy: 2022, 328
  size: 39, 24
  orig: 39, 24
  offset: 0, 0
  index: -1
foot_back
  rotate: false
  xy: 1039, 225
  size: 101, 98
  orig: 101, 98
  offset: 0, 0
  index: -1
foot_back_part
  rotate: false
  xy: 453, 156
  size: 39, 48
  orig: 39, 48
  offset: 0, 0
  index: -1
foot_front
  rotate: true
  xy: 1039, 325
  size: 45, 114
  orig: 45, 114
  offset: 0, 0
  index: -1
foot_front_pile_02
  rotate: false
  xy: 1459, 291
  size: 41, 41
  orig: 41, 41
  offset: 0, 0
  index: -1
hand_back
  rotate: false
  xy: 490, 24
  size: 41, 38
  orig: 41, 38
  offset: 0, 0
  index: -1
hand_front
  rotate: true
  xy: 455, 311
  size: 36, 37
  orig: 36, 37
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 1142, 213
  size: 81, 99
  orig: 81, 99
  offset: 0, 0
  index: -1
head_part_right
  rotate: false
  xy: 778, 207
  size: 21, 92
  orig: 21, 92
  offset: 0, 0
  index: -1
leg_back
  rotate: false
  xy: 469, 212
  size: 29, 97
  orig: 29, 97
  offset: 0, 0
  index: -1
leg_front
  rotate: false
  xy: 275, 6
  size: 53, 57
  orig: 53, 57
  offset: 0, 0
  index: -1
neck
  rotate: false
  xy: 490, 99
  size: 50, 52
  orig: 50, 52
  offset: 0, 0
  index: -1
pelvis
  rotate: true
  xy: 290, 266
  size: 64, 57
  orig: 64, 57
  offset: 0, 0
  index: -1
poison_effect0001
  rotate: false
  xy: 144, 332
  size: 153, 138
  orig: 153, 138
  offset: 0, 0
  index: -1
poison_effect0002
  rotate: false
  xy: 540, 372
  size: 153, 138
  orig: 153, 138
  offset: 0, 0
  index: -1
poison_effect0003
  rotate: false
  xy: 695, 372
  size: 153, 138
  orig: 153, 138
  offset: 0, 0
  index: -1
poison_effect0004
  rotate: false
  xy: 850, 372
  size: 153, 138
  orig: 153, 138
  offset: 0, 0
  index: -1
poison_effect0005
  rotate: false
  xy: 1005, 372
  size: 153, 138
  orig: 153, 138
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 300, 147
  size: 25, 13
  orig: 25, 13
  offset: 0, 0
  index: -1
shoulder_arm_front
  rotate: true
  xy: 674, 301
  size: 69, 126
  orig: 69, 126
  offset: 0, 0
  index: -1
shoulder_back
  rotate: true
  xy: 253, 65
  size: 60, 73
  orig: 60, 73
  offset: 0, 0
  index: -1
slaver0001
  rotate: true
  xy: 2, 11
  size: 47, 102
  orig: 47, 102
  offset: 0, 0
  index: -1
slaver0002
  rotate: true
  xy: 171, 9
  size: 47, 102
  orig: 47, 102
  offset: 0, 0
  index: -1
slaver0003
  rotate: false
  xy: 300, 162
  size: 47, 102
  orig: 47, 102
  offset: 0, 0
  index: -1
slaver0004
  rotate: true
  xy: 351, 300
  size: 47, 102
  orig: 47, 102
  offset: 0, 0
  index: -1
slaver0005
  rotate: true
  xy: 349, 156
  size: 47, 102
  orig: 47, 102
  offset: 0, 0
  index: -1
slaver0006
  rotate: true
  xy: 1918, 361
  size: 47, 102
  orig: 47, 102
  offset: 0, 0
  index: -1
slaver0007
  rotate: true
  xy: 1896, 312
  size: 47, 102
  orig: 47, 102
  offset: 0, 0
  index: -1
tail_front
  rotate: false
  xy: 2, 472
  size: 351, 38
  orig: 351, 38
  offset: 0, 0
  index: -1
txt_!_1
  rotate: true
  xy: 144, 265
  size: 65, 144
  orig: 65, 144
  offset: 0, 0
  index: -1
txt_!_2
  rotate: true
  xy: 540, 301
  size: 69, 132
  orig: 69, 132
  offset: 0, 0
  index: -1
txt_H_1
  rotate: true
  xy: 1760, 440
  size: 70, 169
  orig: 70, 169
  offset: 0, 0
  index: -1
txt_H_2
  rotate: true
  xy: 1616, 407
  size: 103, 142
  orig: 103, 142
  offset: 0, 0
  index: -1
txt_O_1
  rotate: true
  xy: 1757, 330
  size: 60, 137
  orig: 60, 137
  offset: 0, 0
  index: -1
txt_O_2
  rotate: true
  xy: 355, 349
  size: 63, 137
  orig: 63, 137
  offset: 0, 0
  index: -1
txt_S
  rotate: true
  xy: 110, 58
  size: 67, 141
  orig: 67, 141
  offset: 0, 0
  index: -1
txt_W
  rotate: true
  xy: 355, 414
  size: 96, 183
  orig: 96, 183
  offset: 0, 0
  index: -1
