
chara_parts.png
size: 1024,128
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_back
  rotate: true
  xy: 108, 31
  size: 30, 82
  orig: 30, 82
  offset: 0, 0
  index: -1
arm_front_anime_up
  rotate: false
  xy: 639, 86
  size: 75, 40
  orig: 75, 40
  offset: 0, 0
  index: -1
arm_front_anime_up_claw_01
  rotate: false
  xy: 995, 91
  size: 27, 35
  orig: 27, 35
  offset: 0, 0
  index: -1
arm_front_anime_up_claw_02
  rotate: false
  xy: 192, 28
  size: 31, 33
  orig: 31, 33
  offset: 0, 0
  index: -1
belly_button
  rotate: false
  xy: 299, 28
  size: 19, 19
  orig: 19, 19
  offset: 0, 0
  index: -1
big_mouse
  rotate: false
  xy: 560, 81
  size: 77, 45
  orig: 77, 45
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 410, 72
  size: 54, 73
  orig: 54, 73
  offset: 0, 0
  index: -1
bom_back
  rotate: false
  xy: 226, 49
  size: 94, 77
  orig: 94, 77
  offset: 0, 0
  index: -1
bom_back_belt
  rotate: true
  xy: 225, 13
  size: 34, 41
  orig: 34, 41
  offset: 0, 0
  index: -1
bomb
  rotate: true
  xy: 794, 64
  size: 62, 69
  orig: 62, 69
  offset: 0, 0
  index: -1
claw_back_01
  rotate: true
  xy: 2, 2
  size: 22, 32
  orig: 22, 32
  offset: 0, 0
  index: -1
claw_back_02
  rotate: true
  xy: 995, 65
  size: 24, 27
  orig: 24, 27
  offset: 0, 0
  index: -1
ear_back_bomber
  rotate: true
  xy: 930, 78
  size: 48, 63
  orig: 48, 63
  offset: 0, 0
  index: -1
ear_front_bomber
  rotate: true
  xy: 485, 75
  size: 51, 73
  orig: 51, 73
  offset: 0, 0
  index: -1
eye_anime_dark
  rotate: true
  xy: 36, 2
  size: 22, 23
  orig: 22, 23
  offset: 0, 0
  index: -1
eye_anime_light
  rotate: false
  xy: 83, 8
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
eye_bace_back
  rotate: false
  xy: 387, 32
  size: 52, 38
  orig: 52, 38
  offset: 0, 0
  index: -1
eye_bace_front
  rotate: true
  xy: 493, 2
  size: 35, 44
  orig: 35, 44
  offset: 0, 0
  index: -1
face_small_open_mouse
  rotate: false
  xy: 436, 2
  size: 31, 25
  orig: 31, 25
  offset: 0, 0
  index: -1
face_smile
  rotate: false
  xy: 716, 89
  size: 76, 37
  orig: 76, 37
  offset: 0, 0
  index: -1
foot_back
  rotate: true
  xy: 441, 29
  size: 41, 50
  orig: 41, 50
  offset: 0, 0
  index: -1
foot_front
  rotate: true
  xy: 493, 39
  size: 34, 49
  orig: 34, 49
  offset: 0, 0
  index: -1
gogle
  rotate: false
  xy: 108, 63
  size: 116, 63
  orig: 116, 63
  offset: 0, 0
  index: -1
gogle_eye_front
  rotate: false
  xy: 61, 4
  size: 20, 20
  orig: 20, 20
  offset: 0, 0
  index: -1
gogle_part_back
  rotate: true
  xy: 865, 47
  size: 28, 58
  orig: 28, 58
  offset: 0, 0
  index: -1
gogre_part_front
  rotate: true
  xy: 387, 2
  size: 28, 47
  orig: 28, 47
  offset: 0, 0
  index: -1
head_bomber
  rotate: false
  xy: 2, 26
  size: 104, 100
  orig: 104, 100
  offset: 0, 0
  index: -1
mouse
  rotate: false
  xy: 268, 21
  size: 29, 26
  orig: 29, 26
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 469, 2
  size: 25, 13
  orig: 25, 13
  offset: 0, 0
  index: -1
zbon_part_2
  rotate: true
  xy: 322, 31
  size: 41, 63
  orig: 41, 63
  offset: 0, 0
  index: -1
zbon_part_anime
  rotate: true
  xy: 865, 77
  size: 49, 63
  orig: 49, 63
  offset: 0, 0
  index: -1
zubon
  rotate: false
  xy: 322, 74
  size: 86, 52
  orig: 86, 52
  offset: 0, 0
  index: -1
