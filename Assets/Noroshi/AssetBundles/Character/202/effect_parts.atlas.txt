
effect_parts.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
efe
  rotate: false
  xy: 171, 343
  size: 113, 113
  orig: 113, 113
  offset: 0, 0
  index: -1
p_skill1_wepo_effect0001
  rotate: true
  xy: 1201, 1941
  size: 105, 238
  orig: 174, 377
  offset: 44, 0
  index: -1
p_skill1_wepo_effect0002
  rotate: true
  xy: 938, 1916
  size: 130, 261
  orig: 174, 377
  offset: 29, 0
  index: -1
p_skill1_wepo_effect0003
  rotate: true
  xy: 668, 1904
  size: 142, 268
  orig: 174, 377
  offset: 17, 13
  index: -1
p_skill1_wepo_effect0004
  rotate: false
  xy: 2, 814
  size: 158, 251
  orig: 174, 377
  offset: 7, 51
  index: -1
p_skill1_wepo_effect0005
  rotate: false
  xy: 2, 573
  size: 159, 239
  orig: 174, 377
  offset: 7, 69
  index: -1
p_skill1_wepo_effect0006
  rotate: false
  xy: 494, 1806
  size: 172, 240
  orig: 174, 377
  offset: 1, 76
  index: -1
p_skill1_wepo_effect0007
  rotate: false
  xy: 2, 1067
  size: 173, 239
  orig: 174, 377
  offset: 1, 83
  index: -1
p_skill1_wepo_effect0008
  rotate: false
  xy: 2, 340
  size: 167, 231
  orig: 174, 377
  offset: 3, 91
  index: -1
p_skill1_wepo_effect0009
  rotate: false
  xy: 2, 116
  size: 155, 222
  orig: 174, 377
  offset: 7, 99
  index: -1
saveing0001
  rotate: false
  xy: 353, 1060
  size: 127, 129
  orig: 265, 265
  offset: 69, 68
  index: -1
saveing0002
  rotate: false
  xy: 1642, 1869
  size: 175, 177
  orig: 265, 265
  offset: 45, 44
  index: -1
saveing0003
  rotate: true
  xy: 1441, 1849
  size: 197, 199
  orig: 265, 265
  offset: 34, 33
  index: -1
saveing0004
  rotate: false
  xy: 2, 1308
  size: 217, 219
  orig: 265, 265
  offset: 24, 23
  index: -1
saveing0005
  rotate: false
  xy: 256, 1805
  size: 236, 241
  orig: 265, 265
  offset: 13, 12
  index: -1
saveing0006
  rotate: false
  xy: 2, 1785
  size: 252, 261
  orig: 265, 265
  offset: 3, 2
  index: -1
saveing0007
  rotate: false
  xy: 2, 1529
  size: 247, 254
  orig: 265, 265
  offset: 5, 5
  index: -1
step3_effect0001
  rotate: false
  xy: 1779, 1736
  size: 60, 131
  orig: 65, 166
  offset: 5, 4
  index: -1
step3_effect0002
  rotate: true
  xy: 307, 1329
  size: 61, 135
  orig: 65, 166
  offset: 4, 2
  index: -1
step3_effect0003
  rotate: false
  xy: 1973, 1742
  size: 61, 134
  orig: 65, 166
  offset: 4, 1
  index: -1
step3_effect0004
  rotate: false
  xy: 196, 2
  size: 61, 112
  orig: 65, 166
  offset: 4, 2
  index: -1
step3_effect0005
  rotate: false
  xy: 228, 116
  size: 60, 109
  orig: 65, 166
  offset: 5, 3
  index: -1
step3_effect0006
  rotate: false
  xy: 259, 2
  size: 59, 112
  orig: 65, 166
  offset: 6, 4
  index: -1
step3_effect0007
  rotate: false
  xy: 159, 220
  size: 58, 118
  orig: 65, 166
  offset: 6, 4
  index: -1
step3_effect0008
  rotate: false
  xy: 162, 939
  size: 60, 126
  orig: 65, 166
  offset: 5, 4
  index: -1
wepon effect0001
  rotate: false
  xy: 251, 1531
  size: 86, 185
  orig: 91, 236
  offset: 1, 0
  index: -1
wepon effect0002
  rotate: true
  xy: 256, 1718
  size: 85, 189
  orig: 91, 236
  offset: 2, 0
  index: -1
wepon effect0003
  rotate: true
  xy: 1201, 1853
  size: 86, 190
  orig: 91, 236
  offset: 3, 0
  index: -1
wepon effect0004
  rotate: true
  xy: 2, 25
  size: 89, 192
  orig: 91, 236
  offset: 2, 0
  index: -1
wepon effect0005
  rotate: false
  xy: 263, 1128
  size: 88, 196
  orig: 91, 236
  offset: 2, 0
  index: -1
wepon effect0006
  rotate: true
  xy: 1819, 1878
  size: 88, 202
  orig: 91, 236
  offset: 2, 0
  index: -1
wepon effect0007
  rotate: false
  xy: 221, 1326
  size: 84, 201
  orig: 91, 236
  offset: 4, 0
  index: -1
wepon effect0008
  rotate: false
  xy: 177, 1103
  size: 84, 203
  orig: 91, 236
  offset: 2, 0
  index: -1
wepon_effect
  rotate: true
  xy: 668, 1824
  size: 78, 220
  orig: 78, 225
  offset: 0, 5
  index: -1
wepon_effect2
  rotate: true
  xy: 1819, 1968
  size: 78, 220
  orig: 78, 225
  offset: 0, 5
  index: -1
wepon_top_effect0001
  rotate: false
  xy: 925, 1701
  size: 103, 55
  orig: 165, 260
  offset: 47, 0
  index: -1
wepon_top_effect0002
  rotate: false
  xy: 925, 1758
  size: 112, 50
  orig: 165, 260
  offset: 35, 0
  index: -1
wepon_top_effect0003
  rotate: true
  xy: 219, 227
  size: 114, 87
  orig: 165, 260
  offset: 27, 0
  index: -1
wepon_top_effect0004
  rotate: false
  xy: 938, 1810
  size: 113, 104
  orig: 165, 260
  offset: 20, 0
  index: -1
wepon_top_effect0005
  rotate: false
  xy: 339, 1576
  size: 114, 140
  orig: 165, 260
  offset: 14, 1
  index: -1
wepon_top_effect0006
  rotate: true
  xy: 668, 1700
  size: 122, 133
  orig: 165, 260
  offset: 19, 8
  index: -1
wepon_top_effect0007
  rotate: true
  xy: 803, 1692
  size: 130, 120
  orig: 165, 260
  offset: 28, 73
  index: -1
wepon_top_effect0008
  rotate: false
  xy: 162, 817
  size: 113, 120
  orig: 165, 260
  offset: 48, 94
  index: -1
wepon_top_effect0009
  rotate: true
  xy: 163, 690
  size: 125, 124
  orig: 165, 260
  offset: 40, 95
  index: -1
wepon_top_effect0010
  rotate: true
  xy: 353, 1191
  size: 136, 120
  orig: 165, 260
  offset: 26, 100
  index: -1
wepon_top_effect0011
  rotate: false
  xy: 1642, 1740
  size: 135, 127
  orig: 165, 260
  offset: 23, 117
  index: -1
wepon_top_effect0012
  rotate: false
  xy: 307, 1392
  size: 130, 137
  orig: 165, 260
  offset: 24, 123
  index: -1
wepon_top_effect0013
  rotate: false
  xy: 1841, 1744
  size: 130, 132
  orig: 165, 260
  offset: 20, 128
  index: -1
wepon_top_effect0014
  rotate: true
  xy: 171, 458
  size: 114, 107
  orig: 165, 260
  offset: 23, 153
  index: -1
wepon_top_effect0015
  rotate: false
  xy: 163, 574
  size: 113, 114
  orig: 165, 260
  offset: 27, 146
  index: -1
wepon_top_effect0016
  rotate: false
  xy: 1053, 1842
  size: 107, 72
  orig: 165, 260
  offset: 32, 188
  index: -1
wepon_top_effect0017
  rotate: true
  xy: 159, 116
  size: 102, 67
  orig: 165, 260
  offset: 36, 193
  index: -1
wepon_top_effect0018
  rotate: true
  xy: 1393, 1856
  size: 83, 35
  orig: 165, 260
  offset: 50, 210
  index: -1
wepon_top_effect0019
  rotate: true
  xy: 221, 1308
  size: 16, 20
  orig: 165, 260
  offset: 55, 205
  index: -1
