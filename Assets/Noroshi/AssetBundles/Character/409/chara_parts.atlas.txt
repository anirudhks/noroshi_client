
chara_parts.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
arm_left_4-5
  rotate: false
  xy: 1147, 612
  size: 106, 112
  orig: 106, 112
  offset: 0, 0
  index: -1
arm_right_4-5
  rotate: false
  xy: 1140, 881
  size: 140, 96
  orig: 140, 96
  offset: 0, 0
  index: -1
body01_5
  rotate: false
  xy: 651, 1136
  size: 484, 331
  orig: 484, 331
  offset: 0, 0
  index: -1
body02_4-5
  rotate: true
  xy: 545, 213
  size: 280, 334
  orig: 280, 334
  offset: 0, 0
  index: -1
contours_active_4-5
  rotate: false
  xy: 1140, 979
  size: 165, 164
  orig: 165, 164
  offset: 0, 0
  index: -1
contours_attack_4-5
  rotate: false
  xy: 477, 47
  size: 165, 164
  orig: 165, 164
  offset: 0, 0
  index: -1
contours_normal_4-5
  rotate: false
  xy: 644, 47
  size: 165, 164
  orig: 165, 164
  offset: 0, 0
  index: -1
eye_damage_1-5
  rotate: true
  xy: 1043, 2
  size: 58, 68
  orig: 58, 68
  offset: 0, 0
  index: -1
eye_dead_1-5
  rotate: true
  xy: 1371, 1445
  size: 58, 68
  orig: 58, 68
  offset: 0, 0
  index: -1
eye_normal_1-5
  rotate: true
  xy: 1441, 1445
  size: 58, 68
  orig: 58, 68
  offset: 0, 0
  index: -1
eye_win_1-5
  rotate: true
  xy: 1511, 1445
  size: 58, 68
  orig: 58, 68
  offset: 0, 0
  index: -1
finger01_left_foot_5
  rotate: false
  xy: 1300, 1457
  size: 69, 46
  orig: 69, 46
  offset: 0, 0
  index: -1
finger01_left_hand_5
  rotate: false
  xy: 1782, 1453
  size: 61, 50
  orig: 61, 50
  offset: 0, 0
  index: -1
finger01_right_foot_5
  rotate: false
  xy: 1651, 1442
  size: 66, 61
  orig: 66, 61
  offset: 0, 0
  index: -1
finger01_right_hand_5
  rotate: true
  xy: 1581, 1446
  size: 57, 68
  orig: 57, 68
  offset: 0, 0
  index: -1
finger02_left_foot_5
  rotate: false
  xy: 1974, 1790
  size: 72, 59
  orig: 72, 59
  offset: 0, 0
  index: -1
finger02_left_hand_5
  rotate: false
  xy: 1904, 1452
  size: 50, 51
  orig: 50, 51
  offset: 0, 0
  index: -1
finger02_right_foot_5
  rotate: false
  xy: 1043, 130
  size: 76, 83
  orig: 76, 83
  offset: 0, 0
  index: -1
finger02_right_hand_5
  rotate: false
  xy: 1845, 1452
  size: 57, 51
  orig: 57, 51
  offset: 0, 0
  index: -1
finger03_left_foot_5
  rotate: false
  xy: 1974, 1508
  size: 71, 66
  orig: 71, 66
  offset: 0, 0
  index: -1
finger03_left_hand_5
  rotate: false
  xy: 1719, 1443
  size: 61, 60
  orig: 61, 60
  offset: 0, 0
  index: -1
finger03_right_foot_5
  rotate: false
  xy: 1974, 1576
  size: 71, 81
  orig: 71, 81
  offset: 0, 0
  index: -1
finger03_right_hand_5
  rotate: false
  xy: 1043, 62
  size: 67, 66
  orig: 67, 66
  offset: 0, 0
  index: -1
horn_left_5
  rotate: false
  xy: 1137, 1277
  size: 140, 190
  orig: 140, 190
  offset: 0, 0
  index: -1
horn_right_5
  rotate: false
  xy: 811, 19
  size: 85, 192
  orig: 85, 192
  offset: 0, 0
  index: -1
leg_left_4-5
  rotate: false
  xy: 898, 51
  size: 143, 162
  orig: 143, 162
  offset: 0, 0
  index: -1
leg_right_4-5
  rotate: true
  xy: 1137, 1145
  size: 130, 175
  orig: 130, 175
  offset: 0, 0
  index: -1
pattern01_leftwing_5
  rotate: false
  xy: 2, 1469
  size: 647, 577
  orig: 647, 577
  offset: 0, 0
  index: -1
pattern01_rightwing_5
  rotate: true
  xy: 2, 216
  size: 672, 541
  orig: 672, 541
  offset: 0, 0
  index: -1
pattern01_tail_5
  rotate: false
  xy: 545, 495
  size: 299, 365
  orig: 299, 365
  offset: 0, 0
  index: -1
shadow
  rotate: true
  xy: 2, 2
  size: 25, 13
  orig: 25, 13
  offset: 0, 0
  index: -1
shoulder_left_5
  rotate: true
  xy: 1033, 926
  size: 208, 105
  orig: 208, 105
  offset: 0, 0
  index: -1
shoulder_right_5
  rotate: false
  xy: 1147, 726
  size: 125, 153
  orig: 125, 153
  offset: 0, 0
  index: -1
tail01_5
  rotate: false
  xy: 846, 495
  size: 299, 365
  orig: 299, 365
  offset: 0, 0
  index: -1
tail02_5
  rotate: true
  xy: 2, 29
  size: 185, 258
  orig: 185, 258
  offset: 0, 0
  index: -1
thigh_left_5
  rotate: false
  xy: 881, 298
  size: 230, 195
  orig: 230, 195
  offset: 0, 0
  index: -1
thigh_right_5
  rotate: false
  xy: 262, 9
  size: 213, 205
  orig: 213, 205
  offset: 0, 0
  index: -1
whisker_left_4-5
  rotate: true
  xy: 1974, 1851
  size: 195, 69
  orig: 195, 69
  offset: 0, 0
  index: -1
whisker_right_4-5
  rotate: false
  xy: 881, 215
  size: 197, 81
  orig: 197, 81
  offset: 0, 0
  index: -1
wing01_left_5
  rotate: false
  xy: 2, 890
  size: 647, 577
  orig: 647, 577
  offset: 0, 0
  index: -1
wing01_right_5
  rotate: false
  xy: 1300, 1505
  size: 672, 541
  orig: 672, 541
  offset: 0, 0
  index: -1
wing02_left_5
  rotate: false
  xy: 651, 1469
  size: 647, 577
  orig: 647, 577
  offset: 0, 0
  index: -1
wing02_right_5
  rotate: true
  xy: 651, 862
  size: 272, 380
  orig: 272, 380
  offset: 0, 0
  index: -1
zafan_H
  rotate: false
  xy: 1147, 496
  size: 61, 114
  orig: 61, 114
  offset: 0, 0
  index: -1
zafan_M
  rotate: false
  xy: 1210, 514
  size: 77, 96
  orig: 77, 96
  offset: 0, 0
  index: -1
zafan_S
  rotate: false
  xy: 1974, 1659
  size: 46, 129
  orig: 46, 129
  offset: 0, 0
  index: -1
zafan_U
  rotate: true
  xy: 1033, 884
  size: 40, 89
  orig: 40, 89
  offset: 0, 0
  index: -1
