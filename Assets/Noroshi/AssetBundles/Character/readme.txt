/Assets/Noroshi/Resources/Character/にあった素材は、
AssetBundleLabelを付与した上でAssetBundleにビルドして、/Assets/Noroshi/AssetBundles/Character/に移動しています。
素材の移動はビルドを行った人物が行いますので、開発中はこれまで通り/Assets/Noroshi/Resources/Character/に素材を保管してPushしてください。

AssetBundleのビルドには数時間かかりますので、１日に１回〜２回程度でまとめてビルドします。
