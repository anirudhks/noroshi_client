﻿using System;
using System.Runtime.InteropServices;
using AOT;
using UniRx;
using UnityEngine;

namespace Flaggs.Client.Account
{
	/// <summary>
	/// Game Center のアカウントを扱うクラス。
	/// </summary>
	public class GameCenterAccount : IAccount
	{
		/// <summary>
		/// バックエンドサーバー認証のために必要な情報取得時に呼ばれるデリゲート。ネイティブプラグインで使われる。
		/// </summary>
		public delegate void OnFinishGenerateIdentityVerificationSignature(bool success, string publicKeyUrl, string signature, string salt, ulong timestamp);

		static GameCenterAccount _instance = null;

		/// <summary>
		/// （シングルトン）インスタンスを取得。
		/// </summary>
		public static GameCenterAccount Instance
		{
			get
			{
#if UNITY_IOS
				if (_instance == null) _instance = new GameCenterAccount();
#endif
				return _instance;
			}
		}

		/// <summary>
		/// バックエンドサーバー認証のために必要な情報を取得する。ネイティブプラグインを利用。
		/// </summary>
		/// <param name="onFinishGenerateIdentityVerificationSignature">必要な情報取得後に呼ばれるコールバック</param>
		[DllImport("__Internal")]
		static extern void _generateIdentityVerificationSignature(OnFinishGenerateIdentityVerificationSignature onFinishGenerateIdentityVerificationSignature);

		/// <summary>
		/// バックエンドサーバー認証のために必要な情報取得時に呼ばれるコールバック。ネイティブプラグインで使われる。
		/// static でないと利用できない制約があるので、仕方なくシングルトンインスタンス呼び出しを挟んで該当インスタンスに紐付ける。
		/// </summary>
		/// <param name="success">情報取得可否</param>
		/// <param name="publicKeyUrl">公開鍵取得用URL</param>
		/// <param name="signature">Signature</param>
		/// <param name="salt">Salt</param>
		/// <param name="timestamp">Timestamp</param>
		[MonoPInvokeCallback(typeof(OnFinishGenerateIdentityVerificationSignature))]
		static void _onFinishGenerateIdentityVerificationSignature(bool success, string publicKeyUrl, string signature, string salt, ulong timestamp)
		{
			if (success)
			{
				Instance._finishGenerateIdentityVerificationSignature(new IdentityVerificationSignature
				{
					PublicKeyUrl = publicKeyUrl,
					Signature = signature,
					Salt = salt,
					Timestamp = timestamp,
				});
			}
			else
			{
				Instance._failToGenerateIdentityVerificationSignature();
			}
		}


		Subject<IdentityVerificationSignature> _onFinishGenerateIdentityVerificationSignatureSubject;

		GameCenterAccount()
		{
		}

		/// <summary>
		/// 認証状態判定。
		/// </summary>
		public bool IsAuthenticated { get { return Social.localUser.authenticated; } }

		/// <summary>
		/// 認証。
		/// </summary>
		public IObservable<bool> Authenticate()
		{
			var onLoggedIn = new Subject<bool>();
			Social.localUser.Authenticate(success =>
			{
				Debug.Log("Game Center Login : " + success);
				onLoggedIn.OnNext(success);
				onLoggedIn.OnCompleted();
			});
			return onLoggedIn.AsObservable();
		}

		/// <summary>
		/// バックエンドサーバー認証。
		/// </summary>
		public IObservable<T> AuthenticateWithBackendServer<T>(Func<WWWForm, IObservable<T>> postBackendServer)
		{
			if (!IsAuthenticated) throw new InvalidOperationException("Is Not Authenticated");
			_onFinishGenerateIdentityVerificationSignatureSubject = new Subject<IdentityVerificationSignature>();
			var observable = _onFinishGenerateIdentityVerificationSignatureSubject.AsObservable()
				.SelectMany(identityVerificationSignature => {
					var form = new WWWForm();
					form.AddField("PlayerID", Social.localUser.id);
//					form.AddField("BundleID", "jp.co.flaggs.test.app");
					form.AddField("PublicKeyUrl", identityVerificationSignature.PublicKeyUrl);
					form.AddField("Signature", identityVerificationSignature.Signature);
					form.AddField("Salt", identityVerificationSignature.Salt);
					form.AddField("Timestamp", identityVerificationSignature.Timestamp.ToString());
					return postBackendServer(form);
				});
			_generateIdentityVerificationSignature(_onFinishGenerateIdentityVerificationSignature);
			return observable;
		}

		void _finishGenerateIdentityVerificationSignature(IdentityVerificationSignature identityVerificationSignature)
		{
			_onFinishGenerateIdentityVerificationSignatureSubject.OnNext(identityVerificationSignature);
		}
		void _failToGenerateIdentityVerificationSignature()
		{
			_onFinishGenerateIdentityVerificationSignatureSubject.OnError(new SystemException());
		}

		/// <summary>
		/// サインアウト。
		/// </summary>
		public void SignOut()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// バックエンドサーバー認証のために必要な情報。
		/// </summary>
		struct IdentityVerificationSignature
		{
			public string PublicKeyUrl { get; set; }
			public string Signature { get; set; }
			public string Salt { get; set; }
			public ulong Timestamp { get; set; }
		}
	}
}
