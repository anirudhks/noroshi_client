﻿using System;
using UniRx;
using UnityEngine;
using GooglePlayGames;

namespace Flaggs.Client.Account
{
	/// <summary>
	/// Google Play Games のアカウントを扱うクラス。
	/// </summary>
	public class GooglePlayGamesAccount : IAccount
	{
		static GooglePlayGamesAccount _instance = null;

		/// <summary>
		/// （シングルトン）インスタンスを取得。
		/// </summary>
		public static GooglePlayGamesAccount Instance
		{
			get
			{
#if UNITY_ANDROID
				if (_instance == null) _instance = new GooglePlayGamesAccount();
#endif
				return _instance;
			}
		}

		GooglePlayGamesAccount()
		{
			PlayGamesPlatform.Activate();
		}

		/// <summary>
		/// 認証状態判定。
		/// </summary>
		public bool IsAuthenticated { get { return Social.localUser.authenticated; } }

		/// <summary>
		/// 認証。
		/// </summary>
		public IObservable<bool> Authenticate()
		{
			var onLoggedIn = new Subject<bool>();
			Social.localUser.Authenticate(success =>
			{
				Debug.Log("Google Play Games Login : " + success);
				// トークン取得は内部的に遅延処理をするっぽいのでここで一度呼び出しておく。原因は不明。
				var idToken = ((PlayGamesLocalUser)Social.localUser).idToken;
				onLoggedIn.OnNext(success);
				onLoggedIn.OnCompleted();
			});
			return onLoggedIn.AsObservable();
		}

		/// <summary>
		/// バックエンドサーバー認証。
		/// </summary>
		public IObservable<T> AuthenticateWithBackendServer<T>(Func<WWWForm, IObservable<T>> postBackendServer)
		{
			if (!IsAuthenticated) throw new InvalidOperationException("Is Not Authenticated");
			var form = new WWWForm();
			form.AddField("idToken", _getIdToken());
			return postBackendServer(form);
		}

		string _getIdToken()
		{
			if (!IsAuthenticated) throw new InvalidOperationException("Is Not Authenticated");
			return ((PlayGamesLocalUser)Social.localUser).idToken;
		}

		/// <summary>
		/// サインアウト。
		/// </summary>
		public void SignOut()
		{
			PlayGamesPlatform.Instance.SignOut();
		}
	}
}
