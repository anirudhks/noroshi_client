﻿using System;
using UniRx;
using UnityEngine;

namespace Flaggs.Client.Account
{
	/// <summary>
	/// 外部アカウントを扱うインターフェース。
	/// </summary>
	public interface IAccount
	{
		/// <summary>
		/// 認証状態判定。
		/// </summary>
		bool IsAuthenticated { get; }

		/// <summary>
		/// 認証。
		/// </summary>
		IObservable<bool> Authenticate();

		/// <summary>
		/// バックエンドサーバー認証。
		/// </summary>
		IObservable<T> AuthenticateWithBackendServer<T>(Func<WWWForm, IObservable<T>> postBackendServer);

		/// <summary>
		/// サインアウト。
		/// </summary>
		void SignOut();
	}
}
