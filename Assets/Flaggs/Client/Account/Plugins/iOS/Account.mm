#import <GameKit/GameKit.h>

typedef void (*ANSWERCB)(bool, const char*, const char*, const char*, uint64_t);
static ANSWERCB onFinish;

extern "C" void _generateIdentityVerificationSignature(ANSWERCB onFinishGenerateIdentityVerificationSignature)
{
    onFinish = onFinishGenerateIdentityVerificationSignature;
    
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    [localPlayer generateIdentityVerificationSignatureWithCompletionHandler:^(NSURL *publicKeyUrl, NSData *signature, NSData *salt, uint64_t timestamp, NSError *error)
    {
        if(error != nil)
        {
            NSLog(@"error: %@", [[error userInfo] description]);
            onFinish(false, "", "", "", 0);
            return;
        }
            
        onFinish(true, [[publicKeyUrl absoluteString] UTF8String], [[signature base64EncodedStringWithOptions:0] UTF8String], [[salt base64EncodedStringWithOptions:0] UTF8String], timestamp);
    }];
}
