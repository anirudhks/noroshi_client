﻿using System;
using System.Collections.Generic;

namespace Flaggs.Extensions.Heam
{
    /// <summary>
    /// ヒープ領域を最適化した IEnumerable<T> の拡張メソッド集。
    /// </summary>
    public static class Enumerable
    {
        public static TResult[] ToArrayWithOptimization<TSource, TResult>(
            this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            return _toArrayWithOptimization(source, _ => true, selector);
        }
        public static TResult[] ToArrayWithOptimization<TSource, TResult>(
            this IEnumerable<TSource> source, Predicate<TSource> predicate, Func<TSource, TResult> selector)
        {
            return _toArrayWithOptimization(source, predicate, selector);
        }
        public static TSource[] ToArrayWithOptimization<TSource>(this IEnumerable<TSource> source)
        {
            return _toArrayWithOptimization(source);
        }

        /// <summary>
        /// 無駄なヒープ領域を利用しないように、Index アクセスと配列サイズ動的変更をしながら、配列へ変換する。
        /// </summary>
        /// <returns>The array with optimization.</returns>
        /// <param name="source">Source.</param>
        /// <typeparam name="TSource">The 1st type parameter.</typeparam>
        static TSource[] _toArrayWithOptimization<TSource>(IEnumerable<TSource> source)
        {
            // Array の場合はそのまま。
            var array = source as TSource[];
            if (array != null) return array;

            return _toArrayWithOptimization(source, element => element);
        }

        /// <summary>
        /// 無駄なヒープ領域を利用しないように、Index アクセスと配列サイズ動的変更をしながら、要素変換をしつつ、配列へ変換する。
        /// </summary>
        /// <returns>The array with optimization.</returns>
        /// <param name="source">Source.</param>
        /// <param name="selector">Selector.</param>
        /// <typeparam name="TSource">The 1st type parameter.</typeparam>
        /// <typeparam name="TResult">The 2nd type parameter.</typeparam>
        static TResult[] _toArrayWithOptimization<TSource, TResult>(
            IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            TResult[] array;

            // IList の場合は Index アクセス。
            var list = source as IList<TSource>;
            if (list != null)
            {
                var count = list.Count;
                array = new TResult[count];
                for (var i = 0; i < count; i++)
                {
                    array[i] = selector(list[i]);
                }
                return array;
            }

            // ICollection の場合はサイズを確定した後に foreach。
            var collection = source as ICollection<TSource>;
            if (collection != null)
            {
                var count = collection.Count;
                if (count == 0) return new TResult[0];
                array = new TResult[count];
                var index = 0;
                foreach (var element in collection)
                {
                    array[index++] = selector(element);
                }
                return array;
            }

            // それ以外の場合は仕方ないのでサイズ動的変更対応。
            return _toArrayWithDynamicAllocation(source, _ => true, selector);
        }

        /// <summary>
        /// 無駄なヒープ領域を利用しないように、Index アクセスと配列サイズ動的変更をしながら、フィルタリングと要素変換をしつつ、配列へ変換する。
        /// </summary>
        /// <returns>The array with optimization.</returns>
        /// <param name="source">Source.</param>
        /// <param name="predicate">Predicate.</param>
        /// <param name="selector">Selector.</param>
        /// <typeparam name="TSource">The 1st type parameter.</typeparam>
        /// <typeparam name="TResult">The 2nd type parameter.</typeparam>
        static TResult[] _toArrayWithOptimization<TSource, TResult>(
            IEnumerable<TSource> source, Predicate<TSource> predicate, Func<TSource, TResult> selector)
        {
            TResult[] array;

            // IList の場合は Index アクセス。
            var list = source as IList<TSource>;
            if (list != null)
            {
                var count = list.Count;
                var arrayLength = 0;
                for (var i = 0; i < count; i++)
                {
                    if (predicate(list[i])) arrayLength++;
                }
                array = new TResult[arrayLength];
                var arrayIndex = 0;
                for (var i = 0; i < count; i++)
                {
                    if (predicate(list[i])) array[arrayIndex++] = selector(list[i]);
                }
                return array;
            }
            // それ以外の場合は仕方ないので配列サイズ動的変更対応。
            return _toArrayWithDynamicAllocation(source, predicate, selector);
        }

        /// <summary>
        /// 無駄なヒープ領域を利用しないように、配列サイズ動的変更をしながら、フィルタリングと要素変換をしつつ、配列へ変換する。
        /// </summary>
        /// <returns>The array with dynamic allocation.</returns>
        /// <param name="source">Source.</param>
        /// <param name="predicate">Predicate.</param>
        /// <param name="selector">Selector.</param>
        /// <typeparam name="TSource">The 1st type parameter.</typeparam>
        /// <typeparam name="TResult">The 2nd type parameter.</typeparam>
        static TResult[] _toArrayWithDynamicAllocation<TSource, TResult>(
            IEnumerable<TSource> source, Predicate<TSource> predicate, Func<TSource, TResult> selector)
        {
            var array = new TResult[0];
            var index = 0;
            foreach (var element in source)
            {
                if (!predicate(element)) continue;
                if (index == array.Length)
                {
                    if (index == 0)
                    {
                        array = new TResult[1];
                    }
                    else
                    {
                        Array.Resize(ref array, index * 2);
                    }
                }
                array[index++] = selector(element);
            }
            if (index != array.Length)
            {
                Array.Resize(ref array, index);
            }
            return array;
        }
    }
}
