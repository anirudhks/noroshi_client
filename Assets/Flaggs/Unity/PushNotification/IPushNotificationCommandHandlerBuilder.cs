﻿namespace Flaggs.Unity.PushNotification
{
    /// <summary>
    /// プッシュ通知関連の命令を扱うオブジェクトをビルドするインターフェース。
    /// </summary>
    public interface IPushNotificationCommandHandlerBuilder
    {
        /// <summary>
        /// Google Cloud Messaging 関連の命令を扱うオブジェクトをビルドする。
        /// </summary>
        /// <returns>Google Cloud Messaging 関連の命令を扱うオブジェクト。</returns>
        CloudMessagingCommandHandler BuildCloudMessagingCommandHandler();
    }
}
