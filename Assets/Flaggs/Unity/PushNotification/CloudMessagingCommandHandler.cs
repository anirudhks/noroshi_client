﻿using System;
using UniRx;
using UnityEngine;

namespace Flaggs.Unity.PushNotification
{
    public class CloudMessagingCommandHandler : MonoBehaviour
    {
        const string NATIVE_PLUGIN_REGISTER_CLASS_NAME = "jp.co.flaggs.lib_cloud_messaging_unity_plugin.Register";
        const string NATIVE_PLUGIN_REGISTER_METHOD_NAME = "registerInBackground";

        GameObject _gameObject;
        AndroidJavaObject _applicationContext = null;
        Subject<string> _onRegisterSubject = null;

        void Awake()
        {
            _gameObject = gameObject;
        }

        void OnDestroy()
        {
            if (_onRegisterSubject != null) _onRegisterSubject.OnCompleted();
            if (_applicationContext != null) _applicationContext.Dispose();
        }

        public IObservable<string> Register(string senderId)
        {
            #if UNITY_EDITOR
            return Observable.Empty<string>();
            #else
            if (_onRegisterSubject != null) throw new InvalidOperationException();
            _onRegisterSubject = new Subject<string>();
            using (var register = new AndroidJavaClass(NATIVE_PLUGIN_REGISTER_CLASS_NAME))
            {
                register.CallStatic(NATIVE_PLUGIN_REGISTER_METHOD_NAME, new object[] { _getApplicationContext(), gameObject.name, senderId, });
            }
            return _onRegisterSubject.AsObservable();
            #endif
        }

        AndroidJavaObject _getApplicationContext()
        {
            if (_applicationContext == null)
            {
                var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                _applicationContext = activity.Call<AndroidJavaObject>("getApplicationContext");
                unityPlayer.Dispose();
                activity.Dispose();
            }
            return _applicationContext;
        }

        void OnRegister(string registrationToken)
        {
            _onRegisterSubject.OnNext(registrationToken);
            _onRegisterSubject.OnCompleted();
        }

        public void Dispose()
        {
            Destroy(_gameObject);
        }
    }
}
