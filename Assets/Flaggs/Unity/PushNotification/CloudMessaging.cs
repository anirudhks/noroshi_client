﻿using UniRx;
using UnityEngine;
using System.Collections;

namespace Flaggs.Unity.PushNotification
{
    public class CloudMessaging
    {
        readonly string _senderId;
        CloudMessagingCommandHandler _commandHandler;

        public CloudMessaging(string senderId)
        {
            _senderId = senderId;
        }

        public void Initialize(IPushNotificationCommandHandlerBuilder commandHandlerBuilder)
        {
            _commandHandler = commandHandlerBuilder.BuildCloudMessagingCommandHandler();
        }

        public IObservable<string> GetIdentifier()
        {
            return _commandHandler.Register(_senderId);
        }

        public void Dispose()
        {
            if (_commandHandler != null) _commandHandler.Dispose();
        }
    }
}
