﻿using UnityEngine;

namespace Flaggs.Unity.PushNotification
{
    /// <summary>
    /// プッシュ通知関連の命令を扱うオブジェクトをビルドするクラス。
    /// </summary>
    public class PushNotificationCommandHandlerBuilder : IPushNotificationCommandHandlerBuilder
    {
        /// <summary>
        /// 利用（作成）する GameObject 名。
        /// </summary>
        const string GAME_OBJECT_NAME = "Flaggs.Unity.PushNotification.CloudMessagingCommandHandler";

        CloudMessagingCommandHandler _cloudMessagingCommandHandler;

        /// <summary>
        /// Google Cloud Messaging 関連の命令を扱うオブジェクトをビルドする。
        /// </summary>
        /// <returns>Google Cloud Messaging 関連の命令を扱うオブジェクト。</returns>
        public CloudMessagingCommandHandler BuildCloudMessagingCommandHandler()
        {
            if (_cloudMessagingCommandHandler != null) return _cloudMessagingCommandHandler;
            var gameObject = GameObject.Find(GAME_OBJECT_NAME);
            if (gameObject != null)
            {
                _cloudMessagingCommandHandler = gameObject.GetComponent<CloudMessagingCommandHandler>();
            }
            else
            {
                gameObject = new GameObject(GAME_OBJECT_NAME);
                GameObject.DontDestroyOnLoad(gameObject);
                _cloudMessagingCommandHandler = gameObject.AddComponent<CloudMessagingCommandHandler>();
            }
            return _cloudMessagingCommandHandler;
        }
    }
}
