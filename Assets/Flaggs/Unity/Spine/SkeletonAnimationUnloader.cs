﻿using System;
using UnityEngine;

namespace Flaggs.Unity.Spine
{
    /// <summary>
    /// SkeletonAnimation のアンロードを担当するクラス。
    /// </summary>
    public class SkeletonAnimationUnloader
    {
        /// <summary>
        /// 子 SkeletonAnimation に紐付いた Texture をアンロードした上で破棄する。
        /// </summary>
        /// <param name="gameObject">Game object.</param>
        public void UnloadTextureAndDestroy(GameObject gameObject)
        {
            if (gameObject == null) throw new InvalidOperationException();
            var skeletonAnimations = gameObject.GetComponents<SkeletonAnimation>();
            foreach (var skeletonAnimation in skeletonAnimations)
            {
                var atlasAssets = skeletonAnimation.skeletonDataAsset.atlasAssets;
                foreach(var atlasAsset in atlasAssets)
                {
                    foreach(var material in atlasAsset.materials)
                    {
                        Resources.UnloadAsset(material.mainTexture);
                    }
                }
            }
            GameObject.Destroy(gameObject);
        }
    }
}
