﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Noroshi.Core.Game.Character;
using CharacterConstant = Noroshi.Core.Game.Character.Constant;
using Noroshi.BattleScene;
using Noroshi.BattleScene.MonoBehaviours;
using Noroshi.Grid;
using StoryLayoutType = Noroshi.Core.Game.Battle.StoryLayoutType;

namespace NoroshiDebug.PickupCharacterEdit.Editor.Window
{
    public class PickupCharacterEditWindow : EditorWindow
    {
        const string RESOURCES_PATH = "Assets/Noroshi/Resources/Character/{0}/Character.prefab";
        const string SKIN_NAME = "step";

        int _characterID = 0;
        GameObject _character = null;
        SerializedObject _serializeObject = null;
        Vector2 _scrollPos = Vector2.zero;
        bool _isDirectionLeft = false;
        bool _isPreviewStory = true;
        SkeletonAnimation _skeletonAnimation = null;
        CenterPositionForUIType _centerPositionForUIType = CenterPositionForUIType.Story;
        StoryLayoutType _storyLayoutType = StoryLayoutType.LeftAndRightDivision;

        [MenuItem("PickupCharacter/Edit")]
        static void Init()
        {
            var editorWindow = GetWindow(typeof(PickupCharacterEditWindow), false, "PickupCharacterEditWindow");
            editorWindow.autoRepaintOnSceneChange = true;
            editorWindow.Show();
        }

        void OnDestroy()
        {
            if (_serializeObject != null) _serializeObject.Dispose();
            _deleteCharacter();
        }

        void _loadCharacterPrefab()
        {
            _deleteCharacter();
            var path = string.Format(RESOURCES_PATH, _characterID);
            var characterObj = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject));
            if (characterObj == null)
            {
                EditorUtility.DisplayDialog("Error", "Not Found CharacterID = " + _characterID.ToString(), "OK");
                return;
            }
            _character = PrefabUtility.InstantiatePrefab(characterObj) as GameObject;
            characterObj = null;
            if (_character.GetComponent<CharacterView>() == null)
            {
                EditorUtility.DisplayDialog("Error", "Missing Component : CharacterView", "OK");
                _deleteCharacter();
            }

            Resources.UnloadUnusedAssets();
        }

        void _drawCharacterViewData()
        {
            if (_character == null) return;
            _serializeObject = new SerializedObject(_character.GetComponent<CharacterView>());
            _serializeObject.Update();

            var scaleForUIWithStoryProp = _serializeObject.FindProperty("_scaleForUIWithStory");
            scaleForUIWithStoryProp.floatValue = EditorGUILayout.FloatField(scaleForUIWithStoryProp.displayName, scaleForUIWithStoryProp.floatValue);
            var scaleForUIWithResultProp = _serializeObject.FindProperty("_scaleForUIWithResult");
            scaleForUIWithResultProp.floatValue = EditorGUILayout.FloatField(scaleForUIWithResultProp.displayName, scaleForUIWithResultProp.floatValue);

            var centerPositionForUIStoryProp = _serializeObject.FindProperty("_centerPositionsForUIWithStory");
            _drawUnreversibleTransforms(centerPositionForUIStoryProp);

            var centerPositionForUIResultProp = _serializeObject.FindProperty("_centerPositionForUIWithResult");
            EditorGUILayout.PropertyField(centerPositionForUIResultProp);

            var scaleForUI = 0.0f;
            switch (_centerPositionForUIType)
            {
            case CenterPositionForUIType.Story:
                scaleForUI = scaleForUIWithStoryProp.floatValue;
                _setPreviewPositionWithStory(centerPositionForUIStoryProp);
                break;
            case CenterPositionForUIType.Result:
                scaleForUI = scaleForUIWithResultProp.floatValue;
                _setPreviewPositionWithResult(centerPositionForUIResultProp);
                break;
            }

            _character.transform.localScale = new Vector3(scaleForUI, scaleForUI, 1);

            _character.transform.eulerAngles = new Vector3(0, _isDirectionLeft ? 0 : 180, 0);

            _serializeObject.ApplyModifiedProperties();
        }

        void _drawUnreversibleTransforms(SerializedProperty prop)
        {
            var foldoutName = ObjectNames.NicifyVariableName(prop.displayName);
            switch (prop.propertyType)
            {
            case SerializedPropertyType.Generic:
                prop.isExpanded = EditorGUILayout.Foldout(prop.isExpanded, foldoutName);
                if (!prop.isExpanded) break;
                if (prop.isArray)
                {
                    prop.arraySize = EditorGUILayout.IntField("Size", prop.arraySize);
                    EditorGUI.indentLevel++;
                    for (int i = 0; i < prop.arraySize; i++)
                    {
                        _drawUnreversibleTransforms(prop.GetArrayElementAtIndex(i));
                    }
                    EditorGUI.indentLevel--;
                }
                break;
            default:
                EditorGUILayout.PropertyField(prop);
                break;
            }
        }

        void _setPreviewPositionWithResult(SerializedProperty positionProp)
        {
            _character.SetActive(true);
            _character.transform.position = new Vector3(-positionProp.vector2Value.x * (_isDirectionLeft ? 1 : -1),
                                                        -positionProp.vector2Value.y,
                                                        0);
        }
        void _setPreviewPositionWithStory(SerializedProperty positionProp)
        {
            var isSpecial = positionProp.arraySize > 1;
            _character.SetActive(true);

            if (positionProp.arraySize == 0) return;

            var pos = positionProp.GetArrayElementAtIndex(0).vector2Value;

            switch (_storyLayoutType)
            {
            case StoryLayoutType.Centering:
                pos = isSpecial ? positionProp.GetArrayElementAtIndex(1).vector2Value : new Vector2((pos.x / 2.0f), pos.y);
                _character.transform.position = new Vector3(-pos.x * (_isDirectionLeft ? 1 : -1) , -pos.y, 0);
                break;
            case StoryLayoutType.LeftAndRightDivision:
                _character.transform.position = new Vector3(-pos.x * (_isDirectionLeft ? 1 : -1),
                                                            -pos.y,
                                                            0);
                break;
            case StoryLayoutType.CenterAlone:
                _character.transform.position = new Vector3(0, -pos.y, 0);
                break;
            }
        }

        void _setSpine()
        {
            if (_character == null) return;
            _skeletonAnimation = _character.GetComponent<SkeletonAnimation>();
            var stepCnt = 1;
            bool haveStepSkin = false;
            while(_skeletonAnimation.Skeleton.Data.FindSkin(SKIN_NAME + stepCnt) != null)
            {
                haveStepSkin = true;
                stepCnt++;
            }
            _skeletonAnimation.skeleton.SetSkin(haveStepSkin ? (SKIN_NAME + (stepCnt - 1)) : "default");
            _changeSkeletonAnimation();
        }

        void _changeSkeletonAnimation()
        {
            if (_skeletonAnimation == null) return;
            _skeletonAnimation.state.SetAnimation(0, _isPreviewStory ? CharacterConstant.ANIMATION_NAME_MAP[CharacterAnimation.Idle] : CharacterConstant.ANIMATION_NAME_MAP[CharacterAnimation.Win], false)
            .Time = _isPreviewStory ? 0 : 100;
            _skeletonAnimation.Update(0);
            foreach (var chilsSkeleton in _character.GetComponentsInChildren<SkeletonAnimation>())
            {
                chilsSkeleton.state.SetAnimation(0, _isPreviewStory ? CharacterConstant.ANIMATION_NAME_MAP[CharacterAnimation.Idle] : CharacterConstant.ANIMATION_NAME_MAP[CharacterAnimation.Win], false)
                .Time = _isPreviewStory ? 0 : 100;
                chilsSkeleton.Update(0);
            }
        }

        void _deleteCharacter()
        {
            if (_character == null) return;
            _skeletonAnimation = null;
            DestroyImmediate(_character);
            _character = null;
        }

        void _applyPrefab()
        {
            // Prefabに保存しないのでリセット--
            _character.transform.position = Vector3.zero;
            _character.transform.rotation = Quaternion.identity;
            _character.transform.localScale = Vector3.one;
            //---

            var originalPrefab = PrefabUtility.GetPrefabParent(_character);
            if (originalPrefab == null)
            {
                EditorUtility.DisplayDialog("Error", "Not Found Prefab", "OK");
                return;
            }
            PrefabUtility.ReplacePrefab(_character, originalPrefab);
        }
        
        // EditorWindow上のUI表示用
        void OnGUI()
        {
            _characterID = EditorGUILayout.IntField("CharacterID", _characterID);
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Load", GUILayout.Width(100)))
            {
                _loadCharacterPrefab();
                _setSpine();
            }
            
            if (GUILayout.Button("Delete", GUILayout.Width(100)))
            {
                _deleteCharacter();
            }
            GUILayout.EndHorizontal();

            _isDirectionLeft = EditorGUILayout.Toggle("Direction Left", _isDirectionLeft);

            if (GUILayout.Toggle(_centerPositionForUIType == CenterPositionForUIType.Story, "Preview Story"))
            {
                _centerPositionForUIType = CenterPositionForUIType.Story;
                _isPreviewStory = true;
                _changeSkeletonAnimation();
            }
            if (GUILayout.Toggle(_centerPositionForUIType == CenterPositionForUIType.Result, "Preview Result"))
            {
                _centerPositionForUIType = CenterPositionForUIType.Result;
                _isPreviewStory = false;
                _changeSkeletonAnimation();
            }
            GUILayout.BeginHorizontal();
            if (GUILayout.Toggle(_storyLayoutType == StoryLayoutType.Centering, "preview 中央寄せ"))
            {
                _storyLayoutType = StoryLayoutType.Centering;
            }
            if (GUILayout.Toggle(_storyLayoutType == StoryLayoutType.LeftAndRightDivision, "preview 左右分け"))
            {
                _storyLayoutType = StoryLayoutType.LeftAndRightDivision;
            }
            if (GUILayout.Toggle(_storyLayoutType == StoryLayoutType.CenterAlone, "preview 中央ピン"))
            {
                _storyLayoutType = StoryLayoutType.CenterAlone;
            }
            GUILayout.EndHorizontal();

            _scrollPos = GUILayout.BeginScrollView(_scrollPos);
            _drawCharacterViewData();
            GUILayout.EndScrollView();
            
            if (GUILayout.Button("Apply", GUILayout.Width(100)))
            {
                _applyPrefab();
            }
            
            var style = new GUIStyle();
            style.fontSize = 25;
            style.fontStyle = FontStyle.Bold;
            GUILayout.Label("Apply後はシーンのセーブを忘れずに！", style);

            GUILayout.EndVertical();
        }
    }
}