﻿using UniLinq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Noroshi.WebApi;
using Noroshi;

namespace NoroshiDebug.SoundDebug
{
    public class Main : MonoBehaviour
    {
        [SerializeField] InputField _inputField;

        void Awake()
        {
            GlobalContainer.WebApiRequester.LoginForUnityEditor()
            .Do(_ =>
            {
            }).Subscribe();
        }

        public void Play()
        {
            if (string.IsNullOrEmpty(_inputField.text)) return;
            var soundId = uint.Parse(_inputField.text);
            GlobalContainer.SoundManager.Play(soundId).Subscribe(success =>
            {
                Debug.Log("Play Finish : " + success);
            });
        }

        public void Stop()
        {
            if (string.IsNullOrEmpty(_inputField.text)) return;
            var soundId = uint.Parse(_inputField.text);
            GlobalContainer.SoundManager.Stop(soundId);
        }
    }
}
