﻿using UnityEngine;
using System.Collections;
using UniRx;
using UniRx.Triggers;
using DG.Tweening;

namespace NoroshiDebug.FieldEdit
{
    public class FieldEdit : MonoBehaviour
    {
        [SerializeField] Noroshi.BattleScene.MonoBehaviours.FieldView _fieldView = null;
        [SerializeField] float _moveDuration = 1.0f;

        void Awake()
        {
            DOTween.Init();
        }

    	void Start()
        {
            var initDammyWaveNo = 1;
            _moveX(Noroshi.BattleScene.WaveField.GetPositionX((byte)initDammyWaveNo), 0.01f).Subscribe();
            this.UpdateAsObservable().Where(_ => Input.GetMouseButtonUp(0))
            .Select(_ => 1)
            .Scan((acc, current) => acc + current)
            .SelectMany(currentDammyWaveNo => _moveX(Noroshi.BattleScene.WaveField.GetPositionX((byte)(currentDammyWaveNo + initDammyWaveNo)), _moveDuration))
            .Subscribe(_ => _fieldView.AdjustAfterSwitchWave())
            .AddTo(this);
    	}

        IObservable<bool> _moveX(float endPosX, float duration)
        {
            var tsCamera = Camera.main.transform;
            Subject<bool> onCompleteSubject = new Subject<bool>();
            tsCamera.DOMoveX(endPosX, duration).SetEase(Ease.InOutQuad).OnComplete(() => 
            {
                onCompleteSubject.OnNext(true);
                onCompleteSubject.OnCompleted();
            });
            return onCompleteSubject.AsObservable();
        }
    }
}
