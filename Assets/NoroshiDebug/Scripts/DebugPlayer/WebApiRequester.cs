﻿using UniRx;
using Noroshi.Core.WebApi.Response.Debug;
using Noroshi;

namespace NoroshiDebug.DebugPlayer
{
    public class WebApiRequester
    {
        /// プレイヤー情報リセット実行。レスポンスが帰ってきた後はログインシーンへ遷移してください。
        public static IObservable<PlayerDebugResponse> Reset()
        {
            return GlobalContainer.WebApiRequester.Post<PlayerDebugResponse>("PlayerDebug/Reset");
        }
        /// プレイヤー情報入れ替え実行。レスポンスが帰ってきた後はログインシーンへ遷移してください。
        public static IObservable<PlayerDebugResponse> Swap(uint targetPlayerId)
        {
            var request = new TargetPlayerIDRequest { TargetPlayerID = targetPlayerId };
            return GlobalContainer.WebApiRequester.Post<TargetPlayerIDRequest, PlayerDebugResponse>("PlayerDebug/Swap", request);
        }
        class TargetPlayerIDRequest
        {
            public uint TargetPlayerID { get; set; }
        }
        /// デバッグプレイヤー登録。
        public static IObservable<PlayerDebugResponse> Register(uint registerNum)
        {
            var request = new RegisterRequest { RegisterNum = registerNum };
            return GlobalContainer.WebApiRequester.Post<RegisterRequest, PlayerDebugResponse>("PlayerDebug/Register", request);
        }
        class RegisterRequest
        {
            public uint RegisterNum { get; set; }
        }

        public static IObservable<PlayerDebugResponse> ChangeLevel(ushort level)
        {
            var request = new ChangeLevelRequest { Level = level };
            return GlobalContainer.WebApiRequester.Post<ChangeLevelRequest, PlayerDebugResponse>("PlayerDebug/ChangeLevel", request);
        }
        class ChangeLevelRequest
        {
            public ushort Level { get; set; }
        }

        public static IObservable<PlayerDebugResponse> ChangeExp(uint exp)
        {
            var request = new ChangeExpRequest { Exp = exp };
            return GlobalContainer.WebApiRequester.Post<ChangeExpRequest, PlayerDebugResponse>("PlayerDebug/ChangeExp", request);
        }
        class ChangeExpRequest
        {
            public uint Exp { get; set; }
        }

        public static IObservable<PlayerDebugResponse> ChangeVipLevel(ushort vipLevel)
        {
            var request = new ChangeVipLevelRequest { VipLevel = vipLevel };
            return GlobalContainer.WebApiRequester.Post<ChangeVipLevelRequest, PlayerDebugResponse>("PlayerDebug/ChangeVipLevel", request);
        }
        class ChangeVipLevelRequest
        {
            public ushort VipLevel { get; set; }
        }

        public static IObservable<PlayerDebugResponse> ChangeStamina(ushort stamina)
        {
            var request = new ChangeStaminaRequest { Stamina = stamina };
            return GlobalContainer.WebApiRequester.Post<ChangeStaminaRequest, PlayerDebugResponse>("PlayerDebug/ChangeStamina", request);
        }
        class ChangeStaminaRequest
        {
            public ushort Stamina { get; set; }
        }

        public static IObservable<PlayerDebugResponse> ChangeGold(uint gold)
        {
            var request = new ChangeGoldRequest { Gold = gold };
            return GlobalContainer.WebApiRequester.Post<ChangeGoldRequest, PlayerDebugResponse>("PlayerDebug/ChangeGold", request);
        }
        class ChangeGoldRequest
        {
            public uint Gold { get; set; }
        }
        public static IObservable<PlayerDebugResponse> ChangeFreeGem(uint freeGem)
        {
            var request = new ChangeFreeGemRequest { FreeGem = freeGem };
            return GlobalContainer.WebApiRequester.Post<ChangeFreeGemRequest, PlayerDebugResponse>("PlayerDebug/ChangeFreeGem", request);
        }
        class ChangeFreeGemRequest
        {
            public uint FreeGem { get; set; }
        }
        public static IObservable<PlayerDebugResponse> ChangeArenaPoint(uint arenaPoint)
        {
            var request = new ChangeArenaPointRequest { ArenaPoint = arenaPoint };
            return GlobalContainer.WebApiRequester.Post<ChangeArenaPointRequest, PlayerDebugResponse>("PlayerDebug/ChangeArenaPoint", request);
        }
        class ChangeArenaPointRequest
        {
            public uint ArenaPoint { get; set; }
        }
        public static IObservable<PlayerDebugResponse> ChangeGuildPoint(uint guildPoint)
        {
            var request = new ChangeGuildPointRequest { GuildPoint = guildPoint };
            return GlobalContainer.WebApiRequester.Post<ChangeGuildPointRequest, PlayerDebugResponse>("PlayerDebug/ChangeGuildPoint", request);
        }
        class ChangeGuildPointRequest
        {
            public uint GuildPoint { get; set; }
        }
        public static IObservable<PlayerDebugResponse> ChangeExpeditionPoint(uint expeditionPoint)
        {
            var request = new ChangeExpeditionPointRequest { ExpeditionPoint = expeditionPoint };
            return GlobalContainer.WebApiRequester.Post<ChangeExpeditionPointRequest, PlayerDebugResponse>("PlayerDebug/ChangeExpeditionPoint", request);
        }
        class ChangeExpeditionPointRequest
        {
            public uint ExpeditionPoint { get; set; }
        }
    }
}
