using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using UniRx;
using Noroshi.Core.Game.Player;
using Noroshi.Core.Game.Story;
using Noroshi.UI;
using NoroshiDebug.DebugPlayer;
using NoroshiDebug.TimeDebug;
using UniLinq;

namespace Noroshi.NoroshiDebug {
    public class DebugPlayerController : MonoBehaviour {
        [SerializeField] Text txtPlayerName;
        [SerializeField] Text txtPlayerLevel;
        [SerializeField] Text txtPlayerExp;
        [SerializeField] Text txtGold;
        [SerializeField] Text txtGem;
        [SerializeField] Text txtArenaPoint;
        [SerializeField] Text txtGuildPoint;
        [SerializeField] Text txtExpeditionPoint;
        [SerializeField] Text txtStamina;
        [SerializeField] Text txtMaxStamina;
        [SerializeField] Text txtVipLevel;
        [SerializeField] Text txtYear;
        [SerializeField] Text txtMonth;
        [SerializeField] Text txtDay;
        [SerializeField] Text txtHour;
        [SerializeField] Text txtMinute;
        [SerializeField] Text txtSecond;
        [SerializeField] Text txtDayOfWeek;
        [SerializeField] Text txtStoryChapter;
        [SerializeField] Text txtStoryEpisode;
        [SerializeField] Text txtStoryStage;
        [SerializeField] Text txtBackStoryChapter;
        [SerializeField] Text txtBackStoryEpisode;
        [SerializeField] Text txtBackStoryStage;
        [SerializeField] BtnCommon btnResetData;
        [SerializeField] BtnCommon btnChangePlayer;
        [SerializeField] BtnCommon btnPlusPlayerLevel;
        [SerializeField] BtnCommon btnMinusPlayerLevel;
        [SerializeField] BtnCommon btnPlus10PlayerLevel;
        [SerializeField] BtnCommon btnMinus10PlayerLevel;
        [SerializeField] BtnCommon btnPlus10PlayerExp;
        [SerializeField] BtnCommon btnMinus10PlayerExp;
        [SerializeField] BtnCommon btnPlus1kPlayerExp;
        [SerializeField] BtnCommon btnMinus1kPlayerExp;
        [SerializeField] BtnCommon btnPlusGold;
        [SerializeField] BtnCommon btnMinusGold;
        [SerializeField] BtnCommon btnPlus10Gold;
        [SerializeField] BtnCommon btnMinus10Gold;
        [SerializeField] BtnCommon btnPlusGem;
        [SerializeField] BtnCommon btnMinusGem;
        [SerializeField] BtnCommon btnPlus10Gem;
        [SerializeField] BtnCommon btnMinus10Gem;
        [SerializeField] BtnCommon btnPlus100ArenaPoint;
        [SerializeField] BtnCommon btnMinus100ArenaPoint;
        [SerializeField] BtnCommon btnPlus1kArenaPoint;
        [SerializeField] BtnCommon btnMinus1kArenaPoint;
        [SerializeField] BtnCommon btnPlus100GuildPoint;
        [SerializeField] BtnCommon btnMinus100GuildPoint;
        [SerializeField] BtnCommon btnPlus1kGuildPoint;
        [SerializeField] BtnCommon btnMinus1kGuildPoint;
        [SerializeField] BtnCommon btnPlus100ExpeditionPoint;
        [SerializeField] BtnCommon btnMinus100ExpeditionPoint;
        [SerializeField] BtnCommon btnPlus1kExpeditionPoint;
        [SerializeField] BtnCommon btnMinus1kExpeditionPoint;
        [SerializeField] BtnCommon btnStamina1;
        [SerializeField] BtnCommon btnStaminaFull;
        [SerializeField] BtnCommon btnPlus1VipLevel;
        [SerializeField] BtnCommon btnMinus1VipLevel;
        [SerializeField] BtnCommon btnPlus10VipLevel;
        [SerializeField] BtnCommon btnMinus10VipLevel;
        [SerializeField] BtnCommon btnTimeSetting;
        [SerializeField] BtnCommon btnStorySetting;
        [SerializeField] BtnCommon btnBackStorySetting;
        [SerializeField] DebugTimeSetPanel timeSetPanel;
        [SerializeField] StoryDebug.ClearStoryForciblySetPanel clearStoryForciblySetPanel;
        [SerializeField] StoryDebug.ClearStoryForciblySetPanel clearBackStoryForciblySetPanel;
        [SerializeField] GameObject processing;
        [SerializeField] GameObject resetConfirmPanel;
        [SerializeField] GameObject changePlayerPanel;
        [SerializeField] BtnCommon btnCancelReset;
        [SerializeField] BtnCommon btnDecideReset;
        [SerializeField] BtnCommon btnCancelChangePlayer;
        [SerializeField] BtnCommon btnDecideChangePlayer;
        [SerializeField] InputField inputChangeID;
        [SerializeField] BtnCommon btnRegisterDebugPlayer;

        private int currentPlayerLevel;
        private int currentPlayerExp;
        private int currentGold;
        private int currentGem;
        private int currentArenaPoint;
        private int currentGuildPoint;
        private int currentExpeditionPoint;
        private int currentStamina;
        private int currentVipLevel;
        private int maxStamina;

        private TimeDebugRepository _timeDebug = new TimeDebugRepository();
        private bool isLoad = false;
        private bool isSetTime = false;

        StaminaHandler _staminaHandler = new StaminaHandler();

        private void Start() {
            GlobalContainer.SetFactory(() => new Repositories.RepositoryManager());
            var gcrm = GlobalContainer.RepositoryManager;
            gcrm.PlayerStatusRepository.Get().Do(playerData => {
                currentPlayerLevel = (int)playerData.Level;
                currentPlayerExp = (int)playerData.Exp;
                currentGold = (int)playerData.Gold;
                currentGem = (int)playerData.Gem;
                currentStamina = (int)_staminaHandler.CurrentValue(playerData.Level, playerData.LastStamina, playerData.LastStaminaUpdatedAt, GlobalContainer.TimeHandler.UnixTime);
                currentVipLevel = (int)playerData.VipLevel;
                maxStamina = (int)_staminaHandler.MaxValue(playerData.Level);
                txtPlayerName.text = playerData.Name.ToString();
                txtPlayerLevel.text = playerData.Level.ToString();
                txtPlayerExp.text = playerData.Exp.ToString();
                txtGold.text = playerData.Gold.ToString();
                txtGem.text = playerData.Gem.ToString();
                txtArenaPoint.text = playerData.ArenaPoint.ToString();
                txtGuildPoint.text = playerData.GuildPoint.ToString();
                txtExpeditionPoint.text = playerData.ExpeditionPoint.ToString();
                txtStamina.text = currentStamina.ToString();
                txtMaxStamina.text = maxStamina.ToString();
                txtVipLevel.text = currentVipLevel.ToString();
                isLoad = true;
            }).Subscribe();
            Story.WebApiRequester.ChapterList().Do(chapterListResponse =>            {                var chapterList = chapterListResponse.StoryChapters;                var lastStoryChapter = chapterList.Where(sc => sc.IsOpen).OrderByDescending(sc => sc.No).FirstOrDefault();                var lastStoryEpisode = lastStoryChapter.Episodes.Where(se => se.NormalStoryIsOpen).OrderByDescending(se => se.No).FirstOrDefault();                txtStoryChapter.text = string.Format("Chapter:{0}", lastStoryChapter.No);                txtStoryEpisode.text = string.Format("Episode:{0}", lastStoryEpisode.No);                Story.WebApiRequester.Episode(lastStoryEpisode.ID, StoryCategory.NormalStory).Do(episodeResponse =>                {                    var lastStoryStage = episodeResponse.StoryEpisode.Stages.Where(stage => stage.IsOpen).OrderByDescending(stage => stage.No).FirstOrDefault();                    txtStoryStage.text = string.Format("Stage:{0}", lastStoryStage.No);                }).Subscribe();                var lastBackStoryChapter = chapterList.Where(sc => sc.IsOpen && sc.Episodes.Any(se => se.BackStoryIsOpen)).OrderByDescending(sc => sc.No).FirstOrDefault();                var lastBackStoryEpisode = lastBackStoryChapter.Episodes.Where(se => se.BackStoryIsOpen).OrderByDescending(se => se.No).FirstOrDefault();                txtBackStoryChapter.text = string.Format("Chapter:{0}", lastStoryChapter.No);                txtBackStoryEpisode.text = string.Format("Episode:{0}", lastBackStoryEpisode.No);                Story.WebApiRequester.Episode(lastBackStoryEpisode.ID, StoryCategory.BackStory).Do(backEpisodeResponse =>                {                    var lastBackStoryStage = backEpisodeResponse.StoryEpisode.Stages.Where(stage => stage.IsOpen).OrderByDescending(stage => stage.No).FirstOrDefault();                    txtBackStoryStage.text = string.Format("Stage:{0}", lastBackStoryStage.No);                }).Subscribe();            }).Subscribe();
            _timeDebug.Get().Do(timeDebugData => {
                SetDebugTime(timeDebugData.DebugServerTime);
                isSetTime = true;
            }).Subscribe();

            btnResetData.OnClickedBtn.Subscribe(_ => {
                resetConfirmPanel.SetActive(true);
            });
            btnCancelReset.OnClickedBtn.Subscribe(_ => {
                resetConfirmPanel.SetActive(false);
            });
            btnDecideReset.OnClickedBtn.Subscribe(_ => {
                processing.SetActive(true);
                SoundController.Instance.StopBGM();
                WebApiRequester.Reset().Delay(TimeSpan.FromSeconds(3)).Do(__ => {
                    Destroy(BattleCharacterSelect.Instance.gameObject);
                    UILoading.Instance.ResetHistory();
                    UILoading.Instance.Destroy();
                    PlayerPrefs.DeleteAll();
                    GlobalContainer.ClearPlayerIDAndSessionID();
                    GlobalContainer.AssetBundleManager.UnloadAll();
                    SceneManager.LoadScene(Noroshi.UI.Constant.SCENE_LOGIN);
                }).Subscribe();
            });
            btnDecideReset.OnPlaySE.Subscribe(_ => {
                SoundController.Instance.PlaySE(SoundController.SEKeys.LEVEL_UP);
            });

            btnChangePlayer.OnClickedBtn.Subscribe(_ => {
                changePlayerPanel.SetActive(true);
            });
            btnCancelChangePlayer.OnClickedBtn.Subscribe(_ => {
                changePlayerPanel.SetActive(false);
            });
            btnDecideChangePlayer.OnClickedBtn.Subscribe(_ => {
                uint id = 0;
                uint.TryParse(inputChangeID.text, out id);
                if(id < 1) {return;}
                processing.SetActive(true);
                SoundController.Instance.StopBGM();
                WebApiRequester.Swap(id).Delay(TimeSpan.FromSeconds(3)).Do(__ => {
                    Destroy(BattleCharacterSelect.Instance.gameObject);
                    UILoading.Instance.ResetHistory();
                    PlayerPrefs.DeleteAll();
                    GlobalContainer.ClearPlayerIDAndSessionID();
                    GlobalContainer.AssetBundleManager.UnloadAll();
                    SceneManager.LoadScene(Noroshi.UI.Constant.SCENE_LOGIN);
                }).Subscribe();
            });
            btnRegisterDebugPlayer.OnClickedBtn.First()
                .SelectMany(_ => WebApiRequester.Register(100)).Subscribe().AddTo(this);

            btnPlusPlayerLevel.OnClickedBtn.Subscribe(ChangePlayerLevel);
            btnMinusPlayerLevel.OnClickedBtn.Subscribe(ChangePlayerLevel);
            btnPlus10PlayerLevel.OnClickedBtn.Subscribe(ChangePlayerLevel);
            btnMinus10PlayerLevel.OnClickedBtn.Subscribe(ChangePlayerLevel);

            btnPlus10PlayerExp.OnClickedBtn.Subscribe(ChangePlayerExp);
            btnMinus10PlayerExp.OnClickedBtn.Subscribe(ChangePlayerExp);
            btnPlus1kPlayerExp.OnClickedBtn.Subscribe(ChangePlayerExp);
            btnMinus1kPlayerExp.OnClickedBtn.Subscribe(ChangePlayerExp);

            btnPlus1VipLevel.OnClickedBtn.Subscribe(ChangeVipLevel);
            btnMinus1VipLevel.OnClickedBtn.Subscribe(ChangeVipLevel);
            btnPlus10VipLevel.OnClickedBtn.Subscribe(ChangeVipLevel);
            btnMinus10VipLevel.OnClickedBtn.Subscribe(ChangeVipLevel);

            btnPlusGold.OnClickedBtn.Subscribe(ChangeGold);
            btnMinusGold.OnClickedBtn.Subscribe(ChangeGold);
            btnPlus10Gold.OnClickedBtn.Subscribe(ChangeGold);
            btnMinus10Gold.OnClickedBtn.Subscribe(ChangeGold);

            btnPlusGem.OnClickedBtn.Subscribe(ChangeGem);
            btnMinusGem.OnClickedBtn.Subscribe(ChangeGem);
            btnPlus10Gem.OnClickedBtn.Subscribe(ChangeGem);
            btnMinus10Gem.OnClickedBtn.Subscribe(ChangeGem);

            btnPlus100ArenaPoint.OnClickedBtn.Subscribe(ChangeArenaPoint);
            btnMinus100ArenaPoint.OnClickedBtn.Subscribe(ChangeArenaPoint);
            btnPlus1kArenaPoint.OnClickedBtn.Subscribe(ChangeArenaPoint);
            btnMinus1kArenaPoint.OnClickedBtn.Subscribe(ChangeArenaPoint);

            btnPlus100GuildPoint.OnClickedBtn.Subscribe(ChangeGuildPoint);
            btnMinus100GuildPoint.OnClickedBtn.Subscribe(ChangeGuildPoint);
            btnPlus1kGuildPoint.OnClickedBtn.Subscribe(ChangeGuildPoint);
            btnMinus1kGuildPoint.OnClickedBtn.Subscribe(ChangeGuildPoint);

            btnPlus100ExpeditionPoint.OnClickedBtn.Subscribe(ChangeExpeditionPoint);
            btnMinus100ExpeditionPoint.OnClickedBtn.Subscribe(ChangeExpeditionPoint);
            btnPlus1kExpeditionPoint.OnClickedBtn.Subscribe(ChangeExpeditionPoint);
            btnMinus1kExpeditionPoint.OnClickedBtn.Subscribe(ChangeExpeditionPoint);


            btnStaminaFull.OnClickedBtn.Subscribe(_ => {
                RecoverStamina();
            });
            btnStamina1.OnClickedBtn.Subscribe(ChangeStamina);

            btnTimeSetting.OnClickedBtn.Subscribe(_ => {
                timeSetPanel.OpenTimeSetPanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            btnStorySetting.OnClickedBtn.Subscribe(_ =>
            {
                clearStoryForciblySetPanel.OpenClearStoryForciblySetPanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });
            btnBackStorySetting.OnClickedBtn.Subscribe(_ =>
            {
                clearBackStoryForciblySetPanel.OpenClearStoryForciblySetPanel();
                SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
            });

            timeSetPanel.OnSetDebugTime.Subscribe(debugTime => {
                SetDebugTime(debugTime);
            });

            StartCoroutine("OnLoading");
        }

        private IEnumerator OnLoading() {
            while(!isLoad || !isSetTime) {
                yield return new WaitForEndOfFrame();
            }
            if(UILoading.Instance != null) {
                UILoading.Instance.HideLoading();
            }
        }
        
        private void SetDebugTime(DateTime debugTime) {
            var hour = debugTime.Hour;
            string hourString = hour < 10 ? "0" + hour : hour.ToString();
            var minute = debugTime.Minute;
            string minuteString = minute < 10 ? "0" + minute : minute.ToString();
            var second = debugTime.Second;
            string secondString = second < 10 ? "0" + second : second.ToString();
            txtYear.text = debugTime.Year.ToString();
            txtMonth.text = debugTime.Month.ToString();
            txtDay.text = debugTime.Day.ToString();
            txtHour.text = hourString;
            txtMinute.text = minuteString;
            txtSecond.text = secondString;
            txtDayOfWeek.text = debugTime.DayOfWeek.ToString();
        }
        
        private void ChangePlayerLevel(int value) {
            currentPlayerLevel += value;
            if(currentPlayerLevel > 99) {
                currentPlayerLevel = 99;
            }
            if(currentPlayerLevel < 1) {
                currentPlayerLevel = 1;
            }
            processing.SetActive(true);
            WebApiRequester.ChangeLevel((ushort)currentPlayerLevel).Do(response => {
                currentPlayerLevel = response.PlayerStatus.Level;
                currentPlayerExp = (int)response.PlayerStatus.Exp;
                currentStamina = (int)_staminaHandler.CurrentValue(response.PlayerStatus.Level, response.PlayerStatus.LastStamina, response.PlayerStatus.LastStaminaUpdatedAt, GlobalContainer.TimeHandler.UnixTime);
                maxStamina = (int)_staminaHandler.MaxValue(response.PlayerStatus.Level);
                txtPlayerLevel.text = currentPlayerLevel.ToString();
                txtPlayerExp.text = response.PlayerStatus.Exp.ToString();
                txtStamina.text = currentStamina.ToString();
                txtMaxStamina.text = maxStamina.ToString();
                processing.SetActive(false);
            }).Subscribe();
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }

        private void ChangePlayerExp(int value) {
            currentPlayerExp = Math.Max(1,(currentPlayerExp + value));
            processing.SetActive(true);
            WebApiRequester.ChangeExp((uint)currentPlayerExp).Do(response => {
                currentPlayerLevel = response.PlayerStatus.Level;
                currentPlayerExp = (int)response.PlayerStatus.Exp;
                currentStamina = (int)_staminaHandler.CurrentValue(response.PlayerStatus.Level, response.PlayerStatus.LastStamina, response.PlayerStatus.LastStaminaUpdatedAt, GlobalContainer.TimeHandler.UnixTime);
                maxStamina = (int)_staminaHandler.MaxValue(response.PlayerStatus.Level);
                txtPlayerLevel.text = currentPlayerLevel.ToString();
                txtPlayerExp.text = response.PlayerStatus.Exp.ToString();
                txtStamina.text = currentStamina.ToString();
                txtMaxStamina.text = maxStamina.ToString();
                processing.SetActive(false);
            }).Subscribe();
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }

        private void ChangeVipLevel(int value)
        {
            currentVipLevel = Mathf.Clamp(currentVipLevel + value, 0, 14);
            processing.SetActive(true);
            WebApiRequester.ChangeVipLevel((ushort)currentVipLevel).Do(response => {
                currentVipLevel = (int)response.PlayerStatus.VipLevel;
                txtVipLevel.text = currentVipLevel.ToString();
                processing.SetActive(false);
            }).Subscribe();
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }

        private void ChangeGold(int value) {
            currentGold += value;
            if(currentGold > 99999999) {
                currentGold = 99999999;
            }
            if(currentGold < 0) {
                currentGold = 0;
            }
            processing.SetActive(true);
            WebApiRequester.ChangeGold((uint)currentGold).Do(response => {
                currentGold = (int)response.PlayerStatus.Gold;
                txtGold.text = currentGold.ToString();
                processing.SetActive(false);
            }).Subscribe();
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }

        private void ChangeGem(int value) {
            currentGem += value;
            if(currentGem > 99999999) {
                currentGem = 99999999;
            }
            if(currentGem < 0) {
                currentGem = 0;
            }
            processing.SetActive(true);
            WebApiRequester.ChangeFreeGem((uint)currentGem).Do(response => {
                currentGem = (int)response.PlayerStatus.Gem;
                txtGem.text = currentGem.ToString();
                processing.SetActive(false);
            }).Subscribe();
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }

        private void ChangeArenaPoint(int value)
        {
            currentArenaPoint += value;
            if (currentArenaPoint > 99999999)
            {
                currentArenaPoint = 99999999;
            }
            if (currentArenaPoint < 0)
            {
                currentArenaPoint = 0;
            }
            processing.SetActive(true);
            WebApiRequester.ChangeArenaPoint((uint)currentArenaPoint).Do(response => {
                currentArenaPoint = (int)response.PlayerStatus.ArenaPoint;
                txtArenaPoint.text = currentArenaPoint.ToString();
                processing.SetActive(false);
            }).Subscribe();
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }

        private void ChangeGuildPoint(int value)
        {
            currentGuildPoint += value;
            if (currentGuildPoint > 99999999)
            {
                currentGuildPoint = 99999999;
            }
            if (currentGuildPoint < 0)
            {
                currentGuildPoint = 0;
            }
            processing.SetActive(true);
            WebApiRequester.ChangeGuildPoint((uint)currentGuildPoint).Do(response => {
                currentGuildPoint = (int)response.PlayerStatus.GuildPoint;
                txtGuildPoint.text = currentGuildPoint.ToString();
                processing.SetActive(false);
            }).Subscribe();
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }

        private void ChangeExpeditionPoint(int value)
        {
            currentExpeditionPoint += value;
            if (currentExpeditionPoint > 99999999)
            {
                currentExpeditionPoint = 99999999;
            }
            if (currentExpeditionPoint < 0)
            {
                currentExpeditionPoint = 0;
            }
            processing.SetActive(true);
            WebApiRequester.ChangeExpeditionPoint((uint)currentExpeditionPoint).Do(response => {
                currentExpeditionPoint = (int)response.PlayerStatus.ExpeditionPoint;
                txtExpeditionPoint.text = currentExpeditionPoint.ToString();
                processing.SetActive(false);
            }).Subscribe();
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }

        private void RecoverStamina() {
            processing.SetActive(true);
            WebApiRequester.ChangeStamina((ushort)maxStamina).Do(response => {
                currentStamina = (int)_staminaHandler.CurrentValue(response.PlayerStatus.Level, response.PlayerStatus.LastStamina, response.PlayerStatus.LastStaminaUpdatedAt, GlobalContainer.TimeHandler.UnixTime);
                txtStamina.text = currentStamina.ToString();
                processing.SetActive(false);
            }).Subscribe();
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }

        private void ChangeStamina(int value) {
            currentStamina = value;
            processing.SetActive(true);
            WebApiRequester.ChangeStamina((ushort)value).Do(response => {
                currentStamina = (int)_staminaHandler.CurrentValue(response.PlayerStatus.Level, response.PlayerStatus.LastStamina, response.PlayerStatus.LastStaminaUpdatedAt, GlobalContainer.TimeHandler.UnixTime);
                txtStamina.text = currentStamina.ToString();
                processing.SetActive(false);
            }).Subscribe();
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }
    }
}
