﻿using UniLinq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Noroshi.WebApi;
using Noroshi;
using Noroshi.Core.WebApi.Response.Debug;
using Noroshi.Core.Game.Battle;
using UIConstant = Noroshi.UI.Constant;

namespace NoroshiDebug.BattleDebug
{
    public class BattleDebugController : MonoBehaviour
    {
        [SerializeField] Image _loadingImage;
        [SerializeField] InputField _ownPartyIdInputField;
        [SerializeField] InputField _ownLevelInputField;
        [SerializeField] InputField _ownPromotionLevelInputField;
        [SerializeField] InputField _ownEvolutionLevelInputField;
        [SerializeField] InputField _enemyPartyIdInputField;
        [SerializeField] InputField _enemyLevelInputField;
        [SerializeField] InputField _enemyPromotionLevelInputField;
        [SerializeField] InputField _enemyEvolutionLevelInputField;

        void Awake()
        {
            _loadingImage.gameObject.SetActive(Noroshi.UI.UILoading.Instance == null);
            GlobalContainer.WebApiRequester.LoginForUnityEditor()
                .Subscribe(_ =>
                {
                    _initializeInputFields();
                    _loadingImage.gameObject.SetActive(false);
                    if (Noroshi.UI.UILoading.Instance != null) Noroshi.UI.UILoading.Instance.HideLoading();
                });
        }
        void _initializeInputFields()
        {
            _ownLevelInputField.text = "1";
            _ownPromotionLevelInputField.text = "1";
            _ownEvolutionLevelInputField.text = "1";
            _enemyLevelInputField.text = "1";
            _enemyPromotionLevelInputField.text = "1";
            _enemyEvolutionLevelInputField.text = "1";
        }

        public void TransitToDebugBattle()
        {
            _transitToDebugBattle()
                .Subscribe(_ =>
                {
                });
        }

        IObservable<bool> _transitToDebugBattle()
        {
            return _setDebugBattleSessionResponse()
                .Do(_ => Noroshi.BattleScene.Bridge.Transition.TransitToDebugBattle())
                .Select(_ => true);
        }
        IObservable<SetDebugBattleSessionResponse> _setDebugBattleSessionResponse()
        {
            var request = new SetDebugBattleSessionRequest
            {
                OwnPartyID = uint.Parse(_ownPartyIdInputField.text),
                OwnLevel = ushort.Parse(_ownLevelInputField.text),
                OwnPromotionLevel = byte.Parse(_ownPromotionLevelInputField.text),
                OwnEvolutionLevel = byte.Parse(_ownEvolutionLevelInputField.text),
                EnemyPartyID = uint.Parse(_enemyPartyIdInputField.text),
                EnemyLevel = ushort.Parse(_enemyLevelInputField.text),
                EnemyPromotionLevel = byte.Parse(_enemyPromotionLevelInputField.text),
                EnemyEvolutionLevel = byte.Parse(_enemyEvolutionLevelInputField.text),
            };
            return GlobalContainer.WebApiRequester.Post<SetDebugBattleSessionRequest, SetDebugBattleSessionResponse>("/BattleDebug/SetDebugBattleSession", request);
        }

        class SetDebugBattleSessionRequest
        {
            public uint OwnPartyID { get; set; }
            public ushort OwnLevel { get; set; }
            public byte OwnPromotionLevel { get; set; }
            public byte OwnEvolutionLevel { get; set; }
            public uint EnemyPartyID { get; set; }
            public ushort EnemyLevel { get; set; }
            public byte EnemyPromotionLevel { get; set; }
            public byte EnemyEvolutionLevel { get; set; }
        }
    }
}
