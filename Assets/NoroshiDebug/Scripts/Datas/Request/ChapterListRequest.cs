﻿using Noroshi.Core.Game.Story;

namespace NoroshiDebug.Datas.Request
{
    class ChapterListRequest
    {
        public StoryCategory Category { get; set; }
    }
}
