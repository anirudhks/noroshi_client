﻿using Noroshi.Core.Game.Story;
using Noroshi.UI;
using UnityEngine;
using UniRx;

namespace Noroshi.NoroshiDebug.StoryDebug
{
    internal class ClearStoryForciblySetPanel : MonoBehaviour
    {
        [SerializeField] private GameObject timeSetWrapper;
        [SerializeField] private GameObject content;
        [SerializeField] private BtnCommon btnClose;
        [SerializeField] private StoryCategory category;

        private void Start()
        {
            btnClose.OnClickedBtn.Subscribe(_ => {
                CloseClearStoryForciblySetPanel();
            });
        }

        public void OpenClearStoryForciblySetPanel()
        {
            gameObject.SetActive(true);
            TweenA.Add(timeSetWrapper, 0.2f, 1).From(0).EaseInCubic();
            var wapperInstance = Resources.Load("StoryDebug/ClearStoryForciblySetWrapper") as GameObject;
            WebApiRequester.ChapterList(category).Do(response =>
            {
                var chapters = response.Chapters;
                foreach (var chapter in chapters)
                {
                    foreach (var episode in chapter.Episodes)
                    {
                        foreach (var stage in episode.Stages)
                        {
                            var wapper = Instantiate(wapperInstance);
                            wapper.transform.SetParent(content.gameObject.transform, false);
                            var clearSetWapper = wapper.GetComponent<ClearStageForciblySetWapper>();
                            clearSetWapper.StageID = stage.StageID;
                            clearSetWapper.ChapterNo = chapter.ChapterNo;
                            clearSetWapper.EpisodeNo = episode.EpisodeNo;
                            clearSetWapper.StageNo = stage.StageNo;
                            clearSetWapper.ChangeText();
                        }
                    }
                }
            }).Subscribe();
        }

        public void CloseClearStoryForciblySetPanel()
        {
            TweenA.Add(timeSetWrapper, 0.2f, 0).EaseOutCubic().Then(() => {
                gameObject.SetActive(false);
            });
            SoundController.Instance.PlaySE(SoundController.SEKeys.SELECT);
        }
    }
}
