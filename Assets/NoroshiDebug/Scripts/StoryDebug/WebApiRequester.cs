﻿using Noroshi.Core.Game.Story;
using Noroshi.Core.WebApi.Response.Debug;
using NoroshiDebug.Datas.Request;
using UniRx;

namespace Noroshi.NoroshiDebug.StoryDebug
{
    public class WebApiRequester
    {
        static Noroshi.WebApi.WebApiRequester _getWebApiRequester()
        {
            return new Noroshi.WebApi.WebApiRequester();
        }

        public static IObservable<ChapterListResponse> ChapterList(StoryCategory category)
        {
            var request = new ChapterListRequest
            {
                Category = category
            };
            return _getWebApiRequester().Post<ChapterListRequest, ChapterListResponse>("StoryDebug/ChapterList", request);
        }
        public static IObservable<ClearStageForciblyResponse> ClearStageForcibly(uint stageId)
        {
            var request = new ClearStageForciblyRequest
            {
                StageID = stageId
            };
            return _getWebApiRequester().Post<ClearStageForciblyRequest, ClearStageForciblyResponse>("StoryDebug/ClearStageForcibly", request);
        }


    }
}