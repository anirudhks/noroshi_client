﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Noroshi.NoroshiDebug.StoryDebug
{
    class ClearStageForciblySetWapper : MonoBehaviour
    {
        public uint StageID;
        public uint ChapterNo;
        public uint EpisodeNo;
        public uint StageNo;

        [SerializeField] private Text txtStageInfo;
        [SerializeField] private BtnCommon btnSettingClearStoryForcibly;

        public void Start()
        {
            btnSettingClearStoryForcibly.OnClickedBtn.Subscribe(_ =>
            {
                WebApiRequester.ClearStageForcibly(StageID).Subscribe();
            });
        }

        public void ChangeText()
        {
            txtStageInfo.text = string.Format("Chapter:{0} Episode:{1} Stage:{2}", ChapterNo, EpisodeNo, StageNo);
        }
    }
}
