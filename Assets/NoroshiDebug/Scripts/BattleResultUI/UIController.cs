﻿using UnityEngine;
using Noroshi.BattleScene.UI;

namespace NoroshiDebug.BattleResultUI
{
    public class UIController : Noroshi.MonoBehaviours.MonoBehaviour
    {
        [SerializeField] Noroshi.MonoBehaviours.MonoBehaviour _modalContainer;

        public void AddResultUIView(IResultUIView uiView)
        {
            uiView.SetParent(_modalContainer, false);
        }
    }
}