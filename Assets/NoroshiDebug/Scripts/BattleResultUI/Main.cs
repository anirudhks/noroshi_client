using System;
using UniLinq;
using UniRx;
using UnityEngine;
using Noroshi.BattleScene;
using Noroshi.BattleScene.UI;

namespace NoroshiDebug.BattleResultUI
{
    public class Main : MonoBehaviour
    {
        enum ResultType
        {
            Win,
            Loss,
            TrainingDefeatingNumResult,
            TrainingTotalDamageResult,
        }
        
        [SerializeField] Factory _factory;
        [SerializeField] ResultType _resultType;
        [SerializeField] byte _rank = 1;
        [SerializeField] ushort _playerExp = 1;
        [SerializeField] ushort _money = 10;
        [SerializeField] uint[] _itemIds;
        [SerializeField] SerializableCharacterThumbnail[] _ownCharacterThumbnails;
        [SerializeField] float[] _ownPreviousExpRatio;
        [SerializeField] float[] _ownCurrentExpRatio;
        [SerializeField] ushort[] _ownLevelUpNum;
        [SerializeField] ushort _defeatingNum;
        [SerializeField] uint _totalDamage;

        void Start()
        {
            var prefabName = _resultType == ResultType.Win || _resultType == ResultType.Loss
                ? _resultType.ToString() + "UI"
                : "TrainingResultUI";
            Noroshi.GlobalContainer.WebApiRequester.LoginForUnityEditor()
            .SelectMany(_ => Noroshi.GlobalContainer.Load(0))
            .SelectMany(_ => _factory.LoadResultUIView(prefabName))
            .Delay(TimeSpan.FromSeconds(1))
            .SelectMany(v => _factory.LoadUIController().Do(c => c.AddResultUIView(v)).Select(_ => v))
            .Do(_setParams)
            .SelectMany(v => v.Open())
            .Subscribe();
        }

        void _setParams(IResultUIView view)
        {
            var possessionObjects = _itemIds.Select(itemId => new Noroshi.Core.WebApi.Response.Possession.PossessionObject
            {
                ID = itemId,
                Num = 3,
            });
            switch (_resultType)
            {
                case ResultType.Win:
                    view.SetRank(_rank);
                    view.SetPlayerExp(_playerExp);
                    view.SetGold(_money);
                    view.SetRewards(possessionObjects);
                    view.SetOwnCharacterThumbnails(_ownCharacterThumbnails.Select(ch => new CharacterThumbnail(ch.CharacterID, ch.Level, ch.EvolutionLevel, ch.PromotionLevel, ch.SkinLevel, ch.IsDead)));
                    for (byte no = 1; no <= _ownPreviousExpRatio.Length; no++)
                    {
                        view.SetOwnCharacterProgress(no, _ownPreviousExpRatio[no - 1], _ownCurrentExpRatio[no - 1], _ownLevelUpNum[no - 1]);
                    }
                    break;
                case ResultType.Loss:
                    view.SetTips(1);
                    break;
                case ResultType.TrainingDefeatingNumResult:
                    view.SetTrainingResult(_defeatingNum, null);
                    view.SetRewards(possessionObjects);
                    break;
                case ResultType.TrainingTotalDamageResult:
                    view.SetTrainingResult(null, _totalDamage);
                    view.SetRewards(possessionObjects);
                    break;
                default:
                    break;
            }
        }
    }
}
