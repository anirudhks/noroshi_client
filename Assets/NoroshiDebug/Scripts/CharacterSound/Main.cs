﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using UniLinq;
using UniRx;
using Noroshi;
using Noroshi.Random;
using Noroshi.Character;
using Noroshi.Core.Game.Character;
using Noroshi.Core.WebApi.Response.Master;
using Noroshi.BattleScene.Sound;
using DG.Tweening;

namespace NoroshiDebug.CharacterSound
{
    public class Main : MonoBehaviour
    {
        // 準備中パネル
        [SerializeField] RectTransform _loadingPanel;
        // キャラクター表示位置
        [SerializeField] GameObject _characterStage;
        // キャラクターID
        [SerializeField] InputField _characterInputField;
        // スキン名
        [SerializeField] Dropdown _skinDropDown;
        // タイムラインパネル
        [SerializeField] RectTransform _timelinePanel;
        // アクション名
        [SerializeField] Dropdown _timelineDropDown;
        // グループ番号
        [SerializeField] InputField _timelineInputField;
        // タイムラインプレハブ
        [SerializeField] GameObject _timelineSoundPanelPrefab;
        // タイムライン情報
        [SerializeField] Text _timelineSoundText;
        // タイムライン追加ボタン
        [SerializeField] Button _timelineSoundButton;
        // 再生パネル
        [SerializeField] RectTransform _playPanel;
        // 再生時間
        [SerializeField] Text _playTimeText;

        // キャラクターデータ
        private Noroshi.Character.CharacterView _characterView;
        private Noroshi.BattleScene.MonoBehaviours.ActionView _actionView;
        private Noroshi.Core.WebApi.Response.Master.Character _characterMasterData;

        // タイムラインデータ
        private String _timelineAnimationName;
        private CharacterAnimation _timelineAnimation;
        private String _timelineSuffix;
        private byte _timelineGroup;
        private List<GameObject> _timelineList = new List<GameObject>();

        // 一時停止フラグ
        private bool _isStart = false;
        private bool _isStop = false;
        // タイムカウントデータ
        private float _timeCountValueStart = 0.0f;
        private float _timeCountValueDiff = 0.0f;

        // トリガー
        private Color _triggerColorUsual = new Color(1.0f, 1.0f, 1.0f, 0.5f);
        private Color _triggerColorFlash = new Color(1.0f, 0.0f, 0.0f, 0.5f);
        private Tweener _triggerTweener = null;

        // 起動時処理
        void Awake()
        {
            // GlobalContainer に DI
            GlobalContainer.SetFactory<IRandomGenerator>(() => new RandomGenerator());

            // SceneContainer に DI
            SceneContainer.Register<Noroshi.BattleScene.SceneManager>(() => 
                new Noroshi.BattleScene.SceneManager(
                    Noroshi.BattleScene.Bridge.Transition.BattleType,
                    Noroshi.BattleScene.Bridge.Transition.BattleCategory,
                    Noroshi.BattleScene.Bridge.Transition.ID,
                    Noroshi.BattleScene.Bridge.Transition.PlayerCharacterIDs,
                    null,
                    Noroshi.BattleScene.Bridge.Transition.PaymentNum
                )
            );

            // データロード
            Caching.CleanCache();
            GlobalContainer.WebApiRequester.LoginForUnityEditor()
            .Do(_ => {
                 // データロード完了
                _loadingPanel.gameObject.SetActive(false);
            }).Subscribe();

            // UIを一部非表示
            _timelinePanel.gameObject.SetActive(false);
            _playPanel.gameObject.SetActive(false);

            // タイムカウンタ始動
            _timeCountSet();
        }

        // キャラクターロードボタン
        public void LoadCharacter()
        {
            // キャラクターID確認
            if (string.IsNullOrEmpty(_characterInputField.text)) return;
            var characterID = uint.Parse(_characterInputField.text);

            // Spine アニメーションのアンローダー。
            var unloader = new Flaggs.Unity.Spine.SkeletonAnimationUnloader();

            if (_characterView != null)
            {
                // 古いキャラクター削除
                unloader.UnloadTextureAndDestroy(_characterView.gameObject);
                _characterView = null;
            };

            if (_actionView != null)
            {
                // 古いアクション削除
                unloader.UnloadTextureAndDestroy(_actionView.gameObject);
                _actionView = null;
            }

            var factory = new Noroshi.BattleScene.MonoBehaviours.Factory();

            // characterViewの読み込み
            factory.BuildCharacterView(characterID).Subscribe(characterView =>
                {
                    _characterView = (Noroshi.Character.CharacterView)characterView;
                    _characterMasterData = GlobalContainer.MasterManager.CharacterMaster.Get(characterID);
                    _characterView.transform.parent = _characterStage.transform;

                    // characterViewにサウンド再生設定
                    _characterView.GetOnPlaySoundObservable().Subscribe(soundID => {GlobalContainer.SoundManager.Play(soundID).Subscribe();});
                    _characterView.GetOnCancelSoundObservable().Subscribe(soundID => {GlobalContainer.SoundManager.Cancel(soundID);});

                    // アクションをリストアップ
                    var options = new List<String>();
                    var informations = _characterView.GetActionAnimationInformations();
                    foreach (var information in informations) options.Add(information.Name);
                    options.Add("damage");
                    options.Add("dead");
                    options.Add("win");
                    _timelineDropDown.ClearOptions();
                    _timelineDropDown.AddOptions(options);

                    // 位置変更
                    _setPosition(_characterView.transform, 0.0f, 0.0f);
                    // サイズ変更
                    var sca = _characterView.transform.localScale;
                    sca.x = 0.4f;
                    sca.y = 0.4f;
                    _characterView.transform.localScale = sca;

                    // キャラクターの設定
                    ChangeSkin();
                    LoadTimeline();

                    // トリガー
                    var image = _playPanel.GetComponent<Image>();
                    _characterView.GetOnExecuteActionObservable().Subscribe(_ => {
                        if (_triggerTweener != null)
                        {
                            _triggerTweener.Kill();
                            _triggerTweener = null;
                        }
                        image.color = _triggerColorFlash;
                        _triggerTweener = image.DOColor(_triggerColorUsual, 0.5f);
                    }).AddTo(_characterView);

                    _characterView.SetActive(true);
                }, e =>
                {
                    Debug.Log("no character");
                });

            // actionViewの読み込み
            factory.BuildActionView(characterID).Subscribe(actionView =>
                {
                    _actionView = (Noroshi.BattleScene.MonoBehaviours.ActionView)actionView;
                    _actionView.transform.parent = _characterStage.transform;
                }, e =>
                {
                    Debug.Log("no Action");
                });

            // UIを表示
            _timelinePanel.gameObject.SetActive(true);
            _playPanel.gameObject.SetActive(true);
        }

        // ズームインする
        public void ZoomIn()
        {
            var sca = _characterView.transform.localScale;
            sca.x *= 1.2f;
            sca.y *= 1.2f;
            _characterView.transform.localScale = sca;
        }

        // ズームアウトする
        public void ZoomOut()
        {
            var sca = _characterView.transform.localScale;
            sca.x /= 1.2f;
            sca.y /= 1.2f;
            _characterView.transform.localScale = sca;
        }

        // スキン変更ボタン
        public void ChangeSkin()
        {
            if (_characterView == null) return;

            _characterView.SetSkinName(_skinDropDown.captionText.text);
        }

        // タイムラインロードボタン
        public void LoadTimeline()
        {
            if (_characterView == null) return;

            // 選択しているアニメーションの確認
            _timelineAnimationName = _timelineDropDown.captionText.text;
            foreach(KeyValuePair<CharacterAnimation, string> pair in Constant.ANIMATION_NAME_MAP)
            {
                if (_timelineAnimationName.IndexOf(pair.Value) == 0)
                {
                    _timelineAnimation = pair.Key;
                    _timelineSuffix = (_timelineAnimationName.Length > pair.Value.Length + 1) ? _timelineAnimationName.Substring(pair.Value.Length + 1) : "";
                }
            }

            // 表示しているタイムラインデータを全て消す
            foreach (var timeline in _timelineList)
            {
                DestroyImmediate(timeline);
            }
            _timelineList.Clear();

            _timelineGroup = string.IsNullOrEmpty(_timelineInputField.text) ? (byte)1 : byte.Parse(_timelineInputField.text);

            // タイムラインデータ作成
            for (int i = 0; i < _characterMasterData.AnimationSounds.Length; i++)
            {
                var keyFrame = _characterMasterData.AnimationSounds[i];
                if (keyFrame.Animation != _timelineAnimation) continue;
                if (keyFrame.AnimationNameSuffix != _timelineSuffix) continue;
                if (keyFrame.Group != _timelineGroup) continue;

                // タイムラインデータを作成して設置
                var timeline = Instantiate<GameObject>(_timelineSoundPanelPrefab);
                timeline.transform.SetParent(_timelinePanel.transform);
                _timelineList.Add(timeline);
                // タイムラインデータ設定
                var timelineSound = timeline.GetComponent<TimelineSound>();
                timelineSound.Time = keyFrame.Time;
                timelineSound.SoundID = keyFrame.SoundID;
                timelineSound.GetOnDeleteObservable().Subscribe(_deleteTimeline);
            }

            // タイムラインの説明作成
            _timelineSoundText.text = string.Format("{0} Group:{1}", _timelineAnimationName, _timelineGroup);
            // タイムラインデータの表示位置設定
            _layoutTimeline();
        }

        // タイムライン追加ボタン
        public void AddTimeline()
        {
            // タイムラインデータを作成して設置
            var timeline = Instantiate<GameObject>(_timelineSoundPanelPrefab);
            timeline.transform.SetParent(_timelinePanel.transform);
            _timelineList.Add(timeline);
            // タイムラインデータ設定
            var timelineSound = timeline.GetComponent<TimelineSound>();
            timelineSound.Time = 0;
            timelineSound.SoundID = 0;
            timelineSound.GetOnDeleteObservable().Subscribe(_deleteTimeline);

            // タイムラインデータの表示位置設定
            _layoutTimeline();
        }

        // タイムライン削除処理
        private void _deleteTimeline(GameObject timeline)
        {
            _timelineList.Remove(timeline);
            DestroyImmediate(timeline);

            // タイムラインデータの表示位置設定
            _layoutTimeline();
        }

        // タイムラインデータの表示位置設定
        private void _layoutTimeline()
        {
            // 時間でソートする
            _timelineList.Sort((timeline1, timeline2) => {
                var timelineSound1 = timeline1.GetComponent<TimelineSound>();
                var timelineSound2 = timeline2.GetComponent<TimelineSound>();
                if (timelineSound1.Time > timelineSound2.Time) return 1;
                if (timelineSound1.Time < timelineSound2.Time) return -1;
                if (timelineSound1.SoundID > timelineSound2.SoundID) return 1;
                if (timelineSound1.SoundID < timelineSound2.SoundID) return -1;
                return 0;
            });

            // タイムラインデータ位置設定
            var index = 0;
            foreach (var timeline in _timelineList)
            {
                var timelineRect = timeline.GetComponent<RectTransform>();
                var timelineVec = timelineRect.anchoredPosition;
                timelineVec.x = -10;
                timelineVec.y = -(90 + 40 * index);
                timelineRect.anchoredPosition = timelineVec;
                index++;
            }

            // 追加ボタン位置設定
            var buttonRect = _timelineSoundButton.GetComponent<RectTransform>();
            var buttonVec = buttonRect.anchoredPosition;
            buttonVec.x = -10;
            buttonVec.y = -(90 + 40 * index);
            buttonRect.anchoredPosition = buttonVec;

            // パネルサイズ設定
            var sizeVec = _timelinePanel.sizeDelta;
            sizeVec.y = 130 + 40 * index;
            _timelinePanel.sizeDelta = sizeVec;
        }

        // アニメーション再生
        public void PlayAnimation()
        {
            if (_characterView == null) return;

            if (_timelineAnimationName != _timelineDropDown.captionText.text)
            {
                // 選択しているアニメーションが変わったので読み込み直し
                LoadTimeline();
            }
            else
            {
                // タイムラインデータの表示位置設定
                _layoutTimeline();
            }
                
            // サウンドリスト作成
            var index = 0;
            var animationSounds = new CharacterAnimationSound[_timelineList.Count];
            foreach (var timeline in _timelineList)
            {
                var timelineSound = timeline.GetComponent<TimelineSound>();
                var animationSound = new CharacterAnimationSound();
                animationSound.CharacterID = _characterMasterData.ID;
                animationSound.Animation = _timelineAnimation;
                animationSound.Group = _timelineGroup;
                animationSound.Time = timelineSound.Time;
                animationSound.SoundID = timelineSound.SoundID;
                animationSounds[index++] = animationSound;
            }

            // アニメーション再生開始
            _characterView.SetAnimationSounds(animationSounds);
            _characterView.PlayAnimation(_timelineAnimation, false, _timelineSuffix).Subscribe(_ =>
            {
                // タイムカウンタ停止
                _timeCountStop();
                _isStart = false;
            });
            // タイムカウンタ開始
            _timeCountStart();
            _isStart = true;
            _isStop = false;

            if (_actionView != null)
            {
                // 付随するアクションも再生開始
                try{
                    var pos = new Vector2 (0.0f, 0.0f);
                    var dir = Noroshi.Grid.Direction.Left;
                    switch (_timelineAnimation)
                    {
                        case CharacterAnimation.Action0: _actionView.Appear(pos, dir, Noroshi.BattleScene.Constant.ACTION_RANK_0_ANIMATION_NAME, 0); break;
                        case CharacterAnimation.Action1: _actionView.Appear(pos, dir, Noroshi.BattleScene.Constant.ACTION_RANK_1_ANIMATION_NAME, 0); break;
                        case CharacterAnimation.Action2: _actionView.Appear(pos, dir, Noroshi.BattleScene.Constant.ACTION_RANK_2_ANIMATION_NAME, 0); break;
                        case CharacterAnimation.Action3: _actionView.Appear(pos, dir, Noroshi.BattleScene.Constant.ACTION_RANK_3_ANIMATION_NAME, 0); break;
                        case CharacterAnimation.Action4: _actionView.Appear(pos, dir, Noroshi.BattleScene.Constant.ACTION_RANK_4_ANIMATION_NAME, 0); break;
                        default: throw new ArgumentException();
                    }
                    _actionView.SetVisible(true);
                    // 位置変更
                    _setPosition(_actionView.transform, 1000 * _characterView.transform.localScale.x, 0.0f);
                    // サイズ変更
                    _actionView.transform.localScale = _characterView.transform.localScale;
                }
                catch (ArgumentException e)
                {
                    // 存在しないアクションは非表示
                    _actionView.SetVisible(false);
                }
            }

            // アニメーション停止解除
            ((Noroshi.BattleScene.MonoBehaviours.CharacterView)_characterView).PauseOff();
            if (_actionView != null) _actionView.PauseOff();
        }

        // アニメーション停止
        public void StopAnimation()
        {
            if (!_isStart) return;
            
            if (!_isStop)
            {
                // アニメーション停止
                ((Noroshi.BattleScene.MonoBehaviours.CharacterView)_characterView).PauseOn();
                if (_actionView != null) _actionView.PauseOn();
                // タイムカウンタ一時停止
                _timeCountStop();
                _isStop = true;
            }
            else
            {
                // アニメーション停止解除
                ((Noroshi.BattleScene.MonoBehaviours.CharacterView)_characterView).PauseOff();
                if (_actionView != null) _actionView.PauseOff();
                // タイムカウンタ再開
                _timeCountRestart();
                _isStop = false;
            }
        }

        // 画面右下基準に位置調整
        private void _setPosition(Transform transform, float x, float y)
        {
            var cameraHeight = 5 * 2;
            var cameraWidth = (cameraHeight * Screen.width / Screen.height);
            var posx = _playPanel.sizeDelta.x + 50 + x;
            var posy = _playPanel.sizeDelta.y + 50 + y;
            posx = posx > Screen.width  ? Screen.width  : posx < 0 ? 0 : posx;
            posy = posy > Screen.height ? Screen.height : posy < 0 ? 0 : posy;
            var vec = transform.localPosition;
            vec.x =  ((Screen.width  * 0.5f - posx) / Screen.width ) * cameraWidth;
            vec.y = -((Screen.height * 0.5f - posy) / Screen.height) * cameraHeight;
            transform.localPosition = vec;
        }

        // タイムカウンタ設定
        private void _timeCountSet()
        {
            Observable.EveryUpdate().Subscribe(_ => 
            {
                if (_timeCountValueStart > 0)
                {
                    var currTime = Time.time;
                    var diffTime = currTime - _timeCountValueStart;
                    var nextText = diffTime.ToString("F2");
                    if (_playTimeText.text != nextText) _playTimeText.text = nextText;
                }
            });
        }

        // タイムカウンタカウント開始
        private void _timeCountStart()
        {
            _timeCountValueDiff = 0.0f;
            _timeCountValueStart = Time.time;
        }

        // タイムカウンタカウント停止
        private void _timeCountStop()
        {
            _timeCountValueDiff = Time.time - _timeCountValueStart;
            _timeCountValueStart = 0.0f;
        }

        // タイムカウンタカウント再開
        private void _timeCountRestart()
        {
            _timeCountValueStart = Time.time - _timeCountValueDiff;
        }
    }
}
