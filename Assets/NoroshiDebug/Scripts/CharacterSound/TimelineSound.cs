﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace NoroshiDebug.CharacterSound
{
    public class TimelineSound : MonoBehaviour
    {
        [SerializeField] InputField _timeInputField;
        [SerializeField] InputField _soundInputField;

        // 削除時に削除対象が OnNext される Subject。
        private Subject<GameObject> _onDeleteSubject = new Subject<GameObject>();

        // 時間
        public float Time {
            get {
                return float.Parse(_timeInputField.text);
            }
            set {
                _timeInputField.text = value.ToString();
            }
        }

        // サウンドID
        public uint SoundID {
            get {
                return uint.Parse(_soundInputField.text);
            }
            set {
                _soundInputField.text = value.ToString();
            }
        }

        // 削除時に削除対象が OnNext される Observable を取得。
        public IObservable<GameObject> GetOnDeleteObservable()
        {
            return _onDeleteSubject.AsObservable();
        }

        // 削除ボタン
        public void ButtonDelete()
        {
            _onDeleteSubject.OnNext(this.gameObject);
        }
    }
}
