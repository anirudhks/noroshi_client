﻿using UnityEngine;
using System.Collections;
using SmartBeat;

namespace SmartBeat
{

	public class SmartBeatBehaviour : MonoBehaviour {


#if UNITY_ANDROID
		private const string APP_KEY = "e3d8bf76-8f31-4222-ad9a-fe3cdcb1d268";	//TODO: fill in your application key provided from console.
#elif UNITY_IPHONE
		private const string APP_KEY = "bdb72934-6997-48ba-807b-8487c515241b";	//TODO: fill in your application key provided from console.
#else
		private const string APP_KEY = "";	//TODO: fill in your application key provided from console.
#endif

		void Awake(){
			SmartBeat.init(APP_KEY, enabled());
			init();
		}

		private void init(){
			SmartBeat.enableScreenshot();		//TODO uncomment this if screenshot should be enabled
 			SmartBeat.enableLog();				//TODO uncomment this if logcat/NSLog should be sent
//			SmartBeat.enableDebugLog("LogTag");	//TODO uncomment this and edit argument if log in SmartBeat library should be enabled
//			SmartBeat.enableLogRedirect("Unity");		//TODO unccoment this and edit argument is UnityLog shall be redirect to NSLog
		}

		private bool enabled(){
			//TODO
			// エラーの収集をデフォルトで無効にする場合は、こちらをfalseにしてください。
			bool enabled = true;

			// また、デフォルト値を設定等からGetする場合、こちらで読み込む処理を追加してください
			// 実装例)
			//    enabled = (PlayerPrefs.GetInt("任意の設定名", 0) != 0);

			// 有効・無効を切り替える場合は、下記APIを使用してください。
			//  SmartBeat.SmartBeat.enable();	//有効にする
			//  SmartBeat.SmartBeat.disable();	//無効にする
			//  ※PlayerPrefsなどの永続領域への保存は、アプリ側にて実装してください。

			return enabled;
		}

		void OnApplicationPause(bool pauseStatus){
			SmartBeat.onPause(pauseStatus);
		}

	}

}
