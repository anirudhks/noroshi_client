﻿using UnityEngine;
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

public class ModifyXcodeProject : MonoBehaviour
{
    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
    {
        if (buildTarget == BuildTarget.iOS) {
            string projPath = PBXProject.GetPBXProjectPath (path);
            PBXProject proj = new PBXProject ();
            proj.ReadFromString (File.ReadAllText (projPath));
            string target = proj.TargetGuidByName ("Unity-iPhone");

            // これでBitCodeEnableをNOに
            proj.SetBuildProperty(target, "ENABLE_BITCODE", "NO");
			// AdSupport.frameworkを追加
			proj.AddFrameworkToProject(target, "AdSupport.framework", false);

            File.WriteAllText(projPath, proj.WriteToString());
        }
    }
}
