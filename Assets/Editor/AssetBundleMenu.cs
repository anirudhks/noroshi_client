﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class AssetBundleMenu
{
	/// <summary>
	///  現在のアプリケーションにキャッシュされているすべての AssetBundle と Procedural マテリアルのコンテンツを削除します.
	/// </summary>
	[MenuItem ("AssetBundles/Clear Cache")]
	static void ClearCache ()
	{
		Caching.CleanCache ();
	}

	[MenuItem ("AssetBundles/Build Asset Bundles")]
	static void BuildAssetBundles ()
	{
		// Bring up save panel
		string path = Application.dataPath.Replace("/Assets","/BuildAssetBundles");
		if (path.Length != 0) {
			#if UNITY_IOS
			BuildBundle (path + "/iOS", BuildTarget.iOS);
			#elif UNITY_ANDROID
			BuildBundle (path + "/Android", BuildTarget.Android);
			#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
			BuildBundle (path + "/Win", BuildTarget.StandaloneWindows);
			#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
			BuildBundle (path + "/OSX", BuildTarget.StandaloneOSXUniversal);
			#endif
		}
	}

	static void BuildBundle (string path, BuildTarget target)
	{
		Debug.Log ("AssetBundle Make Dir : " + path);

		if (!Directory.Exists (path)) {
			Directory.CreateDirectory (path);
		}

		BuildPipeline.BuildAssetBundles (path, BuildAssetBundleOptions.None, target);
		Debug.Log (target.ToString ());
	}
}
