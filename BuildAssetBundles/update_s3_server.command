#!/bin/sh
cd `dirname $0`
echo "\n"

echo '>バケットを選択してください[0=開発, 1=本番]'
read b
bucket=noroshi-dev
if [ $b -eq 1 ]; then
bucket=noroshi-prod
fi
echo bucket:${bucket}

echo '> バージョンを入力してください(x.x.x)'
read version

echo '> 素材のグループを入力してください[0=チュートリアル, 1=通常]'
read group

echo "環境:${bucket} バージョン:${version} グループ:${group}\nよろしいですか？[y/n]"
read submit

if [ $submit == "y" ]; then
    echo "実行!"
aws s3 sync . s3://${bucket}/asset-bundle/${version}/${group}/ --exclude "*.DS_Store" --exclude "*.command"
    exit 1
else
    echo "中断しました"
    exit 2
fi


