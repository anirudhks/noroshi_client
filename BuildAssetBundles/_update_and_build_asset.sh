#!/bin/sh
ch `dirname $0`

#branch=feature/build-asset-bundles
branch=$1
bucket=$2
version=$3

build_platfrom_android=Android
build_platfrom_ios=iOS
build_platfrom_osx=OSX
build_platfrom_win=Win

bucket_prod=noroshi-prod
bucket_dev=noroshi-dev

init()
{
rm -rf $1
mkdir $1
mkdir $1/0
mkdir $1/1
}

update_git_platfrom()
{
echo "\n==============================="
echo Platfrom ${1} Update Start
echo "-------------------------------\n"

echo ${3}/${1}/Noroshi 
cd ${3}
git fetch
git reset --hard
git checkout .
git clean -f
git checkout -b $2 origin/$2
git checkout $2
git reset --hard
git checkout .
git clean -f
git pull origin $2
git checkout --ours .

echo "\n------------------------------"
echo Platfrom ${1} Update End
echo "==============================\n"
}

setup_platfrom_label_tutorial()
{
setup_platfrom_label_nomal ${1}
}

setup_platfrom_label_nomal()
{
cd ${1}
find . -type f -name '*meta' | xargs grep -l assetBundleName | xargs sed -i '' 's/assetBundleName.*/assetBundleName: /g'

find ./Sound/BGM -type f -name '*meta' | xargs grep -l assetBundleName | xargs sed -i '' 's/assetBundleName.*/assetBundleName: bgm /g'
find ./Sound/SE -type f -name '*meta' | xargs grep -l assetBundleName | xargs sed -i '' 's/assetBundleName.*/assetBundleName: se /g'
find ./Sound/Voice -type f -name  '*meta'| xargs grep -l assetBundleName | xargs sed -i '' 's/assetBundleName.*/assetBundleName: voice /g'
find ./Item -type f -name '*meta' | xargs grep -l assetBundleName | xargs sed -i '' 's/assetBundleName.*/assetBundleName: item /g'

for dir in ./Character/*; do
    if echo "$dir" | grep -v 'meta';then
        find $dir -type f -name '*meta' | xargs grep -l assetBundleName | xargs sed -i '' "s/assetBundleName.*/assetBundleName: character\/${dir##*/} /g"
    fi
done

for dir in ./UICharacter/*; do
    if echo "$dir" | grep -v 'meta';then
        find $dir -type f -name '*meta' | xargs grep -l assetBundleName | xargs sed -i '' "s/assetBundleName.*/assetBundleName: character\/${dir##*/} /g"
        for file in $dir/*; do
            if echo ${file##*/} | grep "Character.prefab" ; then
                mv $file ${file/\/Character.prefab/\/UICharacter.prefab}
            fi
        done
    fi
done
}

build_platform_asset_bundle()
{
echo "\n========================================"
echo Platfrom ${1} AssetBundle Build Start
echo "----------------------------------------\n"
rm -rf $2
/Applications/Unity/Unity.app/Contents/MacOS/Unity -batchmode -quit -username 'develop@flaggs.co.jp' -password 'deVflaggs2' -projectPath $3 -executeMethod AssetBundleMenu.BuildAssetBundles
mv $2 $4
echo "\n--------------------------------------\n"
echo Platfrom ${1} AssetBundle Build End
echo "========================================\n"
}

update_s3_server()
{
echo $1
cd $1
bucket=$2
version=$3
aws s3 sync . s3://${bucket}/asset-bundle/${version}/ --exclude "*.DS_Store" --exclude "*.command"
}

update_dir_path=/Users/flaggs/Desktop/UpdateData

root_project_path=/Users/flaggs/Desktop/Noroshi
root_client_path=/Noroshi/Noroshi.Client
client_resources_path=${root_client_path}/Assets/Noroshi/Resources
client_asset_bundle_path=${root_client_path}/Assets/Noroshi/AssetBundles
client_build_path=${root_client_path}/BuildAssetBundles

client_contstant_script_path=${root_project_path}/${build_platfrom_android}${root_client_path}/Assets/Noroshi/Scripts/AppConstant.cs

echo ${branch}

init $update_dir_path

for p in ${!build_platfrom_*}; do
    platfrom=${!p}
    update_git_platfrom $platfrom $branch ${root_project_path}/${platfrom}/Noroshi
done

for p in ${!build_platfrom_*}; do
    platfrom=${!p}
    setup_platfrom_label_tutorial ${root_project_path}/${platfrom}/${client_asset_bundle_path}
    setup_platfrom_label_tutorial ${root_project_path}/${platfrom}/${client_resources_path}
    build_platform_asset_bundle ${platfrom} ${root_project_path}/${platfrom}/${client_build_path}/${platfrom} ${root_project_path}/${platfrom}/${root_client_path} ${update_dir_path}/0
done

for p in ${!build_platfrom_*}; do
    platfrom=${!p}
    setup_platfrom_label_nomal ${root_project_path}/${platfrom}/${client_resources_path}
    setup_platfrom_label_nomal ${root_project_path}/${platfrom}/${client_asset_bundle_path}
    build_platform_asset_bundle ${platfrom} ${root_project_path}/${platfrom}/${client_build_path}/${platfrom} ${root_project_path}/${platfrom}/${root_client_path} ${update_dir_path}/1
done

#version=`grep APP_VERSION ${client_contstant_script_path} | sed -e 's/[^"]*"\([^"]*\)".*/\1/'`
update_s3_server ${update_dir_path} ${bucket} ${version}


